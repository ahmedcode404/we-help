<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains job categoreis - اقسام الادوار الوظيفيه
        Schema::create('job_categories', function (Blueprint $table) {
            $table->id();

            $table->string('slug');
            
            $table->string('name_ar');
            $table->string('name_en');
            $table->enum('guard', ['admin', 'charity']);
            $table->integer('guard_id')->nullable();

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_categories');
    }
}
