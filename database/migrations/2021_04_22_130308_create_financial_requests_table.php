<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains financial requests (طلبات الصرف و القبض مع السندات)
        Schema::create('financial_requests', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            // $table->float('cost'); // التكلفه
            // $table->float('total_supports'); // مجموع التبرعات
            // $table->float('remaining_cost'); // المتبقى
            $table->float('out_to_charity'); // المصروف للجمعيه
            
            /**
              * سندات الصرف فى الجمعيه ليها حالات:
              *  الحاله الاولى: مستحقه و دى هتبقى بشكل اتوماتيك عند انشاء السند من الادمن - deserved
              *  الحالة الثانية: مدفوعه و هى هتبقى من الادمن فى حالة تحويل الفلوس فعليا - paid
              *  الحالة الثالثة: تم تأكيد الإستلام و دا من الجمعيه فى حالة ان الجمعيه اتأكدت من نزول المبلغ فى حسابها بالفعل - recieved
              *  الحالة الرابعة: لم يتم التحويل و دا من الجمعيه برده فى حالة عدم نزول المبلغ فى حساب الجمعيه - not_transfered
             */
            $table->enum('recieve_status', ['deserved', 'paid', 'recieved', 'not_transfered'])->default('deserved'); 

            // $table->unsignedBigInteger('charity_id')->index()->nullable(); // الجمعيه
            // $table->foreign('charity_id')->references('id')->on('charities');


            $table->integer('requestable_id')->index()->nullable(); 
            $table->string('requestable_type')->index()->nullable();

            $table->unsignedBigInteger('project_id')->index()->nullable(); // المشروع
            $table->foreign('project_id')->references('id')->on('projects');

            $table->unsignedBigInteger('phase_id')->index()->nullable(); // المرحلة
            $table->foreign('phase_id')->references('id')->on('phases');

            $table->enum('status', ['waiting', 'approved', 'rejected', 'hold']); // حالة المشروع - انتظار - معتمده - مرفوضه - معلقه - مغلق
            $table->text('notes')->nullable(); // Extra notes

            $table->enum('type', ['exchange', 'catch']); // نوع الطلب (صرف - قبض)

            $table->string('bank_account_num')->nullable(); // رقم الحساب البنكى (فقط فى حالة اصدار السند)
            $table->string('voucher_num')->nullable(); // رقم الحواله (فقط فى حالة اصدار السند)

            //currency_id is set in currencies migration file

            // $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_requests');
    }
}
