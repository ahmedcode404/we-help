<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains agreements - كل العقود و الاتفاقيات الخاصه بالتسجيل
        Schema::create('agreements', function (Blueprint $table) {
            $table->id();

            $table->string('key'); // unique key to be used in queries

            $table->longText('content'); // محتوى كل عقد او اتفاقيه

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
