<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->id();

            $table->string('name_ar');
            $table->string('name_en');
            $table->string('symbol');
            $table->string('icon')->nullable();

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });

        Schema::table('project_user', function (Blueprint $table){

            $table->unsignedBigInteger('currency_id')->index()->nullable(); // العملة 
            $table->foreign('currency_id')->references('id')->on('currencies');
        });

        Schema::table('projects', function (Blueprint $table){

            $table->unsignedBigInteger('currency_id')->index()->nullable(); // العملة 
            $table->foreign('currency_id')->references('id')->on('currencies');
        });

        Schema::table('users', function (Blueprint $table){

            $table->unsignedBigInteger('currency_id')->index()->nullable(); // العملة 
            $table->foreign('currency_id')->references('id')->on('currencies');
        });

        Schema::table('charities', function (Blueprint $table){

            $table->unsignedBigInteger('currency_id')->index()->nullable(); // العملة 
            $table->foreign('currency_id')->references('id')->on('currencies');
        });  

        Schema::table('financial_requests', function (Blueprint $table){

            $table->unsignedBigInteger('currency_id')->index()->nullable(); // العملة 
            $table->foreign('currency_id')->references('id')->on('currencies');
        });  
              
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
