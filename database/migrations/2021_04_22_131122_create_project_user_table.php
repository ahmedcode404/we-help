<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Supports
        // Pivot table (user - project many to many relation) with extra data: (cost - type - currency)
        Schema::create('project_user', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->index()->nullable(); // الداعم / المتبرع
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('cart_no')->nullable(); // cart number useed in session when user is not logged in

            $table->unsignedBigInteger('project_id')->index()->nullable(); // المشروع
            $table->foreign('project_id')->references('id')->on('projects');

            $table->enum('status', ['cart', 'support']); // cart -> project is added to cart - support -> actual support process and project removed from cart

            $table->double('cost', 20, 4)->nullable(); // التكلفه
            $table->double('cost_gbp', 20, 4)->nullable(); // التكلفه
            $table->enum('type', ['monthly', 'once' , 'gift' , 'ambassador']); // نوع الاستقطاع (مره واحده - شهرى)

            $table->string('name_anonymouse')->nullable(); //  الاسم اذا كان متبرع مجهول

            $table->enum('project_covering', ['total', 'partial'])->nullable(); // تغطية المشروع كلى - جزئى فى حالة التبرعاعت المباشره من خارج النظام - vip supports

            $table->string('receipt')->nullable(); // ايصال الدفع فى حالة التبرعات المباشره من خارج النظام - vip supports

            $table->boolean('vip')->default(0); // vip supports

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_user');
    }
}
