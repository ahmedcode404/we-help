<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSponsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // الاطراف الخارجيه ذات علاقة بالمشروع
        Schema::create('sponsers', function (Blueprint $table) {
            $table->id();

            $table->string('name_ar'); // اسم الجهه
            $table->string('name_en'); // اسم الجهه

            $table->unsignedBigInteger('work_field_id')->index()->nullable(); // مجال عمل الجهه الخارجية المشاركة فى المشروع
            $table->foreign('work_field_id')->references('id')->on('work_fields');

            $table->unsignedBigInteger('project_id')->index()->nullable(); //  أطراف خارجية ذات علاقة بالمشروع -  رقم المشروع
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sponsers');
    }
}
