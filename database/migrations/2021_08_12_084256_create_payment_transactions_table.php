<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transactions', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->index()->nullable();  
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('project_id')->index()->nullable(); // المشروع  
            $table->foreign('project_id')->references('id')->on('projects');

            $table->text('product_id')->nullable();
            $table->text('setup_fee_id')->nullable();
            $table->text('setup_fee_price_id')->nullable();
            $table->text('customer_id')->nullable();
            $table->text('invoice_item_id')->nullable();
            $table->text('invoice_id')->nullable();
            $table->text('subscription_id')->nullable();
            $table->text('session_id')->nullable();

            $table->enum('type', ['once', 'monthly']);

            $table->boolean('canceled')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transactions');
    }
}
