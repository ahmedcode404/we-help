<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEditRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabel contains charities and projects edit requests
        Schema::create('edit_requests', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('model_id')->index()->nullable(); // الجمعية -  المشروع
            
            $table->enum('type', ['project', 'charity']); // تعديل بيانات الجمعيه - تعديل بيانات المشروع
            $table->enum('status', ['waiting', 'approved', 'rejected', 'hold']); // حالة المشروع - انتظار - معتمده - مرفوضه - معلقه - مغلق
            $table->text('notes')->nullable(); // Extra notes

            $table->unsignedBigInteger('emp_id')->nullable(); // ال id بتاع الشخص اللى باعت الطلب

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('edit_requests');
    }
}
