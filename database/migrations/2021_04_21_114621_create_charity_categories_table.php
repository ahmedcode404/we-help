<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharityCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains categories opf charities
        Schema::create('charity_categories', function (Blueprint $table) {
            $table->id();

            $table->string('slug');
            
            $table->string('name_ar'); // 
            $table->string('name_en'); // 
            $table->float('superadmin_ratio')->default(0); // نسبة ربح السوبر ادمن 
            $table->string('main_color')->nullable(); //  اللون الخاص بالقسم - لون تقيل
            $table->string('sub_color')->nullable(); //  اللون الخاص بالقسم - لون خفيف
            $table->string('icon')->nullable(); // الايقونه الخاصه بالقسم - مستخدمه فى التطبيق فقط

            /**
             * انواع التصنيفات
             * engineering: انشائية و مطلوب معاها ادخل ال (eng_maps)
             * educational: تعليمية و مطلوب معها ادخال ال (stage, edu_service_other)  
             * sponsorship: كفالة اليتيم و مطلوب معها ادخال ال (care_type, care_duration)
             * emergency: طارئة و يطلب معها ادخال ال (emerg_service_other)
             * sustainable: مشاريع مستدامة و يطلب معها ادخال ال (stable_project)
            */
            $table->enum('type', ['engineering', 'educational', 'sponsorship', 'emergency', 'sustainable'])->nullable(); 

            $table->integer('nation_id')->nullable(); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charity_categories');
    }
}
