<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains phases of each project
        Schema::create('phases', function (Blueprint $table) {
            $table->id();

            $table->string('slug');
            
            $table->string('name_ar')->nullable(); // اسم المرحلة عربى
            $table->string('name_en'); // اسم المرحلة انجليزى

            $table->text('desc_ar')->nullable(); // وصف المرحلة عربى
            $table->text('desc_en'); // وصف المرحلة انجليزى

            $table->date('start_date'); // تاريخ بداية المرحلة
            $table->date('end_date'); // تاريخ نهاية المرحلة
            $table->integer('duration'); // المده
            $table->float('cost'); // تكلفة المرحلة
            $table->float('remaining_cost')->default(0); //  المتبقى من تكلفة المرحلة
            $table->text('cost_details_ar')->nullable(); // التكلفة التفصيليه 
            $table->text('cost_details_en'); // التكلفة التفصيليه 

            $table->text('output_ar')->nullable(); // مخرجات المرحلة عربى
            $table->text('output_en'); // مخرجات المرحلة انجليزى
            
            $table->integer('order'); // ترتيب المرحله (مرحلة اولى - ثانيه - ...)
            
            $table->boolean('approved')->default(0); // اعتماد المرحلة

            $table->text('notes')->nullable(); // Extra notes

            $table->unsignedBigInteger('project_id')->index()->nullable(); // المشروع
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phases');
    }
}
