<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains charities data - بيانات الجمعيه
        Schema::create('charities', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            // ALTER TABLE `charities` DROP FOREIGN KEY `charities_city_id_foreign`

            // BASIC INFO
            $table->enum('type', ['foundation', 'organization', 'charity'])->nullable(); // النوع: مؤسسه - جمعيه - منظمه
            $table->string('name')->nullable(); // اسم الجمعيه
            $table->string('license_number')->nullable(); // رقم الترخيص 
            $table->date('license_start_date')->nullable(); // تاريخ الترخيص
            $table->date('license_end_date')->nullable(); // تاريخ انتهاء الترخيص
            $table->string('license_file')->nullable(); // صورة الترخيص - image
            $table->date('establish_date')->nullable(); // تاريخ التأسيس
            $table->integer('branches_num')->nullable(); // عدد الفروع
            $table->integer('members_num')->nullable(); // عدد الأعضاء
            $table->date('contract_date')->nullable(); // تاريخ التعاقد
            $table->text('strategy_ar')->nullable(); // استراتيجية الجمعيه
            $table->text('strategy_en')->nullable(); // استراتيجية الجمعيه
            $table->string('attach')->nullable(); // المرفقات (صورة من صك الملكية أو عقد الأيجار) - image
            $table->string('internal_image')->nullable(); // صورة من النظام الداخلى للجمعيه - pdf
            $table->longText('year_report')->nullable(); // التقرير السنوى - file pdf
            $table->enum('years', ['one', 'two', 'three'])->nullable(); // عام - عامان - ثلاث اعوام و يستخدم مع التقرير السنوى 
            $table->string('logo')->default('charity/avatar.png'); // اللوجو - image

            $table->text('work_fields')->nullable();

            $table->integer('verification_code')->nullable();

            // ADDRESS INFO
            $table->string('address_ar')->nullable(); //  العنوان
            $table->string('address_en')->nullable(); //  العنوان
            $table->string('password')->nullable(); // كلمة المرور 

            $table->string('country')->nullable();
            $table->string('city')->nullable();

            // $table->unsignedBigInteger('city_id')->index()->nullable()->nullable(); // المدينه
            // $table->foreign('city_id')->references('id')->on('cities');

            // $table->unsignedBigInteger('country_id')->index()->nullable(); // الدوله
            // $table->foreign('country_id')->references('id')->on('cities');

            // CONTACT INFO
            $table->string('phone')->nullable(); // هاتف الجمعيه
            $table->string('mobile')->nullable(); // جوال الجمعيه
            $table->string('email')->nullable(); // بريد الجمعيه
            
            $table->string('representer_name_ar')->nullable(); // اسم ممثل الجمعيه
            $table->string('representer_name_en')->nullable(); // اسم ممثل الجمعيه
            $table->string('representer_passport_image')->nullable(); // صورة جواز السفر لممثل الجمعيه - image
            $table->string('representer_nation_image')->nullable(); // صورة الهويه لممثل الجمعيه - image
            $table->string('representer_title_ar')->nullable(); // صفة الممثل عربى  
            $table->string('representer_title_en')->nullable(); // صفة الممثل انجليزى
            $table->string('representer_title_file')->nullable(); //  تحميل صورة من القرار او التكليف - pdf
            $table->boolean('international_member') ->default(0)->nullable(); // هل الجمعيه عضو فى اى منظمه دولية
            $table->text('international_name_ar')->nullable(); // اسماء المنظمات الدوليه اللى الجمعيه عضو فيها ان وجد
            $table->text('international_name_en')->nullable(); // اسماء المنظمات الدوليه اللى الجمعيه عضو فيها ان وجد
            $table->string('representer_email')->nullable(); // بريد ممثل الجمعية

            // SOCIAL INFO
            $table->string('website')->nullable(); // الموقع الالكترونى
            $table->string('twitter_link')->nullable(); // رابط تويتر
            $table->string('facebook_link')->nullable(); // رابط الفيس بوك

            // BANK ACCOUNT INFO
            $table->string('iban')->nullable(); // رقم الايبان
            $table->string('bank_name')->nullable(); // اسم البنك
            $table->string('bank_address')->nullable(); // عنوان البنك
            $table->string('bank_city')->nullable(); // مدينة البنك
            $table->string('bank_phone')->nullable(); // هاتف البنك
            $table->string('swift_code')->nullable(); // السويفت كود

            // CONTRACT INFO
            $table->string('contract_after_sign')->nullable();
            $table->longText('signature')->nullable();

            // APPROVEMENT STATUS INFO
            $table->enum('status', ['waiting', 'approved', 'rejected', 'hold']); // حالة الجمعيه - انتظار - معتمده - مرفوضه - معلقه
            $table->text('notes')->nullable(); // Extra notes

            // النسخه
            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->string('device_token')->nullable(); // توكين فايربيز
            $table->string('is_notify')->default(1); // توكين فايربيز


            // Employee Data
            $table->string('name_ar')->nullable(); // الاسم عربى ثلاثى
            $table->string('name_en')->nullable(); // الاسم انجليزى ثلاثى - ممكن ما ادخلش هنا كلام لان الداعم مش هيدخل الاسم بالانجليزى
            $table->string('nation_number')->unique()->nullable(); // رقم الهويه الوطنيه
            $table->string('region')->nullable(); // الحى السكنى    
            $table->string('street')->nullable(); // الشارع
            $table->string('unit_number')->nullable(); // رقم الوحده
            $table->string('mail_box')->nullable(); // الصندوق البريدى
            $table->string('postal_code')->nullable(); // الرمز البريدى
            $table->date('contract_start_date')->nullable(); // تاريخ بداية التعاقد
            $table->date('contract_end_date')->nullable(); // تاريخ نهاية التعاقد
            $table->integer('parent_id')->nullable(); // للجمعية - parent_id == null ? charity : charity employee
            $table->boolean('first_login')->default(1)->nullable(); // تسجيل دخول للمره الاول - هيبقى فى الاول 1 علشان نظهر الاتفاقيه و بعد كدا هتبقى 0 و مش هتتعدل تانى
            $table->boolean('agreement_approve')->default(0)->nullable(); // الموافقه على الاتفاقيه و بعد كدا هتبقى 1 و مش هتتعدل تانى


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charities');
    }
}
