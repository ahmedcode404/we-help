<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmbassadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambassadors', function (Blueprint $table) {
            $table->id();

            $table->string('ambassador_name');
            $table->string('campaign_name');            
            
            $table->string('affiliate'); // رابط الحمله - زى التسويق بالظبط هيتم انشاء رابط و تحسب عدد المشاهدات من الكليك بتاعته
            $table->bigInteger('seen')->default(0); // تم المشاهده و هيت حسابها من عدد النقرات على رابط الحمله

            $table->unsignedBigInteger('user_id')->index()->nullable();  
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('project_id')->index()->nullable();  
            $table->foreign('project_id')->references('id')->on('projects');

            $table->unsignedBigInteger('campaign_goal_id')->index()->nullable();  
            $table->foreign('campaign_goal_id')->references('id')->on('campaign_goals');            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambassadors');
    }
}
