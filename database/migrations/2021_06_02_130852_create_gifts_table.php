<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gifts', function (Blueprint $table) {
            $table->id();

            $table->string('sender_nick_name')->nullable(); // اسم المتبرع
            
            $table->unsignedBigInteger('user_id')->index()->nullable(); // المتبرع
            $table->foreign('user_id')->references('id')->on('users');
            

            $table->string('gift_owner_name'); // اسم صاحب الهديه
            $table->string('email'); // ايميل صاحب الهديه
            $table->string('phone'); // هاتف صاحب الهديه

            $table->unsignedBigInteger('project_id')->index()->nullable(); // المشروع
            $table->foreign('project_id')->references('id')->on('projects');
            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
