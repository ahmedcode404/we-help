<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains Admin - Empployees - Supporters
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            $table->string('name_ar'); // الاسم عربى ثلاثى
            $table->string('name_en')->nullable(); // الاسم انجليزى ثلاثى - ممكن ما ادخلش هنا كلام لان الداعم مش هيدخل الاسم بالانجليزى

            $table->string('password'); // كلمة المرور
            $table->string('email')->unique(); // البريد الالكترونى
            $table->string('phone')->nullable(); // الهاتف // is set to nullable because supporter dose not require phone to register
            $table->string('nation_number')->unique()->nullable(); // رقم الهويه الوطنيه

            $table->integer('verification_code')->nullable();

            $table->string('region')->nullable(); // الحى السكنى    
            $table->string('street')->nullable(); // الشارع
            $table->string('unit_number')->nullable(); // رقم الوحده
            $table->string('mail_box')->nullable(); // الصندوق البريدى
            $table->string('postal_code')->nullable(); // الرمز البريدى

            $table->date('contract_start_date')->nullable(); // تاريخ بداية التعاقد
            $table->date('contract_end_date')->nullable(); // تاريخ نهاية التعاقد

            $table->boolean('first_login')->default(1)->nullable(); // تسجيل دخول للمره الاول - هيبقى فى الاول 1 علشان نظهر الاتفاقيه و بعد كدا هتبقى 0 و مش هتتعدل تانى
            $table->boolean('agreement_approve')->default(0)->nullable(); // الموافقه على الاتفاقيه و بعد كدا هتبقى 1 و مش هتتعدل تانى

            $table->boolean('blocked')->defaulte(0); // blocked == 1 ? user is blocked : user is not blocked و تستخم فقط للداعمين
            
            $table->timestamp('email_verified_at')->nullable();

            $table->string('device_token')->nullable(); // توكين فايربيز
            $table->string('is_notify')->default(1); //  1 -> notifications is enabled / 0 -> notifications in disabled

            $table->string('image')->nullable(); // profile picture

            $table->text('api_token')->nullable(); // laravel sanctum api token

            $table->string('country')->nullable();
            $table->string('city')->nullable();
            
            // default currency_id is set in currencies table

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
