<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();

            $table->string('title');

            $table->unsignedBigInteger('send_user_id')->index()->nullable();

            $table->string('model_id')->nullable();
            $table->string('type')->nullable();

            $table->text('message_ar')->nullable();
            $table->text('message_en')->nullable();

            $table->text('content_ar')->nullable();
            $table->text('content_en')->nullable();  
            
            $table->string('image')->nullable();

            $table->integer('read')->default(0);            

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
