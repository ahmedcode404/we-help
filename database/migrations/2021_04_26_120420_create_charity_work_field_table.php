<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharityWorkFieldTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('charity_work_field', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('charity_id')->index()->nullable(); // الجمعيه
            $table->foreign('charity_id')->references('id')->on('charities');

            $table->unsignedBigInteger('work_field_id')->index()->nullable(); // مجال العمل
            $table->foreign('work_field_id')->references('id')->on('work_fields');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charity_work_field');
    }
}
