<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketingUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing_user', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('marketing_id')->index()->nullable();  
            $table->foreign('marketing_id')->references('id')->on('marketings')->onDelete('cascade');

            $table->unsignedBigInteger('user_id')->index()->nullable();  
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing_user');
    }
}
