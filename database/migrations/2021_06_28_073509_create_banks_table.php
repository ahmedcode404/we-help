<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function (Blueprint $table) {
            $table->id();
            $table->string('name_ar');
            $table->string('name_en');
            $table->string('address');
            $table->string('phone')->unique();

            $table->unsignedBigInteger('city_id')->index()->nullable()->nullable(); // المدينه
            $table->foreign('city_id')->references('id')->on('cities');

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه
            $table->timestamps();
        });

        Schema::table('charities', function(Blueprint $table){

            $table->unsignedBigInteger('bank_id')->index()->nullable(); // اسم البنك
            $table->foreign('bank_id')->references('id')->on('banks');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banks');
    }
}
