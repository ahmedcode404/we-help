<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharityCharityCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains charity and charity categories relation (many to many relation)
        // Each charity can belong to many categories and each category can contain many charities
        Schema::create('charity_charity_category', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('charity_id')->index()->nullable(); // الجمعيه
            $table->foreign('charity_id')->references('id')->on('charities');
            
            $table->unsignedBigInteger('charity_category_id')->index()->nullable(); // تصنيف الجمعيه
            $table->foreign('charity_category_id')->references('id')->on('charity_categories');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('charity_charity_category');
    }
}
