<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains projects images
        Schema::create('images', function (Blueprint $table) {
            $table->id();

            $table->integer('imageable_id')->index()->nullable(); 
            $table->string('imageable_type')->index()->nullable();

            $table->string('path'); // image path

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
}
