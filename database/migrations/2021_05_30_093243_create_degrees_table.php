<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDegreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('degrees', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            $table->string('name_ar');
            $table->string('name_en');

            $table->enum('guard', ['admin', 'charity']);
            $table->integer('guard_id')->nullable();

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });

        Schema::table('users', function(Blueprint $table) {

            $table->unsignedBigInteger('degree_id')->index()->nullable(); // degree - الدرجة العلمية
            $table->foreign('degree_id')->references('id')->on('degrees');

        }); 
        
        Schema::table('charities', function(Blueprint $table) {

            $table->unsignedBigInteger('degree_id')->index()->nullable(); // degree - الدرجة العلمية
            $table->foreign('degree_id')->references('id')->on('degrees');

        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('degrees');
    }
}
