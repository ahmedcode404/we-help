<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains website + dashboards settings
        Schema::create('settings', function (Blueprint $table) {
            $table->id();

            $table->string('key');
            $table->string('neckname'); // الاسم اللى هيظهر فى لوحة التحكم
            $table->mediumText('type'); // نوع ال input - file - textarea - ....
            $table->mediumText('value'); // 
            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
