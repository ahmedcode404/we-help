<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_options', function (Blueprint $table) {
            $table->id();

            $table->string('name_ar');
            $table->string('name_en');

            $table->unsignedBigInteger('parent_id')->nullable();

            $table->unsignedBigInteger('charity_category_id')->index()->nullable();  
            $table->foreign('charity_category_id')->references('id')->on('charity_categories')->onDelete('cascade');

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });

        Schema::table('projects', function (Blueprint $table){

            $table->unsignedBigInteger('service_option_id')->index()->nullable();  
            $table->foreign('service_option_id')->references('id')->on('service_options')->onDelete('cascade');

            $table->unsignedBigInteger('service_feature_id')->index()->nullable();  
            $table->foreign('service_feature_id')->references('id')->on('service_options')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_options');
    }
}
