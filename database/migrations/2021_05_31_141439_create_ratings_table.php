<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('user_id')->index()->nullable(); // user
            $table->foreign('user_id')->references('id')->on('users');

            $table->unsignedBigInteger('project_id')->index()->nullable(); // project
            $table->foreign('project_id')->references('id')->on('projects');

            $table->unsignedBigInteger('rating_criteria_id')->index()->nullable(); // degree
            $table->foreign('rating_criteria_id')->references('id')->on('rating__criterias');

            $table->enum('grade', [1, 2, 3, 4, 5]);
            $table->string('comment')->nullable();
            
            $table->boolean('show_for_web')->default(0);

            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
