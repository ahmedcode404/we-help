<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNationalitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nationalities', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            $table->string('name_ar');
            $table->string('name_en');

            $table->enum('guard', ['admin', 'charity']);
            $table->integer('guard_id')->nullable();

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });

        Schema::table('users', function(Blueprint $table){

            $table->unsignedBigInteger('nationality_id')->index()->nullable(); //  الجنسية
            $table->foreign('nationality_id')->references('id')->on('nationalities');

        });

        Schema::table('charities', function(Blueprint $table){

            $table->unsignedBigInteger('nationality_id')->index()->nullable(); //  الجنسية
            $table->foreign('nationality_id')->references('id')->on('nationalities');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nationalities');
    }
}
