<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains projects data
        Schema::create('projects', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            // BASIC INFO
            $table->integer('project_num')->unique();
            $table->unsignedBigInteger('charity_category_id')->index()->nullable(); // نوع المشروع حسب تصنيف الجمعيه بتاعته 
            $table->foreign('charity_category_id')->references('id')->on('charity_categories');
            // $table->enum('name', ['health', 'medical', 'drilling', 'sponsoring'])->nullable(); // اسم المشروع: حملات صحيه - حملات طبيه -حفر بئر - كفالة يتيم
            $table->string('name')->nullable(); // اسم المشروع: حملات صحيه - حملات طبيه -حفر بئر - كفالة يتيم
            $table->string('type')->nullable(); // نوع المشروع
            $table->integer('duration')->nullable(); // المده
            $table->date('start_date')->nullable(); // تاريخ البداية

            $table->string('image')->nullable(); // الصورة الاساسية للمشروع اللى هيتم عرضها فى الموقع فى الرئيسيه

            $table->unsignedBigInteger('charity_id')->index()->nullable(); // الجمعية 
            $table->foreign('charity_id')->references('id')->on('charities');
            

            // GOOGLE MAP INFO
            $table->string('location')->nullable(); // الموقع على الخريطه
            $table->string('lat')->nullable(); // خط الطول 
            $table->string('lng')->nullable(); // خط العرض

            $table->string('country')->nullable();
            $table->string('city')->nullable();

            // $table->unsignedBigInteger('city_id')->index()->nullable(); // المدينه
            // $table->foreign('city_id')->references('id')->on('cities');

            // $table->unsignedBigInteger('country_id')->index()->nullable(); // الدوله
            // $table->foreign('country_id')->references('id')->on('cities');
            
            // APPROVEMENT STATUS INFO
            $table->enum('status', ['waiting', 'approved', 'rejected', 'hold'])->nullable(); // حالة الجمعيه - انتظار - معتمده - مرفوضه - معلقه
            $table->text('notes')->nullable(); // Extra notes            

            // GENERAL DESCRIPTION
            $table->integer('benef_num')->nullable(); // عدد  المستفيدين
            
            $table->text('goals_ar')->nullable(); // الاهداف عربى
            $table->text('goals_en')->nullable(); // الاهداف انجليزى

            $table->text('desc_ar')->nullable(); // وصف المشروع عربى
            $table->text('desc_en')->nullable(); // وصف المشروع انجليزى

            $table->longText('long_desc_ar')->nullable(); // تفاصيل المشروع عربى
            $table->longText('long_desc_en')->nullable(); // تفاصيل المشروع انجليزى

            $table->string('attach')->nullable(); // مرفق المواصفات العامه للمشروع 

            // CONSTRUCTION PROJECTS DESC 
            $table->string('eng_maps')->nullable(); // الخرائط الهندسيه للمشروع - مطلوب فقط فى حالة المشاريع الانشائيه

            $table->boolean('active')->default(1); //  هل المشروع يقبل التبرعات ام لا - فى حالة حذف المشروع من السوبر ادمن مش بيتحذف فعليا لكن بنغير القيمه دى ل 0 و مش بنقبل تبرعات ليه

            // service_option_id -> in service_options mogration file

            // service_feature_id -> in service options migration file
            
            $table->string('other_service_option')->nullable();

            $table->string('other_service_feature')->nullable();
            
            $table->double('cost', 20, 4)->nullable(); // التكلفه
            
            $table->boolean('profile')->default(0); //  لو ب 1 يبقى دا مشروع فى البروفايل بتاع الجمعيه و مش هيظهر فى الموقع مع المشاريع اللى بيتم التبرع ليها

            $table->boolean('donation_complete')->default(0); //  1 -> project donation complete - 0 project donation not complete

            // النسخه
            $table->integer('nation_id')->nullable(); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
