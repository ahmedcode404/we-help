<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains marketings - حملات التسويق
        Schema::create('marketings', function (Blueprint $table) {
            $table->id();

            $table->enum('type', ['sms', 'email', 'mobile']); //  نوع الحملة
            $table->string('title')->nullable(); // عنوان الحملة
            $table->longText('content')->nullable(); // محتوى الحملة
            $table->date('date'); // تاريخ الحملة
            $table->time('time'); // توقيت الحملة
            $table->string('aff')->nullable(); // رابط الحمله - زى التسويق بالظبط هيتم انشاء رابط و تحسب عدد المشاهدات من الكليك بتاعته
            $table->enum('intended_users', ['all', 'most_donated', 'least_donated', 'never_donated']); // المستخدمين المستهدفين (الاكثر تبرعا - الكل - الاقل تبرعا - مسجل و لم يتبرع)
            $table->bigInteger('seen')->default(0); // تم المشاهده و هيت حسابها من عدد النقرات على رابط الحمله

            $table->string('project_link')->nullable(); // المشروع

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketings');
    }
}
