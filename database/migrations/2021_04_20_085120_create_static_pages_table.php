<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Tabel contains static pages content
        Schema::create('static_pages', function (Blueprint $table) {
            $table->id();

            $table->string('key'); // unique key to be used in queries

            // $table->enum('type', ['main', 'sub']);
            $table->integer('parent_id')->nullable();
            
            $table->string('title_ar')->nullable(); // titles can be changed by user
            $table->string('title_en')->nullable(); // titles can be changed by user

            $table->longText('content_ar')->nullable(); // content can be changed by user
            $table->longText('content_en')->nullable(); // content can be changed by user

            $table->string('image')->nullable();
            $table->string('link')->nullable();

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_pages');
    }
}
