<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contians slider's data of the website
        Schema::create('sliders', function (Blueprint $table) {
            $table->id();

            $table->string('path');
            $table->string('title_ar');
            $table->string('title_en');
            $table->text('text_ar');
            $table->text('text_en');
            $table->enum('appearance', ['mob', 'web']);

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
