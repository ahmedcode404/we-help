<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains jobs - الادوار الوظيفيه
        Schema::create('m_jobs', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            $table->string('name_ar');
            $table->string('name_en');
            $table->enum('guard', ['admin', 'charity']);
            $table->integer('guard_id')->nullable();

            $table->unsignedBigInteger('job_category_id')->index()->nullable(); // القسم
            $table->foreign('job_category_id')->references('id')->on('job_categories');

            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table){

            $table->unsignedBigInteger('job_id')->index()->nullable(); // الوظيفة 
            $table->foreign('job_id')->references('id')->on('m_jobs');
            
        });

        Schema::table('charities', function (Blueprint $table){

            $table->unsignedBigInteger('job_id')->index()->nullable(); // الوظيفة 
            $table->foreign('job_id')->references('id')->on('m_jobs');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_jobs');
    }
}
