<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrozenSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // جدول بيتخزن فيه التبرعات المجمده للمشاريع اللى جمعيتها اتحذفت و التبرعات بتاعتها ما دخلتش فى حساب مشاريع تانيه
        Schema::create('frozen_supports', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            // المشروع اللى تم تحويل الفلوس منه
            $table->unsignedBigInteger('from_project_id')->index()->nullable(); // المشروع  
            $table->foreign('from_project_id')->references('id')->on('projects');

            $table->double('prev_donations', 20, 4); // التبرعات السابقه للمشروع المحذوف
            $table->double('out_to_charity', 20, 4); // ما تم تحويله فعليا للجمعيه

            // المشروع اللى تم تحويل الفلوس عليه
            $table->unsignedBigInteger('to_project_id')->index()->nullable(); // المشروع  
            $table->foreign('to_project_id')->references('id')->on('projects');

            $table->double('to_project_cost', 20, 4)->nullable();

            $table->double('cost', 20, 4); // المبلغ المجمد - المتبقى من المشروع
            

            $table->unsignedBigInteger('currency_id')->index()->nullable(); // العملة 
            $table->foreign('currency_id')->references('id')->on('currencies');

            $table->longText('supporters_ids')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('frozen_supports');
    }
}
