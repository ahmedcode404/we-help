<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains cities and countries and are related to each user through parent_id
        Schema::create('cities', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            
            $table->string('name_ar'); // الاسم بالعربى
            $table->string('name_en'); // الاسم انجليزى
            $table->integer('parent_id')->nullable(); // للدوله - parent_id == null ? country : city

            $table->string('code')->nullable(); // Country code (SA - UK - EG - ...)

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه
            
            $table->timestamps();
        });

        Schema::table('users', function(Blueprint $table) {

            // $table->unsignedBigInteger('country_id')->index()->nullable(); // الدوله - للداعم فقط
            // $table->foreign('country_id')->references('id')->on('cities');

            $table->integer('nation_id'); // كود الدوله - هيتم استخدامه فى التفرقه بين النسخه السعوديه و النسخه البريطانيه
        });

        Schema::table('users', function(Blueprint $table) {

            // $table->unsignedBigInteger('city_id')->index()->nullable(); // الدوله - للداعم فقط
            // $table->foreign('city_id')->references('id')->on('cities');

        });        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
