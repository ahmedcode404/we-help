<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Table contains reports of project phases
        Schema::create('reports', function (Blueprint $table) {
            $table->id();

            $table->string('slug');

            $table->unsignedBigInteger('charity_id')->index()->nullable(); // الجمعية
            $table->foreign('charity_id')->references('id')->on('charities');

            $table->unsignedBigInteger('project_id')->index()->nullable(); // المشروع
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');

            $table->unsignedBigInteger('phase_id')->index()->nullable(); // المرحلة
            $table->foreign('phase_id')->references('id')->on('phases')->onDelete('cascade');

            $table->longText('report_ar')->nullable(); //  التقرير بالعربي
            $table->longText('report_en')->nullable(); // التقرير بالانجليزي

            $table->string('vedio')->nullable(); // الفيديو

            $table->enum('status', ['waiting', 'approved', 'rejected', 'hold', 'closed'])->nullable(); // حالة المشروع - انتظار - معتمده - مرفوضه - معلقه - مغلق
            $table->text('notes')->nullable(); // Extra notes  

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
