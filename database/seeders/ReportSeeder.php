<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class ReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reports')->insert([
            [
                'charity_id' => 1,
                'project_id' => 1,
                'phase_id' => 1,
                'report_ar' => 'report1 ar',
                'report_en' => 'report1 en',
                'vedio' => 'aaaaa1',
                'status' => 'approved',
            ],
            [
                'charity_id' => 1,
                'project_id' => 1,
                'phase_id' => 2,
                'report_ar' => 'report2 ar',
                'report_en' => 'report2 en',
                'vedio' => 'aaaaa2',
                'status' => 'rejected'
            ],
            [
                'charity_id' => 1,
                'project_id' => 1,
                'phase_id' => 3,
                'report_ar' => 'report3 ar',
                'report_en' => 'report3 en',
                'vedio' => 'aaaaa3',
                'status' => 'waiting'
            ],
        ]);
    }
}
