<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use App\Models\User;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([

            // SA
            [
                'name_ar' => 'دولار أمريكى',
                'name_en' => 'USD',
                'symbol' => 'USD',
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'جنية إسترلينى',
                'name_en' => 'GBP',
                'symbol' => 'GBP',
                'nation_id' => 1,
            ],

            
            // UK
            [
                'name_ar' => 'دولار أمريكى',
                'name_en' => 'USD',
                'symbol' => 'USD',
                'nation_id' => 2,
            ],
            [
                'name_ar' => 'جنية إسترلينى',
                'name_en' => 'GBP',
                'symbol' => 'GBP',
                'nation_id' => 2,
            ],
        ]);

        //SA
        if(getNationId() == 1){

            User::find(1)->update(['currency_id' => 2]);
        }
        //UK
        else{

            User::find(1)->update(['currency_id' => 4]);
        }
    }
}
