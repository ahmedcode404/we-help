<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            // city egypt
            // [
            //     'name_ar' => 'المنصوره',
            //     'name_en' => 'Mansoura',
            //     'parent_id' => 3,
            //     'nation_id' => 3,
            // ],
            // [
            //     'name_ar' => 'القاهره',
            //     'name_en' => 'Cairo',
            //     'parent_id' => 3,                
            //     'nation_id' => 3,
            // ],
            // [
            //     'name_ar' => 'السنبلاوين',
            //     'name_en' => 'Alsinblawen',
            //     'parent_id' => 3,                
            //     'nation_id' => 3,
            // ],
            
            // city Britain
            [
                'name_ar' => 'بريطانيا 1',
                'name_en' => 'Britain 1',
                'slug' => 'britain1',
                'parent_id' => 2,
                'nation_id' => 2,
            ],
            [
                'name_ar' => 'بريطانيا 2',
                'name_en' => 'Britain 2',
                'slug' => 'britain2',
                'parent_id' => 2,
                'nation_id' => 2,
            ],
            [
                'name_ar' => 'بريطانيا 3',
                'name_en' => 'Britain 3',
                'slug' => 'britain3',
                'parent_id' => 2,
                'nation_id' => 2,
            ],

            // city saudia
            [
                'name_ar' => 'الرياض',
                'name_en' => 'Elryad',
                'slug' => 'Elryad',
                'parent_id' => 1,
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'الدمام',
                'name_en' => 'eldmam',
                'slug' => 'eldmam',
                'parent_id' => 1,
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'مكه',
                'name_en' => 'maka',
                'slug' => 'maka',
                'parent_id' => 1,
                'nation_id' => 1,
            ],            
           
            
        ]);
    }
}
