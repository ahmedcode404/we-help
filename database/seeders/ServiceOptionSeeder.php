<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ServiceOption;

class ServiceOptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Educationsl services
        $edu_primary = ServiceOption::create(['name_ar' => 'المرحلة الإبتدائية', 'name_en' => 'Primary Stage', 'charity_category_id' => 10, 'nation_id' => 2]);
        $edu_secondary = ServiceOption::create(['name_ar' => 'المرحلة الإعدادية', 'name_en' => 'Secondary Stage', 'charity_category_id' => 10, 'nation_id' => 2]);
        $edu_high = ServiceOption::create(['name_ar' => 'المرحلة الثانوية', 'name_en' => 'High Stage', 'charity_category_id' => 10, 'nation_id' => 2]);

        // Educational features
        ServiceOption::create(['name_ar' => 'زى مدرسى', 'name_en' => 'School Uniform', 'parent_id' => $edu_primary->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'توفير حقيبة مدرسية', 'name_en' => 'Provide a school bag', 'parent_id' => $edu_primary->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'وجبات غذائية', 'name_en' => 'Lesson fee', 'parent_id' => $edu_primary->id, 'nation_id' => 2]);

        ServiceOption::create(['name_ar' => 'زى مدرسى', 'name_en' => 'School Uniform', 'parent_id' => $edu_secondary->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'توفير حقيبة مدرسية', 'name_en' => 'Provide a school bag', 'parent_id' => $edu_secondary->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'وجبات غذائية', 'name_en' => 'Lesson fee', 'parent_id' => $edu_secondary->id, 'nation_id' => 2]);

        ServiceOption::create(['name_ar' => 'زى مدرسى', 'name_en' => 'School Uniform', 'parent_id' => $edu_high->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'توفير حقيبة مدرسية', 'name_en' => 'Provide a school bag', 'parent_id' => $edu_high->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'وجبات غذائية', 'name_en' => 'Lesson fee', 'parent_id' => $edu_high->id, 'nation_id' => 2]);

        // Orphan
        $orphane_1 = ServiceOption::create(['name_ar' => 'كفالة', 'name_en' => 'Guarantee', 'charity_category_id' => 12, 'nation_id' => 2]);
        $orphane_2 = ServiceOption::create(['name_ar' => 'كسوة', 'name_en' => 'Livery', 'charity_category_id' => 12, 'nation_id' => 2]);
        $orphane_3 = ServiceOption::create(['name_ar' => 'عيدية', 'name_en' => 'Eid', 'charity_category_id' => 12, 'nation_id' => 2]);

        // Orphan Features
        ServiceOption::create(['name_ar' => 'سنوية', 'name_en' => 'Annual', 'parent_id' => $orphane_1->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'سنوية', 'name_en' => 'Annual', 'parent_id' => $orphane_2->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'سنوية', 'name_en' => 'Annual', 'parent_id' => $orphane_3->id, 'nation_id' => 2]);

        ServiceOption::create(['name_ar' => 'مقطوعة', 'name_en' => 'Discontinued', 'parent_id' => $orphane_1->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'مقطوعة', 'name_en' => 'Discontinued', 'parent_id' => $orphane_2->id, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'مقطوعة', 'name_en' => 'Discontinued', 'parent_id' => $orphane_3->id, 'nation_id' => 2]);

        // Emergency
        ServiceOption::create(['name_ar' => 'توفير سكن', 'name_en' => 'Provide housing', 'charity_category_id' => 13, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'إيواء', 'name_en' => 'Accommodation', 'charity_category_id' => 13, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'مواد غذائية', 'name_en' => 'Foodstuffs', 'charity_category_id' => 13, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'توفير خيام', 'name_en' => 'Provide tents', 'charity_category_id' => 13, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'توفير بطانيات', 'name_en' => 'Provide blankets', 'charity_category_id' => 13, 'nation_id' => 2]);
        ServiceOption::create(['name_ar' => 'توفير تدفئة', 'name_en' => 'Save heating', 'charity_category_id' => 13, 'nation_id' => 2]);

        // Stable
        ServiceOption::create(['name_ar' => 'دين', 'name_en' => 'Loan', 'charity_category_id' => 14, 'nation_id' => 2]);     
        ServiceOption::create(['name_ar' => 'قرض حسن', 'name_en' => 'Good loan', 'charity_category_id' => 14, 'nation_id' => 2]);     
        ServiceOption::create(['name_ar' => 'تبرع', 'name_en' => 'Donation', 'charity_category_id' => 14, 'nation_id' => 2]);     
        
    }
}
