<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
        $this->call(CountrySeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(DegreeTableSeeder::class);
        // $this->call(BankSeeder::class);
        $this->call(RatingCriteriaTableSeeder::class);
        $this->call(CarityCategoryTableSeeder::class);
        $this->call(PermissionsSeeder::class);
        $this->call(AgreementSeeder::class);
        $this->call(SettingUKSeeder::class);
        $this->call(SettingSASeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CurrencySeeder::class);
        // $this->call(CharityTableSeeder::class);
        // $this->call(CharityCharityCategoryTableSeeder::class);
        $this->call(StaticPageSeeder::class);
        // $this->call(ProjectSeeder::class);
        $this->call(WorkFieldSeeder::class);
        // $this->call(PhaseSeeder::class);
        // $this->call(ReportSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(ServiceOptionSeeder::class);
        
    }
}
