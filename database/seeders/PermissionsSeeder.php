<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionsSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // users roles
        $admin = Role::create(['name' => 'admin' ,  'type' => 'role' ]);
        $supporter = Role::create(['name' => 'supporter' ,  'type' => 'role']);
        $employee_role = Role::create(['name' => 'employee' ,  'type' => 'role']);
        $charity = Role::create([ 'name' => 'charity' ,  'type' => 'role']);

        // Charity roles
        $charity_employee_role = Role::create(['name' => 'charity_employee', 'type' => 'role']);
        // $charity_projects_management = Role::create(['name' => 'charity_projects_management', 'type' => 'charity']);
        // $charity_phases_management = Role::create(['name' => 'charity_phases_management', 'type' => 'charity']);
        // $charity_employees_management = Role::create(['name' => 'charity_employees_management', 'type' => 'charity']);
        // $charity_sponsers_management = Role::create(['name' => 'charity_sponsers_management', 'type' => 'charity']);
        // $charity_exchange_bonds_management = Role::create(['name' => 'charity_exchange_bonds_management', 'type' => 'charity']);
        // $charity_job_categories_management = Role::create(['name' => 'charity_job_categories_management', 'type' => 'charity']);
        // $charity_jobs_management = Role::create(['name' => 'charity_jobs_management', 'type' => 'charity']);
        // $charity_degrees_management = Role::create(['name' => 'charity_degrees_management', 'type' => 'charity']);
        // $charity_nationalities_management = Role::create(['name' => 'charity_nationalities_management', 'type' => 'charity']);
        // $charity_reports_management = Role::create(['name' => 'charity_reports_management', 'type' => 'charity']);


        $question_role = Role::create(['name' => 'question' ,  'type' => 'role']);
        $question_permission = [
            ['name' => 'create_question', 'guard_name'=> 'web'],
            ['name' => 'edit_question', 'guard_name'=> 'web'],
            ['name' => 'delete_question', 'guard_name'=> 'web'], 
            ['name' => 'show_question', 'guard_name'=> 'web'], 
        ];
        Permission::insert($question_permission);
        $question_role->givePermissionTo(['create_question', 'edit_question', 'delete_question', 'show_question']);


        // compaign
        $campaign = Role::create(['name' => 'campaign_goal' ,  'type' => 'role']);
        $campaign_permission = [
            ['name' => 'list_campaign_goal', 'guard_name'=> 'web'],
            ['name' => 'create_campaign_goal', 'guard_name'=> 'web'],
            ['name' => 'edit_campaign_goal', 'guard_name'=> 'web'],
            ['name' => 'delete_campaign_goal', 'guard_name'=> 'web'], 
            ['name' => 'show_campaign_goal', 'guard_name'=> 'web'], 
        ];
        Permission::insert($campaign_permission);
        $campaign->givePermissionTo(['create_campaign_goal', 'edit_campaign_goal', 'delete_campaign_goal', 'show_campaign_goal']);


        // ambassador
        $ambassador = Role::create(['name' => 'ambassador' ,  'type' => 'role']);
        $ambassador_permission = [
            ['name' => 'list_ambassadors', 'guard_name'=> 'web'],
            ['name' => 'show_ambassadors', 'guard_name'=> 'web'], 
        ];
        Permission::insert($ambassador_permission);
        $ambassador->givePermissionTo(['list_ambassadors', 'show_ambassadors']);


        // rating
        $rating_role = Role::create(['name' => 'rating']);
        $rating_role_permission = [
            ['name' => 'show_ratingcriteria', 'guard_name'=> 'web'],
            ['name' => 'edit_ratingcriteria', 'guard_name'=> 'web'],
        ];
        Permission::insert($rating_role_permission);
        $rating_role->givePermissionTo(['show_ratingcriteria', 'edit_ratingcriteria']);

        
        // currency
        $currency_role = Role::create(['name' => 'currency']);
        $currency_permission = [
            ['name' => 'create_currency', 'guard_name'=> 'web'],
            ['name' => 'edit_currency', 'guard_name'=> 'web'],
            ['name' => 'delete_currency', 'guard_name'=> 'web'], 
            ['name' => 'show_currency', 'guard_name'=> 'web'], 
        ];
        Permission::insert($currency_permission);
        $currency_role->givePermissionTo(['create_currency', 'edit_currency', 'delete_currency', 'show_currency']);


        // charity_management
        $charity_management = Role::create(['name' => 'charity_management']);
        $charity_management_per = [
                ['name' => 'list_charities', 'guard_name'=> 'web'],
                ['name' => 'show_charity', 'guard_name'=> 'web'],
                ['name' => 'accept_refuse_charities', 'guard_name'=> 'web'], 
                ['name' => 'create_charity', 'guard_name'=> 'web'], 
                ['name' => 'edit_charity', 'guard_name'=> 'web'], 
                ['name' => 'delete_charity', 'guard_name'=> 'web']
            ];
        Permission::insert($charity_management_per);
        $charity_management->givePermissionTo(['list_charities', 'show_charity', 'accept_refuse_charities', 'create_charity', 'edit_charity', 'delete_charity']);


        // emp_management
        $emp_management = Role::create(['name' => 'emp_management']);
        $emp_management_per = [
                ['name' => 'list_emp', 'guard_name'=> 'web'], 
                ['name' => 'create_emp', 'guard_name'=> 'web'], 
                ['name' => 'edit_emp', 'guard_name'=> 'web'], 
                ['name' => 'delete_emp', 'guard_name'=> 'web'], 
                ['name' => 'show_emp', 'guard_name'=> 'web'], 
                ['name' => 'assign_remove_job_to_emp', 'guard_name'=> 'web']
        ];
        Permission::insert($emp_management_per);
        $emp_management->givePermissionTo(['list_emp', 'create_emp', 'edit_emp', 'delete_emp', 'show_emp', 'assign_remove_job_to_emp']);


        // project_management
        $project_management = Role::create(['name' => 'project_management']);
        $project_management_per = [
                ['name' => 'list_projects', 'guard_name'=> 'web'], 
                ['name' => 'show_project', 'guard_name'=> 'web'], 
                ['name' => 'accept_refuse_projects', 'guard_name'=> 'web'], 
                ['name' => 'delete_project', 'guard_name'=> 'web'], 
                ['name' => 'restore_project', 'guard_name'=> 'web'], 
                ['name' => 'edit_project', 'guard_name'=> 'web'],
                ['name' => 'create_project', 'guard_name'=> 'web'], 
            ];
        Permission::insert($project_management_per);
        $project_management->givePermissionTo(['list_projects', 'show_project', 'accept_refuse_projects', 'delete_project', 'restore_project', 'edit_project', 'create_project']);


        // report_management
        $report_management = Role::create(['name' => 'report_management']);
        $report_management_per = [
                ['name' => 'list_reports', 'guard_name'=> 'web'], 
                ['name' => 'show_report', 'guard_name'=> 'web'], 
                ['name' => 'accept_refuse_reports', 'guard_name'=> 'web'], 
                ['name' => 'edit_report', 'guard_name'=> 'web'], 
                ['name' => 'create_report', 'guard_name'=> 'web'], 
                ['name' => 'delete_report', 'guard_name'=> 'web']
            ];
        Permission::insert($report_management_per);
        $report_management->givePermissionTo(['list_reports', 'show_report', 'accept_refuse_reports', 'edit_report', 'delete_report']);


        // // supporters_management
        $supporters_management = Role::create(['name' => 'supporters_management']);
        $supporters_management_per = [
                ['name' => 'list_supporters', 'guard_name'=> 'web'], 
                ['name' => 'show_supporter', 'guard_name'=> 'web'], 
                ['name' => 'block_unblock_supporter', 'guard_name'=> 'web']
            ];
        Permission::insert($supporters_management_per);
        $supporters_management->givePermissionTo(['list_supporters', 'show_supporter', 'block_unblock_supporter']);


        // // finance_management
        $finance_management = Role::create(['name' => 'finance_management']);
        $finance_management_per = [
                // طلبات الصرف و القبض
                ['name' => 'list_exchange_requests', 'guard_name'=> 'web'], 
                // ['name' => 'list_catch_requests', 'guard_name'=> 'web'], 
                ['name' => 'create_finance_request', 'guard_name'=> 'web'], 
                ['name' => 'edit_finance_request', 'guard_name'=> 'web'], 
                ['name' => 'show_finance_request', 'guard_name'=> 'web'], 
                ['name' => 'delete_finance_request', 'guard_name'=> 'web'], 
                ['name' => 'accept_refuse_finance_requests', 'guard_name'=> 'web'], 

                // سندات الصرف و القبض

                // VIP supports
                ['name' => 'list_exchange_vouchers', 'guard_name'=> 'web'], 
                // ['name' => 'list_catch_vouchers', 'guard_name'=> 'web'], 
                ['name' => 'create_finance_voucher', 'guard_name'=> 'web'], 
                ['name' => 'edit_finance_voucher', 'guard_name'=> 'web'], 
                ['name' => 'show_finance_voucher', 'guard_name'=> 'web'], 
                ['name' => 'delete_finance_voucher', 'guard_name'=> 'web'], 
                ['name' => 'accept_refuse_finance_vouchers', 'guard_name'=> 'web'], 

            ];
        Permission::insert($finance_management_per);
        $finance_management->givePermissionTo([
            // طلبات الصرف و القبض
            'list_exchange_requests',
            // 'list_catch_requests',
            'create_finance_request',
            'edit_finance_request',
            'show_finance_request',
            'delete_finance_request',
            'accept_refuse_finance_requests',

            // سندات الصرف و القبض
            'list_exchange_vouchers',
            // 'list_catch_vouchers',
            'create_finance_voucher',
            'edit_finance_voucher',
            'show_finance_voucher',
            'delete_finance_voucher',
            'accept_refuse_finance_vouchers',
        ]);


        // // cities_management
        $cities_management = Role::create(['name' => 'cities_management']);
        $cities_management_arr = [
                ['name' => 'list_cities', 'guard_name'=> 'web'], 
                ['name' => 'create_city', 'guard_name'=> 'web'], 
                ['name' => 'edit_city', 'guard_name'=> 'web'], 
                ['name' => 'delete_city', 'guard_name'=> 'web'], 
                ['name' => 'show_city', 'guard_name'=> 'web']
            ];
        Permission::insert($cities_management_arr);
        $cities_management->givePermissionTo(['list_cities', 'create_city', 'edit_city', 'delete_city', 'show_city']);

        // //job_cats
        $job_cats = Role::create(['name' => 'job_cats']);
        $job_cats_arr = [
                ['name' => 'list_job_cats', 'guard_name'=> 'web'], 
                ['name' => 'create_job_cats', 'guard_name'=> 'web'], 
                ['name' => 'edit_job_cats', 'guard_name'=> 'web'], 
                ['name' => 'delete_job_cats', 'guard_name'=> 'web'], 
                ['name' => 'show_job_cats', 'guard_name'=> 'web']
            ];
        Permission::insert($job_cats_arr);
        $job_cats->givePermissionTo(['list_job_cats', 'create_job_cats', 'edit_job_cats', 'delete_job_cats', 'show_job_cats']);

        // // work_fields
        // $work_fields = Role::create(['name' => 'work_fields']);
        // $work_fields_per = [
        //         ['name' => 'list_work_fields', 'guard_name'=> 'web'], 
        //         ['name' => 'create_work_fields', 'guard_name'=> 'web'], 
        //         ['name' => 'edit_work_fields', 'guard_name'=> 'web'], 
        //         ['name' => 'delete_work_fields', 'guard_name'=> 'web'], 
        //         ['name' => 'show_work_fields', 'guard_name'=> 'web']
        //     ];
        // Permission::insert($work_fields_per);
        // $work_fields->givePermissionTo(['list_work_fields', 'create_work_fields', 'edit_work_fields', 'delete_work_fields', 'show_work_fields']);


        // //charity_categories
        $charity_categories = Role::create(['name' => 'charity_categories']);
        $charity_categories_per = [
                ['name' => 'list_charity_cats', 'guard_name'=> 'web'], 
                ['name' => 'create_charity_cats', 'guard_name'=> 'web'], 
                ['name' => 'edit_charity_cats', 'guard_name'=> 'web'], 
                ['name' => 'delete_charity_cats', 'guard_name'=> 'web'], 
                ['name' => 'show_charity_cats', 'guard_name'=> 'web']
            ];
        Permission::insert($charity_categories_per);
        $charity_categories->givePermissionTo(['list_charity_cats', 'create_charity_cats', 'edit_charity_cats', 'delete_charity_cats', 'show_charity_cats']);


        // // contacts_management
        $contacts_management = Role::create(['name' => 'contacts_management']);
        $contacts_management_per = [
                ['name' => 'list_contacts', 'guard_name'=> 'web'], 
                ['name' => 'delete_contacts', 'guard_name'=> 'web']
            ];
        Permission::insert($contacts_management_per);
        $contacts_management->givePermissionTo(['list_contacts', 'delete_contacts']);


        // // static_pages_management
        $static_pages_management = Role::create(['name' => 'static_pages_management']);
        $static_pages_management_per = [
                ['name' => 'edit_static_pages', 'guard_name'=> 'web']
            ];
        Permission::insert($static_pages_management_per);
        $static_pages_management->givePermissionTo(['edit_static_pages']);


        // // slider_management
        $slider_management = Role::create(['name' => 'slider_management']);
        $slider_management_per = [
            ['name' => 'create_sliders', 'guard_name'=> 'web'], 
            ['name' => 'edit_sliders', 'guard_name'=> 'web'], 
            ['name' => 'delete_sliders', 'guard_name'=> 'web']
        ];
        Permission::insert($slider_management_per);
        $slider_management->givePermissionTo(['create_sliders', 'edit_sliders', 'delete_sliders']);

        // // charity_edit_requests
        $charity_edit_requests = Role::create(['name' => 'charity_edit_requests']);
        $charity_edit_requests_per = [
                ['name' => 'list_charity_edit_requests', 'guard_name'=> 'web'], 
                ['name' => 'delete_charity_edit_requests', 'guard_name'=> 'web'], 
                ['name' => 'accept_refuse_charity_edit_requests', 'guard_name'=> 'web']
            ];
        Permission::insert($charity_edit_requests_per);
        $charity_edit_requests->givePermissionTo(['list_charity_edit_requests', 'accept_refuse_charity_edit_requests', 'delete_charity_edit_requests']);


        // //project_edit_requests
        $project_edit_requests = Role::create(['name' => 'project_edit_requests']);
        $project_edit_requests_per = [
                ['name' => 'list_project_edit_requests', 'guard_name'=> 'web'], 
                ['name' => 'delete_project_edit_requests', 'guard_name'=> 'web'], 
                ['name' => 'accept_refuse_project_edit_requests', 'guard_name'=> 'web']
            ];
        Permission::insert($project_edit_requests_per);
        $project_edit_requests->givePermissionTo(['list_project_edit_requests', 'accept_refuse_project_edit_requests', 'delete_project_edit_requests']);


        // // settings
        $settings = Role::create(['name' => 'settings']);
        $settings_per = [
                ['name' => 'edit_setting', 'guard_name'=> 'web']
            ];
        Permission::insert($settings_per);
        $settings->givePermissionTo(['edit_setting']);


        // // agreements
        $agreements = Role::create(['name' => 'agreements']);
        $agreements_per = [
                ['name' => 'edit_agreement', 'guard_name'=> 'web']
            ];
        Permission::insert($agreements_per);
        $agreements->givePermissionTo(['edit_agreement']);

        // contracts
        $contracts = Role::create(['name' => 'contracts']);
        $contracts_per = [
                ['name' => 'edit_contract', 'guard_name'=> 'web'], 
                ['name' => 'delete_contract', 'guard_name'=> 'web'], 
                ['name' => 'add_contract', 'guard_name'=> 'web'], 
                ['name' => 'show_contract', 'guard_name'=> 'web']
            ];
        Permission::insert($contracts_per);
        $contracts->givePermissionTo(['edit_contract', 'delete_contract', 'add_contract', 'show_contract']);

        // services
        $services = Role::create(['name' => 'services']);
        $services_per = [
                ['name' => 'edit_service', 'guard_name'=> 'web'], 
                ['name' => 'add_service', 'guard_name'=> 'web'], 
                ['name' => 'delete_service', 'guard_name'=> 'web'], 
                ['name' => 'show_service', 'guard_name'=> 'web']
            ];
        Permission::insert($services_per);
        $services->givePermissionTo(['edit_service', 'add_service', 'delete_service', 'show_service']);


        // jobs
        $jobs = Role::create(['name' => 'jobs']);
        $jobs_per = [
                ['name' => 'list_jobs', 'guard_name'=> 'web'], 
                ['name' => 'create_job', 'guard_name'=> 'web'], 
                ['name' => 'edit_job', 'guard_name'=> 'web'], 
                ['name' => 'show_job', 'guard_name'=> 'web'], 
                ['name' => 'delete_job', 'guard_name'=> 'web']
            ];
        Permission::insert($jobs_per);
        $jobs->givePermissionTo(['list_jobs', 'create_job', 'edit_job', 'show_job', 'delete_job']);


        // parteners
        $parteners = Role::create(['name' => 'parteners']);
        $parteners_per = [
                ['name' => 'list_parteners', 'guard_name'=> 'web'], 
                ['name' => 'create_partener', 'guard_name'=> 'web'], 
                ['name' => 'edit_partener', 'guard_name'=> 'web'], 
                ['name' => 'show_partener', 'guard_name'=> 'web'], 
                ['name' => 'delete_partener', 'guard_name'=> 'web']
            ];
        Permission::insert($parteners_per);
        $parteners->givePermissionTo(['list_parteners', 'create_partener', 'edit_partener', 'show_partener', 'delete_partener']);


        // marketings
        $marketings = Role::create(['name' => 'marketings']);
        $marketings_per = [
                ['name' => 'list_marketings', 'guard_name'=> 'web'], 
                ['name' => 'create_marketings', 'guard_name'=> 'web'], 
                ['name' => 'edit_marketings', 'guard_name'=> 'web'], 
                ['name' => 'show_marketings', 'guard_name'=> 'web'], 
                ['name' => 'delete_marketings', 'guard_name'=> 'web']
            ];
        Permission::insert($marketings_per);
        $marketings->givePermissionTo(['list_marketings', 'create_marketings', 'edit_marketings', 'show_marketings', 'delete_marketings']);

        // degrees_management
        $degrees_management = Role::create(['name' => 'degrees_management']);
        $degrees_management_per = [
            ['name' => 'list_degrees', 'guard_name'=> 'web'], 
            ['name' => 'create_degrees', 'guard_name'=> 'web'], 
            ['name' => 'edit_degrees', 'guard_name'=> 'web'], 
            ['name' => 'show_degrees', 'guard_name'=> 'web'], 
            ['name' => 'delete_degrees', 'guard_name'=> 'web']
        ];
        Permission::insert($degrees_management_per);
        $degrees_management->givePermissionTo(['list_degrees', 'create_degrees', 'edit_degrees', 'show_degrees', 'delete_degrees']);

        // supports
        $supports = Role::create(['name' => 'supports']);
        $supports_per = [
            ['name' => 'list_supports', 'guard_name' => 'web'],
            ['name' => 'list_edit_suspended_supports', 'guard_name' => 'web']
        ];
        Permission::insert($supports_per);
        $supports->givePermissionTo('list_supports', 'list_edit_suspended_supports');


        // ratings
        $ratings = Role::create(['name' => 'ratings']);
        $ratings_per = [
            ['name' => 'list_ratings', 'guard_name' => 'web'],
            ['name' => 'delete_comment', 'guard_name' => 'web'],
        ];
        Permission::insert($ratings_per);
        $ratings->givePermissionTo('list_ratings', 'delete_comment');
        
        // Banks
        // $banks_management = Role::create(['name' => 'banks_management']);
        // $banks_management_per = [
        //     ['name' => 'list_banks', 'guard_name'=> 'web'], 
        //     ['name' => 'create_banks', 'guard_name'=> 'web'], 
        //     ['name' => 'edit_banks', 'guard_name'=> 'web'], 
        //     ['name' => 'show_banks', 'guard_name'=> 'web'], 
        //     ['name' => 'delete_banks', 'guard_name'=> 'web']
        // ];
        // Permission::insert($banks_management_per);
        // $banks_management->givePermissionTo(['list_banks', 'create_banks', 'edit_banks', 'show_banks', 'delete_banks']);

        // Nationalities
        $nationalities_management = Role::create(['name' => 'nationalities_management']);
        $nationalities_management_per = [
            ['name' => 'list_nationalities', 'guard_name'=> 'web'], 
            ['name' => 'create_nationalities', 'guard_name'=> 'web'], 
            ['name' => 'edit_nationalities', 'guard_name'=> 'web'], 
            ['name' => 'show_nationalities', 'guard_name'=> 'web'], 
            ['name' => 'delete_nationalities', 'guard_name'=> 'web']
        ];
        Permission::insert($nationalities_management_per);
        $nationalities_management->givePermissionTo(['list_nationalities', 'create_nationalities', 'edit_nationalities', 'show_nationalities', 'delete_nationalities']);

        // assign all permissions to admin
        $admin->givePermissionTo(Permission::all());

    }
}
