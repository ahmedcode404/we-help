<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DegreeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('degrees')->insert([
            
            // SA
            [
                'slug' => 'Bachelor',
                'name_ar' => 'بكالوريوس',
                'name_en' => 'Bachelor',
                'nation_id' => 1
            ],
            [
                'slug' => 'BA',
                'name_ar' => 'ليسانس',
                'name_en' => 'BA',
                'nation_id' => 1
            ],
            [
                'slug' => 'diploma',
                'name_ar' => 'دبلومه',
                'name_en' => 'diploma',
                'nation_id' => 1,
            ],

            //UK
            [
                'slug' => 'Bachelor-',
                'name_ar' => 'بكالوريوس',
                'name_en' => 'Bachelor',
                'nation_id' => 2
            ],
            [
                'slug' => 'BA-',
                'name_ar' => 'ليسانس',
                'name_en' => 'BA',
                'nation_id' => 2
            ],
            [
                'slug' => 'diploma-',
                'name_ar' => 'دبلومه',
                'name_en' => 'diploma',
                'nation_id' => 12
            ],

        ]);
    }
}
