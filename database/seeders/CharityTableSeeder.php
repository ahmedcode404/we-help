<?php

namespace Database\Seeders;

use App\Models\Charity;
use Illuminate\Database\Seeder;
use DB;
use Spatie\Permission\Models\Permission;

class CharityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $charity = Charity::create([

                'type'                => 'charity',
                'name'                => 'Resala',
                'email'               => 'Resala@gmail.com',
                'password'            => bcrypt('123456'),
                'phone'               => '+966512345698',
                'notes'               => 'Resala',
                // 'city_id'             => 1,
                // 'country_id'          => 1,
                'city'                => 'Jeddah',
                'country'             => 'Saudi Arabia',
                'establish_date'      => date('Y-m-d H:i:s'),
                'license_number'      => '215466',
                'license_file'        => 'charity/avatar.png', // image
                'address_ar'          => 'الرياض',
                'address_en'          => 'Ryadh',
                'representer_name_ar' => 'محمد على',
                'representer_name_en' => 'Mohamed Ali',
                'representer_title_ar'   => 'عنوان الممثل', // pdf
                'representer_title_en'   => 'title representer', // pdf
                'representer_title_file' => 'charity/sample.pdf', // pdf
                'representer_email'   => 'representer@gmail.com', // pdf
                'strategy_ar'         => 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق "ليتراسيت" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل "ألدوس بايج مايكر" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.',
                'strategy_en'         => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                'logo'                => 'user/avatar.png',
                'twitter_link'        => 'https://www.twitter.com',
                'facebook_link'       => 'https://www.facebook.com',
                'nation_id'           => 1,
                'license_start_date'  => date('Y-m-d H:i:s'),
                'license_end_date'    => date('Y-m-d H:i:s'),
                'branches_num'        => 1,
                'members_num'         => 30,
                'contract_date'       => date('Y-m-d H:i:s'),
                'attach'              => 'charity/sample.pdf', // pdf
                'internal_image'      => 'charity/sample.pdf', // pdf
                'year_report'         => 'charity/sample.pdf', // pdf
                'years'               => 'two',
                'mobile'                     => +966548732147,
                'representer_passport_image' => 'user/avatar.png', 
                'representer_nation_image'   => 'user/avatar.png', 
                'international_name_ar'      => 'مؤسسة الصحة العالمية',
                'international_name_en'      => 'World Health Foundation',
                'website'                    => 'https://www.web.com',
                'iban'                       => 123456,
                'bank_id'                  => 1,
                'swift_code'                 => 65468,
                // 'contract_after_sign'        => 'contract_after_sign.pdf', // pdf
                // 'signature'                  => 'signature.png',
                'status'                     => 'hold',
                'currency_id'                => 1,

        ]);

        $charity2 = Charity::create([

            'type'                => 'charity',
            'name'                => 'Resala',
            'email'               => 'Resala2@gmail.com',
            'password'            => bcrypt('123456'),
            'phone'               => '+44792345698',
            'notes'               => 'Resala2',
            'city'                => 'Meka',
            'country'             => 'Suadi Arabia',
            // 'city_id'             => 1,
            // 'country_id'          => 2,
            'establish_date'      => date('Y-m-d H:i:s'),
            'license_number'      => '215466',
            'license_file'        => 'charity/avatar.png', // image
            'address_ar'          => 'الرياض',
            'address_en'          => 'Ryadh',
            'representer_name_ar' => 'محمد على',
            'representer_name_en' => 'Mohamed Ali',
            'representer_title_ar'   => 'عنوان الممثل', // pdf
            'representer_title_en'   => 'title representer', // pdf
            'representer_title_file' => 'charity/sample.pdf', // pdf
            'representer_email'   => 'representer@gmail.com', // pdf
            'strategy_ar'         => 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق "ليتراسيت" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل "ألدوس بايج مايكر" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.',
            'strategy_en'         => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            'logo'                => 'user/avatar.png',
            'twitter_link'        => 'https://www.twitter.com',
            'facebook_link'       => 'https://www.facebook.com',
            'nation_id'           => 2,
            'license_start_date'  => date('Y-m-d H:i:s'),
            'license_end_date'    => date('Y-m-d H:i:s'),
            'branches_num'        => 1,
            'members_num'         => 30,
            'contract_date'       => date('Y-m-d H:i:s'),
            'attach'              => 'charity/sample.pdf', // pdf
            'internal_image'      => 'charity/sample.pdf', // pdf
            'year_report'         => 'charity/sample.pdf', // pdf
            'years'               => 'two',
            'mobile'                     => +966548732147,
            'representer_passport_image' => 'user/avatar.png', 
            'representer_nation_image'   => 'user/avatar.png', 
            'international_name_ar'      => 'مؤسسة الصحة العالمية',
            'international_name_en'      => 'World Health Foundation',
            'website'                    => 'https://www.web.com',
            'iban'                       => 123456,
            'bank_id'                  => 1,
            'swift_code'                 => 65468,
            // 'contract_after_sign'        => 'contract_after_sign.pdf', // pdf
            // 'signature'                  => 'signature.png',
            'status'                     => 'hold',
            'currency_id'                => 2,

    ]);

        Permission::create(['name' => 'edit_charity_' . $charity->id]);
        $charity->assignRole('charity');
   
    }
}


