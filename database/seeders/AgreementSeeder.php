<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class AgreementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('agreements')->insert([

            // هذه الوثيقة لابد أن يوافق عليها الداعم قبل الدخول على عملية السداد أو الدفع أو الشراء - START
            // النسخه السعودى
            [
                'key' => 'supporter_payment_rules_sa_ar',
                'content' =>       '1- يخضع كل مستخدم لمنصة we help لبنود وشروط الاستخدام العامة، ويُعد إقرارك على هذه الشروط العامة موافقة دون أي قيد أو شرط على بنود وشروط الاستخدام، سواء أكنت مستخدماً مسجلاً أم لم تكن، وتسري هذه الموافقة اعتباراً من تاريخ أول استخدام لك لهذه المنصة.
                                    2- منصة We help تعمل كوسيط مقابل عمولة من المشاريع التي بين المؤسسات الإنسانية والراغبين بدعم مشاريعها، وتقدم عدة خدمات منها: الرقابة على تنفيذ المشاريع، والتسويق الرقمي، وتقدم هذه الخدمات مقابل نسبة أو عموله من مبلغ الدعم . 
                                    3- يقر الداعم بأن أي معلومات أو بيانات يقوم بتسجيلها أو إضافتها هي تحت مسؤوليته، وللمنصة الحق بمطالبته بأي أضرار أو نتائج تنتج جراء عدم صحة المعلومات.
                                    4- تنحصر مسؤولية المنصة في الوساطة باستلام قيمة المشروع، ودفعه للمؤسسة المنفذة والرقابة على تنفيذ المشاريع وتزويد الداعم بالتقارير والتحديثات حول المشروع وبذلك لا تكون مسؤولة عن أي أخطاء أو تقصير يقوم به الداعم أو الجمعية خارج النطاق المحدد آنفاً.
                                    5- جميع المعلومات في هذا الموقع محمية بحقوق النشر ولا يجوز إعادة انتاجها أو ملكيتها بأي صورة دون موافقة إدارة المنصة، ويعتبر أي تعديل على المواد أو استخدامها لأغراض أخرى انتهاكاً لحقوق الملكية والنشر.
                                    6- يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والقوانين الخاصة باستخدام بالمنصة.
                                    7- تتيح المنصة الكشف عن المبلغ المستحق وآلية سداد المشروع، وفي حالة الاتفاق على السداد المجزأ يتعهد الداعم بأن يقوم بسداد المبلغ المتبقي في الوقت المحدد للسداد.
                                    8- تقدم المنصة خدمة اختيار المشروع، ويحق له استرداد مبلغ الدعم خلال (48) ساعه من تاريخ الدعم، وفي حالة تم طلب الاسترداد بعد مضي (48) ساعه فإنه يمكن إعادة المبلغ بعد خصم النسبة المخصصة للمنصة خلال 7 أيام من تاريخ طلب الاسترداد . كما أنه لا يحق للداعم الرجوع عن الدعم بعد مضي 7 أيام من تاريخ الدعم.
                                    9- الخدمات الإلكترونية التي تقدمها المنصة على الإنترنت، تقدم فقط لتسهيل الإجراءات اليدوية؛ وبهذا تقر بعلمك الكامل بأن الاتصالات عبر شبكة الإنترنت عرضة للأعطال أو للتدخل والاعتراض بواسطة الغير، فإن اللجوء إلى هذه المنصة يظل تحت مسؤوليتك الخاصة. 
                                    10- إن عدم كتابة البيانات والمعلومات الصحيحة للتواصل تفقد الداعم الحق بالحصول على أي تقرير أو تحديث أو مطالبات عن الخدمة والمنتج المدعوم. 
                                    11- يقر الداعم بموافقته للمنصة بتغيير موقع المشروع أو الخدمة في الحالات الطارئة أو الحروب أو الكوارث الطبيعية على حسب المبلغ المتوفر للدعم، أو في حالة الكفالة فإن الداعم يفوض المنصة بتغيير المكفول بالبديل الأنسب بما يتوافق مع مواصفات المكفول الأول.
                                    12- يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة المرعية وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والأحكام الخاصة باستخدام بالمنصة.
                                    13- الداعم والمنصة بالحفاظ على سرية المعلومات بموجب هذه الاتفاقية لأي جهة.
                                    14- يقر الداعم بأنه قد قرأ بنود هذه الاتفاقية وتفهمها فهماً نافياً للجهالة، واتفق على الالتزام ببنود هذه الاتفاقية والشروط والأحكام الواردة فيها.
                                    15- تخضع هذه الاتفاقية للأنظمة المعمول بها في المملكة المتحدة ويجري تفسيره وتنفيذه والفصل فيما ينشأ من دعاوى أو نزاعات وفقاً لهذه الأنظمة.
                                    16- بمجرد الضغط على موافق سيكون الداعم ملزماً بإتباع الشروط والأحكام.',
                'nation_id' => 1,
            ],

            [
                'key' => 'supporter_payment_rules_sa_en',
                'content' =>       '1- Each user of the we help platform is subject to the general terms and conditions of use, and your acknowledgment of these general terms is your agreement without any restriction or condition to the terms and conditions of use, whether you are a registered user or not, and this approval is effective from the date of your first use of this platform.
                                    2- The We Help platform works as an intermediary in exchange for a commission from projects between humanitarian institutions and those wishing to support their projects, and provides several services, including: monitoring project implementation, digital marketing, and provides these services in exchange for a percentage or commission from the amount of support.
                                    3- The supporter declares that any information or data that he registers or adds is under his responsibility, and the platform has the right to claim any damages or results that result from the incorrectness of the information.
                                    4- The responsibility of the platform is limited to mediation in receiving the value of the project, paying it to the implementing institution, monitoring the implementation of projects, and providing the supporter with reports and updates about the project, and thus it is not responsible for any errors or omissions made by the supporter or the association outside the aforementioned scope.
                                    5- All information on this site is protected by copyright, and it is not permissible to reproduce or own it in any way without the consent of the management of the platform, and any modification to the material or its use for other purposes is considered a violation of copyright and copyright.
                                    6- The supporter declares that he is aware, through the information shown on the platform, of the basic rules, regulations, purchase and support policies stipulated in relation to the basic characteristics of the service, its value, payment method, and terms of implementation, and he acknowledges that he has read the terms and laws for using the platform.
                                    7- The platform allows the disclosure of the amount due and the mechanism for paying the project, and in the event of an agreement on a split payment, the supporter undertakes to pay the remaining amount at the specified time for payment.
                                    8- The platform provides the project selection service, and he is entitled to refund the support amount within (48) hours from the date of support, and in the event that the refund is requested after the lapse of (48) hours, the amount can be returned after deducting the percentage allocated to the platform within 7 days from the date of the refund request. Also, the supporter has no right to withdraw from the support after 7 days have passed from the date of support.
                                    9- Failure to write the correct data and information for communication loses the supporter the right to obtain any report, update or claims about the service and the supported product.
                                    10- The supporter acknowledges his agreement to the platform to change the project or service site in emergency situations, wars or natural disasters, depending on the amount available for support, or in the case of sponsorship, the supporter authorizes the platform to change the sponsored person with the most appropriate alternative in accordance with the specifications of the first sponsored.
                                    11- The supporter declares that he is aware, through the information shown on the platform, of the basic rules and regulations in force and the purchase and support policies stipulated in relation to the basic characteristics of the service, its value, payment method, and terms of implementation. He also acknowledges that he has read the terms and conditions for using the platform.
                                    12- The supporter and platform to maintain the confidentiality of information under this agreement for any party.
                                    13- The supporter declares that he has read the terms of this agreement and understands them with a negative understanding of ignorance, and has agreed to abide by the terms of this agreement and the terms and conditions contained therein.
                                    14- This agreement is subject to the regulations in force in the United Kingdom, and it shall be interpreted, implemented, and adjudicated in relation to claims or disputes arising in accordance with these regulations.
                                    15- Once you click on I agree, the supporter will be obligated to follow the terms and conditions.
                                    16- The online services provided by the platform on the Internet, provided only to facilitate manual procedures; Thus you acknowledge with your full knowledge that communications over the Internet are subject to interruptions or interference and interception by others, so resorting to this platform remains at your own risk.',
                'nation_id' => 1,
            ],


                        // هذه الوثيقة لابد أن يوافق عليها الداعم قبل الدخول على عملية السداد أو الدفع أو الشراء - END

            //------------------------------------------------------------------------------------------------------

            // شروط التسجيل فى المنصه للجمعيه - START
            // النسخه السعودى
          

            [
                'key' => 'charity_register_rules_sa_en',
                'content' =>       '1- Charity must have an official license from the competent authority in her country to conduct its activity.
                                    2- To have an elected bylaw and an elected board of directors whose members are not less than five persons, and an executive department with specific persons and positions.
                                    3- It must have a disciplined financial system according to professional principles and a bank account registered in its name and as an association (non-profit)
                                    4- To have a tangible and real activity and clear work plans
                                    5- It must have a minimum amount of funding in running its main business.
                                    6- At least 5 years or 3 years have passed since its establishment
                                    7- It must have a specific location or address and can be reached
                                    8- Not to be included in a considered classification list.',
                'nation_id' => 1,
            ],

            [
                'key' => 'charity_register_rules_sa_ar',
                'content' => '1- أن تكون حاصلة على ترخيص رسمي من جهة الاختصاص في بلدها المزاولة نشاطها.
                                    2- ان يكون لها نظام داخلي ومجلس إدارة منتخب لا يقل عدد أعضائه عن خمسة اشخاص وإدارة تنفيذية محددة الأشخاص والمناصب.
                                    3- ان يكون لديها نظام مالي منضبط وفق الأصول المهنية وحساب بنكي مسجل باسمها وبصفتها جمعية ( غير ربحية )
                                    4- ان يكون لها نشاط ملموس وحقيقي وخطط عمل واضحة 
                                    5- ان يكون لديها حد أدنى لتمويل في تسير اعمالها الرئيسية.
                                    6- ان يكون قد مضى على انشائها ( 5 سنوات أو 3 سنوات)على الأقل
                                    7- ان يكون لها مقر أو عنوان محدد ويمكن الوصول اليها 
                                    8- الاتكون مدرجة في قائمة تصنيف معتبرة .',
                'nation_id' => 1,
            ],              

            // النسخه البريطانى
            [
                'key' => 'supporter_payment_rules_uk_ar',
                'content' =>       '1- يخضع كل مستخدم لمنصة we help لبنود وشروط الاستخدام العامة، ويُعد إقرارك على هذه الشروط العامة موافقة دون أي قيد أو شرط على بنود وشروط الاستخدام، سواء أكنت مستخدماً مسجلاً أم لم تكن، وتسري هذه الموافقة اعتباراً من تاريخ أول استخدام لك لهذه المنصة.
                                    2- منصة We help تعمل كوسيط مقابل عمولة من المشاريع التي بين المؤسسات الإنسانية والراغبين بدعم مشاريعها، وتقدم عدة خدمات منها: الرقابة على تنفيذ المشاريع، والتسويق الرقمي، وتقدم هذه الخدمات مقابل نسبة أو عموله من مبلغ الدعم . 
                                    3- يقر الداعم بأن أي معلومات أو بيانات يقوم بتسجيلها أو إضافتها هي تحت مسؤوليته، وللمنصة الحق بمطالبته بأي أضرار أو نتائج تنتج جراء عدم صحة المعلومات.
                                    4- تنحصر مسؤولية المنصة في الوساطة باستلام قيمة المشروع، ودفعه للمؤسسة المنفذة والرقابة على تنفيذ المشاريع وتزويد الداعم بالتقارير والتحديثات حول المشروع وبذلك لا تكون مسؤولة عن أي أخطاء أو تقصير يقوم به الداعم أو الجمعية خارج النطاق المحدد آنفاً.
                                    5- جميع المعلومات في هذا الموقع محمية بحقوق النشر ولا يجوز إعادة انتاجها أو ملكيتها بأي صورة دون موافقة إدارة المنصة، ويعتبر أي تعديل على المواد أو استخدامها لأغراض أخرى انتهاكاً لحقوق الملكية والنشر.
                                    6- يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والقوانين الخاصة باستخدام بالمنصة.
                                    7- تتيح المنصة الكشف عن المبلغ المستحق وآلية سداد المشروع، وفي حالة الاتفاق على السداد المجزأ يتعهد الداعم بأن يقوم بسداد المبلغ المتبقي في الوقت المحدد للسداد.
                                    8- تقدم المنصة خدمة اختيار المشروع، ويحق له استرداد مبلغ الدعم خلال (48) ساعه من تاريخ الدعم، وفي حالة تم طلب الاسترداد بعد مضي (48) ساعه فإنه يمكن إعادة المبلغ بعد خصم النسبة المخصصة للمنصة خلال 7 أيام من تاريخ طلب الاسترداد . كما أنه لا يحق للداعم الرجوع عن الدعم بعد مضي 7 أيام من تاريخ الدعم.
                                    9- الخدمات الإلكترونية التي تقدمها المنصة على الإنترنت، تقدم فقط لتسهيل الإجراءات اليدوية؛ وبهذا تقر بعلمك الكامل بأن الاتصالات عبر شبكة الإنترنت عرضة للأعطال أو للتدخل والاعتراض بواسطة الغير، فإن اللجوء إلى هذه المنصة يظل تحت مسؤوليتك الخاصة. 
                                    10- إن عدم كتابة البيانات والمعلومات الصحيحة للتواصل تفقد الداعم الحق بالحصول على أي تقرير أو تحديث أو مطالبات عن الخدمة والمنتج المدعوم. 
                                    11- يقر الداعم بموافقته للمنصة بتغيير موقع المشروع أو الخدمة في الحالات الطارئة أو الحروب أو الكوارث الطبيعية على حسب المبلغ المتوفر للدعم، أو في حالة الكفالة فإن الداعم يفوض المنصة بتغيير المكفول بالبديل الأنسب بما يتوافق مع مواصفات المكفول الأول.
                                    12- يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة المرعية وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والأحكام الخاصة باستخدام بالمنصة.
                                    13- الداعم والمنصة بالحفاظ على سرية المعلومات بموجب هذه الاتفاقية لأي جهة.
                                    14- يقر الداعم بأنه قد قرأ بنود هذه الاتفاقية وتفهمها فهماً نافياً للجهالة، واتفق على الالتزام ببنود هذه الاتفاقية والشروط والأحكام الواردة فيها.
                                    15- تخضع هذه الاتفاقية للأنظمة المعمول بها في المملكة المتحدة ويجري تفسيره وتنفيذه والفصل فيما ينشأ من دعاوى أو نزاعات وفقاً لهذه الأنظمة.
                                    16- بمجرد الضغط على موافق سيكون الداعم ملزماً بإتباع الشروط والأحكام.',
                'nation_id' => 2,
            ],

            [
                'key' => 'supporter_payment_rules_uk_en',
                'content' =>       '1- Each user of the we help platform is subject to the general terms and conditions of use, and your acknowledgment of these general terms is your agreement without any restriction or condition to the terms and conditions of use, whether you are a registered user or not, and this approval is effective from the date of your first use of this platform.
                                    2- The We Help platform works as an intermediary in exchange for a commission from projects between humanitarian institutions and those wishing to support their projects, and provides several services, including: monitoring project implementation, digital marketing, and provides these services in exchange for a percentage or commission from the amount of support.
                                    3- The supporter declares that any information or data that he registers or adds is under his responsibility, and the platform has the right to claim any damages or results that result from the incorrectness of the information.
                                    4- The responsibility of the platform is limited to mediation in receiving the value of the project, paying it to the implementing institution, monitoring the implementation of projects, and providing the supporter with reports and updates about the project, and thus it is not responsible for any errors or omissions made by the supporter or the association outside the aforementioned scope.
                                    5- All information on this site is protected by copyright, and it is not permissible to reproduce or own it in any way without the consent of the management of the platform, and any modification to the material or its use for other purposes is considered a violation of copyright and copyright.
                                    6- The supporter declares that he is aware, through the information shown on the platform, of the basic rules, regulations, purchase and support policies stipulated in relation to the basic characteristics of the service, its value, payment method, and terms of implementation, and he acknowledges that he has read the terms and laws for using the platform.
                                    7- The platform allows the disclosure of the amount due and the mechanism for paying the project, and in the event of an agreement on a split payment, the supporter undertakes to pay the remaining amount at the specified time for payment.
                                    8- The platform provides the project selection service, and he is entitled to refund the support amount within (48) hours from the date of support, and in the event that the refund is requested after the lapse of (48) hours, the amount can be returned after deducting the percentage allocated to the platform within 7 days from the date of the refund request. Also, the supporter has no right to withdraw from the support after 7 days have passed from the date of support.
                                    9- The online services provided by the platform on the Internet, provided only to facilitate manual procedures; Thus you acknowledge with your full knowledge that communications over the Internet are subject to interruptions or interference and interception by others, so resorting to this platform remains at your own risk.
                                    10- Failure to write the correct data and information for communication loses the supporter the right to obtain any report, update or claims about the service and the supported product.
                                    11- The supporter acknowledges his agreement to the platform to change the project or service site in emergency situations, wars or natural disasters, depending on the amount available for support, or in the case of sponsorship, the supporter authorizes the platform to change the sponsored person with the most appropriate alternative in accordance with the specifications of the first sponsored.
                                    12- The supporter declares that he is aware, through the information shown on the platform, of the basic rules and regulations in force and the purchase and support policies stipulated in relation to the basic characteristics of the service, its value, payment method, and terms of implementation. He also acknowledges that he has read the terms and conditions for using the platform.
                                    13- The supporter and platform to maintain the confidentiality of information under this agreement for any party.
                                    14- The supporter declares that he has read the terms of this agreement and understands them with a negative understanding of ignorance, and has agreed to abide by the terms of this agreement and the terms and conditions contained therein.
                                    15- This agreement is subject to the regulations in force in the United Kingdom, and it shall be interpreted, implemented, and adjudicated in relation to claims or disputes arising in accordance with these regulations.
                                    16- Once you click on I agree, the supporter will be obligated to follow the terms and conditions.',
                'nation_id' => 2,
            ],            
            // هذه الوثيقة لابد أن يوافق عليها الداعم قبل الدخول على عملية السداد أو الدفع أو الشراء - END

            //------------------------------------------------------------------------------------------------------

            // شروط التسجيل فى المنصه للجمعيه - START
            // النسخه البريطانى
          

            // النسخه البريطانى

            [
                'key' => 'charity_register_rules_uk_ar',
                'content' =>       '1- أن تكون حاصلة على ترخيص رسمي من جهة الاختصاص في بلدها المزاولة نشاطها.
                                    2- ان يكون لها نظام داخلي ومجلس إدارة منتخب لا يقل عدد أعضائه عن خمسة اشخاص وإدارة تنفيذية محددة الأشخاص والمناصب.
                                    3- ان يكون لديها نظام مالي منضبط وفق الأصول المهنية وحساب بنكي مسجل باسمها وبصفتها جمعية ( غير ربحية )
                                    4- ان يكون لها نشاط ملموس وحقيقي وخطط عمل واضحة 
                                    5- ان يكون لديها حد أدنى لتمويل في تسير اعمالها الرئيسية.
                                    6- ان يكون قد مضى على انشائها ( 5 سنوات أو 3 سنوات)على الأقل
                                    7- ان يكون لها مقر أو عنوان محدد ويمكن الوصول اليها 
                                    8- الاتكون مدرجة في قائمة تصنيف معتبرة .',
                'nation_id' => 2,
            ],              

            [
                'key' => 'charity_register_rules_uk_en',
                'content' =>       '1- Charity must have an official license from the competent authority in her country to conduct its activity.
                                    2- To have an elected bylaw and an elected board of directors whose members are not less than five persons, and an executive department with specific persons and positions.
                                    3- It must have a disciplined financial system according to professional principles and a bank account registered in its name and as an association (non-profit)
                                    4- To have a tangible and real activity and clear work plans
                                    5- It must have a minimum amount of funding in running its main business.
                                    6- At least 5 years or 3 years have passed since its establishment
                                    7- It must have a specific location or address and can be reached
                                    8- Not to be included in a considered classification list.',
                'nation_id' => 2,
            ],
          
            // شروط التسجيل فى المنصه للجمعيه - END

            //------------------------------------------------------------------------------------------------------




            //------------------------------------------------------------------------------------------------------

            //  السياسات المتعلقه بالموظفين المستخدمين للمنصه   - START 
            // هذه السياسات يوافق عليها الموظف عند تسجيل الدخول للمره الاولى فقط
            // فى حالة عد الموافقه لا يتمكن الموظف من استخدام المنصه


            // النسخه البريطانى
          

            [
                'key' => 'emp_politics_rules_uk_ar',
                'content' =>       '1- أن تكون حاصلة على ترخيص رسمي من جهة الاختصاص في بلدها المزاولة نشاطها.
                                    2- ان يكون لها نظام داخلي ومجلس إدارة منتخب لا يقل عدد أعضائه عن خمسة اشخاص وإدارة تنفيذية محددة الأشخاص والمناصب.
                                    3- ان يكون لديها نظام مالي منضبط وفق الأصول المهنية وحساب بنكي مسجل باسمها وبصفتها جمعية ( غير ربحية )
                                    4- ان يكون لها نشاط ملموس وحقيقي وخطط عمل واضحة 
                                    5- ان يكون لديها حد أدنى لتمويل في تسير اعمالها الرئيسية.
                                    6- ان يكون قد مضى على انشائها ( 5 سنوات أو 3 سنوات)على الأقل
                                    7- ان يكون لها مقر أو عنوان محدد ويمكن الوصول اليها 
                                    8- الاتكون مدرجة في قائمة تصنيف معتبرة .',
                'nation_id' => 2,
            ],              

            [
                'key' => 'emp_politics_rules_uk_en',
                'content' =>       '1- Charity must have an official license from the competent authority in her country to conduct its activity.
                                    2- To have an elected bylaw and an elected board of directors whose members are not less than five persons, and an executive department with specific persons and positions.
                                    3- It must have a disciplined financial system according to professional principles and a bank account registered in its name and as an association (non-profit)
                                    4- To have a tangible and real activity and clear work plans
                                    5- It must have a minimum amount of funding in running its main business.
                                    6- At least 5 years or 3 years have passed since its establishment
                                    7- It must have a specific location or address and can be reached
                                    8- Not to be included in a considered classification list.',
                'nation_id' => 2,
            ],

            // النسخه السعودى

            [
                'key' => 'emp_politics_rules_sa_ar',
                'content' =>       '1- أن تكون حاصلة على ترخيص رسمي من جهة الاختصاص في بلدها المزاولة نشاطها.
                                    2- ان يكون لها نظام داخلي ومجلس إدارة منتخب لا يقل عدد أعضائه عن خمسة اشخاص وإدارة تنفيذية محددة الأشخاص والمناصب.
                                    3- ان يكون لديها نظام مالي منضبط وفق الأصول المهنية وحساب بنكي مسجل باسمها وبصفتها جمعية ( غير ربحية )
                                    4- ان يكون لها نشاط ملموس وحقيقي وخطط عمل واضحة 
                                    5- ان يكون لديها حد أدنى لتمويل في تسير اعمالها الرئيسية.
                                    6- ان يكون قد مضى على انشائها ( 5 سنوات أو 3 سنوات)على الأقل
                                    7- ان يكون لها مقر أو عنوان محدد ويمكن الوصول اليها 
                                    8- الاتكون مدرجة في قائمة تصنيف معتبرة .',
                'nation_id' => 1,
            ],              

            [
                'key' => 'emp_politics_rules_sa_en',
                'content' =>       '1- Charity must have an official license from the competent authority in her country to conduct its activity.
                                    2- To have an elected bylaw and an elected board of directors whose members are not less than five persons, and an executive department with specific persons and positions.
                                    3- It must have a disciplined financial system according to professional principles and a bank account registered in its name and as an association (non-profit)
                                    4- To have a tangible and real activity and clear work plans
                                    5- It must have a minimum amount of funding in running its main business.
                                    6- At least 5 years or 3 years have passed since its establishment
                                    7- It must have a specific location or address and can be reached
                                    8- Not to be included in a considered classification list.',
                'nation_id' => 1,
            ],

            
        ]);
    }
}
