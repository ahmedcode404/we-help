<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class RatingCriteriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('rating__criterias')->insert([

            // SA
            [
                'name_ar'   => 'المعيار الأول',
                'name_en'   => 'First Standard',
                'nation_id' => 1,
            ],
            [
                'name_ar'   => 'المعيار الثاني',
                'name_en'   => 'Second Standard',
                'nation_id' => 1,
            ],
            [
                'name_ar'   => 'المعيار الثالث',
                'name_en'   => 'Third Standard',
                'nation_id' => 1,
            ],            
            [
                'name_ar'   => 'المعيار الرابع',
                'name_en'   => 'Fourth Standard',
                'nation_id' => 1,
            ],
            [
                'name_ar'   => 'المعيار الخامس',
                'name_en'   => 'Fifth Standard',
                'nation_id' => 1,
            ],
            
            // UK
            [
                'name_ar'   => 'المعيار الأول',
                'name_en'   => 'First Standard',
                'nation_id' => 2,
            ],
            [
                'name_ar'   => 'المعيار الثاني',
                'name_en'   => 'Second Standard',
                'nation_id' => 2,
            ],
            [
                'name_ar'   => 'المعيار الثالث',
                'name_en'   => 'Third Standard',
                'nation_id' => 2,
            ],            
            [
                'name_ar'   => 'المعيار الرابع',
                'name_en'   => 'Fourth Standard',
                'nation_id' => 2,
            ],
            [
                'name_ar'   => 'المعيار الخامس',
                'name_en'   => 'Fifth Standard',
                'nation_id' => 2,
            ],

        ]);
    }
}
