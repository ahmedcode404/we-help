<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class CarityCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('charity_categories')->insert([

            // SA
            [
                // 1
                'slug' => 'healthy',
                'name_ar' => 'صحي',
                'name_en' => 'healthy',
                'superadmin_ratio' => 10,
                'nation_id' => 1,
                'type' => null,
            ],
            // 2
            [
                'slug' => 'seasonal',
                'name_ar' => 'موسمي',
                'name_en' => 'seasonal',
                'superadmin_ratio' => 20,
                'nation_id' => 1,
                'type' => null,
            ],
            // 3
            [
                'slug' => 'educational',
                'name_ar' => 'تعليمي',
                'name_en' => 'educational',
                'superadmin_ratio' => 40,
                'nation_id' => 1,
                'type' => 'educational',
            ] ,
            // 4
            [
                'slug' => 'structural',
                'name_ar' => 'انشائي',
                'name_en' => 'structural',
                'superadmin_ratio' => 70,
                'nation_id' => 1,
                'type' => 'engineering',
            ] ,
            // 5
            [
                'slug' => 'Take-care-of-an-orphan',
                'name_ar' => 'رعاية يتيم',
                'name_en' => 'Take care of an orphan',
                'superadmin_ratio' => 60,
                'nation_id' => 1,
                'type' => 'sponsorship',
            ] , 
            // 6
            [
                'slug' => 'An-emergency-project',
                'name_ar' => 'مشروع طارئ',
                'name_en' => 'An emergency project',
                'superadmin_ratio' => 30,
                'nation_id' => 1,
                'type' => 'emergency',
            ],
            // 7
            [
                'slug' => 'Sustainable',
                'name_ar' => 'مشاريع مستدامه',
                'name_en' => 'Sustainable projects',
                'superadmin_ratio' => 80,
                'nation_id' => 1,
                'type' => 'sustainable',
            ],   
            
            // UK
            // 8
            [
                'slug' => 'healthy-',
                'name_ar' => 'صحي',
                'name_en' => 'healthy',
                'superadmin_ratio' => 10,
                'nation_id' => 2,
                'type' => null,
            ],
            // 9
            [
                'slug' => 'seasonal-',
                'name_ar' => 'موسمي',
                'name_en' => 'seasonal',
                'superadmin_ratio' => 20,
                'nation_id' => 2,
                'type' => null,
            ],
            // 10
            [
                'slug' => 'educational-',
                'name_ar' => 'تعليمي',
                'name_en' => 'educational',
                'superadmin_ratio' => 40,
                'nation_id' => 2,
                'type' => 'educational',
            ] ,
            // 11
            [
                'slug' => 'structural-',
                'name_ar' => 'انشائي',
                'name_en' => 'structural',
                'superadmin_ratio' => 70,
                'nation_id' => 2,
                'type' => 'engineering',
            ] ,
            // 12
            [
                'slug' => 'Take-care-of-an-orphan-',
                'name_ar' => 'رعاية يتيم',
                'name_en' => 'Take care of an orphan',
                'superadmin_ratio' => 60,
                'nation_id' => 2,
                'type' => 'sponsorship',
            ] , 
            // 13
            [
                'slug' => 'An-emergency-project-',
                'name_ar' => 'مشروع طارئ',
                'name_en' => 'An emergency project',
                'superadmin_ratio' => 30,
                'nation_id' => 2,
                'type' => 'emergency',
            ],
            // 14
            [
                'slug' => 'Sustainable',
                'name_ar' => 'مشاريع مستدامه',
                'name_en' => 'Sustainable projects',
                'superadmin_ratio' => 80,
                'nation_id' => 2,
                'type' => 'sustainable',
            ] 
        ]);
    }
}
