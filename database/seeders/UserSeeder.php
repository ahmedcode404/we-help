<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([

            'name_ar' => 'مؤسسة وى هيلب',
            'name_en' => 'We Help Organization',
            'password' => bcrypt('123456'),
            'email' => 'info@jaadara.com',
            'phone' => '+966587412369',
            'blocked' => 0,
            'country' => 'EG',
            'nation_id' => getNationId(),
        ]);

        $user->assignRole('admin');
        $user->syncPermissions(Permission::all());

    }
}
