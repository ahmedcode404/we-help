<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class SettingSASeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([

            // اسم المنظه عربى + انجليزى للسعوديه
            [
                'key' => 'org_name_sa_ar',
                'neckname' => 'admin.org_name_ar',
                'type' => 'text',
                'value' => 'وى هيلب',
                'nation_id' => 1,
            ],
            [
                'key' => 'org_name_sa_en',
                'neckname' => 'admin.org_name_en',
                'type' => 'text',
                'value' => 'We help',
                'nation_id' => 1,
            ],

            // رقم التسجيل للسعوديه
            [
                'key' => 'license_number_sa',
                'neckname' => 'admin.license_number',
                'type' => 'text',
                'value' => '123456',
                'nation_id' => 1,
            ],

            // ممثل المنظمه للسعوديه 
            [
                'key' => 'representer_name_sa_ar',
                'neckname' => 'admin.representer_name_sa_ar',
                'type' => 'text',
                'value' => 'test',
                'nation_id' => 1,
            ],

            [
                'key' => 'representer_name_sa_en',
                'neckname' => 'admin.representer_name_sa_en',
                'type' => 'text',
                'value' => 'test',
                'nation_id' => 1,
            ],
            
             // رسالة قبول طلب تسجيل الجمعيه للسعوديه
             [
                'key' => 'charity_approve_msg_sa_ar',
                'neckname' => 'admin.charity_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب تسجيل الجمعية الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'charity_approve_msg_sa_en',
                'neckname' => 'admin.charity_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Registeration Request is Approved',
                'nation_id' => 1,
            ],

            // رسالة قبول طلب الصرف للسعوديه
            [
                'key' => 'fin_req_approve_msg_sa_ar',
                'neckname' => 'admin.fin_req_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب الصرف الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'fin_req_approve_msg_sa_en',
                'neckname' => 'admin.fin_req_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Exchange Request is Approved',
                'nation_id' => 1,
            ],

            // رسالة رفض طلب تسجيل الجمعيه للسعوديه
            [
                'key' => 'charity_reject_msg_sa_ar',
                'neckname' => 'admin.charity_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب تسجيل الجمعية الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'charity_reject_msg_sa_en',
                'neckname' => 'admin.charity_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Registeration Request is rejected',
                'nation_id' => 1,
            ],

            // رسالة رفض طلب الصرف للسعوديه
            [
                'key' => 'fin_req_reject_msg_sa_ar',
                'neckname' => 'admin.fin_req_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب الصرف الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'fin_req_reject_msg_sa_en',
                'neckname' => 'admin.fin_req_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Exchange Request is rejected',
                'nation_id' => 1,
            ],
            
            // رسالة قبول تقرير للسعوديه
            [
                'key' => 'report_approve_msg_sa_ar',
                'neckname' => 'admin.report_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول التقرير الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'report_approve_msg_sa_en',
                'neckname' => 'admin.report_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Report is Approved',
                'nation_id' => 1,
            ],

            // رسالة رفض تقرير للسعوديه
            [
                'key' => 'report_reject_msg_sa_ar',
                'neckname' => 'admin.report_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض التقرير الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'report_reject_msg_sa_en',
                'neckname' => 'admin.report_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Report is Rejected',
                'nation_id' => 1,
            ],

            // رسالة قبول طلب تسجيل مشروع  للسعوديه
            [
                'key' => 'project_approve_msg_sa_ar',
                'neckname' => 'admin.project_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب تسجيل المشروع الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'project_approve_msg_sa_en',
                'neckname' => 'admin.project_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Project Request is Approved',
                'nation_id' => 1,
            ], 

            // رسالة رفض طلب تسجيل مشروع  للسعوديه
            [
                'key' => 'project_reject_msg_sa_ar',
                'neckname' => 'admin.project_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب تسجيل المشروع الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'project_reject_msg_sa_en',
                'neckname' => 'admin.project_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Project Request is rejected',
                'nation_id' => 1,
            ], 

            // رسالة قبول طلب تعديل للسعوديه
            [
                'key' => 'edit_approve_msg_sa_ar',
                'neckname' => 'admin.edit_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب التعديل الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'edit_approve_msg_sa_en',
                'neckname' => 'admin.edit_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Edit Request is Approved',
                'nation_id' => 1,
            ], 

            // رسالة رفض طلب تعديل للسعوديه
            [
                'key' => 'edit_reject_msg_sa_ar',
                'neckname' => 'admin.edit_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب التعديل الخاص بكم',
                'nation_id' => 1,
            ],
            [
                'key' => 'edit_reject_msg_sa_en',
                'neckname' => 'admin.edit_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Edit Request is rejected',
                'nation_id' => 1,
            ], 

            // شعار وى هيب للسعوديه
            [
                'key' => 'we_help_slogan_sa_ar',
                'neckname' => 'admin.we_help_slogan_sa_ar',
                'type' => 'text',
                'value' => 'شعار وى هيلب',
                'nation_id' => 1,
            ],
            [
                'key' => 'we_help_slogan_sa_en',
                'neckname' => 'admin.we_help_slogan_sa_en',
                'type' => 'text',
                'value' => 'We help slogan',
                'nation_id' => 1,
            ],

            // بيانات تسجيل دخول الموظفين السعودية

            [
                'key' => 'msg_data_login_sa_ar',
                'neckname' => 'admin.msg_data_login_sa_ar',
                'type' => 'text',
                'value' => 'بيانات الدخول الي منصه وي هيلب',
                'nation_id' => 1,
            ], 
            
            [
                'key' => 'msg_data_login_sa_en',
                'neckname' => 'admin.msg_data_login_sa_en',
                'type' => 'text',
                'value' => 'Data Login To Platform We Help',
                'nation_id' => 1,
            ], 
            
            //   نص مشاريعنا فى الصفحة الرئيسية للسعوديه
            [
                'key' => 'our_projects_text_sa_ar',
                'neckname' => 'admin.our_projects_text_sa_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 1,
            ],
            [
                'key' => 'our_projects_text_sa_en',
                'neckname' => 'admin.our_projects_text_sa_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 1,
            ],

            //   نص الاحصائيات فى الصفحة الرئيسية للسعوديه
            [
                'key' => 'statistics_text_sa_ar',
                'neckname' => 'admin.statistics_text_sa_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 1,
            ],
            [
                'key' => 'statistics_text_sa_en',
                'neckname' => 'admin.statistics_text_sa_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 1,
            ],

            //   نص شركائنا فى الصفحة الرئيسية للسعوديه
            [
                'key' => 'parteners_text_sa_ar',
                'neckname' => 'admin.parteners_text_sa_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 1,
            ],
            [
                'key' => 'parteners_text_sa_en',
                'neckname' => 'admin.parteners_text_sa_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 1,
            ],

            //   نص من نحن فى الصفحة الرئيسية للسعوديه
            [
                'key' => 'about_us_text_sa_ar',
                'neckname' => 'admin.about_us_text_sa_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 1,
            ],
            [
                'key' => 'about_us_text_sa_en',
                'neckname' => 'admin.about_us_text_sa_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 1,
            ],

            //   نص  تواصل معنا فى الصفحة الرئيسية للسعوديه
            [
                'key' => 'contact_us_text_sa_ar',
                'neckname' => 'admin.contact_us_text_sa_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 1,
            ],
            [
                'key' => 'contact_us_text_sa_en',
                'neckname' => 'admin.contact_us_text_sa_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 1,
            ],

            // setting other sa

            [
                'key' => 'text_country_sa_ar',
                'neckname' => 'admin.text_country_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ], 
            
            [
                'key' => 'text_country_sa_en',
                'neckname' => 'admin.text_country_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],             
            
            [
                'key' => 'text_project_sa_ar',
                'neckname' => 'admin.text_project_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],
            
            [
                'key' => 'text_project_sa_en',
                'neckname' => 'admin.text_project_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],            
            
            [
                'key' => 'text_amount_sa_ar',
                'neckname' => 'admin.text_amount_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],
             
            [
                'key' => 'text_amount_sa_en',
                'neckname' => 'admin.text_amount_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],
            
            [
                'key' => 'text_partener_sa_ar',
                'neckname' => 'admin.text_partener_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ], 
            
            [
                'key' => 'text_partener_sa_en',
                'neckname' => 'admin.text_partener_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],             
            
            [
                'key' => 'text_beneficiary_sa_ar',
                'neckname' => 'admin.text_beneficiary_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],  
            
            [
                'key' => 'text_beneficiary_sa_en',
                'neckname' => 'admin.text_beneficiary_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 1,
            ],

            // العنوان للسعوديه
            [
                'key' => 'address_sa',
                'neckname' => 'admin.address',
                'type' => 'text',
                'value' => 'test',
                'nation_id' => 1,
            ],

             // msg notify accept bond exchange (SA)
             [
                'key' => 'msg_bond_accept_sa_ar',
                'neckname' => 'admin.msg_bond_accept_sa_ar',
                'type' => 'text',
                'value' => 'برجاء قبول طلب السند الخاص بكم ',
                'nation_id' => 1,
            ], 

            [
                'key' => 'msg_bond_accept_sa_en',
                'neckname' => 'admin.msg_bond_accept_sa_en',
                'type' => 'text',
                'value' => 'Please accept your bond request',
                'nation_id' => 1,
            ],  

            // الموقع للسعوديه 
            [
                'key' => 'website_sa',
                'neckname' => 'admin.website',
                'type' => 'text',
                'value' => 'www.test.com',
                'nation_id' => 1,
            ],

            [
                'key' => 'facebook_sa',
                'neckname' => 'admin.facebook_sa',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 1,
            ], 

            [
                'key' => 'twitter_sa',
                'neckname' => 'admin.twitter_sa',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 1,
            ], 
            
            [
                'key' => 'snapchat_sa',
                'neckname' => 'admin.snapchat_sa',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 1,
            ],  
            
            [
                'key' => 'insta_sa',
                'neckname' => 'admin.insta_sa',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 1,
            ],

             // مهلة استلام الميلغ من قبل الجمعيه للسعوديه
             [
                'key' => 'revieved_duration_sa',
                'neckname' => 'admin.revieved_duration',
                'type' => 'number',
                'value' => 3,
                'nation_id' => 1,
            ],

            //   عدد مراحل المشاريع للسعوديه
            [
                'key' => 'phases_number_sa',
                'neckname' => 'admin.phases_number',
                'type' => 'number',
                'value' => 10,
                'nation_id' => 1,
            ],

            //     اقل حد للتبرع للسعوديه
            [
                'key' => 'min_support_sa',
                'neckname' => 'admin.min_support',
                'type' => 'number',
                'value' => 100,
                'nation_id' => 1,
            ],

            // هاتف وى هيب للسعوديه
            [
                'key' => 'phone_sa',
                'neckname' => 'admin.phone',
                'type' => 'phone',
                'value' => '+966562514852',
                'nation_id' => 1,
            ],

            // ايميل وى هيب للسعوديه
            [
                'key' => 'email_sa',
                'neckname' => 'admin.email',
                'type' => 'email',
                'value' => 'wehelp@sa.com',
                'nation_id' => 1,
            ],

             // العملة الافتراضية   للسعوديه
             [
                'key' => 'default_currency_sa',
                'neckname' => 'admin.default_currency',
                'type' => 'select',
                'value' => 2,
                'nation_id' => 1,
            ],

            // لوجو وى هيب للسعوديه
            [
                'key' => 'we_help_logo_sa_ar',
                'neckname' => 'admin.we_help_logo_sa_ar',
                'type' => 'file',
                'value' => 'logo-sa-ar.png',
                'nation_id' => 1,
            ],
            [
                'key' => 'we_help_logo_sa_en',
                'neckname' => 'admin.we_help_logo_sa_en',
                'type' => 'file',
                'value' => 'logo-sa-en.png',
                'nation_id' => 1,
            ],

            [
                'key' => 'enable_svg_logo_sa',
                'neckname' => 'admin.enable_svg_logo',
                'type' => 'checkbox',
                'value' => 0,
                'nation_id' => 1,
            ],


            [
                'key' => 'stamp_sa',
                'neckname' => 'admin.stamp_sa',
                'type' => 'file',
                'value' => 'stamp.png',
                'nation_id' => 1,
            ],  
            
           
            // logo footer

            [
                'key' => 'logo_sa_ar',
                'neckname' => 'admin.logo_sa_ar',
                'type' => 'file',
                'value' => 'logo.png',
                'nation_id' => 1,
            ], 
            
            [
                'key' => 'logo_sa_en',
                'neckname' => 'admin.logo_sa_en',
                'type' => 'file',
                'value' => 'logo.png',
                'nation_id' => 1,
            ], 
            
            [
                'key' => 'pages_header_sa',
                'neckname' => 'admin.pages_header',
                'type' => 'file',
                'value' => 'web/images/pages-bg/10.png',
                'nation_id' => 1,
            ],

            // مقدمة التعاقد عربى + انجليزى للسعوديه   
            [
                'key' => 'contract_intro_sa_ar',
                'neckname' => 'admin.contract_intro_ar',
                'type' => 'textarea',
                'value' => 'test',
                'nation_id' => 1,
            ],
            [
                'key' => 'contract_intro_sa_en',
                'neckname' => 'admin.contract_intro_en',
                'type' => 'textarea',
                'value' => 'test',
                'nation_id' => 1,
            ],   

            // وصف وى هيب للسعوديه
            [
                'key' => 'we_help_desc_sa_ar',
                'neckname' => 'admin.we_help_desc_ar',
                'type' => 'textarea',
                'value' => 'وصف وى هيلب',
                'nation_id' => 1,
            ],
            [
                'key' => 'we_help_desc_sa_en',
                'neckname' => 'admin.we_help_desc_en',
                'type' => 'textarea',
                'value' => 'We Help decription',
                'nation_id' => 1,
            ],
   
            // lat  , lang and address sa
            [
                'key' => 'langtude_sa',
                'neckname' => 'admin.langtude',
                'type' => 'hidden',
                'value' => '31.381523',
                'nation_id' => 1,
            ],

            [
                'key' => 'latitude_sa',
                'neckname' => 'admin.latitude',
                'type' => 'hidden',
                'value' => '31.037933',
                'nation_id' => 1,
            ],           
                
        ]);
        
    }
}
