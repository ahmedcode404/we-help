<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class CharityCharityCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('charity_charity_category')->insert([
            [
                'charity_category_id' => 1,
                'charity_id' => 1,
            ],
            [
                'charity_category_id' => 2,
                'charity_id' => 1,
            ],
            [
                'charity_category_id' => 3,
                'charity_id' => 1,
            ], 
            [
                'charity_category_id' => 4,
                'charity_id' => 1,
            ],  
            [
                'charity_category_id' => 5,
                'charity_id' => 1,
            ], 
            [
                'charity_category_id' => 6,
                'charity_id' => 1,
            ],  
            [
                'charity_category_id' => 7,
                'charity_id' => 1,
            ],    
            
            
            [
                'charity_category_id' => 1,
                'charity_id' => 2,
            ],
            [
                'charity_category_id' => 2,
                'charity_id' => 2,
            ],
            [
                'charity_category_id' => 3,
                'charity_id' => 2,
            ], 
            [
                'charity_category_id' => 4,
                'charity_id' => 2,
            ],  
            [
                'charity_category_id' => 5,
                'charity_id' => 2,
            ], 
            [
                'charity_category_id' => 6,
                'charity_id' => 2,
            ],  
            [
                'charity_category_id' => 7,
                'charity_id' => 2,
            ],     
           
        ]);
    }
}
