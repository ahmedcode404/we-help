<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class StaticPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Static pages
        DB::table('static_pages')->insert([

            // UK Version

            // about us strat - id = 1
            [
                'key' => 'about_us',
                'parent_id' => null,
                'title_ar' => 'من نحن',
                'title_en' => 'About Us',
                'content_ar' => 'ICH هي شركة ذات مسؤولية محدودة مرخصة برخصة رقم. (11077577) في المملكة المتحدة (بريطانيا)
                                لديها مكتب في برمنغهام ، ومتخصص في العمل على تقديم خدمات استشارية وتسويقية دولية
                                الهيئات ومؤسسات الإغاثة الإنسانية. في مساعدتها في الحصول على دعم مادي لتنفيذها
                                مشاريع وأعمال إغاثية إنسانية للمستفيدين في المناطق المحتاجة دون تمييز عرقي أو ديني.
                                تقدم المنصة مزايا عديدة للمستفيدين من أهمها خاصية الاسترجاع
                                الدعم المتكرر في الأوقات التي تختارها ، وإعلام المستخدم بأهم الأنشطة والعاجلة
                                الحملات الإنسانية مع التصنيف حسب الدول أو الأنواع ، مع تزويده بكافة المعلومات عنها
                                الجمعية أو المؤسسة التي يدعمها وكيفية التواصل معها.',
                'content_en' => 'ICH is a limited liability company licensed with license no. (11077577) in the Kingdom United ( Britain) 
                                has an office in Birmingham, and specializes in working to provide Advisory and marketing services for international 
                                bodies and humanitarian relief institutions. In helping her get material support for the implementation of her 
                                projects and relief work Humanity for beneficiaries in need areas without ethnic or religious discrimination.
                                The platform offers several advantages to the beneficiaries, the most important of which is the feature of recalling 
                                repeated support at the times of your choice, and informing the user of the most important activities and urgent 
                                humanitarian campaigns with classification by countries or types, while providing him with all the information about 
                                the association or institution to which he supports, and how to communicate with them.',
                'image' => url('images/main/leaf.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            // about us end

            // about corporation start - id = 2
            [
                'key' => 'about_corporation',
                'parent_id' => null,
                'title_ar' => 'حول المؤسسة الدولية للخدمات الإنسانية',
                'title_en' => 'About International Corporation For Humanitarian',
                'content_ar' => 'ICH هي شركة ذات مسؤولية محدودة مرخصة برخصة رقم. (11077577) في المملكة المتحدة (بريطانيا) لديها مكتب
                                في برمنغهام ، وتتخصص في العمل على تقديم خدمات استشارية وتسويقية للهيئات الدولية والإنسانية
                                مؤسسات الإغاثة. في مساعدتها في الحصول على الدعم المادي لتنفيذ مشاريعها وأعمال الإغاثة الإنسانية من أجلها
                                المستفيدون في المناطق المحتاجة دون تمييز عرقي أو ديني.',
                'content_en' => 'ICH is a limited liability company licensed with license no. (11077577) in the Kingdom United ( Britain) has an office 
                                in Birmingham, and specializes in working to provide Advisory and marketing services for international bodies and humanitarian 
                                relief institutions. In helping her get material support for the implementation of her projects and relief work Humanity for 
                                beneficiaries in need areas without ethnic or religious discrimination.',
                'image' => url('images/about/logo.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            // about corporation end

            // vision and mission start - id = 3
            [
                'key' => 'vision_and_mission',
                'parent_id' => null,
                'title_ar' => 'الرؤية و الرسالة',
                'title_en' => 'Vision and Mission',
                'content_ar' => null,
                'content_en' => null,
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'vision_and_mission',
                'parent_id' => 3,
                'title_ar' => 'الرؤية',
                'title_en' => 'Vision',
                'content_ar' => '<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">معاً</span>
                                    <br>
                                    <span style="letter-spacing: 1px; font-size:20px; margin: 10px auto 10px; display: block;">نحافظ على</span>
                                    <span style="letter-spacing: 1px; font-size:30px; font-family: "bold"; text-transform: uppercase;">إنسانيتهم</span>

                                </p>',
                'content_en' => '<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">Together</span>
                                    <br>
                                    <span style="letter-spacing: 1px; font-size:20px; margin: 10px auto 10px; display: block;">we preserve</span>
                                    <span style="letter-spacing: 1px; font-size:30px; font-family: "bold"; text-transform: uppercase;">their humanity</span>

                                </p>',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'vision_and_mission',
                'parent_id' => 3,
                'title_ar' => 'مهمتنا',
                'title_en' => 'Mission',
                'content_ar' => '<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">منصة عالمية متخصصة </span>  تقدم مشاريع تطويرية ذات تأثير بأدوات <span style="color:#3DBECB;font-family: "bold";">متابعة فعالة و مهنية عير شركات إستراتيجية عالية الجودة.</span>
                                </p>',
                'content_en' => '<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">A specialized global platform </span> offering Development projects with impact in ways Effective and professional follow-up tools via <span style="color:#3DBECB;font-family: "bold";">Quality strategic partnerships.</span>
                                </p>',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            // vision and mission end

            // out goal strat - id = 6
            [
                'key' => 'our_goal',
                'parent_id' => null,
                'title_ar' => 'هدفنا',
                'title_en' => 'Our Goal',
                'content_ar' => 'من نقطة انطلاق إنسانية بحتة ، بعيدًا عن اعتبارات الدين والعرق ،
                                اللون،
                                أو النطاق الجغرافي
                                من أجل الإنسانية ، ما يحدد نهجنا في المساعدة هو فقط
                                تحقيق، إنجاز
                                من احتياجات الضعفاء
                                والمنكوبة ، وتلامس اماكن المحتاجين دون تردد ، للمساهمة فيها
                                من خلال الوصول إلى المحتاجين
                                تسهيل الدعم للمشاريع التي تقوم بها إنسانية متخصصة
                                المؤسسات مثل
                                مؤسسة
                                استشاري وتنفيذي يساهم في دعم ودعم العمل الإنساني
                                المنظمات
                                لتحقيق أهدافهم.',
                'content_en' => 'From a purely human starting point, away from considerations of religion, race,
                                color,
                                or geographic scope
                                For the sake of humanity, what defines our approach on Help We is only the
                                fulfillment
                                of the needs of the vulnerable
                                And the afflicted, and touch the places of need without hesitation, to contribute to
                                reaching the needy through
                                Facilitating support for projects undertaken by specialized humanitarian
                                institutions as
                                an institution
                                Advisory and executive who contribute to support and support humanitarian
                                organizations
                                to achieve their goals.',
                'image' => asset('images/about/goal.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            // out goal end

            // our value start - id = 7
            [
                'key' => 'our_value',
                'parent_id' => null,
                'title_ar' => 'قيمتنا',
                'title_en' => 'Our Value',
                'content_ar' => null,
                'content_en' => null,
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 7,
                'title_ar' => 'الإستدامة',
                'title_en' => 'Sustainability',
                'content_ar' => 'نحن نعمل على استدامة أعمالنا واستدامة تأثيرها على المتلقي',
                'content_en' => 'We are working on Sustaining our business and the sustainability of its impact to the recipient',
                'image' => asset('images/about/icons/1.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 7,
                'title_ar' => 'المسؤولية',
                'title_en' => 'Responsibility',
                'content_ar' => 'نحن نلتزم بمسؤولياتنا تجاه أنفسنا والآخرين',
                'content_en' => 'We adhere to our responsibilities towards ourselves and others',
                'image' => asset('images/about/icons/2.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 7,
                'title_ar' => 'الثقة',
                'title_en' => 'Confidence',
                'content_ar' => 'نعمل بجد لخلق الثقة مع المستفيد',
                'content_en' => 'We work hard to create trust with beneficiary',
                'image' => asset('images/about/icons/3.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 7,
                'title_ar' => 'التنوع',
                'title_en' => 'Diversity',
                'content_ar' => 'نحن نعتز بالتنوع ونعتقد أنه إضافة مبتكرة لدينا ويعززها منافستنا في السوق',
                'content_en' => 'We cherish diversity and we believe he is an innovative addition we have and reinforces it our competition is in market',
                'image' => asset('images/about/icons/4.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 7,
                'title_ar' => 'التفان',
                'title_en' => 'Dedication',
                'content_ar' => 'نحن لا نقبل أنفسنا دون المستوى المطلوب ، فنحن نتحرى عن جودة جميع أعمالنا ، حيث إننا نعزز ثقافتها في البرامج والمشاريع التي نقدمها',
                'content_en' => 'We do not accept ourselves substandard we are investigating the quality all our business, as we are consolidating its culture in the programs and projects that we deliver',
                'image' => asset('images/about/icons/5.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 7,
                'title_ar' => 'التشارك',
                'title_en' => 'Participatory',
                'content_ar' => 'العلاقة التشاركية والتواصل مع الأطراف ذات العلاقة والاستثمار الأمثل لإمكانيات الطاقات وتكاملها',
                'content_en' => 'Participatory and communicate with related parties relationship and investment optimization of the potential and integration of energies',
                'image' => asset('images/about/icons/6.png'),
                'link' => null,
                'nation_id' => 2,
            ],
            // our value end

            // our services start - id = 14
            [
                'key' => 'our_services',
                'parent_id' => null,
                'title_ar' => 'خدماتنا',
                'title_en' => 'Our Services',
                'content_ar' => 'الخدمات المقدمة من we help:',
                'content_en' => 'The services provided by we help:',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'our_services',
                'parent_id' => 14,
                'title_ar' => '01',
                'title_en' => '01',
                'content_ar' => 'تسويق المشاريع الانسانية والخيرية والاغاثية وجمع الدعم المادي لها',
                'content_en' => 'Marketing humanitarian, charitable and relief projects and collecting material support for them',
                'image' => null,
                'link' => 'donate.html',
                'nation_id' => 2,
            ],
            [
                'key' => 'our_services',
                'parent_id' => 14,
                'title_ar' => '02',
                'title_en' => '02',
                'content_ar' => 'دعم إدارة المشاريع الإنسانية وجودة تنفيذها',
                'content_en' => 'Support in managing humanitarian projects and the quality of their implementation',
                'image' => null,
                'link' => 'donate.html',
                'nation_id' => 2,
            ],
            [
                'key' => 'our_services',
                'parent_id' => 14,
                'title_ar' => '03',
                'title_en' => '03',
                'content_ar' => 'تقديم تقارير دورية ودقيقة للداعمين',
                'content_en' => 'Provide periodic and accurate reports to the supporters',
                'image' => null,
                'link' => 'donate.html',
                'nation_id' => 2,
            ],
            // our services end

            // we help features start - id = 18
            [
                'key' => 'we_help_features',
                'parent_id' => null,
                'title_ar' => 'مميزات We Help',
                'title_en' => 'We Help Features',
                'content_ar' => 'تقدم المنصة العديد من الميزات للمستفيدين من أهمها تذكير الدعم المتكرر في
                                أوقات اختيارك ، يتم إعلام المستخدم بأبرز الأنشطة والحملات الإنسانية العاجلة مع
                                التصنيف حسب الدول أو الأنواع ، مع تزويده بكافة المعلومات عن الجمعية أو المؤسسة الخاصة بها
                                التي يقدمها ، وكيفية التواصل معها ، بالإضافة إلى:',
                'content_en' => 'The platform offers several features to beneficiaries, the most important of which is the recurring support reminder at 
                                times of your choosing, The user is informed of the most prominent urgent humanitarian activities and campaigns with a 
                                classification according to countries or types, with Provide him with all information about the association or institution for 
                                which he provides support, and how to communicate with it, in addition to:',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 18,
                'title_ar' => '01',
                'title_en' => '01',
                'content_ar' => 'تسويق المشاريع الانسانية والخيرية والاغاثية وجمع الدعم المادي لها',
                'content_en' => 'Marketing humanitarian, charitable and relief projects and collecting material support for them',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 18,
                'title_ar' => '02',
                'title_en' => '02',
                'content_ar' => 'امكانية الدعم والسداد الكامل او الجزئي للمشاريع',
                'content_en' => 'The possibility of support and full or partial payment of the projects',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 18,
                'title_ar' => '03',
                'title_en' => '03',
                'content_ar' => 'تذهب مبالغ الدعم مباشرة إلى حساب بنكي مخصص في بريطانيا ثم يتم تحويلها إلى حساب جمعيات حسب القواعد واللوائح المعتمدة حسب النظام',
                'content_en' => 'Support amounts go directly to a bank account Custom in Britain and is then converted into Account of associations according to approved rules and regulations Legal according to the system',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 18,
                'title_ar' => '04',
                'title_en' => '04',
                'content_ar' => 'منصة آمنة تسمح للجمعيات الخيرية الإنسانية الوحيدة المرخصة للانضمام إلى المنصة',
                'content_en' => 'A safe platform that allows charities The only licensed humanitarian to join the platform',
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            // we help features end

            // help center start - id = 23
            [
                'key' => 'help_center',
                'parent_id' => null,
                'title_ar' => 'مركز المساعدة',
                'title_en' => 'Help Center',
                'content_ar' => "لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.
                                لوريم إيبسوم هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ،
                                عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لعمل كتاب عينة من النوع.
                                لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة في التنضيد الإلكتروني ،
                                يبقى دون تغيير جوهريا. تم نشره في الستينيات بإصدار Letraset
                                أوراق تحتوي على مقاطع لوريم إيبسوم ، ومؤخراً باستخدام برامج النشر المكتبي مثل
                                Aldus PageMaker بما في ذلك إصدارات لوريم إيبسوم. هناك حقيقة مثبتة منذ زمن طويل أن القارئ سيفعل ذلك
                                يصرف المحتوى المقروء لصفحة ما عند النظر إلى تخطيطها. الهدف من استخدام لوريم إيبسوم
                                أنه يحتوي على توزيع طبيعي -إلى حد ما- للأحرف ، بدلاً من استخدام'محتوى هنا ، يوجد محتوى هنا' ،
                                مما يجعلها تبدو وكأنها لغة إنجليزية قابلة للقراءة. تستخدم العديد من حزم النشر المكتبي ومحرري صفحات الويب الآن لوريم إيبسوم كملف
                                نص النموذج الافتراضي الخاص بهم ، وسيكشف البحث عن 'lorem ipsum' عن العديد من مواقع الويب التي لا تزال في مهدها.
                                تطورت إصدارات مختلفة على مر السنين ، أحيانًا عن طريق الصدفة ، وأحيانًا عن قصد (روح الدعابة المحقونة وما شابه ذلك).
                                خلافًا للاعتقاد الشائع ، فإن Lorem Ipsum ليس مجرد نص عشوائي. لها جذور في قطعة من الكلاسيكية اللاتينية
                                الأدب من 45 قبل الميلاد ، مما يجعلها أكثر من 2000 سنة. ريتشارد مكلينتوك ، أستاذ اللغة اللاتينية في هامبدن - سيدني
                                كلية في فيرجينيا ، بحثت عن واحدة من أكثر الكلمات اللاتينية غموضًا ، consectetur ، من مقطع لوريم إيبسوم ،
                                ومن خلال استعراض اقتباسات الكلمة في الأدب الكلاسيكي ، اكتشف المصدر الذي لا شك فيه.
                                يأتي Lorem Ipsum من الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum'
                                (أقصى الخير والشر) بقلم شيشرون ، مكتوب عام 45 قبل الميلاد. هذا الكتاب عبارة عن أطروحة في نظرية الأخلاق ،
                                تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، 'Lorem ipsum dolor sit amet ..' ،
                                يأتي من سطر في القسم 1.10.32. تم نسخ الجزء القياسي من لوريم إيبسوم المستخدم منذ القرن الخامس عشر الميلادي
                                أدناه للمهتمين. الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum' بواسطة Cicero هي أيضًا
                                راكهام مستنسخة في شكلها الأصلي بالضبط ، مصحوبة بنسخ إنجليزية من ترجمة عام 1914 له.
                                هناك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم ، لكن الغالبية قد تعرضت للتغيير في شكل ما ،
                                عن طريق إدخال الدعابة أو الكلمات العشوائية التي لا تبدو قابلة للتصديق إلى حد ما. إذا كنت ستستخدم فقرة لوريم
                                إيبسوم ، عليك التأكد من عدم وجود أي شيء محرج مخفي في منتصف النص. جميع مولدات لوريم إيبسوم
                                على الإنترنت تميل إلى تكرار الأجزاء المحددة مسبقًا حسب الضرورة ، مما يجعل هذا أول مولد حقيقي على الإنترنت.
                                يستخدم قاموسًا يحتوي على أكثر من 200 كلمة لاتينية ، جنبًا إلى جنب مع حفنة من تراكيب الجملة النموذجية ، لإنشاء لوريم
                                Ipsum الذي يبدو معقولاً. لذلك ، فإن لوريم إيبسوم الذي تم إنشاؤه دائمًا ما يكون خاليًا من التكرار ، والفكاهة المحقونة ،
                                أو كلمات غير مميزة وما إلى ذلك.",
                'content_en' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                Aldus PageMaker including versions of Lorem Ipsum. t is a long established fact that a reader will 
                                be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum 
                                is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                                making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as 
                                their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. 
                                Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). 
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin 
                                literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                                and going through the cites of the word in classical literature, discovered the undoubtable source. 
                                Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' 
                                (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, 
                                very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', 
                                comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced 
                                below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also 
                                reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. 
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, 
                                by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem 
                                Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators 
                                on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. 
                                It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem 
                                Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, 
                                or non-characteristic words etc.",
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            // help center end

            // privacy and policy start - id = 24
            [
                'key' => 'privacy_policy',
                'parent_id' => null,
                'title_ar' => 'سياسة الخصوصية',
                'title_en' => 'Provicy Policy',
                'content_ar' => "لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.
                                لوريم إيبسوم هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ،
                                عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لعمل كتاب عينة من النوع.
                                لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة في التنضيد الإلكتروني ،
                                يبقى دون تغيير جوهريا. تم نشره في الستينيات بإصدار Letraset
                                أوراق تحتوي على مقاطع لوريم إيبسوم ، ومؤخراً باستخدام برامج النشر المكتبي مثل
                                Aldus PageMaker بما في ذلك إصدارات لوريم إيبسوم. هناك حقيقة مثبتة منذ زمن طويل أن القارئ سيفعل ذلك
                                يصرف المحتوى المقروء لصفحة ما عند النظر إلى تخطيطها. الهدف من استخدام لوريم إيبسوم
                                أنه يحتوي على توزيع طبيعي -إلى حد ما- للأحرف ، بدلاً من استخدام'محتوى هنا ، يوجد محتوى هنا' ،
                                مما يجعلها تبدو وكأنها لغة إنجليزية قابلة للقراءة. تستخدم العديد من حزم النشر المكتبي ومحرري صفحات الويب الآن لوريم إيبسوم كملف
                                نص النموذج الافتراضي الخاص بهم ، وسيكشف البحث عن 'lorem ipsum' عن العديد من مواقع الويب التي لا تزال في مهدها.
                                تطورت إصدارات مختلفة على مر السنين ، أحيانًا عن طريق الصدفة ، وأحيانًا عن قصد (روح الدعابة المحقونة وما شابه ذلك).
                                خلافًا للاعتقاد الشائع ، فإن Lorem Ipsum ليس مجرد نص عشوائي. لها جذور في قطعة من الكلاسيكية اللاتينية
                                الأدب من 45 قبل الميلاد ، مما يجعلها أكثر من 2000 سنة. ريتشارد مكلينتوك ، أستاذ اللغة اللاتينية في هامبدن - سيدني
                                كلية في فيرجينيا ، بحثت عن واحدة من أكثر الكلمات اللاتينية غموضًا ، consectetur ، من مقطع لوريم إيبسوم ،
                                ومن خلال استعراض اقتباسات الكلمة في الأدب الكلاسيكي ، اكتشف المصدر الذي لا شك فيه.
                                يأتي Lorem Ipsum من الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum'
                                (أقصى الخير والشر) بقلم شيشرون ، مكتوب عام 45 قبل الميلاد. هذا الكتاب عبارة عن أطروحة في نظرية الأخلاق ،
                                تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، 'Lorem ipsum dolor sit amet ..' ،
                                يأتي من سطر في القسم 1.10.32. تم نسخ الجزء القياسي من لوريم إيبسوم المستخدم منذ القرن الخامس عشر الميلادي
                                أدناه للمهتمين. الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum' بواسطة Cicero هي أيضًا
                                راكهام مستنسخة في شكلها الأصلي بالضبط ، مصحوبة بنسخ إنجليزية من ترجمة عام 1914 له.
                                هناك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم ، لكن الغالبية قد تعرضت للتغيير في شكل ما ،
                                عن طريق إدخال الدعابة أو الكلمات العشوائية التي لا تبدو قابلة للتصديق إلى حد ما. إذا كنت ستستخدم فقرة لوريم
                                إيبسوم ، عليك التأكد من عدم وجود أي شيء محرج مخفي في منتصف النص. جميع مولدات لوريم إيبسوم
                                على الإنترنت تميل إلى تكرار الأجزاء المحددة مسبقًا حسب الضرورة ، مما يجعل هذا أول مولد حقيقي على الإنترنت.
                                يستخدم قاموسًا يحتوي على أكثر من 200 كلمة لاتينية ، جنبًا إلى جنب مع حفنة من تراكيب الجملة النموذجية ، لإنشاء لوريم
                                Ipsum الذي يبدو معقولاً. لذلك ، فإن لوريم إيبسوم الذي تم إنشاؤه دائمًا ما يكون خاليًا من التكرار ، والفكاهة المحقونة ،
                                أو كلمات غير مميزة وما إلى ذلك.",
                'content_en' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                Aldus PageMaker including versions of Lorem Ipsum. t is a long established fact that a reader will 
                                be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum 
                                is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                                making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as 
                                their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. 
                                Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). 
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin 
                                literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                                and going through the cites of the word in classical literature, discovered the undoubtable source. 
                                Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' 
                                (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, 
                                very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', 
                                comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced 
                                below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also 
                                reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. 
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, 
                                by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem 
                                Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators 
                                on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. 
                                It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem 
                                Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, 
                                or non-characteristic words etc.",
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            // privacy and policy end

            // terms and conditions start - id = 25
            [
                'key' => 'terms_and_conditions',
                'parent_id' => null,
                'title_ar' => 'الشروط و الأحكام',
                'title_en' => 'Terms and Conditions',
                'content_ar' => "لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.
                                لوريم إيبسوم هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ،
                                عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لعمل كتاب عينة من النوع.
                                لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة في التنضيد الإلكتروني ،
                                يبقى دون تغيير جوهريا. تم نشره في الستينيات بإصدار Letraset
                                أوراق تحتوي على مقاطع لوريم إيبسوم ، ومؤخراً باستخدام برامج النشر المكتبي مثل
                                Aldus PageMaker بما في ذلك إصدارات لوريم إيبسوم. هناك حقيقة مثبتة منذ زمن طويل أن القارئ سيفعل ذلك
                                يصرف المحتوى المقروء لصفحة ما عند النظر إلى تخطيطها. الهدف من استخدام لوريم إيبسوم
                                أنه يحتوي على توزيع طبيعي -إلى حد ما- للأحرف ، بدلاً من استخدام'محتوى هنا ، يوجد محتوى هنا' ،
                                مما يجعلها تبدو وكأنها لغة إنجليزية قابلة للقراءة. تستخدم العديد من حزم النشر المكتبي ومحرري صفحات الويب الآن لوريم إيبسوم كملف
                                نص النموذج الافتراضي الخاص بهم ، وسيكشف البحث عن 'lorem ipsum' عن العديد من مواقع الويب التي لا تزال في مهدها.
                                تطورت إصدارات مختلفة على مر السنين ، أحيانًا عن طريق الصدفة ، وأحيانًا عن قصد (روح الدعابة المحقونة وما شابه ذلك).
                                خلافًا للاعتقاد الشائع ، فإن Lorem Ipsum ليس مجرد نص عشوائي. لها جذور في قطعة من الكلاسيكية اللاتينية
                                الأدب من 45 قبل الميلاد ، مما يجعلها أكثر من 2000 سنة. ريتشارد مكلينتوك ، أستاذ اللغة اللاتينية في هامبدن - سيدني
                                كلية في فيرجينيا ، بحثت عن واحدة من أكثر الكلمات اللاتينية غموضًا ، consectetur ، من مقطع لوريم إيبسوم ،
                                ومن خلال استعراض اقتباسات الكلمة في الأدب الكلاسيكي ، اكتشف المصدر الذي لا شك فيه.
                                يأتي Lorem Ipsum من الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum'
                                (أقصى الخير والشر) بقلم شيشرون ، مكتوب عام 45 قبل الميلاد. هذا الكتاب عبارة عن أطروحة في نظرية الأخلاق ،
                                تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، 'Lorem ipsum dolor sit amet ..' ،
                                يأتي من سطر في القسم 1.10.32. تم نسخ الجزء القياسي من لوريم إيبسوم المستخدم منذ القرن الخامس عشر الميلادي
                                أدناه للمهتمين. الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum' بواسطة Cicero هي أيضًا
                                راكهام مستنسخة في شكلها الأصلي بالضبط ، مصحوبة بنسخ إنجليزية من ترجمة عام 1914 له.
                                هناك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم ، لكن الغالبية قد تعرضت للتغيير في شكل ما ،
                                عن طريق إدخال الدعابة أو الكلمات العشوائية التي لا تبدو قابلة للتصديق إلى حد ما. إذا كنت ستستخدم فقرة لوريم
                                إيبسوم ، عليك التأكد من عدم وجود أي شيء محرج مخفي في منتصف النص. جميع مولدات لوريم إيبسوم
                                على الإنترنت تميل إلى تكرار الأجزاء المحددة مسبقًا حسب الضرورة ، مما يجعل هذا أول مولد حقيقي على الإنترنت.
                                يستخدم قاموسًا يحتوي على أكثر من 200 كلمة لاتينية ، جنبًا إلى جنب مع حفنة من تراكيب الجملة النموذجية ، لإنشاء لوريم
                                Ipsum الذي يبدو معقولاً. لذلك ، فإن لوريم إيبسوم الذي تم إنشاؤه دائمًا ما يكون خاليًا من التكرار ، والفكاهة المحقونة ،
                                أو كلمات غير مميزة وما إلى ذلك.",
                'content_en' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                Aldus PageMaker including versions of Lorem Ipsum. t is a long established fact that a reader will 
                                be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum 
                                is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                                making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as 
                                their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. 
                                Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). 
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin 
                                literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                                and going through the cites of the word in classical literature, discovered the undoubtable source. 
                                Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' 
                                (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, 
                                very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', 
                                comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced 
                                below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also 
                                reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. 
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, 
                                by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem 
                                Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators 
                                on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. 
                                It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem 
                                Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, 
                                or non-characteristic words etc.",
                'image' => null,
                'link' => null,
                'nation_id' => 2,
            ],
            // terms and conditions end



            /********************************************************************************************************** */

            // SA Version

            // about us strat - id = 26
            [
                'key' => 'about_us',
                'parent_id' => null,
                'title_ar' => 'من نحن',
                'title_en' => 'About Us',
                'content_ar' => 'ICH هي شركة ذات مسؤولية محدودة مرخصة برخصة رقم. (11077577) في المملكة المتحدة (بريطانيا)
                                لديها مكتب في برمنغهام ، ومتخصص في العمل على تقديم خدمات استشارية وتسويقية دولية
                                الهيئات ومؤسسات الإغاثة الإنسانية. في مساعدتها في الحصول على دعم مادي لتنفيذها
                                مشاريع وأعمال إغاثية إنسانية للمستفيدين في المناطق المحتاجة دون تمييز عرقي أو ديني.
                                تقدم المنصة مزايا عديدة للمستفيدين من أهمها خاصية الاسترجاع
                                الدعم المتكرر في الأوقات التي تختارها ، وإعلام المستخدم بأهم الأنشطة والعاجلة
                                الحملات الإنسانية مع التصنيف حسب الدول أو الأنواع ، مع تزويده بكافة المعلومات عنها
                                الجمعية أو المؤسسة التي يدعمها وكيفية التواصل معها.',
                'content_en' => 'ICH is a limited liability company licensed with license no. (11077577) in the Kingdom United ( Britain) 
                                has an office in Birmingham, and specializes in working to provide Advisory and marketing services for international 
                                bodies and humanitarian relief institutions. In helping her get material support for the implementation of her 
                                projects and relief work Humanity for beneficiaries in need areas without ethnic or religious discrimination.
                                The platform offers several advantages to the beneficiaries, the most important of which is the feature of recalling 
                                repeated support at the times of your choice, and informing the user of the most important activities and urgent 
                                humanitarian campaigns with classification by countries or types, while providing him with all the information about 
                                the association or institution to which he supports, and how to communicate with them.',
                'image' => asset('images/main/leaf.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            // about us end

            // about corporation start - id = 27
            [
                'key' => 'about_corporation',
                'parent_id' => null,
                'title_ar' => 'حول المؤسسة الدولية للخدمات الإنسانية',
                'title_en' => 'About International Corporation For Humanitarian',
                'content_ar' => 'ICH هي شركة ذات مسؤولية محدودة مرخصة برخصة رقم. (11077577) في المملكة المتحدة (بريطانيا) لديها مكتب
                                في برمنغهام ، وتتخصص في العمل على تقديم خدمات استشارية وتسويقية للهيئات الدولية والإنسانية
                                مؤسسات الإغاثة. في مساعدتها في الحصول على الدعم المادي لتنفيذ مشاريعها وأعمال الإغاثة الإنسانية من أجلها
                                المستفيدون في المناطق المحتاجة دون تمييز عرقي أو ديني.',
                'content_en' => 'ICH is a limited liability company licensed with license no. (11077577) in the Kingdom United ( Britain) has an office 
                                in Birmingham, and specializes in working to provide Advisory and marketing services for international bodies and humanitarian 
                                relief institutions. In helping her get material support for the implementation of her projects and relief work Humanity for 
                                beneficiaries in need areas without ethnic or religious discrimination.',
                'image' => asset('images/about/logo.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            // about corporation end

            // vision and mission start - id = 28
            [
                'key' => 'vision_and_mission',
                'parent_id' => null,
                'title_ar' => 'الرؤية و الرسالة',
                'title_en' => 'Vision and Mission',
                'content_ar' => null,
                'content_en' => null,
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'vision_and_mission',
                'parent_id' => 28,
                'title_en' => 'Vision',
                'title_ar' => 'الرؤية',
                'content_ar' => `<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">معاً</span>
                                    <br>
                                    <span style="letter-spacing: 1px; font-size:20px; margin: 10px auto 10px; display: block;">نحافظ على</span>
                                    <span style="letter-spacing: 1px; font-size:30px; font-family: 'bold'; text-transform: uppercase;">إنسانيتهم</span>

                                </p>`,
                'content_en' => `<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">Together</span>
                                    <br>
                                    <span style="letter-spacing: 1px; font-size:20px; margin: 10px auto 10px; display: block;">we preserve</span>
                                    <span style="letter-spacing: 1px; font-size:30px; font-family: 'bold'; text-transform: uppercase;">their humanity</span>

                                </p>`,
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'vision_and_mission',
                'parent_id' => 28,
                'title_en' => 'Mission',
                'title_ar' => 'مهمتنا',
                'content_ar' => `<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">منصة عالمية متخصصة </span>  تقدم مشاريع تطويرية ذات تأثير بأدوات <span style="color:#3DBECB;font-family: 'bold';">متابعة فعالة و مهنية عير شركات إستراتيجية عالية الجودة.</span>
                                </p>`,
                'content_en' => `<p style="color: #333333;">
                                    <span style="color:#fff; background-color: #3DBECB; padding:0 10px; display: inline-block; border-radius: 2px;">A specialized global platform </span> offering Development projects with impact in ways Effective and professional follow-up tools via <span style="color:#3DBECB;font-family: 'bold';">Quality strategic partnerships.</span>
                                </p>`,
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            // vision and mission end

            // out goal strat - id = 31
            [
                'key' => 'our_goal',
                'parent_id' => null,
                'title_ar' => 'هدفنا',
                'title_en' => 'Our Goal',
                'content_ar' => 'من نقطة انطلاق إنسانية بحتة ، بعيدًا عن اعتبارات الدين والعرق ،
                                اللون،
                                أو النطاق الجغرافي
                                من أجل الإنسانية ، ما يحدد نهجنا في المساعدة هو فقط
                                تحقيق، إنجاز
                                من احتياجات الضعفاء
                                والمنكوبة ، وتلامس اماكن المحتاجين دون تردد ، للمساهمة فيها
                                من خلال الوصول إلى المحتاجين
                                تسهيل الدعم للمشاريع التي تقوم بها إنسانية متخصصة
                                المؤسسات مثل
                                مؤسسة
                                استشاري وتنفيذي يساهم في دعم ودعم العمل الإنساني
                                المنظمات
                                لتحقيق أهدافهم.',
                'content_en' => 'From a purely human starting point, away from considerations of religion, race,
                                color,
                                or geographic scope
                                For the sake of humanity, what defines our approach on Help We is only the
                                fulfillment
                                of the needs of the vulnerable
                                And the afflicted, and touch the places of need without hesitation, to contribute to
                                reaching the needy through
                                Facilitating support for projects undertaken by specialized humanitarian
                                institutions as
                                an institution
                                Advisory and executive who contribute to support and support humanitarian
                                organizations
                                to achieve their goals.',
                'image' => asset('images/about/goal.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            // out goal end

            // our value start - id = 32
            [
                'key' => 'our_value',
                'parent_id' => null,
                'title_ar' => 'قيمتنا',
                'title_en' => 'Our Value',
                'content_ar' => null,
                'content_en' => null,
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 32,
                'title_ar' => 'الإستدامة',
                'title_en' => 'Sustainability',
                'content_ar' => 'نحن نعمل على استدامة أعمالنا واستدامة تأثيرها على المتلقي',
                'content_en' => 'We are working on Sustaining our business and the sustainability of its impact to the recipient',
                'image' => asset('images/about/icons/1.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 32,
                'title_ar' => 'المسؤولية',
                'title_en' => 'Responsibility',
                'content_ar' => 'نحن نلتزم بمسؤولياتنا تجاه أنفسنا والآخرين',
                'content_en' => 'We adhere to our responsibilities towards ourselves and others',
                'image' => asset('images/about/icons/2.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 32,
                'title_ar' => 'الثقة',
                'title_en' => 'Confidence',
                'content_ar' => 'نعمل بجد لخلق الثقة مع المستفيد',
                'content_en' => 'We work hard to create trust with beneficiary',
                'image' => asset('images/about/icons/3.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 32,
                'title_ar' => 'التنوع',
                'title_en' => 'Diversity',
                'content_ar' => 'نحن نعتز بالتنوع ونعتقد أنه إضافة مبتكرة لدينا ويعززها منافستنا في السوق',
                'content_en' => 'We cherish diversity and we believe he is an innovative addition we have and reinforces it our competition is in market',
                'image' => asset('images/about/icons/4.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 32,
                'title_ar' => 'التفان',
                'title_en' => 'Dedication',
                'content_ar' => 'نحن لا نقبل أنفسنا دون المستوى المطلوب ، فنحن نتحرى عن جودة جميع أعمالنا ، حيث إننا نعزز ثقافتها في البرامج والمشاريع التي نقدمها',
                'content_en' => 'We do not accept ourselves substandard we are investigating the quality all our business, as we are consolidating its culture in the programs and projects that we deliver',
                'image' => asset('images/about/icons/5.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'our_value',
                'parent_id' => 32,
                'title_ar' => 'التشارك',
                'title_en' => 'Participatory',
                'content_ar' => 'العلاقة التشاركية والتواصل مع الأطراف ذات العلاقة والاستثمار الأمثل لإمكانيات الطاقات وتكاملها',
                'content_en' => 'Participatory and communicate with related parties relationship and investment optimization of the potential and integration of energies',
                'image' => asset('images/about/icons/6.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            // our value end

            // our services start - id = 39
            [
                'key' => 'our_services',
                'parent_id' => null,
                'title_ar' => 'خدماتنا',
                'title_en' => 'Our Services',
                'content_ar' => 'الخدمات المقدمة من we help:',
                'content_en' => 'The services provided by we help:',
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'our_services',
                'parent_id' => 39,
                'title_ar' => '01',
                'title_en' => '01',
                'content_ar' => 'تسويق المشاريع الانسانية والخيرية والاغاثية وجمع الدعم المادي لها',
                'content_en' => 'Marketing humanitarian, charitable and relief projects and collecting material support for them',
                'image' => null,
                'link' => 'donate.html',
                'nation_id' => 1,
            ],
            [
                'key' => 'our_services',
                'parent_id' => 39,
                'title_ar' => '02',
                'title_en' => '02',
                'content_ar' => 'دعم إدارة المشاريع الإنسانية وجودة تنفيذها',
                'content_en' => 'Support in managing humanitarian projects and the quality of their implementation',
                'image' => null,
                'link' => 'donate.html',
                'nation_id' => 1,
            ],
            [
                'key' => 'our_services',
                'parent_id' => 39,
                'title_ar' => '03',
                'title_en' => '03',
                'content_ar' => 'تقديم تقارير دورية ودقيقة للداعمين',
                'content_en' => 'Provide periodic and accurate reports to the supporters',
                'image' => null,
                'link' => 'donate.html',
                'nation_id' => 1,
            ],
            // our services end

            // we help features start - id = 43
            [
                'key' => 'we_help_features',
                'parent_id' => null,
                'title_ar' => 'مميزات We Help',
                'title_en' => 'We Help Features',
                'content_ar' => 'تقدم المنصة العديد من الميزات للمستفيدين من أهمها تذكير الدعم المتكرر في
                                أوقات اختيارك ، يتم إعلام المستخدم بأبرز الأنشطة والحملات الإنسانية العاجلة مع
                                التصنيف حسب الدول أو الأنواع ، مع تزويده بكافة المعلومات عن الجمعية أو المؤسسة الخاصة بها
                                التي يقدمها ، وكيفية التواصل معها ، بالإضافة إلى:',
                'content_en' => 'The platform offers several features to beneficiaries, the most important of which is the recurring support reminder at 
                                times of your choosing, The user is informed of the most prominent urgent humanitarian activities and campaigns with a 
                                classification according to countries or types, with Provide him with all information about the association or institution for 
                                which he provides support, and how to communicate with it, in addition to:',
                'image' => asset('images/main/white-logo.png'),
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 43,
                'title_ar' => '01',
                'title_en' => '01',
                'content_ar' => 'تسويق المشاريع الانسانية والخيرية والاغاثية وجمع الدعم المادي لها',
                'content_en' => 'Marketing humanitarian, charitable and relief projects and collecting material support for them',
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 43,
                'title_ar' => '02',
                'title_en' => '02',
                'content_ar' => 'امكانية الدعم والسداد الكامل او الجزئي للمشاريع',
                'content_en' => 'The possibility of support and full or partial payment of the projects',
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 43,
                'title_ar' => '03',
                'title_en' => '03',
                'content_ar' => 'تذهب مبالغ الدعم مباشرة إلى حساب بنكي مخصص في بريطانيا ثم يتم تحويلها إلى حساب جمعيات حسب القواعد واللوائح المعتمدة حسب النظام',
                'content_en' => 'Support amounts go directly to a bank account Custom in Britain and is then converted into Account of associations according to approved rules and regulations Legal according to the system',
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            [
                'key' => 'we_help_features',
                'parent_id' => 43,
                'title_ar' => '04',
                'title_en' => '04',
                'content_ar' => 'منصة آمنة تسمح للجمعيات الخيرية الإنسانية الوحيدة المرخصة للانضمام إلى المنصة',
                'content_en' => 'A safe platform that allows charities The only licensed humanitarian to join the platform',
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            // we help features end

            // help center start - id = 48
            [
                'key' => 'help_center',
                'parent_id' => null,
                'title_ar' => 'مركز المساعدة',
                'title_en' => 'Help Center',
                'content_ar' => "لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.
                                لوريم إيبسوم هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ،
                                عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لعمل كتاب عينة من النوع.
                                لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة في التنضيد الإلكتروني ،
                                يبقى دون تغيير جوهريا. تم نشره في الستينيات بإصدار Letraset
                                أوراق تحتوي على مقاطع لوريم إيبسوم ، ومؤخراً باستخدام برامج النشر المكتبي مثل
                                Aldus PageMaker بما في ذلك إصدارات لوريم إيبسوم. هناك حقيقة مثبتة منذ زمن طويل أن القارئ سيفعل ذلك
                                يصرف المحتوى المقروء لصفحة ما عند النظر إلى تخطيطها. الهدف من استخدام لوريم إيبسوم
                                أنه يحتوي على توزيع طبيعي -إلى حد ما- للأحرف ، بدلاً من استخدام'محتوى هنا ، يوجد محتوى هنا' ،
                                مما يجعلها تبدو وكأنها لغة إنجليزية قابلة للقراءة. تستخدم العديد من حزم النشر المكتبي ومحرري صفحات الويب الآن لوريم إيبسوم كملف
                                نص النموذج الافتراضي الخاص بهم ، وسيكشف البحث عن 'lorem ipsum' عن العديد من مواقع الويب التي لا تزال في مهدها.
                                تطورت إصدارات مختلفة على مر السنين ، أحيانًا عن طريق الصدفة ، وأحيانًا عن قصد (روح الدعابة المحقونة وما شابه ذلك).
                                خلافًا للاعتقاد الشائع ، فإن Lorem Ipsum ليس مجرد نص عشوائي. لها جذور في قطعة من الكلاسيكية اللاتينية
                                الأدب من 45 قبل الميلاد ، مما يجعلها أكثر من 2000 سنة. ريتشارد مكلينتوك ، أستاذ اللغة اللاتينية في هامبدن - سيدني
                                كلية في فيرجينيا ، بحثت عن واحدة من أكثر الكلمات اللاتينية غموضًا ، consectetur ، من مقطع لوريم إيبسوم ،
                                ومن خلال استعراض اقتباسات الكلمة في الأدب الكلاسيكي ، اكتشف المصدر الذي لا شك فيه.
                                يأتي Lorem Ipsum من الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum'
                                (أقصى الخير والشر) بقلم شيشرون ، مكتوب عام 45 قبل الميلاد. هذا الكتاب عبارة عن أطروحة في نظرية الأخلاق ،
                                تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، 'Lorem ipsum dolor sit amet ..' ،
                                يأتي من سطر في القسم 1.10.32. تم نسخ الجزء القياسي من لوريم إيبسوم المستخدم منذ القرن الخامس عشر الميلادي
                                أدناه للمهتمين. الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum' بواسطة Cicero هي أيضًا
                                راكهام مستنسخة في شكلها الأصلي بالضبط ، مصحوبة بنسخ إنجليزية من ترجمة عام 1914 له.
                                هناك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم ، لكن الغالبية قد تعرضت للتغيير في شكل ما ،
                                عن طريق إدخال الدعابة أو الكلمات العشوائية التي لا تبدو قابلة للتصديق إلى حد ما. إذا كنت ستستخدم فقرة لوريم
                                إيبسوم ، عليك التأكد من عدم وجود أي شيء محرج مخفي في منتصف النص. جميع مولدات لوريم إيبسوم
                                على الإنترنت تميل إلى تكرار الأجزاء المحددة مسبقًا حسب الضرورة ، مما يجعل هذا أول مولد حقيقي على الإنترنت.
                                يستخدم قاموسًا يحتوي على أكثر من 200 كلمة لاتينية ، جنبًا إلى جنب مع حفنة من تراكيب الجملة النموذجية ، لإنشاء لوريم
                                Ipsum الذي يبدو معقولاً. لذلك ، فإن لوريم إيبسوم الذي تم إنشاؤه دائمًا ما يكون خاليًا من التكرار ، والفكاهة المحقونة ،
                                أو كلمات غير مميزة وما إلى ذلك.",
                'content_en' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                Aldus PageMaker including versions of Lorem Ipsum. t is a long established fact that a reader will 
                                be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum 
                                is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                                making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as 
                                their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. 
                                Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). 
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin 
                                literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                                and going through the cites of the word in classical literature, discovered the undoubtable source. 
                                Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' 
                                (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, 
                                very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', 
                                comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced 
                                below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also 
                                reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. 
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, 
                                by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem 
                                Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators 
                                on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. 
                                It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem 
                                Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, 
                                or non-characteristic words etc.",
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            // help center end

            // privacy and policy start - id = 49
            [
                'key' => 'privacy_policy',
                'parent_id' => null,
                'title_ar' => 'سياسة الخصوصية',
                'title_en' => 'Privacy Policy',
                'content_ar' => "لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.
                                لوريم إيبسوم هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ،
                                عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لعمل كتاب عينة من النوع.
                                لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة في التنضيد الإلكتروني ،
                                يبقى دون تغيير جوهريا. تم نشره في الستينيات بإصدار Letraset
                                أوراق تحتوي على مقاطع لوريم إيبسوم ، ومؤخراً باستخدام برامج النشر المكتبي مثل
                                Aldus PageMaker بما في ذلك إصدارات لوريم إيبسوم. هناك حقيقة مثبتة منذ زمن طويل أن القارئ سيفعل ذلك
                                يصرف المحتوى المقروء لصفحة ما عند النظر إلى تخطيطها. الهدف من استخدام لوريم إيبسوم
                                أنه يحتوي على توزيع طبيعي -إلى حد ما- للأحرف ، بدلاً من استخدام'محتوى هنا ، يوجد محتوى هنا' ،
                                مما يجعلها تبدو وكأنها لغة إنجليزية قابلة للقراءة. تستخدم العديد من حزم النشر المكتبي ومحرري صفحات الويب الآن لوريم إيبسوم كملف
                                نص النموذج الافتراضي الخاص بهم ، وسيكشف البحث عن 'lorem ipsum' عن العديد من مواقع الويب التي لا تزال في مهدها.
                                تطورت إصدارات مختلفة على مر السنين ، أحيانًا عن طريق الصدفة ، وأحيانًا عن قصد (روح الدعابة المحقونة وما شابه ذلك).
                                خلافًا للاعتقاد الشائع ، فإن Lorem Ipsum ليس مجرد نص عشوائي. لها جذور في قطعة من الكلاسيكية اللاتينية
                                الأدب من 45 قبل الميلاد ، مما يجعلها أكثر من 2000 سنة. ريتشارد مكلينتوك ، أستاذ اللغة اللاتينية في هامبدن - سيدني
                                كلية في فيرجينيا ، بحثت عن واحدة من أكثر الكلمات اللاتينية غموضًا ، consectetur ، من مقطع لوريم إيبسوم ،
                                ومن خلال استعراض اقتباسات الكلمة في الأدب الكلاسيكي ، اكتشف المصدر الذي لا شك فيه.
                                يأتي Lorem Ipsum من الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum'
                                (أقصى الخير والشر) بقلم شيشرون ، مكتوب عام 45 قبل الميلاد. هذا الكتاب عبارة عن أطروحة في نظرية الأخلاق ،
                                تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، 'Lorem ipsum dolor sit amet ..' ،
                                يأتي من سطر في القسم 1.10.32. تم نسخ الجزء القياسي من لوريم إيبسوم المستخدم منذ القرن الخامس عشر الميلادي
                                أدناه للمهتمين. الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum' بواسطة Cicero هي أيضًا
                                راكهام مستنسخة في شكلها الأصلي بالضبط ، مصحوبة بنسخ إنجليزية من ترجمة عام 1914 له.
                                هناك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم ، لكن الغالبية قد تعرضت للتغيير في شكل ما ،
                                عن طريق إدخال الدعابة أو الكلمات العشوائية التي لا تبدو قابلة للتصديق إلى حد ما. إذا كنت ستستخدم فقرة لوريم
                                إيبسوم ، عليك التأكد من عدم وجود أي شيء محرج مخفي في منتصف النص. جميع مولدات لوريم إيبسوم
                                على الإنترنت تميل إلى تكرار الأجزاء المحددة مسبقًا حسب الضرورة ، مما يجعل هذا أول مولد حقيقي على الإنترنت.
                                يستخدم قاموسًا يحتوي على أكثر من 200 كلمة لاتينية ، جنبًا إلى جنب مع حفنة من تراكيب الجملة النموذجية ، لإنشاء لوريم
                                Ipsum الذي يبدو معقولاً. لذلك ، فإن لوريم إيبسوم الذي تم إنشاؤه دائمًا ما يكون خاليًا من التكرار ، والفكاهة المحقونة ،
                                أو كلمات غير مميزة وما إلى ذلك.",
                'content_en' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                Aldus PageMaker including versions of Lorem Ipsum. t is a long established fact that a reader will 
                                be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum 
                                is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                                making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as 
                                their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. 
                                Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). 
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin 
                                literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                                and going through the cites of the word in classical literature, discovered the undoubtable source. 
                                Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' 
                                (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, 
                                very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', 
                                comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced 
                                below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also 
                                reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. 
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, 
                                by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem 
                                Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators 
                                on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. 
                                It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem 
                                Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, 
                                or non-characteristic words etc.",
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            // privacy and policy end

            // terms and conditions start - id = 50
            [
                'key' => 'terms_and_conditions',
                'parent_id' => null,
                'title_ar' => 'الشروط و الأحكام',
                'title_en' => 'Terms and Conditions',
                'content_ar' => "لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.
                                لوريم إيبسوم هو النص الوهمي القياسي في الصناعة منذ القرن الخامس عشر الميلادي ،
                                عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لعمل كتاب عينة من النوع.
                                لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة في التنضيد الإلكتروني ،
                                يبقى دون تغيير جوهريا. تم نشره في الستينيات بإصدار Letraset
                                أوراق تحتوي على مقاطع لوريم إيبسوم ، ومؤخراً باستخدام برامج النشر المكتبي مثل
                                Aldus PageMaker بما في ذلك إصدارات لوريم إيبسوم. هناك حقيقة مثبتة منذ زمن طويل أن القارئ سيفعل ذلك
                                يصرف المحتوى المقروء لصفحة ما عند النظر إلى تخطيطها. الهدف من استخدام لوريم إيبسوم
                                أنه يحتوي على توزيع طبيعي -إلى حد ما- للأحرف ، بدلاً من استخدام'محتوى هنا ، يوجد محتوى هنا' ،
                                مما يجعلها تبدو وكأنها لغة إنجليزية قابلة للقراءة. تستخدم العديد من حزم النشر المكتبي ومحرري صفحات الويب الآن لوريم إيبسوم كملف
                                نص النموذج الافتراضي الخاص بهم ، وسيكشف البحث عن 'lorem ipsum' عن العديد من مواقع الويب التي لا تزال في مهدها.
                                تطورت إصدارات مختلفة على مر السنين ، أحيانًا عن طريق الصدفة ، وأحيانًا عن قصد (روح الدعابة المحقونة وما شابه ذلك).
                                خلافًا للاعتقاد الشائع ، فإن Lorem Ipsum ليس مجرد نص عشوائي. لها جذور في قطعة من الكلاسيكية اللاتينية
                                الأدب من 45 قبل الميلاد ، مما يجعلها أكثر من 2000 سنة. ريتشارد مكلينتوك ، أستاذ اللغة اللاتينية في هامبدن - سيدني
                                كلية في فيرجينيا ، بحثت عن واحدة من أكثر الكلمات اللاتينية غموضًا ، consectetur ، من مقطع لوريم إيبسوم ،
                                ومن خلال استعراض اقتباسات الكلمة في الأدب الكلاسيكي ، اكتشف المصدر الذي لا شك فيه.
                                يأتي Lorem Ipsum من الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum'
                                (أقصى الخير والشر) بقلم شيشرون ، مكتوب عام 45 قبل الميلاد. هذا الكتاب عبارة عن أطروحة في نظرية الأخلاق ،
                                تحظى بشعبية كبيرة خلال عصر النهضة. السطر الأول من Lorem Ipsum ، 'Lorem ipsum dolor sit amet ..' ،
                                يأتي من سطر في القسم 1.10.32. تم نسخ الجزء القياسي من لوريم إيبسوم المستخدم منذ القرن الخامس عشر الميلادي
                                أدناه للمهتمين. الأقسام 1.10.32 و 1.10.33 من 'de Finibus Bonorum et Malorum' بواسطة Cicero هي أيضًا
                                راكهام مستنسخة في شكلها الأصلي بالضبط ، مصحوبة بنسخ إنجليزية من ترجمة عام 1914 له.
                                هناك العديد من الأنواع المتوفرة لنصوص لوريم إيبسوم ، لكن الغالبية قد تعرضت للتغيير في شكل ما ،
                                عن طريق إدخال الدعابة أو الكلمات العشوائية التي لا تبدو قابلة للتصديق إلى حد ما. إذا كنت ستستخدم فقرة لوريم
                                إيبسوم ، عليك التأكد من عدم وجود أي شيء محرج مخفي في منتصف النص. جميع مولدات لوريم إيبسوم
                                على الإنترنت تميل إلى تكرار الأجزاء المحددة مسبقًا حسب الضرورة ، مما يجعل هذا أول مولد حقيقي على الإنترنت.
                                يستخدم قاموسًا يحتوي على أكثر من 200 كلمة لاتينية ، جنبًا إلى جنب مع حفنة من تراكيب الجملة النموذجية ، لإنشاء لوريم
                                Ipsum الذي يبدو معقولاً. لذلك ، فإن لوريم إيبسوم الذي تم إنشاؤه دائمًا ما يكون خاليًا من التكرار ، والفكاهة المحقونة ،
                                أو كلمات غير مميزة وما إلى ذلك.",
                'content_en' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                                It has survived not only five centuries, but also the leap into electronic typesetting, 
                                remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset 
                                sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like 
                                Aldus PageMaker including versions of Lorem Ipsum. t is a long established fact that a reader will 
                                be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum 
                                is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', 
                                making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as 
                                their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. 
                                Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). 
                                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin 
                                literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney 
                                College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, 
                                and going through the cites of the word in classical literature, discovered the undoubtable source. 
                                Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' 
                                (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, 
                                very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', 
                                comes from a line in section 1.10.32. The standard chunk of Lorem Ipsum used since the 1500s is reproduced 
                                below for those interested. Sections 1.10.32 and 1.10.33 from 'de Finibus Bonorum et Malorum' by Cicero are also 
                                reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham. 
                                There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, 
                                by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem 
                                Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators 
                                on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. 
                                It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem 
                                Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, 
                                or non-characteristic words etc.",
                'image' => null,
                'link' => null,
                'nation_id' => 1,
            ],
            // terms and conditions end

            [

                'key' => 'supprot_contract',
                'parent_id' => null,
                'title_ar' => 'عقد الداعم',
                'title_en' => 'Supprot Contract',
                'content_ar' => '	يخضع كل مستخدم لمنصة we help لبنود وشروط الاستخدام العامة، ويُعد إقرارك على هذه الشروط العامة موافقة دون أي قيد أو شرط على بنود وشروط الاستخدام، سواء أكنت مستخدماً مسجلاً أم لم تكن، وتسري هذه الموافقة اعتباراً من تاريخ أول استخدام لك لهذه المنصة.
                	منصة We help تعمل كوسيط مقابل عمولة من المشاريع التي بين المؤسسات الإنسانية والراغبين بدعم مشاريعها، وتقدم عدة خدمات منها: الرقابة على تنفيذ المشاريع، والتسويق الرقمي، وتقدم هذه الخدمات مقابل نسبة أو عموله من مبلغ الدعم . 
                	يقر الداعم بأن أي معلومات أو بيانات يقوم بتسجيلها أو إضافتها هي تحت مسؤوليته، وللمنصة الحق بمطالبته بأي أضرار أو نتائج تنتج جراء عدم صحة المعلومات.
                	تنحصر مسؤولية المنصة في الوساطة باستلام قيمة المشروع، ودفعه للمؤسسة المنفذة والرقابة على تنفيذ المشاريع وتزويد الداعم بالتقارير والتحديثات حول المشروع وبذلك لا تكون مسؤولة عن أي أخطاء أو تقصير يقوم به الداعم أو الجمعية خارج النطاق المحدد آنفاً.
                	جميع المعلومات في هذا الموقع محمية بحقوق النشر ولا يجوز إعادة انتاجها أو ملكيتها بأي صورة دون موافقة إدارة المنصة، ويعتبر أي تعديل على المواد أو استخدامها لأغراض أخرى انتهاكاً لحقوق الملكية والنشر.
                	يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والقوانين الخاصة باستخدام بالمنصة.
                	تتيح المنصة الكشف عن المبلغ المستحق وآلية سداد المشروع، وفي حالة الاتفاق على السداد المجزأ يتعهد الداعم بأن يقوم بسداد المبلغ المتبقي في الوقت المحدد للسداد.
                	تقدم المنصة خدمة اختيار المشروع، ويحق له استرداد مبلغ الدعم خلال (48) ساعه من تاريخ الدعم، وفي حالة تم طلب الاسترداد بعد مضي (48) ساعه فإنه يمكن إعادة المبلغ بعد خصم النسبة المخصصة للمنصة خلال 7 أيام من تاريخ طلب الاسترداد . كما أنه لا يحق للداعم الرجوع عن الدعم بعد مضي 7 أيام من تاريخ الدعم.
                	الخدمات الإلكترونية التي تقدمها المنصة على الإنترنت، تقدم فقط لتسهيل الإجراءات اليدوية؛ وبهذا تقر بعلمك الكامل بأن الاتصالات عبر شبكة الإنترنت عرضة للأعطال أو للتدخل والاعتراض بواسطة الغير، فإن اللجوء إلى هذه المنصة يظل تحت مسؤوليتك الخاصة. 
                	إن عدم كتابة البيانات والمعلومات الصحيحة للتواصل تفقد الداعم الحق بالحصول على أي تقرير أو تحديث أو مطالبات عن الخدمة والمنتج المدعوم. 
                	يقر الداعم بموافقته للمنصة بتغيير موقع المشروع أو الخدمة في الحالات الطارئة أو الحروب أو الكوارث الطبيعية على حسب المبلغ المتوفر للدعم، أو في حالة الكفالة فإن الداعم يفوض المنصة بتغيير المكفول بالبديل الأنسب بما يتوافق مع مواصفات المكفول الأول .
                	يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة المرعية وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والأحكام الخاصة باستخدام بالمنصة.
                	الداعم والمنصة بالحفاظ على سرية المعلومات بموجب هذه الاتفاقية لأي جهة.
                	يقر الداعم بأنه قد قرأ بنود هذه الاتفاقية وتفهمها فهماً نافياً للجهالة، واتفق على الالتزام ببنود هذه الاتفاقية والشروط والأحكام الواردة فيها.
                	تخضع هذه الاتفاقية للأنظمة المعمول بها في المملكة المتحدة ويجري تفسيره وتنفيذه والفصل فيما ينشأ من دعاوى أو نزاعات وفقاً لهذه الأنظمة.
                	بمجرد الضغط على موافق سيكون الداعم ملزماً بإتباع الشروط والأحكام.
                ',
                'content_en' => ' Each user of the platform, we help is subject to the general terms and conditions of use, looping in paragraphs, general terms, general terms, provided that you agree to this banner.
                Platform platform We help to work as a mediator in return for a commission from projects that work between humanitarian institutions and those who wish their projects, and provide services, including: project implementation, digital, and these services are provided in exchange for a percentage or commission of support.
                Supporting operations to support it with information or operations to support it.
                You are implementing the project
                Copyright, Distribution and Publication.
                 The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and laws of using the platform.
                 The fee deriving from the amount owed, the project, and in the event of an agreement on payment, the sponsor undertakes to pay the specified amount of reimbursement.
                 The platform provides a project selection service, and he has the right to recover the amount of support within (48) hours from the date of support, and in the case of a request for refund after the lapse of (48) and you have the right to return to support after 7 days from the date of support.
                 electronic services that give the platform on the Internet, to facilitate procedures; In this regard, the Internet connection over the Internet for malfunctions or for interference and interception by third parties.
                 Not writing correct data and information for correct communication to obtain a report, update, update or claims about service and supported product.
                 The sponsor acknowledges Mowafa for the surrounding area, the location of the project or service in emergency cases, wars or natural disasters, according to the amount available for support, or in the case of sponsorship, the guarantee delegates the guaranteed logic to the most appropriate alternative in accordance with the specifications of the first full.
                 The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, applicable regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and conditions for using the platform.
                 The Sponsor and Platform shall maintain the confidentiality of this information for any party.
                defer, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter
                 This page, refers to another, in the regulations in force in the United Kingdom, its interpretation, implementation, separation and linking to each other.
                 Click on Agree, the sponsor will be obligated to follow the terms and conditions.',
                'image' => null,
                'link' => null,
                'nation_id' => 1,

            ],

            [

                'key' => 'supprot_contract',
                'parent_id' => null,
                'title_ar' => 'عقد الداعم',
                'title_en' => 'Supprot Contract',
                'content_ar' => ' يخضع لكل مستخدم لمنصة ، نحن نساعد لبنود وشروط الاستخدام العامة ، يحلق في فقرات ، شروط عامة ، شروط عامة ، شرط أن تكون موافقًا على هذا الشعار.
                منصة منصة نحن نساعد على العمل كوسيط مقابل عمولة من المشاريع التي تعمل بين المؤسسات الإنسانية والراغبين مشاريعها ، وتقدم خدمات منها: تنفيذ المشاريع ، الرقمية الرقمية ، وتقدم هذه الخدمات مقابل نسبة أو عموله من الدعم.
                تدعيم عمليات تدعيمها بمعلومات أو عمليات تدعيمها.
                تقوم بتنفيذ المشروع
                حقوق النشر والتوزيع والنشر.
                 يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والقوانين الخاصة باستخدام بالمنصة.
                 الرسم المنبثق عن المبلغ المستحق ، المشروع ، وفي حالة الاتفاق على السداد ، يتعهد الداعم بأن يقوم بتسديد المبلغ المحدد للسداد.
                 تقدم المنصة خدمة اختيار المشروع ، ويحق له استرداد مبلغ الدعم خلال (48) ساعه من تاريخ الدعم ، وفي حالة تم طلب الاسترداد بعد مضي (48) كما أنه يحق لك الرجوع عن الدعم بعد مضي 7 أيام من تاريخ الدعم.
                 الخدمات الإلكترونية التي تمنح المنصة على الإنترنت ، لتسهيل الإجراءات ؛ وبهذا الصدد ، فإن الاتصال بشبكة الإنترنت عبر الإنترنت للأعطال أو للتدخل والاعتراض بواسطة الغير.
                 عدم كتابة البيانات والمعلومات الصحيحة للتواصل الصحيح للحصول على تقرير أو تحديث أو تحديث أو مطالبات عن الخدمة والمنتج المدعوم.
                 يقر الداعم بموافٍ للمنطقة المحيطة بها ، موقع المشروع أو الخدمة في الحالات الطارئة أو الحروب أو الكوارث الطبيعية على حسب المبلغ المتوفر للدعم ، أو في حالة الكفالة ، فإن الكفالة يفوض المنطق المكفول بالبديل الأنسب بما يتوافق مع مواصفاتفول الأول.
                 يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة المرعية وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والأحكام الخاصة باستخدام بالمنصة.
                 الداعم والمنصة بالحفاظ على سرية المعلومات هذه لأي جهة.
                أرجأ ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة
                 هذه الصفحة ، تشير إلى أخرى ، في أنظمة المعمول بها في المملكة المتحدة ، وتفسيره وتنفيذه والفصل بينها وربطها ببعضها البعض.
                 اضغط على موافق ، سيكون الداعم ملزماً بإتباع الشروط والأحكام.',
                'content_en' => ' Each user of the platform, we help is subject to the general terms and conditions of use, looping in paragraphs, general terms, general terms, provided that you agree to this banner.
                Platform platform We help to work as a mediator in return for a commission from projects that work between humanitarian institutions and those who wish their projects, and provide services, including: project implementation, digital, and these services are provided in exchange for a percentage or commission of support.
                Supporting operations to support it with information or operations to support it.
                You are implementing the project
                Copyright, Distribution and Publication.
                 The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and laws of using the platform.
                 The fee deriving from the amount owed, the project, and in the event of an agreement on payment, the sponsor undertakes to pay the specified amount of reimbursement.
                 The platform provides a project selection service, and he has the right to recover the amount of support within (48) hours from the date of support, and in the case of a request for refund after the lapse of (48) and you have the right to return to support after 7 days from the date of support.
                 electronic services that give the platform on the Internet, to facilitate procedures; In this regard, the Internet connection over the Internet for malfunctions or for interference and interception by third parties.
                 Not writing correct data and information for correct communication to obtain a report, update, update or claims about service and supported product.
                 The sponsor acknowledges Mowafa for the surrounding area, the location of the project or service in emergency cases, wars or natural disasters, according to the amount available for support, or in the case of sponsorship, the guarantee delegates the guaranteed logic to the most appropriate alternative in accordance with the specifications of the first full.
                 The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, applicable regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and conditions for using the platform.
                 The Sponsor and Platform shall maintain the confidentiality of this information for any party.
                defer, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter
                 This page, refers to another, in the regulations in force in the United Kingdom, its interpretation, implementation, separation and linking to each other.
                 Click on Agree, the sponsor will be obligated to follow the terms and conditions.',
                'image' => null,
                'link' => null,
                'nation_id' => 2,

            ],
            
            [

                'key' => 'technical_support',
                'parent_id' => null,
                'title_ar' => 'الدعم الفنى',
                'title_en' => 'Technical Supprot',
                'content_ar' => '	يخضع كل مستخدم لمنصة we help لبنود وشروط الاستخدام العامة، ويُعد إقرارك على هذه الشروط العامة موافقة دون أي قيد أو شرط على بنود وشروط الاستخدام، سواء أكنت مستخدماً مسجلاً أم لم تكن، وتسري هذه الموافقة اعتباراً من تاريخ أول استخدام لك لهذه المنصة.
                	منصة We help تعمل كوسيط مقابل عمولة من المشاريع التي بين المؤسسات الإنسانية والراغبين بدعم مشاريعها، وتقدم عدة خدمات منها: الرقابة على تنفيذ المشاريع، والتسويق الرقمي، وتقدم هذه الخدمات مقابل نسبة أو عموله من مبلغ الدعم . 
                	يقر الداعم بأن أي معلومات أو بيانات يقوم بتسجيلها أو إضافتها هي تحت مسؤوليته، وللمنصة الحق بمطالبته بأي أضرار أو نتائج تنتج جراء عدم صحة المعلومات.
                	تنحصر مسؤولية المنصة في الوساطة باستلام قيمة المشروع، ودفعه للمؤسسة المنفذة والرقابة على تنفيذ المشاريع وتزويد الداعم بالتقارير والتحديثات حول المشروع وبذلك لا تكون مسؤولة عن أي أخطاء أو تقصير يقوم به الداعم أو الجمعية خارج النطاق المحدد آنفاً.
                	جميع المعلومات في هذا الموقع محمية بحقوق النشر ولا يجوز إعادة انتاجها أو ملكيتها بأي صورة دون موافقة إدارة المنصة، ويعتبر أي تعديل على المواد أو استخدامها لأغراض أخرى انتهاكاً لحقوق الملكية والنشر.
                	يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والقوانين الخاصة باستخدام بالمنصة.
                	تتيح المنصة الكشف عن المبلغ المستحق وآلية سداد المشروع، وفي حالة الاتفاق على السداد المجزأ يتعهد الداعم بأن يقوم بسداد المبلغ المتبقي في الوقت المحدد للسداد.
                	تقدم المنصة خدمة اختيار المشروع، ويحق له استرداد مبلغ الدعم خلال (48) ساعه من تاريخ الدعم، وفي حالة تم طلب الاسترداد بعد مضي (48) ساعه فإنه يمكن إعادة المبلغ بعد خصم النسبة المخصصة للمنصة خلال 7 أيام من تاريخ طلب الاسترداد . كما أنه لا يحق للداعم الرجوع عن الدعم بعد مضي 7 أيام من تاريخ الدعم.
                	الخدمات الإلكترونية التي تقدمها المنصة على الإنترنت، تقدم فقط لتسهيل الإجراءات اليدوية؛ وبهذا تقر بعلمك الكامل بأن الاتصالات عبر شبكة الإنترنت عرضة للأعطال أو للتدخل والاعتراض بواسطة الغير، فإن اللجوء إلى هذه المنصة يظل تحت مسؤوليتك الخاصة. 
                	إن عدم كتابة البيانات والمعلومات الصحيحة للتواصل تفقد الداعم الحق بالحصول على أي تقرير أو تحديث أو مطالبات عن الخدمة والمنتج المدعوم. 
                	يقر الداعم بموافقته للمنصة بتغيير موقع المشروع أو الخدمة في الحالات الطارئة أو الحروب أو الكوارث الطبيعية على حسب المبلغ المتوفر للدعم، أو في حالة الكفالة فإن الداعم يفوض المنصة بتغيير المكفول بالبديل الأنسب بما يتوافق مع مواصفات المكفول الأول .
                	يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة المرعية وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والأحكام الخاصة باستخدام بالمنصة.
                	الداعم والمنصة بالحفاظ على سرية المعلومات بموجب هذه الاتفاقية لأي جهة.
                	يقر الداعم بأنه قد قرأ بنود هذه الاتفاقية وتفهمها فهماً نافياً للجهالة، واتفق على الالتزام ببنود هذه الاتفاقية والشروط والأحكام الواردة فيها.
                	تخضع هذه الاتفاقية للأنظمة المعمول بها في المملكة المتحدة ويجري تفسيره وتنفيذه والفصل فيما ينشأ من دعاوى أو نزاعات وفقاً لهذه الأنظمة.
                	بمجرد الضغط على موافق سيكون الداعم ملزماً بإتباع الشروط والأحكام.
                ',
                'content_en' => ' Each user of the platform, we help is subject to the general terms and conditions of use, looping in paragraphs, general terms, general terms, provided that you agree to this banner.
                Platform platform We help to work as a mediator in return for a commission from projects that work between humanitarian institutions and those who wish their projects, and provide services, including: project implementation, digital, and these services are provided in exchange for a percentage or commission of support.
                Supporting operations to support it with information or operations to support it.
                You are implementing the project
                Copyright, Distribution and Publication.
                 The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and laws of using the platform.
                 The fee deriving from the amount owed, the project, and in the event of an agreement on payment, the sponsor undertakes to pay the specified amount of reimbursement.
                 The platform provides a project selection service, and he has the right to recover the amount of support within (48) hours from the date of support, and in the case of a request for refund after the lapse of (48) and you have the right to return to support after 7 days from the date of support.
                 electronic services that give the platform on the Internet, to facilitate procedures; In this regard, the Internet connection over the Internet for malfunctions or for interference and interception by third parties.
                 Not writing correct data and information for correct communication to obtain a report, update, update or claims about service and supported product.
                 The sponsor acknowledges Mowafa for the surrounding area, the location of the project or service in emergency cases, wars or natural disasters, according to the amount available for support, or in the case of sponsorship, the guarantee delegates the guaranteed logic to the most appropriate alternative in accordance with the specifications of the first full.
                 The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, applicable regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and conditions for using the platform.
                 The Sponsor and Platform shall maintain the confidentiality of this information for any party.
                defer, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter
                 This page, refers to another, in the regulations in force in the United Kingdom, its interpretation, implementation, separation and linking to each other.
                 Click on Agree, the sponsor will be obligated to follow the terms and conditions.',
                'image' => null,
                'link' => null,
                'nation_id' => 1,

            ],

            [

                'key' => 'technical_support',
                'parent_id' => null,
                'title_ar' => 'الدعم الفنى',
                'title_en' => 'Technical Supprot',
                'content_ar' => ' يخضع لكل مستخدم لمنصة ، نحن نساعد لبنود وشروط الاستخدام العامة ، يحلق في فقرات ، شروط عامة ، شروط عامة ، شرط أن تكون موافقًا على هذا الشعار.
                منصة منصة نحن نساعد على العمل كوسيط مقابل عمولة من المشاريع التي تعمل بين المؤسسات الإنسانية والراغبين مشاريعها ، وتقدم خدمات منها: تنفيذ المشاريع ، الرقمية الرقمية ، وتقدم هذه الخدمات مقابل نسبة أو عموله من الدعم.
                تدعيم عمليات تدعيمها بمعلومات أو عمليات تدعيمها.
                تقوم بتنفيذ المشروع
                حقوق النشر والتوزيع والنشر.
                يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والقوانين الخاصة باستخدام بالمنصة.
                الرسم المنبثق عن المبلغ المستحق ، المشروع ، وفي حالة الاتفاق على السداد ، يتعهد الداعم بأن يقوم بتسديد المبلغ المحدد للسداد.
                تقدم المنصة خدمة اختيار المشروع ، ويحق له استرداد مبلغ الدعم خلال (48) ساعه من تاريخ الدعم ، وفي حالة تم طلب الاسترداد بعد مضي (48) كما أنه يحق لك الرجوع عن الدعم بعد مضي 7 أيام من تاريخ الدعم.
                الخدمات الإلكترونية التي تمنح المنصة على الإنترنت ، لتسهيل الإجراءات ؛ وبهذا الصدد ، فإن الاتصال بشبكة الإنترنت عبر الإنترنت للأعطال أو للتدخل والاعتراض بواسطة الغير.
                عدم كتابة البيانات والمعلومات الصحيحة للتواصل الصحيح للحصول على تقرير أو تحديث أو تحديث أو مطالبات عن الخدمة والمنتج المدعوم.
                يقر الداعم بموافٍ للمنطقة المحيطة بها ، موقع المشروع أو الخدمة في الحالات الطارئة أو الحروب أو الكوارث الطبيعية على حسب المبلغ المتوفر للدعم ، أو في حالة الكفالة ، فإن الكفالة يفوض المنطق المكفول بالبديل الأنسب بما يتوافق مع مواصفاتفول الأول.
                يقر الداعم بأنه على علم من خلال المعلومات الموضحة عبر المنصة بالقواعد الأساسية والأنظمة المرعية وسياسات الشراء والدعم المنصوص عليها فيما يتعلق بالخصائص الأساسية للخدمة، وقيمتها، وطريقة الدفع، وشروط التنفيذ، كما يقر بأنه اطلع على الشروط والأحكام الخاصة باستخدام بالمنصة.
                الداعم والمنصة بالحفاظ على سرية المعلومات هذه لأي جهة.
                أرجأ ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة ، فاتحة
                هذه الصفحة ، تشير إلى أخرى ، في أنظمة المعمول بها في المملكة المتحدة ، وتفسيره وتنفيذه والفصل بينها وربطها ببعضها البعض.
                اضغط على موافق ، سيكون الداعم ملزماً بإتباع الشروط والأحكام.',
                'content_en' => ' Each user of the platform, we help is subject to the general terms and conditions of use, looping in paragraphs, general terms, general terms, provided that you agree to this banner.
                Platform platform We help to work as a mediator in return for a commission from projects that work between humanitarian institutions and those who wish their projects, and provide services, including: project implementation, digital, and these services are provided in exchange for a percentage or commission of support.
                Supporting operations to support it with information or operations to support it.
                You are implementing the project
                Copyright, Distribution and Publication.
                The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and laws of using the platform.
                The fee deriving from the amount owed, the project, and in the event of an agreement on payment, the sponsor undertakes to pay the specified amount of reimbursement.
                The platform provides a project selection service, and he has the right to recover the amount of support within (48) hours from the date of support, and in the case of a request for refund after the lapse of (48) and you have the right to return to support after 7 days from the date of support.
                electronic services that give the platform on the Internet, to facilitate procedures; In this regard, the Internet connection over the Internet for malfunctions or for interference and interception by third parties.
                Not writing correct data and information for correct communication to obtain a report, update, update or claims about service and supported product.
                The sponsor acknowledges Mowafa for the surrounding area, the location of the project or service in emergency cases, wars or natural disasters, according to the amount available for support, or in the case of sponsorship, the guarantee delegates the guaranteed logic to the most appropriate alternative in accordance with the specifications of the first full.
                The Supporter acknowledges that, through the information shown on the platform, he is aware of the basic rules, applicable regulations, purchase and support policies stipulated regarding the basic features of the service, its value, payment method, and terms of implementation, and also acknowledges that he has read the terms and conditions for using the platform.
                The Sponsor and Platform shall maintain the confidentiality of this information for any party.
                defer, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter, lighter
                This page, refers to another, in the regulations in force in the United Kingdom, its interpretation, implementation, separation and linking to each other.
                Click on Agree, the sponsor will be obligated to follow the terms and conditions.',
                'image' => null,
                'link' => null,
                'nation_id' => 2,

            ], 

            // registeration requirements

            [

                'key' => 'charity_contract',
                'parent_id' => null,
                'title_ar' => 'عقد الجمعية',
                'title_en' => 'charity Contract',
                'content_ar' => 'شروط تتوفر في الجمعية 
                أن تكون حاصلة على ترخيص رسمي من جهة الاختصاص في بلدها المزاولة نشاطها.
                ان يكون لها نظام داخلي ومجلس إدارة منتخب لا يقل عدد أعضائه عن خمسة اشخاص وإدارة تنفيذية محددة الأشخاص والمناصب.
                ان يكون لديها نظام مالي منضبط وفق الأصول المهنية وحساب بنكي مسجل باسمها وبصفتها جمعية ( غير ربحية )
                ان يكون لها نشاط ملموس وحقيقي وخطط عمل واضحة 
                ان يكون لديها حد أدنى لتمويل في تسير اعمالها الرئيسية.
                ان يكون قد مضى على انشائها ( 5 سنوات أو 3 سنوات)على الأقل
                ان يكون لها مقر أو عنوان محدد ويمكن الوصول اليها 
                الاتكون مدرجة في قائمة تصنيف معتبرة .
                ',
                'content_en' => 'Conditions available in the association
                To have an official license from the competent authority in the country in which to carry out its activity.
                It shall have an internal system, an elected board of directors, with no less than five members, and an executive administration that defines people and positions.
                That it has a disciplined financial system in accordance with professional principles and a bank account registered in its name and in its capacity as a (non-profit) association.
                To have a tangible and real activity and clear action plans
                That it have a minimum amount of financing in the conduct of its main business.
                It must have been established (5 or 3 years) at least
                It must have a specific headquarters or address and can be reached
                Not to be included in a valid classification list.',
                'image' => null,
                'link' => null,
                'nation_id' => 1,

            ], 
            
            [

                'key' => 'charity_contract',
                'parent_id' => null,
                'title_ar' => 'عقد الجمعية',
                'title_en' => 'charity Contract',
                'content_ar' => 'شروط تتوفر في الجمعية 
                أن تكون حاصلة على ترخيص رسمي من جهة الاختصاص في بلدها المزاولة نشاطها.
                ان يكون لها نظام داخلي ومجلس إدارة منتخب لا يقل عدد أعضائه عن خمسة اشخاص وإدارة تنفيذية محددة الأشخاص والمناصب.
                ان يكون لديها نظام مالي منضبط وفق الأصول المهنية وحساب بنكي مسجل باسمها وبصفتها جمعية ( غير ربحية )
                ان يكون لها نشاط ملموس وحقيقي وخطط عمل واضحة 
                ان يكون لديها حد أدنى لتمويل في تسير اعمالها الرئيسية.
                ان يكون قد مضى على انشائها ( 5 سنوات أو 3 سنوات)على الأقل
                ان يكون لها مقر أو عنوان محدد ويمكن الوصول اليها 
                الاتكون مدرجة في قائمة تصنيف معتبرة .
                ',
                'content_en' => 'Conditions available in the association
                To have an official license from the competent authority in the country in which to carry out its activity.
                It shall have an internal system, an elected board of directors, with no less than five members, and an executive administration that defines people and positions.
                That it has a disciplined financial system in accordance with professional principles and a bank account registered in its name and in its capacity as a (non-profit) association.
                To have a tangible and real activity and clear action plans
                That it have a minimum amount of financing in the conduct of its main business.
                It must have been established (5 or 3 years) at least
                It must have a specific headquarters or address and can be reached
                Not to be included in a valid classification list.',
                'image' => null,
                'link' => null,
                'nation_id' => 2,

            ],             
            
            
            
        ]);


        // FAQ
        DB::table('questions')->insert([

            // UK
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 2
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 2
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 2
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 2
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 2
            ],

            // SA
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 1
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 1
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 1
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 1
            ],
            [
                'question_ar' => 'هناك العديد من الأشكال المتاحة لنصوص لوريم إيبسوم ، لكن الغالبية تعرضت للتغيير بشكل ما ، عن طريق إدخال الدعابة؟',
                'question_en' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour ?',
                'answer_ar' => 'لوريم إيبسوم هو ببساطة نص شكلي يستخدم في صناعة الطباعة والتنضيد.',
                'answer_en' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                'nation_id' => 1
            ],



        ]);
    }
}
