<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class SettingUKSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([

            // اسم المنظمه عربى + انجليبزى لبريطانيا
            [
                'key' => 'org_name_uk_ar',
                'neckname' => 'admin.org_name_ar',
                'type' => 'text',
                'value' => 'وى هيلب',
                'nation_id' => 2,
            ],
            [
                'key' => 'org_name_uk_en',
                'neckname' => 'admin.org_name_en',
                'type' => 'text',
                'value' => 'We help',
                'nation_id' => 2,
            ],

            // رقم التسجيل لبريطانيا
            [
                'key' => 'license_number_uk',
                'neckname' => 'admin.license_number',
                'type' => 'text',
                'value' => '123456',
                'nation_id' => 2,
            ],

            // ممثل المنظمه لبريطانيا 
            [
                'key' => 'representer_name_uk_ar',
                'neckname' => 'admin.representer_name_uk_ar',
                'type' => 'text',
                'value' => 'test',
                'nation_id' => 2,
            ],

            [
                'key' => 'representer_name_uk_en',
                'neckname' => 'admin.representer_name_uk_en',
                'type' => 'text',
                'value' => 'test',
                'nation_id' => 2,
            ],  
            
            // رسالة قبول طلب تسجيل الجمعيه لبريطانيا 
            [
                'key' => 'charity_approve_msg_uk_ar',
                'neckname' => 'admin.charity_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب تسجيل الجمعية الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'charity_approve_msg_uk_en',
                'neckname' => 'admin.charity_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Registeration Request is Approved',
                'nation_id' => 2,
            ],

            // رسالة قبول طلب الصرف لبريطانيا 
            [
                'key' => 'fin_req_approve_msg_uk_ar',
                'neckname' => 'admin.fin_req_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب الصرف الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'fin_req_approve_msg_uk_en',
                'neckname' => 'admin.fin_req_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Exchange Request is Approved',
                'nation_id' => 2,
            ],
            
            //  رسالة رفض طلب الصرف لبريطانيا
            [
                'key' => 'fin_req_reject_msg_uk_ar',
                'neckname' => 'admin.fin_req_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب الصرف الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'fin_req_reject_msg_uk_en',
                'neckname' => 'admin.fin_req_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Exchange Request is rejected',
                'nation_id' => 2,
            ],

            // رسالة رفض طلب تسجيل الجمعيه لبريطانيا 
            [
                'key' => 'charity_reject_msg_uk_ar',
                'neckname' => 'admin.charity_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب تسجيل الجمعية الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'charity_reject_msg_uk_en',
                'neckname' => 'admin.charity_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Registeration Request is rejected',
                'nation_id' => 2,
            ],

            // رسالة قبول تقرير بريطانيا
            [
                'key' => 'report_approve_msg_uk_ar',
                'neckname' => 'admin.report_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول التقرير الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'report_approve_msg_uk_en',
                'neckname' => 'admin.report_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Report is Approved',
                'nation_id' => 2,
            ],

            // رسالة رفض تقرير بريطانيا
            [
                'key' => 'report_reject_msg_uk_ar',
                'neckname' => 'admin.report_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض التقرير الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'report_reject_msg_uk_en',
                'neckname' => 'admin.report_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Report is Rejected',
                'nation_id' => 2,
            ],

            // رسالة قبول طلب تسجيل مشروع  لبريطانيا
            [
                'key' => 'project_approve_msg_uk_ar',
                'neckname' => 'admin.project_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب تسجيل المشروع الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'project_approve_msg_uk_en',
                'neckname' => 'admin.project_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Project Request is Approved',
                'nation_id' => 2,
            ], 

            // رسالة رفض طلب تسجيل مشروع  لبريطانيا
            [
                'key' => 'project_reject_msg_uk_ar',
                'neckname' => 'admin.project_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب تسجيل المشروع الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'project_reject_msg_uk_en',
                'neckname' => 'admin.project_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Project Request is rejected',
                'nation_id' => 2,
            ], 

            // رسالة قبول طلب تعديل لبريطانيا
            [
                'key' => 'edit_approve_msg_uk_ar',
                'neckname' => 'admin.edit_approve_msg_ar',
                'type' => 'text',
                'value' => 'تم قبول طلب التعديل الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'edit_approve_msg_uk_en',
                'neckname' => 'admin.edit_approve_msg_en',
                'type' => 'text',
                'value' => 'Your Edit Request is Approved',
                'nation_id' => 2,
            ],
            
            // رسالة رفض طلب تعديل لبريطانيا
            [
                'key' => 'edit_reject_msg_uk_ar',
                'neckname' => 'admin.edit_reject_msg_ar',
                'type' => 'text',
                'value' => 'تم رفض طلب التعديل الخاص بكم',
                'nation_id' => 2,
            ],
            [
                'key' => 'edit_reject_msg_uk_en',
                'neckname' => 'admin.edit_reject_msg_en',
                'type' => 'text',
                'value' => 'Your Edit Request is rejected',
                'nation_id' => 2,
            ],

            // شعار وى هيب لبريطانيا
            [
                'key' => 'we_help_slogan_uk_ar',
                'neckname' => 'admin.we_help_slogan_uk_ar',
                'type' => 'text',
                'value' => 'شعار وى هيلب',
                'nation_id' => 2,
            ],
            [
                'key' => 'we_help_slogan_uk_en',
                'neckname' => 'admin.we_help_slogan_uk_en',
                'type' => 'text',
                'value' => 'We help slogan',
                'nation_id' => 2,
            ], 
            
            // بيانات تسجيل دخول الموظفين لبرطانيا

            [
                'key' => 'msg_data_login_uk_ar',
                'neckname' => 'admin.msg_data_login_uk_ar',
                'type' => 'text',
                'value' => 'بيانات الدخول الي منصه وي هيلب',
                'nation_id' => 2,
            ], 
            
            [
                'key' => 'msg_data_login_uk_en',
                'neckname' => 'admin.msg_data_login_uk_en',
                'type' => 'text',
                'value' => 'Data Login To Platform We Help',
                'nation_id' => 2,
            ], 

            //   نص مشتريعنا فى الصفحة الرئيسيه لبريطانيا
            [
                'key' => 'our_projects_text_uk_ar',
                'neckname' => 'admin.our_projects_text_uk_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 2,
            ],
            [
                'key' => 'our_projects_text_uk_en',
                'neckname' => 'admin.our_projects_text_uk_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 2,
            ],

            //   نص الاحصائيات فى الصفحة الرئيسية لبريطانيا
            [
                'key' => 'statistics_text_uk_ar',
                'neckname' => 'admin.statistics_text_uk_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 2,
            ],
            [
                'key' => 'statistics_text_uk_en',
                'neckname' => 'admin.statistics_text_uk_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 2,
            ],

            //   نص شركائنا فى الصفحة الرئيسية لبريطانيا
            [
                'key' => 'parteners_text_uk_ar',
                'neckname' => 'admin.parteners_text_uk_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 2,
            ],
            [
                'key' => 'parteners_text_uk_en',
                'neckname' => 'admin.parteners_text_uk_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 2,
            ],

            //   نص من نحن فى الصفحة الرئيسية لبريطانيا
            [
                'key' => 'about_us_text_uk_ar',
                'neckname' => 'admin.about_us_text_uk_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 2,
            ],
            
            [
                'key' => 'about_us_text_uk_en',
                'neckname' => 'admin.about_us_text_uk_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 2,
            ],

            //   نص  تواصل معنا فى الصفحة الرئيسية لبريطانيا
            [
                'key' => 'contact_us_text_uk_ar',
                'neckname' => 'admin.contact_us_text_uk_ar',
                'type' => 'text',
                'value' => 'تطوير المشاريع ذات التأثير بطرق أدوات تتبع فعالة ومهنية',
                'nation_id' => 2,
            ],
            [
                'key' => 'contact_us_text_uk_en',
                'neckname' => 'admin.contact_us_text_uk_en',
                'type' => 'text',
                'value' => 'Development projects with impact in ways Effective and professional tracking tools.',
                'nation_id' => 2,
            ],           
            
            // setting other uk

            [
                'key' => 'text_country_uk_ar',
                'neckname' => 'admin.text_country_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ], 
            
            [
                'key' => 'text_country_uk_en',
                'neckname' => 'admin.text_country_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ],             
            
            [
                'key' => 'text_project_uk_ar',
                'neckname' => 'admin.text_project_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ],
            
            [
                'key' => 'text_project_uk_en',
                'neckname' => 'admin.text_project_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ],            
            
            [
                'key' => 'text_amount_uk_ar',
                'neckname' => 'admin.text_amount_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ],
             
            [
                'key' => 'text_amount_uk_en',
                'neckname' => 'admin.text_amount_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ],
            
            [
                'key' => 'text_partener_uk_ar',
                'neckname' => 'admin.text_partener_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ], 
            
            [
                'key' => 'text_partener_uk_en',
                'neckname' => 'admin.text_partener_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ],             
            
            [
                'key' => 'text_beneficiary_uk_ar',
                'neckname' => 'admin.text_beneficiary_ar',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ],  
            
            [
                'key' => 'text_beneficiary_uk_en',
                'neckname' => 'admin.text_beneficiary_en',
                'type' => 'text',
                'value' => 'text',
                'nation_id' => 2,
            ], 

             // العنوان لبريطانيا
             [
                'key' => 'address_uk',
                'neckname' => 'admin.address',
                'type' => 'text',
                'value' => 'test',
                'nation_id' => 2,
            ],

             // msg notify accept bond exchange (UK)
             [
                'key' => 'msg_bond_accept_uk_ar',
                'neckname' => 'admin.msg_bond_accept_uk_ar',
                'type' => 'text',
                'value' => 'برجاء قبول طلب السند الخاص بكم ',
                'nation_id' => 2,
            ], 

            [
                'key' => 'msg_bond_accept_uk_en',
                'neckname' => 'admin.msg_bond_accept_uk_en',
                'type' => 'text',
                'value' => 'Please accept your bond request',
                'nation_id' => 2,
            ],
            

            // الموقع لبريطانيا 
            [
                'key' => 'website_uk',
                'neckname' => 'admin.website',
                'type' => 'text',
                'value' => 'www.test.com',
                'nation_id' => 2,
            ],

            [
                'key' => 'facebook_uk',
                'neckname' => 'admin.facebook_uk',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 2,
            ], 

            [
                'key' => 'twitter_uk',
                'neckname' => 'admin.twitter_uk',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 2,
            ], 
            
            [
                'key' => 'snapchat_uk',
                'neckname' => 'admin.snapchat_uk',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 2,
            ],  
            
            [
                'key' => 'insta_uk',
                'neckname' => 'admin.insta_uk',
                'type' => 'text',
                'value' => 'logo.png',
                'nation_id' => 2,
            ], 

            // مهلة استلام الميلغ من قبل الجمعيه لبريطانيا
            [
                'key' => 'revieved_duration_uk',
                'neckname' => 'admin.revieved_duration',
                'type' => 'number',
                'value' => 3,
                'nation_id' => 2,
            ],

             //   عدد مراحل المشاريع لبريطانيا
             [
                'key' => 'phases_number_uk',
                'neckname' => 'admin.phases_number',
                'type' => 'number',
                'value' => 10,
                'nation_id' => 2,
            ],
            
            //  اقل حد للتبرع   لبريطانيا
            [
                'key' => 'min_support_uk',
                'neckname' => 'admin.min_support',
                'type' => 'number',
                'value' => 100,
                'nation_id' => 2,
            ],

            // هاتف وى هيب لبريطانيا
            [
                'key' => 'phone_uk',
                'neckname' => 'admin.phone',
                'type' => 'phone',
                'value' => '+966562514852',
                'nation_id' => 2,
            ],

            // ايميل وى هيب لبريطانيا
            [
                'key' => 'email_uk',
                'neckname' => 'admin.email',
                'type' => 'email',
                'value' => 'wehelp@uk.com',
                'nation_id' => 2,
            ],

            //  العملة الافتراضية   لبريطانيا
            [
                'key' => 'default_currency_uk',
                'neckname' => 'admin.default_currency',
                'type' => 'select',
                'value' => 2,
                'nation_id' => 2,
            ],
            
            // لوجو وى هيب لبريطانيا
            [
                'key' => 'we_help_logo_uk_ar',
                'neckname' => 'admin.we_help_logo_uk_ar',
                'type' => 'file',
                'value' => 'logo-uk-ar.png',
                'nation_id' => 2,
            ],
            [
                'key' => 'we_help_logo_uk_en',
                'neckname' => 'admin.we_help_logo_uk_en',
                'type' => 'file',
                'value' => 'logo-uk-en.png',
                'nation_id' => 2,
            ],  

            [
                'key' => 'enable_svg_logo_uk',
                'neckname' => 'admin.enable_svg_logo',
                'type' => 'checkbox',
                'value' => 0,
                'nation_id' => 2,
            ],

            [
                'key' => 'stamp_uk',
                'neckname' => 'admin.stamp_uk',
                'type' => 'file',
                'value' => 'stamp.png',
                'nation_id' => 2,
            ], 

            [
                'key' => 'logo_uk_ar',
                'neckname' => 'admin.logo_uk_ar',
                'type' => 'file',
                'value' => 'logo.png',
                'nation_id' => 2,
            ], 
            
            [
                'key' => 'logo_uk_en',
                'neckname' => 'admin.logo_uk_en',
                'type' => 'file',
                'value' => 'logo.png',
                'nation_id' => 2,
            ],

            [
                'key' => 'pages_header_uk',
                'neckname' => 'admin.pages_header',
                'type' => 'file',
                'value' => 'web/images/pages-bg/10.png',
                'nation_id' => 2,
            ],

             // مقدمة التعاقد عربى + انجليزى لبريطانيا   
             [
                'key' => 'contract_intro_uk_ar',
                'neckname' => 'admin.contract_intro_ar',
                'type' => 'textarea',
                'value' => 'test',
                'nation_id' => 2,
            ],
            [
                'key' => 'contract_intro_uk_en',
                'neckname' => 'admin.contract_intro_en',
                'type' => 'textarea',
                'value' => 'test',
                'nation_id' => 2,
            ],

            // وصف وى هيب لبريطانيا 
            [
                'key' => 'we_help_desc_uk_ar',
                'neckname' => 'admin.we_help_desc_ar',
                'type' => 'textarea',
                'value' => 'وصف وى هيلب',
                'nation_id' => 2,
            ],
            [
                'key' => 'we_help_desc_uk_en',
                'neckname' => 'admin.we_help_desc_en',
                'type' => 'textarea',
                'value' => 'We Help decription',
                'nation_id' => 2,
            ],
           
            // lat , lang and address uk
            [
                'key' => 'langtude_uk',
                'neckname' => 'admin.langtude',
                'type' => 'hidden',
                'value' => '31.381523',
                'nation_id' => 2,
            ],

            [
                'key' => 'latitude_uk',
                'neckname' => 'admin.latitude',
                'type' => 'hidden',
                'value' => '31.037933',
                'nation_id' => 2,
            ],  
                
        ]);
        
    }
}
