<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class ProjectServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_services')->insert([
            [
                'name_ar' => 'توفير سكن أو إيواء',
                'name_en' => 'Providing housing or shelter',
                'type' => 'emerg',
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'مواد غذائية',
                'name_en' => 'foodstuffs',
                'type' => 'emerg',
                'nation_id' => 1,
            ],
            [
                'name_ar' => ' توفير خيام',
                'name_en' => 'Providing tents',
                'type' => 'emerg',
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'توفير بطانيات',
                'name_en' => 'Provide blankets',
                'type' => 'emerg',
                'nation_id' => 1,
            ]  ,
            [
                'name_ar' => ' توفير تدفئة',
                'name_en' => 'Provide heating',
                'type' => 'emerg',
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'توفير حقيبة مدرسية',
                'name_en' => 'Provide a school bag',
                'type' => 'edu',
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'زي مدرسي',
                'name_en' => 'school uniform',
                'type' => 'edu',
                'nation_id' => 1,
            ],
            [
                'name_ar' => ' رسوم دراسية',
                'name_en' => 'Tuition fees',
                'type' => 'edu',
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'وجبات غذائية',
                'name_en' => 'Dietary meals',
                'type' => 'edu',
                'nation_id' => 1,
            ]          
                        
        ]);
    }
}
