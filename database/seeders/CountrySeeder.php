<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'name_ar' => 'المملكة العربية السعودية',
                'name_en' => 'Saudi Arabia',
                'slug' => 'saudi-saudi',
                'code' => 'SA',
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'بريطانيا',
                'name_en' => 'Britain',
                'slug' => 'britain-britain',
                'code' => 'UK',
                'nation_id' => 2,
            ],
            // [
            //     'name_ar' => 'مصر',
            //     'name_en' => 'Egypt',
            //     'code' => 'EG',
            //     'nation_id' => 3,
            // ]           
        ]);
    }
}
