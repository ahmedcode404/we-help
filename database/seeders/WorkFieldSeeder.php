<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class WorkFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('work_fields')->insert([
            [
                'name_ar' => 'الصحه',
                'name_en' => 'Health',
                'nation_id' => 1
            ],
            [
                'name_ar' => 'الصحه',
                'name_en' => 'Health',
                'nation_id' => 2
            ],
        ]);

        // DB::table('charity_work_field')->insert([
        //     [
        //         'charity_id' => 1,
        //         'work_field_id' => 1,
        //     ],
        // ]);
    }
}
