<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class BankSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banks')->insert([
            
            [
                'name_ar' => 'الراجحى',
                'name_en' => 'Al Rajehi',
                'city_id' => 1,
                'address' => 'test address',
                'phone' => +9665213545555,
                'nation_id' => 1,
            ],
            [
                'name_ar' => 'الراجحى',
                'name_en' => 'Al Rajehi',
                'city_id' => 2,
                'address' => 'test address',
                'phone' => +4478135410255,
                'nation_id' => 2,
            ],

        ]);
    }
}
