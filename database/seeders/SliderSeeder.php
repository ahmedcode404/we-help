<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sliders')->insert([
            [
                'path' => asset('storage/uploads/1.png'),
                'title_ar' => 'We Help',
                'title_en' => 'We Help',
                'text_ar' => 'منصة الكترونية (نحن نساعد) مخصصة لتسويق المشاريع الخيرية والأعمال الإنسانية للجمعيات الخيرية حول العالم من خلال نظام بوابة الدفع الإلكتروني ، هذه العلامة التجارية مسجلة في العديد من الدول ، والتي تمكن الجمعيات والمؤسسات من جمع الدعم المادي لمشاريعها بشكل آمن وفعال في جميع أنحاء العالم.',
                'text_en' => 'An electronic platform (we help) dedicated to marketing charitable projects and humanitarian works to charities around the world through the electronic payment portal system, this brand is registered in many countries, which enables associations and institutions to collect material support for their projects safely and effectively worldwide.',
                'appearance' => 'web',
                'nation_id' => 1,
            ],
            [
                'path' => asset('storage/uploads/2.png'),
                'title_ar' => 'We Help',
                'title_en' => 'We Help',
                'text_ar' => 'منصة الكترونية (نحن نساعد) مخصصة لتسويق المشاريع الخيرية والأعمال الإنسانية للجمعيات الخيرية حول العالم من خلال نظام بوابة الدفع الإلكتروني ، هذه العلامة التجارية مسجلة في العديد من الدول ، والتي تمكن الجمعيات والمؤسسات من جمع الدعم المادي لمشاريعها بشكل آمن وفعال في جميع أنحاء العالم.',
                'text_en' => 'An electronic platform (we help) dedicated to marketing charitable projects and humanitarian works to charities around the world through the electronic payment portal system, this brand is registered in many countries, which enables associations and institutions to collect material support for their projects safely and effectively worldwide.',
                'appearance' => 'web',
                'nation_id' => 1,
            ],
            [
                'path' => asset('storage/uploads/3.png'),
                'title_ar' => 'We Help',
                'title_en' => 'We Help',
                'text_ar' => 'منصة الكترونية (نحن نساعد) مخصصة لتسويق المشاريع الخيرية والأعمال الإنسانية للجمعيات الخيرية حول العالم من خلال نظام بوابة الدفع الإلكتروني ، هذه العلامة التجارية مسجلة في العديد من الدول ، والتي تمكن الجمعيات والمؤسسات من جمع الدعم المادي لمشاريعها بشكل آمن وفعال في جميع أنحاء العالم.',
                'text_en' => 'An electronic platform (we help) dedicated to marketing charitable projects and humanitarian works to charities around the world through the electronic payment portal system, this brand is registered in many countries, which enables associations and institutions to collect material support for their projects safely and effectively worldwide.',
                'appearance' => 'web',
                'nation_id' => 1,
            ],
        ]);
    }
}
