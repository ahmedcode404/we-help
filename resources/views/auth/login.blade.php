@extends('layout-login.app')
@section('title')
{{ __('admin.signin') }}
@endsection
@section('content')

    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v2">
                    <div class="auth-inner row m-0">
                        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">
                       <!--<img class="img-fluid" src="{{ url('dashboard/app-assets/images/logo/logo.png') }}" alt="Login V2" />-->
                       <!--<h2 class="brand-text text-primary ml-1">{{trans('admin.we_help')}}</h2>-->
                        </a>
                        <!-- /Brand logo-->
                        <!-- Left Text-->
                        <div class="d-none d-lg-flex col-lg-8 align-items-center p-5">
                            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid" src="{{ url('dashboard/app-assets/images/logo/logo.png') }}" alt="Login V2" /></div>
                        </div>
                        <!-- /Left Text-->
                        <!-- Login-->
                        <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5" @if(\App::getLocale() == 'ar') dir="rtl" style="text-align: right" @else dir="ltr"  @endif>
                            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
                                <h2 class="card-title font-weight-bold mb-1">{{ __('admin.welcome') }} 👋</h2>

                                <form class="auth-login-form mt-2" action="{{ route('login.auth') }}" method="POST">
                                    @csrf

                                    {{-- <div class="form-group  login_role">
                                        <input class="" id="login-email1" type="radio" name="role" value="admin" />
                                        <label class="testr" for="login-email1">{{ __('admin.admin') }}</label>
                                    </div>  
                                    
                                    <div class="form-group login_role">
                                        <input class="form-control" id="login-email2" type="radio" name="role" value="association" />
                                        <label class="form-label" for="login-email2">{{ __('admin.charity') }}</label>
                                    </div>   --}}

                                    <input id="login-email1" type="hidden" name="role" value="admin" />
                                    
                                    @if ($errors->has('role'))
                                        <span class="help-block">
                                            <strong style="color: red;">{{ $errors->first('role') }}</strong>
                                        </span>
                                    @endif

                                    <div class="form-group">
                                        <label class="form-label" for="login-email">{{ __('admin.email') }}</label>
                                        <input class="form-control" id="login-email" type="email" name="email" placeholder="john@example.com" />
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="d-flex justify-content-between">
                                            <label for="login-password">{{ __('admin.password') }}</label>
                                        </div>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input class="form-control form-control-merge" id="login-password" type="password" name="password" placeholder="············" />
 
                                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                                        </div>
                                    </div>

                                    <button type="submit" class="btn btn-primary btn-block" tabindex="4">{{ __('admin.signin') }}</button>
                                </form>
                                <p class="text-center mt-2">
                                    <a href="{{ route('forgot-password') }}">
                                        <span>&nbsp;{{ __('admin.forget_password') }}</span>
                                    </a>
                                </p>

                            </div>
                        </div>
                        <!-- /Login-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->

@endsection

