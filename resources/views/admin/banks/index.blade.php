@extends('layout.app')

@section('title', trans('admin.banks'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">

                @if(checkPermissions(['create_banks']))
                    <a href="{{route('admin.banks.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif

                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['banks'] as $bank)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$bank->name}}</td>
                                <td>
                                            @if(checkPermissions(['edit_banks']))
                                                <a  href="{{route('admin.banks.edit', $bank->id)}}" title="{{trans('admin.edit')}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                </a>
                                            @endif
                                            @if(checkPermissions(['delete_banks']))
                                                <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.banks.destroy', $bank->id)}}" title="{{trans('admin.delete')}}">
                                                    <i data-feather="trash" class="mr-50"></i>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
            {{$data['banks']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection