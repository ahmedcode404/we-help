@extends('layout.app')

@section('title', trans('admin.create_banks'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" />    
@endpush

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.create')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="banks-form" method="POST" action="{{route('admin.banks.store')}}">
                            @csrf

                            <div class="form-group">
                                <label class="form-label" for="name_ar">{{trans('admin.name_ar')}}</label>
                                <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{old('name_ar')}}" placeholder="{{trans('admin.name_ar')}}" required />
                            </div>
                            
                            <div class="form-group">
                                <label class="form-label" for="name_en">{{trans('admin.name_en')}}</label>
                                <input type="text" id="name_en" name="name_en" class="form-control" value="{{old('name_en')}}" placeholder="{{trans('admin.name_en')}}" required />
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="address">{{trans('admin.address')}}</label>
                                <input type="text" id="address" name="address" class="form-control" value="{{old('address')}}" placeholder="{{trans('admin.address')}}" required />
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="phone_num">{{trans('admin.phone')}}</label>
                                <input type="text" name="phone_num" id="phone" class="form-control" value="{{old('phone')}}" placeholder="{{trans('admin.phone')}}" required >
                            </div>

                            <div class="form-group">
                                <label for="city_id">{{trans('admin.city')}}</label>
                                <select class="form-control select2" id="city_id" name="city_id" required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['cities'] as $country)
                                        @if($country->parent_id != null)
                                            <option value="{{$country->id}}" @if(old('city_id') == $country->id) selected @endif>{{$country->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            {{-- <div class="form-group">
                                <label for="nation_id">{{trans('admin.nation')}}</label>
                                <select class="form-control select2" id="nation_id" name="nation_id" required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['countries'] as $country)
                                        @if($country->id == 1 || $country->id == 2)
                                            <option value="{{$country->id}}" @if(old('nation_id') == $country->id) selected @endif>{{$country->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div> --}}

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection

@push('js')

    <script src="{{asset('dashboard/js/intlTelInput.min.js')}}" type="text/javascript"></script>

    <script>
        // phone
        var input = document.querySelector("#phone");
        window.intlTelInput(input, {
            preferredCountries: ["gb", "sa"],
            hiddenInput: "phone",
            separateDialCode:true,
            formatOnDisplay:false,
            utilsScript: "{{asset('dashboard/js/utils.js')}}"
        });

    </script>

    <script src="{{asset('dashboard/forms/banks/create.js')}}"></script>
    
@endpush