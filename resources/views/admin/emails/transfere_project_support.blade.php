<!DOCTYPE html>

<html>

<head>
    <title> {{trans('admin.we_help')}}  </title>
    <meta charset="utf-8">
    <meta name="description" content="welcoma">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- *******************************
    **********************
    **************
 -->

 </head>
 <body style="padding: 0px; margin: 0px; direction: rtl; text-align: right;">
    @php
        $nation_id = getNationId();
    @endphp
    <div style="width: 750px; margin: auto; background-color: #f5f5f5; padding:20px ;">
        <div style=" background: #fff ; padding: 8px 0px;"  >
                <!-- logo -->
                <div style="margin: 10px ; text-align: right;">
                    <img src="{{asset('storage/'.getSettingMsg('we_help_logo', $nation_id)->value)}}" alt="logo">
                </div>

                <div style="text-align: left; padding: 20px 0px 10px 100px ;">
                    <img src="https://i.ibb.co/M9FDsKt/2021-01-31-1.png" alt="">
                </div> 
                
                <div style="margin: 10px 10px;">
                    <h2 style="color:#354052 ; text-align:right"> {{trans('admin.support_transfere_title')}}</h2>
                    
                    {{trans('admin.support_transfere_text')}}
                    
                    <br>
                    
                    {{trans('admin.project_name')}}: {{$data['name_project']}}
                    
                    <br>
                    
                    {{trans('admin.charity_name')}}: {{$data['charity_name']}}
                    
                </div>
                
            </div>
            
            <div style="color: #354052; margin: 30px 0px ; text-align: center;">
                <h2 style="font-size: 30px; margin-bottom: 10px; text-align:center ">{{trans('admin.we_help')}}</h2>
                <p style="text-align:center ">{{getSettingMsg('we_help_slogan', $nation_id)->value}}</p>

            <p style="text-align:center "> {{getSettingMsg('we_help_slogan', $nation_id)->value}} {{Carbon\Carbon::now()->year}} </p>
            <a href="mailto:{{getSettingValue('email', $nation_id)->value}}"> {{getSettingValue('email', $nation_id)->value}}</a>
            <a href="tel:{{getSettingValue('phone', $nation_id)->value}}"> {{getSettingValue('phone', $nation_id)->value}}</a>
            <a href="{{URL::to('/')}}" style="display: block; margin: 10px 0px ;"> {{URL::to('/')}}</a>

        </div>

        
    </div>
</body>
<!-- end-body
=================== -->

</html>
