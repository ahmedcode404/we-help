@extends('layout.app')

@section('title', trans('admin.show_city'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.show')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name_en')}}</th>
                                <th>{{trans('admin.name_ar')}}</th>
                                <th>{{trans('admin.country')}}</th>
                                <th>{{trans('admin.type')}}</th>
                                {{-- <th>{{trans('admin.nation')}}</th> --}}
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['city']->name_en}}</td>
                                <td>{{$data['city']->name_ar}}</td>
                                <td>{{$data['city']->parent_id != null ? $data['city']->country->name : '-'}}</td>
                                <td>{{$data['city']->parent_id != null ? trans('admin.city') : trans('admin.country')}}</td>
                                {{-- <td>{{$data['city']->nation->code}}</td> --}}
                                <td>
                                    
                                            <a title="{{trans('admin.edit')}}" href="{{route('admin.cities.edit', $data['city']->id)}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.charities')}}</h4>
                    </div>
                    <div class="card-body">
                        
                        <div class="card-datatable table-responsive">
                            <table class="datatables-basic table">
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.type')}}</th>
                                <th>{{trans('admin.contract_date')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                            <tr>
                                @foreach($data['city']->charities as $charity)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$charity->name}}</td>
                                        <td>{{trans('admin.'.$charity->type)}}</td>
                                        <td>{{$charity->contract_date}}</td>
                                        <td>
                                          
                                        <a href="{{route('admin.charities.show', $charity->id)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                        <a  href="{{route('admin.charities.edit', $charity->id)}}" title="{{trans('admin.edit')}}">
                                            <i data-feather="edit-2" class="mr-50"></i>
                                        </a>
                                        <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('admin.charities.destroy', $charity->id)}}">
                                            <i data-feather="trash" class="mr-50"></i>
                                        </a>
                                                
                                        </td>
                                    </tr>
                                @endforeach
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection