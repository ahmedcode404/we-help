@extends('layout.app')

@section('title', trans('admin.cities'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <a href="{{route('admin.cities.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_en')}}</th>
                            <th>{{trans('admin.name_ar')}}</th>
                            <th>{{trans('admin.country')}}</th>
                            <th>{{trans('admin.type')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['cities'] as $city)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$city->name_en}}</td>
                                <td>{{$city->name_ar}}</td>
                                <td>{{$city->parent_id != null ? $city->country->name : '-'}}</td>
                                <td>{{$city->parent_id != null ? trans('admin.city') : trans('admin.country')}}</td>
                                {{-- <td>{{$city->nation->code}}</td> --}}
                                <td>
                                            <a  href="{{route('admin.cities.show', $city->id)}}" title="{{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                            <a  href="{{route('admin.cities.edit', $city->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                            @if($city->id != 1 && $city->id != 2 && $city->id != 3)
                                                <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.cities.destroy', $city->id)}}" title="{{trans('admin.delete')}}">
                                                    <i data-feather="trash" class="mr-50"></i>
                                                </a>
                                            @endif
                                        
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['cities']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection