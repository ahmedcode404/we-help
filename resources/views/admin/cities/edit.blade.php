@extends('layout.app')

@section('title', trans('admin.edit_city'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="create_city" method="POST" action="{{route('admin.cities.update', $data['city']->id)}}">
                            @csrf
                            @method('PUT')

                            <div class="custom-control custom-radio">
                                <input type="radio" id="country" name="category" value="country" class="custom-control-input"
                                @if($data['city']->parent_id == null) checked @endif />
                                <label class="custom-control-label" for="country">{{trans('admin.country')}}</label>
                            </div>

                            {{-- do not change the main countries (SA and UK) to cities  --}}
                            @if($data['city']->id != 1 && $data['city']->id != 2)
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="city" name="category" value="city" class="custom-control-input" 
                                    @if($data['city']->parent_id != null) checked @endif/>
                                    <label class="custom-control-label" for="city">{{trans('admin.city')}}</label>
                                </div>
                            @endif

                            <div class="form-group">
                                <label class="form-label" for="name_ar">{{trans('admin.name_ar')}}</label>
                                <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{$data['city']->name_ar == null ? old('name_ar') : $data['city']->name_ar}}" placeholder="{{trans('admin.name_ar')}}" required />
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="name_en">{{trans('admin.name_en')}}</label>
                                <input type="text" id="name_en" name="name_en" class="form-control" value="{{$data['city']->name_en == null ? old('name_en') : $data['city']->name_en}}" placeholder="{{trans('admin.name_en')}}" required />
                            </div>
                           
                            <div class="form-group">
                                <label for="parent_id">{{trans('admin.country')}}</label>
                                <select class="form-control select2" id="parent_id" name="parent_id" required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['countries'] as $country)
                                        <option value="{{$country->id}}" @if(old('country_id') == $country->id || $data['city']->parent_id == $country->id) selected @endif>{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection