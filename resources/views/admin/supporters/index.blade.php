@extends('layout.app')

@section('title', trans('admin.supporters'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.supporter_name')}}</th>
                            <th>{{trans('admin.email')}}</th>
                            <th>{{trans('admin.phone')}}</th>
                            <th>{{trans('admin.country')}}</th>
                            <th>{{trans('admin.supporter_status')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['supporters'] as $supporter)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$supporter->name}}</td>
                                <td>{{$supporter->email}}</td>
                                <td><span dir="ltr">{{$supporter->phone}}</span></td>
                                <td>{{Countries::getOne($supporter->country, \App::getLocale())}}</td>
                                <td><span id="block_text_{{$supporter->id}}">{{$supporter->blocked == 1 ? trans('admin.blocked') : trans('admin.not_blocked')}}</span></td>
                                <td>
                                    @if(checkPermissions(['show_supporter']))
                                        <a  href="{{route('admin.supporters.show', $supporter->id)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                    {{-- @else
                                        {{trans('admin.do_not_have_permission')}} --}}
                                    @endif
                            
                                    @if(checkPermissions(['block_unblock_supporter']))
                                                {{-- @if(checkPermissions(['block_unblock_supporter'])) --}}
                                                    <a class="block" data-action="{{route('admin.supporters.block', $supporter->id)}}" data-method="POST" title="@if($supporter->blocked == 0) {{trans('admin.block')}} @else {{trans('admin.unblock')}} @endif">
                                                        @if($supporter->blocked == 0)
                                                          <i data-feather="slash" class="mr-50"></i>
                                                          @else  
                                                          <i data-feather="user-check" class="mr-50"></i>
                                                          @endif
                                                    </a>
                                                {{-- @endif --}}
                                                
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['supporters']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/supporters/index.js')}}"></script>
@endpush