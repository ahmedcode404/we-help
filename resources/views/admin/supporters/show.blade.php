@extends('layout.app')

@section('title', trans('admin.show_supporters'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            {{-- supporter data start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.show_supporters')}}</h4>
                    </div>
                    @php
                        $session_currency = currencySymbol(session('currency'));
                    @endphp
                    <div class="card-datatable table-responsive">
                            <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.supporter_name')}}</th>
                                <th>{{trans('admin.email')}}</th>
                                <th>{{trans('admin.phone')}}</th>
                                <th>{{trans('admin.country')}}</th>
                                <th>{{trans('admin.supporter_status')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['supporter']->name}}</td>
                                <td>{{$data['supporter']->email}}</td>
                                <td><span dir="ltr">{{$data['supporter']->phone}}</span></td>
                                <td>{{Countries::getOne($data['supporter']->country, \App::getLocale())}}</td>
                                <td><span id="block_text_{{$data['supporter']->id}}">{{$data['supporter']->blocked == 1 ? trans('admin.blocked') : trans('admin.not_blocked')}}</span></td>
                                
                                <td>
                                    @if(checkPermissions(['block_unblock_supporter']))
                                        <a class="block" data-action="{{route('admin.supporters.block', $data['supporter']->id)}}" data-method="POST" title=" @if($data['supporter']->blocked == 0) {{trans('admin.block')}} @else {{trans('admin.unblock')}} @endif">
                                            <span>
                                                @if($data['supporter']->blocked == 0)
                                                <i data-feather="slash" class="mr-50"></i>
                                                @else
                                                <i data-feather="user-check" class="mr-50"></i>
                                                @endif
                                            </span>
                                        </a>
                                    @endif
                                            
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            {{-- supporter data end --}}

            {{-- supporters projects start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.donated_projects')}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="card-datatable table-responsive">
                            <table class="datatables-basic table">
                            <tr>
                                <th>#</th>
                                <th>{{trans('admin.project_num')}}</th>
                                <th>{{trans('admin.project_name')}}</th>
                                <th>{{ $session_currency }} {{trans('admin.project_cost')}}</th>
                                <th>{{ $session_currency }} {{trans('admin.support')}}</th>
                                <th class="cell-fit">{{trans('admin.actions')}}</th>
                            </tr>
                            <tr>
                                @forelse ($data['supporter']->projects as $project)
                                    @if($project->nation_id == auth()->user()->nation_id)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$project->project_num}}</td>
                                            <td>{{$project->name}}</td>
                                            <td>{{$project->cost}} {{$project->currency->symbol}}</td>
                                            <td>{{$project->pivot->cost}} {{currencySymbol($project->pivot->currency_id)}}</td>
                                            <td>
                                                
                                            <a  href="{{route('admin.projects.show', $project->id)}}" title="{{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                            <a  href="{{route('admin.projects.edit', $project->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                            <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('admin.projects.destroy', $project->id)}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                        </tr>
                                    @endif
                                @empty
                                    <tr>
                                        <td colspan="6">{{trans('admin.no_data')}}</td>
                                    </tr>
                                @endforelse
                            </tr>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            {{-- supporter projects end --}}
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/supporters/index.js')}}"></script>
@endpush