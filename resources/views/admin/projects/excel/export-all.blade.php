<table class="datatables-basic table">
    <thead>
        <tr>
            <th>{{trans('admin.id')}}</th>
            <th>{{ trans('admin.project_num') }}</th>
            <th>{{ trans('admin.category') }}</th>
            <th>{{ trans('admin.name') }}</th>
            <th>{{ trans('admin.duration') }}</th>
            <th>{{ trans('admin.start_date') }}</th>
            <th>{{ trans('admin.charity') }}</th>
            <th>{{ trans('admin.location') }}</th>
            <th>{{ trans('admin.city') }}</th>
            <th>{{ trans('admin.country') }}</th>
            <th>{{ trans('admin.benef_num') }}</th>
            <th>{{ trans('admin.desc') }}</th>
            <th>{{ trans('admin.cost') }}</th>
            <th>{{ trans('admin.total_donations') }}</th>
            <th>{{ trans('admin.remaining_donations') }}</th>
            <th>{{ trans('admin.currency') }}</th>
            <th>{{ trans('admin.status') }}</th>
            <th>{{ trans('admin.service') }}</th>
            <th>{{ trans('admin.service_feature') }}</th>
            <th>{{ trans('admin.phases_count') }}</th>
            <th>{{ trans('admin.reports_count') }}</th>
            <th>{{ trans('admin.sponsers_count') }}</th>
            <th>{{ trans('admin.edit_requests') }}</th>
            <th>{{ trans('admin.exchange_requests') }}</th>
            <th>{{ trans('admin.ratings') }}</th>
            <th>{{ trans('admin.total_bills_out_to_project') }}</th> {{-- getTotalProjectExchangeVouchers() --}}
        </tr>
    </thead>
    <tbody>
        @php
            $session_currency = currencySymbol(session('currency'));
        @endphp
        @foreach($data['projects'] as $project)
            @php
                $project_total_suports = $project->total_supports_for_project;
            @endphp
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{ $project->project_num }}</td>
                <td>{{ $project->category ? $project->category->name : '--' }}</td>
                <td>{{$project->name}}</td>
                <td>{{$project->duration}}</td>
                <td>{{$project->start_date}}</td>
                <td>{{$project->charity ? $project->charity->name : '--'}}</td>
                <td>{{$project->location}}</td>
                <td>{{$project->city}}</td>
                <td>{{Countries::getOne($project->country, \App::getLocale())}}</td>
                <td>{{$project->benef_num}}</td>
                <td>{{$project->desc}}</td>
                <td>{{generalExchange($project->get_total, $project->currency->currency->symbol, $session_currency)}}</td> {{-- total cost --}}
                <td>{{ $project_total_suports}}</td> {{-- total donation --}}
                <td>{{ generalExchange($project->get_total, $project->currency->currency->symbol, $session_currency) -  $project_total_suports }}</td> {{-- remaining donation --}}
                <td>{{$session_currency}}</td>  
                <td>
                    @if($project->status == 'approved')
                        <span class="badge badge-light-success">{{trans('admin.'.$project->status)}}</span>
                    @elseif($project->status == 'waiting')
                        <span class="badge badge-light-warning">{{trans('admin.'.$project->status)}}</span>
                    @elseif($project->status == 'hold')
                        <span class="badge badge-light-info">{{trans('admin.'.$project->status)}}</span>
                    @elseif($project->status == 'rejected')
                        <span class="badge badge-light-danger">{{trans('admin.'.$project->status)}}</span>
                    @endif
                </td>
                <td>{{$project->service_option ? $project->service_option->name : $project->other_service_option}}</td>
                <td>{{$project->service_feature ? $project->service_feature->name : $project->other_service_feature}}</td>
                <td>{{ count($project->phases) }}</td>
                <td>{{ count($project->reports) }}</td>
                <td>{{ count($project->sponsers) }}</td>
                <td>{{ count($project->edit_requests) }}</td>
                <td>{{ count($project->exchange_requests) }}</td>
                <td>{{ $project->getProjectRatings()->first() ? $project->getProjectRatings()->first()->avg_grade : 0 }}</td>
                <td>{{ $project->getTotalProjectExchangeVouchers() }}</td>
            </tr>
        @endforeach
    </tbody>
</table>