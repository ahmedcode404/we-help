<option value="">{{ trans('admin.select') }}</option>
@foreach ($data['categories'] as $category)
    <option value="{{ $category->id }}" data-type="{{$category->type}}">{{ $category->name }}</option>    
@endforeach