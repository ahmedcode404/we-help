@extends('layout.app')

@section('title', trans('admin.edit-project'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
    
@endpush

@section('content')

    <!--Edit project start -->
    <section class="app-user-edit">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ __('admin.edit-project') }}</h4>
            </div>
            <div class="card-body">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center active" id="basic_info-tab" data-toggle="tab" href="#basic_info" aria-controls="basic_info" role="tab" aria-selected="true">
                            <i data-feather="user"></i><span>{{trans('admin.basic_info')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="images-tab" data-toggle="tab" href="#images" aria-controls="images" role="tab" aria-selected="false">
                            <i data-feather="image"></i><span>{{trans('admin.images')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="goals_and_description-tab" data-toggle="tab" href="#goals_and_description" aria-controls="goals_and_description" role="tab" aria-selected="false">
                            <i data-feather="info"></i><span>{{trans('admin.goals_and_description')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="phases-tab" data-toggle="tab" href="#phases" aria-controls="phases" role="tab" aria-selected="false">
                            <i data-feather="activity"></i><span>{{trans('admin.phases')}}</span>
                        </a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="sponsers-tab" data-toggle="tab" href="#sponsers" aria-controls="sponsers" role="tab" aria-selected="false">
                            <i data-feather="users"></i><span>{{trans('admin.sponsers')}}</span>
                        </a>
                    </li> --}}
                </ul>
                <div class="tab-content">
                    <!-- Basic info Tab starts -->
                    @include('admin.projects.edit.basic_info')
                    <!-- Basic info Tab ends -->

                    <!-- images Tab starts -->
                    @include('admin.projects.edit.images')
                    <!-- images Tab ends -->

                    <!-- goals_and_description Tab starts -->
                    @include('admin.projects.edit.goals_and_description')
                    <!-- goals_and_description Tab ends -->

                    <!-- phases Tab starts -->
                    @include('admin.projects.edit.phases')
                    <!-- phases Tab ends -->

                    <!-- sponsers Tab starts -->
                    {{-- @include('admin.projects.edit.sponsers') --}}
                    <!-- sponsers Tab ends -->
                </div>
            </div>
        </div>
    </section>
    <!-- Edit project ends -->
@endsection

@push('js')
<script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>
    
@if(\App::getLocale() == 'ar')
    <!--for arabic-only-->
    <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
    <!--end-->
@endif
<script>
    //datepicker
$(function () {
    $('.date-picker').datepicker({
        changeYear: true,
        dateFormat: 'yy-mm-dd',
    })
});
</script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdarVlRZOccFIGWJiJ2cFY8-Sr26ibiyY&libraries=places&callback=initAutocomplete&language=<?php echo e('ar'); ?>"
        defer></script>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
        
        <script>
            
            // ckeditor custom
            
            CKEDITOR.replace( 'long_desc_ar' );   
            CKEDITOR.replace( 'long_desc_en' );
            CKEDITOR.replace( 'goals_ar' );   
            CKEDITOR.replace( 'goals_en' );
            
            </script>
        <script src="{{ url('custom/preview-image.js') }}"></script>
        <script src="{{ url('dashboard/forms/projects/map.js') }}"></script>
        <script src="{{asset('dashboard/forms/projects/edit.js')}}"></script>
        <script src="{{asset('dashboard/forms/projects/submit_forms.js')}}"></script>
        <script src="{{asset('dashboard/forms/main/delete-image.js')}}"></script>
@endpush