<div class="tab-pane" id="goals_and_description" aria-labelledby="goals_and_description-tab" role="tabpanel">

    <!-- users edit social form start -->
    <form id="goals_and_description_form" class="goals_and_description_form" action="{{route('admin.projects.update', $data['project']->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <input type="hidden" id="edit" value="true">

        <div class="row align-items-center">

            <div class="col-md-6">
              <div class="form-group">
                    <label for="attach">{{ trans('admin.attach') }}</label>
                    <input id="attach" type="file" accept="application/pdf" class="form-control" name="attach" placeholder="{{trans('admin.attach')}}" />
                    <small>(Max size: 50MG)</small>
                    <span class="error-input error-attach"></span>
            </div>
            </div>

            <div class="col-md-6">
                <div class="media mg-1 align-items-center">
                    {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                    <div class="media-body"><a href="{{asset('storage/'.$data['project']->attach)}}" target="_blanck">{{trans('admin.show')}}</a></div>
                </div>
            </div>

            <br><br>
            
            {{-- desc_ar start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="desc_ar">{{ trans('admin.desc_ar') }}</label>
                    <textarea class="form-control" id="desc_ar" name="desc_ar" rows="3" required>{{ $data['project']->desc_ar }}</textarea>
                    <span class="error-input error-desc_ar"></span>
                </div>
            </div>        
            {{-- desc_ar end --}}

            {{-- desc_en start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="desc_en">{{ trans('admin.desc_en') }}</label>
                    <textarea class="form-control" id="desc_en" name="desc_en" rows="3" required>{{ $data['project']->desc_en }}</textarea>
                    <span class="error-input error-desc_en"></span>
                </div>
            </div>        
            {{-- desc_en end --}}

            {{-- long_desc_ar start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="long_desc_ar">{{ trans('admin.long_desc_ar') }}</label>
                    <textarea class="form-control" id="long_desc_ar" name="long_desc_ar" rows="3" required>{{ $data['project']->long_desc_ar }}</textarea>
                    <span class="error-input error-long_desc_ar"></span>
                </div>
            </div>        
            {{-- long_desc_ar end --}}

            {{-- long_desc_en start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="long_desc_en">{{ trans('admin.long_desc_en') }}</label>
                    <textarea class="form-control" id="long_desc_en" name="long_desc_en" rows="3" required>{{ $data['project']->long_desc_en }}</textarea>
                    <span class="error-input error-long_desc_en"></span>
                </div>
            </div>        
            {{-- long_desc_en end --}}

            {{-- goals_ar start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="goals_ar">{{ trans('admin.goals_ar') }}</label>
                    <textarea class="form-control" id="goals_ar" name="goals_ar" rows="3" required>{{ $data['project']->goals_ar }}</textarea>
                    <span class="error-input error-goals_ar"></span>
                </div>
            </div>        
            {{-- goals_ar end --}}

            {{-- goals_en start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="goals_en">{{ trans('admin.goals_en') }}</label>
                    <textarea class="form-control" id="goals_en" name="goals_en" rows="3" required>{{ $data['project']->goals_en }}</textarea>
                    <span class="error-input error-goals_en"></span>
                </div>
            </div>        
            {{-- goals_en end --}}


            <div class="col-12 error text-center general_error"></div>
            <div class="col-12 mt-2" id="loading">
                <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 goals_and_description_form_button">{{trans('admin.save')}}</button>
                <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
            </div>
        </div>
    </form>
    <!-- users edit social form ends -->
</div>