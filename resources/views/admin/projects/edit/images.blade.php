<div class="tab-pane" id="images" class="images_form" aria-labelledby="images-tab" role="tabpanel">

    <section id="ecommerce-products" class="grid-view">

        <div class="card ecommerce-card">
            <div class="item-img">
                <h3 class="mb-2">{{$data['project']->name}} - {{ $data['project']->type }}</h3>
                
                <div class="row mb-5">

                @forelse ($data['project']->images as $image)
                    <div class="mb-2 col-xl-2 col-lg-3 col-md-6" id="image_{{ $image->id }}">
                        <div class="inner-proj-media">
                        <img src="{{asset('storage/'.$image->path)}}" alt="{{trans('admin.logo')}}" class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" style="max-height:70px" width="70" />
                                     {{-- <a class="d-none d-sm-block delete_image" data-model="project" data-action="{{route('admin.main.delete-image')}}" data-image_id="{{$image->id}}" data-id="{{$data['project']->id}}" data-method="POST">{{trans('admin.delete')}}</a> --}}
                                    <a class="delete_image btn btn-primary" data-model="project" data-action="{{route('admin.main.delete-image')}}" data-image_id="{{$image->id}}" data-id="{{$data['project']->id}}" data-method="POST">
                                        <i class="mr-0" data-feather="trash"></i>
                                    </a>
                                </div>
                            </div>
                @empty
                    <div class="col-md-12">
                        <p class="text_01">{{trans('admin.no_data')}}</p>
                    </div>
                @endforelse
            </div>

                <!-- Images form start -->
                <form class="images_form" action="{{route('admin.projects.update', $data['project']->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <input type="hidden" id="edit" value="true">

                    <div class="form-group row">
                        <div class="col-md-2 mb-1">
                            <span>{{trans('admin.image')}}</span>
                        </div>
                        <div class="col-md-10">
                            <div class="input-group control-group image_increment" >
                                <input type="file" name="images[]" class="form-control" accept=".gif, .jpg, .png, .webp">
                                <div class="invalid-feedback">
                                    {{trans('admin.image')}}
                                </div>
                                <div class="input-group-btn"> 
                                    <button class="btn btn-success img-btn-success" type="button"><i data-feather="plus" aria-hidden="true"></i></button>
                                </div>
                            </div>
                            <small>(Max size: 20MG)</small>
                            <div class="image_clone hidden">
                                <div class="control-group input-group" style="margin-top:10px">
                                    <input type="file" name="images[]" class="form-control" accept=".gif, .jpg, .png, .webp">
                                    <div class="input-group-btn"> 
                                        <button class="btn btn-danger img-btn-danger" type="button"><i  data-feather="minus" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="error-input error-images"></span>
                    </div>

                    <div class="col-12 error text-center general_error"></div>
                    <div class="mt-2 text-right" id="loading">
                        <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_images_button">{{trans('admin.save')}}</button>
                        <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
                    </div>
                </form>
                <!-- Images form ends -->

            </div>
            
        </div>
        <hr>
        
    </section>

</div>