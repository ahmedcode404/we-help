<div class="tab-pane" id="phases" aria-labelledby="phases-tab" role="tabpanel">
    <!--project phases form start -->
    <div class="row">
        <a href="{{route('admin.phases.create', $data['project']->id)}}" class="btn btn-primary btn_01">{{ trans('admin.create_phase') }}</a>
    </div>
    <div class="card-datatable table-responsive">

    <table class="datatables-basic table">
        <thead>
            <tr>
                <th>{{trans('admin.phase_name')}}</th>
                <th>{{$data['project']->currency->symbol}} {{trans('admin.phase_cost')}}</th>
                <th class="text-truncate">{{trans('admin.phase_start_date')}}</th>
                <th>{{trans('admin.phase_end_date')}}</th>
                <th>{{trans('admin.phase_approved')}}</th>
                <th>{{trans('admin.actions')}}</th>
            </tr>
        </thead>
        <tbody>
            @php
                $session_currency = currencySymbol(session('currency'));
            @endphp
            @forelse ($data['project']->phases as $phase)
                <tr>
                    <td>{{$phase->name}}</td>
                    <td>{{generalExchange($phase->cost, $phase->project->currency->symbol,  $session_currency)}} {{ $session_currency}}</td>
                    <td>{{$phase->start_date}}</td>
                    <td>{{$phase->end_date}}</td>
                    <td>{{$phase->approved ? trans('admin.approved') : trans('admin.not_approved')}}</td>
                    <td>
                     
                        <a  href="{{route('admin.phases.show', $phase->id)}}" title="{{trans('admin.show')}}">
                            <i data-feather="eye" class="mr-50"></i>
                        </a>

                        <a  href="{{route('admin.phases.edit', $phase->id)}}" title="{{trans('admin.edit')}}">
                            <i data-feather="edit-2" class="mr-50"></i>
                        </a>
                        <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.phases.destroy', $phase->id)}}" title="{{trans('admin.delete')}}">
                            <i data-feather="trash" class="mr-50"></i>
                        </a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="6">{{trans('admin.no_phases_registered')}}</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    </div>
    <!--project phases form ends -->
</div>