<div class="tab-pane active" id="basic_info" aria-labelledby="basic_info-tab" role="tabpanel">
    <form id="basic_info_form" class="basic_info_form" method="POST" action="{{route('admin.projects.update', $data['project']->id)}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        
        <input type="hidden" id="edit" value="true">
        
        <div class="row">

            {{-- project_num start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="project_num">{{trans('admin.project_num')}}</label>
                    <input type="text" class="form-control" placeholder="{{trans('admin.project_num')}}" value="{{$data['project']->project_num ? $data['project']->project_num : old('project_num')}}" name="project_num" id="project_num" readonly />
                    <span class="error-input error-project_num"></span>
                </div>
            </div>
            {{-- project_num end --}}

            {{-- charity_id start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="charity_id">{{trans('admin.charity_name')}}</label>
                    <select name="charity_id" id="charity_id" class="form-control" disabled required>
                        <option value="{{$data['project']->charity_id}}">{{$data['project']->charity ? $data['project']->charity->name : trans('admin.no_data')}}</option>
                        <span class="error-input error-charity_id"></span>
                    </select>
                </div>
            </div>
            {{-- charity_id end --}}

            {{-- name start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">{{trans('admin.project_name')}}</label>
                    <input type="text" class="form-control" placeholder="{{trans('admin.name')}}" value="{{$data['project']->name ? $data['project']->name : old('name')}}" name="name" id="name" />
                    <span class="error-input error-name"></span>
                </div>
            </div>
            {{-- name end --}}

            {{-- start_date start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="start_date">{{trans('admin.project_start_date')}}</label>
                    <input id="start_date" type="text" class="form-control date-picker" readonly  autocomplete="off" name="start_date" placeholder="{{trans('admin.start_date')}}" value="{{$data['project']->start_date ? $data['project']->start_date : old('start_date')}}" required />
                    <span class="error-input error-start_date"></span>
                </div>
            </div>
            {{-- start_date end --}}

            {{-- duration start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="duration">{{trans('admin.project_duration')}}</label>
                    <input type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.duration')}}" value="{{$data['project']->duration && $data['project']->duration != null ? $data['project']->duration : 1}}" name="duration" id="duration" readonly/>
                    <span class="error-input error-duration"></span>
                </div>  
            </div>
            {{-- duration end --}}

            {{-- cost start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="cost">{{trans('admin.cost').' '.trans('admin.cost_note')}}  ({{$data['project']->currency->symbol}})</label>
                    <input type="number" min="1" step=".01" class="form-control" placeholder="{{trans('admin.cost')}}" value="{{$data['project']->cost ? $data['project']->cost : old('cost')}}" name="cost" id="cost"/>
                    <span class="error-input error-cost_note"></span>
                </div>
            </div>
            {{-- cost end --}}

            {{-- currency_id start  --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="currency_id">{{trans('admin.currency')}}</label>
                    <select name="currency_id" id="currency_id" class="form-control" required>
                        @foreach ($data['currencies'] as $currency)
                            <option value="{{$currency->id}}" @if($data['project']->currency_id == $currency->id || old('currency_id') == $currency->id) selected @endif>{{$currency->name}}</option>
                            <span class="error-input error-currency_id"></span>
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- currency_id end --}}

            {{-- benef_num start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="benef_num">{{trans('admin.benef_num')}}</label>
                    <input type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.benef_num')}}" value="{{$data['project']->benef_num ? $data['project']->benef_num : old('benef_num')}}" name="benef_num" id="benef_num" required/>
                    <span class="error-input error-benef_num"></span>
                </div>
            </div>
            {{-- benef_num end --}}

            {{-- status start --}}
            <div class="col-md-2">
                <div class="form-group">
                    <label for="status">{{trans('admin.project_status')}}</label>
                    <select id="status" class="form-control" name="status">
                        <option value="waiting" @if($data['project']->status == 'waiting' || old('status') == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                        <option value="approved" @if($data['project']->status == 'approved' || old('status') == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                        <option value="rejected" @if($data['project']->status == 'rejected' || old('status') == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                        <option value="hold" @if($data['project']->status == 'hold' || old('status') == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                    </select>
                    <span class="error-input error-status"></span>
                </div>
            </div>
            {{-- status end --}}

            {{-- notes start --}}
            <div class="col-md-2">
                <div class="form-group">
                    <a class="btn btn-outline-info btn_02" data-toggle="modal" data-target="#info">{{trans('admin.notes')}}</a>
                </div>
                <!-- Modal -->
                <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="notes">{{trans('admin.notes')}}</label>
                                    <textarea id="notes" class="form-control" name="notes" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$data['project']->notes ? $data['project']->notes : old('notes')}}</textarea>
                                    <span class="error-input error-notes"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="save_status" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- notes end --}}

            {{-- charity_category_id start --}}
            <div class="col-md-12">
                <div class="form-group">
                    <label for="charity_category_id">{{trans('admin.category')}}</label>
                    <select class="form-control" id="charity_category_id" name="charity_category_id" 
                        data-action="{{route('get-category-services')}}" data-method="POST" required>
                        @if($data['project']->charity)
                            @foreach ($data['project']->charity->categories as $category)
                                <option value="{{$category->id}}" data-type="{{$category->type}}" @if($data['project']->charity_category_id == $category->id || old('charity_category_id') == $category->id) selected @endif>{{$category->name}}</option>
                            @endforeach
                        @else
                            <option value="">{{trans('admin.no_data')}}</option>
                        @endif
                    </select>
                    <span class="error-input error-charity_category_id"></span>
                </div>
            </div>
            {{-- charity_category_id end --}}

            {{-- eng_maps start --}}
            {{-- @if($data['project']->category->type == 'engineering') --}}
                <div class="col-md-12">
                    <div class="form-group @if($data['project']->eng_maps == null) hidden @endif" id="eng_maps_container">
                        <label for="select-country">{{ __('admin.eng_maps') }}</label>
                        <input type="file" class="form-control" name="eng_maps" id="eng_maps" accept=",.png,.jpg,.jpeg,.pdf" value="{{old('eng_maps')}}">
                        <small>(Max size: 20MG)</small>
                        @if ($errors->has('eng_maps'))
                            <span class="error-input">{{ $errors->first('eng_maps') }}</span>
                        @endif
                        @if($data['project']->eng_maps != null)
                            <img src="{{asset('storage/'.$data['project']->eng_maps)}}" width="100" height="100" alt="engineering maps">
                        @endif
                        <span class="error-input error-eng_maps"></span>
                    </div>
                </div>
            {{-- @endif --}}
            {{-- eng_maps end --}}

            {{-- @if($data['project']->category && ($data['project']->category->type == 'educational' ||
                $data['project']->category->type == 'sponsorship' || $data['project']->category->type == 'emergency' ||
                $data['project']->category->type == 'sustainable')) --}}
                {{-- service_option_id start --}}
                <div class="col-md-12">
                    <div id="services" class="form-group

                        @if($data['project']->category && ($data['project']->category->type != 'educational' &&
                        $data['project']->category->type != 'sponsorship' && $data['project']->category->type != 'emergency' &&
                        $data['project']->category->type != 'sustainable')) hidden @endif">

                        <label for="select-country">{{ trans('admin.service') }}</label>
                        <select class="form-control select2" id="service_option_id" data-action="{{route('get-service-features')}}" 
                            data-method="POST" name="service_option_id" required>
                            <option value="">{{trans('admin.select')}}...</option>
                            @foreach($data['services'] as $service)
                                <option value="{{ $service->id }}"
                                    @if($data['project']->service_option_id == $service->id) selected @endif>{{ $service->name }}</option>
                            @endforeach
                            @if($data['project']->category && ($data['project']->category->type != 'educational' &&
                                $data['project']->category->type != 'sponsorship' && $data['project']->category->type != 'emergency' &&
                                $data['project']->category->type != 'sustainable') && $data['project']->category->type != 'engineering' &&
                                $data['project']->category->type != null))
                                <option value="other"
                                    @if($data['project']->service_option_id == null) selected @endif>{{trans('admin.other')}}</option>
                            @endif
                        </select>
                        <span class="error-input error-service_option_id"></span>
                    </div>
                </div>
                {{-- service_option_id end --}}

                {{-- other_service_option start --}}
                <div class="col-md-12">
                    <div class="form-group @if($data['project']->other_service_option == null) hidden @endif" id="other_service_option_container">
                        <label for="select-country">{{ __('admin.other_service_option') }}</label>
                        <input type="text" class="form-control" name="other_service_option" id="other_service_option" value="{{$data['project']->other_service_option}}" required>
                        <span class="error-input error-other_service_option"></span>
                    </div> 
                </div>
                {{-- other_service_option end --}}

                {{-- service_feature_id start --}}
                <div class="col-md-12">
                    <div id="features" class="form-group

                        @if($data['project']->category && ($data['project']->category->type != 'educational' &&
                        $data['project']->category->type != 'sponsorship' && $data['project']->category->type != 'emergency' &&
                        $data['project']->category->type != 'sustainable')) hidden @endif">

                        @if($data['project']->service_feature_id != null)
                            <label for="select-country">{{ __('admin.feature') }}</label>
                            <select class="form-control select2" id="service_feature_id" name="service_feature_id" required>
                                <option value="">{{trans('admin.select')}}...</option>
                                @foreach($data['features'] as $feature)
                                    <option value="{{ $feature->id }}"
                                        @if($data['project']->service_feature_id == $feature->id) selected @endif>{{ $feature->name }}</option>
                                @endforeach
                                <option value="other" @if($data['project']->service_feature_id == null) selected @endif>{{trans('admin.other')}}</option>
                            </select>
                            <span class="error-input error-service_feature_id"></span>
                        @endif
                    </div>
                </div>
                {{-- service_feature_id end --}}

                {{-- other_service_feature start --}}
                <div class="col-md-12">
                    <div class="form-group @if($data['project']->other_service_feature == null) hidden @endif" id="other_service_feature_container">
                        <label for="select-country">{{ __('admin.other_service_feature') }}</label>
                        <input type="text" class="form-control" name="other_service_feature" id="other_service_feature" value="{{$data['project']->other_service_feature}}" required>
                        <span class="error-input error-other_service_feature"></span>
                    </div>
                </div>
                {{-- other_service_feature end --}}
            {{-- @endif --}}

            {{-- country_id start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="country">{{trans('admin.country')}}</label>
                    <select id="country" class="form-control" name="country" data-action="{{ route('admin.city.country') }}" required>
                        <option value="" disabled>{{trans('admin.select')}}</option>
                        @foreach ($data['countries'] as $key => $country)
                            <option value="{{$key}}" @if($data['project']->country == $key || old('country') == $key) selected @endif>{{$country}}</option>
                        @endforeach
                    </select>
                    <span class="error-input error-country"></span>
                </div>
            </div>
            {{-- country_id end --}}

            {{-- city_id start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="city">{{trans('admin.city')}}</label>
                    <input type="text" name="city" id="city" class="form-control" placeholder="{{trans('admin.city')}}" value="{{$data['project']->city ? $data['project']->city : old('city')}}">
                    <span class="error-input error-city"></span>
                </div>
            </div>
            {{-- city_id end --}}

            {{-- main image start --}}
            
            <div class="col-md-12">
                <div class="form-group">
                    <label for="image">{{trans('admin.main_image')}}
                        (Height:200px / Min-width:400px)
                    </label>
                    <div class="custom-file">
                    <input type="file" id="image" class="form-control image" accept=".gif, .jpg, .png, .webp" name="image">
                    <small>(Max size: 20MG)</small>
                    <span class="error-input error-image"></span>
                    </div>
                    <div class="form-group prev" style="display: inline-block" >
                        <img src="{{asset('storage/'.$data['project']->image)}}" style="width: 100px" class="img-thumbnail preview-image" alt="{{$data['project']->name}}">
                    </div>       
                </div>
            </div>
            {{-- main inage end --}}

            {{-- location start --}}
            <div class="col-md-12">
                <!-- LOCATION -->
                <div class="form-group" style="position: relative">
                    <label class="form-label" for="location">{{ __('admin.project_location') }}</label>
                    <input type="text" id="location" value="{{$data['project']->location}}" name="location" class="form-control" placeholder="{{ __('admin.location') }}" />
                    <span class="error-input"></span>
                    <span class="error-input error"></span>
                    <input type="hidden" name="lat" value="{{$data['project']->lat}}" id="lat">
                    <input type="hidden" name="lng" value="{{$data['project']->lng}}" id="lng">
                    <span class="error-input error-location"></span>
                    <span class="error-input error-lat"></span>
                    <span class="error-input error-lng"></span>
                    <i class="fa fa-map-marker-alt" id="myloc" title="تحديد موقعك الحالي"></i>
                    <div id="map" style="width:100%;height:500px;"></div>
                </div>
            </div>
            {{-- location end --}}
            
            <div class="col-12 error text-center general_error"></div>
            <div class="col-12 mt-2" id="loading">
                <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_basic_info_button">{{trans('admin.save')}}</button>
                <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
            </div>
        </div>
    </form>
</div>