<div class="tab-pane" id="sponsers" aria-labelledby="sponsers-tab" role="tabpanel">
    <!-- sponsers form start -->
    <div class="row">
        <a href="{{route('admin.sponsers.create', $data['project']->id)}}" class="btn btn-primary btn_01"><i  data-feather="plus" aria-hidden="true"></i></a>
    </div>

    <div class="card-datatable table-responsive">
        <table class="datatables-basic table">
        <thead>
            <tr>
                <th>{{trans('admin.name_ar')}}</th>
                <th>{{trans('admin.name_en')}}</th>
                <th>{{trans('admin.work_field')}}</th>
                <th>{{trans('admin.actions')}}</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data['project']->sponsers as $sponser)
                <tr>
                    <form id="sponsers_form" method="POST" action="{{route('admin.sponsers.update', $sponser->id)}}">
                        @csrf
                        @method('PUT')

                        <input type="hidden" name="project_id" value="{{$data['project']->id}}">

                        <td>
                            <div class="form-group">
                                <input type="text" name="sponser_name_ar" id="name{{$sponser->id}}" class="form-control" placeholder="{{trans('admin.name_ar')}}" value="{{$sponser->name_ar}}">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <input type="text" name="sponser_name_en" id="name{{$sponser->id}}" class="form-control" placeholder="{{trans('admin.name_en')}}" value="{{$sponser->name_ar}}">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <select name="sponser_work_field_id" id="work_field{{$sponser->id}}" class="form-control">
                                    @foreach ($data['work_fields'] as $field)
                                        <option value="{{$field->id}}" @if($sponser->work_field_id == $field->id) selected @endif>{{$field->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">{{trans('admin.save')}}</button>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-outline-danger remove-table" href="javascript:void(0);" data-url="{{route('admin.sponsers.destroy', $sponser->id)}}">
                                        <i data-feather="trash" class="mr-50"></i>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </form>
                </tr>
            @empty
            <tr>
                <td colspan="2">{{trans('admin.no_data')}}</td>
            </tr>
            
        @endforelse
        </tbody>
    </table>
    </div>
    <!-- sponsers form ends -->
</div>