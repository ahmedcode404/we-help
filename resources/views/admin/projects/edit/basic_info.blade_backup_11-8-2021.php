<div class="tab-pane active" id="basic_info" aria-labelledby="basic_info-tab" role="tabpanel">
    <form id="basic_info_form" method="POST" action="{{route('admin.projects.update', $data['project']->id)}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row">

            {{-- project_num start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="project_num">{{trans('admin.project_num')}}</label>
                    <input type="text" class="form-control" placeholder="{{trans('admin.project_num')}}" value="{{$data['project']->project_num ? $data['project']->project_num : old('project_num')}}" name="project_num" id="project_num" readonly />
                </div>
            </div>
            {{-- project_num end --}}

            {{-- charity_id start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="charity_id">{{trans('admin.charity')}}</label>
                    <select name="charity_id" id="charity_id" class="form-control" disabled required>
                        <option value="{{$data['project']->charity_id}}">{{$data['project']->charity ? $data['project']->charity->name : trans('admin.no_data')}}</option>
                    </select>
                </div>
            </div>
            {{-- charity_id end --}}

            {{-- name start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="name">{{trans('admin.name')}}</label>
                    <select name="name" id="name" class="form-control" required>
                        <option value="health" @if($data['project']->name == "health" || old('name') == "health") selected @endif>{{trans('admin.health')}}</option>
                        <option value="medical" @if($data['project']->name == "medical" || old('name') == "medical") selected @endif>{{trans('admin.medical')}}</option>
                        <option value="drilling" @if($data['project']->name == "drilling" || old('name') == "drilling") selected @endif>{{trans('admin.drilling')}}</option>
                        <option value="sponsoring" @if($data['project']->name == "sponsoring" || old('name') == "sponsoring") selected @endif>{{trans('admin.sponsoring')}}</option>
                        <option value="other" @if($data['project']->name == "other" || old('name') == "other") selected @endif>{{trans('admin.other')}}</option>
                    </select>
                </div>
            </div>
            {{-- name end --}}

            {{-- start_date start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="start_date">{{trans('admin.start_date')}}</label>
                    <input id="start_date" type="text" class="form-control date-picker" readonly  autocomplete="off" name="start_date" placeholder="{{trans('admin.start_date')}}" value="{{$data['project']->start_date ? $data['project']->start_date : old('start_date')}}" required />
                </div>
            </div>
            {{-- start_date end --}}

            {{-- duration start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="duration">{{trans('admin.duration')}}</label>
                    <input type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.duration')}}" value="{{$data['project']->duration && $data['project']->duration != null ? $data['project']->duration : 1}}" name="duration" id="duration" readonly/>
                </div>
            </div>
            {{-- duration end --}}

            {{-- cost start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="cost">{{trans('admin.cost')}} ({{$data['project']->currency->symbol}})</label>
                    <input type="number" min="1" step="0.1" class="form-control" placeholder="{{trans('admin.cost')}}" value="{{$data['project']->cost ? $data['project']->cost : old('cost')}}" name="cost" id="cost" readonly/>
                </div>
            </div>
            {{-- cost end --}}

            {{-- currency_id start  --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="currency_id">{{trans('admin.currency')}}</label>
                    <select name="currency_id" id="currency_id" class="form-control" required>
                        @foreach ($data['currencies'] as $currency)
                            <option value="{{$currency->id}}" @if($data['project']->currency_id == $currency->id || old('currency_id') == $currency->id) selected @endif>{{$currency->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- currency_id end --}}

            {{-- benef_num start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="benef_num">{{trans('admin.benef_num')}}</label>
                    <input type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.benef_num')}}" value="{{$data['project']->benef_num ? $data['project']->benef_num : old('benef_num')}}" name="benef_num" id="benef_num" required/>
                </div>
            </div>
            {{-- benef_num end --}}

            {{-- charity_category_id start --}}
            <input type="hidden" name="type" id="type" value="{{$data['project']->category ? $data['project']->category->type : null}}">

            <div class="col-md-4">
                <div class="form-group">
                    <label for="charity_category_id">{{trans('admin.category')}}</label>
                    <select class="form-control" id="charity_category_id" name="charity_category_id" required>
                        @if($data['project']->charity)
                            @foreach ($data['project']->charity->categories as $category)
                                <option value="{{$category->id}}" data-type="{{$category->type}}" @if($data['project']->charity_category_id == $category->id || old('charity_category_id') == $category->id) selected @endif>{{$category->name}}</option>
                            @endforeach
                        @else
                            <option value="">{{trans('admin.no_data')}}</option>
                        @endif
                    </select>
                </div>
            </div>
            {{-- charity_category_id end --}}

            @if($data['project']->category)
                {{-- edu_service_id start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'educational') hidden @endif" id="edu_service_id_container_1">
                    <div class="form-group">
                        <label for="edu_service_id">{{trans('admin.service')}}</label>
                        <select class="form-control" id="edu_service_id" name="edu_service_id">
                            @foreach ($data['services'] as $service)
                                @if($service->type == 'edu')
                                    <option value="{{$service->id}}" @if($data['project']->edu_service_id == $service->id || old('edu_service_id') == $service->id) selected @endif>{{$service->name}}</option>
                                @endif
                            @endforeach
                            <option value="other">{{trans('admin.other')}}</option>
                        </select>
                    </div>
                </div>
                {{-- edu_service_id end --}}

                {{-- edu_service_other start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'educational' || $data['project']->edu_service_other == null) hidden @endif" id="edu_service_id_container_2">
                    <div class="form-group">
                        <label for="edu_service_other">{{trans('admin.edu_service_other')}}</label>
                        <input type="text" name="edu_service_other" id="edu_service_other" class="form-control" placeholder="{{trans('admin.edu_service_other')}}" value="{{$data['project']->edu_service_other ? $data['project']->edu_service_other : old('edu_service_other')}}">
                    </div>
                </div>
                {{-- edu_service_other end --}}

                {{-- stage start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'educational') hidden @endif" id="edu_service_id_container_3">
                    <div class="form-group">
                        <label for="stage">{{trans('admin.stage')}}</label>
                        <select class="form-control" id="stage" name="stage">
                            <option value="primary" @if($data['project']->stage == 'primary' || old('satge') == 'primary') selected @endif>{{trans('admin.primary')}}</option>
                            <option value="intermediate" @if($data['project']->stage == 'intermediate' || old('satge') == 'intermediate') selected @endif>{{trans('admin.intermediate')}}</option>
                            <option value="secondary" @if($data['project']->stage == 'secondary' || old('satge') == 'secondary') selected @endif>{{trans('admin.secondary')}}</option>
                        </select>
                    </div>
                </div>
                {{-- stage end  --}}

                {{-- eng_maps start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'engineering') hidden @endif" id="eng_maps_container">
                    <div class="form-group">
                        <label for="eng_maps">{{trans('admin.eng_maps')}}</label>
                        <input type="file" class="form-control" placeholder="{{trans('admin.eng_maps')}}" name="eng_maps" id="eng_maps" accept="image/png, image/jpeg, image/jpg"/>
                        <img src="{{asset('storage/'.$data['project']->eng_maps)}}" width="100" height="100" alt="{{$data['project']->name}}">
                    </div>
                </div>
                {{-- eng_maps end --}}

                {{-- care_type start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'sponsorship') hidden @endif" id="care_type_container">
                    <div class="form-group">
                        <label for="care_type">{{trans('admin.care_type')}}</label>
                        <select class="form-control" id="care_type" name="care_type">
                            <option value="warranty" @if($data['project']->care_type == 'warranty' || old('care_type') == 'warranty') selected @endif>{{trans('admin.warranty')}}</option>
                            <option value="clothing" @if($data['project']->care_type == 'clothing' || old('care_type') == 'clothing') selected @endif>{{trans('admin.clothing')}}</option>
                            <option value="feast" @if($data['project']->care_type == 'feast' || old('care_type') == 'feast') selected @endif>{{trans('admin.feast')}}</option>
                        </select>
                    </div>
                </div>
                {{-- care_type end --}}

                {{-- care_duration start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'sponsorship') hidden @endif" id="care_duration_container">
                    <div class="form-group">
                        <label for="care_duration">{{trans('admin.care_duration')}}</label>
                        <select class="form-control" id="care_duration" name="care_duration">
                            <option value="annual" @if($data['project']->care_duration == 'annual' || old('care_duration') == 'annual') selected @endif>{{trans('admin.annual')}}</option>
                            <option value="cut_off" @if($data['project']->care_duration == 'cut_off' || old('care_duration') == 'cut_off') selected @endif>{{trans('admin.cut_off')}}</option>
                        </select>
                    </div>
                </div>
                {{-- care_duration end --}}

                {{-- emerg_service_id start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'emergency') hidden @endif" id="emerg_service_id_container_1">
                    <div class="form-group">
                        <label for="emerg_service_id">{{trans('admin.service')}}</label>
                        <select class="form-control" id="emerg_service_id" name="emerg_service_id" required>
                            @foreach ($data['services'] as $service)
                                @if($service->type == 'emerg')
                                    <option value="{{$service->id}}" @if($data['project']->emerg_service_id == $service->id || old('emerg_service_id') == $service->id) selected @endif>{{$service->name}}</option>
                                @endif
                            @endforeach
                            <option value="other">{{trans('admin.other')}}</option>
                        </select>
                    </div>
                </div>
                {{-- emerg_service_id end --}}

                {{-- emerg_service_other start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'emergency' || $data['project']->emerg_service_other == null) hidden @endif" id="emerg_service_id_container_2">
                    <div class="form-group">
                        <label for="emerg_service_other">{{trans('admin.emerg_service_other')}}</label>
                        <input type="text" name="emerg_service_other" id="emerg_service_other" class="form-control" placeholder="{{trans('admin.emerg_service_other')}}" value="{{$data['project']->emerg_service_other ? $data['project']->emerg_service_other : old('emerg_service_other')}}">
                    </div>
                </div>
                {{-- emerg_service_other end --}}

                {{-- stable_project start --}}
                <div class="col-md-4 @if($data['project']->category->type != 'sustainable') hidden @endif" id="stable_project_container">
                    <div class="form-group">
                        <label for="stable_project">{{trans('admin.stable_project')}}</label>
                        <select class="form-control" id="stable_project" name="stable_project">
                            <option value="debt" @if($data['project']->stable_project == 'debt' || old('stable_project') == 'debt') selected @endif>{{trans('admin.debt')}}</option>
                            <option value="loan" @if($data['project']->stable_project == 'loan' || old('stable_project') == 'loan') selected @endif>{{trans('admin.loan')}}</option>
                            <option value="donation" @if($data['project']->stable_project == 'donation' || old('stable_project') == 'donation') selected @endif>{{trans('admin.donation')}}</option>
                        </select>
                    </div>
                </div>
                {{-- stable_project end --}}

            @else
                {{-- edu_service_id start --}}
                <div class="col-md-4 hidden" id="edu_service_id_container_1">
                    <div class="form-group">
                        <label for="edu_service_id">{{trans('admin.service')}}</label>
                        <select class="form-control" id="edu_service_id" name="edu_service_id">
                            @foreach ($data['services'] as $service)
                                @if($service->type == 'edu')
                                    <option value="{{$service->id}}" @if($data['project']->edu_service_id == $service->id || old('edu_service_id') == $service->id) selected @endif>{{$service->name}}</option>
                                @endif
                            @endforeach
                            <option value="other">{{trans('admin.other')}}</option>
                        </select>
                    </div>
                </div>
                {{-- edu_service_id end --}}

                {{-- edu_service_other start --}}
                <div class="col-md-4 hidden" id="edu_service_id_container_2">
                    <div class="form-group">
                        <label for="edu_service_other">{{trans('admin.edu_service_other')}}</label>
                        <input type="text" name="edu_service_other" id="edu_service_other" class="form-control" placeholder="{{trans('admin.edu_service_other')}}" value="{{$data['project']->edu_service_other ? $data['project']->edu_service_other : old('edu_service_other')}}">
                    </div>
                </div>
                {{-- edu_service_other end --}}

                {{-- stage start --}}
                <div class="col-md-4 hidden" id="edu_service_id_container_3">
                    <div class="form-group">
                        <label for="stage">{{trans('admin.stage')}}</label>
                        <select class="form-control" id="stage" name="stage">
                            <option value="primary" @if($data['project']->stage == 'primary' || old('satge') == 'primary') selected @endif>{{trans('admin.primary')}}</option>
                            <option value="intermediate" @if($data['project']->stage == 'intermediate' || old('satge') == 'intermediate') selected @endif>{{trans('admin.intermediate')}}</option>
                            <option value="secondary" @if($data['project']->stage == 'secondary' || old('satge') == 'secondary') selected @endif>{{trans('admin.secondary')}}</option>
                        </select>
                    </div>
                </div>
                {{-- stage end  --}}

                {{-- eng_maps start --}}
                <div class="col-md-4 hidden" id="eng_maps_container">
                    <div class="form-group">
                        <label for="eng_maps">{{trans('admin.eng_maps')}}</label>
                        <input type="file" class="form-control" placeholder="{{trans('admin.eng_maps')}}" name="eng_maps" id="eng_maps" accept="image/png, image/jpeg, image/jpg"/>
                        <img src="{{asset('storage/'.$data['project']->eng_maps)}}" width="100" height="100" alt="{{$data['project']->name}}">
                    </div>
                </div>
                {{-- eng_maps end --}}

                {{-- care_type start --}}
                <div class="col-md-4 hidden" id="care_type_container">
                    <div class="form-group">
                        <label for="care_type">{{trans('admin.care_type')}}</label>
                        <select class="form-control" id="care_type" name="care_type">
                            <option value="warranty" @if($data['project']->care_type == 'warranty' || old('care_type') == 'warranty') selected @endif>{{trans('admin.warranty')}}</option>
                            <option value="clothing" @if($data['project']->care_type == 'clothing' || old('care_type') == 'clothing') selected @endif>{{trans('admin.clothing')}}</option>
                            <option value="feast" @if($data['project']->care_type == 'feast' || old('care_type') == 'feast') selected @endif>{{trans('admin.feast')}}</option>
                        </select>
                    </div>
                </div>
                {{-- care_type end --}}

                {{-- care_duration start --}}
                <div class="col-md-4 hidden" id="care_duration_container">
                    <div class="form-group">
                        <label for="care_duration">{{trans('admin.care_duration')}}</label>
                        <select class="form-control" id="care_duration" name="care_duration">
                            <option value="annual" @if($data['project']->care_duration == 'annual' || old('care_duration') == 'annual') selected @endif>{{trans('admin.annual')}}</option>
                            <option value="cut_off" @if($data['project']->care_duration == 'cut_off' || old('care_duration') == 'cut_off') selected @endif>{{trans('admin.cut_off')}}</option>
                        </select>
                    </div>
                </div>
                {{-- care_duration end --}}

                {{-- emerg_service_id start --}}
                <div class="col-md-4 hidden" id="emerg_service_id_container_1">
                    <div class="form-group">
                        <label for="emerg_service_id">{{trans('admin.service')}}</label>
                        <select class="form-control" id="emerg_service_id" name="emerg_service_id" required>
                            @foreach ($data['services'] as $service)
                                @if($service->type == 'emerg')
                                    <option value="{{$service->id}}" @if($data['project']->emerg_service_id == $service->id || old('emerg_service_id') == $service->id) selected @endif>{{$service->name}}</option>
                                @endif
                            @endforeach
                            <option value="other">{{trans('admin.other')}}</option>
                        </select>
                    </div>
                </div>
                {{-- emerg_service_id end --}}

                {{-- emerg_service_other start --}}
                <div class="col-md-4 hidden" id="emerg_service_id_container_2">
                    <div class="form-group">
                        <label for="emerg_service_other">{{trans('admin.emerg_service_other')}}</label>
                        <input type="text" name="emerg_service_other" id="emerg_service_other" class="form-control" placeholder="{{trans('admin.emerg_service_other')}}" value="{{$data['project']->emerg_service_other ? $data['project']->emerg_service_other : old('emerg_service_other')}}">
                    </div>
                </div>
                {{-- emerg_service_other end --}}

                {{-- stable_project start --}}
                <div class="col-md-4 hidden" id="stable_project_container">
                    <div class="form-group">
                        <label for="stable_project">{{trans('admin.stable_project')}}</label>
                        <select class="form-control" id="stable_project" name="stable_project">
                            <option value="debt" @if($data['project']->stable_project == 'debt' || old('stable_project') == 'debt') selected @endif>{{trans('admin.debt')}}</option>
                            <option value="loan" @if($data['project']->stable_project == 'loan' || old('stable_project') == 'loan') selected @endif>{{trans('admin.loan')}}</option>
                            <option value="donation" @if($data['project']->stable_project == 'donation' || old('stable_project') == 'donation') selected @endif>{{trans('admin.donation')}}</option>
                        </select>
                    </div>
                </div>
                {{-- stable_project end --}}
            @endif

            {{-- status start --}}
            <div class="col-md-2">
                <div class="form-group">
                    <label for="status">{{trans('admin.status')}}</label>
                    <select id="status" class="form-control" name="status">
                        <option value="waiting" @if($data['project']->status == 'waiting' || old('status') == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                        <option value="approved" @if($data['project']->status == 'approved' || old('status') == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                        <option value="rejected" @if($data['project']->status == 'rejected' || old('status') == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                        <option value="hold" @if($data['project']->status == 'hold' || old('status') == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                    </select>
                </div>
            </div>
            {{-- status end --}}

            {{-- notes start --}}
            <div class="col-md-2">
                <div class="form-group">
                    <a class="btn btn-outline-info btn_02" data-toggle="modal" data-target="#info">{{trans('admin.notes')}}</a>
                </div>
                <!-- Modal -->
                <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="notes">{{trans('admin.notes')}}</label>
                                    <textarea id="notes" class="form-control" name="notes" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$data['project']->notes ? $data['project']->notes : old('notes')}}</textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="save_status" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- notes end --}}

            {{-- country_id start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="country_id">{{trans('admin.country_id')}}</label>
                    <select id="country_id" class="form-control" name="country_id" data-action="{{ route('admin.city.country') }}" required>
                        <option value="" disabled>{{trans('admin.select')}}</option>
                        @foreach ($data['countries'] as $country)
                            @if($country->parent_id == null)
                                <option value="{{$country->id}}" @if($data['project']->country_id == $country->id || old('country_id') == $country->id) selected @endif>{{$country->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- country_id end --}}

            {{-- city_id start --}}
            <div class="col-md-4 append-city">
                <div class="form-group">
                    <label for="city_id">{{trans('admin.city')}}</label>
                    <select id="city_id" class="form-control" name="city_id" required>
                        <option value="" disabled>{{trans('admin.select')}}</option>
                        @foreach ($data['project']->country->cities as $city)
                            @if($city->parent_id != null)
                                <option value="{{$city->id}}" @if($data['project']->city_id == $city->id || old('city_id') == $city->id) selected @endif>{{$city->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            {{-- city_id end --}}

            {{-- main image start --}}
            <div class="col-md-12">
                <div class="form-group">
                    <label for="image">{{trans('admin.main_image')}}</label>
                    <input type="file" id="main_image" class="form-control" accept=".gif, .jpg, .png, .webp" name="image">
                </div>
                <h3>{{trans('admin.main_image')}}</h3>
                <div class="card">
                    <div class="card-body">
                        <img src="{{asset('storage/'.$data['project']->image)}}" width="200" height="200" alt="{{$data['project']->name}}">
                    </div>
                </div>
            </div>
            {{-- main inage end --}}

            {{-- location start --}}
            <div class="col-md-12">
                <!-- LOCATION -->
                <div class="form-group" style="position: relative">
                    <label class="form-label" for="location">{{ __('admin.location') }}</label>
                    <input type="text" id="location" value="{{$data['project']->location}}" name="location" class="form-control" placeholder="{{ __('admin.location') }}" />
                    <span class="error-input"></span>
                    <span class="error-input error"></span>
                    <input type="hidden" name="lat" value="{{$data['project']->lat}}" id="lat">
                    <input type="hidden" name="lng" value="{{$data['project']->lng}}" id="lng">
                    <i class="fa fa-map-marker-alt" id="myloc" title="تحديد موقعك الحالي"></i>
                       <div id="map" style="width:100%;height:500px;"></div>
                </div>
            </div>
            {{-- location end --}}

            <div class="col-12 mt-2">
                <button type="submit" class="btn btn-primary  mb-sm-0 mr-0 mr-sm-1">{{trans('admin.save')}}</button>
                <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
            </div>
        </div>
    </form>
</div>
