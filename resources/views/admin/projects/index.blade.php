@extends('layout.app')

@section('title', trans('admin.projects'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')
<style>
    .table-responsive {
        overflow-x: auto;
    -webkit-overflow-scrolling: touch;
}
</style>
<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="text-md-left text-center mb-2">

                    @if(checkPermissions(['create_project']))
                        <a href="{{route('admin.projects.create')}}" class="btn btn-primary">{{trans('admin.create_project')}}</a>
                    @endif
                    
                    <a href="{{route('admin.export-all-projects', request()->segment(count(request()->segments())))}}" class="btn btn-primary">{{trans('admin.export')}}</a>
                </div>

                <div class="table-responsive"> 
                    <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                {{-- <th>{{trans('admin.project_num')}}</th> --}}
                                <th>{{trans('admin.project_name')}}</th>
                                <th>{{trans('admin.category')}}</th>
                                <th>{{trans('admin.project_cost')}}</th>
                                <th>{{trans('admin.superadmin_ratio')}}</th>
                                <th>{{trans('admin.project_active_status')}}</th>
                                <th>{{trans('admin.project_status')}}</th>
                                <th>{{trans('admin.create_phase')}}</th>
                                <th>{{trans('admin.create_report')}}</th>
                                {{-- <th>{{trans('admin.show')}}</th>
                                <th>{{ trans('admin.export') }}</th> --}}
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['projects'] as $project)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    {{-- <td>{{$project->project_num}}</td> --}}
                                    <td>{{$project->name}}</td>
                                    <td>{{$project->category? $project->category->name : ''}}</td>
                                    <td>{{generalExchange($project->get_total, $project->currency->symbol, $data['session_currency'])}} {{$data['session_currency']}}</td>
                                    <td>{{$project->category ? $project->category->superadmin_ratio : 0}} %</td>
                                    <td>
                                        @if($project->active == 1)
                                            <div class="badge badge-success">{{ trans('admin.active') }}</div>
                                        @else
                                            <div class="badge badge-danger">{{ trans('admin.not_active') }}</div>
                                        @endif
                                    </td>
                                    <td>
                                        @if($project->status == 'approved')
                                            <span id="current_status-{{$project->id}}" class="badge badge-light-success">{{trans('admin.'.$project->status)}}</span>
                                        @elseif($project->status == 'waiting')
                                            <span id="current_status-{{$project->id}}" class="badge badge-light-warning">{{trans('admin.'.$project->status)}}</span>
                                        @elseif($project->status == 'hold')
                                            <span id="current_status-{{$project->id}}" class="badge badge-light-info">{{trans('admin.'.$project->status)}}</span>
                                        @elseif($project->status == 'rejected')
                                            <span id="current_status-{{$project->id}}" class="badge badge-light-danger">{{trans('admin.'.$project->status)}}</span>
                                        @endif
                                        {{-- <span id="current_status-{{$project->id}}">{{trans('admin.'.$project->status)}}</span> --}}
                                        {{-- @if($project->status != 'approved')
                                            <a class="btn btn-primary" data-toggle="modal" data-target="#project-{{$project->id}}">{{trans('admin.change')}}</a>
                                            <!-- Modal -->  
                                            <div class="modal fade modal-info text-left" id="project-{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="spinner-grow text-success mr-1 pross processing-div-{{ $project->id }}" style="position: absolute;margin: 26px -30px 37px 242px;z-index: 100;display: none;" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div> 
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="change_status">{{trans('admin.select_status')}}</label>
                                                            <select id="change_project_status-{{$project->id}}" class="form-control">
                                                                <option value="approved" @if($project->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                <option value="rejected" @if($project->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                <option value="hold" @if($project->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="notes">{{trans('admin.notes')}}</label>
                                                            <textarea id="project_notes-{{$project->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <button type="button" id="" class="btn btn-info save_status" data-model="project" data-id="{{$project->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                                    </div>
                                                </div>
                                            </div>                           
                                        @endif --}}
                                    </td>
                                    <td><a href="{{ route('admin.phases.create', $project->id) }}">{{ trans('admin.create_phase') }}</a></td>
                                    <td><a href="{{ route('admin.reports.create-report', $project->id) }}">{{ trans('admin.create_report') }}</a></td>
                                    <td>
                                        @if(checkPermissions(['show_project']))
                                            <a href="{{route('admin.projects.show', $project->id)}}" title=" {{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                        @else
                                            {{-- {{trans('admin.do_not_have_permission')}} --}}
                                        @endif
                                        
                                        <a href="{{route('admin.export-project', $project->id)}}"  title="{{trans('admin.export')}}"></a>
                                        @if(checkPermissions(['edit_project']) || (checkPermissions(['delete_project']) && $project->active == 1) ||
                                            (checkPermissions(['restore_project']) && $project->active == 0))
                                        
                                            {{-- @if(checkPermissions(['show_project']))
                                                <a class="dropdown-item" href="{{route('admin.projects.show', $project->id)}}">
                                                    <i data-feather="eye" class="mr-50"></i>
                                                    <span>{{trans('admin.show')}}</span>
                                                </a>
                                            @endif --}}
                                            @if(checkPermissions(['edit_project']))
                                                <a  href="{{route('admin.projects.edit', $project->id)}}" title="{{trans('admin.edit')}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                </a>
                                            @endif
                                            @if(checkPermissions(['delete_project']) && $project->active == 1)
                                                <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.projects.destroy', $project->id)}}" title="{{trans('admin.delete')}}">
                                                    <i data-feather="trash" class="mr-50"></i>
                                                </a>
                                            @elseif(checkPermissions(['restore_project']) && $project->active == 0)
                                                <a class="restore-table" href="javascript:void(0);" data-url="{{route('admin.projects.restore', $project->id)}}" title="{{trans('admin.activate')}}">
                                                    <i data-feather="activity" class="mr-50"></i>
                                                </a>
                                            @endif
                                            
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            {{$data['projects']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection


@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
    <script src="{{asset('dashboard/forms/projects/index.js')}}"></script>
@endpush