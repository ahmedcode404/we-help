@extends('layout.app')

@section('title', trans('admin.show-project'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section class="app-user-view">

        <input type="hidden" id="edit" value="true">
        
        @php
            $session_currency = currencySymbol(session('currency'));
        @endphp

        <!-- Project base data & Plan Starts -->
        <div class="row">
            <!-- project basic info starts-->
            <div class="col-12">
            <div class="card user-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-6 col-lg-12">
                            <div class="user-avatar-section mb-3">
                                <div class="d-flex justify-content-start">
                                    <div class="d-flex flex-column">
                                        <div class="user-info mb-1">
                                            <h4 class="mb-0">{{$data['project']->name}}</h4>
                                            <span class="card-text">{{$data['project']->charity ? $data['project']->charity->email : trans('admin.no_charity')}}</span>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                            <a href="{{route('admin.projects.edit', $data['project']->id)}}" class="btn btn-primary">{{trans('admin.edit')}}</a>
                                            <button class="btn btn-outline-danger ml-1 remove-table" data-redirect_to_main="project" data-url="{{route('admin.projects.destroy', $data['project']->id)}}">{{trans('admin.delete')}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="d-sm-flex align-items-center user-total-numbers mt-2 d-block">
                                <div class="d-sm-flex align-items-center mr-2 mb-1 d-inline-block">
                                    <div class="color-box">
                                        <i data-feather="star" class="text-primary"></i>
                                    </div>
                                    <div class="ml-sm-1">
                                        <h5 class="mb-0">{{$data['project']->getRatings()}}</h5>
                                        <small>{{trans('admin.ratings')}}</small>
                                    </div>
                                </div>
                                <div class="d-sm-flex align-items-center mr-1 mb-2 d-inline-block">
                                    <div class="color-box">
                                        {{$session_currency}}
                                    </div>
                                    <div class="ml-sm-1">
                                        <h5 class="mb-0">{{$data['project']->total_supports_for_project}}</h5>
                                        <small>{{trans('admin.total_supports')}}</small>
                                    </div>
                                </div>
                                <div class="d-sm-flex align-items-center  mb-1 d-inline-block">
                                    <div class="color-box">
                                        <i data-feather="trending-up" class="text-success"></i>
                                    </div>
                                    <div class="ml-sm-1">
                                        <h5 class="mb-0">{{count($data['project']->supports)}}</h5>
                                        <small>{{trans('admin.total_supporters')}}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                            <div class="user-info-wrapper">
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.project_num')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{$data['project']->project_num}}</p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.category')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{$data['project']->category ?  $data['project']->category->name : ' - '}}</p>
                                </div>
                                @if(($data['project']->category && $data['project']->category->type == 'engineering'))
                                    <div class="d-flex flex-wrap mb-1">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.eng_maps') }}:</span>
                                        </div>
                                        <a href="{{asset('storage/'.$data['project']->eng_maps)}}">{{ trans('admin.eng_maps') }}</a>
                                    </div>
                                    
                                @elseif(($data['project']->service_option_id || $data['project']->other_service_option))

                                    <div class="d-flex flex-wrap mb-1">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.service') }}:</span>
                                        </div>
                                        <p class="card-text mb-0 ml-2">{{$data['project']->service_option ? $data['project']->service_option->name : $data['project']->other_service_option}}</p>
                                    </div>
                                    
                                @endif
                                @if($data['project']->service_feature_id || $data['project']->other_service_feature)
                                    <div class="d-flex flex-wrap mb-1">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.feature') }}:</span>
                                        </div>
                                        <p class="card-text mb-0 ml-2">{{$data['project']->service_feature ? $data['project']->service_feature->name : $data['project']->other_service_feature}}</p>
                                    </div>
                                @endif
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.charity_name')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{$data['project']->charity ? $data['project']->charity->name : ''}}</p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.project_status')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{trans('admin.'.$data['project']->status)}}</p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.country')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{Countries::getOne($data['project']->country, \App::getLocale())}}</p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.city')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{$data['project']->city}}</p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.phone')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{$data['project']->charity ? $data['project']->charity->phone : ''}}</p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.mobile')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{$data['project']->charity ? $data['project']->charity->mobile : ''}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /project basic info Ends-->

            <!-- Project details info  starts-->
            <div class="col-12">
                <div class="card plan-card border-primary">
                    <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                        <h5 class="mb-0">{{trans('admin.basic_info')}}</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-borderless">
                            <tbody>
                                <tr>
                                    <td><div class="badge badge-light-primary">{{trans('admin.project_cost')}}</div></td>
                                    <td>{{generalExchange($data['project']->get_total, $data['project']->currency->symbol, $session_currency)}} {{$session_currency}}</td>
                                </tr>
                                <tr>
                                    <td><div class="badge badge-light-primary">{{trans('admin.superadmin_ratio')}}</div></td>
                                    <td>{{$data['project']->category ? $data['project']->category->superadmin_ratio : 0}} %</td>
                                </tr>
                                <tr>
                                    <td><div class="badge badge-light-primary">{{trans('admin.project_start_date')}}</div></td>
                                    <td>{{$data['project']->start_date}}</td>
                                </tr>
                                <tr>
                                    <td><div class="badge badge-light-primary">{{trans('admin.project_end_date')}}</div></td>
                                    <td>{{getEndDate($data['project']->start_date, $data['project']->duration)}}</td>
                                </tr>
                                <tr>
                                    <td><div class="badge badge-light-primary">{{trans('admin.project_duration')}}</div></td>
                                    <td>{{$data['project']->duration}}</td>
                                </tr>
                                <tr>
                                    <td><div class="badge badge-light-primary">{{trans('admin.benef_num')}}</div></td>
                                    <td>{{$data['project']->benef_num}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /Project details info Ends -->
        </div>
        <!-- Project base data & Plan Ends -->

        <!-- Projetc images starts -->
        <div class="row">
            <div class="col-12">
                <div class="card profile-header mb-2">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.project_images')}}</h4>
                    </div>
                    <div class="card-body">
                        <div id="carousel-keyboard" class="carousel slide" data-keyboard="true">
                            @if(count($data['project']->images) > 1)
                                <ol class="carousel-indicators">
                                    @foreach ($data['project']->images as $image)
                                        <li data-target="#carousel-keyboard" data-slide-to="{{ $loop->iteration-1 }}" @if($loop->iteration == 1) class="active" @endif></li>
                                    @endforeach
                                </ol>
                            @endif
                            <div class="carousel-inner" style="height: auto" role="listbox">
                                @foreach ($data['project']->images as $image)
                                    <div class="carousel-item @if($loop->iteration == 1) active @endif">
                                        <img class="img-fluid"  src="{{asset('storage/'.$image->path)}}" alt="{{$data['project']->name}}" />
                                    </div>
                                @endforeach
                            </div>

                            @if (count($data['project']->images) > 1)

                                <a class="carousel-control-prev" href="#carousel-keyboard" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">{{ trans('admin.Previous') }}</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel-keyboard" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">{{ trans('admin.Next') }}</span>
                                </a>
                                
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Projetc images ends -->

        <!-- Project general data Starts -->
        <div class="row">
            <!-- Goal starts -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.project_goals')}}</h4>
                    </div>
                    <div class="card-body">
                        {!! $data['project']->goal !!}
                    </div>
                </div>
            </div>
            <!-- Goal Ends -->

            <!-- Description starts -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">
                            {{trans('admin.project_desc')}}
                        </h4>
                    </div>
                            {{-- <a class="btn btn-outline-info" data-toggle="modal" data-target="#info">{{trans('admin.full_desc')}}</a> --}}
                            
                            <!-- Modal -->
                            {{-- <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body"> --}}
                                          
                                        {{-- </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                        </div>
                                    </div> --}}
                                {{-- </div>
                            </div> --}}
                    <div class="card-body">
                        <div class="media align-items-center mb-1">
                            {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                            <div class="media-body"><a href="{{asset('storage/'.$data['project']->attach)}}">{{trans('admin.attach')}}</a></div>
                        </div>
                        {!! $data['project']->desc !!}
                        <hr>
                        <div class="form-group">
                            {!! $data['project']->long_desc !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- Description Ends -->

            <div class="col-md-12">
            <!-- eng_maps starts -->
            {{-- مشروع انشائى --}}
            @if ($data['project']->charity && $data['project']->charity->type == 'engineering')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.eng_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.image')}}</div></td>
                                        <td>
                                            <div class="profile-img-container d-flex align-items-center">
                                                <div class="profile-img">
                                                    <img src="{{asset('storage/'.$data['project']->eng_maps)}}" class="rounded img-fluid" alt="{{$data['project']->name}}" />
                                                </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- eng_maps Ends -->

            <!-- edu_project starts -->
            {{-- مشروع تعليمى --}}
            @if ($data['project']->charity && $data['project']->charity->type == 'educational')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.edu_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.stage')}}: {{trans('admin.'.$data['project']->stage)}}</div></td>
                                        <td>{{trans('admin.service')}}: {{$data['project']->edu_services ? $data['project']->edu_services->name : $data['project']->edu_service_other}}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- edu_project Ends -->

            <!-- care_project starts -->
            {{-- مشروع رعاية يتيم --}}
            @if ($data['project']->charity && $data['project']->charity->type == 'sponsorship')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.care_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.care_type')}}</div></td>
                                        <td>{{trans('admin.'.$data['project']->care_type)}}</td>
                                    </tr>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.care_duration')}}</div></td>
                                        <td>{{trans('admin.'.$data['project']->care_duration)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- care_project Ends -->
            
            <!-- emerg_project starts -->
            {{-- مشروع طارئ --}}
            @if ($data['project']->charity && $data['project']->charity->type == 'emergency')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.emerg_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.service')}}</div></td>
                                        <td>{{$data['project']->emerg_services ? $data['project']->emerg_services->name : $data['project']->emerg_services_other}}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- emerg_project Ends -->

            <!-- stable_projects starts -->
            {{-- مشروع  مستديم --}}
            @if ($data['project']->charity && $data['project']->charity->type == 'sustainable')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.stable_projects')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.service')}}</div></td>
                                        <td>{{trans('admin.'.$data['project']->stable_project)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- stable_projects Ends -->

            {{-- Project notes --}}
            @if($data['project']->notes)
                <!-- Notes starts -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-2">{{trans('admin.notes')}}</h4>
                        </div>
                        <div class="card-body">
                            {!! $data['project']->notes !!}
                        </div>
                    </div>
                </div>
                <!-- Notes Ends -->
            @endif
            </div>

        </div>
        <!-- Project general data Ends -->

        <!-- Project Phases Starts-->
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-2">{{trans('admin.phases')}}</h4>
            </div>
            <div class="card-body">
                <div class="invoice-list-wrapper">
                            <div class="card-datatable table-responsive">
                                <table class="invoice-list-table table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{trans('admin.phase_name')}}</th>
                                            <th>{{$session_currency}} {{trans('admin.phase_cost')}}</th>
                                            <th class="text-truncate">{{trans('admin.phase_start_date')}}</th>
                                            <th>{{trans('admin.phase_end_date')}}</th>
                                            <th>{{trans('admin.phase_approved')}}</th>
                                            {{-- <th>{{ trans('admin.show') }}</th> --}}
                                            <th>{{trans('admin.actions')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($data['project']->phases as $phase)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                <td>{{$phase->name}}</td>
                                                <td>{{generalExchange($phase->cost, $phase->project->currency->symbol, $session_currency)}} {{$session_currency}}</td>
                                                <td>{{$phase->start_date}}</td>
                                                <td>{{$phase->end_date}}</td>
                                                <td>{{$phase->approved ? trans('admin.approved') : trans('admin.not_approved')}}</td>
                                                <td>
                                                    <a  href="{{route('admin.phases.show', $phase->id)}}" title="{{trans('admin.show')}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                    </a>
                                        
                                                            <a  href="{{route('admin.phases.edit', $phase->id)}}" title="{{trans('admin.edit')}}">
                                                                <i data-feather="edit-2" class="mr-50"></i>
                                                            </a>
                                                            <a class="remove-table"  title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('admin.phases.destroy', $phase->id)}}">
                                                                <i data-feather="trash" class="mr-50"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="6">{{trans('admin.no_phases_registered')}}</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                </div>
            </div>
        </div>
    <!-- /Project Phases Ends-->
    
        <!-- Project reports Starts-->
        <div class="invoice-list-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.reports')}}</h4>
                    </div>
                    <div class="card-body">
                         <div class="card-datatable table-responsive">
                        <table class="invoice-list-table table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('admin.phase_name')}}</th>
                                    <th>{{trans('admin.report_status')}}</th>
                                    {{-- <th>{{trans('admin.show')}}</th> --}}
                                    <th>{{trans('admin.actions')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data['project']->reports as $report)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$report->phase ? $report->phase->name : trans('admin.project_report')}}</td>
                                        <td>
                                            <span id="current_status-{{$report->id}}">{{trans('admin.'.$report->status)}}</span>
                                            <a class="btn btn-primary" data-toggle="modal" data-target="#report-{{$report->id}}">{{trans('admin.change')}}</a>
                                            <!-- Modal -->  
                                            <div class="modal fade modal-info text-left" id="report-{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="change_report_status-{{$report->id}}">{{trans('admin.select_status')}}</label>
                                                            <select id="change_report_status-{{$report->id}}" class="form-control">
                                                                <option value="waiting" @if($report->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                <option value="approved" @if($report->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                <option value="rejected" @if($report->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                <option value="hold" @if($report->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                            </select>
                                                        </div>
        
                                                        <div class="form-group">
                                                            <label for="report_notes-{{$report->id}}">{{trans('admin.notes')}}</label>
                                                            <textarea id="report_notes-{{$report->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <button type="button" id="" class="btn btn-info save_status" data-model="report" data-id="{{$report->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST"> <div class="spinner-border  processing-div-{{ $report->id}}" role="status"></div>{{trans('admin.save')}}</button>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                            </div>
                                        </td>
                                        <td>
                                            <a title="{{trans('admin.show')}}" href="{{route('admin.reports.show', $report->id)}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                    
                                                    <a title="{{trans('admin.edit')}}" href="{{route('admin.reports.edit', $report->id)}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                    <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('admin.reports.destroy', $report->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                             
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">{{trans('admin.no_data')}}</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
        </div>
        <!-- Project reports Ends-->

        <!-- Project Sponsers Starts-->
            {{-- <div  class="card">
                <div class="card-header">
                    <h4 class="card-title mb-2">{{trans('admin.sponsers')}}</h4>
                </div>
                <div class="card-body">
                    <div class="invoice-list-wrapper">
                        <div class="card-datatable table-responsive">
                            <table class="invoice-list-table table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('admin.name')}}</th>
                                        <th>{{trans('admin.work_field')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data['project']->sponsers as $sponser)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$sponser->name}}</td>
                                            <td>{{$sponser->work_field ? $sponser->work_field->name : '-'}}</td>
                                        </tr>
                                    @empty
                                            <tr>
                                                <td colspan="6">{{trans('admin.no_data')}}</td>
                                            </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> --}}
        <!-- /Project Sponsers Ends-->

        <!-- Project edit_requests Starts-->
        <div class="invoice-list-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.edit_requests')}}</h4>
                    </div>
                    <div class="card-body">
                    <div class="card-datatable table-responsive">
                        <table class="invoice-list-table table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('admin.request_type')}}</th>
                                    <th>{{trans('admin.request_status')}}</th>
                                    <th>{{trans('admin.notes')}}</th>
                                    <th>{{trans('admin.actions')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data['project']->edit_requests as $req)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{trans('admin.'.$req->type)}}</td>
                                        <td>
                                            <span id="current_status">{{trans('admin.'.$req->status)}}</span>
                                        </td>
                                        <td>
                                            @if($req->notes)
                                            <a  title="{{trans('admin.notes')}}" href="javascript:void(0);" data-toggle="modal" data-target="#edit_request">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                                <!-- Modal -->
                                                <div class="modal fade modal-info text-left" id="edit_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {!! $req->notes !!}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        
                                            @endif
                                       
                                          
                                                   
                                        </td>
                                        <td>
                                            <a  href="#" title="{{trans('admin.change')}}" data-toggle="modal" data-target="#req_status-{{$req->id}}"><i data-feather="edit-2" class="mr-50"></i></a>
                                            <!-- Modal -->
                                            <div class="modal fade modal-info text-left" id="req_status-{{$req->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="change_edit_status-{{$req->id}}">{{trans('admin.select_status')}}</label>
                                                                <select id="change_edit_status-{{$req->id}}" class="form-control">
                                                                    <option value="waiting" @if($req->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                    <option value="approved" @if($req->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                    <option value="rejected" @if($req->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                    <option value="hold" @if($req->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                                </select>
                                                            </div>
        
                                                            <div class="form-group">
                                                                <label for="edit_notes-{{$req->id}}">{{trans('admin.notes')}}</label>
                                                                <textarea id="edit_notes-{{$req->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                             <button type="button" id="" class="btn btn-info save_status" data-model="{{ $req->type == 'project' ? 'edit_request_project' : 'edit_request_charity' }}" data-id="{{$req->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST"> <div class="spinner-border  processing-div-{{ $req->id}}" role="status"></div>{{trans('admin.save')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('admin.requests.destroy', $req->id)}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">{{trans('admin.no_data')}}</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Project edit_requests Ends-->

        <!-- Project location Starts-->
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-2">{{trans('admin.project_location')}}</h4>
                </div>
                <div class="card-body">
                <!-- LOCATION -->   
                <div class="form-group" style="position: relative">
                    <label class="form-label" for="location">{{ __('admin.project_location') }}</label>
                    <input type="text" id="location" name="location" value="{{$data['project']->location}}" disabled class="form-control" placeholder="{{ __('admin.location') }}" />
                    <span class="error-input"></span>
                    <span class="error-input error"></span>
                    <input type="hidden" name="lat" id="lat"  value="{{$data['project']->lat}}">
                    <input type="hidden" name="lng" id="lng"  value="{{$data['project']->lng}}">
                    <div id="map" style="width:100%;height:500px;"></div>
                </div>
                </div>
            </div>
        <!-- Project location end-->
    </section>

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
    <script src="{{ url('dashboard/forms/projects/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdarVlRZOccFIGWJiJ2cFY8-Sr26ibiyY&libraries=places&callback=initAutocomplete&language=<?php echo e('ar'); ?>"
    defer></script>
@endpush