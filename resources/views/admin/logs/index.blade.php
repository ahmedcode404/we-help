@extends('layout.app')

@section('title', trans('admin.logs'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.emp_name')}}</th>
                            <th>{{trans('admin.emp_type')}}</th>
                            <th>{{trans('admin.model_name')}}</th>
                            <th>{{trans('admin.charity')}}</th>
                            <th>{{trans('admin.edit_type')}}</th>
                            <th>{{trans('admin.date')}}</th>
                            {{-- <th>{{trans('admin.actions')}}</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['logs'] as $log)
                            <tr>
                                <td>{{$loop->iteration}}</td>

                                <td>
                                    @if($log->user_type == 'admin')
                                        {{$log->admin_emp ? $log->admin_emp->name : ''}}
                                    @elseif($log->user_type == 'charity')
                                        {{$log->charity_emp ? $log->charity_emp->emp_name : ''}}
                                    @endif
                                </td>

                                <td>
                                    @if($log->user_type == 'admin')
                                        {{trans('admin.administration')}}
                                    @elseif($log->user_type == 'charity')
                                        {{trans('admin.charity')}}
                                    @endif
                                </td>

                                <td>
                                    @if($log->model_type == 'project')
                                        {{$log->project ? $log->project->name : ''}}
                                    @elseif($log->model_type == 'charity')
                                        {{ $log->charity ? $log->charity->name : '' }}
                                    @elseif($log->model_type == 'finance')
                                        {{ $log->finance && $log->finance->project ? $log->finance->project->name : '' }}
                                    @endif
                                </td>

                                <td>
                                    @if($log->model_type == 'project')
                                        @if($log->project && $log->project->charity)
                                            @if($log->project->charity->hasRole('charity_employee')) 
                                                {{$log->project->charity->charity->name}} 
                                            @else
                                                {{$log->project->charity->name}} 
                                            @endif
                                        @endif

                                    @elseif($log->model_type == 'charity')
                                        {{ $log->charity ? $log->charity->name : '' }}
                                    @elseif($log->model_type == 'finance')
                                        {{ $log->finance && $log->finance->project && $log->finance->project->charity ? $log->finance->project->charity->name : '' }}
                                    @endif
                                </td>

                                <td>{{$log->edit_type}}</td>

                                <td>{{$log->created_at->format('Y-m-d h:i A')}}</td>

                                
                                {{-- <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i data-feather="more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('admin.cities.show', $log->id)}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                                <span>{{trans('admin.show')}}</span>
                                            </a>
                                            <a class="dropdown-item" href="{{route('admin.cities.edit', $log->id)}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                                <span>{{trans('admin.edit')}}</span>
                                            </a>
                                            @if($log->id != 1 && $log->id != 2 && $log->id != 3)
                                                <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.cities.destroy', $log->id)}}">
                                                    <i data-feather="trash" class="mr-50"></i>
                                                    <span>{{trans('admin.delete')}}</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td> --}}
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['logs']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection