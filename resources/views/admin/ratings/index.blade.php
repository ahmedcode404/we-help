@extends('layout.app')

@section('title', trans('admin.supports'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.supporter_name')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.charity_name')}}</th>
                            <th>{{trans('admin.rating')}}</th>
                            <th>{{trans('admin.show_for_web')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['ratings'] as $rate)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$rate->user->name}}</td>
                                <td>{{$rate->project->name}}</td>
                                <td>{{$rate->project->charity ? $rate->project->charity->name : ''}}</td>
                                <td>{{$rate->avg_grade}}</td>
                                <td>
                                    <select name="" id="" class="form-control show_rate" style="max-width: 80px"
                                        data-method="POST"
                                        data-action="{{route('admin.ratings.show-for-web', ['user_id' => $rate->user_id, 'project_id' => $rate->project_id])}}">
                                        <option selected value="1" @if($rate->show_for_web == 1) selected @endif>{{ trans('admin.yes') }}</option>
                                        <option value="0" @if($rate->show_for_web == 0) selected @endif>{{ trans('admin.no') }}</option>

                                    </select>
                                    {{-- {{$rate->show_for_web ? trans('admin.yes') : trans('admin.no')}}
                                    <a href="#" class="btn btn-primary show_rate" data-method="POST" data-action="{{route('admin.ratings.show-for-web', ['user_id' => $rate->user_id, 'project_id' => $rate->project_id])}}">{{trans('admin.change')}}</a>
                                --}}
                                </td>
                                <td>
                                    <a title="{{trans('admin.delete_comment')}}" title=">{{trans('admin.details')}}" href="javascript:void(0);" data-toggle="modal" data-target="#info-{{$rate->user_id}}{{$rate->project_id}}">
                                        <i data-feather="eye" class="mr-50"></i>
                                    </a>
                                 
                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="info-{{$rate->user_id}}{{$rate->project_id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5>{{trans('admin.details')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="datatables-basic table-striped table">
                                                        <tbody>
                                                            @php
                                                                $details = $rate->details($rate->user_id, $rate->project_id);
                                                            @endphp
                                                            @foreach ($details as $detail)
                                                                <tr>
                                                                    <td>{{$detail->criteria->name}}</td>
                                                                    <td>{{$detail->grade}}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>

                                                    @if($rate->getCommentForAdmin($rate->user_id, $rate->project_id))
                                                        <textarea name="comment" cols="30" rows="6" class="form-control">{{$rate->getCommentForAdmin($rate->user_id, $rate->project_id)->comment}}</textarea>
                                                    @endif
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" id="save_status" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              
                                    <div id="delete_btn{{$rate->user_id}}{{$rate->project_id}}" style="display: inline-block">
                                    @if($rate->getCommentForAdmin($rate->user_id, $rate->project_id) && checkPermissions(['delete_comment']))
                                        <a class="delete-comment" title="{{trans('admin.delete_comment')}}" href="javascript:void(0);" data-id="{{$rate->user_id.''.$rate->project_id}}" data-method="POST" data-action="{{route('admin.ratings.delete-comment', ['user_id' => $rate->user_id, 'project_id' => $rate->project_id])}}">
                                            <i data-feather="trash" class="mr-50"></i>
                                        </a>
                                    @endif
                                </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['ratings']->links()}}   
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/ratings/index.js')}}"></script>
@endpush