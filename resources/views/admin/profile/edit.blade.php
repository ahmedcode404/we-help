@extends('layout.app')

@section('title', trans('admin.profile'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" />    
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
    
@endpush

@section('content')

    <!-- edit profile start -->
    <section class="app-user-edit">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{trans('admin.edit')}}</h4>
            </div>                   
            <div class="card-body">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center active" id="basic_info-tab" data-toggle="tab" href="#basic_info" aria-controls="basic_info" role="tab" aria-selected="true">
                            <i data-feather="user"></i><span>{{trans('admin.basic_info')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="change_password-tab" data-toggle="tab" href="#change_password" aria-controls="change_password" role="tab" aria-selected="false">
                            <i data-feather="info"></i><span>{{trans('admin.change_password')}}</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">

                    <!-- basic_info Tab starts -->
                    <div class="tab-pane active show" id="basic_info" aria-labelledby="basic_info-tab" role="tabpanel">
                        
                        @include('admin.profile.edit.basic_info')

                    </div>
                    <!-- basic_info Tab ends -->

                    <!-- change_password Tab starts -->
                    <div class="tab-pane" id="change_password" aria-labelledby="change_password-tab" role="tabpanel">
                        
                        @include('admin.profile.edit.change_password')
                        
                    </div>
                    <!-- change_password Tab ends -->   

                </div>
            </div>
        </div>
    </section>
    <!-- edit profile end -->

@endsection

@push('js')

    <script src="{{asset('dashboard/js/intlTelInput.min.js')}}" type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>
    
    @if(\App::getLocale() == 'ar')
        <!--for arabic-only-->
        <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
        <!--end-->
    @endif
    <script src="{{ url('custom/preview-image.js') }}"></script>
    <script src="{{asset('charity/js/projects/country-for-city.js')}}"></script>
    <script src="{{asset('dashboard/forms/profile/edit.js')}}"></script>
@endpush