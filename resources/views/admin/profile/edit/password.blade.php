<div class="tab-pane" id="password" aria-labelledby="password-tab" role="tabpanel">
    <form class="form-validate" id="password_form" method="post" action="{{ route('admin.profile.update', $data['user']->id) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row">

            {{-- old_password start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="old_password">{{trans('admin.old_password')}}</label>
                    <input type="password" class="form-control" id="old_password" value="{{$data['user']->password ? $data['user']->password : old('password')}}" placeholder="{{trans('admin.old_password')}}" />                                          
                    @if($errors->has('old_password'))
                        <div class="error">{{ $errors->first('old_password') }}</div>
                    @endif
                </div>
            </div>
            {{-- old_password end --}}

            {{-- password start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="password">{{trans('admin.new_password')}}</label>
                    <input type="password" class="form-control" id="password" name="password" value="{{$data['user']->password ? $data['user']->password : old('password')}}" placeholder="{{trans('admin.password')}}" />                                          
                    @if($errors->has('password'))
                        <div class="error">{{ $errors->first('password') }}</div>
                    @endif
                </div>
            </div>
            {{-- password end --}}

            {{-- password_confirmation start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="password_confirmation">{{trans('admin.password_confirmation')}}</label>
                    <input type="password_confirmation" class="form-control" id="password_confirmation" name="password_confirmation" value="{{$data['user']->password_confirmation ? $data['user']->password_confirmation : old('password_confirmation')}}" placeholder="{{trans('admin.password_confirmation')}}" />                                          
                </div>
            </div>
            {{-- password_confirmation end --}}


    </form>
    <!-- users edit account form ends -->
</div>