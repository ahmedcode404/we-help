<form class="form-validate" id="change_password_form" method="post" action="{{ route('admin.profile.update', $data['user']->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    
    <div class="row">

        {{-- password start --}}
        <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="password">{{trans('admin.new_password')}}</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="******" />                                          
                @if($errors->has('password'))
                    <div class="error">{{ $errors->first('password') }}</div>
                @endif
            </div>
        </div>
        {{-- password end --}}

        {{-- password_confirmation start --}}
        <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="password_confirmation">{{trans('admin.password_confirmation')}}</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation"  placeholder="******" />                                          
            </div>
        </div>
        {{-- password_confirmation end --}}

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">{{trans('admin.save')}}</button>
            <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
        </div>

    </div>


</form>