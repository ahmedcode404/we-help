<form class="form-validate basic_info_form" id="basic_info_form" method="post" action="{{ route('admin.profile.update', $data['user']->id) }}" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <input type="hidden" id="edit" value="true">

    @if($data['user']->hasRole('employee'))
        <input type="hidden" name="emp_data" id="emp_data" value="true">
    @else
        <input type="hidden" name="emp_data" id="emp_data" value="false">
    @endif 

    <div class="row">

        @if($data['user']->hasRole('employee'))
            <div class="col-12">
                <div class="form-group">
                    <label class="form-label" for="name_ar">{{trans('admin.name_ar')}}</label>
                    <select name="job_id" id="job_id" class="form-control" disabled>
                        <option value="{{$data['user']->job_id}}" selected>{{$data['user']->job ? $data['user']->job->name : trans('admin.no_data')}}</option>
                    </select>
                    <div class="error error-job_id"></div>
                </div>
            </div>
            <input type="hidden" name="job_id" value="{{$data['user']->job_id}}">
        @endif

        {{-- name_ar start --}}
        <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="name_ar">{{trans('admin.name_ar')}}</label>
                <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{$data['user']->name_ar ? $data['user']->name_ar : old('name_ar')}}" placeholder="{{trans('admin.name_ar')}}" />                                          
                <div class="error error-name_ar"></div>
            </div>
        </div>
        {{-- name_ar end --}}

        {{-- name_en start --}}
        <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="name_en">{{trans('admin.name_en')}}</label>
                <input type="text" class="form-control" id="name_en" name="name_en" value="{{$data['user']->name_en ? $data['user']->name_en : old('name_en')}}" placeholder="{{trans('admin.name_en')}}" />                                               
                <div class="error error-name_en"></div>
            </div>
        </div>
        {{-- name_en end --}}

    </div>

    <div class="row">
        {{-- email start --}}
        <div class="col-md-4">
            <div class="form-group">
                <label class="form-label" for="email">{{trans('admin.email')}}</label>
                <input type="email" class="form-control" id="email" name="email" value="{{$data['user']->email ? $data['user']->email : old('email')}}" placeholder="{{trans('admin.email')}}" />                                            
                <div class="error error-email"></div>
            </div>
        </div>
        {{-- email end --}}

        {{-- phone start --}}
        <div class="col-md-4">
            <div class="form-group">
                <label class="form-label" for="phone">{{trans('admin.phone')}}</label>
                <input type="tel" class="form-control" id="phone" name="phone_num" value="{{$data['user']->phone ? $data['user']->phone : old('phone')}}" placeholder="{{trans('admin.phone')}}" />                                              
                <div class="error error-phone"></div>
            </div>
        </div>
        {{-- phone end --}}  

        {{-- image start --}}
        <div class="col-md-4">
            <div class="form-group">
                <label class="form-label" for="phone">{{trans('admin.image')}}</label>
                <div class="custom-file">
                <input type="file" name="image" class="form-control image" id="image" accept=".gif, .jpg, .png, .webp">
                <div class="error error-image"></div>
            </div>
                <div class="form-group prev" style="display: block">
                    <img src="{{ asset('storage/'.auth()->user()->image) }}" style="width: 100px" class="img-thumbnail preview-image" alt="">
                </div> 
            </div>
        </div>
        {{-- image end --}}

        @if($data['user']->hasRole('employee'))
            {{-- degree start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="degree_id">{{trans('admin.degree')}}</label>
                    <select class="form-control select2" id="degree_id" name="degree_id">
                        <option value="">{{trans('admin.select')}} ...</option>
                        @foreach ($data['degrees'] as $degree)
                            <option value="{{$degree->id}}" @if(old('degree_id') == $degree->id || $data['user']->degree_id == $degree->id) selected @endif>{{$degree->name}}</option>
                        @endforeach
                    </select>  
                    <div class="error error-degree_id"></div>                                               
                </div> 
            </div>
            {{-- degree end --}}
        @endif
    </div>

    @if($data['user']->hasRole('employee'))
    
        <div class="row">

            {{-- region start --}}
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-label" for="region">{{trans('admin.region')}}</label>
                        <input type="text" class="form-control" id="region" name="region" value="{{$data['user']->region ? $data['user']->region : old('region')}}" placeholder="{{trans('admin.region')}}" />                                                    
                        <div class="error error-region"></div>
                    </div>
                </div>
            {{-- region end --}} 


            {{-- street start --}}
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-label" for="street">{{trans('admin.street')}}</label>
                        <input type="text" class="form-control" id="street" name="street" value="{{$data['user']->street ? $data['user']->street : old('street')}}" placeholder="{{trans('admin.street')}}" />                                                   
                        <div class="error error-street"></div>
                    </div>
                </div>
            {{-- street end --}} 

            {{-- unit_number start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-label" for="unit_number">{{trans('admin.unit_number')}}</label>
                    <input type="number" class="form-control" id="unit_number" name="unit_number" value="{{$data['user']->unit_number ? $data['user']->unit_number : old('unit_number')}}" placeholder="{{trans('admin.unit_number')}}" />                                           
                    <div class="error error-unit_number"></div>
                </div>
            </div>
            {{-- unit_number end --}} 

        </div>


        <div class="row">

            {{-- nation_number start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label class="form-label" for="nation_number">{{trans('admin.nation_number')}}</label>
                    <input type="text" class="form-control" id="nation_number" name="nation_number" value="{{$data['user']->nation_number ? $data['user']->nation_number : old('nation_number')}}" placeholder="{{trans('admin.nation_number')}}" />                                               
                    <div class="error error-nation_number"></div>
                </div>
            </div>
            {{-- nation_number end --}} 

            {{-- mail_box start --}}
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-label" for="mail_box">{{trans('admin.mail_box')}}</label>
                        <input type="text" class="form-control" id="mail_box" name="mail_box" value="{{$data['user']->mail_box ? $data['user']->mail_box : old('mail_box')}}" placeholder="{{trans('admin.mail_box')}}" />
                        <div class="error error-mail_box"></div>                                                     
                    </div>
                </div>
            {{-- mail_box end --}}

            {{-- postal_code start --}}
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-label" for="postal_code">{{trans('admin.postal_code')}}</label>
                        <input type="number" class="form-control" id="postal_code" name="postal_code" value="{{$data['user']->postal_code ? $data['user']->postal_code : old('postal_code')}}" placeholder="{{trans('admin.postal_code')}}" />                                                      
                        <div class="error error-postal_code"></div>
                    </div>
                </div>
            {{-- postal_code end --}}

        </div>

        <div class="row">

            {{-- contract_start_date start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="contract_start_date">{{trans('admin.contract_start_date')}}</label>
                    <input id="contract_start_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{$data['user']->contract_start_date ? $data['user']->contract_start_date : old('contract_start_date')}}" name="contract_start_date" placeholder="{{trans('admin.contract_start_date')}}" required />
                    <div class="error error-contract_start_date"></div>
                </div>
            </div>
            {{-- contract_start_date end --}}  

            {{-- contract_end_date start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="contract_end_date">{{trans('admin.contract_end_date')}}</label>
                    <input id="contract_end_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{$data['user']->contract_end_date ? $data['user']->contract_end_date : old('contract_end_date')}}" name="contract_end_date" placeholder="{{trans('admin.contract_end_date')}}" required />
                    <div class="error error-contract_end_date"></div>
                </div>
            </div>
            {{-- contract_end_date end --}}  

        </div>

        {{-- phase country start --}}
        <div class="col-md-12">
            <div class="form-group">
                <label for="country">{{trans('admin.country')}}</label>
                <select name="country" id="country" class="form-control">
                    <option value="">{{trans('admin.select')}}</option> 
                    @foreach($data['countries'] as $key => $country) 
                        <option value="{{ $key }}" @if(old('country') == $key || $data['user']->country == $key) selected @endif>{{ $country }}</option>
                    @endforeach  
                </select>    
                <div class="error error-country"></div>
            </div>
        </div>

        {{-- <div class="col-md-12">
            <div class="form-group">
                <label for="country_id">{{trans('admin.country')}}</label>
                <select name="country_id" id="country_id" data-action="{{ route('admin.city.country') }}" class="form-control">
                    <option value="">{{trans('admin.select')}}</option> 
                    @foreach($data['countries'] as $country) 
                        @if($country->parent_id == null)
                            <option value="{{ $country->id }}" @if(old('country_id') == $country->id || $data['user']->country_id == $country->id) selected @endif>{{ $country->name }}</option>
                        @endif
                    @endforeach  
                </select>                                             
            </div>
        </div> --}}
        {{-- phase country end --}}  

        <div class="row append-cities">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="city">{{trans('admin.city')}}</label>
                    <input type="text" name="city" id="city" value="{{ $data['user']->city }}" class="form-control">                                            
                    <div class="error error-city"></div>
                </div>
            </div>
        </div> 

        {{-- <div class="row append-cities">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="city_id">{{trans('admin.city')}}</label>
                    <select name="city_id" id="city_id" data-url="{{ route('admin.city.country') }}" class="form-control">
                        <option value="">{{trans('admin.select')}}</option> 
                        @foreach($data['cities'] as $city) 
                            <option value="{{ $city->id }}" @if(old('city_id') == $city->id || $data['user']->city_id == $city->id) selected @endif>{{ $city->name }}</option>
                        @endforeach  
                    </select>                                             
                </div>
            </div>
        </div>  --}}

        {{-- nation_id start --}}

        {{-- <div class="col-md-12">

            <div class="form-group">
                <label for="nation_id">{{trans('admin.nation')}}</label>
                <select class="form-control select2" id="nation_id" name="nation_id">
                    <option value="">{{trans('admin.select')}} ...</option>
                    @foreach ($data['countries'] as $country)
                        @if($country->id == 1 || $country->id == 2)
                            <option value="{{$country->id}}" @if(old('nation_id') == $country->id || $data['user']->nation_id == $country->id) selected @endif>{{$country->name}}</option>
                        @endif
                    @endforeach
                </select>                                                
            </div> 

        </div>                                        --}}
        {{-- nation_id end --}}  

    @endif

    <div class="col-12 d-flex flex-sm-row flex-column mt-2" id="loading">
        <button type="button" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 submit_basic_info_form_button">{{trans('admin.save')}}</button>
        <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
    </div>

</form>
