@extends('layout.app')

@section('title', trans('admin.contacts'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.client_name')}}</th>
                            <th>{{trans('admin.email')}}</th>
                            <th>{{trans('admin.message_date')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['contacts'] as $contact)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$contact->name}}</td>
                                <td>{{$contact->email}}</td>
                                <td>{{$contact->created_at->format('Y-m-d')}}</td>
                                {{-- <td>{{$contact->nation->code}}</td> --}}
                                <!-- Modal -->
                                <div class="modal fade modal-info text-left" id="info-{{ $contact->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.content')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {{$contact->content}}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <td>
                                    <a href="#" data-toggle="modal" data-target="#info-{{ $contact->id }}" title="{{trans('admin.show')}}">
                                        <i data-feather="eye" class="mr-50"></i>
                                    </a>
                             
                                    @if(checkPermissions(['delete_contacts']))
                                                {{-- @if(checkPermissions(['delete_contacts'])) --}}
                                                    <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('admin.contacts.destroy', $contact->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                {{-- @endif --}}
                                      
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['contacts']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection