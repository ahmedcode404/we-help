@extends('layout.app')
@section('title', trans('admin.home'))
@section('content')

    <!-- BEGIN: Content-->
    
            <div class="content-body">
                <!-- Dashboard Ecommerce Starts -->
                <section id="dashboard-ecommerce">
                    <div class="row match-height">

                        <!-- Statistics UK Card -->
                        <div class="col-12">
                            <div class="card card-statistics">
                                <div class="card-header">
                                    <h4 class="card-title">{{trans('admin.statistics')}}</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">

                                        {{-- projects_count start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-primary mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="trending-up" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['all_projects']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.total_projects')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- projects_count end --}}

                                        {{-- active_projects_count start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-primary mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="trending-up" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['active_projects_count']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.active_projects')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- active_projects_count end --}}

                                        {{-- deleted_projects_count start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-primary mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="trending-up" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['deleted_projects_count']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.deleted_projects')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- deleted_projects_count end --}}

                                        {{-- beneficiaries start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-primary mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="trending-up" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['benef_count']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.beneficiaries')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- beneficiaries end --}}

                                        {{-- donations start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-info mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="dollar-sign" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0" dir="ltr">{{$data['donation_ammounts']}} {{$data['session_currency']}} </h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.total_donations')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- donations end --}}

                                        {{-- totla_parteners start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-danger mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['parteners_count']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.totla_parteners')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- totla_parteners end --}}

                                        {{-- total_countries start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="flag" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['countries']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.total_countries')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- total_countries end --}}

                                        {{-- charities start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['charities']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.charities')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- charities end --}}

                                        {{-- edit_requests start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="file" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['edit_requests']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.edit_requests')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- edit_requests end --}}

                                        {{-- supporters start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['supporters']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.supporters')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- supporters end --}}

                                        {{-- marketings start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="box" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['compaigns']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.marketings')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- marketings end --}}

                                        {{-- employees start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{$data['emps']}}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.employees')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- employees end --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Statistics UK Card -->

                        <!-- Statistics SA Card -->
                        
                        
                        <!--/ Statistics SA Card -->
                    </div>

                </section>
                <!-- Dashboard Ecommerce ends -->

            </div>
        
    <!-- END: Content-->


@endsection