@extends('layout.app')

@section('title', trans('admin.show_nationalities'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div>{{trans('admin.nation_name_en')}} : {{$data['nationality']->name_en}}</div>
                    <br>
                    <div>{{trans('admin.nation_name_ar')}} : {{$data['nationality']->name_ar}}</div>
                    <br>

                    <div class="text-right">
                    <a class="btn btn-primary" href="{{route('admin.nationalities.edit', $data['nationality']->id)}}" title="{{trans('admin.edit')}}">
                        <i data-feather="edit-2" class="mr-50"></i>{{trans('admin.edit')}}
                        
                    </a>
                </div>
            </div>

            {{-- nationality emps start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.emps')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.emp_name')}}</th>
                                <th>{{trans('admin.phone')}}</th>
                                <th>{{trans('admin.email')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['nationality']->users as $emp)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$emp->name}}</td>
                                    <td>{{$emp->phone}}</td>
                                    <td>{{$emp->email}}</td>
                                    <td>
                                      
                                        @if(checkPermissions(['show_emp']))
                                            <a href="{{route('admin.employees.show', $emp->id)}}" title="{{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                        @endif
                                        @if(checkPermissions(['edit_emp']))
                                            <a  href="{{route('admin.employees.edit', $emp->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                        @endif
                                        @if(checkPermissions(['delete_emp']))
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.employees.destroy', $emp->id)}}" title="{{trans('admin.delete')}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            {{-- job emps end --}}
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection