@extends('layout.app')

@section('title', trans('admin.nationalities'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">

                @if(checkPermissions(['create_nationalities']))
                    <a href="{{route('admin.nationalities.create')}}" class="btn btn-primary">{{trans('admin.create_nationality')}}</a>
                @endif

                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.nation_name_ar')}}</th>
                            <th>{{trans('admin.nation_name_en')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['nationalities'] as $nationality)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$nationality->name_ar}}</td>
                                <td>{{$nationality->name_en}}</td>
                                <td>
                                    @if(checkPermissions(['show_nationalities']))
                                        <a  href="{{route('admin.nationalities.show', $nationality->id)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                            {{-- <span>{{trans('admin.show')}}</span> --}}
                                        </a>
                                    {{-- @else
                                        {{trans('admin.do_not_have_permission')}} --}}
                                    @endif
                              
                                    @if(checkPermissions(['edit_nationalities']) || checkPermissions(['delete_nationalities']))
                                                @if(checkPermissions(['edit_nationalities']))
                                                    <a  href="{{route('admin.nationalities.edit', $nationality->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_nationalities']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.nationalities.destroy', $nationality->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                       
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['nationalities']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection