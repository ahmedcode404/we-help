@extends('layout.app')

@section('title', trans('admin.create_phase'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
    
@endpush

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.create')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="create_phase" method="POST" action="{{route('admin.phases.store')}}">
                            @csrf
                            <input type="hidden" name="project_id" value="{{$data['project']->id}}">

                            {{-- phase order start --}}
                            <input type="hidden" name="order" value="{{getOrder($data['project'])}}">
                            <div class="row align-items-center">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="order">{{trans('admin.order')}}</label>
                                        <select name="order" id="order" class="form-control" disabled>
                                            <option>{{trans('admin.order_'.getOrder($data['project']))}}</option>  
                                        </select>
                                        @if($errors->has('order'))
                                            <div class="error">{{ $errors->first('order') }}</div>
                                        @endif 
                                    </div>
                                </div>
                            {{-- phase order end --}}

                                   {{-- approved start --}}
                                   <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="custom-control custom-control-primary custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="approved" value="{{old('approved')}}" name="approved" @if(old('approved') == 1) checked @endif />
                                            <label class="custom-control-label" for="approved">{{trans('admin.approved')}}</label>
                                            @if($errors->has('approved'))
                                                <div class="error">{{ $errors->first('approved') }}</div>
                                            @endif 
                                        </div>
                                    </div>
                                </div>
                                {{-- approved end --}}

                                {{-- name_ar start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_ar">{{trans('admin.name_ar')}}</label>
                                        <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{old('name_ar')}}" placeholder="{{trans('admin.name_ar')}}" required />
                                        @if($errors->has('name_ar'))
                                            <div class="error">{{ $errors->first('name_ar') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- name_ar end --}}

                                {{-- name_en start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_en">{{trans('admin.name_en')}}</label>
                                        <input type="text" class="form-control" id="name_en" name="name_en" value="{{old('name_en')}}" placeholder="{{trans('admin.name_en')}}" required />
                                        @if($errors->has('name_en'))
                                            <div class="error">{{ $errors->first('name_en') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- name_en end --}}
                            </div>

                            <div class="row">
                                {{-- start_date start --}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="start_date">{{trans('admin.start_date')}}</label>
                                        <input id="start_date" type="text" class="form-control date-picker" readonly  autocomplete="off" 
                                        @if(count($data['project']->phases) == 0) value="{{$data['project']->start_date}}" @else value="{{old('start_date')}}"  @endif
                                        @if(count($data['project']->phases) == 0) disabled @endif
                                        name="start_date" placeholder="{{trans('admin.start_date')}}" data-action="{{route('admin.get-diff-days')}}" data-method="POST" required />
                                        @if($errors->has('start_date'))
                                            <div class="error">{{ $errors->first('start_date') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- start_date end --}}

                                {{-- end_date start --}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="end_date">{{trans('admin.end_date')}}</label>
                                        <input id="end_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{old('end_date')}}" name="end_date" placeholder="{{trans('admin.end_date')}}" data-action="{{route('admin.get-diff-days')}}" data-method="POST" required />
                                        @if($errors->has('end_date'))
                                            <div class="error">{{ $errors->first('end_date') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- end_date end --}}

                                {{-- duration start --}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="duration">{{trans('admin.duration')}}</label>
                                        <input id="duration" type="number" min="1" class="form-control" name="duration" value="{{old('duration')}}" placeholder="{{trans('admin.duration')}}" readonly />
                                        @if($errors->has('duration'))
                                            <div class="error">{{ $errors->first('duration') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- duration end --}}

                                {{-- cost start --}}
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="cost">{{trans('admin.cost')}}</label>
                                        <input id="cost" type="number" min="1" step=".01" class="form-control" name="cost" value="{{old('cost')}}" placeholder="{{trans('admin.cost')}}" required />
                                        @if($errors->has('cost'))
                                            <div class="error">{{ $errors->first('cost') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- cost end --}}

                           
                            </div>

                            <div class="row">
                                {{-- desc_ar start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="desc_ar">{{trans('admin.desc_ar')}}</label>
                                        <textarea name="desc_ar" id="desc_ar" class="form-control" cols="30" rows="6" placeholder="{{trans('admin.desc_ar')}}" required>{{old('desc_ar')}}</textarea>
                                        @if($errors->has('desc_ar'))
                                            <div class="error">{{ $errors->first('desc_ar') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- desc_ar end --}}

                                {{-- desc_en start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="desc_en">{{trans('admin.desc_en')}}</label>
                                        <textarea name="desc_en" id="desc_en" class="form-control" cols="30" rows="6" placeholder="{{trans('admin.desc_en')}}" required>{{old('desc_en')}}</textarea>
                                        @if($errors->has('desc_en'))
                                            <div class="error">{{ $errors->first('desc_en') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- desc_en end --}}
                            </div>
                            
                           

                            <div class="row">
                                {{-- cost_details_ar start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="cost_details_ar">{{trans('admin.cost_details_ar')}}</label>
                                        <textarea name="cost_details_ar" id="cost_details_ar" class="form-control" cols="30" rows="6" placeholder="{{trans('admin.cost_details_ar')}}" required>{{old('cost_details_ar')}}</textarea>
                                        @if($errors->has('cost_details_ar'))
                                            <div class="error">{{ $errors->first('cost_details_ar') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- cost_details_ar end --}}

                                {{-- cost_details_en start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="cost_details_en">{{trans('admin.cost_details_en')}}</label>
                                        <textarea name="cost_details_en" id="cost_details_en" class="form-control" cols="30" rows="6" placeholder="{{trans('admin.cost_details_en')}}" required>{{old('cost_details_en')}}</textarea>
                                        @if($errors->has('cost_details_en'))
                                            <div class="error">{{ $errors->first('cost_details_en') }}</div>
                                        @endif 
                                    </div>
                                </div>
                                {{-- cost_details_en end --}}
                            </div>

                            <div class="row">
                                {{-- output_ar start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="output_ar">{{trans('admin.output_ar')}}</label>
                                        <textarea name="output_ar" id="output_ar" class="form-control" cols="30" rows="6" placeholder="{{trans('admin.output_ar')}}" required>{{old('output_ar')}}</textarea>
                                        @if($errors->has('output_ar'))
                                            <div class="error">{{ $errors->first('output_ar') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- output_ar end --}}

                                {{-- output_en start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="output_en">{{trans('admin.output_en')}}</label>
                                        <textarea name="output_en" id="output_en" class="form-control" cols="30" rows="6" placeholder="{{trans('admin.output_en')}}" required>{{old('output_en')}}</textarea>
                                        @if($errors->has('output_en'))
                                            <div class="error">{{ $errors->first('output_en') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- output_en end --}}
                            </div>
                        
                            <div class="row">

                                {{-- notes start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="notes">{{trans('admin.notes')}}</label>
                                        <textarea name="notes" id="notes" class="form-control" cols="30" rows="6" placeholder="{{trans('admin.notes')}}">{{old('notes')}}</textarea>
                                        @if($errors->has('notes'))
                                            <div class="error">{{ $errors->first('notes') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- notes end --}}

                              

                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
    
    <script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>
    
    @if(\App::getLocale() == 'ar')
        <!--for arabic-only-->
        <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
        <!--end-->
    @endif
    <script>
        //datepicker
    $(function () {
        $('.date-picker').datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        })
    });
    </script>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
        
        <script>
        
            // ckeditor custom
            
            CKEDITOR.replace( 'desc_ar' );   
            CKEDITOR.replace( 'desc_en' );
            CKEDITOR.replace( 'cost_details_ar' );   
            CKEDITOR.replace( 'cost_details_en' );
            CKEDITOR.replace( 'output_ar' );   
            CKEDITOR.replace( 'output_en' );
            CKEDITOR.replace( 'notes' );
        
        </script>

        <script src="{{asset('dashboard/forms/phases/create.js')}}"></script>
    @endpush
@endsection