@extends('layout.app')

@section('title', trans('admin.show-phase'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section class="app-user-view">

        @if(count($data['phase']->images) > 0)
            <!-- Phase images starts -->
            <div class="row">
                <div class="col-12">
                    <div class="card profile-header mb-2">
                        <div class="card-header">
                            <h4 class="card-title">{{trans('admin.phase_images')}}</h4>
                        </div>
                        <div class="card-body">

                            <div id="carousel-keyboard" class="carousel slide" data-keyboard="true">
                                @if(count($data['phase']->images) > 1)
                                    <ol class="carousel-indicators">
                                        @foreach ($data['phase']->images as $image)
                                            <li data-target="#carousel-keyboard" data-slide-to="{{ $loop->iteration-1 }}" @if($loop->iteration == 1) class="active" @endif></li>
                                        @endforeach
                                    </ol>
                                @endif
                                <div class="carousel-inner" style="height: auto" role="listbox">
                                    @foreach ($data['phase']->images as $image)
                                        <div class="carousel-item @if($loop->iteration == 1) active @endif">
                                            <img class="img-fluid"  src="{{asset('storage/'.$image->path)}}" alt="{{$data['phase']->name}}" />
                                        </div>
                                    @endforeach
                                </div>

                                @if (count($data['phase']->images) > 1)

                                    <a class="carousel-control-prev" href="#carousel-keyboard" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">{{ trans('admin.Previous') }}</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-keyboard" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">{{ trans('admin.Next') }}</span>
                                    </a>
                                    
                                @endif
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Phase images ends -->
        @endif

        <!-- Phase base data & Plan Starts -->
        <div class="row">
            <!-- phase basic info starts-->
            <div class="col-xl-12 col-lg-12 col-md-7">
                <div class="card user-card">
                    <div class="card-body">
                        <div class="row">

                            <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                <div class="user-avatar-section mb-3">
                                    <div class="d-flex justify-content-start">
                                        <div class="d-flex flex-column ml-1">
                                            <div class="user-info mb-1">
                                                <h4 class="mb-0">{{$data['phase']->name}} - {{$data['phase']->project->name}}</h4>
                                                <span class="card-text">{{$data['phase']->project && $data['phase']->project->charity ? $data['phase']->project->charity->email : trans('admin.no_charity')}}</span>
                                            </div>
                                            <div class="d-flex flex-wrap">
                                                <a href="{{route('admin.phases.edit', $data['phase']->id)}}" class="btn btn-primary">{{trans('admin.edit')}}</a>
                                                {{-- <button class="btn btn-outline-danger ml-1 remove-table" data-redirect_to_main="phase" data-url="{{route('admin.phases.destroy', $data['phase']->id)}}">{{trans('admin.delete')}}</button> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $session_currency = currencySymbol(session('currency'));
                                @endphp
                                <div class="d-flex align-items-center user-total-numbers">
                                    <div class="d-flex align-items-center mr-2">
                                        <div class="color-box bg-light-primary">
                                            <i data-feather="dollar-sign" class="text-primary"></i>
                                        </div>
                                        <div class="ml-1">
                                            <h5 class="mb-0">{{generalExchange($data['phase']->cost, $data['phase']->project->currency->symbol, $session_currency)}} {{$session_currency}}</h5>
                                            <small>{{trans('admin.cost')}}</small>
                                        </div>
                                    </div>
                                    <br><br><br>
                                    <div class="d-flex align-items-center">
                                        <div class="color-box bg-light-success">
                                            <i data-feather="dollar-sign" class="text-primary"></i>
                                        </div>
                                        <div class="ml-1">
                                            <h5 class="mb-0">{{generalExchange($data['phase']->project->get_total, $data['phase']->project->currency->symbol, $session_currency)}} {{$session_currency}}</h5>
                                            <small>{{trans('admin.total_project_cost')}}</small>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                <div class="user-info-wrapper">
                                    <div class="d-flex flex-wrap">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">{{trans('admin.order')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{trans('admin.order_'.$data['phase']->order)}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">{{trans('admin.start_date')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$data['phase']->start_date}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">{{trans('admin.end_date')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$data['phase']->end_date}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">{{trans('admin.duration')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$data['phase']->duration}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-0">{{trans('admin.status')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$data['phase']->approved ? trans('admin.closed') : trans('open')}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- phase basic info Ends-->
        </div>
        <!-- Phase base data & Plan Ends -->

        <!-- Phase general data Starts -->
        <div class="row">
            <!-- desc starts -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.desc')}}</h4>
                    </div>
                    <div class="card-body">
                        {!! $data['phase']->desc !!}
                    </div>
                </div>
            </div>
            <!-- desc Ends -->

            <!-- output starts -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.outside_level')}}</h4>
                    </div>
                    <div class="card-body">
                        {!! $data['phase']->output !!}
                    </div>
                </div>
            </div>
            <!-- output Ends -->

            <!-- cost_detail starts -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.details_cost')}}</h4>
                    </div>
                    <div class="card-body">
                        {!! $data['phase']->cost_details !!}
                    </div>
                </div>
            </div>
            <!-- cost_detail Ends -->

            @if($data['phase']->notes)
                <!-- notes starts -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-2">{{trans('admin.notes')}}</h4>
                        </div>
                        <div class="card-body">
                            {!! $data['phase']->notes !!}
                        </div>
                    </div>
                </div>
                <!-- notes Ends -->
            @endif

        </div>
        <!-- Phase general data Ends -->

        @if(count($data['phase']->reports) > 0)
            <!-- Phase reports Starts-->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.reports')}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row invoice-list-wrapper">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-datatable table-responsive">
                                        <table class="invoice-list-table table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>{{trans('admin.status')}}</th>
                                                    <th>{{trans('admin.created_at')}}</th>
                                                    <th>{{trans('admin.show')}}</th>
                                                    <th>{{trans('admin.actions')}}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse ($data['phase']->reports as $report)
                                                    <tr>
                                                        <td>{{$loop->iteration}}</td>
                                                        <td>{{trans('admin.'.$report->status)}}</td>
                                                        <td>{{$report->created_at->format('Y-m-d')}}</td>
                                                        <td>
                                                            <a href="{{route('admin.reports.show', $report->id)}}" title="{{trans('admin.show')}}">
                                                                <i data-feather="eye" class="mr-50"></i>
                                                            </a>
                                                    
                                                                    <a  href="{{route('admin.reports.edit', $report->id)}}" title="{{trans('admin.edit')}}">
                                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                                    </a>
                                                                    <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('admin.reports.destroy', $report->id)}}">
                                                                        <i data-feather="trash" class="mr-50"></i>
                                                                    </a>
                                                        </td>
                                                    </tr>
                                                @empty
                                                        <tr>
                                                            <td colspan="6">{{trans('admin.no_data')}}</td>
                                                        </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Phase reports Ends-->
        @endif

    </section>

@endsection