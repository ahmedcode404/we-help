@extends('layout.app')

@section('title', trans('admin.edit_question'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit_question')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="edit_contract" method="POST" action="{{ route('admin.questions.update' , $question->id) }}">
                            @csrf
                            @method('PUT')

                            <!-- BEGIN : question -->
                            <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="question_ar">{{trans('admin.question_ar')}}</label>
                                    <textarea class="form-control" id="question_ar" name="question_ar" cols="50" rows="10">{{$question->question_ar}}</textarea>
                                    @if($errors->has('question_ar'))
                                        <div class="error">{{ $errors->first('question_ar') }}</div>
                                    @endif                            
                                </div>                            
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="question_en">{{trans('admin.question_en')}}</label>
                                    <textarea class="form-control" id="question_en" name="question_en" cols="50" rows="10">{{$question->question_en}}</textarea>                                    
                                    @if($errors->has('question_en'))
                                        <div class="error">{{ $errors->first('question_en') }}</div>
                                    @endif                            
                                </div> 
                            </div>                           
                            
                            </div>
                            <!-- END : question -->


                            <!-- BEGIN : answer -->
                            <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="answer_ar">{{trans('admin.answer_ar')}}</label>
                                    <textarea class="form-control" id="answer_ar" name="answer_ar" cols="50" rows="10">{{$question->answer_ar}}</textarea>
                                    @if($errors->has('answer_ar'))
                                        <div class="error">{{ $errors->first('answer_ar') }}</div>
                                    @endif                            
                                </div>                            
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="answer_en">{{trans('admin.answer_en')}}</label>
                                    <textarea class="form-control" id="answer_en" name="answer_en" cols="50" rows="10">{{$question->answer_en}}</textarea>
                                    @if($errors->has('answer_en'))
                                        <div class="error">{{ $errors->first('answer_en') }}</div>
                                    @endif                            
                                </div> 
                            </div>                           
                            
                            </div>
                            <!-- END : answer -->

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')

        <script src="{{asset('dashboard/forms/contracts/edit.js')}}"></script>
        <script src="{{ url('custom/custom-validate.js') }}"></script>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <script>

            // ckeditor custom

            CKEDITOR.replace( 'content_ar' );
            CKEDITOR.replace( 'content_en' );
            
        </script>       
    @endpush

@endsection