@extends('layout.app')

@section('title', trans('admin.contracts'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            
                @if(checkPermissions(['add_contract']))
                    <a href="{{route('admin.questions.create')}}" style="display: block" class="btn btn-primary mb-2">{{trans('admin.create_question')}}</a>
                @endif

             
                {{-- <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.question_ar')}}</th>
                            <th>{{trans('admin.question_en')}}</th>
                            {{-- <th>{{trans('admin.answer_ar')}}</th>
                            <th>{{trans('admin.answer_en')}}</th> --}}
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            {{-- <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>  --}}
                        @foreach ($questions as $question)
                            <div class="card">
                                <div class="question-div">
                                    <h4>{{$question->question_ar}}</h4>
                                    {{ $question->answer_ar }}
                                </div>
                                <div class="question-div">
                                    <h4>{{$question->question_en}}</h4>
                                    {{ $question->answer_en }}
                                </div>

                                <div class="text-right">
                                    @if(checkPermissions(['edit_question']) || checkPermissions(['delete_question']))
                                        @if(checkPermissions(['edit_question']))
                                            <a  href="{{route('admin.questions.edit', $question->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                        @endif
                                        @if(checkPermissions(['delete_question']))
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.questions.destroy', $question->id)}}" title="{{trans('admin.delete')}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        @endforeach
                            {{-- <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$question->question_ar}}</td>
                                <td>{{$question->question_en}}</td>
                                {{-- <td>{{ $question->answer_ar }}</td>
                                <td>{{ $question->answer_en }}</td> --}}
                                {{-- <td>{{$question->nation->code}}</td> --}}
                                {{-- <td>
                                  
                                </td>
                            </tr>  --}}
                    {{-- </tbody>
                </table> --}}
            </div>

        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection