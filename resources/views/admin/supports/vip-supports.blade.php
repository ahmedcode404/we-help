@extends('layout.app')

@section('title', trans('admin.vip_supports'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    @if(checkPermissions(['create_finance_voucher']))
                        <a href="{{route('admin.supports.create-vip-support')}}" class="btn btn-primary">{{trans('admin.create_vip_supports')}}</a>
                    @endif
                    @php
                        $session_currency = currencySymbol(session('currency'));
                    @endphp
                   <div class="card-datatable table-responsive">
                       <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.supporter_name')}}</th>
                                <th>{{ trans('admin.email') }}</th>
                                <th>{{ trans('admin.phone') }}</th>
                                <th>{{trans('admin.project_name')}}</th>
                                <th>{{trans('admin.charity_name')}}</th>
                                <th>{{trans('admin.donation_cost')}} ({{$session_currency}})</th>
                                <th>{{trans('admin.project_covering')}}</th>
                                <th>{{trans('admin.support_repeating')}}</th>
                                {{-- <th>{{ trans('admin.receipt') }}</th> --}}
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['supports'] as $support)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$support->name_anonymouse == null ? $support->user->name : $support->name_anonymouse}}</td>
                                    <td>{{ $support->user ? $support->user->email : '' }}</td>
                                    <td>
                                        @if($support->user)
                                            <span dir="ltr">{{ $support->user->phone }}</span>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>{{$support->project ? $support->project->name : '-'}}</td>
                                    <td>{{$support->project->charity ? $support->project->charity->name : ''}}</td>
                                    <td>
                                        @if($support->project && $support->project->active == 1)
                                            {{generalExchange($support->cost, $support->currency->symbol, currencySymbol(session('currency')))}} {{currencySymbol(session('currency'))}}
                                        @elseif($support->project && $support->project->active == 0)
                                            {{trans('admin.not_active')}}
                                        @endif
                                    </td>
                                    <td>{{ $support->project_covering == 'total' ? trans('admin.total') : trans('admin.partial') }}</td>
                                    <td>{{trans('admin.'.$support->type)}}</td>
                                    <td>
                                        <a  href="{{ asset('storage/'.$support->receipt) }}" target="_blank" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                        @if(checkPermissions(['edit_finance_voucher']))
                                            <a  href="{{route('admin.supports.edit-vip-support', $support->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                   </div>
                </div>
                {{$data['supports']->links()}}
            </div>
        </div>
        
    </section>
    <!--/ Basic table -->

@endsection