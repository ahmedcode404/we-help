@extends('layout.app')

@section('title', trans('admin.suspended-supports'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')
<style>
    .table-responsive {
        overflow-x: auto;
    -webkit-overflow-scrolling: touch;
}
</style>
<!-- Basic table -->
<section id="basic-datatable">
    @php
        $session_currency = currencySymbol(session('currency'));
    @endphp
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.project')}}</th>
                            <th>{{trans('admin.charity')}}</th>
                            <th>{{trans('admin.frozen_cost')}} ({{$session_currency}})</th>  
                            <th>{{ trans('admin.actions') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['supports'] as $support)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$support->fromProject ? $support->fromProject->name : '-'}}</td>
                                <td>{{$support->fromProject && $support->fromProject->charity ? $support->fromProject->charity->name : '-'}}</td>
                                <td>{{exchange($support->cost, $support->currency_id, session('currency'))}} {{currencySymbol(session('currency'))}}</td>
                                <td>
                                    <a href="{{route('admin.supports.show-suspended-supports', $support->slug)}}" title=" {{trans('admin.show')}}">
                                        <i data-feather="eye" class="mr-50"></i>
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#info-{{$support->id}}" title="{{trans('admin.edit')}}">
                                        <i data-feather="edit-2" class="mr-50"></i>
                                    </a>
                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="info-{{$support->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5>{{trans('admin.transfere_cost')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{route('admin.supports.store', $support->id)}}" id="support" method="POST">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <select name="to_project_id" id="to_project_id" class="form-control" required>
                                                                    <option value="">{{trans('admin.select')}}</option>
                                                                    @foreach ($data['projects'] as $project)
                                                                        <option value="{{$project->id}}" @if(old('to_project_id') == $project->id) selected @endif required>{{$project->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                            <div class="col-3">
                
                                                                <input type="number" min="1" step=".01" name="cost" value="{{ old('cost') }}" placeholder="{{ trans('admin.cost') }}" class="form-control" required>
                                                            </div>
                                                            <div class="col-3">
                                                                <button type="submit" class="btn btn-primary">{{trans('admin.save')}}</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div class="modal-footer">
                                                    {{-- <button type="button" id="save_status" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['supports']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/frozen/create.js')}}"></script>
@endpush