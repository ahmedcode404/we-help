<div class="card-datatable table-responsive">
    <table class="datatables-basic table">
    <thead>
        <tr>
            <th>{{trans('admin.name')}}</th>
            <th>{{trans('admin.email')}}</th>
            <th>{{trans('admin.phone')}}</th>
            <th>{{trans('admin.country')}}</th>
            <th>{{trans('admin.city')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data['supports'] as $support)
            @foreach ($support->getSupporters() as $user)
                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{Countries::getOne($user->country, \App::getLocale())}}</td>
                    <td>{{$user->city}}</td>
                </tr>
            @endforeach
        @endforeach
    </tbody>
</table>
</div>
