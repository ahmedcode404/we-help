@extends('layout.app')

@section('title', trans('admin.supports'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.supporter_name')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.charity_name')}}</th>
                            <th>{{trans('admin.project_cost')}} ({{currencySymbol(session('currency'))}})</th>
                            <th>{{trans('admin.support_repeating')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['supports'] as $support)

                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$support->name_anonymouse == null ? $support->user->name : $support->name_anonymouse}}</td>
                                <td>{{$support->project ? $support->project->name : '-'}}</td>
                                <td>{{$support->project->charity ? $support->project->charity->name : ''}}</td>
                                <td>
                                    @if($support->project && $support->project->active == 1)
                                        {{generalExchange($support->cost, $support->currency->symbol, currencySymbol(session('currency')))}} {{currencySymbol(session('currency'))}}
                                    @elseif($support->project && $support->project->active == 0)
                                        {{trans('admin.not_active')}}
                                    @endif
                                </td>
                                <td>{{trans('admin.'.$support->type)}}</td>
                                {{-- <td>{{$support->nation->code}}</td> --}}
                                
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['supports']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection