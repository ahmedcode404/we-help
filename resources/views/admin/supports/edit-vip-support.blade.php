@extends('layout.app')

@section('title', trans('admin.edit_vip_supports'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" /> 
@endpush

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="vip-support" class="vip_support_form" method="POST" action="{{route('admin.supports.update-vip-support', $data['support']->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <input type="hidden" id="edit" value="true">

                            <input type="hidden" name="id" value="{{ $data['support']->user->id }}">
                            
                            <div class="row align-items-center">
                            @if($data['support']->user)
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="name">{{trans('admin.supporter_name')}}</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{$data['support']->user->name}}" placeholder="{{trans('admin.name')}}" required />
                                    <span class="error error-name"></span>
                                </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                    <label class="form-label" for="email">{{trans('admin.email')}}</label>
                                    <input type="email" id="email" name="email" class="form-control" value="{{$data['support']->user->email}}" placeholder="{{trans('admin.email')}}" required />
                                    <span class="error error-email"></span>
                                </div>
                            </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="phone">{{trans('admin.phone')}}</label>
                                    <input type="tel" class="form-control" value="{{ $data['support']->user->phone }}" name="phone" placeholder="{{trans('admin.phone')}}" id="phone" />
                                    <span class="error error-phone"></span>
                                </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="country">{{trans('admin.country')}}</label>
                                    <select class="form-control select2" id="country" name="country" required>
                                        <option value="">{{trans('admin.select')}} ...</option>
                                        @foreach ($data['countries'] as $key => $country)
                                            <option value="{{$key}}" @if($data['support']->user->country == $key) selected @endif>{{$country}}</option>
                                        @endforeach
                                    </select>
                                    <span class="error error-country"></span>
                                </div>
                                </div>
                            @endif

                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="project_id">{{trans('admin.project_name')}}</label>
                                <select class="form-control select2" id="project_id" name="project_id" required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['projects'] as $project)
                                        @if($project->get_total > getTotalSupportsForProject($project))
                                            <option value="{{$project->id}}" @if($data['support']->project_id == $project->id) selected @endif>{{$project->name}} - {{ $project->charity->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <span class="error error-project_id"></span>
                            </div>
                            </div>

                            <div class="col-md-6">
                            <div class="form-group demo-inline-spacing">
                                <label for="project_covering">{{trans('admin.project_covering')}}</label>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="total" name="project_covering" value="total" class="custom-control-input"
                                    @if($data['support']->project_covering == 'total') checked @endif />
                                    <label class="custom-control-label " for="total">{{trans('admin.total')}}</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input type="radio" id="partial" name="project_covering" value="partial" class="custom-control-input"
                                    @if($data['support']->project_covering == 'partial') checked @endif 
                                    />
                                    <label class="custom-control-label" for="partial">{{trans('admin.partial')}}</label>
                                </div>
                                <span class="error error-project_covering"></span>
                            </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                <label class="form-label" for="cost">{{trans('admin.cost')}}</label>
                                <input type="number" min="0" step=".01" id="cost" name="cost" class="form-control" value="{{$data['support']->cost}}" placeholder="{{trans('admin.cost')}}" required />
                                <span class="error error-cost"></span>
                            </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="currency_id">{{trans('admin.currency')}}</label>
                                <select class="form-control select2" id="currency_id" name="currency_id" required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['currencies'] as $currency)
                                        <option value="{{$currency->id}}" @if($data['support']->currency_id == $currency->id) selected @endif>{{$currency->symbol}}</option>
                                    @endforeach
                                </select>
                                <span class="error error-currency_id"></span>
                            </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                <label class="form-label" for="receipt">{{trans('admin.support_receipt')}}</label>
                                <input type="file" accept=".png,.jpg,.jpeg,.pdf" id="receipt" name="receipt" class="form-control" value="{{old('receipt')}}" placeholder="{{trans('admin.receipt')}}" />
                                <small>(Max size: 50MB)</small>
                                <span class="error error-receipt"></span>
                            </div>
                            </div>

                            
                            <div class="col-md-6">
                               <a  href="{{ asset('storage/'.$data['support']->receipt) }}" target="_blank">
                                    <i data-feather="eye" class="mr-50"></i>
                                    <span>{{trans('admin.show')}}</span>
                                </a>
                            </div>

                            <div class="col-12 error text-center general_error"></div>
                                <div class="col-12 mt-2" id="loading">
                                    <button type="button" class="btn btn-primary submit_vip_support_form_button" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection

@push('js')
    <script src="{{asset('dashboard/js/intlTelInput.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('dashboard/forms/vip-supports/edit.js')}}"></script>
@endpush