@extends('layout.app')

@section('title', trans('admin.show-suspended-supports'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section class="app-user-view">


        <!-- Project base data & Plan Starts -->
        <div class="row">
            <!-- project basic info starts-->
            <div class="col-12">
                <div class="card user-card">
                    <div class="card-body">
                                <div class="user-avatar-section">
                                    <div class="d-flex justify-content-start">
                                        <div class="d-flex flex-column">
                                            <div class="user-info mb-1">
                                                <h4 class="mb-0">{{$data['support']->fromProject ? $data['support']->fromProject->name : ''}}</h4>
                                                <span class="card-text">{{$data['support']->fromProject && $data['support']->fromProject->charity ? $data['support']->fromProject->charity->email : trans('admin.no_charity') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                               @if($data['support']->fromProject)
                                <div class="col-xl-3 col-sm-6 col-12 mb-2 user-total-numbers">
                                    <div class="media">
                                        <div class="avatar bg-light-primary mr-2">
                                            <div class="avatar-content">
                                                <i data-feather="dollar-sign" class="avatar-icon"></i>
                                            </div>
                                        </div>
                                        <div class="media-body my-auto">
                                            <h4 class="font-weight-bolder mb-0">{{$data['support']->fromProject->get_total}} {{$data['session_currency']}}</h4>
                                            <p class="card-text font-small-3 mb-0">{{trans('admin.project_cost')}}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif 

                            <div class="col-xl-3 col-sm-6 col-12 mb-2 user-total-numbers">
                                <div class="media">
                                    <div class="avatar bg-light-primary mr-2">
                                        <div class="avatar-content">
                                            <i data-feather="dollar-sign" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{$data['support']->prev_donations}} {{$data['session_currency']}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{trans('admin.total_supports')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-sm-6 col-12 mb-2 user-total-numbers">
                                <div class="media">
                                    <div class="avatar bg-light-primary mr-2">
                                        <div class="avatar-content">
                                            <i data-feather="dollar-sign" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{ $data['support']->out_to_charity }} {{$data['session_currency']}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{trans('admin.out_to_charity')}}</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-3 col-sm-6 col-12 mb-2 user-total-numbers">
                                <div class="media">
                                    <div class="avatar bg-light-primary mr-2">
                                        <div class="avatar-content">
                                            <i data-feather="dollar-sign" class="avatar-icon"></i>
                                        </div>
                                    </div>
                                    <div class="media-body my-auto">
                                        <h4 class="font-weight-bolder mb-0">{{$data['support']->cost}} {{$data['session_currency']}}</h4>
                                        <p class="card-text font-small-3 mb-0">{{trans('admin.frozen_cost')}}</p>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /project basic info Ends-->

        </div>
        <!-- Project base data & Plan Ends -->
       

        <!-- Projects  Starts-->
        <div class="row invoice-list-wrapper">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.projects')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table  class="invoice-list-table datatables-basic table text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('admin.project_num')}}</th>
                                    <th>{{trans('admin.project_name')}}</th>
                                    <th>{{$data['session_currency']}} {{trans('admin.project_cost')}}</th>
                                    <th>{{ trans('admin.transfered_cost') }}</th>
                                    <th>{{trans('admin.superadmin_ratio')}}</th>
                                    <th>{{trans('admin.actions')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data['support_projects'] as $support)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$support->toProject ? $support->toProject->project_num : '-'}}</td>
                                        <td>{{$support->toProject ? $support->toProject->name : '-'}}</td>
                                        <td>
                                            @if($support->toProject)
                                                {{generalExchange($support->toProject->get_total, $support->toProject->currency->symbol, $data['session_currency'])}} {{$data['session_currency']}}
                                            @else
                                                0.0 {{$data['session_currency']}}
                                            @endif
                                        </td>
                                        <td>{{ $support->to_project_cost }} {{ $data['session_currency'] }}</td>
                                        <td>{{$support->toProject && $support->toProject->category ? $support->toProject->category->superadmin_ratio.' %' : 0.0.' %'}}</td>
                                        <td>
                                            @if($support->toProject)

                                                @if(checkPermissions(['show_project']))
                                                    <a  href="{{route('admin.projects.show', $support->toProject->id)}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                        {{-- <span>{{trans('admin.show')}}</span> --}}
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['edit_project_data']))
                                                    <a  href="{{route('admin.projects.edit', $support->toProject->id)}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                        {{-- <span>{{trans('admin.edit')}}</span> --}}
                                                    </a>
                                                @endif
                                                {{-- @if(checkPermissions(['delete_project']))
                                                    <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.projects.destroy', $support->toProject->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                        <span>{{trans('admin.delete')}}</span>
                                                    </a>
                                                @endif --}}
                                            @endif
                                             
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="6">{{trans('admin.no_data')}}</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Projects  Ends-->
        
        <!-- Supporters  Starts-->
        <div class="row invoice-list-wrapper">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.supporters')}}</h4>
                        <a href="{{ route('admin.export-suspended-supports-supporters') }}" class="btn btn-primary">{{ trans('admin.export') }}</a>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table  class="invoice-list-table datatables-basic table  text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('admin.name')}}</th>
                                    <th>{{trans('admin.email')}}</th>
                                    <th>{{trans('admin.phone')}}</th>
                                    <th>{{trans('admin.country')}}</th>
                                    {{-- <th>{{trans('admin.city')}}</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data['support']->getSupporters() as $user)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td><span dir="ltr">{{$user->phone}}</span></td>
                                        <td>{{Countries::getOne($user->country, \App::getLocale())}}</td>
                                        {{-- <td>{{$user->city}}</td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <!-- Supporters  Ends-->

       
    </section>
@endsection