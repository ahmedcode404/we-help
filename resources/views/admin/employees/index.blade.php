@extends('layout.app')

@section('title', trans('admin.employees'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_emp']))
                    <a href="{{route('admin.employees.create')}}" class="btn btn-primary">{{trans('admin.add_employee')}}</a>
                @endif            
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_employee_ar')}}</th>
                            <th>{{trans('admin.name_employee_en')}}</th>
                            <th>{{trans('admin.nation_number')}}</th>
                            <th>{{trans('admin.country')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($employees as $employee)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $employee->name_ar }}</td>
                                <td>{{ $employee->name_en }}</td>
                                <td>{{ $employee->nation_number }}</td>
                                <td>{{ Countries::getOne($employee->country, \App::getLocale()) }}</td>
                                <td>
                                    @if(checkPermissions(['show_emp']))
                                        <a  href="{{route('admin.employees.show', $employee->id)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                    {{-- @else
                                        {{trans('admin.do_not_have_permission')}} --}}
                                    @endif
                                    @if(checkPermissions(['edit_emp']) || checkPermissions(['delete_emp']))
                                                @if(checkPermissions(['edit_emp']))
                                                    <a  href="{{route('admin.employees.edit', $employee->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_emp']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.employees.destroy', $employee->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$employees->links()}}
            </div>
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection


@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush