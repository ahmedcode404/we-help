@extends('layout.app')

@section('title', trans('admin.show_emp'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section class="app-user-view">
        <!-- Charity Card & Plan Starts -->
            <!-- Charity Card starts-->
                <div class="card user-card">
                        <div class="row">
                            <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                                <div class="user-avatar-section">
                                    <div class="d-flex justify-content-start">
                                        <img class="img-fluid rounded" src="{{asset('dashboard/images/avatar.png')}}" style="max-height:70px" width="70" alt="{{$employee->name}}" />
                                        <div class="d-flex flex-column ml-1">
                                            <div class="user-info mb-1">
                                                <h4 class="mb-0">{{$employee->name}} ({{$employee->nation->code}})</h4>
                                                <span class="card-text">{{$employee->email}}</span><br>
                                                <span class="card-text">{{Countries::getOne($employee->country, \App::getLocale())}}</span><br>
                                                <span class="card-text">{{$employee->city}}</span>
                                            </div>
                                            <div class="d-flex flex-wrap">
                                                @if(checkPermissions(['edit_emp']))

                                                    <a href="{{route('admin.employees.edit', $employee->id)}}" class="btn btn-primary">{{trans('admin.edit')}}</a>
                                               
                                                @endif
                                            
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                                <div class="user-info-wrapper">
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.nationality')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->nationality->name}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.nation_number')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->nation_number}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.region')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->region}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.street')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->street}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.unit_number')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->unit_number}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.mail_box')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->mail_box}}</p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.postal_code')}}</span>
                                        </div>
                                        <p class="card-text mb-0">
                                            {{$employee->postal_code}}
                                        </p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.phone')}}</span>
                                        </div>
                                        <p class="card-text mb-0" dir="ltr">
                                            {{$employee->phone}}
                                        </p>
                                    </div>
                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.contract_start_date')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->contract_start_date}}</p>
                                    </div>

                                    <div class="d-flex flex-wrap my-50">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.contract_end_date')}}</span>
                                        </div>
                                        <p class="card-text mb-0">{{$employee->contract_end_date}}</p>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                </div>
            <!-- /Charity Card Ends-->

            <!-- charity categories Card starts-->
                <div class="card plan-card border-primary">
                    <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                        <h5 class="mb-0">{{trans('admin.permissions')}}</h5>
                        
                    </div>
                    <div class="card-body">
                        <ul class="list-inline my-0">
                            @forelse ($employee->permissions as $permission)
                                <li class=" badge badge-light-primary  mb-1">{{ __('admin.' .$permission->name) }}</li> 
                            @empty
                                <div>{{trans('admin.no_data')}}</div>
                            @endforelse
                        </ul>
                        <br>
                    </div>
                </div>
            <!-- /charity categories CardEnds -->
            
        </div>
        <!-- Charity Card & Plan Ends -->

    </section>

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush