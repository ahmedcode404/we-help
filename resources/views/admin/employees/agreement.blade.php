@extends('layout-login.app')

@section('title', trans('admin.emp_agreement'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')


<div class="mian-div"> 
    <!-- search header -->
    <section id="faq-search-filter">
        <div class="card faq-search" style="background:#fff">
            <div class="card-body">
                <!-- main title -->
                <h2 class="text-primary">{{trans('admin.emp_agreement')}}</h2>

                <!-- subtitle -->
                <p class="card-text mb-2">{{trans('admin.emp_agreement_text')}}</p>

    <!-- /search header -->

    <!-- frequently asked questions tabs pills -->
    <div id="faq-tabs">
        <!-- vertical tab pill -->
                {!! $data['agreement']->content !!}

                <br><br>

                <!-- search input -->
                <form id="agreement_form" class="faq-search-input" action="{{route('emp.agree')}}" method="POST">
                    @csrf

                        <input type="hidden" value="{{$data['user_email']}}" name="user_email">

                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="agreement_approve" name="agreement_approve" required />
                                <label class="custom-control-label" for="agreement_approve">{{trans('admin.do_agree')}}</label>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">{{trans('admin.save')}}</button>
                </form>
            </div>
    </div>
    <!-- / frequently asked questions tabs pills -->
</div>

    </section>
@endsection

@push('js')
    <script src="{{asset('dashboard/forms/employees/check-role-all-permission.js')}}"></script>
@endpush