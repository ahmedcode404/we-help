@extends('layout.app')

@section('title', trans('admin.add_employee'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
     
    <link rel="stylesheet" href="{{ asset('dashboard/css/intlTelInput.min.css') }}" type="text/css" />
@endpush
@section('content')

    <!-- users edit start -->
    <section class="app-user-edit">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ trans('admin.add_employee') }}</h4>
            </div>
            <div class="card-body">
                <div class="tab-content">
                    <!-- Account Tab starts -->
                    <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
                        <form class="form-validate employee_form" id="form-employee" method="post"
                            action="{{ route('admin.employees.store') }}">
                            @csrf
                            <div class="row">

                                {{-- name_employee_ar start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="name_employee_ar">{{ trans('admin.name_employee_ar') }}</label>
                                        <input type="text" class="form-control" id="name_employee_ar"
                                            name="name_employee_ar" value="{{ old('name_employee_ar') }}"
                                            placeholder="{{ trans('admin.name_employee_ar') }}" />
                                            <div class="error error-name_employee_ar"></div>
                                        
                                    </div>
                                </div>
                                {{-- name_employee_ar end --}}

                                {{-- name_employee_en start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="name_employee_en">{{ trans('admin.name_employee_en') }}</label>
                                        <input type="text" class="form-control" id="name_employee_en"
                                            name="name_employee_en" value="{{ old('name_employee_en') }}"
                                            placeholder="{{ trans('admin.name_employee_en') }}" />
                                            <div class="error error-name_employee_en"></div>
                                    </div>
                                </div>
                                {{-- name_employee_en end --}}


                                {{-- jop category start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="jop_category_id">{{ trans('admin.jop_category') }}</label>
                                        <select name="jop_category_id" id="jop_category_id"
                                            data-url="{{ route('admin.jop.jopCategory') }}" class="form-control">
                                            <option value="">{{ trans('admin.choose_jop_category') }}</option>
                                            @foreach ($jop_categories as $jop_category)
                                                <option value="{{ $jop_category->id }}">{{ $jop_category->name }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <div class="error error-jop_category_id"></div>
                                        <div class="error error-job_id"></div>
                                        
                                    </div>
                                </div>
                                {{-- jop category end --}}

                                {{-- job start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="job_id">{{ trans('admin.jop') }}</label>
                                        <select name="job_id" id="job_id" class="form-control append-jop">
                                            <option>{{ trans('admin.select') }}</option>
                                        </select>
                                        <div class="error error-job_id"></div>
                                    </div>
                                </div>
                                {{-- job end --}}

                                {{-- nation_number start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="nation_number">{{ trans('admin.nation_number') }}</label>
                                        <input type="text" class="form-control" id="nation_number" name="nation_number"
                                            value="{{ old('nation_number') }}"
                                            placeholder="{{ trans('admin.nation_number') }}" />
                                            <div class="error error-nation_number"></div>
                                    </div>
                                </div>
                                {{-- nation_number end --}}

                                {{-- phase country start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">{{ trans('admin.country') }}</label>
                                        <select name="country" id="country"
                                            data-action="{{ route('admin.city.country') }}" class="form-control select2">
                                            <option value="">{{ trans('admin.select') }} ...</option>
                                            @foreach ($countries as $key => $country)
                                                <option value="{{ $key }}" @if ($key == old('country')) selected @endif>
                                                    {{ $country }}</option>
                                            @endforeach
                                        </select>
                                        <div class="error error-country"></div>
                                    </div>
                                </div>
                                {{-- phase country end --}}


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="city">{{ trans('admin.city') }}</label>
                                        <input type="text" class="form-control" id="city" name="city"
                                            value="{{ old('city') }}" placeholder="{{ trans('admin.city') }}" />
                                            <div class="error error-city"></div>
                                    </div>
                                </div>

                                {{-- region start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="region">{{ trans('admin.region') }}</label>
                                        <input type="text" class="form-control" id="region" name="region"
                                            value="{{ old('region') }}" placeholder="{{ trans('admin.region') }}" />
                                            <div class="error error-region"></div>
                                    </div>
                                </div>
                                {{-- region end --}}


                                {{-- street start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="street">{{ trans('admin.street') }}</label>
                                        <input type="text" class="form-control" id="street" name="street"
                                            value="{{ old('street') }}" placeholder="{{ trans('admin.street') }}" />
                                            <div class="error error-street"></div>
                                    </div>
                                </div>
                                {{-- street end --}}


                                {{-- unit_number start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="unit_number">{{ trans('admin.unit_number') }}</label>
                                        <input type="number" step="1" min="0" class="form-control" id="unit_number"
                                            name="unit_number" value="{{ old('unit_number') }}"
                                            placeholder="{{ trans('admin.unit_number') }}" />
                                            <div class="error error-unit_number"></div>
                                    </div>
                                </div>
                                {{-- unit_number end --}}


                                {{-- mail_box start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="mail_box">{{ trans('admin.mail_box') }}</label>
                                        <input type="text" class="form-control" id="mail_box" name="mail_box"
                                            value="{{ old('mail_box') }}" placeholder="{{ trans('admin.mail_box') }}" />
                                            <div class="error error-mail_box"></div>
                                    </div>
                                </div>
                                {{-- mail_box end --}}

                                {{-- postal_code start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="postal_code">{{ trans('admin.postal_code') }}</label>
                                        <input type="number" min="0" minlength="5" class="form-control"
                                            id="postal_code" name="postal_code" value="{{ old('postal_code') }}"
                                            placeholder="{{ trans('admin.postal_code') }}" />
                                            <div class="error error-postal_code"></div>
                                    </div>
                                </div>
                                {{-- postal_code end --}}

                                {{-- phone start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="phone">{{ trans('admin.phone') }}</label>
                                        <input type="tel" minlength="8" maxlength="16" class="form-control" id="phone"
                                            name="phone_num" value="{{ session('emp_phone') ? session('emp_phone') : old('phone_num') }}"
                                            placeholder="{{ trans('admin.phone') }}" />
                                            <div class="error error-phone"></div>
                                    </div>
                                </div>
                                {{-- phone end --}}


                                {{-- email start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="email">{{ trans('admin.email') }}</label>
                                        <input type="email" class="form-control" id="" name="email"
                                            value="{{ old('email') }}" placeholder="{{ trans('admin.email') }}" />
                                            <div class="error error-email"></div>
                                    </div>
                                </div>
                                {{-- email end --}}

                                {{-- contract_start_date start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="contract_start_date">{{ trans('admin.contract_start_date') }}</label>
                                        
                                        <input type="text" class="form-control date-picker" readonly  autocomplete="off" id="contract_start_date"
                                            name="contract_start_date" value="{{ old('contract_start_date') }}"
                                            placeholder="{{ trans('admin.contract_start_date') }}" />

                                            <div class="error error-contract_start_date"></div>
                                    </div>
                                </div>
                                {{-- contract_start_date end --}}

                                {{-- contract_end_date start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="contract_end_date">{{ trans('admin.contract_end_date') }}</label>

                                        <input type="text" class="form-control date-picker" readonly  autocomplete="off" id="contract_end_date"
                                            name="contract_end_date" value="{{ old('contract_end_date') }}"
                                            placeholder="{{ trans('admin.contract_end_date') }}" />

                                            <div class="error error-contract_end_date"></div>
                                    </div>
                                </div>
                                {{-- contract_end_date end --}}

                                {{-- degree_id start --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="degree_id">{{ trans('admin.degree') }}</label>
                                        <select class="form-control select2" id="degree" name="degree_id">
                                            <option value="">{{ trans('admin.select') }} ...</option>
                                            @foreach ($degrees as $degree)
                                                <option value="{{ $degree->id }}" @if (old('degree_id') == $degree->id) selected @endif>
                                                    {{ $degree->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="error error-degree_id"></div>
                                    </div>

                                </div>
                                {{-- degree_id end --}}

                                {{-- nationality_id start --}}
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="nationality_id">{{ trans('admin.nationality') }}</label>
                                        <select class="form-control select2" id="nationality_id" name="nationality_id">
                                            <option value="">{{ trans('admin.select') }} ...</option>
                                            @foreach ($nationalities as $nationality)
                                                <option value="{{ $nationality->id }}" @if (old('nationality_id') == $nationality->id) selected @endif>
                                                    {{ $nationality->name }}</option>
                                            @endforeach
                                        </select>
                                        <div class="error error-nationality_id"></div>
                                    </div>

                                </div>
                                {{-- nationality_id end --}}

                                {{-- nation_id start --}}

                                {{-- <div class="col-md-12">

                                            <div class="form-group">
                                                <label for="nation_id">{{trans('admin.nation')}}</label>
                                                <select class="form-control select2" id="nation_id" name="nation_id">
                                                    <option value="">{{trans('admin.select')}} ...</option>
                                                    @foreach ($countries as $country)
                                                        @if ($country->id == 1 || $country->id == 2)
                                                            <option value="{{$country->id}}" @if (old('nation_id') == $country->id) selected @endif>{{$country->name}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('nation_id'))
                                                    <div class="error">{{ $errors->first('nation_id') }}</div>
                                                @endif                                                  
                                            </div> 

                                        </div> --}}
                                {{-- nation_id end --}}
                                    </div>
                                <hr>
                                <!-- PERMISSIONS starts -->
                                            <h4 class="card-title">{{ __('admin.permissions') }}</h4>
                                            <ul class="nav nav-tabs row" role="tablist">
                                                @foreach ($roles as $key => $role)
                                                    <li class="nav-item col-lg-3 col-sm-6 mb-2">
                                                        <a class="nav-link {{ $roles[0]->id == $role->id ? 'active' : '' }}"
                                                            id="homeIcon-tab" data-toggle="tab"
                                                            href="#homeIcon-{{ $key }}" aria-controls="home"
                                                            role="tab" aria-selected="true">
                                                            {{ __('admin.' . $role->name) }}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <hr>
                                            <div class="tab-content">
                                                @foreach ($roles as $key => $role)
                                                    <div class="tab-pane {{ $roles[0]->id == $role->id ? 'active' : '' }}"
                                                        id="homeIcon-{{ $key }}" aria-labelledby="homeIcon-tab"
                                                        role="tabpanel">


                                                        <div class="row no-marg-row">
                                                            <div class="col-lg-3 col-md-4 col-sm-6">
                                                                <div class="form-group">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox"
                                                                            id="role-{{ $role->id }}"
                                                                            data-id="{{ $role->id }}"
                                                                            class="custom-control-input check-all-permission {{ $role->name }}">
                                                                            <label class="custom-control-label" for="role-{{ $role->id }}">{{ __('admin.check_all') }}</label>
                                                                        <span class="error-input error"></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            @foreach ($role->permissions as $permission)


                                                                <div class="col-lg-3 col-md-4 col-sm-6">
                                                                    <div class="form-group">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                            id="{{ $permission->id }}"
                                                                            value="{{ $permission->id ? $permission->id : old('permission') }}"
                                                                            name="permission[]"
                                                                                class="custom-control-input one-permission permission-{{ $role->id }}">
                                                                                <label class="custom-control-label" for="{{ $permission->id }}">{{ __('admin.' . $permission->name) }}</label>
                                                                            <span class="error-input error"></span>
                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            @endforeach

                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                <!-- PERMISSIONS ends -->

                                <hr>
                                <div class="error error-permission"></div>
                              
                                <div class="col-12 error text-center general_error"></div>
                                <div class="mt-2" id="loading">
                                    <button type="button" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 employee_form_button">
                                        {{ __('admin.save') }}</button>
                                </div>
                            </div>
                        </form>
                        <!-- users edit account form ends -->
                    </div>
                    <!-- Account Tab ends -->

                </div>
            </div>
    </section>
    <!-- users edit ends -->

    @push('js')
        <script src="{{ asset('dashboard/js/intlTelInput.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('dashboard/forms/employees/check-role-all-permission.js') }}"></script>
        <script src="{{ asset('dashboard/forms/employees/phone_key.js') }}"></script>
        <script src="{{ asset('dashboard/forms/employees/jopOfjopCategory.js') }}"></script>
        <script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>
        @if(\App::getLocale() == 'ar')
        <!--for arabic-only-->
        <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
        <!--end-->
    @endif     
    <script>
         $('.date-picker').datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        })
    </script>
    @endpush

@endsection
