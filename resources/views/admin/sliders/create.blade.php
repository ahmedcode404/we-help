@extends('layout.app')

@section('title', trans('admin.create-sliders'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.create-sliders')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="slider" method="POST" class="request_form" action="{{route('admin.sliders.store')}}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label for="appearance">{{trans('admin.slider_appearance')}}</label>
                                <select class="form-control select2" id="appearance" name="appearance" required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    <option value="web" @if(old('appearance') == 'web') selected @endif>{{trans('admin.web')}} (Width: 1920px * Height: 650px)</option>
<<<<<<< HEAD
                                    <option value="mob" @if(old('appearance') == 'mob') selected @endif>{{trans('admin.mob')}} (Max width: 760px -  Max height: 500px)</option>
=======
                                    <option value="mob" @if(old('appearance') == 'mob') selected @endif>{{trans('admin.mob')}} (Max width: 760px)</option>
>>>>>>> f9190a43505a1bb494353d40385d2013490f91ee
                                </select>
                                <span class="error error-appearance"></span>
                            </div>
                            
                            <div class="form-group">
                                <label class="form-label" for="path">{{trans('admin.slider_image')}} 
                                    (For web: Width: 1920px * Height: 650px ) <br>
<<<<<<< HEAD
                                    (For mobile: Max width: 760px - Max height: 500px)
=======
                                    (For mobile: Max width: 760px)
>>>>>>> f9190a43505a1bb494353d40385d2013490f91ee
                                </label>
                                <input type="file" accept=".jpg, .jpeg,.png,.gif" class="form-control" id="path" name="path" placeholder="{{trans('admin.path')}}" required />
                                <small>(Max size: 20MB)</small>
                                <span class="error error-path"></span>
                            </div>
                            
                            <div class="form-group">
                                <label class="form-label" for="title_ar">{{trans('admin.slider_title_ar')}}</label>
                                <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{old('title_ar')}}" placeholder="{{trans('admin.title_ar')}}" required />
                                <span class="error error-title_ar"></span>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="title_en">{{trans('admin.slider_title_en')}}</label>
                                <input type="text" class="form-control" id="title_en" name="title_en" value="{{old('title_en')}}" placeholder="{{trans('admin.title_en')}}" required />
                                <span class="error error-title_en"></span>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="text_ar">{{trans('admin.slider_content_ar')}}</label>
                                <textarea id="text_ar" name="text_ar" class="form-control" placeholder="{{trans('admin.content_ar')}}" required >{{old('text_ar')}}</textarea>
                                <span class="error error-text_ar"></span>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="text_en">{{trans('admin.slider_content_en')}}</label>
                                <textarea id="text_en" name="text_en" class="form-control" placeholder="{{trans('admin.content_en')}}" required >{{old('text_en')}}</textarea>
                                <span class="error error-text_en"></span>
                            </div>                                                      

                            <div class="row">
                                <div class="col-12 error text-center general_error"></div>
                                <div class="col-12" id="loading">
                                    <button type="button" class="btn btn-primary submit_request_form_button" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/slider/create.js')}}"></script>
        <script src="{{asset('custom/submit_simple_forms.js')}}"></script>
    @endpush
@endsection