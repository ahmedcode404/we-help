@extends('layout.app')

@section('title', trans('admin.sliders'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_sliders']))
                    <a href="{{route('admin.sliders.create')}}" class="btn btn-primary">{{trans('admin.create-sliders')}}</a>
                @endif
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.slider_image')}}</th>
                            <th>{{trans('admin.slider_title')}}</th>
                            <th>{{trans('admin.slider_text')}}</th>
                            <th>{{trans('admin.slider_appearance')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['sliders'] as $slider)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    <img src="{{asset('storage/'.$slider->path)}}" style="max-width: 100px;max-height:100px"  alt="{{$slider->title}}">
                                </td>
                                <td>{{$slider->title}}</td>
                                <td>{{$slider->text}}</td>
                                <td>{{trans('admin.'.$slider->appearance)}}</td>
                                <td>
                                    @if(checkPermissions(['edit_sliders']) || checkPermissions(['delete_sliders']))
                                   
                                                @if(checkPermissions(['edit_sliders']))
                                                    <a href="{{route('admin.sliders.edit', $slider->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_sliders']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.sliders.destroy', $slider->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                           
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['sliders']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection