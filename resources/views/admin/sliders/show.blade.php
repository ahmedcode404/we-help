@extends('layout.app')

@section('title', trans('admin.show-job-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.show')}}</h4>
                    </div>
                    <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name_en')}}</th>
                                <th>{{trans('admin.name_ar')}}</th>
                                <th>{{trans('admin.nation')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['cat']->name_en}}</td>
                                <td>{{$data['cat']->name_ar}}</td>
                                <td>{{$data['cat']->nation->code}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i data-feather="more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('admin.job-categories.edit', $data['cat']->id)}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                                <span>{{trans('admin.edit')}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.jobs')}}</h4>
                    </div>
                    <div class="card-body">
                        
                        <table>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                            <tr>
                                @foreach($data['cat']->jobs as $job)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$job->name}}</td>
                                        <td></td>
                                    </tr>
                                @endforeach
                            </tr>
                        </table>
                          
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection