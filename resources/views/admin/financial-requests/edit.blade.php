@extends('layout.app')

@section('title', trans('admin.edit-financial-request'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit-financial-request')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="financial-request" class="request_form" method="POST" action="{{route('admin.financial-requests.update', $data['request']->id)}}">
                            @csrf
                            @method('PUT')

                            <input type="hidden" id="edit" value="true">

                            <div class="form-group">
                                <label for="requestable_id">{{trans('admin.charity_name')}}</label>
                                <select class="form-control select2" id="requestable_id" name="requestable_id"
                                    data-action="{{route('admin.get-charity-projects')}}"
                                    data-method="POST"
                                    required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['charities'] as $charity)
                                        <option value="{{$charity->id}}" @if(old('requestable_id') == $charity->id || $data['request']->requestable_id == $charity->id) selected @endif>{{$charity->name}}</option>
                                    @endforeach
                                </select>
                                <span class="error error-requestable_id"></span>
                            </div>

                            <div class="form-group">
                                <label for="project_id">{{trans('admin.project_name')}}</label>
                                <div id="projects_container">
                                    <select class="form-control select2" id="project_id" name="project_id" required>
                                        <option value="">{{trans('admin.select')}} ...</option>
                                        @foreach ($data['projects'] as $project)
                                            <option value="{{$project->id}}" @if(old('project_id') == $project->id || $data['request']->project_id == $project->id) selected @endif>{{$project->name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="error error-project_id"></span>
                                </div>
                            </div>

                            @if(checkPermissions(['accept_refuse_finance_requests']))
                                <div class="form-group">
                                    <label for="status">{{trans('admin.request_status')}}</label>
                                    <select id="status" class="form-control" name="status">
                                        <option value="waiting" @if($data['request']->status == 'waiting' || old('status') == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                        <option value="approved" @if($data['request']->status == 'approved' || old('status') == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                        <option value="rejected" @if($data['request']->status == 'rejected' || old('status') == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                        <option value="hold" @if($data['request']->status == 'hold' || old('status') == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                    </select>
                                </div>
                                <span class="error error-status"></span>
                            @endif

                            <div class="form-group">
                                <label for="notes">{{trans('admin.notes')}}</label>
                                <textarea name="notes" id="notes" cols="30" rows="6" class="form-control">{{$data['request']->notes ? $data['request']->notes : old('notes')}}</textarea>
                                <span class="error error-notes"></span>
                            </div>

                            <div class="form-group">
                                <label for="out_to_charity">{{trans('admin.out_to_charity')}}
                                    ( {{ trans('admin.fin_note') }} )
                                </label>
                                <input type="number" style="0.1" name="out_to_charity" id="out_to_charity" placeholder="0.0" class="form-control" value="{{$data['request']->out_to_charity ? $data['request']->out_to_charity : old('out_to_charity')}}" required>
                                <span class="error error-out_to_charity"></span>
                            </div>

                            <div class="row">
                                <div class="col-12" id="loading">
                                    <button type="button" class="btn btn-primary submit_request_form_button">{{trans('admin.save')}}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/financial-requests/create.js')}}"></script>
    @endpush
@endsection