@extends('layout.app')

@section('title', trans('admin.financial_requests'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_finance_request']))
                    <a href="{{route('admin.financial-requests.create')}}" class="btn btn-primary">{{trans('admin.create-financial-request')}}</a>
                @endif
                <br>
                <div class="row taps_eidt  justify-content-center">
                    @php
                        if(request()->segment(count(request()->segments())) == 'exchange'){
                            $type = 'exchange';
                        }
                        else if(request()->segment(count(request()->segments())) == 'catch'){
                            $type = 'catch';
                        }
                        else{
                            $type = 'all';
                        }
                    @endphp
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-requests.index')}}" class="btn btn-primary">{{trans('admin.all')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-requests-status', 'waiting')}}" class="btn btn-warning">{{trans('admin.waiting')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-requests-status', 'approved')}}" class="btn btn-success">{{trans('admin.approved')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-requests-status', 'hold')}}" class="btn btn-info">{{trans('admin.hold')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-requests-status', 'rejected')}}" class="btn btn-danger">{{trans('admin.rejected')}}</a>
                    </div>
                </div>
                <br>
                @php
                    $session_currency = currencySymbol(session('currency'));
                @endphp
                <div class="card-datatable table-responsive">
                      <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.charity_name')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.total_out_to_charity')}}</th>
                            <th>{{trans('admin.out_to_charity')}}</th>
                            <th>{{trans('admin.request_status')}}</th>
                            <th>{{trans('admin.recieve_status')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['financial_requests'] as $request)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$request->requestable ? $request->requestable->name : ''}}</td>
                                <td>{{$request->project ? $request->project->name : ''}}</td>
                                <td>{{$request->requestable && $request->requestable->getTotalOutToCharity() ? $request->requestable->getTotalOutToCharity().' '.$session_currency : 0.0}}</td>
                                <td>{{generalExchange($request->out_to_charity, $request->currency->symbol, $session_currency)}} {{$session_currency}}</td>
                                <td>
                                    @if($request->status == 'approved')
                                        <span id="current_status-{{$request->id}}" class="badge badge-light-success">{{trans('admin.'.$request->status)}}</span>
                                    @elseif($request->status == 'waiting')
                                        <span id="current_status-{{$request->id}}" class="badge badge-light-warning">{{trans('admin.'.$request->status)}}</span>
                                    @elseif($request->status == 'hold')
                                        <span id="current_status-{{$request->id}}" class="badge badge-light-info">{{trans('admin.'.$request->status)}}</span>
                                    @elseif($request->status == 'rejected')
                                        <span id="current_status-{{$request->id}}" class="badge badge-light-danger">{{trans('admin.'.$request->status)}}</span>
                                    @endif
                                    {{-- <a class="btn btn-primary" data-toggle="modal" data-target="#financial_request-{{$request->id}}">{{trans('admin.change')}}</a>
                                    <!-- Modal -->  
                                    <div class="modal fade modal-info text-left" id="financial_request-{{$request->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="spinner-grow text-success mr-1 pross processing-div-{{ $request->id }}" style="position: absolute;margin: 26px -30px 37px 242px;z-index: 100;display: none;" role="status">
                                            <span class="sr-only">Loading...</span>
                                        </div> 
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="change_request_status-{{$request->id}}">{{trans('admin.select_status')}}</label>
                                                    <select id="change_request_status-{{$request->id}}" class="form-control">
                                                        <option value="approved" @if($request->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                        <option value="rejected" @if($request->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                        <option value="hold" @if($request->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="request_notes-{{$request->id}}">{{trans('admin.notes')}}</label>
                                                    <textarea id="request_notes-{{$request->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$request->notes}}</textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="modal-footer">
                                                <button type="button" id="" class="btn btn-info save_status" data-model="financial_request" data-id="{{$request->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                            </div>
                                        </div>
                                    </div>                                                             --}}
                                </td>
                                <td>{{trans('admin.'.$request->recieve_status)}}</td>
                                <td>
                                    @if(checkPermissions(['show_finance_request']))
                                        <a href="{{route('get-voucher', $request->slug)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                    {{-- @else
                                        {{trans('admin.do_not_have_permission')}}  --}}
                                    @endif
                              
                                    @if(checkPermissions(['edit_finance_request']) || checkPermissions(['delete_finance_request']))
                                        @if(checkPermissions(['edit_finance_request']))
                                            <a  href="{{route('admin.financial-requests.edit', $request->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                        @endif
                                        @if(checkPermissions(['delete_finance_request']))
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.financial-requests.destroy', $request->id)}}" title="{{trans('admin.delete')}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['financial_requests']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush