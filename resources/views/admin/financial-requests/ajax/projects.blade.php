<select class="form-control select2" id="project_id" name="project_id" required>
    @foreach ($data['projects'] as $project)
        <option value="{{$project->id}}">{{$project->name}}</option>
    @endforeach
</select>