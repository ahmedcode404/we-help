<!DOCTYPE html>
<html>

<head>

    @if(\App::getLocale() == 'ar')
    <html dir="rtl" class="language" lang="ar">
    @else
    <html dir="ltr" class="language" lang="en">
    @endif

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{trans('admin.show')}}</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

</head>

<body>

    <style>
        * {
            box-sizing: border-box;
            -webkit-box-sizing: border-box;
        }


        body {
            margin: 0;
            padding: 0;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            color: #3DBECB;
        }

        html[dir="rtl"] body {
            direction: rtl;

        }

        .table-1 {
            direction: rtl;
        }

        html[dir="ltr"] .table-1 td {
            direction: ltr;
        }

        .text-center {
            text-align: center;
        }

        table {
            width: 100%;
            text-align: center;
        }

        .gray_bg,
        th {
            background: #CBF5FF;
            color: #000;

        }

        table,
        th,
        td {
            border: 1px solid #888;
            border-collapse: collapse;
            font-weight: normal;

        }

        td,
        th {
            padding: 10px;
        }

        .container {
            width: 95%;
            margin: auto;
        }

        td span {
            display: block;
            font-weight: bold;
            font-size: 15px;
            margin-top: 15px;
        }

        .no-border-table,
        .no-border-table td {
            border: none;
        }

        .table-1 {
            border-bottom: 1px solid #eee;
            font-size: 18px;
            line-height: 30px;
            font-weight: bold;
            text-align: right;
        }

        .table-1 td {
            vertical-align: bottom;
            padding: 10px 0
        }

        .table-1 td i {
            font-style: normal;
            display: inline-block;
            direction: ltr;
        }

        h2 {
            text-align: center;
        }


        img {
            display: inline;

        }

        html[dir="rtl"] .table-responsive {
            direction: ltr;

        }

        .table-responsive {
            overflow-y: hidden;
            overflow-x: auto;
            white-space: nowrap;
        }

        html[dir="rtl"] .table-responsive table {
            direction: rtl;
        }

        @media only screen and (max-width:576px) {

            .table-1,
            .table-1 tr,
            .table-1 td,
            .table-1 tbody {
                display: block;
                text-align: center !important;
            }

            .table-1 tr td:first-of-type {
                border-bottom: 1px solid #eee;
            }

            .table-1 {
                border-width: 4px;
            }
        }
    </style>



    <div class="app-content content">



        <div class="container">
            <table class="no-border-table table-1">
                <tr>
                    <td>
                        @php
                        $nation_id = getNationId();
                        @endphp
                        <img src="{{asset('storage/'.getSettingMsg('we_help_logo', $nation_id)->value)}}" height="55"
                            alt="logo">
                        <br>
                        {{getSettingMsg('org_name', $nation_id)->value}}
                        <br>
                        {{getSettingValue('address', $nation_id)->value}}
                        <br>
                        <div dir="ltr">{{getSettingValue('phone', $nation_id)->value}}</div>
                        {{getSettingValue('email', $nation_id)->value}}
                        <br>

                        {{__('web.date')}}: {{$voucher->created_at->format('Y-m-d')}} - {{__('web.time')}}:
                        {{$voucher->created_at->format('H:i')}}


                    </td>
                    <td style="text-align: left;">
                        @php
                            if($voucher->requestable_type == 'App\Models\User' && $voucher->requestable->image != null){
                                $image = asset('storage/'.$voucher->requestable->image);
                            }
                            else if($voucher->requestable_type == 'App\Models\Charity' && $voucher->requestable->logo != null){
                                $image = asset('storage/'.$voucher->requestable->logo);
                            }
                            else{
                                $image = asset('web/images/main/user_avatar.png');
                            }
                        @endphp
                        <img src="{{$image}}" height="55" alt="logo">
                        <br>
                        @if($voucher->type == 'catch')
                            {{__('admin.supporter_name')}}: {{$voucher->requestable->name}}
                        @else
                            {{__('web.charity_name')}}: {{$voucher->requestable->name}}
                        @endif
                        <br>
                        {{__('web.address')}} : {{$voucher->requestable->address}}
                        <br>
                        {{__('web.phone')}} : <i>{{$voucher->requestable->phone}} </i>
                        <br>
                        {{__('web.mobile')}} : <i>{{$voucher->requestable->mobile}} </i>
                        <br>
                        {{__('web.email')}} : {{$voucher->requestable->email}}

                    </td>

                </tr>

            </table>

            <h2>{{__('admin.support_voucher')}}</h2>

            <div class="table-responsive">
                <table>
                    <thead>
                        <tr>
                            @if($voucher->type == 'catch')
                                <th>{{trans('admin.supporter_name')}}</th>
                            @else
                                <th>{{trans('admin.charity_name')}}</th>
                            @endif
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.request_type')}}</th>
                            <th>{{trans('admin.project_cost')}}</th>
                            @if($voucher->type == 'exchange')
                            <th>{{trans('admin.total_out_to_charity')}}</th>
                            @endif
                            <th>{{$voucher->type == 'exchange' ? trans('admin.out_to_charity') :
                                trans('admin.voucher_cost')}}</th>
                            @if($voucher->type == 'exchange' && $voucher->bank_account_num && $voucher->voucher_num)
                            <th>{{trans('admin.bank_account')}}</th>
                            <th>{{trans('admin.voucher_num')}}</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$voucher->requestable ? $voucher->requestable->name : ''}}</td>
                            <td>{{$voucher->project ? $voucher->project->name : ''}}</td>
                            <td>{{trans('admin.'.$voucher->type)}}</td>
                            <td>{{$voucher->project ? exchange($voucher->project->get_total,
                                $voucher->project->currency_id, session('currency')).'
                                '.currencySymbol(session('currency')) : '0.0'}}</td>
                            @if($voucher->type == 'exchange')
                            <td>

                                {{-- فى حالة سند الصرف بيتم عرض اجمالى سندات الصرف --}}
                                @if($voucher->bank_account_num && $voucher->voucher_num)
                                @if($voucher->type == 'exchange' && $voucher->requestable &&
                                $voucher->requestable->getTotalCharityExchangeVouchers())
                                {{$voucher->requestable->getTotalCharityExchangeVouchers().'
                                '.currencySymbol(session('currency'))}}
                                @elseif($voucher->project && $voucher->project->charity)
                                {{$voucher->project->charity->getTotalCharityExchangeVouchers().'
                                '.currencySymbol(session('currency'))}}
                                @endif
                                @else
                                {{-- فى حالة طلبات الصرف بيتم عرض اجمالى طلبات الصرف --}}
                                @if($voucher->type == 'exchange' && $voucher->requestable &&
                                $voucher->requestable->getTotalOutToCharity())
                                {{$voucher->requestable->getTotalOutToCharity().'
                                '.currencySymbol(session('currency'))}}
                                @elseif($voucher->project && $voucher->project->charity)
                                {{$voucher->project->charity->getTotalOutToCharity().'
                                '.currencySymbol(session('currency'))}}
                                @endif
                                @endif

                            </td>
                            @endif
                            {{-- <td>{{exchange($voucher->out_to_charity, $voucher->project->currency_id,
                                session('currency')).' '.currencySymbol(session('currency'))}}</td> --}}
                            <td>{{$voucher->out_to_charity.' '.currencySymbol($voucher->currency_id)}}</td>
                            @if($voucher->type == 'exchange' && $voucher->bank_account_num && $voucher->voucher_num)
                            <td>{{$voucher->bank_account_num}}</td>
                            <td>{{$voucher->voucher_num}}</td>
                            @endif
                        </tr>
                    </tbody>
                </table>
            </div>

            <h2> {{__('admin.project_phases')}} </h2>
            <div class="table-responsive">
                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('admin.name')}}</th>
                            <th>{{currencySymbol(session('currency'))}} {{trans('admin.cost')}}</th>
                            <th class="text-truncate">{{trans('admin.start_date')}}</th>
                            <th>{{trans('admin.end_date')}}</th>
                            <th>{{trans('admin.approved')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $session_currency = currencySymbol(session('currency'));
                        @endphp
                        @forelse ($voucher->project->phases as $phase)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$phase->name}}</td>
                            <td>{{generalExchange($phase->cost, $phase->project->currency->symbol, $session_currency)}}
                                {{$session_currency}}</td>
                            <td>{{$phase->start_date}}</td>
                            <td>{{$phase->end_date}}</td>
                            <td>{{$phase->approved ? trans('admin.approved') : trans('admin.not_approved')}}</td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="6">{{trans('admin.no_phases_registered')}}</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>

</html>