@extends('layout.app')

@section('title', trans('admin.edit-static-pages'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit-static-pages')}}: {{$data['page']->title}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="static_pages" method="POST" class="request_form" action="{{route('admin.static-pages.update', $data['page']->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PATCH')

                            <input type="hidden" id="edit" value="true">

                            
                            {{-- main page start --}}

                            @if($data['page']->key == 'about_us' || $data['page']->key == 'about_corporation' || $data['page']->key == 'vision_and_mission' || $data['page']->key == 'our_goal')
                                <div class="form-group">
                                    <label class="form-label" for="image">{{trans('admin.image')}}</label>
                                    <input type="file" class="form-control" id="image" name="image" placeholder="{{trans('admin.image')}}" accept="image/png, image/jpeg, image/jpg" />
                                    <small>(Max size: 20MB)</small>
                                    <span class="error error-image"></span>
                                    @if($errors->has('image'))
                                        <span class="error error-image">{{ $errors->first('image') }}</span>
                                    @endif
                                    {{-- {{ dd($data['page']->image) }} --}}
                                    <img src="{{asset('storage/'.$data['page']->image)}}"  height="200" alt="{{$data['page']->title}}">
                                </div>
                            @endif
                            
                            @if($data['page']->title_ar)
                                <div class="form-group">
                                    <label class="form-label" for="title_ar">{{trans('admin.page_title_ar')}}</label>
                                    <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{$data['page']->title_ar == null ? old('title_ar') : $data['page']->title_ar}}" placeholder="{{trans('admin.page_title_ar')}}" required />
                                    <span class="error error-title_ar"></span>
                                    @if($errors->has('title_ar'))
                                        <span class="error error-title_ar">{{ $errors->first('title_ar') }}</span>
                                    @endif
                                </div>
                            @endif

                            @if($data['page']->title_en)
                                <div class="form-group">
                                    <label class="form-label" for="title_en">{{trans('admin.page_title_en')}}</label>
                                    <input type="text" id="title_en" name="title_en" class="form-control" value="{{$data['page']->title_en == null ? old('title_en') : $data['page']->title_en}}" placeholder="{{trans('admin.page_title_en')}}" required />
                                    <span class="error error-title_en"></span>
                                    @if($errors->has('title_en'))
                                        <span class="error error-title_en">{{ $errors->first('title_en') }}</span>
                                    @endif
                                </div>
                            @endif

                            @if($data['page']->content_ar)
                                <div class="form-group">
                                    <label class="form-label" for="content_ar">{{trans('admin.page_content_ar')}}</label>
                                    <textarea id="content_ar" name="content_ar" class="form-control ckeditor" placeholder="{{trans('admin.page_content_ar')}}" required >{{$data['page']->content_ar == null ? old('content_ar') : $data['page']->content_ar}}</textarea>
                                    <span class="error error-content_ar"></span>
                                    @if($errors->has('content_ar'))
                                        <span class="error error-content_ar">{{ $errors->first('content_ar') }}</span>
                                    @endif
                                </div>
                            @endif

                            @if($data['page']->content_en)
                                <div class="form-group">
                                    <label class="form-label" for="content_en">{{trans('admin.page_content_en')}}</label>
                                    <textarea id="content_en" name="content_en" class="form-control ckeditor" placeholder="{{trans('admin.page_content_en')}}" required >{{$data['page']->content_en == null ? old('content_en') : $data['page']->content_en}}</textarea>
                                    <span class="error error-content_en"></span>
                                    @if($errors->has('content_en'))
                                        <span class="error error-content_en">{{ $errors->first('content_en') }}</span>
                                    @endif
                                </div>
                            @endif

                            {{-- main page end --}}

                            <br><br>
                            {{-- chidlren pages start --}}

                            @foreach ($data['page']->children as $child)

                                <input type="hidden" name="child_ids[]" value="{{ $child->id }}">

                                <br><br>
                                <hr>
                                <h3 class="card-title">{{trans('admin.edit')}}: {{$child->title}}</h3>

                                @if($data['page']->key == 'our_value')
                                    <div class="form-group">
                                        <label class="form-label" for="image{{$child->id}}">{{trans('admin.image')}}</label>
                                        <input type="file" class="form-control" id="image{{$child->id}}" name="image-{{$child->id}}" placeholder="{{trans('admin.image')}}" accept="image/png, image/jpeg, image/jpg" />
                                        <small>(Max size: 20MB)</small>
                                        <img src="{{asset('storage/'.$child->image)}}" style="max-height:400px" alt="{{$data['page']->title}}">
                                        <span class="error error-image-"></span>
                                        @if($errors->has('image-'.$child->id))
                                            <span class="error error-image-{{ $child->id }}">{{ $errors->first('image-'.$child->id) }}</span>
                                        @endif
                                    </div>
                                @endif
                                @if($child->title_ar)
                                    <div class="form-group">
                                        <label class="form-label" for="title_ar{{$child->id}}">{{trans('admin.title_ar')}}</label>
                                        <input type="text" class="form-control" id="title_ar{{$child->id}}" readonly name="title_ar_{{$child->id}}" value="{{$child->title_ar}}" placeholder="{{trans('admin.title_ar')}}" required />
                                        <span class="error error-title_ar_"></span>
                                        @if($errors->has('title_ar_'.$child->id))
                                            <span class="error error-title_ar_{{ $child->id }}">{{ $errors->first('title_ar_'.$child->id) }}</span>
                                        @endif
                                    </div>
                                @endif

                                @if($child->title_en)
                                    <div class="form-group">
                                        <label class="form-label" for="title_en{{$child->id}}">{{trans('admin.title_en')}}</label>
                                        <input type="text" id="title_en{{$child->id}}" name="title_en_{{$child->id}}" readonly class="form-control" value="{{$child->title_en == null ? old('child_title_en') : $child->title_en}}" placeholder="{{trans('admin.title_en')}}" required />
                                        <span class="error error-title_en_"></span>
                                        @if($errors->has('title_en_'.$child->id))
                                            <span class="error error-title_en_{{ $child->id }}">{{ $errors->first('title_en_'.$child->id) }}</span>
                                        @endif
                                    </div>
                                @endif

                                @if($child->key == 'our_services')
                                    <div class="form-group">
                                        <label class="form-label" for="link{{$child->id}}">{{trans('admin.link')}}</label>
                                        <input type="text" class="form-control" id="link{{$child->id}}" name="link-{{$child->id}}" value="{{$child->link}}" placeholder="{{trans('admin.link')}}" required />
                                        <span class="error error-link-"></span>
                                        @if($errors->has('link-'.$child->id))
                                            <span class="error error-link-{{ $child->id }}">{{ $errors->first('link-'.$child->id) }}</span>
                                        @endif
                                    </div>
                                @endif

                                <div class="content-test">
                                    @if($child->content_ar)
                                    <div class="form-group">
                                        <label class="form-label" for="content_ar{{$child->id}}">{{trans('admin.content_ar')}}</label>
                                        <textarea id="content_ar{{$child->id}}" name="content_ar_{{$child->id}}" class="form-control ckeditor" placeholder="{{trans('admin.content_ar')}}" required >{{$child->content_ar == null ? old('child_content_ar') : $child->content_ar}}</textarea>
                                        <span class="error error-content_ar_"></span>
                                        @if($errors->has('content_ar_'.$child->id))
                                            <span class="error error-content_ar_{{ $child->id }}">{{ $errors->first('content_ar_'.$child->id) }}</span>
                                        @endif
                                    </div>
                                @endif

                                @if($child->content_en)
                                    <div class="form-group">
                                        <label class="form-label" for="content_en_{{$child->id}}">{{trans('admin.content_en')}}</label>
                                        <textarea id="content_en_{{$child->id}}" name="content_en_{{$child->id}}" class="form-control ckeditor" placeholder="{{trans('admin.child_content_en')}}" required >{{$child->content_en == null ? old('child_content_en') : $child->content_en}}</textarea>
                                        <span class="error error-content_en_"></span>
                                        @if($errors->has('content_en_'.$child->id))
                                            <span class="error error-content_en_{{ $child->id }}">{{ $errors->first('content_en_'.$child->id) }}</span>
                                        @endif
                                    </div>
                                @endif
                                </div>
                                
                            @endforeach
                            {{-- chidlren pages end --}}

                            <div class="row">
                                <div class="col-12" id="loading">
                                    <button type="submit" class="btn btn-primary submit_request_form_button" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <script src="{{asset('dashboard/forms/static_pages/edit.js')}}"></script>

    @endpush

@endsection