@extends('layout.app')

@section('title', trans('admin.static_pages'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.page_title')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['pages'] as $page)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$page->title}}</td>
                                <td>
                                    @if(checkPermissions(['edit_static_pages']))
                                                {{-- @if(checkPermissions(['edit_static_pages'])) --}}
                                                    <a  href="{{route('admin.static-pages.edit', $page->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                {{-- @endif --}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['pages']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection