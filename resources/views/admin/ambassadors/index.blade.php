@extends('layout.app')

@section('title', trans('admin.ambassadors'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.ambassador_name')}}</th>
                            <th>{{trans('admin.campaign_name')}}</th>
                            <th>{{trans('admin.campaing_link')}}</th>
                            <th>{{trans('admin.seen')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['ambassadors'] as $amb)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$amb->ambassador_name}}</td>
                                <td>{{$amb->campaign_name}}</td>
                                <td><a href="{{route('web.campaign', $amb->affiliate)}}">{{trans('admin.link')}}</a></td>
                                <td>{{$amb->seen}}</td>

                                <!-- Modal -->  
                                <div class="modal fade modal-info text-left" id="amb-{{$amb->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                    {{-- <div class="spinner-grow text-success mr-1 pross processing-div-{{ $amb->id }}" style="position: absolute;margin: 26px -30px 37px 242px;z-index: 100;display: none;" role="status">
                                        <span class="sr-only">Loading...</span>
                                    </div>  --}}
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.details')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            
                                            <div class="mb-1"><strong>{{trans('admin.ambassador_name')}}</strong>: {{$amb->ambassador_name}}</div>
                                            <div class="mb-1"> <strong>{{trans('admin.campaign_name')}}</strong>: {{$amb->campaign_name}} </div>
                                            <div class="mb-1"><strong>{{trans('admin.link')}}</strong>: <a href="{{route('web.campaign', $amb->affiliate)}}">{{trans('admin.link')}}</a> </div>
                                            <div class="mb-1"><strong>{{trans('admin.seen')}}</strong>: {{$amb->seen}} </div>
                                            <div class="mb-1"><strong>{{trans('admin.project')}}</strong>: {{$amb->project->name}} </div>
                                            <div class="mb-1"><strong>{{trans('admin.campaign_goal')}}</strong>: {{$amb->campaign->name}} </div>
                                            <div class="mb-1"><strong>{{trans('admin.user')}}</strong>: {{$amb->user->name}}</div>

                                            <br>
                                                                                         
                                        </div>
                                    </div>
                                </div>    
                                
                                <td>
                                    @if(checkPermissions(['list_ambassadors']))
                                      
                                                {{-- @if(checkPermissions(['list_ambassadors'])) --}}
                                                    <a  data-toggle="modal" data-target="#amb-{{$amb->id}}" title="{{trans('admin.show')}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                    </a>
                                                {{-- @endif --}}
                                           
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['ambassadors']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/supporters/index.js')}}"></script>
@endpush