@extends('layout.app')

@section('title', trans('admin.create-financial-bill'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.create-financial-bill')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="financial-request" class="request_form" method="POST" action="{{route('admin.financial-bills.store')}}">
                            @csrf

                            <div class="form-group">
                                <label for="request_id">{{trans('admin.financial_request')}}</label>
                                <select class="form-control select2" id="request_id" name="request_id"
                                    data-action="{{ route('admin.get-charity-iban') }}"
                                    data-method="POST"
                                    required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['requests'] as $request)
                                        <option value="{{$request->id}}"
                                            data-charity_id="{{ $request->requestable_id }}"
                                            @if(old('request_id') == $request->id) selected @endif>
                                            {{$request->requestable->name.' - '.exchange($request->out_to_charity, $request->currency_id, session('currency'))}} {{currencySymbol(session('currency'))}}</option>
                                    @endforeach
                                </select>
                                <span class="error error-request_id"></span>
                            </div>

                            <div class="form-group">
                                <label for="bank_account_num">{{trans('admin.bank_account')}}</label>
                                <input type="text" name="bank_account_num" id="bank_account_num" placeholder="{{trans('admin.bank_account')}}" readonly class="form-control" value="{{old('bank_account_num')}}">
                            </div>

                            <div class="form-group">
                                <label for="voucher_num">{{trans('admin.voucher_num')}}</label>
                                <input type="number" step=".01" name="voucher_num" id="voucher_num" placeholder="523140236" class="form-control" value="{{old('voucher_num')}}" required>
                                <span class="error error-voucher_num"></span>
                            </div>

                            
                            <div class="form-group">
                                <label for="notes">{{trans('admin.notes')}}</label>
                                <textarea name="notes" id="notes" cols="30" rows="6" class="form-control">{{old('notes')}}</textarea>
                                <span class="error error-notes"></span>
                            </div>

                            <div class="row">
                                <div class="col-12" id="loading">
                                    <button type="button" class="btn btn-primary submit_request_form_button">{{trans('admin.save')}}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/financial-bills/create.js')}}"></script>
    @endpush
@endsection