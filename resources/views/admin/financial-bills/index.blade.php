@extends('layout.app')

@section('title', trans('admin.financial_bills'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_finance_voucher']))
                    <a href="{{route('admin.financial-bills.create')}}" class="btn btn-primary">{{trans('admin.create-financial-bill')}}</a>
                @endif
                <br>
                <div class="row taps_eidt  justify-content-center">
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-bills.index')}}" class="btn btn-primary">{{trans('admin.all')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-bills-status', 'waiting')}}" class="btn btn-warning">{{trans('admin.waiting')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-bills-status', 'approved')}}" class="btn btn-success">{{trans('admin.approved')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-bills-status', 'hold')}}" class="btn btn-info">{{trans('admin.hold')}}</a>
                    </div>
                    <div class="col-md-2 col-6 mb-1">
                        <a href="{{route('admin.financial-bills-status', 'rejected')}}" class="btn btn-danger">{{trans('admin.rejected')}}</a>
                    </div>
                </div>
                <br>
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.charity_name')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.total_bills')}}</th>
                            <th>{{trans('admin.voucher_cost')}}</th>
                            <th>{{trans('admin.status')}}</th>
                            <th>{{trans('admin.recieve_status')}}</th>
                            {{-- <th>{{trans('admin.bank_account')}}</th>
                            <th>{{trans('admin.voucher_num')}}</th> --}}
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['bills'] as $bill)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$bill->requestable ? $bill->requestable->name : ''}}</td>
                                <td>{{$bill->project ? $bill->project->name : ''}}</td>
                                <td>
                                    @if($bill->type == 'exchange' && $bill->requestable && $bill->requestable->getTotalCharityExchangeVouchers())
                                        {{$bill->requestable->getTotalCharityExchangeVouchers().' '.currencySymbol(session('currency'))}}
                                    @elseif($bill->project && $bill->project->charity)
                                        {{$bill->project->charity->getTotalCharityExchangeVouchers().' '.currencySymbol(session('currency'))}}
                                    @endif
                                </td>
                                <td>{{exchange($bill->out_to_charity, $bill->project->currency_id, session('currency'))}} {{currencySymbol(session('currency'))}}</td>
                                <td>
                                    @if($bill->status == 'approved')
                                        <span id="current_status-{{$bill->id}}" class="badge badge-light-success">{{trans('admin.'.$bill->status)}}</span>
                                    @elseif($bill->status == 'waiting')
                                        <span id="current_status-{{$bill->id}}" class="badge badge-light-warning">{{trans('admin.'.$bill->status)}}</span>
                                    @elseif($bill->status == 'hold')
                                        <span id="current_status-{{$bill->id}}" class="badge badge-light-info">{{trans('admin.'.$bill->status)}}</span>
                                    @elseif($bill->status == 'rejected')
                                        <span id="current_status-{{$bill->id}}" class="badge badge-light-danger">{{trans('admin.'.$bill->status)}}</span>
                                    @endif
                                    {{-- <span id="current_status-{{$bill->id}}">{{trans('admin.'.$bill->status)}}</span> --}}
                                    {{-- @if($bill->status != 'approved')
                                        @if($bill->type == 'exchange')
                                            <a class="btn btn-primary" data-toggle="modal" data-target="#financial_request-{{$bill->id}}">{{trans('admin.change')}}</a>
                                            <!-- Modal -->  
                                            <div class="modal fade modal-info text-left" id="financial_request-{{$bill->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="spinner-grow text-success mr-1 pross processing-div-{{ $bill->id }}" style="position: absolute;margin: 26px -30px 37px 242px;z-index: 100;display: none;" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div> 
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="change_request_status-{{$bill->id}}">{{trans('admin.select_status')}}</label>
                                                            <select id="change_request_status-{{$bill->id}}" class="form-control">
                                                                <option value="approved" @if($bill->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                <option value="rejected" @if($bill->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                <option value="hold" @if($bill->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="request_notes-{{$bill->id}}">{{trans('admin.notes')}}</label>
                                                            <textarea id="request_notes-{{$bill->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$bill->notes}}</textarea>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <button type="button" id="" class="btn btn-info save_status" data-model="financial_request" data-id="{{$bill->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                        @endif
                                    @endif --}}
                                </td>
                                <td>
                                    <select name="recieve_status" id="recieve_status" class="form-control"
                                        data-action="{{ route('admin.financial-bills.change-revceive-status') }}"
                                        data-method="POST"
                                        data-id="{{ $bill->id }}">
                                        <option value="deserved" @if($bill->recieve_status =='deserved') selected @endif>{{ trans('admin.deserved') }}</option>
                                        <option value="paid" @if($bill->recieve_status =='paid') selected @endif>{{ trans('admin.paid') }}</option>
                                    </select>
                                </td>
                                {{-- <td>{{$bill->bank_account_num}}</td>
                                <td>{{$bill->voucher_num}}</td> --}}
                                
                                <td>
                                    @if(checkPermissions(['show_finance_voucher']))
                                    <a href="{{route('get-voucher', $bill->slug)}}" title="{{trans('admin.show')}}">
                                        <i data-feather="eye" class="mr-50"></i>
                                    </a>
                                @else
                                    {{trans('admin.do_not_have_permission')}}
                                @endif

                                    @if(checkPermissions(['edit_finance_voucher']) || checkPermissions(['delete_finance_voucher']))
                                        @if(checkPermissions(['edit_finance_voucher']))
                                            <a href="{{route('admin.financial-bills.edit', $bill->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                        @endif
                                        @if(checkPermissions(['delete_finance_voucher']))
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.financial-bills.destroy', $bill->id)}}" title="{{trans('admin.delete')}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['bills']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
    <script src="{{asset('dashboard/forms/financial-bills/create.js')}}"></script>
@endpush