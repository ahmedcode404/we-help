@extends('layout.app')

@section('title', trans('admin.financial_bills'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_finance_voucher']))
                    <a href="{{route('admin.financial-bills.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif
                <br>
                <div class="row taps_eidt  justify-content-center">
                    @php
                        if(request()->segment(count(request()->segments())) == 'exchange'){
                            $type = 'exchange';
                        }
                        else if(request()->segment(count(request()->segments())) == 'catch'){
                            $type = 'catch';
                        }
                        else{
                            $type = 'all';
                        }
                    @endphp
                    <div class="col-md-2">
                        <a href="{{route('admin.bill-req.get-req-with-status', ['status' => 'all', 'type' => $type])}}" >{{trans('admin.all')}}</a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('admin.bill-req.get-req-with-status', ['status' => 'waiting', 'type' => $type])}}" >{{trans('admin.waiting')}}</a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('admin.bill-req.get-req-with-status', ['status' => 'approved', 'type' => $type])}}" >{{trans('admin.approved')}}</a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('admin.bill-req.get-req-with-status', ['status' => 'hold', 'type' => $type])}}">{{trans('admin.hold')}}</a>
                    </div>
                    <div class="col-md-2">
                        <a href="{{route('admin.bill-req.get-req-with-status', ['status' => 'rejected', 'type' => $type])}}" >{{trans('admin.rejected')}}</a>
                    </div>
                </div>
                <br>
                <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.charity')}}</th>
                            <th>{{trans('admin.project')}}</th>
                            <th>{{trans('admin.type')}}</th>
                            <th>{{trans('admin.project_cost')}}</th>
                            <th>{{trans('admin.total_bills')}}</th>
                            <th>{{trans('admin.voucher_cost')}}</th>
                            <th>{{trans('admin.status')}}</th>
                            <th>{{trans('admin.recieve_status')}}</th>
                            <th>{{trans('admin.bank_account')}}</th>
                            <th>{{trans('admin.voucher_num')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['bills'] as $bill)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$bill->requestable ? $bill->requestable->name : ''}}</td>
                                <td>{{$bill->project ? $bill->project->name : ''}}</td>
                                <td>{{trans('admin.'.$bill->type)}}</td>
                                <td>{{$bill->project ? exchange($bill->project->cost, $bill->project->currency_id, session('currency')).' '.currencySymbol(session('currency')) : 0.0}}</td>
                                <td>
                                    @if($bill->type == 'exchange' && $bill->requestable && $bill->requestable->getTotalCharityExchangeVouchers())
                                        {{$bill->requestable->getTotalCharityExchangeVouchers().' '.currencySymbol(session('currency'))}}
                                    @elseif($bill->project && $bill->project->charity)
                                        {{$bill->project->charity->getTotalCharityExchangeVouchers().' '.currencySymbol(session('currency'))}}
                                    @endif
                                </td>
                                <td>{{exchange($bill->out_to_charity, $bill->project->currency_id, session('currency'))}} {{currencySymbol(session('currency'))}}</td>
                                <td>
                                    <span id="current_status-{{$bill->id}}">{{trans('admin.'.$bill->status)}}</span>
                                    @if($bill->status != 'approved')
                                        @if($bill->type == 'exchange')
                                            <a class="btn btn-primary" data-toggle="modal" data-target="#financial_request-{{$bill->id}}">{{trans('admin.change')}}</a>
                                            <!-- Modal -->  
                                            <div class="modal fade modal-info text-left" id="financial_request-{{$bill->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="spinner-grow text-success mr-1 pross processing-div-{{ $bill->id }}" style="position: absolute;margin: 26px -30px 37px 242px;z-index: 100;display: none;" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div> 
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="change_request_status-{{$bill->id}}">{{trans('admin.select_status')}}</label>
                                                            <select id="change_request_status-{{$bill->id}}" class="form-control">
                                                                <option value="waiting" @if($bill->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                <option value="approved" @if($bill->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                <option value="rejected" @if($bill->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                <option value="hold" @if($bill->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                            </select>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="request_notes-{{$bill->id}}">{{trans('admin.notes')}}</label>
                                                            <textarea id="request_notes-{{$bill->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$bill->notes}}</textarea>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <button type="button" id="" class="btn btn-info save_status" data-model="financial_request" data-id="{{$bill->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                                    </div>
                                                </div>
                                            </div>                                                            
                                        @endif
                                    @endif
                                </td>
                                <td>{{$bill->recieve == 1 ? trans('admin.recieved') : trans('admin.not_recieved')}}</td>
                                <td>{{$bill->bank_account_num}}</td>
                                <td>{{$bill->voucher_num}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i data-feather="more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            @if(checkPermissions(['show_finance_voucher']))
                                                <a class="dropdown-item" href="{{route('get-voucher', $bill->slug)}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                    <span>{{trans('admin.show')}}</span>
                                                </a>
                                            @endif
                                            @if(checkPermissions(['edit_finance_voucher']))
                                                <a class="dropdown-item" target="_blank" href="{{route('admin.financial-bills.edit', $bill->id)}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                    <span>{{trans('admin.edit')}}</span>
                                                </a>
                                            @endif
                                            @if(checkPermissions(['delete_finance_voucher']))
                                                <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.financial-bills.destroy', $bill->id)}}">
                                                    <i data-feather="trash" class="mr-50"></i>
                                                    <span>{{trans('admin.delete')}}</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{$data['bills']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush