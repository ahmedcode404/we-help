@extends('layout.app')

@section('title', trans('admin.edit-financial-bill'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit-financial-bill')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="financial-request" method="POST" class="request_form" action="{{route('admin.financial-bills.update', $data['bill']->id)}}">
                            @csrf
                            @method('PUT')

                            <input type="hidden" id="edit" value="true">

                            <div class="form-group">
                                <label for="request_id">{{trans('admin.financial_request')}}</label>
                                <select class="form-control select2" id="request_id" name="request_id" required>
                                    <option value="{{$data['bill']->id}}" selected>
                                        {{$data['bill']->requestable->name.' - '.exchange($data['bill']->out_to_charity, $data['bill']->currency_id, session('currency'))}} {{currencySymbol(session('currency'))}}
                                    </option>
                                </select>
                                <span class="error error-request_id"></span>
                            </div>

                            <div class="form-group">
                                <label for="bank_account_num">{{trans('admin.bank_account')}}</label>
                                <input type="text" name="bank_account_num" id="bank_account_num" placeholder="5231402361425001" class="form-control" value="{{$data['bill']->bank_account_num ? $data['bill']->bank_account_num : old('bank_account_num')}}" readonly required>                            </div>

                            <div class="form-group">
                                <label for="voucher_num">{{trans('admin.voucher_num')}}</label>
                                <input type="number" step=".01" name="voucher_num" id="voucher_num" placeholder="523140236" class="form-control" value="{{$data['bill']->voucher_num ? $data['bill']->voucher_num : old('voucher_num')}}" required>
                                <span class="error error-voucher_num"></span>
                            </div>

                            @if(checkPermissions(['accept_refuse_finance_requests']))
                                <div class="form-group">
                                    <label for="status">{{trans('admin.status')}}</label>
                                    <select id="status" class="form-control" name="status">
                                        <option value="waiting" @if($data['bill']->status == 'waiting' || old('status') == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                        <option value="approved" @if($data['bill']->status == 'approved' || old('status') == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                        <option value="rejected" @if($data['bill']->status == 'rejected' || old('status') == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                        <option value="hold" @if($data['bill']->status == 'hold' || old('status') == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                    </select>
                                    <span class="error error-status"></span>
                                </div>
                            @endif

                            <div class="form-group">
                                <label for="notes">{{trans('admin.notes')}}</label>
                                <textarea name="notes" id="notes" cols="30" rows="6" class="form-control">{{$data['bill']->notes ? $data['bill']->notes  : old('notes')}}</textarea>
                                <span class="error error-notes"></span>
                            </div>

                            <div class="row">
                                <div class="col-12" id="loading">
                                    <button type="button" class="btn btn-primary submit_request_form_button">{{trans('admin.save')}}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/financial-bills/create.js')}}"></script>
    @endpush
@endsection