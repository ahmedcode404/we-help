@extends('layout.app')

@section('title', trans('admin.update_agreement'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="create_agreement" method="POST" action="{{route('admin.agreements.store')}}">
                            @csrf
                            
                            @foreach($agreements as $agreement)

                                {{-- @if($agreement->key == 'supporter_payment_rules_sa_ar')
                                    <h2 style="margin-bottom: 65px;margin-top: 65px;">{{ __('admin.SA') }}</h2>
                                @endif
                                @if($agreement->key == 'supporter_payment_rules_uk_ar')
                                    <hr>
                                    <h2 style="margin-bottom: 65px;margin-top: 65px;" >{{ __('admin.UK') }}</h2>
                                @endif                                  --}}

                                <div class="form-group">
                                    <label class="form-label" for="{{ $agreement->key }}">{{trans('admin.' . $agreement->key)}}</label>
                                    <textarea id="{{ $agreement->key }}" name="{{ $agreement->key }}" rows="10" class="form-control" required >{{ $agreement->content }}</textarea>
                                    <span class="error-input"></span>
                                </div>
   
                            @endforeach


                            <div class="row">
                                <div class="col-12">
                                    <button type="button" id="click_create_agreement" url="{{route('admin.agreements.store')}}" class="btn btn-primary" name="submit" value="Submit"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/agreements/update.js')}}"></script>
        <script src="{{ url('custom/custom-validate.js') }}"></script>


        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <script>

            // ckeditor custom

            CKEDITOR.replace( 'supporter_payment_rules_sa_ar' );
            CKEDITOR.replace( 'supporter_payment_rules_sa_en' );

            CKEDITOR.replace( 'supporter_payment_rules_uk_ar' );
            CKEDITOR.replace( 'supporter_payment_rules_uk_en' );            

            CKEDITOR.replace( 'charity_register_rules_sa_ar' );
            CKEDITOR.replace( 'charity_register_rules_sa_en' );

            CKEDITOR.replace( 'charity_register_rules_uk_ar' );
            CKEDITOR.replace( 'charity_register_rules_uk_en' );
  

            
        </script>

    @endpush
@endsection