@extends('layout.app')

@section('title', trans('admin.edit-charity-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')

<link rel="stylesheet" href="{{asset('node_modules/@furcan/iconpicker/dist/fontawesome-5.11.2/css/all.min.css')}}" />

<link rel="stylesheet" href="{{asset('node_modules/@furcan/iconpicker/dist/iconpicker-1.5.0.css')}}" />

@endpush

@section('content')

<!-- Basic Horizontal form layout section start -->
<section id="basic-horizontal-layouts">
    <div class="row justify-content-md-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{trans('admin.edit-charity-categories')}}</h4>
                </div>
                <div class="card-body">
                    <form id="charity-categories" method="POST" class="request_form"
                        action="{{route('admin.charity-categories.update', $data['cat']->id)}}"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <input type="hidden" id="edit" value="true">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="name_ar">{{trans('admin.ch_category_name_ar')}}</label>
                                    <input type="text" class="form-control" id="name_ar" name="name_ar"
                                        value="{{$data['cat']->name_ar == null ? old('name_ar') : $data['cat']->name_ar}}"
                                        placeholder="{{trans('admin.name_ar')}}" required />
                                    <span class="error error-name_ar"></span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="name_en">{{trans('admin.ch_category_name_en')}}</label>
                                    <input type="text" id="name_en" name="name_en" class="form-control"
                                        value="{{$data['cat']->name_en == null ? old('name_en') : $data['cat']->name_en}}"
                                        placeholder="{{trans('admin.name_en')}}" required />
                                    <span class="error error-name_en"></span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="type">{{trans('admin.ch_category_type')}}</label>
                                    <select name="type" id="type" class="form-control">
                                        <option value="">{{trans('admin.select')}}</option>
                                        <option value="engineering" @if($data['cat']->type == 'engineering') selected
                                            @endif>{{trans('admin.engineering')}}</option>
                                        <option value="educational" @if($data['cat']->type == 'educational') selected
                                            @endif>{{trans('admin.educational')}}</option>
                                        <option value="sponsorship" @if($data['cat']->type == 'sponsorship') selected
                                            @endif>{{trans('admin.sponsorship')}}</option>
                                        <option value="emergency" @if($data['cat']->type == 'emergency') selected
                                            @endif>{{trans('admin.emergency')}}</option>
                                        <option value="sustainable" @if($data['cat']->type == 'sustainable') selected
                                            @endif>{{trans('admin.sustainable')}}</option>
                                    </select>
                                    <span class="error error-type"></span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label"
                                        for="superadmin_ratio">{{trans('admin.superadmin_ratio')}}</label>
                                    <input type="number" min="0" step="0.1" id="superadmin_ratio"
                                        name="superadmin_ratio" class="form-control"
                                        value="{{$data['cat']->superadmin_ratio == null ? old('superadmin_ratio') : $data['cat']->superadmin_ratio}}"
                                        placeholder="{{trans('admin.superadmin_ratio')}}" required />
                                        <span class="error error-superadmin_ratio"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label" for="main_color">{{trans('admin.main_color')}}</label>
                                    <input type="color" id="main_color" name="main_color" class="form-control"
                                        value="{{$data['cat']->main_color == null ? old('main_color') : $data['cat']->main_color}}"
                                        placeholder="{{trans('admin.main_color')}}" required />
                                        <span class="error error-main_color"></span>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="form-group">
                                    <label class="form-label" for="sub_color">{{trans('admin.sub_color')}}</label>
                                    <input type="color" id="sub_color" name="sub_color" class="form-control"
                                        value="{{$data['cat']->sub_color == null ? old('sub_color') : $data['cat']->sub_color}}"
                                        placeholder="{{trans('admin.sub_color')}}" required />
                                        <span class="error error-sub_color"></span>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="d-block" for="image">{{ __('admin.icon') }}</label>
                                    <div class="custom-file">
                                        <input type="file" class="form-control image" id="image" name="icon"
                                        value="{{old('icon')}}" placeholder="{{trans('admin.icon')}}"
                                        accept="image/png, image/jpeg, image/jpg" />
                                        <small>(Max size: 20MB)</small>
                                        <span class="error error-icon"></span>
                                    </div>
                                    <div class="prev" style="display: inline-block">
                                        <img src="{{asset('storage/'.$data['cat']->icon)}}" class="img-thumbnail preview-image"  style="width: 100px"
                                        alt="category">
                                    </div>
                                </div>

                              
                                {{-- <div class="form-group row align-items-end">
                                    <div class="col-6">
                                        <label class="form-label" for="IconInput">{{trans('admin.icon')}}</label>
                                        <input type="text" name="icon" id="IconInput" class="form-control"
                                            value="{{$data['cat']->icon ? $data['cat']->icon : old('icon')}}"
                                            placeholder="{{trans('admin.icon')}}" required>
                                    </div>
                                    <div class="col-3">
                                        <i class="{{$data['cat']->icon ? $data['cat']->icon : old('icon')}}"></i>
                                    </div>
                                    <div class="col-3">
                                        <button type="button" id="GetIconPicker" class="form-control btn btn-primary"
                                            data-iconpicker-input="input#IconInput"
                                            data-iconpicker-preview="i#IconPreview">{{trans('admin.select')}}</button>
                                    </div>
                                </div> --}}

                            </div>

                            <div class="col-12 error text-center general_error"></div>
                            <div class="col-12 mt-3" id="loading">
                                <button type="button"
                                    class="btn btn-primary submit_request_form_button">{{trans('admin.save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Basic Horizontal form layout section end -->

@push('js')

    {{-- <script src="{{asset('node_modules/@furcan/iconpicker/dist/iconpicker-1.5.0.js')}}"></script>

    <script>
        // Default options
                IconPicker.Init({
                    // Required: '{{asset("node_modules/@furcan/iconpicker/dist/iconpicker-1.5.0.json")}}',
                    jsonUrl: '{{asset("node_modules/@furcan/iconpicker/dist/iconpicker-1.5.0.json")}}',
                    // Optional: Change the buttons or search placeholder text according to the language.
                    searchPlaceholder: 'Search Icon',
                    showAllButton: 'Show All',
                    cancelButton: 'Cancel',
                    noResultsFound: 'No results found.', // v1.5.0 and the next versions
                    borderRadius: '20px', // v1.5.0 and the next versions
                });

                // Select your Button element (ID or Class)
                IconPicker.Run('#GetIconPicker');
                
    </script> --}}
    <script src="{{ url('custom/preview-image.js') }}"></script>
    <script src="{{asset('dashboard/forms/charity-categories/create.js')}}"></script>
    <script src="{{asset('custom/submit_simple_forms.js')}}"></script>
@endpush
@endsection