@extends('layout.app')

@section('title', trans('admin.charity-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_charity_cats']))
                    <a href="{{route('admin.charity-categories.create')}}" class="btn btn-primary">{{trans('admin.create-charity-categories')}}</a>
                @endif
                <div class="card-datatable table-responsive">
                <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.ch_category_name_en')}}</th>
                            <th>{{trans('admin.ch_category_name_ar')}}</th>
                            <th>{{trans('admin.ch_category_type')}}</th>
                            <th>{{trans('admin.superadmin_ratio')}}</th>
                            <th>{{trans('admin.main_color')}}</th>
                            <th>{{trans('admin.sub_color')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['charity-categories'] as $cat)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$cat->name_en}}</td>
                                <td>{{$cat->name_ar}}</td>
                                <td>{{$cat->type != null ? trans('admin.'.$cat->type) : '-'}}</td>
                                <td>{{$cat->superadmin_ratio}} %</td>
                                <td><div style="width: 30px; height: 30px; background-color: {{$cat->main_color}}"></div></td>
                                <td><div style="width: 30px; height: 30px; background-color: {{$cat->sub_color}}"></div></td>
                                <td>
                                    @if(checkPermissions(['show_charity_cats']))
                                        <a  href="{{route('admin.charity-categories.show', $cat->id)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                    {{-- @else
                                        {{trans('admin.do_not_have_permission')}} --}}
                                    @endif
                             
                                    @if(checkPermissions(['edit_charity_cats']) || checkPermissions(['delete_charity_cats']))
                                      
                                                @if(checkPermissions(['edit_charity_cats']))
                                                    <a  href="{{route('admin.charity-categories.edit', $cat->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_charity_cats']))
                                                    <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('admin.charity-categories.destroy', $cat->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['charity-categories']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection