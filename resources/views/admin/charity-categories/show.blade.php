@extends('layout.app')

@section('title', trans('admin.show-charity-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.show-charity-categories')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.ch_category_name_en')}}</th>
                                <th>{{trans('admin.ch_category_name_ar')}}</th>
                                <th>{{ trans('admin.ch_category_type') }}</th>
                                <th>{{trans('admin.superadmin_ratio')}}</th>
                                <th>{{trans('admin.main_color')}}</th>
                                <th>{{trans('admin.sub_color')}}</th>
                                {{-- <th>{{trans('admin.nation')}}</th> --}}
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['cat']->name_en}}</td>
                                <td>{{$data['cat']->name_ar}}</td>
                                <td>{{trans('admin.'.$data['cat']->type)}}</td>
                                <td>{{$data['cat']->superadmin_ratio}} %</td>
                                <td><div style="width: 30px; height: 30px; background-color: {{$data['cat']->main_color}}"></div></td>
                                <td><div style="width: 30px; height: 30px; background-color: {{$data['cat']->sub_color}}"></div></td>
                                {{-- <td>{{$data['cat']->nation->code}}</td> --}}
                                <td>
                                            <a  href="{{route('admin.charity-categories.edit', $data['cat']->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                      
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.charities')}}</h4>
                    </div>
                    <div class="card-body">
                        <div class="card-datatable table-responsive">
                            <table class="datatables-basic table">
                            <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.charity_name')}}</th>
                                <th>{{trans('admin.charity_type')}}</th>
                                <th>{{trans('admin.contract_date')}}</th>
                                {{-- <th>{{trans('admin.actions')}}</th> --}}
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                @foreach($data['charities'] as $charity)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$charity->name}}</td>
                                        <td>{{trans('admin.'.$charity->type)}}</td>
                                        <td>{{$charity->contract_date}}</td>
                                        {{-- <td></td> --}}
                                    </tr>
                                @endforeach
                            </tr>
                        </tbody>
                        </table>
                    </div>
                    </div>
                    {{$data['charities']->links()}}
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection