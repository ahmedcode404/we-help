@extends('layout.app')

@section('title', trans('admin.create_marketing'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{asset('dashboard/app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css')}}">
<link rel="stylesheet" type="text/css"
    href="{{asset('dashboard/app-assets/css/plugins/forms/pickers/form-flat-pickr.css')}}">
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endpush

@section('content')

<section id="basic-horizontal-layouts">
    <div class="row">
        <div class="col-md-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{trans('admin.create_marketing')}}</h4>
                </div>
                <div class="card-body">
                    <form id="marketing_form" method="POST" class="submit_form"
                        action="{{route('admin.marketings.store')}}" enctype="multipart/form-data">
                        @csrf

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="type">{{trans('admin.send_method')}}</label>
                                    <select class="form-control select2" id="type" name="type" required>
                                        <option value="sms" @if(old('type')=='sms' ) selected @endif>
                                            {{trans('admin.sms')}}</option>
                                        <option value="email" @if(old('type')=='email' ) selected @endif>
                                            {{trans('admin.email')}}</option>
                                        <option value="mobile" @if(old('type')=='mobile' ) selected @endif>
                                            {{trans('admin.mobile_app')}}</option>
                                    </select>
                                    <div class="error error-type"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="intended_users">{{trans('admin.intended_users')}}</label>
                                    <select class="form-control select2" id="intended_users" name="intended_users"
                                        required>
                                        <option value="all" @if(old('intended_users')=='all' ) selected @endif>
                                            {{trans('admin.all')}}</option>
                                        <option value="most_donated" @if(old('intended_users')=='most_donated' )
                                            selected @endif>{{trans('admin.most_donated')}}</option>
                                        <option value="least_donated" @if(old('intended_users')=='least_donated' )
                                            selected @endif>{{trans('admin.least_donated')}}</option>
                                        <option value="never_donated" @if(old('intended_users')=='never_donated' )
                                            selected @endif>{{trans('admin.never_donated')}}</option>
                                    </select>
                                    <div class="error error-intended_users"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="date">{{trans('admin.send_date')}}</label>
                                    <input type="text" id="date" class="form-control date-picker" readonly
                                        autocomplete="off" value="{{old('date')}}" name="date"
                                        placeholder="{{trans('admin.date')}}" required />
                                    <div class="error error-date"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="time">{{trans('admin.send_time')}}</label>
                                    <input type="text" id="time" name="time"
                                        class="form-control flatpickr-time text-left" value="{{old('time')}}"   
                                        placeholder="HH:MM" required />
                                    <div class="error error-time"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="time">{{trans('admin.project')}}</label>
                                    <select name="project_link" id="project_id" class="form-control">
                                        <option value="">{{ trans('admin.select') }}</option>
                                        @foreach ($data['projects'] as $project)
                                        <option value="{{ $project->id }}" data-slug="{{ $project->slug }}">
                                            {{ $project->name}}
                                            @if($project->charity)
                                            - {{ $project->charity->name }}
                                            @endif
                                        </option>
                                        @endforeach
                                    </select>
                                    <div class="error error-project_link"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="link">{{trans('admin.campaing_link')}}</label>
                                    <input type="text" id="link" name="link" class="form-control"
                                        value="{{\URL::to('/').'/aff/'.$data['aff']}}"
                                        placeholder="{{trans('admin.link')}}" readonly />
                                    <input type="hidden" id="aff" name="aff" value="{{$data['aff']}}">
                                    <input type="hidden" id="aff2" value="{{ $data['aff'] }}">
                                    <div class="error error-aff"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="title">{{trans('admin.campaing_title')}}</label>
                                    <input type="text" class="form-control" id="title" name="title"
                                        value="{{old('title')}}" placeholder="{{trans('admin.title')}}" required />
                                    <div class="error error-title"></div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label class="form-label" for="content">{{trans('admin.campaing_content')}}</label>
                                    <textarea name="content" id="content" class="form-control" cols="30" rows="20"
                                        placeholder="{{trans('admin.content')}}" required>{{old('content')}}</textarea>
                                    <div class="error error-content"></div>
                                </div>
                            </div>

                            <div class="col-12 error text-center general_error"></div>
                            <div class="col-12" id="loading">
                                <button type="button"
                                    class="btn btn-primary submit_button">{{trans('admin.save')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@push('js')
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script src="{{asset('dashboard/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>

@if(\App::getLocale() == 'ar')
<!--for arabic-only-->
<script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
<script src="{{asset('dashboard/js/summernote-ar-AR.min.js')}}"></script>
<!--end-->
@endif
<script>
    //datepicker
    $(function () {
        $('.date-picker').datepicker({
            changeYear: true,
            dateFormat: 'yy-mm-dd',
        })
    });
</script>
{{-- <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script> --}}
@if(\App::getLocale() == 'ar')
<script>
    $('#content').summernote({
                height: 250,
                lang:"ar-AR"
            });
</script>
@else
<script>
    $('#content').summernote({
                height: 250,
            });
</script>
@endif

<script src="{{asset('dashboard/forms/marketings/create.js')}}"></script>

@endpush
@endsection