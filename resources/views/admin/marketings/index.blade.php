@extends('layout.app')

@section('title', trans('admin.marketings'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_marketings']))
                    <a href="{{route('admin.marketings.create')}}" class="btn btn-primary">{{trans('admin.create_marketing')}}</a>
                @endif

                <br>

                <div class="row taps_eidt  justify-content-center mb-4">
                    <div class="col-lg-2 col-md-3 col-6 mb-1">
                        <a href="{{route('admin.marketings.index')}}" class="btn btn-primary">{{trans('admin.all')}}</a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-6 mb-1">
                        <a href="{{route('admin.get-marketings-by-type', 'sms')}}" class="btn btn-info">{{trans('admin.sms')}}</a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-6 mb-1">
                        <a href="{{route('admin.get-marketings-by-type', 'email')}}" class="btn btn-warning">{{trans('admin.email')}}</a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-6 mb-1">
                        <a href="{{route('admin.get-marketings-by-type', 'mobile')}}" class="btn btn-danger">{{trans('admin.mobile_app')}}</a>
                    </div>
                </div>
                
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.campaing_title')}}</th>
                            {{-- <th>{{trans('admin.content')}}</th> --}}
                            <th>{{trans('admin.date')}} / {{trans('admin.time')}}</th>
                            <th>{{trans('admin.intended_users')}}</th>
                            <th>{{trans('admin.seen')}}</th>
                            <th>{{trans('admin.donated')}}</th>
                            <th>{{trans('admin.donors_ratio')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['marketings'] as $marketing)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    @php
                                        $route = '#';

                                        if($marketing->aff == null){

                                            $route = route('web.projects.show', $marketing->project_link);
                                        }
                                        else{
                                            $route = route('aff' , $marketing->aff);
                                        }
                                    @endphp         
                                    <a href="{{$route}}">{{$marketing->title}}</a>
                                </td>
                               
                                <td>{{$marketing->date->format('Y-m-d')}} / {{$marketing->time->format('H:i')}}</td>
                                <td>{{trans('admin.'.$marketing->intended_users)}}</td>
                                <td>{{$marketing->seen}}</td>
                                <td>{{$marketing->getDonationAmounts()}}</td> {{-- اجمالى التبرعات اللى جت من الناس بتوع الحملة دل --}}
                                <td>{{$marketing->usersRatio()}} %</td> {{-- نسبة المتبرعين اللى اتبرعوا بسبب الحملة دى بالنسبه لاجمالى المتبرعين --}}
                                {{-- <td>{{$marketing->nation->code}}</td> --}}
                                
                                <td>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#info{{$marketing->id}}" title="{{trans('admin.show')}}">
                                        <i data-feather="eye" class="mr-50"></i>
                                    </a>
                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="info{{$marketing->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myModalLabel130">{{trans('admin.content')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        {!! $marketing->content!!}
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if(checkPermissions(['edit_marketings']) || checkPermissions(['delete_marketings']))
                                        
                                        @if(checkPermissions(['edit_marketings']))
                                        @if($marketing->id == 2)
                                        @endif
                                            @if($marketing->date->format('Y-m-d') >= now()->format('Y-m-d') && $marketing->time->format('H:i') > now()->format('H:i'))
                                                <a  href="{{route('admin.marketings.edit', $marketing->id)}}" title="{{trans('admin.edit')}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                </a>
                                            @endif
                                        @endif
                                        @if(checkPermissions(['delete_marketings']))
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.marketings.destroy', $marketing->id)}}" title="{{trans('admin.delete')}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                        @endif
                                            
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['marketings']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection