@extends('layout.app')

@section('title', trans('admin.add_campaign'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

        <!-- users edit start -->
        <section class="app-user-edit">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{trans('admin.add_campaign')}}</h4>
                </div>                    
                <div class="card-body">
                    <div class="tab-content">
                        <!-- Account Tab starts -->
                        <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
                            <form class="form-validate" id="nationalities-form" method="post" action="{{ route('admin.campaigns.store') }}">
                                @csrf
                                <div class="row">

                                    {{-- name_ar start --}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="name_ar">{{trans('admin.name_campaign_ar')}}</label>
                                            <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{old('name_ar')}}" placeholder="{{trans('admin.name_campaign_ar')}}" required/>
                                            @if($errors->has('name_ar'))
                                                <div class="error">{{ $errors->first('name_ar') }}</div>
                                            @endif                                            
                                        </div>
                                    </div>
                                    {{-- name_ar end --}}

                                    {{-- name_en start --}}
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="form-label" for="name_en">{{trans('admin.name_campaign_en')}}</label>
                                            <input type="text" class="form-control" id="name_en" name="name_en" value="{{old('name_en')}}" placeholder="{{trans('admin.name_campaign_en')}}" required/>
                                            @if($errors->has('name_en'))
                                                <div class="error">{{ $errors->first('name_en') }}</div>
                                            @endif                                                
                                        </div>
                                    </div>
                                    {{-- name_en end --}}

                              

                                <div class="col-12 mt-2">
                                    <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1"> {{ __('admin.save') }}</button>
                                </div>
                                </div>
                            </form>
                            <!-- users edit account form ends -->
                        </div>
                        <!-- Account Tab ends -->

                    </div>
                </div>
            </div>
        </section>
        <!-- users edit ends -->

@endsection

@push('js')

    <script src="{{asset('dashboard/forms/nationalities/create.js')}}"></script>
    
@endpush