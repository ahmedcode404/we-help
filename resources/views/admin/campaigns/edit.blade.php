@extends('layout.app')

@section('title', trans('admin.edit_campaign'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" />    
@endpush

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit_campaign')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="nationalities-form" method="POST" action="{{route('admin.campaigns.update', $campaign_one->id)}}">
                            @csrf
                            @method('PUT')

                            <div class="row">

                                {{-- name_ar start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_ar">{{trans('admin.name_campaign_ar')}}</label>
                                        <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{ $campaign_one->name_ar ? $campaign_one->name_ar : old('name_ar')}}" placeholder="{{trans('admin.name_campaign_ar')}}" />
                                        @if($errors->has('name_ar'))
                                            <div class="error">{{ $errors->first('name_ar') }}</div>
                                        @endif                                            
                                    </div>
                                </div>
                                {{-- name_ar end --}}

                                {{-- name_en start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_en">{{trans('admin.name_campaign_en')}}</label>
                                        <input type="text" class="form-control" id="name_en" name="name_en" value="{{$campaign_one->name_en ? $campaign_one->name_en : old('name_en')}}" placeholder="{{trans('admin.name_campaign_en')}}" />
                                        @if($errors->has('name_en'))
                                            <div class="error">{{ $errors->first('name_en') }}</div>
                                        @endif                                                
                                    </div>
                                </div>
                                {{-- name_en end --}}

                                </div>   


                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary"> <i data-feather="edit" class="mr-50"></i> {{trans('admin.edit')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection


@push('js')

    <script src="{{asset('dashboard/forms/nationalities/create.js')}}"></script>
    
@endpush