@extends('layout.app')

@section('title', trans('admin.campaigns_goals'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_campaign_goal']))
                    <a href="{{route('admin.campaigns.create')}}" class="btn btn-primary">{{trans('admin.add_campaign')}}</a>
                @endif            
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_campaign_ar')}}</th>
                            <th>{{trans('admin.name_campaign_en')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($campaigns as $campaign)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $campaign->name_ar }}</td>
                                <td>{{ $campaign->name_en }}</td>

                                <td>
                                    @if(checkPermissions(['edit_campaign_goal']) || checkPermissions(['delete_campaign_goal']))
                                                @if(checkPermissions(['edit_campaign_goal']))
                                                    <a  href="{{route('admin.campaigns.edit', $campaign->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_campaign_goal']))
                                                    <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('admin.campaigns.destroy', $campaign->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection
