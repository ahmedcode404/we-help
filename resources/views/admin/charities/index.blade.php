@extends('layout.app')

@section('title', trans('admin.charities'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="text-md-left text-center mb-3">

                    @if(checkPermissions(['create_charity']))
                        <a href="{{route('admin.charities.create')}}" class="btn btn-primary">{{trans('admin.create_charity')}}</a>
                    @endif
                    
                    <a href="{{route('admin.export-all-charities', request()->segment(count(request()->segments())))}}" class="btn btn-primary">{{trans('admin.export')}}</a>
                </div>

                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.charity_name')}}</th>
                            <th>{{trans('admin.charity_type')}}</th>
                            <th>{{trans('admin.contract_date')}}</th>
                            <th>{{trans('admin.charity_status')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th>
                            <th>{{trans('admin.export')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['charities'] as $charity)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    {{$charity->name}}
                                </td>
                                <td>{{trans('admin.'.$charity->type)}}</td>
                                <td>{{$charity->contract_date}}</td>
                                <td>
                                    @if($charity->status == 'approved')
                                        <span id="current_status-{{$charity->id}}" class="badge badge-light-success">{{trans('admin.'.$charity->status)}}</span>
                                    @elseif($charity->status == 'waiting')
                                        <span id="current_status-{{$charity->id}}" class="badge badge-light-warning">{{trans('admin.'.$charity->status)}}</span>
                                    @elseif($charity->status == 'hold')
                                        <span id="current_status-{{$charity->id}}" class="badge badge-light-info">{{trans('admin.'.$charity->status)}}</span>
                                    @elseif($charity->status == 'rejected')
                                        <span id="current_status-{{$charity->id}}" class="badge badge-light-danger">{{trans('admin.'.$charity->status)}}</span>
                                    @endif
                                    {{-- <a class="btn btn-primary" data-toggle="modal" data-target="#charity-{{$charity->id}}">{{trans('admin.change')}}</a>
                                    <!-- Modal -->  
                                    <div class="modal fade modal-info text-left" id="charity-{{$charity->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                       
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="change_charity_status-{{$charity->id}}">{{trans('admin.select_status')}}</label>
                                                    <select id="change_charity_status-{{$charity->id}}" class="form-control">
                                                        <option value="approved" @if($charity->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                        <option value="rejected" @if($charity->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                        <option value="hold" @if($charity->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="charity_notes-{{$charity->id}}">{{trans('admin.notes')}}</label>
                                                    <textarea id="charity_notes-{{$charity->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$charity->notes}}</textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="modal-footer">
                                                <button type="button" id="" class="btn btn-info save_status" data-model="charity" data-id="{{$charity->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                            </div>
                                        </div>
                                    </div> --}}
                                </td>
                              
                                <td>
                                    @if(checkPermissions(['edit_charity']) || checkPermissions(['delete_charity']))
                                                @if(checkPermissions(['show_charity']))
                                                <a  href="{{route('admin.charities.show', $charity->id)}}"><i data-feather="eye" class="mr-50"></i></a>
                                            {{-- @else
                                                {{trans('admin.do_not_have_permission')}} --}}
                                            @endif
                                                {{-- @if(checkPermissions(['show_charity']))
                                                    <a  href="{{route('admin.charities.show', $charity->id)}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                        <span>{{trans('admin.show')}}</span>
                                                    </a>
                                                @endif --}}
                                                @if(checkPermissions(['edit_charity']))
                                                    <a  href="{{route('admin.charities.edit', $charity->id)}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif

                                                <a  href="{{route('admin.export-charity', $charity->id)}}" data-url="" title="{{trans('admin.export')}}">
                                                    <i data-feather="download" class="mr-50"></i>
                                                </a>

                                                @if(checkPermissions(['delete_charity']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.charities.destroy', $charity->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                             
                                            </div>
                                          
                                    @endif
                                 

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['charities']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush