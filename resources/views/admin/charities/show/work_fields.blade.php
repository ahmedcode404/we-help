<div class="row invoice-list-wrapper">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-2">{{trans('admin.charity_work-fields')}}</h4>
            </div>
            <div class="card-datatable table-responsive">
                <table  class="invoice-list-table table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('admin.name')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data['charity']->work_fields as $field)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$field->name}}</td>
                                <td>
                                            <a  href="{{route('admin.work-fields.show', $field->id)}}" title="{{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                            <a  href="{{route('admin.work-fields.edit', $field->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                            <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('admin.work-fields.destroy', $field->id)}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">{{trans('admin.no_data')}}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>