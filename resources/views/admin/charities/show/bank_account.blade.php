    <!-- Banl Account Data -->
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{trans('admin.bank_data')}}</h4>
        </div>
        <p class="card-text ml-2">{{trans('admin.charity_bank_data')}}</p>
        <div class="table-responsive">
            <table    class="table table-striped table-borderless">
                <tbody>
                    <tr>
                        <td><strong>{{trans('admin.iban')}}</strong></td>
                        <td>{{$data['charity']->iban}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.name')}}</strong></td>
                        <td>{{$data['charity']->bank_name}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.address')}}</strong></td>
                        <td>{{$data['charity']->bank_address}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.city')}}</strong></td>
                        <td>{{$data['charity']->bank_city}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.phone')}}</strong></td>
                        <td dir="ltr">{{$data['charity']->bank_phone}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.swift_code')}}</strong></td>
                        <td>{{$data['charity']->swift_code}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- /Banl Account Data -->
