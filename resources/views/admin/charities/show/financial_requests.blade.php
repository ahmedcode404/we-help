<div class="row invoice-list-wrapper">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-2">{{trans('admin.financial_requests')}}</h4>
            </div>
            <div class="card-datatable table-responsive">
                <table class="invoice-list-table datatables-basic table  text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('admin.type')}}</th>
                            <th>{{trans('admin.date')}}</th>
                            <th>{{trans('admin.status')}}</th>
                            {{-- <th>{{trans('admin.notes')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data['charity']->exchange_requests as $req)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{trans('admin.'.$req->type)}}</td>
                                <td>{{$req->created_at->format('Y-m-d')}}</td>
                                <td>
                                    <span id="current_status">{{trans('admin.'.$req->status)}}</span>
                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="finanace_req-{{$req->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="change_fin_status-{{$req->id}}">{{trans('admin.select_status')}}</label>
                                                        <select id="change_fin_status-{{$req->id}}" class="form-control">
                                                            <option value="waiting" @if($req->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                            <option value="approved" @if($req->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                            <option value="rejected" @if($req->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                            <option value="hold" @if($req->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="fin_notes-{{$req->notes}}">{{trans('admin.notes')}}</label>
                                                        <textarea id="fin_notes-{{$req->notes}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" id="" class="btn btn-info save_status" data-model="fin_request" data-id="{{$req->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                           
                                <td>
                                    @if($req->notes)
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#info" title="{{trans('admin.notes')}}"><i data-feather="eye" class="mr-50"></i></a>
                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    {!! $req->notes !!}
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <a  href="javascript:void(0);" data-toggle="modal" data-target="#finanace_req-{{$req->id}}" title="{{trans('admin.change')}}"><i data-feather="more-vertical" class="mr-50"></i></a>

                                    {{-- <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i data-feather="more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            {{-- <a class="dropdown-item" href="{{route('admin.projects.show', $project->id)}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                                <span>{{trans('admin.show')}}</span>
                                            </a>
                                            <a class="dropdown-item" href="{{route('admin.projects.edit', $project->id)}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                                <span>{{trans('admin.edit')}}</span>
                                            </a>
                                            <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.projects.destroy', $project->id)}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                                <span>{{trans('admin.delete')}}</span>
                                            </a> --}}
                                        {{-- </div>
                                    </div>  --}}
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">{{trans('admin.no_data')}}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>