    <div class="card">
        <div class="card-header">
            <h4 class="card-title mb-2">{{trans('admin.official_info')}}</h4>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <ul class="timeline">
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.license_number')}}</h6>
                                </div>
                                <p>{{$data['charity']->license_number}}</p>

                                <div class="timeline-event">
                                    <div class="media align-items-center">
                                        <div class="media-body"><a href="{{asset('storage/'.$data['charity']->license_file)}}">{{trans('admin.show')}}</a></div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-warning timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.license_start_date')}}</h6>
                                </div>
                                <p>{{$data['charity']->license_start_date}}</p>
                            </div>
                        </li>
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.license_end_date')}}</h6>
                                </div>
                                <p class="mb-0">{{$data['charity']->license_end_date}}</p>
                            </div>
                        </li>
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.establish_date')}}</h6>
                                </div>
                                <p class="mb-0">{{$data['charity']->establish_date}}</p>
                            </div>
                        </li>
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.branches_num')}}</h6>
                                </div>
                                <p class="mb-0">{{$data['charity']->branches_num}}</p>
                            </div>
                        </li>
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.internal_image')}}</h6>
                                </div>
                                <p>{{trans('admin.internal_image_text')}}</p>
                                <div class="media align-items-center">
                                    {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                                    <div class="media-body"><a href="{{asset('storage/'.$data['charity']->internal_image)}}">{{trans('admin.show')}}</a></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="timeline">
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.members_num')}}</h6>
                                </div>
                                <p class="mb-0">{{$data['charity']->members_num}}</p>
                            </div>
                        </li>
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-info timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.contract_date')}}</h6>
                                </div>
                                <p class="mb-0">{{$data['charity']->contract_date}}</p>
                                @if($data['charity']->contract_after_sign))
                                    <div class="media align-items-center">
                                        {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                                        <div class="media-body"><a href="{{asset('storage/'.$data['charity']->contract_after_sign)}}">{{trans('admin.show')}}</a></div>
                                    </div>
                                @endif
                            </div>
                        </li>
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.attach')}}</h6>
                                </div>
                                <p>{{trans('admin.attach_text')}}</p>
                                <div class="media align-items-center">
                                    {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                                    <div class="media-body"><a href="{{asset('storage/'.$data['charity']->attach)}}">{{trans('admin.show')}}</a></div>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-item">
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
                                    <h6>{{trans('admin.year_report')}}</h6>
                                </div>
                                <p>
                                    @if($data['charity']->years == 'one')
                                        {{trans('admin.year_report')}} {{trans('admin.for')}} {{trans('last')}} {{trans('year')}}
                                    @elseif($data['charity']->years == 'two')
                                        {{trans('admin.year_report')}} {{trans('admin.for')}} {{trans('last')}} {{trans('admin.2')}}  @if(\App::getLocale() == 'en') {{trans('years')}} @endif
                                    @else
                                        {{trans('admin.year_report')}} {{trans('admin.for')}} {{trans('last')}} {{trans('admin.3')}} {{trans('years')}} 
                                    @endif
                                </p>
                                <div class="media align-items-center">
                                    {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                                    <div class="media-body"><a href="{{asset('storage/'.$data['charity']->year_report)}}">{{trans('admin.show')}}</a></div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
