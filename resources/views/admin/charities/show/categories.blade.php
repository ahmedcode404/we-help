<div class="col-xl-12 col-lg-12 col-md-12">
    <div class="card plan-card border-primary">
        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
            <h4 class="mb-0">{{trans('admin.categories')}}</h4> 
        </div>
        <div class="card-body">
            <ul class="list-inline mb-0">
                @forelse ($data['charity']->categories as $category)
                    <li class="mb-1 badge badge-light-primary">{{$category->name}}</li> 
                @empty
                    <li>{{trans('admin.no_data')}}</li>
                @endforelse
            </ul>
            <br>
        </div>
    </div>
</div>