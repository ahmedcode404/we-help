<div class="row invoice-list-wrapper">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-2">{{trans('admin.reports')}}</h4>
            </div>
            <div class="card-datatable table-responsive">
                <table class="invoice-list-table datatables-basic table  text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('admin.project')}}</th>
                            <th>{{trans('admin.phase')}}</th>
                            <th>{{trans('admin.status')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data['charity']->reports as $report)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$report->project->name}}</td>
                                <td>{{$report->phase->name}}</td>
                                <td>
                                    <span id="current_status-{{$report->id}}">{{trans('admin.'.$report->status)}}</span>
                                    <!-- Modal -->  
                                    <div class="modal fade modal-info text-left" id="report-{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                    
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="change_report_status-{{$report->id}}">{{trans('admin.select_status')}}</label>
                                                    <select id="change_report_status-{{$report->id}}" class="form-control">
                                                        <option value="waiting" @if($report->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                        <option value="approved" @if($report->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                        <option value="rejected" @if($report->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                        <option value="hold" @if($report->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="report_notes-{{$report->id}}">{{trans('admin.notes')}}</label>
                                                    <textarea id="report_notes-{{$report->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="modal-footer">
                                                <button type="button" id="" class="btn btn-info save_status" data-model="report" data-id="{{$report->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST"> <div class="spinner-border  processing-div-{{ $report->id}}" role="status"></div>{{trans('admin.save')}}</button>
                                            </div>
                                        </div>
                                    </div>                                                            
                                    </div>
                                </td>
                                <td>
                                  
                                            <a  href="{{route('admin.reports.show', $report->id)}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                                {{-- <span>{{trans('admin.show')}}</span> --}}
                                            </a>
                                            <a  href="{{route('admin.reports.edit', $report->id)}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                                {{-- <span>{{trans('admin.edit')}}</span> --}}
                                            </a>
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.reports.destroy', $report->id)}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                                {{-- <span>{{trans('admin.delete')}}</span> --}}
                                            </a>
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#report-{{$report->id}}" title="{{trans('admin.change')}}"><i data-feather="more-vertical" class="mr-50"></i></a>

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">{{trans('admin.no_data')}}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>