<section class="app-user-edit">
    <div class="card">
        <div class="card-header">
            {{trans('admin.charity_signature')}}
        </div>
        <div class="card-body">
            <ul class="nav nav-pills" role="tablist">
                <li class="nav-item">
                    @if($data['charity']->signature != null)
                        <img src="{{$data['charity']->signature}}">
                    @else
                        <a href="{{asset('storage/'.$data['charity']->contract_after_sign)}}"></a>
                    @endif
                </li>
            </ul>
        </div>
    </div>
</section>