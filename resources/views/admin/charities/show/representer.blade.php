    <!-- charity Representer data -->
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{trans('admin.representer')}}</h4>
        </div>
        <p class="card-text ml-2">{{trans('admin.representer_data')}}</p>
        <div class="table-responsive">
            <table  class="table table-striped table-borderless">
                <tbody>
                    <tr>
                        <td><strong>{{trans('admin.name')}}</strong></td>
                        <td>{{$data['charity']->representer_name}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.representer_passport_image')}}</strong></td>
                        <td>
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="media align-items-center">
                                    <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_passport_image)}}">{{trans('admin.show')}}</a></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.representer_nation_image')}}</strong></td>
                        <td>
                            <span class="timeline-point timeline-point-indicator"></span>
                            <div class="timeline-event">
                                <div class="media align-items-center">
                                    <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_nation_image)}}">{{trans('admin.show')}}</a></div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.representer_title')}}</strong></td>
                        <td>{{$data['charity']->representer_title}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.representer_email')}}</strong></td>
                        <td>{{$data['charity']->representer_email}}</td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.representer_title_file')}}</strong></td>
                        <td>
                            <div class="media align-items-center">
                                {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_title_file)}}">{{trans('admin.show')}}</a></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>{{trans('admin.international_member')}}</strong></td>
                        <td>
                            {{-- <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="user-read" @if($data['charity']->international_member == 1) checked @endif disabled />
                                <label class="custom-control-label" for="user-read">{{$data['charity']->international_member == 1 ? trans('admin.yes') : trans('admin.no')}}</label>
                            </div> --}}
                            {{$data['charity']->international_member == 1 ? trans('admin.yes') : trans('admin.no')}}

                            <br>
                            {{$data['charity']->international_member == 1 ? $data['charity']->international_name : ''}}
                        </td>
                    </tr>
                    {{-- @if($data['charity']->international_member == 1)
                        <tr>
                            <td><strong>{{trans('admin.international_name')}}</strong></td>
                            <td>{!! $data['charity']->international_name !!}</td>
                        </tr>
                    @endif --}}
                </tbody>
            </table>
        </div>
    </div>
    <!-- /Charity Representer data -->
    
