<div class="row invoice-list-wrapper">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-2">{{trans('admin.projects')}}</h4>
            </div>
            @php
                $session_currency = currencySymbol(session('currency'));
            @endphp
            <div class="card-datatable table-responsive">
                <table  class="invoice-list-table datatables-basic table  text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('admin.project_num')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{$session_currency}} {{trans('admin.project_cost')}}</th>
                            <th>{{trans('admin.superadmin_ratio')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $projects = $data['charity']->projects()->where('profile', 0)->get();
                        @endphp
                        @forelse ($projects as $project)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$project->project_num}}</td>
                                <td>{{$project->name}}</td>
                                <td>{{generalExchange($project->get_total, $project->currency->symbol, $session_currency)}} {{$session_currency}}</td>
                                <td>{{$project->category ? $project->category->superadmin_ratio.' %' : 0.0.' %'}}</td>
                                <td>
                                  
                                    @if(checkPermissions(['show_project']))
                                        <a  href="{{route('admin.projects.show', $project->id)}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                            {{-- <span>{{trans('admin.show')}}</span> --}}
                                        </a>
                                    @endif
                                    @if(checkPermissions(['edit_project_data']))
                                        <a  href="{{route('admin.projects.edit', $project->id)}}">
                                            <i data-feather="edit-2" class="mr-50"></i>
                                            {{-- <span>{{trans('admin.edit')}}</span> --}}
                                        </a>
                                    @endif
                                    {{-- @if(checkPermissions(['delete_project']))
                                        <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.projects.destroy', $project->id)}}">
                                            <i data-feather="trash" class="mr-50"></i>
                                            <span>{{trans('admin.delete')}}</span>
                                        </a>
                                    @endif --}}
                                     
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">{{trans('admin.no_data')}}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
</div>