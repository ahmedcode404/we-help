    <!-- charity Strategy -->
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{trans('admin.charity_strategy')}}</h4>
        </div>
        <div class="card-body">
          {!! $data['charity']->strategy !!}
        </div>
    </div>
    <!-- /Charity Strategy -->

    <!-- charity work_fields -->
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">{{trans('admin.charity_work_fields')}}</h4>
        </div>
        <div class="card-body">
            {!! $data['charity']->work_fields !!}</td>
        </div>      
    </div>
    <!-- /Charity work_fields -->
