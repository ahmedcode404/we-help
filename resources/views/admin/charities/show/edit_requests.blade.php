<div class="row invoice-list-wrapper">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-2">{{trans('admin.edit_requests')}}</h4>
            </div>
            <div class="card-datatable table-responsive">
                <table class="invoice-list-table datatables-basic table  text-center">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{trans('admin.type')}}</th>
                            <th>{{trans('admin.status')}}</th>
                            <th>{{trans('admin.notes')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($data['charity']->edit_requests as $req)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{trans('admin.'.$req->type)}}</td>
                                <td>
                                    <span id="current_status">{{trans('admin.'.$req->status)}}</span>
                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="req_status-{{$req->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                           
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="change_edit_status-{{$req->id}}">{{trans('admin.select_status')}}</label>
                                                        <select id="change_edit_status-{{$req->id}}" class="form-control">
                                                            <option value="waiting" @if($req->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                            <option value="approved" @if($req->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                            <option value="rejected" @if($req->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                            <option value="hold" @if($req->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="edit_notes-{{$req->id}}">{{trans('admin.notes')}}</label>
                                                        <textarea id="edit_notes-{{$req->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                     <button type="button" id="" class="btn btn-info save_status" data-model="{{ $req->type == 'project' ? 'edit_request_project' : 'edit_request_charity' }}" data-id="{{$req->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST"> <div class="spinner-border  processing-div-{{ $req->id}}" role="status"></div> {{trans('admin.save')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @if($req->notes)
                                        <a class="btn btn-outline-info" data-toggle="modal" data-target="#edit_request">{{trans('admin.notes')}}</a>
                                        <!-- Modal -->
                                        <div class="modal fade modal-info text-left" id="edit_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {!! $req->notes !!}
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        __
                                    @endif
                                </td>
                                <td>
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.requests.destroy', $req->id)}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                                {{-- <span>{{trans('admin.delete')}}</span> --}}
                                            </a>

                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#req_status-{{$req->id}}" title="{{trans('admin.change')}}"><i data-feather="edit-2" class="mr-50"></i></a>

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">{{trans('admin.no_data')}}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>