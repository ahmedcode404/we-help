<div class="col-xl-12 col-lg-12 col-md-12">
    <div class="card user-card">
        <div class="card-body">
            <div class="row">
                <div class="col-xl-5 col-lg-5 d-flex flex-column justify-content-between border-container-lg mb-1">
                    <div class="user-avatar-section">
                        <div class="d-flex justify-content-start">
                            <img class="img-fluid rounded" src="{{$data['charity']->logo ? asset('storage/'.$data['charity']->logo) : asset('dashboard/images/avatar.png')}}" style="max-height:70px" width="70" alt="{{$data['charity']->name}}" />
                            <div class="d-flex flex-column ml-1">
                                <div class="user-info mb-1">
                                    <h4 class="mb-0">{{$data['charity']->name}} ({{$data['charity']->nation->code}})</h4>
                                    <span class="card-text">{{$data['charity']->email}}</span>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <a href="{{route('admin.charities.edit', $data['charity']->id)}}" class="btn btn-primary">{{trans('admin.edit')}}</a>
                                    <button class="btn btn-outline-danger ml-1 remove-table" data-redirect_to_main="charity" data-url="{{route('admin.charities.destroy', $data['charity']->id)}}">{{trans('admin.delete')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex align-items-center user-total-numbers mt-2">
                        <div class="d-flex align-items-center mr-2">
                            <div class="color-box bg-light-primary">
                                {{$session_currency}}
                            </div>
                            <div class="ml-1">
                                <h5 class="mb-0">{{getTotalSupports($data['charity']->projects()->where('profile', 0)->get())}}</h5>
                                <small>{{trans('admin.total_supports')}}</small>
                            </div>
                        </div>
                        <div class="d-flex align-items-center">
                            <div class="color-box bg-light-success">
                                <i data-feather="check" class="text-success"></i>
                            </div>
                            <div class="ml-1">
                                <h5 class="mb-0">{{count($data['charity']->projects()->where('profile', 0)->get())}}</h5>
                                <small>{{trans('admin.total_projects')}}</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4  col-md-6  mt-2 mt-xl-0">
                    <div class="user-info-wrapper">
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.charity_type')}}:</span>
                            </div>
                            <p class="card-text mb-0">{{trans('admin.'.$data['charity']->type)}}</p>
                        </div>
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.charity_status')}}:</span>
                            </div>
                            <p class="card-text mb-0">{{trans('admin.'.$data['charity']->status)}}</p>
                        </div>
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.role')}}:</span>
                            </div>
                            <p class="card-text mb-0">{{trans('admin.charity')}}</p>
                        </div>
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.country')}}:</span>
                            </div>
                            {{-- <p class="card-text mb-0">{{$data['charity']->country->name}}</p> --}}
                            <p class="card-text mb-0">{{Countries::getOne($data['charity']->country, \App::getLocale())}}</p>
                        </div>
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.city')}}:</span>
                            </div>
                            {{-- <p class="card-text mb-0">{{$data['charity']->city ? $data['charity']->city->name : '--'}}</p> --}}
                            <p class="card-text mb-0">{{$data['charity']->city ? $data['charity']->city : '--'}}</p>
                        </div>
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.phone')}}:</span>
                            </div>
                            <p class="card-text mb-0">
                                <span dir="ltr">{{$data['charity']->phone}}</span>
                            </p>
                        </div>
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.mobile')}}:</span>
                            </div>
                            <p class="card-text mb-0">
                                <span dir="ltr">{{$data['charity']->mobile}}</span>
                            </p>
                        </div>
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="check" class="mr-1"></i>
                                <span class="card-text text-primary user-info-title font-weight-bold mb-1 mr-1">{{trans('admin.charity_address')}}:</span>
                            </div>
                            <p class="card-text mb-0">{{$data['charity']->address}}</p>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-3 col-md-6 mt-2 mt-xl-0">
                    <div class="user-info-wrapper with-icon">
                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="chrome" class="mr-1"></i>
                                <span class="align-middle">{{$data['charity']->website == null ? trans('admin.not_registered') : $data['charity']->website }}</span>
                            </div>
                        </div>

                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="facebook" class="mr-1"></i>
                                <span class="align-middle">{{$data['charity']->twitter_link == null ? trans('admin.not_registered') : $data['charity']->twitter_link}}</span>
                            </div>
                        </div>

                        <div class="d-flex flex-wrap mb-1">
                            <div class="user-info-title">
                                <i data-feather="twitter" class="mr-1"></i>
                                <span class="align-middle">{{$data['charity']->facebook_link == null ? trans('admin.not_registered') : $data['charity']->facebook_link}}</span>
                            </div>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>