@php
    $prev_projects = $data['charity']->projects()->where('profile', 1)->get();
@endphp


<section class="app-user-edit">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title"> {{trans('admin.prev_projects')}}</h4>
        </div>
        <div class="card-body">
            {{-- <ul class="nav nav-pills" role="tablist">

                @forelse ($prev_projects as $project)
                    <li class="nav-item">
                        <h6 class="card-title"> {{trans('admin.name')}}: {{$project->name}} - {{ $project->type }}</h6>
                    </li>
                    <br>
                @empty
                    <li>{{trans('admin.no_data')}}</li>
                @endforelse
                
            </ul> --}}
                <!-- basic info Tab starts -->
                @foreach ($prev_projects as $project)
                    <h6 class="card-title"> {{trans('admin.name')}}: {{$project->name}} - {{ $project->type }}</h6>
                    <div id="project-{{$project->id}}">
                        <div class="profile-header my-2">
                                {{-- <h4 class="card-title">{{trans('admin.project_images')}}</h4> --}}
                                <div id="carousel-keyboard-{{ $loop->iteration}}" class="carousel slide" data-keyboard="true">
                                    @if(count($project->images) > 1)
                                        <ol class="carousel-indicators">
                                            @foreach ($project->images as $image)
                                                <li data-target="#carousel-keyboard-{{ $loop->iteration}}" data-slide-to="{{ $loop->iteration-1 }}" @if($loop->iteration == 1) class="active" @endif></li>
                                            @endforeach
                                        </ol>
                                    @endif
                                    <div class="carousel-inner" style="height: auto" role="listbox">
                                        @foreach ($project->images as $image)
                                            <div class="carousel-item @if($loop->iteration == 1) active @endif">
                                                <img class="img-fluid" src="{{asset('storage/'.$image->path)}}" alt="{{$project->name}}" />
                                            </div>
                                        @endforeach
                                    </div>

                                    @if (count($project->images) > 1)

                                        <a class="carousel-control-prev" href="#carousel-keyboard-{{ $loop->iteration}}" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                            <span class="sr-only">{{ trans('admin.Previous') }}</span>
                                        </a>
                                        <a class="carousel-control-next" href="#carousel-keyboard-{{ $loop->iteration}}" role="button" data-slide="next">
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                            <span class="sr-only">{{ trans('admin.Next') }}</span>
                                        </a>
                                        
                                    @endif
                                </div>
                        </div>
                    </div>
                <!-- Projetc images ends -->
                <hr>

                @endforeach
                <!-- basic info Tab ends -->
            </div>
        </div>
    </div>
</section>