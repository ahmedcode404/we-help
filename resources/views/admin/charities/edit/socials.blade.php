<form id="socials_form" class="socials_form" action="{{route('admin.charities.update', $data['charity']->id)}}" method="POST">
    @csrf
    @method('PUT')

    <input type="hidden" id="edit" value="true">

    <div class="row">
        <div class="col-lg-4 col-md-6 form-group">
            <label for="website">{{trans('admin.website')}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">
                        <i data-feather="chrome" class="font-medium-2"></i>
                    </span>
                </div>
                <input id="website" type="text" class="form-control" name="website" value="{{$data['charity']->website ? $data['charity']->website : old('website')}}" placeholder="https://www.example.com/" aria-describedby="basic-addon3"/>
                <span class="error-input error-website"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 form-group">
            <label for="facebook_link">{{trans('admin.facebook_link')}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon4">
                        <i data-feather="facebook" class="font-medium-2"></i>
                    </span>
                </div>
                <input id="facebook_link" type="text" name="facebook_link" class="form-control" value="{{$data['charity']->facebook_link ? $data['charity']->facebook_link : old('facebook_link')}}" placeholder="https://www.facebook.com/" aria-describedby="basic-addon4" />
                <span class="error-input error-facebook_link"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 form-group">
            <label for="twitter_link">{{trans('admin.twitter_link')}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon5">
                        <i data-feather="twitter" class="font-medium-2"></i>
                    </span>
                </div>
                <input id="twitter_link" type="text" name="twitter_link" class="form-control" value="{{$data['charity']->twitter_link ? $data['charity']->twitter_link : old('twitter_link')}}" placeholder="https://www.twitter.com/" aria-describedby="basic-addon5" />
                <span class="error-input error-twitter_link"></span>
            </div>
        </div>

        <div class="col-12 error text-center general_error"></div>
        <div class="col-12 mt-2" id="loading">
            <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_socials_form_button">{{trans('admin.save_changes')}}</button>
            <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
        </div>
    </div>
</form>