<form id="bank_data_form" class="bank_data_form" action="{{route('admin.charities.update', $data['charity']->id)}}" method="POST">
    @csrf
    @method('PUT')

    <input type="hidden" id="edit" value="true">

    <div class="row">

        <div class="col-lg-4 col-md-6 form-group">
            <label for="iban">{{trans('admin.iban')}}</label>
            <input id="iban" type="text" class="form-control" name="iban" value="{{$data['charity']->iban ? $data['charity']->iban : old('iban')}}" placeholder="{{trans('admin.iban')}}" aria-describedby="basic-addon3" required />
            <span class="error-input error-iban"></span>
        </div>

        <div class="col-lg-4 col-md-6 form-group">
            <label for="bank_name">{{trans('admin.bank_name')}}</label>
            <input type="text" name="bank_name" id="bank_name" class="form-control" value="{{$data['charity']->bank_name ? $data['charity']->bank_name : old('bank_name')}}" placeholder="{{trans('admin.bank_name')}}" required>
            <span class="error-input error-bank_name"></span>
        </div>

        <div class="col-lg-4 col-md-6 form-group">
            <label for="bank_address">{{trans('admin.bank_adress')}}</label>
            <input id="bank_address" type="text" class="form-control" name="bank_address" value="{{$data['charity']->bank_address ? $data['charity']->bank_address : old('bank_address')}}" placeholder="{{trans('admin.bank_adress')}}" aria-describedby="basic-addon3" required />
            <span class="error-input error-bank_adress"></span>
        </div>

        <div class="col-lg-4 col-md-6 form-group">
            <div class="form-group" data-aos="fade-in">
                <label for="bank_phone">{{__('web.phone')}}</label>
                <input type="text" class="form-control" name="bank_phone" id="bank_phone" value="{{$data['charity']->bank_phone ? $data['charity']->bank_phone : old('bank_phone')}}" placeholder="{{__('web.phone')}}" required>
                <span class="error-input error-bank_phone"></span>
            </div>
        </div>
        
        <div class="col-lg-4 col-md-6 form-group">
            <label for="bank_city">{{trans('admin.city')}}</label>
            <input id="bank_city" type="text" class="form-control" name="bank_city" value="{{$data['charity']->bank_city ? $data['charity']->bank_city : old('bank_city')}}" placeholder="{{trans('admin.city')}}" aria-describedby="basic-addon3" required />
            <span class="error-input error-bank_city"></span>
        </div>

        <div class="col-lg-4 col-md-6 form-group">
            <label for="swift_code">{{trans('admin.swift_code')}}</label>
            <input id="swift_code" type="text" class="form-control" name="swift_code" value="{{$data['charity']->swift_code ? $data['charity']->swift_code : old('swift_code')}}" placeholder="{{trans('admin.swift_code')}}" aria-describedby="basic-addon3" required />
            <span class="error-input error-swift_code"></span>
        </div>

        <div class="col-12 error text-center general_error"></div>
        <div class="col-12 mt-2" id="loading">
            <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_bank_data_button">{{trans('admin.save_changes')}}</button>
            <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
        </div>
    </div>
</form>