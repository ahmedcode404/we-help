﻿<form id="basic_info_form" class="basic_info_form" action="{{route('admin.charities.update', $data['charity']->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <input type="hidden" id="edit" value="true">

    <!-- charity edit media object start -->
    <div class="media mb-2 align-items-center">
        <img src="{{asset('storage/'.$data['charity']->logo)}}" alt="{{trans('admin.logo')}}" class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" style="max-height:70px" width="70" />
        <div class="media-body mt-50">
            <h4>{{$data['charity']->name}}</h4>
            <div class="col-12 d-flex mt-1 px-0">
                <label class="btn btn-primary mr-75 mb-0" for="logo">
                    <span>{{trans('admin.change')}}</span>
                    <input class="form-control" type="file" name="logo" id="logo" hidden accept="image/png, image/jpeg, image/jpg" />
                </label>
            </div>
            <span class="error-input error-logo"></span>
            <span>(Max size: 20MB)</span>
        </div>
    </div>
    <!-- charity edit media object ends -->
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="name">{{trans('admin.charity_name')}}</label>
                <input type="text" class="form-control" placeholder="{{trans('admin.name')}}" value="{{$data['charity']->name ? $data['charity']->name : old('name')}}" name="name" id="name" required />
                <span class="error-input error-name"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="email">{{trans('admin.email')}}</label>
                <input type="email" class="form-control" placeholder="{{trans('admin.email')}}" value="{{$data['charity']->email ? $data['charity']->email : old('email')}}" name="email" id="email" required />
                <span class="error-input error-email"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="type">{{trans('admin.charity_type')}}</label>
                <select class="form-control" id="type" name="type">
                    <option value="" disabled>{{trans('admin.select')}}</option>
                    <option value="foundation" @if($data['charity']->type == 'foundation' || old('type') == 'foundation') selected @endif>{{trans('admin.foundation')}}</option>
                    <option value="organization" @if($data['charity']->type == 'organization' || old('type') == 'organization') selected @endif>{{trans('admin.organization')}}</option>
                    <option value="charity" @if($data['charity']->type == 'charity' || old('type') == 'charity') selected @endif>{{trans('admin.charity')}}</option>
                </select>
                <span class="error-input error-type"></span>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label for="status">{{trans('admin.charity_status')}}</label>
                <select id="status" class="form-control" name="status">
                    <option value="waiting" @if($data['charity']->status == 'waiting' || old('status') == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                    <option value="approved" @if($data['charity']->status == 'approved' || old('status') == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                    <option value="rejected" @if($data['charity']->status == 'rejected' || old('status') == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                    <option value="hold" @if($data['charity']->status == 'hold' || old('status') == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                </select>
                <span class="error-input error-status"></span>
            </div>
        </div>
        
        <div class="col-md-2">
            <div class="form-group">
                <a class="btn btn_02 btn-outline-info" data-toggle="modal" data-target="#info">{{trans('admin.notes')}}</a>
            </div>
            <!-- Modal -->
            <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="notes">{{trans('admin.notes')}}</label>
                                <textarea id="notes" class="form-control" name="notes" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$data['charity']->notes ? $data['charity']->notes : old('notes')}}</textarea>
                                <span class="error-input error-notes"></span>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="save_status" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="country">{{trans('admin.country')}}</label>
                <select id="country" class="form-control select2" name="country" required>
                    <option value="" disabled>{{trans('admin.select')}}</option>
                    @foreach ($data['countries'] as $key => $country)
                        <option value="{{$key}}" @if($data['charity']->country == $key || old('country') == $key) selected @endif>{{$country}}</option>
                    @endforeach
                </select>
                <span class="error-input error-country"></span>
                {{-- <select id="country_id" class="form-control" name="country_id" data-action="{{ route('admin.city.country') }}" required>
                    <option value="" disabled>{{trans('admin.select')}}</option>
                    @foreach ($data['countries'] as $country)
                        @if($country->parent_id == null)
                            <option value="{{$country->id}}" @if($data['charity']->country_id == $country->id || old('country_id') == $country->id) selected @endif>{{$country->name}}</option>
                        @endif
                    @endforeach
                </select> --}}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="">{{trans('admin.city')}}</label>
                <input type="text" name="city" value="{{$data['charity']->city ? $data['charity']->city : old('city')}}" class="form-control">                                         
                <span class="error-input error-city"></span>
            </div>
        </div>
        {{-- <div class="col-md-4 append-cities">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">{{trans('admin.city')}}</label>
                    <select name="city_id" class="form-control">
                        <option value=""></option> 
                        @foreach($data['cities'] as $city) 
                            <option value="{{ $city->id }}"  {{ $data['charity']->city_id == $city->id ? 'selected' : old('city_id') }}>{{ $city->name }}</option>
                        @endforeach  
                    </select>                                           
                </div>
            </div>
        </div> --}}
        {{-- <div class="col-md-4">
            <div class="form-group">
                <label for="city_id">{{trans('admin.city')}}</label>
                <select id="city_id" class="form-control" name="city_id" required>
                    <option value="" disabled>{{trans('admin.select')}}</option>
                    @foreach ($data['countries'] as $city)
                        @if($city->parent_id != null)
                            <option value="{{$city->id}}" @if($data['charity']->city_id == $city->id || old('city_id') == $city->id) selected @endif>{{$city->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div> --}}
        <div class="col-md-4">
            <div class="form-group">
                <label for="phone">{{trans('admin.phone')}}</label>
                <input type="tel" class="form-control" value="{{$data['charity']->phone ? $data['charity']->phone : old('phone')}}" name="phone_num" placeholder="{{trans('admin.phone')}}" id="phone" />
                <span class="error-input error-phone"></span>
                <span class="error-input error-phone_num"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="mobile">{{trans('admin.mobile')}}</label>
                <input type="tel" class="form-control" value="{{$data['charity']->mobile ? $data['charity']->mobile : old('mobile')}}" name="mobile_num" placeholder="{{trans('admin.mobile')}}" id="mobile" />
                <span class="error-input error-mobile"></span>
                <span class="error-input error-mobile_num"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="address_ar">{{trans('admin.charity_address_ar')}}</label>
                <input type="text" class="form-control" placeholder="{{trans('admin.charity_address_ar')}}" value="{{$data['charity']->address_ar ? $data['charity']->address_ar : old('address_ar')}}" name="address_ar" id="address_ar" required />
                <span class="error-input error-address_ar"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="address_en">{{trans('admin.charity_address_en')}}</label>
                <input type="text" class="form-control" placeholder="{{trans('admin.charity_address_en')}}" value="{{$data['charity']->address_en ? $data['charity']->address_en : old('address_en')}}" name="address_en" id="address_en" required />
                <span class="error-input error-address_en"></span>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="charity_category_id">{{trans('admin.category')}}:</label>
                @if(count($data['categories']) > 0)
                    <select class="form-control select2" id="charity_category_id" name="charity_category_ids[]" multiple required>
                        @foreach ($data['categories'] as $category)
                            <option value="{{$category->id}}" @if(old('charity_category_id') == $category->id || $data['charity']->categories->contains('id', $category->id)) selected @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                    <span class="error-input error-category"></span>
                @else  
                    <br>{{trans('admin.no_data')}} 
                @endif
            </div>
        </div>
        
        <div class="col-12 error text-center general_error"></div>
        <div class="col-12 mt-2" id="loading">
            <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_basic_info_button">{{trans('admin.save')}}</button>
            <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
        </div>
    </div>
</form>