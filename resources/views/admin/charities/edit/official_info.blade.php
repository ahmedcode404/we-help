<form id="official_info_data" class="official_info_data" action="{{route('admin.charities.update', $data['charity']->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <input type="hidden" id="edit" value="true">
    
    <div class="row mt-1">
        <div class="col-12">
            {{-- <h4 class="mb-1">
                <i data-feather="user" class="font-medium-4 mr-25"></i>
                <span class="align-middle">{{trans('admin.official_info')}}</span>
            </h4> --}}
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="license_number">{{trans('admin.license_number')}}</label>
                <input id="license_number" type="number" min="0" minlength="5" maxlength="12" step="1" class="form-control" value="{{$data['charity']->license_number ? $data['charity']->license_number : old('license_number')}}" name="license_number" placeholder="{{trans('admin.license_number')}}" required />
                <span class="error-input error-license_number"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="license_start_date">{{trans('admin.license_start_date')}}</label>
                <input id="license_start_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{$data['charity']->license_start_date ? $data['charity']->license_start_date : old('license_start_date')}}" name="license_start_date" placeholder="{{$data['charity']->license_start_date}}" required />
                <span class="error-input error-license_start_date"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="license_end_date">{{trans('admin.license_end_date')}}</label>
                <input id="license_end_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{$data['charity']->license_end_date ? $data['charity']->license_end_date : old('license_end_date')}}" name="license_end_date" placeholder="{{$data['charity']->license_end_date}}" required />
                <span class="error-input error-license_end_date"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="license_file">{{trans('admin.license_file')}}</label>
                <input id="license_file" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="license_file" placeholder="{{trans('admin.license_file')}}" />
                <span>(Max size: 50MB)</span>
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->license_file)}}" target="_blank">{{trans('admin.show')}}</a></div>
                <span class="error-input error-license_file"></span>
            </div>
            <div class="media align-items-center">
                {{-- <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="establish_date">{{trans('admin.establish_date')}}</label>
                <input id="establish_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{$data['charity']->establish_date ? $data['charity']->establish_date : old('establish_date')}}" name="establish_date" placeholder="{{$data['charity']->establish_date}}" required />
                <span class="error-input error-establish_date"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="branches_num">{{trans('admin.branches_num')}}</label>
                <input id="branches_num" type="number" min="0" step="1" class="form-control" placeholder="{{trans('admin.branches_num')}}" value="{{$data['charity']->branches_num ? $data['charity']->branches_num : old('branches_num')}}" name="branches_num" required />
                <span class="error-input error-branches_num"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="members_num">{{trans('admin.members_num')}}</label>
                <input id="members_num" type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.members_num')}}" value="{{$data['charity']->members_num ? $data['charity']->members_num : old('members_num')}}" name="members_num" required />
                <span class="error-input error-members_num"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="contract_date">{{trans('admin.contract_date')}}</label>
                <input id="contract_date" type="text" class="form-control date-picker" readonly  autocomplete="off" name="contract_date" placeholder="{{trans('admin.contract_date')}}" value="{{$data['charity']->contract_date ? $data['charity']->contract_date : old('contract_date')}}" required readonly />
                <span class="error-input error-contract_date"></span>
                @if($data['charity']->contract_after_sign)
                        {{-- <label for="contract_date">{{trans('admin.show_contract')}}</label>
                        <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                        <div class="media-body"><a href="{{asset('storage/'.$data['charity']->contract_after_sign)}}">{{trans('admin.show')}}</a></div>
                @endif
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="attach">{{trans('admin.attach_text')}}</label>
                <input id="attach" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="attach" placeholder="{{trans('admin.attach')}}" />
                <span>(Max size: 50MB)</span>
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->attach)}}" target="_blank">{{trans('admin.show')}}</a></div>
                <span class="error-input error-attach"></span>
            </div>
            <div class="media align-items-center">
                {{-- <label for="contract_date">{{trans('admin.show_attach')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="year_report">{{trans('admin.year_report')}}</label>
                <input id="year_report" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="year_report" placeholder="{{trans('admin.year_report')}}" value="{{$data['charity']->year_report ? $data['charity']->year_report : old('year_report')}}" />
                <span>(Max size: 50MB)</span>
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->year_report)}}" target="_blank">{{trans('admin.show')}}</a></div>
                <span class="error-input error-year_report"></span>
            </div>
            <div class="media align-items-center">
                {{-- <label for="contract_date">{{trans('admin.show_year_report')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="years">{{trans('admin.for')}} {{trans('admin.last')}}</label>
                <select id="languages" class="form-control" name="years">
                    <option value="one" @if($data['charity']->years == 'one' || old('years') == 'one') selected @endif>{{trans('admin.year')}}</option>
                    <option value="two" @if($data['charity']->years == 'two' || old('years') == 'two') selected @endif>{{trans('admin.2')}} {{trans('admin.years')}}</option>
                    <option value="three" @if($data['charity']->years == 'three' || old('years') == 'three') selected @endif>{{trans('admin.3')}} {{trans('admin.years')}}</option>
                </select>
                <span class="error-input error-years"></span>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="internal_image">{{trans('admin.internal_image')}}</label>
                <input id="internal_image" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="internal_image" placeholder="{{trans('admin.internal_image')}}" value="{{$data['charity']->internal_image ? $data['charity']->internal_image : old('internal_image')}}" />
                <span>(Max size: 50MB)</span>
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->internal_image)}}" target="_blank">{{trans('admin.show')}}</a></div>
                <span class="error-input error-internal_image"></span>
            </div>
            <div class="media align-items-center">
                {{-- <label for="contract_date">{{trans('admin.show_internal_image')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="work_fields">{{trans('admin.charity_work_fields')}}</label>
                <textarea id="work_fields" rows="6" class="form-control" name="work_fields" placeholder="{{trans('web.work_fields')}}" required >{{$data['charity']->work_fields ? $data['charity']->work_fields : old('work_fields')}}</textarea>
                <span class="error-input error-work_fields"></span>
                {{-- <select name="work_fields" id="work_fields" class="form-control" multiple>
                    @foreach ($data['work_fields'] as $field)
                        <option value="{{$field->id}}" @if($data['charity']->work_fields->contains('id', $field->id)) selected @endif>{{$field->name}}</option>
                    @endforeach
                </select> --}}
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="strategy_ar">{{trans('admin.charity_strategy_ar')}}</label>
                <textarea id="strategy_ar" class="form-control" name="strategy_ar" placeholder="{{trans('admin.charity_strategy_ar')}}" >{{$data['charity']->strategy_ar ? $data['charity']->strategy_ar : old('strategy_ar')}}</textarea>
                <span class="error-input error-strategy_ar"></span>
            </div>
        </div>

        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="strategy_en">{{trans('admin.charity_strategy_en')}}</label>
                <textarea id="strategy_en" class="form-control" name="strategy_en" placeholder="{{trans('admin.charity_strategy_en')}}" required >{{$data['charity']->strategy_en ? $data['charity']->strategy_en : old('strategy_en')}}</textarea>
                <span class="error-input error-strategy_en"></span>
            </div>
        </div>

        <div class="col-12 error text-center general_error"></div>
        <div class="col-12 mt-2" id="loading">
            <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_official_info_data_button">{{trans('admin.save')}}</button>
            <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
        </div>
    </div>
</form>