<form id="representer_form" class="representer_form" action="{{route('admin.charities.update', $data['charity']->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <input type="hidden" id="edit" value="true">

    <div class="row mt-1">
        <div class="col-12">
            {{-- <h4 class="mb-1">
                <i data-feather="user" class="font-medium-4 mr-25"></i>
                <span class="align-middle">{{trans('admin.representer')}}</span>
            </h4> --}}
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_name_ar">{{trans('admin.representer_name_ar')}}</label>
                <input id="representer_name_ar" type="text" class="form-control" value="{{$data['charity']->representer_name_ar ? $data['charity']->representer_name_ar : old('representer_name_ar')}}" name="representer_name_ar" placeholder="{{trans('admin.representer_name_ar')}}" />
                <span class="error-input error-representer_name_ar"></span>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_name_en">{{trans('admin.representer_name_en')}}</label>
                <input id="representer_name_en" type="text" class="form-control" value="{{$data['charity']->representer_name_en ? $data['charity']->representer_name_en : old('representer_name_en')}}" name="representer_name_en" placeholder="{{trans('admin.representer_name_en')}}" required />
                <span class="error-input error-representer_name_en"></span>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_title_ar">{{trans('admin.representer_title_ar')}}</label>
                <input id="representer_title_ar" type="text" class="form-control" value="{{$data['charity']->representer_title_ar ? $data['charity']->representer_title_ar : old('representer_title_ar')}}" name="representer_title_ar" placeholder="{{trans('admin.representer_title_ar')}}" />
                <span class="error-input error-representer_title_ar"></span>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_title_en">{{trans('admin.representer_title_en')}}</label>
                <input id="representer_title_en" type="text" class="form-control" value="{{$data['charity']->representer_title_en ? $data['charity']->representer_title_en : old('representer_title_en')}}" name="representer_title_en" placeholder="{{trans('admin.representer_title_en')}}" required />
                <span class="error-input error-representer_title_en"></span>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_title_file">{{trans('admin.representer_title_file')}} ({{trans('admin.representer_title_text')}})</label>
                <input id="representer_title_file" type="file" accept=".jpeg,.jpg,.png,.pdf" class="form-control" name="representer_title_file" placeholder="{{trans('admin.representer_title_file')}}" />
                <span>(Max size: 50MB)</span>
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_title_file)}}" target="_blank">{{trans('admin.show')}}</a></div>
                <span class="error-input error-representer_title_file"></span>
            </div>
            {{-- <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
            </div> --}}
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_email">{{trans('admin.representer_email')}}</label>
                <input id="representer_email" type="text" class="form-control" value="{{$data['charity']->representer_email ? $data['charity']->representer_email : old('representer_email')}}" name="representer_email" placeholder="{{trans('admin.representer_email')}}" required />
                <span class="error-input error-representer_email"></span>
            </div>
        </div>
      
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_passport_image">{{trans('admin.representer_passport_image')}} ({{trans('admin.representer_title_text')}})</label>
                <input id="representer_passport_image" type="file" accept=".png,.jpg,.jpeg,.pdf" class="form-control" name="representer_passport_image" placeholder="{{trans('admin.representer_passport_image')}}" />
                <span>(Max size: 50MB)</span>
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_passport_image)}}">{{trans('admin.show')}}</a></div>
                <span class="error-input error-representer_passport_image"></span>
            </div>
            {{-- <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_passport_image)}}">{{trans('admin.show')}}</a></div>
            </div> --}}
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_nation_image">{{trans('admin.representer_nation_image')}} ({{trans('admin.representer_title_text')}})</label>
                <input id="representer_nation_image" type="file" accept=".png,.jpg,.jpeg,.pdf" class="form-control" name="representer_nation_image" placeholder="{{trans('admin.representer_nation_image')}}" />
                <span>(Max size: 50MB)</span>
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_nation_image)}}">{{trans('admin.show')}}</a></div>
                <span class="error-input error-representer_nation_image"></span>
            </div>
            {{-- <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$data['charity']->representer_nation_image)}}">{{trans('admin.show')}}</a></div>
            </div> --}}
        </div>

        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="international_member" value="1" name="international_member" @if($data['charity']->international_member == 1) checked @endif />
                    <label class="custom-control-label" for="international_member">{{trans('admin.international_member')}}</label>
                    <span class="error-input error-international_member"></span>
                </div>
            </div>
        </div>

        <div class="col-12 @if($data['charity']->international_member == 0) hidden @endif" id="international_name_container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="form-group">
                    <label for="international_name_ar">{{trans('admin.international_name_ar')}}</label>
                    <input type="string" class="form-control" name="international_name_ar" id="international_name_ar" placeholder="{{trans('admin.international_name_ar')}}" value="{{$data['charity']->international_name_ar ? $data['charity']->international_name_ar : old('international_name_ar')}}">
                    <span class="error-input error-international_name_ar"></span>
                </div>
            </div>

            <div class="col-lg-6 col-md-6">
                <div class="form-group">
                    <label for="international_name_en">{{trans('admin.international_name_en')}}</label>
                    <input type="string" class="form-control" name="international_name_en" id="international_name_en" placeholder="{{trans('admin.international_name_en')}}" value="{{$data['charity']->international_name_en ? $data['charity']->international_name_en : old('international_name_en')}}" required>
                    <span class="error-input error-international_name_en"></span>
                </div>
            </div>
        </div>
        </div>

        <div class="col-12 error text-center general_error"></div>
        <div class="col-12  mt-2" id="loading">
            <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_representer_form_button">{{trans('admin.save')}}</button>
            <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
        </div>
    </div>
</form>