@extends('layout.app')

@section('title', trans('admin.show-charity'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section class="app-user-view">

        <input type="hidden" id="edit" value="true">
        
        @php
            $session_currency = currencySymbol(session('currency'));
        @endphp
        <!-- Charity Card & Plan Starts -->
        <div class="row">
            <!-- Charity Card starts-->
            @include('admin.charities.show.basic_info')
            <!-- /Charity Card Ends-->

            {{-- @if($data['charity']->status == 'approved') --}}
                <!-- charity categories Card starts-->
                @include('admin.charities.show.categories')
                <!-- /charity categories CardEnds -->

                <!-- charity previous projects Card starts-->
                <div class="col-12">

                    @include('admin.charities.show.prev_projects')
                </div>
                <!-- /charity previous projects CardEnds -->
            {{-- @endif --}}
            
        <!-- Charity Card & Plan Ends -->

        <!-- Charity Timeline & Permissions Starts -->

            <!-- information starts -->
            @include('admin.charities.show.official_info')
            <!-- information Ends -->
            
             <!-- Charity signature Starts-->
            @include('admin.charities.show.signature')
            <!-- /Charity signature Ends-->

            <!-- charity Representer data Starts -->
            @include('admin.charities.show.representer')
            <!-- Charity Representer data Ends -->

            <!-- charity Strategy Starts -->
            @include('admin.charities.show.strategy')
            <!-- Charity Strategy Ends -->

            <!-- Banl Account Data Starts -->
            @include('admin.charities.show.bank_account')
            <!-- Banl Account Data Ends -->
            <!-- User Timeline & Permissions Ends -->

        @if($data['charity']->status == 'approved')

            <!-- Charity projects Starts-->
            <div class="invoice-list-wrapper">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-2">{{trans('admin.projects')}}</h4>
                        </div>
                        @php
                            $session_currency = currencySymbol(session('currency'));
                        @endphp
                        <div class="card-datatable table-responsive">
                            <table  class="invoice-list-table datatables-basic table  text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('admin.project_num')}}</th>
                                        <th>{{trans('admin.project_name')}}</th>
                                        <th>{{$session_currency}} {{trans('admin.project_cost')}}</th>
                                        <th>{{trans('admin.superadmin_ratio')}}</th>
                                        <th>{{trans('admin.actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $projects = $data['charity']->projects()->where('profile', 0)->get();
                                    @endphp
                                    @forelse ($projects as $project)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$project->project_num}}</td>
                                            <td>{{$project->name}}</td>
                                            <td>{{generalExchange($project->get_total, $project->currency->symbol, $session_currency)}} {{$session_currency}}</td>
                                            <td>{{$project->category ? $project->category->superadmin_ratio.' %' : 0.0.' %'}}</td>
                                            <td>
                                              
                                                @if(checkPermissions(['show_project']))
                                                    <a  href="{{route('admin.projects.show', $project->id)}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                        {{-- <span>{{trans('admin.show')}}</span> --}}
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['edit_project_data']))
                                                    <a  href="{{route('admin.projects.edit', $project->id)}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                        {{-- <span>{{trans('admin.edit')}}</span> --}}
                                                    </a>
                                                @endif
                                                {{-- @if(checkPermissions(['delete_project']))
                                                    <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.projects.destroy', $project->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                        <span>{{trans('admin.delete')}}</span>
                                                    </a>
                                                @endif --}}
                                                 
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">{{trans('admin.no_data')}}</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                   </div>
                    </div>
            </div>
            <!-- /Charity projects Ends-->

            <!-- Charity reports Starts-->
            <div class="invoice-list-wrapper">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-2">{{trans('admin.reports')}}</h4>
                        </div>
                        <div class="card-datatable table-responsive">
                            <table class="invoice-list-table datatables-basic table  text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('admin.project_name')}}</th>
                                        <th>{{trans('admin.phase_name')}}</th>
                                        <th>{{trans('admin.report_status')}}</th>
                                        <th>{{trans('admin.actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data['charity']->reports as $report)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$report->project->name}}</td>
                                            <td>{{$report->phase ? $report->phase->name : trans('admin.project_report')}}</td>
                                            <td>
                                                <span id="current_status-{{$report->id}}">{{trans('admin.'.$report->status)}}</span>
                                                <!-- Modal -->  
                                                <div class="modal fade modal-info text-left" id="report-{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">

                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <label for="change_report_status-{{$report->id}}">{{trans('admin.select_status')}}</label>
                                                                <select id="change_report_status-{{$report->id}}" class="form-control">
                                                                    <option value="waiting" @if($report->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                    <option value="approved" @if($report->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                    <option value="rejected" @if($report->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                    <option value="hold" @if($report->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                                </select>
                                                            </div>
            
                                                            <div class="form-group">
                                                                <label for="report_notes-{{$report->id}}">{{trans('admin.notes')}}</label>
                                                                <textarea id="report_notes-{{$report->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" id="" class="btn btn-info save_status" data-model="report" data-id="{{$report->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST"><div class="spinner-border  processing-div-{{ $report->id}}" role="status"></div>{{trans('admin.save')}}</button>
                                                        </div>
                                                    </div>
                                                </div>                                                            
                                                </div>
                                            </td>
                                            <td>
                                              
                                                        <a  href="{{route('admin.reports.show', $report->id)}}">
                                                            <i data-feather="eye" class="mr-50"></i>
                                                            {{-- <span>{{trans('admin.show')}}</span> --}}
                                                        </a>
                                                        <a  href="{{route('admin.reports.edit', $report->id)}}">
                                                            <i data-feather="edit-2" class="mr-50"></i>
                                                            {{-- <span>{{trans('admin.edit')}}</span> --}}
                                                        </a>
                                                        <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.reports.destroy', $report->id)}}">
                                                            <i data-feather="trash" class="mr-50"></i>
                                                            {{-- <span>{{trans('admin.delete')}}</span> --}}
                                                        </a>
                                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#report-{{$report->id}}" title="{{trans('admin.change')}}"><i data-feather="more-vertical" class="mr-50"></i></a>
            
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">{{trans('admin.no_data')}}</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
            <!-- Charity reports Ends-->


            <!-- Charity edit_requests Starts-->
            <div class="invoice-list-wrapper">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.edit_requests')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="invoice-list-table datatables-basic table  text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('admin.request_type')}}</th>
                                    <th>{{trans('admin.request_status')}}</th>
                                    <th>{{trans('admin.notes')}}</th>
                                    <th>{{trans('admin.actions')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($data['charity']->edit_requests as $req)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{trans('admin.'.$req->type)}}</td>
                                    <td>
                                        <span id="current_status">{{trans('admin.'.$req->status)}}</span>
                                        <!-- Modal -->
                                        <div class="modal fade modal-info text-left" id="req_status-{{$req->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                              
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="change_edit_status-{{$req->id}}">{{trans('admin.select_status')}}</label>
                                                            <select id="change_edit_status-{{$req->id}}" class="form-control">
                                                                <option value="waiting" @if($req->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                <option value="approved" @if($req->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                <option value="rejected" @if($req->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                <option value="hold" @if($req->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                            </select>
                                                        </div>
    
                                                        <div class="form-group">
                                                            <label for="edit_notes-{{$req->id}}">{{trans('admin.notes')}}</label>
                                                            <textarea id="edit_notes-{{$req->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                         <button type="button" id="" class="btn btn-info save_status" data-model="{{ $req->type == 'project' ? 'edit_request_project' : 'edit_request_charity' }}" data-id="{{$req->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST"> <div class="spinner-border  processing-div-{{ $req->id }}" role="status"></div> {{trans('admin.save')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @if($req->notes)
                                            <a  data-toggle="modal" data-target="#edit_request">{{trans('admin.show')}}</a>
                                            <!-- Modal -->
                                            <div class="modal fade modal-info text-left" id="edit_request" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            {!! $req->notes !!}
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                            __
                                        @endif
                                    </td>
                                    <td>
                                    <a href="javascript:void(0);" data-toggle="modal" data-target="#req_status-{{$req->id}}" title="{{trans('admin.change')}}"><i data-feather="edit-2" class="mr-50"></i></a>
                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.requests.destroy', $req->id)}}">
                                        <i data-feather="trash" class="mr-50"></i>
                                        {{-- <span>{{trans('admin.delete')}}</span> --}}
                                    </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">{{trans('admin.no_data')}}</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>         
            <!-- /Charity edit_requests Ends-->

            <!-- Charity financial_requests Starts-->
            <div class="invoice-list-wrapper">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-2">{{trans('admin.financial_requests')}}</h4>
                        </div>
                        <div class="card-datatable table-responsive">
                            <table class="invoice-list-table datatables-basic table  text-center">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('admin.request_type')}}</th>
                                        <th>{{trans('admin.request_date')}}</th>
                                        <th>{{trans('admin.request_status')}}</th>
                                        {{-- <th>{{trans('admin.notes')}}</th> --}}
                                        <th>{{trans('admin.actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($data['charity']->exchange_requests as $req)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{trans('admin.'.$req->type)}}</td>
                                            <td>{{$req->created_at->format('Y-m-d')}}</td>
                                            <td>
                                                <span id="current_status">{{trans('admin.'.$req->status)}}</span>
                                                <!-- Modal -->
                                                <div class="modal fade modal-info text-left" id="finanace_req-{{$req->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label for="change_fin_status-{{$req->id}}">{{trans('admin.select_status')}}</label>
                                                                    <select id="change_fin_status-{{$req->id}}" class="form-control">
                                                                        <option value="waiting" @if($req->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                        <option value="approved" @if($req->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                        <option value="rejected" @if($req->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                        <option value="hold" @if($req->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                                    </select>
                                                                </div>
            
                                                                <div class="form-group">
                                                                    <label for="fin_notes-{{$req->notes}}">{{trans('admin.notes')}}</label>
                                                                    <textarea id="fin_notes-{{$req->notes}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" id="" class="btn btn-info save_status" data-model="fin_request" data-id="{{$req->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                       
                                            <td>
                                                @if($req->notes)
                                                <a href="javascript:void(0);" data-toggle="modal" data-target="#info" title="{{trans('admin.notes')}}"><i data-feather="eye" class="mr-50"></i></a>
                                                <!-- Modal -->
                                                <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {!! $req->notes !!}
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
            
                                            <a  href="javascript:void(0);" data-toggle="modal" data-target="#finanace_req-{{$req->id}}" title="{{trans('admin.change')}}"><i data-feather="more-vertical" class="mr-50"></i></a>
            
                                                {{-- <div class="dropdown">
                                                    <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                        <i data-feather="more-vertical"></i>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        {{-- <a class="dropdown-item" href="{{route('admin.projects.show', $project->id)}}">
                                                            <i data-feather="eye" class="mr-50"></i>
                                                            <span>{{trans('admin.show')}}</span>
                                                        </a>
                                                        <a class="dropdown-item" href="{{route('admin.projects.edit', $project->id)}}">
                                                            <i data-feather="edit-2" class="mr-50"></i>
                                                            <span>{{trans('admin.edit')}}</span>
                                                        </a>
                                                        <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.projects.destroy', $project->id)}}">
                                                            <i data-feather="trash" class="mr-50"></i>
                                                            <span>{{trans('admin.delete')}}</span>
                                                        </a> --}}
                                                    {{-- </div>
                                                </div>  --}}
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">{{trans('admin.no_data')}}</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
        </div>

            <!-- /Charity financial_requests Ends-->

            <!-- Charity work_fields Starts-->
            {{-- @include('admin.charities.show.work_fields') --}}
            <!-- /Charity work_fields Ends-->
        @endif
    </section>

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush