<table class="datatables-basic table">
    <thead>
        <tr>
            <th>{{trans('admin.id')}}</th>
            <th>{{trans('admin.name')}}</th>
            <th>{{trans('admin.phone')}}</th>
            <th>{{trans('admin.mobile')}}</th>
            <th>{{trans('admin.email')}}</th>
            <th>{{trans('admin.license_number')}}</th>
            <th>{{trans('admin.license_start_date')}}</th>
            <th>{{trans('admin.license_end_date')}}</th>
            <th>{{trans('admin.establish_date')}}</th>
            <th>{{trans('admin.branches_num')}}</th>
            <th>{{trans('admin.members_num')}}</th>
            <th>{{trans('admin.contract_date')}}</th>
            <th>{{trans('admin.address')}}</th>
            <th>{{trans('admin.country')}}</th>
            <th>{{trans('admin.city')}}</th>
            <th>{{trans('admin.representer_name')}}</th>
            <th>{{trans('admin.representer_title')}}</th>
            <th>{{trans('admin.representer_email')}}</th>
            <th>{{trans('admin.international_member')}}</th>
            <th>{{trans('admin.international_name')}}</th>
            <th>{{trans('admin.website')}}</th>
            <th>{{trans('admin.twitter_link')}}</th>
            <th>{{trans('admin.facebook_link')}}</th>
            <th>{{trans('admin.iban')}}</th>
            <th>{{trans('admin.bank_name')}}</th>
            <th>{{trans('admin.bank_address')}}</th>
            <th>{{trans('admin.bank_city')}}</th>
            <th>{{trans('admin.bank_phone')}}</th>
            <th>{{trans('admin.swift_code')}}</th>
            <th>{{trans('admin.status')}}</th>
            <th>{{trans('admin.contract_date')}}</th>
            <th>{{trans('admin.categories')}}</th>
            <th>{{trans('admin.projects_count')}}</th>
            <th>{{trans('admin.edit_requests')}}</th>
            <th>{{trans('admin.exchange_requests')}}</th>
            <th>{{trans('admin.reports')}}</th>
            <th>{{trans('admin.total_out_to_charity')}}</th> {{-- $charity->getTotalOutToCharity() --}}
            <th>{{trans('admin.total_bills_out_to_charity')}}</th> {{-- $charity->getTotalCharityExchangeVouchers() --}}
            <th>{{trans('admin.employees')}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data['charities'] as $charity)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$charity->name}}</td>
                <td>{{$charity->phone}}</td>
                <td>{{$charity->mobile}}</td>
                <td>{{$charity->email}}</td>
                <td>{{$charity->license_number}}</td>
                <td>{{$charity->license_start_date}}</td>
                <td>{{$charity->license_end_date}}</td>
                <td>{{$charity->establish_date}}</td>
                <td>{{$charity->branches_num}}</td>
                <td>{{$charity->members_num}}</td>
                <td>{{$charity->contract_date}}</td>
                <td>{{$charity->address}}</td>
                <td>{{Countries::getOne($charity->country, \App::getLocale())}}</td>
                <td>{{$charity->city}}</td>
                <td>{{$charity->representer_name}}</td>
                <td>{{$charity->representer_title}}</td>
                <td>{{$charity->representer_email}}</td>
                <td>{{$charity->international_member == 1 ? trans('admin.yes') : trans('admin.no')}}</td>
                <td>{{$charity->international_name ? $charity->international_name : '__'}}</td>
                <td>{{$charity->website}}</td>
                <td>{{$charity->twitter_link}}</td>
                <td>{{$charity->facebook_link}}</td>
                <td>{{$charity->iban}}</td>
                <td>{{$charity->bank_name}}</td>
                <td>{{$charity->bank_address}}</td>
                <td>{{$charity->bank_city}}</td>
                <td>{{$charity->bank_phone}}</td>
                <td>{{$charity->swift_code}}</td>
                <td>
                    @if($charity->status == 'approved')
                        <span class="badge badge-light-success">{{trans('admin.'.$charity->status)}}</span>
                    @elseif($charity->status == 'waiting')
                        <span class="badge badge-light-warning">{{trans('admin.'.$charity->status)}}</span>
                    @elseif($charity->status == 'hold')
                        <span class="badge badge-light-info">{{trans('admin.'.$charity->status)}}</span>
                    @elseif($charity->status == 'rejected')
                        <span class="badge badge-light-danger">{{trans('admin.'.$charity->status)}}</span>
                    @endif
                </td>
                <td>{{$charity->contract_date}}</td>
                <td>
                    @foreach ($charity->categories as $category)
                        {{ $category->name }} <br>
                    @endforeach
                </td>
                <td>{{count($charity->projects)}}</td>
                <td>{{count($charity->edit_requests)}}</td>
                <td>{{count($charity->exchange_requests)}}</td>
                <td>{{count($charity->reports)}}</td>
                <td>{{ $charity->getTotalOutToCharity() }}</td>
                <td>{{$charity->getTotalCharityExchangeVouchers()}}</td>
                <td>{{count($charity->employees)}}</td>
            </tr>
        @endforeach
    </tbody>
</table>