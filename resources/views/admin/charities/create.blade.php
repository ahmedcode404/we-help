@extends('layout.app')

@section('title', trans('admin.create_charity'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
    
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" />    
@endpush

@section('content')

    <!-- charities edit start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.create_charity')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="create_charities" method="POST" class="submit_form" action="{{route('admin.charities.store')}}">
                            @csrf

                            <input type="hidden" name="admin_add_charity" value="true">

                            <h4 class="card-title">{{trans('admin.basic_info')}}</h4>
                            <div class="row">
                                {{-- name --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="name">{{trans('admin.charity_name')}}</label>
                                        <input type="text" class="form-control" placeholder="{{trans('admin.charity_name')}}" value="{{old('name')}}" name="name" id="name" required />
                                        <span class="error-input error-name"></span>
                                    </div>
                                </div>

                                {{-- email --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="email">{{trans('admin.email')}}</label>
                                        <input type="email" class="form-control" placeholder="{{trans('admin.email')}}" value="{{old('email')}}" name="email" id="email" required />
                                        <span class="error-input error-email"></span>
                                    </div>
                                </div>

                                {{-- phone --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="phone">{{trans('admin.phone')}}</label>
                                        <input type="tel" class="form-control" value="{{old('phone')}}" name="phone_num" placeholder="{{trans('admin.phone')}}" id="phone" />
                                        <span class="error-input error-phone"></span>
                                    </div>
                                </div>

                                {{-- mobile --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="mobile">{{trans('admin.mobile')}}</label>
                                        <input type="tel" class="form-control" value="{{old('mobile')}}" name="mobile_num" placeholder="{{trans('admin.mobile')}}" id="mobile" />
                                        <span class="error-input error-mobile"></span>
                                    </div>
                                </div>

                                {{-- address_ar --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address_ar">{{trans('admin.charity_address_ar')}}</label>
                                        <input type="text" class="form-control" placeholder="{{trans('admin.charity_address_ar')}}" value="{{old('address_ar')}}" name="address_ar" id="address_ar" required />
                                        <span class="error-input error-address_ar"></span>
                                    </div>
                                </div>

                                {{-- address_en --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="address_en">{{trans('admin.charity_address_en')}}</label>
                                        <input type="text" class="form-control" placeholder="{{trans('admin.charity_address_en')}}" value="{{old('address_en')}}" name="address_en" id="address_en" required />
                                        <span class="error-input error-address_en"></span>
                                    </div>
                                </div>

                                {{-- logo --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="logo">{{trans('admin.logo')}}</label>
                                        <input type="file" name="logo" id="logo" accept=".png,.jpeg,.jpeg" class="form-control" value="{{old('logo')}}" required>
                                        <span>(Max size: 20MB)</span>
                                        <span class="error-input error-logo"></span>
                                    </div>
                                </div>

                                {{-- charity_category_id --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="charity_category_id">{{trans('admin.category')}}:</label>
                                        @if(count($data['categories']) > 0)
                                            <select class="form-control select2" id="charity_category_id" name="charity_category_ids[]" multiple required>
                                                @foreach ($data['categories'] as $category)
                                                    <option value="{{$category->id}}" @if(old('charity_category_id') == $category->id) selected @endif>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            <span class="error-input error-charity_category_id"></span>
                                        @else  
                                            <br>{{trans('admin.no_data')}} 
                                        @endif
                                    </div>
                                </div>

                                {{-- type --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="type">{{trans('admin.charity_type')}}</label>
                                        <select class="form-control" id="type" name="type">
                                            <option value="" disabled>{{trans('admin.select')}}</option>
                                            <option value="foundation" @if(old('type') == 'foundation') selected @endif>{{trans('admin.foundation')}}</option>
                                            <option value="organization" @if(old('type') == 'organization') selected @endif>{{trans('admin.organization')}}</option>
                                            <option value="charity" @if(old('type') == 'charity') selected @endif>{{trans('admin.charity')}}</option>
                                        </select>
                                        <span class="error-input error-type"></span>
                                    </div>
                                </div>

                                {{-- country --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="country">{{trans('admin.country')}}</label>
                                        <select id="country" class="form-control select2" name="country" required>
                                            <option value="" disabled>{{trans('admin.select')}}</option>
                                            @foreach ($data['countries'] as $key => $country)
                                                <option value="{{$key}}" @if(old('country') == $key) selected @endif>{{$country}}</option>
                                            @endforeach
                                        </select>
                                        <span class="error-input error-country"></span>
                                    </div>
                                </div>

                                {{-- city --}}
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">{{trans('admin.city')}}</label>
                                        <input type="text" name="city" value="{{old('city')}}" class="form-control">                                         
                                        <span class="error-input error-city"></span>
                                    </div>
                                </div>

                                         {{-- status --}}
                                         <div class="col-lg-2 col-md-4 col-sm-8">
                                            <div class="form-group mb-sm-1 mb-0">
                                                <label for="status">{{trans('admin.charity_status')}}</label>
                                                <select id="status" class="form-control" name="status">
                                                    <option value="waiting" @if(old('status') == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                    <option value="approved" @if(old('status') == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                    <option value="rejected" @if(old('status') == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                    <option value="hold" @if(old('status') == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                </select>
                                                <span class="error-input error-status"></span>
                                            </div>
                                        </div>
        
                                        {{-- status note --}}
                                        <div class="col-lg-2 col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <a class="btn btn_02 btn-outline-info" style="padding: 0.786rem 5px" data-toggle="modal" data-target="#info">{{trans('admin.add_note')}}</a>
                                            </div>
                                            <!-- Modal -->
                                            <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.add_note')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <textarea id="notes" class="form-control" name="notes" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{old('notes')}}</textarea>
                                                                <span class="error-input error-notes"></span>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" id="save_status" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                <div class="col-12">
                                    <hr>
                                    <h4 class="card-title">{{trans('admin.official_info')}}</h4>
                                </div>

                                {{-- license_number --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="license_number">{{trans('admin.license_number')}}</label>
                                        <input id="license_number" type="number" min="0" minlength="5" maxlength="12" step="1" class="form-control" value="{{old('license_number')}}" name="license_number" placeholder="{{trans('admin.license_number')}}" required />
                                        <span class="error-input error-license_number"></span>
                                    </div>
                                </div>

                                {{-- license_start_date --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="license_start_date">{{trans('admin.license_start_date')}}</label>
                                        <input id="license_start_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{old('license_start_date')}}" name="license_start_date" placeholder="{{now()->format('Y-m-d')}}" required />
                                        <span class="error-input error-license_start_date"></span>
                                    </div>
                                </div>

                                {{-- license_end_date --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="license_end_date">{{trans('admin.license_end_date')}}</label>
                                        <input id="license_end_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{old('license_end_date')}}" name="license_end_date" placeholder="{{now()->format('Y-m-d')}}" required />
                                        <span class="error-input error-license_end_date"></span>
                                    </div>
                                </div>

                                {{-- license_file --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="license_file">{{trans('admin.license_file')}}</label>
                                        <input id="license_file" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="license_file" placeholder="{{trans('admin.license_file')}}" required />
                                        <span>(Max size: 50MB)</span>
                                        <span class="error-input error-license_file"></span>
                                    </div>
                                </div>

                                {{-- establish_date --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="establish_date">{{trans('admin.establish_date')}}</label>
                                        <input id="establish_date" type="text" class="form-control date-picker" readonly  autocomplete="off" value="{{old('establish_date')}}" name="establish_date" placeholder="{{now()->format('Y-m-d')}}" required />
                                        <span class="error-input error-establish_date"></span>
                                    </div>
                                </div>

                                {{-- branches_num --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="branches_num">{{trans('admin.branches_num')}}</label>
                                        <input id="branches_num" type="number" min="0" step="1" class="form-control" placeholder="{{trans('admin.branches_num')}}" value="{{old('branches_num')}}" name="branches_num" required />
                                        <span class="error-input error-branches_num"></span>
                                    </div>
                                </div>

                                {{-- members_num --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="members_num">{{trans('admin.members_num')}}</label>
                                        <input id="members_num" type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.members_num')}}" value="{{old('members_num')}}" name="members_num" required />
                                        <span class="error-input error-members_num"></span>
                                    </div>
                                </div>

                                {{-- contract_date --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="contract_date">{{trans('admin.contract_date')}}</label>
                                        <input id="contract_date" type="text" class="form-control" autocomplete="off" name="contract_date" placeholder="{{trans('admin.contract_date')}}" value="{{now()->format('Y-m-d')}}" required readonly />
                                        <span class="error-input error-contract_date"></span>
                                    </div>
                                </div>

                                {{-- contract_after_sign --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="contract_after_sign">{{trans('admin.contract_after_sign')}}</label>
                                        <input id="contract_after_sign" type="file" class="form-control" accept=".pdf" placeholder="{{trans('admin.contract_after_sign')}}" value="{{old('contract_after_sign')}}" name="contract_after_sign" required />
                                        <span>(Max size: 50MB)</span>
                                        <span class="error-input error-contract_after_sign"></span>
                                    </div>
                                </div>

                                {{-- attach --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="attach">{{trans('admin.attach')}}</label>
                                        <input id="attach" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="attach" placeholder="{{trans('admin.attach')}}" required />
                                        <span>(Max size: 50MB)</span>
                                        <span class="error-input error-attach"></span>
                                    </div>
                                </div>

                                {{-- year_report --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="year_report">{{trans('admin.year_report')}}</label>
                                        <input id="year_report" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="year_report" placeholder="{{trans('admin.year_report')}}" value="{{old('year_report')}}" required/>
                                        <span>(Max size: 50MB)</span>
                                        <span class="error-input error-year_report"></span>
                                    </div>
                                </div>

                                {{-- years --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="years">{{trans('admin.for')}} {{trans('admin.last')}}</label>
                                        <select id="languages" class="form-control" name="years">
                                            <option value="one" @if(old('years') == 'one') selected @endif>{{trans('admin.year')}}</option>
                                            <option value="two" @if(old('years') == 'two') selected @endif>{{trans('admin.2')}} {{trans('admin.years')}}</option>
                                            <option value="three" @if(old('years') == 'three') selected @endif>{{trans('admin.3')}} {{trans('admin.years')}}</option>
                                        </select>
                                        <span class="error-input error-years"></span>
                                    </div>
                                </div>

                                {{-- internal_image --}}
                                <div class="col-lg-4 col-md-6">
                                    <div class="form-group">
                                        <label for="internal_image">{{trans('admin.internal_image')}}</label>
                                        <input id="internal_image" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="internal_image" placeholder="{{trans('admin.internal_image')}}" value="{{old('internal_image')}}" required />
                                        <span>(Max size: 50MB)</span>
                                        <span class="error-input error-internal_image"></span>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <hr>
                                    <h4 class="card-title">{{trans('admin.representer_data')}}</h4>
                                </div>

                                {{-- representer_name_ar --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_name_ar">{{trans('admin.representer_name_ar')}}</label>
                                        <input id="representer_name_ar" type="text" class="form-control" value="{{old('representer_name_ar')}}" name="representer_name_ar" placeholder="{{trans('admin.representer_name_ar')}}" />
                                        <span class="error-input error-representer_name_ar"></span>
                                    </div>
                                </div>

                                {{-- representer_name_en --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_name_en">{{trans('admin.representer_name_en')}}</label>
                                        <input id="representer_name_en" type="text" class="form-control" value="{{old('representer_name_en')}}" name="representer_name_en" placeholder="{{trans('admin.representer_name_en')}}" required />
                                        <span class="error-input error-representer_name_ar"></span>
                                    </div>
                                </div>

                                {{-- representer_title_ar --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_title_ar">{{trans('admin.representer_title_ar')}}</label>
                                        <input id="representer_title_ar" type="text" class="form-control" value="{{old('representer_title_ar')}}" name="representer_title_ar" placeholder="{{trans('admin.representer_title_ar')}}" />
                                        <span class="error-input error-representer_title_ar"></span>
                                    </div>
                                </div>

                                {{-- representer_title_en --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_title_en">{{trans('admin.representer_title_en')}}</label>
                                        <input id="representer_title_en" type="text" class="form-control" value="{{old('representer_title_en')}}" name="representer_title_en" placeholder="{{trans('admin.representer_title_en')}}" required />
                                        <span class="error-input error-representer_title_en"></span>
                                    </div>
                                </div>

                                {{-- representer_title_text --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_title_file">{{trans('admin.representer_title_file')}} ({{trans('admin.representer_title_text')}})</label>
                                        <input id="representer_title_file" type="file" accept=".jpeg,.jpg,.png,.pdf" class="form-control" name="representer_title_file" placeholder="{{trans('admin.representer_title_file')}}" required />
                                        <span>(Max size: 50MB)</span>
                                        <span class="error-input error-representer_title_file"></span>
                                    </div>
                                </div>

                                {{-- representer_email --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_email">{{trans('admin.representer_email')}}</label>
                                        <input id="representer_email" type="text" class="form-control" value="{{old('representer_email')}}" name="representer_email" placeholder="{{trans('admin.representer_email')}}" required />
                                        <span class="error-input error-representer_email"></span>
                                    </div>
                                </div>

                                {{-- representer_passport_image --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_passport_image">{{trans('admin.representer_passport_image')}}</label>
                                        <input id="representer_passport_image" type="file" accept=".png,.jpg,.jpeg,.pdf" class="form-control" name="representer_passport_image" placeholder="{{trans('admin.representer_passport_image')}}" required />
                                        <span class="error-input error-representer_passport_image"></span>
                                    </div>
                                </div>

                                {{-- representer_nation_image --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="representer_nation_image">{{trans('admin.representer_nation_image')}}</label>
                                        <input id="representer_nation_image" type="file" accept=".png,.jpg,.jpeg,.pdf" class="form-control" name="representer_nation_image" placeholder="{{trans('admin.representer_nation_image')}}" required />
                                        <span>(Max size: 50MB)</span>
                                        <span class="error-input error-representer_nation_image"></span>
                                    </div>
                                </div>

                                {{-- international_member --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="international_member" value="1" name="international_member" />
                                            <label class="custom-control-label" for="international_member">{{trans('admin.international_member')}}</label>
                                            <span class="error-input error-international_member"></span>
                                        </div>
                                    </div>
                                </div>

                                {{-- international_name --}}
                                <div class="hidden" id="international_name_container">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <label for="international_name_ar">{{trans('admin.international_name_ar')}}</label>
                                                <textarea name="international_name_ar" class="form-control" id="international_name_ar" cols="30" rows="6" placeholder="{{trans('admin.international_name_ar')}}">{{ old('international_name_ar')}}</textarea>
                                                <span class="error-input error-international_name_ar"></span>
                                            </div>
                                        </div>
                            
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group">
                                                <label for="international_name_en">{{trans('admin.international_name_en')}}</label>
                                                <textarea name="international_name_en" class="form-control" id="international_name_en" cols="30" rows="6" placeholder="{{trans('admin.international_name_en')}}">{{ old('international_name_en')}}</textarea>
                                                <span class="error-input error-international_name_en"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <hr>
                                    <h4 class="card-title">{{trans('admin.bank_data')}}</h4>
                                </div>

                                {{-- bank_name --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="bank_name">{{trans('admin.bank_name')}}</label>
                                    <input type="text" name="bank_name" id="bank_name" class="form-control" value="{{old('bank_name')}}" placeholder="{{trans('admin.bank_name')}}" required>
                                    <span class="error-input error-bank_name"></span>
                                </div>

                                {{-- bank_adress --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="bank_address">{{trans('admin.bank_adress')}}</label>
                                    <input id="bank_address" type="text" class="form-control" name="bank_address" value="{{old('bank_address')}}" placeholder="{{trans('admin.bank_adress')}}" aria-describedby="basic-addon3" required />
                                    <span class="error-input error-bank_adress"></span>
                                </div>

                                {{-- bank_phone --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <div class="form-group" data-aos="fade-in">
                                        <label for="bank_phone">{{trans('admin.bank_phone')}}</label>
                                        <input type="text" class="form-control" name="bank_phone" id="bank_phone" value="{{old('bank_phone')}}" placeholder="{{__('web.phone')}}" required>
                                        <span class="error-input error-bank_phone"></span>
                                    </div>
                                </div>

                                {{-- bank_city --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="bank_city">{{trans('admin.bank_city')}}</label>
                                    <input id="bank_city" type="text" class="form-control" name="bank_city" value="{{old('bank_city')}}" placeholder="{{trans('admin.city')}}" aria-describedby="basic-addon3" required />
                                    <span class="error-input error-bank_city"></span>
                                </div>

                                {{-- swift_code --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="swift_code">{{trans('admin.swift_code')}}</label>
                                    <input id="swift_code" type="text" class="form-control" name="swift_code" value="{{old('swift_code')}}" placeholder="{{trans('admin.swift_code')}}" aria-describedby="basic-addon3" required />
                                    <span class="error-input error-swift_code"></span>
                                </div>

                                {{-- iban --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="iban">{{trans('admin.iban')}}</label>
                                    <input id="iban" type="text" class="form-control" name="iban" value="{{old('iban')}}" placeholder="{{trans('admin.iban')}}" aria-describedby="basic-addon3" required />
                                    <span class="error-input error-iban"></span>
                                </div>

                                <div class="col-12">
                                    <hr>
                                    <h4 class="card-title">{{trans('admin.socials')}}</h4>
                                </div>

                                {{-- website --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="website">{{trans('admin.website')}}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon3">
                                                <i data-feather="chrome" class="font-medium-2"></i>
                                            </span>
                                        </div>
                                        <input id="website" type="text" class="form-control" name="website" value="{{ old('website')}}" placeholder="https://www.example.com/" aria-describedby="basic-addon3"/>
                                    </div>
                                </div>

                                {{-- facebook_link --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="facebook_link">{{trans('admin.facebook_link')}}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon4">
                                                <i data-feather="facebook" class="font-medium-2"></i>
                                            </span>
                                        </div>
                                        <input id="facebook_link" type="text" name="facebook_link" class="form-control" value="{{old('facebook_link')}}" placeholder="https://www.facebook.com/" aria-describedby="basic-addon4" />
                                    </div>
                                </div>

                                {{-- twitter_link --}}
                                <div class="col-lg-4 col-md-6 form-group">
                                    <label for="twitter_link">{{trans('admin.twitter_link')}}</label>
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon5">
                                                <i data-feather="twitter" class="font-medium-2"></i>
                                            </span>
                                        </div>
                                        <input id="twitter_link" type="text" name="twitter_link" class="form-control" value="{{old('twitter_link')}}" placeholder="https://www.twitter.com/" aria-describedby="basic-addon5" />
                                    </div>
                                </div>

                                <div class="col-12">
                                    <hr>
                                    <h4 class="card-title">{{trans('admin.prev_projects')}}</h4>
                                </div>

                                
                                <div class="col-md-12">
                                    <div class="image_increment" >
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 mb-1">
                                                <input type="text" name="projects_names[]" id="names" class="form-control" placeholder="{{ trans('admin.project_name') }}">
                                            </div>
                                            <div class="col-lg-4 col-md-3 col-sm-6 mb-1">
                                                <input type="text" name="projects_types[]" id="types" class="form-control" placeholder="{{ trans('admin.type') }}">
                                            </div>
                                            <div class="col-lg-3 col-md-3 col-sm-6 mb-1">
                                                <input type="file" name="images[]" class="form-control" accept=".gif, .jpg, .png, .webp">
                                                <span>(Max size: 20MB)</span>
                                                <div class="invalid-feedback">
                                                    {{trans('admin.image')}}
                                                </div>
                                            </div>
                                        <div class="col-lg-1 col-md-2 text-right">
                                                <button class="btn btn-success img-btn-success" type="button"><i data-feather="plus" aria-hidden="true"></i></button>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="image_clone  hidden">
                                        <div  class="control-group" style="margin-top:10px">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 mb-1">
                                                    <input type="text" name="projects_names[]" id="names" class="form-control" placeholder="{{ trans('admin.project_name') }}" value="{{ old('names') }}">
                                                </div>
                                                <div class="col-lg-4 col-md-3 col-sm-6 mb-1">
                                                    <input type="text" name="projects_types[]" id="types" class="form-control" placeholder="{{ trans('admin.type') }}" value="{{ old('types') }}">
                                                </div>
                                                <div class="col-lg-3 col-md-3 col-sm-6 mb-1">
                                                    <input type="file" name="images[]" class="form-control" accept=".gif, .jpg, .png, .webp">
                                                    <span>(Max size: 20MB)</span>
                                                </div>
                                            <div class="col-lg-1 col-md-2 text-right">
                                                <button class="btn btn-danger img-btn-danger" type="button"><i  data-feather="minus" aria-hidden="true"></i></button>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            

                                {{-- work_fields --}}
                                <div class="col-lg-12 col-md-12">
                                    <div class="form-group">
                                        <label for="work_fields">{{trans('admin.charity_work_fields')}}</label>
                                        <textarea id="work_fields" rows="6" class="form-control" name="work_fields" placeholder="{{trans('web.work_fields')}}" required >{{old('work_fields')}}</textarea>
                                        <span class="error-input error-work_fields"></span>
                                    </div>
                                </div>

                                {{-- strategy_ar --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="strategy_ar">{{trans('admin.charity_strategy_ar')}}</label>
                                        <textarea id="strategy_ar" class="form-control" name="strategy_ar" placeholder="{{trans('admin.charity_strategy_ar')}}" >{{old('strategy_ar')}}</textarea>
                                        <span class="error-input error-strategy_ar"></span>
                                    </div>
                                </div>

                                {{-- strategy_en --}}
                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group">
                                        <label for="strategy_en">{{trans('admin.charity_strategy_en')}}</label>
                                        <textarea id="strategy_en" class="form-control" name="strategy_en" placeholder="{{trans('admin.charity_strategy_en')}}" required >{{old('strategy_en')}}</textarea>
                                        <span class="error-input error-strategy_en"></span>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-12 error text-center general_error"></div>
                                <div class="col-12" id="loading">
                                    <button type="button" class="btn btn-primary submit_button">{{trans('admin.save')}}</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- charities edit ends -->

@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>

        @if(\App::getLocale() == 'ar')
            <!--for arabic-only-->
            <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
            <!--end-->
        @endif

    <script>
        //datepicker
        $(function () {
            $('.date-picker').datepicker({
                changeYear: true,
                dateFormat: 'yy-mm-dd',
            })
        });
    </script>

    <script src="{{asset('dashboard/js/intlTelInput.min.js')}}" type="text/javascript"></script>
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

    <script>

        // ckeditor custom
    
        CKEDITOR.replace( 'strategy_ar' );   
        CKEDITOR.replace( 'strategy_en' );   
        CKEDITOR.replace( 'international_name_ar' );
        CKEDITOR.replace( 'international_name_en' );
        
    </script>

    <script src="{{asset('dashboard/forms/charities/create.js')}}"></script>
    <script src="{{asset('dashboard/forms/main/delete-image.js')}}"></script>
@endpush