@extends('layout.app')

@section('title', trans('admin.update_setting_uk'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

@push('css')
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" />    
@endpush


    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                {{--@if($errors->any())
                    {{ implode('', $errors->all('<div>:message</div>')) }}
                @endif--}}           
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit_setting_uk')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="form-setting-uk"  method="POST" action="{{route('admin.setting.update', $nation_id)}}" enctype="multipart/form-data">
                            @csrf
                        
                                <input type="hidden" name="nation_id" id="nation_id" value="{{$nation_id}}">

                                <div class="row">
                                    @foreach($settings as $setting_uk)
    
                                        <!-- ----------------------------------------- type text --------------------------------------->
                                        
                                        <!-- BEGIN : type text -->
                                        @if($setting_uk->type == 'text')
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                    <input type="text" class="form-control" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" value="{{ $setting_uk->value }}" placeholder="{{ trans($setting_uk->neckname) }}" required />
                                                    @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif                             
                                                </div> 
                                            </div>                           
                                        @endif
                                        <!-- END : type text -->


                                        <!-- END : type hidden -->
                                        @if($setting_uk->type == 'hidden')

                                            <input type="hidden" value="{{$setting_uk->value}}" name="{{$setting_uk->key}}" id="{{$setting_uk->key}}">
                        
                                        @endif
                                        <!-- END : type hidden -->                                        


                                        <!-- END : type text -->                                        

                                        @if($setting_uk->key == 'latitude_uk')
                                        <div class="col-md-12">
                                            <div class="form-group" style="position: relative">
                                                <label class="form-label">{{ __('admin.location')  }}</label>
                                                <input type="text" class="form-control" id="location" name="address_uk" value="{{ getSettingValue('address', getNationId())->value }}" placeholder="{{ __('admin.location')  }}">
                                                 <i class="fa fa-map-marker-alt" id="myloc" title="{{ trans('admin.select_location') }}"></i>
                                            <div id="map-{{$setting_uk->key}}" style="width:100%;height:500px;"></div> 
                                        </div>
                                        </div>
                                        @endif                                        

                                        <!-- ----------------------------------------- type select --------------------------------------->
                                        
                                        <!-- BEGIN : type select -->
                                        @if($setting_uk->type == 'select')
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                    <select name="{{ $setting_uk->key }}" id="{{ $setting_uk->key }}" class="form-control" required>
                                                        @foreach ($currencies as $currency)
                                                            <option value="{{$currency->id}}" @if($setting_uk->value == $currency->id) selected @endif>{{$currency->name}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif                             
                                                </div> 
                                            </div>                           
                                        @endif
                                        <!-- END : type select -->


                                        <!-- ----------------------------------------- type number --------------------------------------->
                                    
                                        <!-- BEGIN : type number -->
                                        @if($setting_uk->type == 'number')
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label class="form-label" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                    <input type="number" class="form-control" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" value="{{ $setting_uk->value }}" placeholder="{{ trans($setting_uk->neckname) }}" required />
                                                    @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif                             
                                                </div> 
                                            </div>                           
                                        @endif
                                        <!-- END : type number -->  

                                        <!-- ----------------------------------------- type phone --------------------------------------->
                                    
                                        <!-- BEGIN : type phone -->
                                        @if($setting_uk->type == 'phone')
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label class="form-label" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                    <input type="tel" class="form-control" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" value="{{ $setting_uk->value }}" placeholder="{{ trans($setting_uk->neckname) }}" required />
                                                    @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif                             
                                                </div> 
                                            </div>                           
                                        @endif
                                        <!-- END : type phone -->                                          

                                        <!-- ----------------------------------------- type email --------------------------------------->
                                    

                                        <!-- BEGIN : type email -->
                                        @if($setting_uk->type == 'email')
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                    <label class="form-label" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                    <input type="email" class="form-control" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" value="{{ $setting_uk->value }}" placeholder="{{ trans($setting_uk->neckname) }}" required />
                                                    @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif                             
                                                </div> 
                                            </div>                           
                                        @endif
                                        <!-- END : type email --> 

                                        <!-- ----------------------------------------- type url --------------------------------------->
                                    
                                        <!-- BEGIN : type url -->
                                        @if($setting_uk->type == 'url')
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="form-label" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                    <input type="text" class="form-control" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" value="{{ $setting_uk->value }}" placeholder="{{ trans($setting_uk->neckname) }}" required />
                                                    @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif                             
                                                </div> 
                                            </div>                           
                                        @endif
                                        <!-- END : type url -->                                                                                   

                                        <!-- ----------------------------------------- type file --------------------------------------->
                                    
                                        <!-- BEGIN : type file -->
                                        @if($setting_uk->type == 'file')
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>{{ trans($setting_uk->neckname) }}
                                                        @if($setting_uk->key == 'we_help_logo_uk_ar' || $setting_uk->key == 'we_help_logo_uk_en')
                                                            ( Max-height:75px / Max-width:180px )
                                                        @endif
                                                        @if($setting_uk->key == 'logo_uk_ar' || $setting_uk->key == 'logo_uk_en')
                                                            ( Max-height:80px )
                                                        @endif
                                                        @if($setting_uk->key == 'pages_header_uk')
                                                            ( Widht :1920px / Height: 600px)
                                                        @endif
                                                    </label>
                                                    <div class="custom-file">
                                                        <input type="file" class="form-control image" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" />
                                                        <small>(Max size: 20MB)</small>
                                                        @if($errors->has($setting_uk->key))
                                                            <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                        @endif 
                                                    </div>
                                                    <div class="form-group show-image" >
                                                        <img src="{{ url('storage/' . $setting_uk->value) }}" style="width: 100px" class="img-thumbnail preview-{{ $setting_uk->key }}" alt="">
                                                    </div>                    
                                                </div> 
                                            </div>                         
                                        @endif
                                        <!-- END : type file -->

                                        {{-- type:checkbox --}}
                                        @if($setting_uk->type == 'checkbox')
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="form-control custom-control-input" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" 
                                                    value="{{ $setting_uk->value }}" placeholder="{{ trans($setting_uk->neckname) }}"
                                                    @if($setting_uk->value == 1) checked @endif />
                                                    <label class="custom-control-label" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                  @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif  
                                                </div>                           
                                                </div> 
                                            </div>   
                                        @endif
                                        {{-- end type:checkbok --}}

                                        <!-- ----------------------------------------- type textarea --------------------------------------->
                                    
                                        <!-- BEGIN : type textarea -->
                                        @if($setting_uk->type == 'textarea')
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <label class="d-block" for="{{ $setting_uk->key }}">{{ trans($setting_uk->neckname) }}</label>
                                                    <textarea class="form-control" id="{{ $setting_uk->key }}" name="{{ $setting_uk->key }}" rows="3">{{ $setting_uk->value }}</textarea>
                                                    @if($errors->has($setting_uk->key))
                                                        <div class="error">{{ $errors->first($setting_uk->key) }}</div>
                                                    @endif                                                 </div>
                                            </div>                        
                                        @endif
                                        <!-- END : type textarea -->
                                                                                                                

                                    @endforeach
                                </div>

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" url="{{route('admin.setting.update', $nation_id)}}" class="btn btn-primary" name="submit" value="Submit"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/settings/update-uk.js')}}"></script>
        <script src="{{ url('custom/custom-validate.js') }}"></script>
        <script src="{{ url('custom/preview-image.js') }}"></script>
        <script src="{{ url('dashboard/js/intlTelInput.min.js')}}" type="text/javascript"></script>
        <script src="{{ url('dashboard/forms/settings/phone-uk-key.js')}}" type="text/javascript"></script>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdarVlRZOccFIGWJiJ2cFY8-Sr26ibiyY&libraries=places&callback=initAutocomplete&language=<?php echo e('ar'); ?>"
    defer></script>
        <script>

            // ckeditor custom
            

            CKEDITOR.replace( 'contract_intro_uk_en' );
            CKEDITOR.replace( 'contract_intro_uk_ar' );

            CKEDITOR.replace( 'we_help_desc_uk_en' );
            CKEDITOR.replace( 'we_help_desc_uk_ar' );            
  
        </script>

    @endpush
@endsection