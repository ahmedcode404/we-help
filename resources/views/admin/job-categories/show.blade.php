@extends('layout.app')

@section('title', trans('admin.show-job-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section id="basic-horizontal-layouts">

        <div class="row justify-content-md-center">

            {{-- job_category start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{$data['cat']->name}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.category_name_en')}}</th>
                                <th>{{trans('admin.category_name_ar')}}</th>
                                {{-- <th>{{trans('admin.nation')}}</th> --}}
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['cat']->name_en}}</td>
                                <td>{{$data['cat']->name_ar}}</td>
                                {{-- <td>{{$data['cat']->nation->code}}</td> --}}
                                <td>
                                @if(checkPermissions(['edit_job_cats']))
                                    <a  href="{{route('admin.job-categories.edit', $data['cat']->id)}}" title="{{trans('admin.edit')}}">
                                        <i data-feather="edit-2" class="mr-50"></i>
                                    </a>
                                @endif
                                     
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            {{-- job_category end --}}

            {{-- jobs start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.jobs')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.job_name')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data['cat']->jobs as $job)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$job->name}}</td>
                                        <td>
                                           
                                                    @if(checkPermissions(['show_job']))
                                                        <a  href="{{route('admin.jobs.show', $job->id)}}" title="{{trans('admin.show')}}">
                                                            <i data-feather="eye" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                                    @if(checkPermissions(['edit_job']))
                                                        <a  href="{{route('admin.jobs.edit', $job->id)}}" title="{{trans('admin.edit')}}">
                                                            <i data-feather="edit-2" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                                    @if(checkPermissions(['delete_job']))
                                                        <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.jobs.destroy', $job->id)}}" title="{{trans('admin.delete')}}">
                                                            <i data-feather="trash" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                        </td>
                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            {{-- jobs end --}}
        </div>

    </section>

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection