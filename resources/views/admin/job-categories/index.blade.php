@extends('layout.app')

@section('title', trans('admin.job-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_job_cats']))
                    <a href="{{route('admin.job-categories.create')}}" class="btn btn-primary">{{trans('admin.create-job-categories')}}</a>
                @endif
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.category_name_en')}}</th>
                            <th>{{trans('admin.category_name_ar')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['job-categories'] as $cat)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$cat->name_en}}</td>
                                <td>{{$cat->name_ar}}</td>
                                {{-- <td>{{$cat->nation->code}}</td> --}}
                                <td>
                                    @if(checkPermissions(['show_job_cats']))
                                        <a  href="{{route('admin.job-categories.show', $cat->id)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                    {{-- @else
                                        {{trans('admin.do_not_have_permission')}} --}}
                                    @endif
                             
                                    @if(checkPermissions(['edit_job_cats']) || checkPermissions(['delete_job_cats']))
                                       
                                                @if(checkPermissions(['edit_job_cats']))
                                                    <a  href="{{route('admin.job-categories.edit', $cat->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_job_cats']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.job-categories.destroy', $cat->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                          
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['job-categories']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection