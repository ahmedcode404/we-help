@extends('layout.app')

@section('title', trans('admin.edit-job-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit-job-categories')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="job-categories" method="POST" action="{{route('admin.job-categories.update', $data['cat']->id)}}">
                            @csrf
                            @method('PUT')

                            <div class="row">
                            <div class="form-group col-md-6">
                                <label class="form-label" for="name_ar">{{trans('admin.category_name_ar')}}</label>
                                <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{$data['cat']->name_ar == null ? old('name_ar') : $data['cat']->name_ar}}" placeholder="{{trans('admin.category_name_ar')}}" required />
                                @if($errors->has('name_ar'))
                                    <div class="error">{{ $errors->first('name_ar') }}</div>
                                @endif 
                            </div>
                            <div class="form-group col-md-6">
                                <label class="form-label" for="name_en">{{trans('admin.category_name_en')}}</label>
                                <input type="text" id="name_en" name="name_en" class="form-control" value="{{$data['cat']->name_en == null ? old('name_en') : $data['cat']->name_en}}" placeholder="{{trans('admin.category_name_en')}}" required />
                                @if($errors->has('name_en'))
                                    <div class="error">{{ $errors->first('name_en') }}</div>
                                @endif 
                            </div>

                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/job-categories/create.js')}}"></script>
    @endpush
@endsection