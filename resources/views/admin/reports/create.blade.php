@extends('layout.app')
@section('title')
{{ __('admin.add_report') }}
@endsection
@section('content')


    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">

            <!-- jQuery Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('admin.add_report') }}</h4>
                    </div>
                    <div class="card-body">

                        <form id="form-report" action="{{ route('admin.reports.store-report', $project_id) }}" method="post"
                                enctype="multipart/form-data" class="create_report_form">

                            @csrf

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="status_input">{{trans('admin.report_status')}}</label>
                                        <select id="status_input" class="form-control" name="status" required>
                                            <option value="waiting">{{trans('admin.waiting')}}</option>
                                            <option value="approved">{{trans('admin.approved')}}</option>
                                            <option value="rejected">{{trans('admin.rejected')}}</option>
                                            <option value="hold">{{trans('admin.hold')}}</option>
                                        </select>
                                        <span class="error error-status"></span>
                                    </div>
                                </div>
    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>{{ __('admin.images') }}</label>
                                        <div class="custom-file">
                                            <input type="file" class="form-control" id="images" name="images[]" accept=".gif, .jpg, .png, .webp" multiple />
                                            <small>(Max size: 20MB)</small>
                                            <span class="error error-images"></span>
                                        </div>
                                    </div>  
                                </div>
    
                            </div>

                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="report_ar">{{ __('admin.report_ar') }}</label>
                                        <textarea class="form-control" id="report_ar" name="report_ar" rows="3"></textarea>
                                        <span class="error error-report_ar"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="report_en">{{ __('admin.report_en') }}</label>
                                        <textarea class="form-control" id="report_en" name="report_en" rows="3"></textarea>
                                        <span class="error error-report_en"></span>
                                    </div>
                                </div>
 
                            </div>                                        
                            <!--  END: desc -->  


                            <!-- IMAGES -->
                            
                            
                            <!-- vedio -->
                            <div class="form-group">
                                <label>{{ __('admin.vedio') }}</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control" accept="video/mp4,video/ogg, video/webm" id="vedio" name="vedio" />
                                    <small>(Max size: 50MB)</small>
                                    <span class="error error-vedio"></span>
                                </div>
                                <div id="progress_bar" style="text-align: center;">
                                    <progress id="progressBar" value="0" max="100" style="width:1065px;display:none;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>                                
                                </div>

                            </div>

                        

                            <div class="row">
                                <div class="col-12 error text-center general_error"></div>
                                <div class="col-12" id="loading">
                                    <button type="button" id="click-form-report" class="btn btn-primary submit_create_report_form_button"> {{ __('admin.create') }} </button>
                                </div>                               
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /jQuery Validation -->


        </div>
    </section>
    <!-- /Validation -->





@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous"></script>

<script src="{{ url('dashboard/forms/reports/create.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>

    // ckeditor custom

    CKEDITOR.replace( 'report_ar' );
    CKEDITOR.replace( 'report_en' );
    

    
</script>

@endsection






