<div class="tab-pane active" id="basic_info" aria-labelledby="basic_info-tab" role="tabpanel">
    <form id="basic_info_form" method="POST" class="basic_info_form" action="{{route('admin.reports.update', $data['report']->id)}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <input type="hidden" id="edit" value="true">
        
        <div class="row align-items-center">

            {{-- charity_id start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="charity_id">{{trans('admin.charity_name')}}</label>
                    <select name="charity_id" id="charity_id" class="form-control" disabled required>
                        <option value="{{$data['report']->charity_id}}" checked>{{$data['report']->charity ? $data['report']->charity->name : trans('admin.no_data')}}</option>
                    </select>
                </div>
            </div>
            {{-- charity_id end --}}

            {{-- project_id start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="project_id">{{trans('admin.project_name')}}</label>
                    <select name="project_id" id="project_id" class="form-control" disabled required>
                        <option value="{{$data['report']->project_id}}" checked>{{$data['report']->project ? $data['report']->project->name : trans('admin.no_data')}}</option>
                    </select>
                </div>
            </div>
            {{-- project_id end --}}

            {{-- phase_id start --}}
            <div class="col-md-4">
                <div class="form-group">
                    <label for="phase_id">{{trans('admin.phase_name')}}</label>
                    <select name="phase_id" id="phase_id" class="form-control" disabled required>
                        <option value="{{$data['report']->phase_id ? $data['report']->phase_id : null}}" checked>{{$data['report']->phase ? $data['report']->phase->name : trans('admin.project_report')}}</option>
                    </select>
                </div>
            </div>
            {{-- phase_id end --}}

            {{-- report_ar start --}}
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="report_ar">{{ trans('admin.report_ar') }}</label>
                    <textarea class="form-control" id="report_ar" name="report_ar" rows="3" required>{{ $data['report']->report_ar }}</textarea>
                    <span class="error error-report_ar"></span>
                </div>
            </div>        
            {{-- report_ar end --}}

            {{-- report_en start --}}
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="report_en">{{ trans('admin.report_en') }}</label>
                    <textarea class="form-control" id="report_en" name="report_en" rows="3" required>{{ $data['report']->report_en }}</textarea>
                    <span class="error error-report_en"></span>
                </div>
            </div>        
            {{-- report_en end --}}

            {{-- status start --}}
            <div class="col-md-3">
                <div class="form-group">
                    <label for="status">{{trans('admin.report_status')}}</label>
                    <select id="status" class="form-control" name="status" required>
                        <option value="waiting" @if($data['report']->status == 'waiting' || old('status') == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                        <option value="approved" @if($data['report']->status == 'approved' || old('status') == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                        <option value="rejected" @if($data['report']->status == 'rejected' || old('status') == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                        <option value="hold" @if($data['report']->status == 'hold' || old('status') == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                    </select>
                    <span class="error error-status"></span>
                </div>
            </div>
            {{-- status end --}}

            {{-- notes start --}}
            <div class="col-md-3">
                    <a class="btn btn-outline-info mt-sm-1" data-toggle="modal" data-target="#info">{{trans('admin.notes')}}</a>
                <!-- Modal -->
                <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="notes">{{trans('admin.notes')}}</label>
                                    <textarea id="notes" class="form-control" name="notes" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$data['report']->notes ? $data['report']->notes : old('notes')}}</textarea>
                                    <span class="error error-notes"></span>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="save_status" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- notes end --}}

            {{-- vedio start --}}
            <div class="col-md-6">
                <div class="form-group">
                    <label for="vedio">{{ trans('admin.vedio') }}</label>
                    <input type="file" name="vedio" class="form-control" id="vedio" accept="video/mp4,video/ogg, video/webm">
                    <small>(Max size: 50MB)</small>
                </div>
                <div id="progress_bar" style="text-align: center;">
                    <progress id="progressBar" value="0" max="100" style="width:520px;display:none;"></progress>
                    <h3 id="status_up"></h3>
                    <p id="loaded_n_total"></p>                                
                </div>                
            </div> 
            <div class="col-md-6">
                <div class="form-group">
                    <video height="300px" style="max-width: 100%" controls>
                        <source src="{{asset('storage/'.$data['report']->vedio)}}" type="video/mp4">
                        <source src="{{asset('storage/'.$data['report']->vedio)}}" type="video/ogg">
                        <source src="{{asset('storage/'.$data['report']->vedio)}}" type="video/webm">
                    </video>
                </div>
            </div>
            <span class="error error-vedio"></span>
            {{-- vedio end --}}
      
            <div class="col-12 error text-center general_error"></div>
            <div class="col-12 mt-2" id="loading">
                <button type="button" class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_basic_info_form_button">{{trans('admin.save')}}</button>
            </div>
        </div>
    </form>
</div>