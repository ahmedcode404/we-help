@extends('layout.app')

@section('title', trans('admin.show-report'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section class="app-user-view">

        @if(count($data['report']->images) > 0)
            <!-- Phase images starts -->
            <div class="row">
                <div class="col-12">
                    <div class="card profile-header mb-2">
                        <div class="card-header">
                            <h4 class="card-title">{{trans('admin.report_images')}}</h4>
                        </div>
                        <div class="card-body">
                            <div id="carousel-keyboard" class="carousel slide" data-keyboard="true">
                                @if(count($data['report']->images) > 1)
                                    <ol class="carousel-indicators">
                                        @foreach ($data['report']->images as $image)
                                            <li data-target="#carousel-keyboard" data-slide-to="{{ $loop->iteration-1 }}" @if($loop->iteration == 1) class="active" @endif></li>
                                        @endforeach
                                    </ol>
                                @endif
                                <div class="carousel-inner" style="height: auto" role="listbox">
                                    @foreach ($data['report']->images as $image)
                                        <div class="carousel-item @if($loop->iteration == 1) active @endif">
                                            <img class="img-fluid"  src="{{asset('storage/'.$image->path)}}" alt="{{$data['report']->name}}" />
                                        </div>
                                    @endforeach
                                </div>

                                @if (count($data['report']->images) > 1)

                                    <a class="carousel-control-prev" href="#carousel-keyboard" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">{{ trans('admin.Previous') }}</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-keyboard" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">{{ trans('admin.Next') }}</span>
                                    </a>
                                    
                                @endif
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- Phase images ends -->
        @endif

       <!-- Report basic info Starts-->
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-2">{{trans('admin.basic_info')}}</h4>
                </div>
                <div class="card-body">
                    <div class="row invoice-list-wrapper">
                                <div class="card-datatable table-responsive">
                                    <table class="datatables-basic table">
                                        <thead>
                                            <tr>
                                                <th>{{trans('admin.id')}}</th>
                                                <th>{{trans('admin.charity')}}</th>
                                                <th>{{trans('admin.project')}}</th>
                                                <th>{{trans('admin.phase')}}</th>
                                                <th>{{trans('admin.status')}}</th>
                                                <th>{{trans('admin.actions')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1</td>
                                                <td>{{$data['report']->charity ? $data['report']->charity->name : ''}}</td>
                                                <td>{{$data['report']->project ? $data['report']->project->name : ''}}</td>
                                                <td>{{$data['report']->phase ? $data['report']->phase->name : ''}}</td>
                                                <td>
                                                    @if($data['report']->status == 'approved')
                                                        <span id="current_status-{{$data['report']->id}}" class="badge badge-light-success">{{trans('admin.'.$data['report']->status)}}</span>
                                                    @elseif($data['report']->status == 'waiting')
                                                        <span id="current_status-{{$data['report']->id}}" class="badge badge-light-warning">{{trans('admin.'.$data['report']->status)}}</span>
                                                    @elseif($data['report']->status == 'hold')
                                                        <span id="current_status-{{$data['report']->id}}" class="badge badge-light-info">{{trans('admin.'.$data['report']->status)}}</span>
                                                    @elseif($data['report']->status == 'rejected')
                                                        <span id="current_status-{{$data['report']->id}}" class="badge badge-light-danger">{{trans('admin.'.$data['report']->status)}}</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <a href="#" data-toggle="modal" data-target="#report-{{$data['report']->id}}" title="{{trans('admin.change')}}"><i data-feather="edit-2"></i></a>
                                                    <!-- Modal -->
                                                    <div class="modal fade modal-info text-left" id="report-{{$data['report']->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label for="change_report_status-{{$data['report']->id}}">{{trans('admin.select_status')}}</label>
                                                                        <select id="change_report_status-{{$data['report']->id}}" class="form-control">
                                                                            <option value="waiting" @if($data['report']->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                            <option value="approved" @if($data['report']->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                            <option value="rejected" @if($data['report']->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                            <option value="hold" @if($data['report']->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                                        </select>
                                                                    </div>
                
                                                                    <div class="form-group">
                                                                        <label for="report_notes-{{$data['report']->id}}">{{trans('admin.notes')}}</label>
                                                                        <textarea id="notes" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{$data['report']->notes ? $data['report']->notes : old('notes')}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-info save_status" data-model="report" data-id="{{$data['report']->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                </div>
            </div>
        <!-- Report basic info Ends-->

        {{-- report desc and video start --}}
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.outside_level')}}</h4>
                    </div>
                    <div class="card-body">
                        {!! $data['report']->report !!}
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.video')}}</h4>
                    </div>
                    <div class="card-body">
                        @if($data['report']->vedio)
                        <video height="300px" style="max-width: 100%" controls>
                            <source src="{{asset('storage/'.$data['report']->vedio)}}" type="video/mp4">
                                <source src="{{asset('storage/'.$data['report']->vedio)}}" type="video/ogg">
                                <source src="{{asset('storage/'.$data['report']->vedio)}}" type="video/webm">
                            </video>
                        @else
                            {{trans('admin.no_data')}}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        {{-- report desc and video end --}}

    </section>

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush