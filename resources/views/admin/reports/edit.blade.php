@extends('layout.app')

@section('title', trans('admin.edit-report'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!--Edit phase start -->
    <section class="app-user-edit">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center active" id="basic_info-tab" data-toggle="tab" href="#basic_info" aria-controls="basic_info" role="tab" aria-selected="true">
                            <i data-feather="user"></i><span>{{trans('admin.basic_info')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="images-tab" data-toggle="tab" href="#images" aria-controls="images" role="tab" aria-selected="false">
                            <i data-feather="image"></i><span>{{trans('admin.images')}}</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- Basic info Tab starts -->
                    @include('admin.reports.edit.basic_info')
                    <!-- Basic info Tab ends -->

                    <!-- images Tab starts -->
                    @include('admin.reports.edit.images')
                    <!-- images Tab ends -->
                </div>
            </div>
        </div>
    </section>
    <!-- Edit phase ends -->
@endsection

@push('js')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous"></script>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <script>
        
            // ckeditor custom
        
            CKEDITOR.replace( 'report_ar' );   
            CKEDITOR.replace( 'report_en' );
            
        </script>

        <script src="{{asset('dashboard/forms/reports/edit.js')}}"></script>
        <script src="{{asset('dashboard/forms/main/delete-image.js')}}"></script>
@endpush