@extends('layout.app')

@section('title', trans('admin.reports'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.charity_name')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.phase_name')}}</th>
                            <th>{{trans('admin.report_status')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['reports'] as $report)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$report->charity ? $report->charity->name : ''}}</td>
                                <td>{{$report->project ? $report->project->name : ''}}</td>
                                <td>{{$report->phase ? $report->phase->name : ''}}</td>
                                <td>
                                    @if($report->status == 'approved')
                                        <span id="current_status-{{$report->id}}" class="badge badge-light-success">{{trans('admin.'.$report->status)}}</span>
                                    @elseif($report->status == 'waiting')
                                        <span id="current_status-{{$report->id}}" class="badge badge-light-warning">{{trans('admin.'.$report->status)}}</span>
                                    @elseif($report->status == 'hold')
                                        <span id="current_status-{{$report->id}}" class="badge badge-light-info">{{trans('admin.'.$report->status)}}</span>
                                    @elseif($report->status == 'rejected')
                                        <span id="current_status-{{$report->id}}" class="badge badge-light-danger">{{trans('admin.'.$report->status)}}</span>
                                    @endif
                                    {{-- <span id="current_status-{{$report->id}}">{{trans('admin.'.$report->status)}}</span> --}}
                                    {{-- <a class="btn btn-primary" data-toggle="modal" data-target="#report-{{$report->id}}">{{trans('admin.change')}}</a>
                                    <!-- Modal -->  
                                    <div class="modal fade modal-info text-left" id="report-{{$report->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                  
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="change_report_status-{{$report->id}}">{{trans('admin.select_status')}}</label>
                                                    <select id="change_report_status-{{$report->id}}" class="form-control">
                                                        <option value="approved" @if($report->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                        <option value="rejected" @if($report->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                        <option value="hold" @if($report->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="report_notes-{{$report->id}}">{{trans('admin.notes')}}</label>
                                                    <textarea id="report_notes-{{$report->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                </div>
                                            </div>
                                            
                                            <div class="modal-footer">
                                                <button type="button" id="" class="btn btn-info save_status" data-model="report" data-id="{{$report->id}}" data-action="{{route('admin.main.change-status')}}" data-method="POST">{{trans('admin.save')}}</button>
                                            </div>
                                        </div>
                                    </div>                                                            
                                    </div> --}}
                                </td>
                               
                                <td>
                                    @if(checkPermissions(['show_report']))
                                    <a  href="{{route('admin.reports.show', $report->id)}}" title="{{trans('admin.show')}}">
                                        <i data-feather="eye" class="mr-50"></i>
                                    </a>
                                @endif

                                    @if(checkPermissions(['show_report']) || checkPermissions(['edit_report']) || checkPermissions(['delete_report']))
                                          
                                                @if(checkPermissions(['edit_report']))
                                                    <a  href="{{route('admin.reports.edit', $report->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_report']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.reports.destroy', $report->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['reports']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection