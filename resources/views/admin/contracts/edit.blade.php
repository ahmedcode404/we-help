@extends('layout.app')

@section('title', trans('admin.edit_contract'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit_contract')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="edit_contract" method="POST" action="">
                            @csrf
                            @method('PUT')

                            <!-- item -->
                            <input type="hidden" id="item" name="item" value="{{ $contract_one->id  }}" />


                            <!-- BEGIN : title -->
                            <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="title_ar">{{trans('admin.contract_title_ar')}}</label>
                                    <input type="text" class="form-control" id="title_ar" name="title_ar" value="{{ $contract_one->title_ar ? $contract_one->title_ar : old('title_ar') }}" placeholder="{{trans('admin.contract_title_ar')}}" required />
                                    <span class="error-input"></span>                                
                                </div>                            
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="title_en">{{trans('admin.contract_title_en')}}</label>
                                    <input type="text" class="form-control" id="title_en" name="title_en" value="{{ $contract_one->title_en ? $contract_one->title_en : old('title_en') }}" placeholder="{{trans('admin.contract_title_en')}}" required />
                                    <span class="error-input"></span>                                
                                </div> 
                            </div>                           
                            
                            </div>
                            <!-- END : title -->

                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="content_ar">{{ __('admin.contract_content_ar') }}</label>
                                        <textarea class="form-control" id="content_ar" name="content_ar" rows="3">{{ $contract_one->content_ar ? $contract_one->content_ar : old('content_ar') }}</textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="content_en">{{ __('admin.contract_content_en') }}</label>
                                        <textarea class="form-control" id="content_en" name="content_en" rows="3">{{ $contract_one->content_en ? $contract_one->content_en : old('content_en') }}</textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>  

                            </div>                                        
                            <!--  END: desc -->                            

                            <div class="row">
                                <div class="col-12">
                                    <button type="button" id="click_edit_contract" redirect="{{route('admin.contracts.index')}}" url="{{route('admin.contracts.update' , $contract_one->id)}}" class="btn btn-primary" name="submit" value="Submit"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')

        <script src="{{asset('dashboard/forms/contracts/edit.js')}}"></script>
        <script src="{{ url('custom/custom-validate.js') }}"></script>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <script>

            // ckeditor custom

            CKEDITOR.replace( 'content_ar' );
            CKEDITOR.replace( 'content_en' );
            
        </script>       
    @endpush

@endsection