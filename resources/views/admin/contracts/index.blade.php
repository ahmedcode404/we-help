@extends('layout.app')

@section('title', trans('admin.contracts'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['add_contract']))
                    <a href="{{route('admin.contracts.create')}}" class="btn btn-primary">{{trans('admin.create_contract')}}</a>
                @endif
                @foreach ($contracts as $contract)
                    <div class="card">
                        <div class="question-div">
                            <h4>{{$contract->title_ar}}</h4>
                            {!! $contract->content_ar !!}
                        </div>
                        <div class="question-div">
                            <h4>{{$contract->title_en}}</h4>
                            {!! $contract->content_en !!}
                        </div>

                        <div class="text-right">
                            @if(checkPermissions(['edit_contract']) || checkPermissions(['delete_contract']))
                                        
                                @if(checkPermissions(['edit_contract']))
                                    <a  href="{{route('admin.contracts.edit', $contract->id)}}" title="{{trans('admin.edit')}}">
                                        <i data-feather="edit-2" class="mr-50"></i>
                                    </a>
                                @endif
                                @if(checkPermissions(['delete_contract']))

                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.contracts.destroy', $contract->id)}}" title="{{trans('admin.delete')}}">
                                        <i data-feather="trash" class="mr-50"></i>
                                    </a>
                                @endif

                            @endif
                        </div>
                    </div>
                @endforeach
                
                {{-- <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.title_ar')}}</th>
                            <th>{{trans('admin.title_en')}}</th>
                            <th>{{trans('admin.content_ar')}}</th>
                            <th>{{trans('admin.content_en')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($contracts as $contract)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$contract->title_ar}}</td>
                                <td>{{$contract->title_en}}</td>
                                <td>{!! $contract->content_ar !!}</td>
                                <td>{!! $contract->content_en !!}</td>
                                <td>
                                    @if(checkPermissions(['edit_contract']) || checkPermissions(['delete_contract']))
                                      
                                            @if(checkPermissions(['edit_contract']))
                                                <a  href="{{route('admin.contracts.edit', $contract->id)}}" title="{{trans('admin.edit')}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                </a>
                                            @endif
                                            @if(checkPermissions(['delete_contract']))

                                                <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.contracts.destroy', $contract->id)}}" title="{{trans('admin.delete')}}">
                                                    <i data-feather="trash" class="mr-50"></i>
                                                </a>
                                            @endif

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table> --}}
            </div>

        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection