@extends('layout.app')

@section('title', trans('admin.edit_request'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <!-- Project edit_requests Starts-->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title mb-2">{{trans('admin.edit_requests')}}</h4>
                </div>
                <div class="card-body">
                    <div class="row invoice-list-wrapper">
                                <div class="card-datatable table-responsive">
                                    <table class="invoice-list-table table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>{{trans('admin.charity_name')}}</th>
                                                <th>{{trans('admin.charity_status')}}</th>
                                                <th>{{trans('admin.request_status')}}</th>
                                                <th>{{trans('admin.notes')}}</th>
                                                <th>{{trans('admin.actions')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        
                                            @foreach ($edit_requests as $edit_request)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$edit_request->charity ? $edit_request->charity->name : '-'}}</td>
                                                    <td>{{$edit_request->charity ? trans('admin.'.$edit_request->charity->status) : '-'}}</td>
                                                    
                                                    @if(checkPermissions(['accept_refuse_charity_edit_requests']))

                                                        <td>

                                                            @if($edit_request->status == 'approved')
                                                                <span id="current_status-{{$edit_request->id}}" class="badge badge-light-success">{{trans('admin.'.$edit_request->status)}}</span>
                                                            @elseif($edit_request->status == 'waiting')
                                                                <span id="current_status-{{$edit_request->id}}" class="badge badge-light-warning">{{trans('admin.'.$edit_request->status)}}</span>
                                                            @elseif($edit_request->status == 'hold')
                                                                <span id="current_status-{{$edit_request->id}}" class="badge badge-light-info">{{trans('admin.'.$edit_request->status)}}</span>
                                                            @elseif($edit_request->status == 'rejected')
                                                                <span id="current_status-{{$edit_request->id}}" class="badge badge-light-danger">{{trans('admin.'.$edit_request->status)}}</span>
                                                            @endif

                                                        </td>

                                                    @endif
                                                    <td>
                                                        <a href="#" data-toggle="modal" data-target="#req_notes-{{$edit_request->id}}" title="{{trans('admin.change')}}">{{ trans('admin.show') }}</a>
                                                        <!-- Modal -->  
                                                        <div class="modal fade modal-info text-left" id="req_notes-{{$edit_request->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <textarea id="req_notes-{{$edit_request->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6">{{ $edit_request->notes }}</textarea>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="modal-footer">
                                                                    <button type="button" id="" class="btn btn-info" data-model="{{ $edit_request->type == 'project' ? 'edit_request_project' : 'edit_request_charity' }}" data-id="{{$edit_request->id}}" data-action="{{route('admin.projects-edit-request')}}" data-method="POST">{{trans('admin.save')}}</button>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </td>
                                                    <td>
                                                        @if(($edit_request->type == 'charity' && checkPermissions(['delete_charity_edit_requests'])))
                                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.requests.destroy', $edit_request->id)}}" title="{{trans('admin.delete')}}">
                                                                <i data-feather="trash" class="mr-50"></i>
                                                            </a>
                                                        @endif

                                                        @if(($edit_request->type == 'charity' && checkPermissions(['accept_refuse_charity_edit_requests'])))
                                                            <a href="#" data-toggle="modal" data-target="#req_status-{{$edit_request->id}}" title="{{trans('admin.change')}}"><i data-feather="edit-2"></i></a>
                                                            <!-- Modal -->  
                                                            <div class="modal fade modal-info text-left" id="req_status-{{$edit_request->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                 
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label for="change_status">{{trans('admin.select_status')}}</label>
                                                                            <select id="change_edit_status-{{$edit_request->id}}" class="form-control">
                                                                                <option value="waiting" @if($edit_request->status == 'waiting') selected @endif>{{trans('admin.waiting')}}</option>
                                                                                <option value="approved" @if($edit_request->status == 'approved') selected @endif>{{trans('admin.approved')}}</option>
                                                                                <option value="rejected" @if($edit_request->status == 'rejected') selected @endif>{{trans('admin.rejected')}}</option>
                                                                                <option value="hold" @if($edit_request->status == 'hold') selected @endif>{{trans('admin.hold')}}</option>
                                                                            </select>
                                                                        </div>
                    
                                                                        <div class="form-group">
                                                                            <label for="notes">{{trans('admin.notes')}}</label>
                                                                            <textarea id="edit_notes-{{$edit_request->id}}" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"></textarea>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="button" id="" class="btn btn-info save_status" data-model="{{ $edit_request->type == 'project' ? 'edit_request_project' : 'edit_request_charity' }}" data-id="{{$edit_request->id}}" data-action="{{route('admin.projects-edit-request')}}" data-method="POST"><div class="spinner-border  processing-div-{{ $edit_request->id }}"  role="status"></div>{{trans('admin.save')}}</button>
                                                                    </div>
                                                                </div>
                                                            </div>                                                            
                                                        @endif

                                                        @if($edit_request->notes)
                                                            <a class="btn btn-outline-info" data-toggle="modal" data-target="#edit_request-{{$edit_request->id}}">{{trans('admin.notes')}}</a>
                                                            <!-- Modal -->
                                                            <div class="modal fade modal-info text-left" id="edit_request-{{$edit_request->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            {!! $edit_request->notes !!}
                                                                        </div>
                                                                        <div class="modal-footer" id="loading">
                                                                            <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </td>
                                                </tr>
                                            
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Project edit_requests Ends-->
    </div>
    
</section>
<!--/ Basic table -->

@endsection


@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush