@extends('layout.app')

@section('title', trans('admin.currencies'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_rating_criteria_ar')}}</th>
                            <th>{{trans('admin.name_rating_criteria_en')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($ratingcriterias as $ratingcriteria)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$ratingcriteria->name_ar}}</td>
                                <td>{{$ratingcriteria->name_en}}</td>
                                {{-- <td>{{$ratingcriteria->nation->code}}</td> --}}
                                <td>
                                    @if(checkPermissions(['edit_ratingcriteria']))
                                                {{-- @if(checkPermissions(['edit_ratingcriteria'])) --}}
                                                    <a href="{{route('admin.rating-criterias.edit', $ratingcriteria->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                {{-- @endif --}}
                                          
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection