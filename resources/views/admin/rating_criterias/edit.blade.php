@extends('layout.app')

@section('title', trans('admin.edit_ratin_criteria'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="edit_contract" method="POST" action="{{ route('admin.rating-criterias.update' , $ratingcriteria->id) }}">
                            @csrf
                            @method('PUT')

                            <!-- BEGIN : title -->
                            <div class="row">
                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_rating_criteria_ar">{{trans('admin.name_rating_criteria_ar')}}</label>
                                        <input type="text" class="form-control" id="name_rating_criteria_ar" name="name_rating_criteria_ar" value="{{ $ratingcriteria->name_ar ? $ratingcriteria->name_ar :  old('name_rating_criteria_ar')}}" placeholder="{{trans('admin.name_rating_criteria_ar')}}" />
                                        @if($errors->has('name_rating_criteria_ar'))
                                            <div class="error">{{ $errors->first('name_rating_criteria_ar') }}</div>
                                        @endif                                                                         
                                    </div>                            
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_rating_criteria_en">{{trans('admin.name_rating_criteria_en')}}</label>
                                        <input type="text" class="form-control" id="name_rating_criteria_en" name="name_rating_criteria_en" value="{{ $ratingcriteria->name_en ? $ratingcriteria->name_en : old('name_rating_criteria_en')}}" placeholder="{{trans('admin.name_rating_criteria_en')}}" />
                                        @if($errors->has('name_rating_criteria_en'))
                                            <div class="error">{{ $errors->first('name_rating_criteria_en') }}</div>
                                        @endif                                                                         
                                    </div> 
                                </div>                           
                            
                            </div>
                            <!-- END : title -->
                           
                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" id="" class="btn btn-primary" ><i data-feather="edit"></i> {{trans('admin.edit')}}</button>                                
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->



@endsection