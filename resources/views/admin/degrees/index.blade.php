@extends('layout.app')

@section('title', trans('admin.degrees'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">

                @if(checkPermissions(['create_degrees']))
                    <a href="{{route('admin.degrees.create')}}" class="btn btn-primary">{{trans('admin.create_degrees')}}</a>
                @endif

                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.degree_name')}}</th>
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['degrees'] as $degree)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$degree->name}}</td>
                                <td>
                                    @if(checkPermissions(['show_degrees']))
                                        <a  href="{{route('admin.degrees.show', $degree->id)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                    {{-- @else
                                        {{trans('admin.do_not_have_permission')}} --}}
                                    @endif
                            
                                    @if(checkPermissions(['edit_degrees']) || checkPermissions(['delete_degrees']))
                                       
                                                @if(checkPermissions(['edit_degrees']))
                                                    <a href="{{route('admin.degrees.edit', $degree->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_degrees']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.degrees.destroy', $degree->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                          
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['degrees']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection