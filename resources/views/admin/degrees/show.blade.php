@extends('layout.app')

@section('title', trans('admin.show_degrees'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div>{{trans('admin.degree_name_en')}} : {{$data['degree']->name_en}}</div>
                    <br>
                    <div>{{trans('admin.degree_name_ar')}} : {{$data['degree']->name_ar}}</div>
                    <br>

                    <div class="text-right">
                    <a class="btn btn-primary" href="{{route('admin.degrees.edit', $data['degree']->id)}}" title="{{trans('admin.edit')}}">
                        <i data-feather="edit-2" class="mr-50"></i>{{trans('admin.edit')}}   
                    </a>
                </div>
            </div>
        </div>
            
            
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.employees')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                            <thead>
                                <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.emp_name')}}</th>
                                <th>{{trans('admin.email')}}</th>
                                <th>{{trans('admin.phone')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data['degree']->users as $user)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>
                                            <a  href="{{route('admin.employees.show', $user->id)}}" title="{{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                            <a  href="{{route('admin.employees.edit', $user->id)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.employees.destroy', $user->id)}}" title="{{trans('admin.delete')}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>
                                              
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                        </table>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
@endsection