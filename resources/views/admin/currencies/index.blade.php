@extends('layout.app')

@section('title', trans('admin.currencies'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_currency']))
                    <a href="{{route('admin.currencies.create')}}" class="btn btn-primary">{{trans('admin.create_currency')}}</a>
                @endif
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_currency_ar')}}</th>
                            <th>{{trans('admin.name_currency_en')}}</th>
                            <th>{{trans('admin.symbol')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($currencies as $currency)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$currency->name_ar}}</td>
                                <td>{{$currency->name_en}}</td>
                                <td>{{ $currency->symbol }}</td>
                                {{-- <td>{{$currency->nation->code}}</td> --}}
                                <td>
                                    @if(checkPermissions(['edit_currency']) || (checkPermissions(['delete_currency']) && ($currency->id != 1 && $currency->id != 2)))
                                                 @if(checkPermissions(['edit_currency']))
                                                    <a  href="{{route('admin.currencies.edit', $currency->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_currency']) && ($currency->id != 1 && $currency->id != 2))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.currencies.destroy', $currency->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>

        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection