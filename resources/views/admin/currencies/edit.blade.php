@extends('layout.app')

@section('title', trans('admin.edit_currency'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit_currency')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="currencies_form" method="POST" class="request_form" action="{{ route('admin.currencies.update' , $currency->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <input type="hidden" id="edit" value="true">

                            <!-- BEGIN : title -->
                            <div class="row">
                            
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_currency_ar">{{trans('admin.name_currency_ar')}}</label>
                                        <input type="text" class="form-control" id="name_currency_ar" name="name_currency_ar" value="{{ $currency->name_ar ? $currency->name_ar :  old('name_currency_ar')}}" placeholder="{{trans('admin.name_currency_ar')}}" />
                                        <div class="error error-name_currency_ar"></div>                                                                           
                                    </div>                            
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_currency_en">{{trans('admin.name_currency_en')}}</label>
                                        <input type="text" class="form-control" id="name_currency_en" name="name_currency_en" value="{{ $currency->name_en ? $currency->name_en : old('name_currency_en')}}" placeholder="{{trans('admin.name_currency_en')}}" />
                                        <div class="error error-name_currency_en"></div>                                                                       
                                    </div> 
                                </div>                           
                            
                            </div>
                            <!-- END : title -->

                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="symbol">{{ __('admin.symbol') }}</label>
                                        <input type="text" class="form-control" id="symbol" name="symbol" value="{{ $currency->symbol ? $currency->symbol : old('symbol')}}" placeholder="{{trans('admin.symbol')}}" />
                                        <div class="error error-symbol"></div>       
                                        <small>{{trans('admin.symbol_hint')}}</small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="image">{{ __('admin.icon') }}</label>
                                        <div class="custom-file">
                                        <input type="file" class="form-control image" id="image" name="icon" value="{{old('icon')}}" placeholder="{{trans('admin.icon')}}" accept="image/png, image/jpeg, image/jpg" />
                                        <small>(Max size: 20MB)</small>
                                        <div class="error error-icon"></div>    
                                        </div>  
                                        <div class="form-group prev" style="display: inline-block">
                                            <img src="{{asset('storage/'.$currency->icon)}}" style="width: 100px" class="img-thumbnail preview-image" alt="currency">
                                        </div>                                     
                                    </div>
                                </div>

                            <!--  END: desc -->                             
                         
                            <div class="row">
                                <div class="col-12 error text-center general_error"></div>
                                <div class="col-12" id="loading">
                                    <button type="button" id="" class="btn btn-primary submit_request_form_button"> {{trans('admin.save')}}</button>                                
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection

@push('js')
        <script src="{{ url('custom/preview-image.js') }}"></script>
        <script src="{{asset('dashboard/forms/currencies/create.js')}}"></script>
        <script src="{{asset('custom/submit_simple_forms.js')}}"></script>
@endpush