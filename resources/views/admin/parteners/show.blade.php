@extends('layout.app')

@section('title', trans('admin.show_job'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{$data['job']->name}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.category')}}</th>
                                <th>{{trans('admin.nation')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['job']->name}}</td>
                                <td>{{$data['job']->category ? $data['job']->category->name : ''}}</td>
                                <td>{{$data['job']->category ? $data['job']->category->nation->code : ''}}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i data-feather="more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="{{route('admin.work-fields.edit', $data['job']->id)}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                                <span>{{trans('admin.edit')}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>

            {{-- job emps start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.emps')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['job']->emps as $emp)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$data['job']->name}}</td>
                                    <td>
                                        {{-- <div class="dropdown">
                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                <i data-feather="more-vertical"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                <a class="dropdown-item" href="{{route('admin.work-fields.edit', $data['job']->id)}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                    <span>{{trans('admin.edit')}}</span>
                                                </a>
                                            </div> --}}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            {{-- job emps end --}}
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection