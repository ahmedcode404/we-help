@extends('layout.app')

@section('title', trans('admin.edit_partener'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit_partener')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="partener_form" class="request_form" method="POST" action="{{route('admin.parteners.update', $data['part']->id)}}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <input type="hidden" id="edit" value="true">

                            <div class="form-group">
                                <label class="form-label" for="name_ar">{{trans('admin.partener_name_ar')}}</label>
                                <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{$data['part']->name_ar ? $data['part']->name_ar : old('name_ar')}}" placeholder="{{trans('admin.partener_name_ar')}}" required />
                                <span class="error error-name_ar"></span>
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="name_en">{{trans('admin.partener_name_en')}}</label>
                                <input type="text" id="name_en" name="name_en" class="form-control" value="{{$data['part']->name_en ? $data['part']->name_en : old('name_en')}}" placeholder="{{trans('admin.partener_name_en')}}" required />
                                <span class="error error-name_en"></span>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="logo">{{trans('admin.logo')}} ( Max-height:115px / Max-width:115px )</label>
                                <input type="file" id="logo" name="logo" class="form-control" 
                                placeholder="{{trans('admin.logo')}}"
                                accept="image/png, image/jpeg, image/jpg" />
                                <small>(Max size: 20MB)</small>
                                <img src="{{$data['part']->logo ? asset('storage/'.$data['part']->logo) : asset('dashboard/images/avatar.png')}}" alt="{{$data['part']->name}}" height="100">
                                <span class="error error-logo"></span>
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="link">{{trans('admin.partener_link')}}</label>
                                <input type="text" id="link" name="link" class="form-control" 
                                value="{{$data['part']->link ? $data['part']->link : old('link')}}" 
                                placeholder="{{trans('admin.partener_link')}}" required />
                                <span class="error error-link"></span>
                            </div>

                            <div class="row">
                                <div class="col-12 error text-center general_error"></div>
                                <div class="col-12" id="loading">
                                    <button type="button" class="btn btn-primary submit_request_form_button" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/parteners/create.js')}}"></script>
        <script src="{{asset('custom/submit_simple_forms.js')}}"></script>
    @endpush
@endsection