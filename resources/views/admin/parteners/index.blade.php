@extends('layout.app')

@section('title', trans('admin.parteners'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_partener']))
                    <a href="{{route('admin.parteners.create')}}" class="btn btn-primary">{{trans('admin.create_partener')}}</a>
                @endif
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.partener_name')}}</th>
                            <th>{{trans('admin.partener_link')}}</th>
                            <th>{{trans('admin.logo')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            {{-- <th>{{trans('admin.show')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['parteners'] as $part)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$part->name}}</td>
                                <td>
                                    <a href="{{$part->link}}">{{trans('admin.link')}}</a>
                                </td>
                                <td>
                                    <img src="{{$part->logo ? asset('storage/'.$part->logo) : asset('dashboard/images/avatar.png')}}" alt="{{$part->name}}"   style="max-width: 100px;max-height:100px">
                                </td>
                                {{-- <td>{{$part->nation->code}}</td> --}}
                                {{-- <td>
                                    @if(checkPermissions(['show_partener']))
                                        <a class="dropdown-item" href="{{route('admin.parteners.show', $part->id)}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                            <span>{{trans('admin.show')}}</span>
                                        </a>
                                    @else
                                        {{trans('admin.do_not_have_permission')}}
                                    @endif
                                </td> --}}
                                <td>
                                    @if(checkPermissions(['edit_partener']) || checkPermissions(['delete_partener']))
                                               @if(checkPermissions(['edit_partener']))
                                                    <a href="{{route('admin.parteners.edit', $part->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_partener']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.parteners.destroy', $part->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['parteners']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection