@extends('layout.app')

@section('title', trans('admin.edit_service'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="create_feature" method="POST" action="{{route('admin.service-features.update', $data['feature']->id)}}">
                            @csrf
                            @method('PUT')
                            
                            <!-- BEGIN : title -->
                            <div class="row">
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="name_ar">{{trans('admin.name_ar')}}</label>
                                    <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{$data['feature']->name_ar}}" placeholder="{{trans('admin.name_ar')}}" required />
                                    @if($errors->has('name_ar'))
                                        <div class="error">{{ $errors->first('name_ar') }}</div>
                                    @endif                             
                                </div>                            
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="name_en">{{trans('admin.name_en')}}</label>
                                    <input type="text" class="form-control" id="name_en" name="name_en" value="{{$data['feature']->name_en}}" placeholder="{{trans('admin.name_en')}}" required />
                                    @if($errors->has('name_en'))
                                        <div class="error">{{ $errors->first('name_en') }}</div>
                                    @endif                                
                                </div> 
                            </div>     

                            </div>
                            <!-- END : name -->

                            <div class="form-group">
                                <label for="parent_id">{{trans('admin.service')}}</label>
                                <select class="form-control select2" id="parent_id" name="parent_id" required>
                                    <option value="">{{trans('admin.select')}} ...</option>
                                    @foreach ($data['services'] as $service)
                                        <option value="{{$service->id}}" @if($data['feature']->parent_id == $service->id ) selected @endif>{{ $service->name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('parent_id'))
                                    <div class="error">{{ $errors->first('parent_id') }}</div>
                                @endif
                            </div> 

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary"> {{trans('admin.save')}}</button>                                 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection

@push('js')
    <script src="{{asset('dashboard/forms/services/create-feature.js')}}"></script>
@endpush