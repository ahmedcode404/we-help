@extends('layout.app')

@section('title', trans('admin.service-features'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['add_service']))

                    <a href="{{route('admin.service-features.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                
                @endif
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_ar')}}</th>
                            <th>{{trans('admin.name_en')}}</th>
                            <th>{{trans('admin.service')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['features'] as $feature)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$feature->name_ar}}</td>
                                <td>{{$feature->name_en}}</td>
                                <td>{{$feature->service ? $feature->service->name : '-' }}</td>
                                <td>
                                    @if(checkPermissions(['edit_service']) || checkPermissions(['delete_service']))
                                                @if(checkPermissions(['edit_service']))
                                                    <a  href="{{route('admin.service-features.edit', $feature->id)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_service']))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('admin.service-features.destroy', $feature->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
            {{$data['features']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection