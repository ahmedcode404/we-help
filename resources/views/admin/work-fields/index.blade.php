@extends('layout.app')

@section('title', trans('admin.work-fields'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_work_fields']))
                    <a href="{{route('admin.work-fields.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_en')}}</th>
                            <th>{{trans('admin.name_ar')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            <th>{{trans('admin.show')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['work-fields'] as $field)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$field->name_en}}</td>
                                <td>{{$field->name_ar}}</td>
                                {{-- <td>{{$field->nation->code}}</td> --}}
                                <td>
                                    @if(checkPermissions(['show_work_fields']))
                                        <a class="dropdown-item" href="{{route('admin.work-fields.show', $field->id)}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                            <span>{{trans('admin.show')}}</span>
                                        </a>
                                    @else
                                        {{trans('admin.do_not_have_permission')}}
                                    @endif
                                </td>
                                <td>
                                    @if(checkPermissions(['edit_work_fields']) || checkPermissions(['delete_work_fields']))
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                <i data-feather="more-vertical"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                @if(checkPermissions(['edit_work_fields']))
                                                    <a class="dropdown-item" href="{{route('admin.work-fields.edit', $field->id)}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                        <span>{{trans('admin.edit')}}</span>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_work_fields']))
                                                    <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.work-fields.destroy', $field->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                        <span>{{trans('admin.delete')}}</span>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['work-fields']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection