@extends('layout.app')

@section('title', trans('admin.show-work-fields'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">

            {{-- work_field start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{$data['field']->name}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name_en')}}</th>
                                <th>{{trans('admin.name_ar')}}</th>
                                {{-- <th>{{trans('admin.nation')}}</th> --}}
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>{{$data['field']->name_en}}</td>
                                <td>{{$data['field']->name_ar}}</td>
                                {{-- <td>{{$data['field']->nation->code}}</td> --}}
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                            <i data-feather="more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            @if(checkPermissions(['edit_work_fields']))
                                                <a class="dropdown-item" href="{{route('admin.work-fields.edit', $data['field']->id)}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                    <span>{{trans('admin.edit')}}</span>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            {{-- work_field end --}}

            {{-- charitires start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.charities')}}</h4>
                    </div>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.type')}}</th>
                                <th>{{trans('admin.contract_date')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data['field']->charities as $charity)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$charity->name}}</td>
                                    <td>{{trans('admin.'.$charity->type)}}</td>
                                    <td>{{$charity->contract_date}}</td>
                                    <td>
                                        <div class="dropdown">
                                            <button type="button" class="btn btn-sm dropdown-toggle hide-arrow" data-toggle="dropdown">
                                                <i data-feather="more-vertical"></i>
                                            </button>
                                            <div class="dropdown-menu">
                                                @if(checkPermissions(['show_charity']))
                                                    <a class="dropdown-item" href="{{route('admin.charities.show', $charity->id)}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                        <span>{{trans('admin.show')}}</span>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['edit_charity']))
                                                    <a class="dropdown-item" href="{{route('admin.charities.edit', $charity->id)}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                        <span>{{trans('admin.edit')}}</span>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_charity']))
                                                    <a class="dropdown-item remove-table" href="javascript:void(0);" data-url="{{route('admin.charities.destroy', $charity->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                        <span>{{trans('admin.delete')}}</span>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
            {{-- charities end --}}

        </div>
    </section>

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection