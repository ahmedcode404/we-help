@extends('layout.app')

@section('title', trans('admin.create_sponser'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-6 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.create')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="create_sponser" method="POST" action="{{route('admin.sponsers.store')}}">
                            @csrf
                            <input type="hidden" name="project_id" value="{{$data['project_id']}}">

                            <div class="form-group">
                                <label class="form-label" for="sponser_name_ar">{{trans('admin.name_ar')}}</label>
                                <input type="text" class="form-control" id="sponser_name_ar" name="sponser_name_ar" value="{{old('sponser_name_ar')}}" placeholder="{{trans('admin.name_ar')}}" required />
                            </div>

                            <div class="form-group">
                                <label class="form-label" for="sponser_name_en">{{trans('admin.name_en')}}</label>
                                <input type="text" class="form-control" id="sponser_name_en" name="sponser_name_en" value="{{old('sponser_name_en')}}" placeholder="{{trans('admin.name_en')}}" required />
                            </div>
                            
                            <div class="form-group">
                                <label for="sponser_work_field_id">{{trans('admin.work_field')}}</label>
                                <select class="form-control select2" id="sponser_work_field_id" name="sponser_work_field_id" required>
                                    @foreach ($data['work_fields'] as $field)
                                        <option value="{{$field->id}}" @if(old('sponser_work_field_id') == $field->id) selected @endif>{{$field->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary" value="Submit">{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/sponsers/create.js')}}"></script>
    @endpush
@endsection