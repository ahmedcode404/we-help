<!DOCTYPE html>
@if(\App::getLocale() == 'ar')
    <html class="loading" lang="ar" data-textdirection="rtl">
@else
    <html class="loading" lang="en" data-textdirection="ltr">
@endif
<!-- BEGIN: Head-->

<head>

    @yield('meta')

    <title>{{trans('admin.we_help')}} | @yield('title')</title>
    <meta name="description" content="@yield('description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta name="image" content="@yield('image')" />
    <meta name="author" content="{{trans('admin.we_help')}}" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   
    <link rel="apple-touch-icon" href="{{ url('dashboard/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('dashboard/app-assets/images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300&display=swap" rel="stylesheet">

    @include('layout-login.style')

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    <!-- BEGIN: Content-->


<!-- BEGIN: content -->
    @yield('content')
<!-- END: content -->


<!-- BEGIN:  -->
    @include('layout-login.footer')
<!-- END: content -->