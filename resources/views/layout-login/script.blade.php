
    <!-- BEGIN: Vendor JS-->
    <script src="{{ url('dashboard/app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ url('dashboard/app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ url('dashboard/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ url('dashboard/app-assets/js/core/app.js') }}"></script>
    <!-- END: Theme JS-->

    <!-- BEGIN: Page JS-->
    <script src="{{ url('dashboard/app-assets/js/scripts/pages/page-auth-login.js') }}"></script>
    <!-- END: Page JS-->

    @if(session('error'))

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>

    <!-- BEGIN: MESSAGE ERROR LOGIN JS-->
        <script src="{{ url('dashboard/login/message-error-login.js') }}"></script>
    <!-- END: MESSAGE ERROR LOGIN JS-->

    @endif

    @yield('script_auth')

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        })
    </script>

    {{-- Sweet Alerts Start --}}
@if (session()->has('success'))
<script>
    Swal.fire({
        title: "{{ session()->get('success') }}",
        html: "{{ session()->get('html') }}",
        type: 'success',
        timer: 1500,
        showCancelButton: false,
        showConfirmButton: false,
    });
    '{{ session()->forget('success') }}';
</script>
@endif

@if (session()->has('error'))
<script>
    Swal.fire({
        title: "{{ session()->get('error') }}",
        html: "{!! session()->get('html') !!}",
        type: 'error',
        timer: 1500,
        showCancelButton: false,
        showConfirmButton: false,
    });
    '{{ session()->forget('error') }}';
</script>
@endif

@if (session()->has('warning'))
<script>
    Swal.fire({
        title: "{{ session()->get('warning') }}",
        html: "{{ session()->get('html') }}",
        type: 'warning',
        timer: 1500,
        showCancelButton: false,
        showConfirmButton: false,
    });
    '{{ session()->forget('warning') }}';
</script>
@endif
