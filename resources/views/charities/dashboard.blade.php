
@extends('layout.app')
@section('title')
{{ __('admin.home') }}
@endsection
@section('content')

    <!-- BEGIN: Content-->
    
    <div class="content-body">
                <!-- Dashboard Ecommerce Starts -->
                <section id="dashboard-ecommerce">
                    <div class="row match-height">

                        @if(auth()->check() && 

                            ($user->hasRole('charity') && 
                            $user->contract_after_sign == NULL &&
                            $user->signature == NULL) ||

                            ($user->hasRole('charity_employee') && 
                            $user->charity->contract_after_sign == NULL &&
                            $user->charity->signature == NULL) 
                            )

                            <!-- contract -->
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert">
                                    <h4 class="alert-heading">{{ __('admin.note') }}</h4>
                                    <div class="alert-body">
                                    <i data-feather="alert-triangle"></i> {{ __('admin.note_contract') }} 

                                    @if($user->hasRole('charity'))
                                        <i data-feather="{{ App::getLocale() == 'ar' ? 'arrow-left' : 'arrow-right' }}"></i> 
                                        <a href="{{ route('charity.contract') }}">{{ __('admin.link_contract') }}</a>
                                    @endif

                                    </div>
                                </div>
                            </div>
                            <!--/ contract -->
                        @endif


                        @if( auth()->check() && 

                            (($user->hasRole('charity') &&
                            ($user->contract_after_sign != NULL || 
                            $user->signature != NULL) && 
                            $user->status != 'approved') ||
                            
                            ($user->hasRole('charity_employee') && 
                            ($user->charity->contract_after_sign != NULL || 
                            $user->charity->signature != NULL) && 
                            $user->charity->status != 'approved') ) )

                            <!-- contract -->
                            <div class="col-12">
                                <div class="alert alert-info" role="alert">
                                    <h4 class="alert-heading">{{ __('admin.note') }}</h4>
                                    <div class="alert-body">
                                    <i data-feather="alert-triangle"></i> {{ __('admin.note_waiting') }}
                                    </div>
                                </div>
                            </div>
                            <!--/ contract -->
                        @endif                       

                        <!-- Statistics SA Card -->
                        <div class="col-12">
                            <div class="card card-statistics">
                                <div class="card-header">
                                    <h4 class="card-title">{{trans('admin.statistics')}}</h4>
                                </div>
                                <div class="card-body statistics-body">
                                    <div class="row">
                                        {{-- beneficiaries start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-primary mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="trending-up" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ count($projects) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.projects')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- beneficiaries end --}}

                                        {{-- donations start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-info mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="dollar-sign" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ getTotalSupports($projects) }} {{ $user->currency->symbol }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.total_donations')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- donations end --}}

                                        {{-- totla_parteners start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-danger mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ $sponsers }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.sponsers')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- totla_parteners end --}}

                                        {{-- total_countries start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="flag" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ $phases }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.phases')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- total_countries end --}}

                                        {{-- charities start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ count($project_waiting) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.project_waiting')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- charities end --}}

                                        {{-- edit_requests start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="file" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ count($project_approved) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.project_approved')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- edit_requests end --}}

                                        {{-- supporters start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="user" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ count($project_rejected) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.project_rejected')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- supporters end --}}

                                        {{-- marketings start --}}
                                        <div class="col-xl-3 col-sm-6 col-12 mb-2">
                                            <div class="media">
                                                <div class="avatar bg-light-success mr-2">
                                                    <div class="avatar-content">
                                                        <i data-feather="box" class="avatar-icon"></i>
                                                    </div>
                                                </div>
                                                <div class="media-body my-auto">
                                                    <h4 class="font-weight-bolder mb-0">{{ count($project_hold) }}</h4>
                                                    <p class="card-text font-small-3 mb-0">{{trans('admin.project_hold')}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- marketings end --}}

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Statistics SA Card -->

                    </div>

                </section>
                <!-- Dashboard Ecommerce ends -->

            </div>
        
    <!-- END: Content-->




@endsection