@extends('layout.app')
@section('title')
{{ __('admin.add_project') }}
@endsection
@section('content')


<section class="app-user-view">

    <input type="hidden" id="edit" value="true">
    <!-- User Card & Plan Starts -->
    <div class="row">
        <!-- User Card starts-->
        <div class="col-xl-12">
            <div class="card user-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-5">
                            <div class="user-avatar-section mb-3">
                                <div class="d-flex justify-content-start">
                                    <img class="img-fluid rounded" src="@if($project->images->isNotEmpty()) {{ url('storage/' . $project->images[0]->path) }} @else {{ url('storage/' . 'charity/default.png'  ) }} @endif" height="70" width="70" alt="User avatar" />
                                    <div class="d-flex flex-column ml-1">
                                        <div class="user-info mb-1">
                                            <h4 class="mb-0">{{ $project->name }}</h4>
                                            <span class="card-text">{{ $project->email }}</span>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                                                                
                                            @if($project->status != 'approved' || checkPermissions(['edit_project_' . $project->id]))
                                                <a href="{{route('charity.projects.edit', $project->slug)}}" class="btn btn-primary">{{ __('admin.edit') }}</a>
                                            @endif

                                            @if(! checkPermissions(['edit_project_' . $project->id]) && $project->status == 'approved')

                                                <a data-toggle="modal" data-target="#edit_notes" title="{{trans('admin.notes')}}" href="javascript:void(0);"
                                                    class="btn {{ $edit_request != null ? 'btn-outline-primary disabled' : 'btn-outline-info' }}">
                                                    <i data-feather="send" class="mr-50"></i>
                                                    <span class="text-send">{{ $edit_request != null ? trans('admin.send_success') : trans('admin.request_edit_project') }}</span>
                                                </a>

                                                <!-- Modal -->  
                                                <div class="modal fade modal-info text-left" id="edit_notes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                                  
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <textarea id="notes" name="notes" class="form-control" placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6" required></textarea>
                                                                <span class="error error-notes"></span>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="modal-footer">
                                                            <button type="button" 
                                                                class="btn {{ $edit_request != null ? 'btn-outline-primary disabled' : 'btn-outline-info' }} send-request" 
                                                                id="{{ $project->id }}" sure="{{ __('admin.sure') }}" 
                                                                text-sure="{{ __('admin.text_sure') }}" 
                                                                data-url="{{route('charity.send.request.edit')}}"
                                                            > <div class="spinner-border  processing-div" role="status"></div>{{trans('admin.save')}}</button>
                                                        </div>
                                                    </div>
                                                </div>  
                                                </div>

                                            @endif                                           
                                            <!-- <a href="" class="btn btn-outline-danger ml-1">Edit</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-7">
                            <div class="user-info-wrapper">
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{trans('admin.category')}}:</span>
                                    </div>
                                    <p class="card-text mb-0">{{$project->category ?  $project->category->name : ' - '}}</p>
                                </div>
                                @if(($project->category && $project->category->type == 'engineering'))
                                    <div class="d-flex flex-wrap mb-1">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.eng_maps') }}:</span>
                                        </div>
                                        <a href="{{asset('storage/'.$project->eng_maps)}}">{{ trans('admin.eng_maps') }}</a>
                                    </div>
                                    
                                @elseif(($project->service_option_id || $project->other_service_option))

                                    <div class="d-flex flex-wrap mb-1">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.service') }}:</span>
                                        </div>
                                        <p class="card-text mb-0 ml-2">{{$project->service_option ? $project->service_option->name : $project->other_service_option}}</p>
                                    </div>
                                    
                                @endif
                                @if($project->service_feature_id || $project->other_service_feature)
                                    <div class="d-flex flex-wrap mb-1">
                                        <div class="user-info-title">
                                            <i data-feather="check" class="mr-1"></i>
                                            <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.feature') }}:</span>
                                        </div>
                                        <p class="card-text mb-0 ml-2">{{$project->service_feature ? $project->service_feature->name : $project->other_service_feature}}</p>
                                    </div>
                                @endif
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.status') }}:  </span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">  {{ trans('admin.'.$project->status) }} </p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="file" class="mr-1"></i>
                                        {{-- <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.attach') }}:</span> --}}
                                    </div>
                                    <p class="card-text mb-0"><a href="{{ url('storage/' . $project->attach ) }}" target="_blank">{{ __('admin.attach') }}</a></p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.country') }}:</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ Countries::getOne($project->country, \App::getLocale()) }}</p>
                                </div>
                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.cost') }}:</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $project->cost }} {{ $project->currency->symbol }}</p>
                                </div>

                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.superadmin_ratio') }}:</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $project->category ? $project->category->superadmin_ratio : 0 }} %</p>
                                </div>

                                <div class="d-flex flex-wrap mb-1">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-1 mr-1 text-primary">{{ __('admin.total_project_cost') }}:</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $project->get_total }} {{ $project->currency->symbol }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <!-- /User Card Ends-->

    </div>
    <!-- User Card & Plan Ends -->
    </div>
        @php
            if(auth()->user()->hasRole('charity_employee')){
                $user = auth()->user()->charity;
            }
            else{
                $user = auth()->user();
            }

        @endphp

        <!-- Project details info  starts-->
        <div class="col-12">
            <div class="card plan-card border-primary">
                <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                    <h5 class="mb-0">{{trans('admin.basic_info')}}</h5>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-borderless">
                        <tbody>
                            <tr>
                                <td><div class="badge badge-light-primary">{{trans('admin.project_cost')}}</div></td>
                                <td>{{generalExchange($project->get_total, $project->currency->symbol, $session_currency)}} {{$session_currency}}</td>
                            </tr>
                            <tr>
                                <td><div class="badge badge-light-primary">{{trans('admin.superadmin_ratio')}}</div></td>
                                <td>{{$project->category ? $project->category->superadmin_ratio : 0}} %</td>
                            </tr>
                            <tr>
                                <td><div class="badge badge-light-primary">{{trans('admin.project_start_date')}}</div></td>
                                <td>{{$project->start_date}}</td>
                            </tr>
                            <tr>
                                <td><div class="badge badge-light-primary">{{trans('admin.project_end_date')}}</div></td>
                                <td>{{getEndDate($project->start_date, $project->duration)}}</td>
                            </tr>
                            <tr>
                                <td><div class="badge badge-light-primary">{{trans('admin.project_duration')}}</div></td>
                                <td>{{$project->duration}}</td>
                            </tr>
                            <tr>
                                <td><div class="badge badge-light-primary">{{trans('admin.benef_num')}}</div></td>
                                <td>{{$project->benef_num}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /Project details info Ends -->

        <!-- Projetc images starts -->
        <div class="row">
            <div class="col-12">
                <div class="card profile-header mb-2">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.project_images')}}</h4>
                    </div>
                    <div class="card-body">
                        <div id="carousel-keyboard" class="carousel slide" data-keyboard="true">
                            @if(count($project->images) > 1)
                                <ol class="carousel-indicators">
                                    @foreach ($project->images as $image)
                                        <li data-target="#carousel-keyboard" data-slide-to="{{ $loop->iteration-1 }}" @if($loop->iteration == 1) class="active" @endif></li>
                                    @endforeach
                                </ol>
                            @endif
                            <div class="carousel-inner" style="height: auto" role="listbox">
                                @foreach ($project->images as $image)
                                    <div class="carousel-item @if($loop->iteration == 1) active @endif">
                                        <img class="img-fluid"  src="{{asset('storage/'.$image->path)}}" alt="{{$project->name}}" />
                                    </div>
                                @endforeach
                            </div>

                            @if (count($project->images) > 1)

                                <a class="carousel-control-prev" href="#carousel-keyboard" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">{{ trans('admin.Previous') }}</span>
                                </a>
                                <a class="carousel-control-next" href="#carousel-keyboard" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">{{ trans('admin.Next') }}</span>
                                </a>
                                
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Projetc images ends -->

        <!-- Project general data Starts -->
        <div class="row">
            <!-- Goal starts -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">{{trans('admin.project_goals')}}</h4>
                    </div>
                    <div class="card-body">
                        {!! $project->goal !!}
                    </div>
                </div>
            </div>
            <!-- Goal Ends -->

            <!-- Description starts -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-2">
                            {{trans('admin.project_desc')}}
                        </h4>
                    </div>
                            {{-- <a class="btn btn-outline-info" data-toggle="modal" data-target="#info">{{trans('admin.full_desc')}}</a> --}}
                            
                            <!-- Modal -->
                            {{-- <div class="modal fade modal-info text-left" id="info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="myModalLabel130">{{trans('admin.change_status')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body"> --}}
                                          
                                        {{-- </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                        </div>
                                    </div> --}}
                                {{-- </div>
                            </div> --}}
                    <div class="card-body">
                        <div class="media align-items-center mb-1">
                            {{-- <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" /> --}}
                            <div class="media-body"><a href="{{asset('storage/'.$project->attach)}}">{{trans('admin.attach')}}</a></div>
                        </div>
                        {!! $project->desc !!}
                        <hr>
                        <div class="form-group">
                            {!! $project->long_desc !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- Description Ends -->

            <div class="col-md-12">
            <!-- eng_maps starts -->
            {{-- مشروع انشائى --}}
            @if ($project->charity && $project->charity->type == 'engineering')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.eng_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.image')}}</div></td>
                                        <td>
                                            <div class="profile-img-container d-flex align-items-center">
                                                <div class="profile-img">
                                                    <img src="{{asset('storage/'.$project->eng_maps)}}" class="rounded img-fluid" alt="{{$project->name}}" />
                                                </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- eng_maps Ends -->

            <!-- edu_project starts -->
            {{-- مشروع تعليمى --}}
            @if ($project->charity && $project->charity->type == 'educational')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.edu_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.stage')}}: {{trans('admin.'.$project->stage)}}</div></td>
                                        <td>{{trans('admin.service')}}: {{$project->edu_services ? $project->edu_services->name : $project->edu_service_other}}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- edu_project Ends -->

            <!-- care_project starts -->
            {{-- مشروع رعاية يتيم --}}
            @if ($project->charity && $project->charity->type == 'sponsorship')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.care_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.care_type')}}</div></td>
                                        <td>{{trans('admin.'.$project->care_type)}}</td>
                                    </tr>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.care_duration')}}</div></td>
                                        <td>{{trans('admin.'.$project->care_duration)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- care_project Ends -->
            
            <!-- emerg_project starts -->
            {{-- مشروع طارئ --}}
            @if ($project->charity && $project->charity->type == 'emergency')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.emerg_project')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.service')}}</div></td>
                                        <td>{{$project->emerg_services ? $project->emerg_services->name : $project->emerg_services_other}}</td>
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- emerg_project Ends -->

            <!-- stable_projects starts -->
            {{-- مشروع  مستديم --}}
            @if ($project->charity && $project->charity->type == 'sustainable')
                    <div class="card plan-card border-primary">
                        <div class="card-header d-flex justify-content-between align-items-center pt-75 pb-1">
                            <h5 class="mb-0">{{trans('admin.stable_projects')}}</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-striped table-borderless">
                                <tbody>
                                    <tr>
                                        <td><div class="badge badge-light-primary">{{trans('admin.service')}}</div></td>
                                        <td>{{trans('admin.'.$project->stable_project)}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            @endif
            <!-- stable_projects Ends -->

            {{-- Project notes --}}
            @if($project->notes)
                <!-- Notes starts -->
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title mb-2">{{trans('admin.notes')}}</h4>
                        </div>
                        <div class="card-body">
                            {!! $project->notes !!}
                        </div>
                    </div>
                </div>
                <!-- Notes Ends -->
            @endif
            </div>

        </div>
        <!-- Project general data Ends -->

        <!-- phases Card -->
            <div class="card card-company-table">
                <div class="card-header">
                    <h4>{{ __('admin.phases') }}</h4>
                </div>

                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="datatables-basic table">
                            <thead>
                                <tr>
                                    <th>{{ __('admin.order') }}</th>
                                    <th>{{ __('admin.total_cost') }}</th>
                                    <th>{{ __('admin.duration_work') }}</th>
                                    <th>{{ __('admin.start_date') }}</th>
                                    <th>{{ __('admin.end_date') }}</th>
                                    <th>{{ __('admin.status') }}</th>
                                    {{-- <th>{{ trans('admin.show') }}</th> --}}
                                    <th>{{ __('admin.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($project->phases as $key=>$phase)
                                    <tr>
                                        <td>{{ $phase->order }}</td>
                                        <td>{{ $phase->cost }}</td>
                                        <td>{{ $phase->duration }} @if($phase->duration <= 2) {{ __('admin.day') }} @elseif($phase->duration > 2 && $phase->duration <= 10) {{ __('admin.days') }} @else {{ __('admin.day') }} @endif </td>
                                        <td>{{ $phase->start_date }}</td>
                                        <td>{{ $phase->end_date }}</td>

                                        @if($phase->approved == 1)

                                            <td> <div class="badge badge-secondary">{{ __('admin.approved') }}</div> </td>

                                        @else

                                            <td> <div class="badge badge-primary">{{ __('admin.not_approved') }}</div> </td>

                                        @endif

                                        <td>
                                            @if(checkPermissions(['show_phases_' . $user->id]) ||
                                                checkPermissions(['list_reports_'.$user->id]) || checkPermissions(['create_reports_'.$user->id]) || 
                                                checkPermissions(['edit_reports_'.$user->id]) || checkPermissions(['show_reports_'.$user->id]) || 
                                                checkPermissions(['delete_reports_'.$user->id]))
                                                
                                                <a href="{{route('charity.phases.show', $phase->slug)}}" target="_blank" title="{{trans('admin.show')}}">
                                                    <i data-feather="eye" class="mr-50"></i>
                                                </a>
                                            @endif
                                    
                                            {{-- صلاحيات التقارير مرتبطة بعرض مراحل المشروع --}}
                                            @if(checkPermissions(['edit_phases_' . $user->id]) || checkPermissions(['delete_phases_' . $user->id]) ||
                                                checkPermissions(['list_reports_'.$user->id]) || checkPermissions(['create_reports_'.$user->id]) || 
                                                checkPermissions(['edit_reports_'.$user->id]) || checkPermissions(['show_reports_'.$user->id]) || 
                                                checkPermissions(['delete_reports_'.$user->id]))
                                                
                                                        @if($phase->approved == 0 || checkPermissions(['edit_phases_' . $user->id]))
                                                            <a  href="{{route('charity.phases.edit', $phase->slug)}}" title="{{trans('admin.edit')}}">
                                                                <i data-feather="edit-2" class="mr-50"></i>
                                                            </a>
                                                        @endif
                                                        
                                                        @if($phase->approved == 0 || checkPermissions(['delete_phases_' . $user->id]))
                                                            <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('charity.phases.destroy', $phase->id)}}">
                                                                <i data-feather="trash" class="mr-50"></i>
                                                            </a> 
                                                        @endif  

                                                        @if($phase->approved == 1)
                                                            <a title="{{trans('admin.add_report')}}" class="@if($phase->end_date <= $dateNow) @else add-repoort @endif"  href="@if($phase->end_date <= $dateNow){{route('charity.phase', ['id' => $phase->slug, 'type' => 'phase'])}}@else javascript:void(0); @endif" >
                                                                <i data-feather="file-plus" class="mr-50"></i>
                                                            </a>
                                                        @endif
                                            @endif                                        
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <!--/ phases Card -->

        <!-- Reports Card -->
            <div class="card card-company-table">
                <div class="card-header">
                    <h4>{{ __('admin.reports') }}</h4>
                </div>
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="datatables-basic table">
                            <thead>
                                <tr>
                                    <th>{{ __('admin.phase_name') }}</th>
                                    <th>{{ __('admin.project_name') }}</th>
                                    <th>{{ __('admin.vedio') }}</th>
                                    <th>{{ __('admin.status') }}</th>
                                    {{-- <th>{{ __('admin.show') }}</th> --}}
                                    <th>{{ __('admin.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($project->reports as $key=>$report)
                                    <tr>
                                        <td>{{ $report->phase ? $report->phase->name  : trans('admin.project_report')}}</td>
                                        <td>{{ $report->project ? $report->project->name : '-' }}</td>
                                        <td>
                                            @if($report->vedio)
                                            <a href="{{ asset('storage/'.$report->vedio) }}" target="_blank">{{trans('admin.show')}}</a>
                                            @else
                                                {{ trans('admin.no_vedio') }}
                                            @endif
                                        </td>

                                        @if($report->status == 'waiting')

                                            <td> <div class="badge badge-secondary">{{ __('admin.waiting') }}</div> </td>

                                        @elseif($report->status == 'approved')

                                            <td> <div class="badge badge-primary">{{ __('admin.approved') }}</div> </td>
                                            
                                        @elseif($report->status == 'rejected')

                                            <td> <div class="badge badge-primary">{{ __('admin.rejected') }}</div> </td>

                                        @elseif($report->status == 'hold')

                                            <td> <div class="badge badge-primary">{{ __('admin.hold') }}</div> </td>

                                        @elseif($report->status == 'closed')

                                            <td> <div class="badge badge-primary">{{ __('admin.closed') }}</div> </td>

                                        @endif

                                        <td>
                                            <a  href="{{route('charity.reports.show', $report->slug)}}" title="{{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                       
                                            @if($report->status == 'waiting' || 
                                            (checkPermissions(['edit_reports_' . $report->id]) && checkPermissions(['delete_reports_' . $report->id])))
                                                        @if($report->status == 'waiting' || checkPermissions(['edit_reports_' . $report->id]))
                                                            <a href="{{route('charity.reports.edit', $report->slug)}}" title="{{trans('admin.edit')}}">
                                                                <i data-feather="edit-2" class="mr-50"></i>
                                                            </a>
                                                        @endif
                                                        
                                                        @if($report->status == 'waiting' || checkPermissions(['delete_reports_' . $report->id]))
                                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('charity.reports.destroy', $report->id)}}" title="{{trans('admin.delete')}}">
                                                                <i data-feather="trash" class="mr-50"></i>
                                                            </a>
                                                        @endif    
                                            @endif                                         
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <!--/ Reports Card -->

        <!-- Project location Starts-->
        <div class="card">
            <div class="card-header">
                <h4 class="card-title mb-2">{{trans('admin.project_location')}}</h4>
            </div>
            <div class="card-body">
            <!-- LOCATION -->   
            <div class="form-group" style="position: relative">
                <label class="form-label" for="location">{{ __('admin.project_location') }}</label>
                <input type="text" id="location" name="location" value="{{$project->location}}" disabled class="form-control" placeholder="{{ __('admin.location') }}" />
                <span class="error-input"></span>
                <span class="error-input error"></span>
                <input type="hidden" name="lat" id="lat"  value="{{$project->lat}}">
                <input type="hidden" name="lng" id="lng"  value="{{$project->lng}}">
                <div id="map" style="width:100%;height:500px;"></div>
            </div>
            </div>
        </div>
    <!-- Project location end-->


   


        <!-- Company Table Card -->
        {{-- <div class="col-lg-12 col-12">
            <div class="card card-company-table">
                <div class="card-body p-0">
                    <h1 style="margin: 12px;">{{ __('admin.sponsers') }}</h1>
                    <div class="table-responsive">
                        <table class="datatables-basic table">
                            <thead>
                                <tr>
                                    <th>{{ __('admin.name_sponser') }}</th>
                                    <th>{{ __('admin.work_field') }}</th>
                                    <th>{{ __('admin.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($project->sponsers as $key=>$work)
                                    <tr>
                                        <td>{{ $work->name }}</td>
                                        <td>{{ $work->work_field->name_en }}</td>
                                        <td>
                                            @if(checkPermissions(['show_sponsers_' . $user->id]))
                                                <a  href="{{route('charity.sponsers.show', $work->id)}}" target="_blank" title="{{trans('admin.show')}}">
                                                    <i data-feather="eye" class="mr-50"></i>
                                                </a>
                                            @endif
                                       
                                            @if(checkPermissions(['edit_sponsers_' . $user->id]) || checkPermissions(['delete_sponsers_' . $user->id]))
                                                        @if($project->status == 'waiting' || checkPermissions(['edit_sponsers_' . $work->project_id]))
                                                            <a  href="{{route('charity.sponsers.edit', $work->id)}}" title="{{trans('admin.edit')}}">
                                                                <i data-feather="edit-2" class="mr-50"></i>
                                                            </a>
                                                        @endif
                                                        @if($project->status == 'waiting' || checkPermissions(['delete_sponsers_' . $work->project_id]))
                                                            <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('charity.sponsers.destroy', $work->id)}}">
                                                                <i data-feather="trash" class="mr-50"></i>
                                                            </a> 
                                                        @endif 
                                            @endif                                      
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> --}}
        <!--/ Company Table Card -->


       


</section>


@endsection

@push('js')

    <script src="{{ url('dashboard/forms/projects/map.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdarVlRZOccFIGWJiJ2cFY8-Sr26ibiyY&libraries=places&callback=initAutocomplete&language=<?php echo e('ar'); ?>"
    defer></script>

@endpush






