@extends('layout.app')

@section('title')
{{ __('admin.edit-project') }}
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
     
@endpush

@section('content')

    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">

            <!-- jQuery Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('admin.edit-project') }}</h4>
                    </div>
                    <div class="card-body">
                        <form id="form-create-project" action="{{route('charity.projects.update', $project->id)}}" class="submit_form" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <input type="hidden" id="edit" value="true">

                            <div class="form-group">
                                <label for="select-country">{{ __('admin.project_name') }}</label>
                               <input type="text" class="form-control" name="name" id="name" value="{{$project->name}}" required>
                                <span class="error-input error-name">{{ $errors->first('name') }}</span>
                            </div>

                            <!-- CATEGORIES -->
                            <div class="form-group">
                                <label for="select-country">{{ __('admin.categories') }}</label>
                                <select class="form-control" id="charity_category_id" name="charity_category_id" 
                                    data-action="{{route('get-category-services')}}" data-method="POST" required>
                                    @if($project->charity)
                                        @foreach ($project->charity->categories as $category)
                                            <option value="{{$category->id}}" data-type="{{$category->type}}" @if($project->charity_category_id == $category->id || old('charity_category_id') == $category->id) selected @endif>{{$category->name}}</option>
                                        @endforeach
                                    @else
                                        <option value="">{{trans('admin.no_data')}}</option>
                                    @endif
                                </select>
                                <span class="error-input error-charity_category_id">{{ $errors->first('charity_category_id') }}</span>
                            </div> 

                           
                                <div class="form-group @if($project->eng_maps == null) hidden @endif" id="eng_maps_container">
                                    <label for="select-country">{{ __('admin.eng_maps') }}</label>
                                    <input type="file" class="form-control" name="eng_maps" id="eng_maps" accept=",.png,.jpg,.jpeg,.pdf" value="{{old('eng_maps')}}">
                                    <small>(Max size: 20MG)</small>
                                    @if ($errors->has('eng_maps'))
                                        <span class="error-input">{{ $errors->first('eng_maps') }}</span>
                                    @endif
                                    @if($project->eng_maps != null)
                                        <img src="{{asset('storage/'.$project->eng_maps)}}"  height="100" alt="engineering maps">
                                    @endif
                                    <span class="error-input error-eng_maps"></span>
                                </div>

                                <div id="services" class="form-group
            
                                    @if($project->category && ($project->category->type != 'educational' &&
                                    $project->category->type != 'sponsorship' && $project->category->type != 'emergency' &&
                                    $project->category->type != 'sustainable')) hidden @endif">
            
                                    <label for="select-country">{{ trans('admin.service') }}</label>
                                    <select class="form-control select2" id="service_option_id" data-action="{{route('get-service-features')}}" 
                                        data-method="POST" name="service_option_id" required>
                                        <option value="">{{trans('admin.select')}}...</option>
                                        @foreach($services as $service)
                                            <option value="{{ $service->id }}"
                                                @if($project->service_option_id == $service->id) selected @endif>{{ $service->name }}</option>
                                        @endforeach
                                        @if($project->category && ($project->category->type != 'educational' &&
                                            $project->category->type != 'sponsorship' && $project->category->type != 'emergency' &&
                                            $project->category->type != 'sustainable') && $project->category->type != 'engineering' && 
                                            $project->category->type != null))
                                            <option value="other"
                                                @if($project->service_option_id == null) selected @endif>{{trans('admin.other')}}</option>
                                        @endif
                                    </select>
                                    <span class="error-input error-service_option_id"></span>
                                </div>

                                <div class="form-group @if($project->other_service_option == null) hidden @endif" id="other_service_option_container">
                                    <label for="select-country">{{ __('admin.other_service_option') }}</label>
                                    <input type="text" class="form-control" name="other_service_option" id="other_service_option" value="{{$project->other_service_option}}" required>
                                    <span class="error-input error-other_service_option"></span>
                                </div> 

                                <div id="features" class="form-group
            
                                    @if($project->category && ($project->category->type != 'educational' &&
                                    $project->category->type != 'sponsorship' && $project->category->type != 'emergency' &&
                                    $project->category->type != 'sustainable')) hidden @endif">
            
                                    @if($project->service_feature_id != null)
                                        <label for="select-country">{{ __('admin.feature') }}</label>
                                        <select class="form-control select2" id="service_feature_id" name="service_feature_id" required>
                                            <option value="">{{trans('admin.select')}}...</option>
                                            @foreach($features as $feature)
                                                <option value="{{ $feature->id }}"
                                                    @if($project->service_feature_id == $feature->id) selected @endif>{{ $feature->name }}</option>
                                            @endforeach
                                            <option value="other" @if($project->service_feature_id == null) selected @endif>{{trans('admin.other')}}</option>
                                        </select>
                                        <span class="error-input error-service_feature_id"></span>
                                    @endif
                                </div>

                                <div class="form-group @if($project->other_service_feature == null) hidden @endif" id="other_service_feature_container">
                                    <label for="select-country">{{ __('admin.other_service_feature') }}</label>
                                    <input type="text" class="form-control" name="other_service_feature" id="other_service_feature" value="{{$project->other_service_feature}}" required>
                                    <span class="error-input error-other_service_feature"></span>
                                </div>

                            <div class="form-group @if($project->other_service_option == null) hidden @endif" id="other_service_option_container">
                                <label for="select-country">{{ __('admin.other_service_option') }}</label>
                                <input type="text" class="form-control" name="other_service_option" id="other_service_option" value="{{$project->other_service_option}}" required>
                                <span class="error-input error-other_service_option">{{ $errors->first('other_service_option') }}</span>
                            </div> 

                            <div class="form-group" id="features">
                                @if($project->service_feature_id != null)
                                    <label for="select-country">{{ __('admin.feature') }}</label>
                                    <select class="form-control select2" id="service_feature_id" name="service_feature_id" required>
                                        <option value="">{{trans('admin.select')}}...</option>
                                        @foreach($features as $feature)
                                            <option value="{{ $feature->id }}"
                                                @if($project->service_feature_id == $feature->id) selected @endif>{{ $feature->name }}</option>
                                        @endforeach
                                        <option value="other" @if($project->service_feature_id == null) selected @endif>{{trans('admin.other')}}</option>
                                    </select>
                                    <span class="error-input error-service_feature_id">{{ $errors->first('service_feature_id') }}</span>
                                @endif
                            </div>

                            <div class="form-group @if($project->other_service_feature == null) hidden @endif" id="other_service_feature_container">
                                <label for="select-country">{{ __('admin.other_service_feature') }}</label>
                                <input type="text" class="form-control" name="other_service_feature" id="other_service_feature" value="{{$project->other_service_feature}}" required>
                                <span class="error-input error-other_service_feature">{{ $errors->first('other_service_feature') }}</span>
                            </div>


                            <!-- STRAT DATE -->
                            <div class="form-group">
                                <label class="form-label" for="basic-default-password">{{ __('admin.project_start_date') }}</label>
                                <input type="text" id="start_date" name="start_date" value="{{$project->start_date}}" class="form-control date-picker" readonly  autocomplete="off" />
                                <span class="error-input error-start_date">{{ $errors->first('start_date') }}</span>
                            </div>
                            <div class="mb-2"> {{ __('admin.hint_start_date') }}</div>
                        
                            <!-- LOCATION -->
                            <div class="form-group" style="position: relative">
                                <label class="form-label" for="location">{{ __('admin.project_location') }}</label>
                                <input type="text" id="location" name="location" value="{{$project->location}}" class="form-control" placeholder="{{ __('admin.location') }}" />
                                <span class="error-input"></span>
                                <span class="error-input error-location"></span>
                                <input type="hidden" name="lat" value="{{$project->lat}}" id="lat">
                                <input type="hidden" name="lng" value="{{$project->lng}}" id="lng">
                                <i class="fa fa-map-marker-alt" id="myloc" title="تحديد موقعك الحالي"></i>
                                <div id="map" style="width:100%;height:500px;"></div>
                            </div>
                            <span class="error-input error-location">{{ $errors->first('location') }}</span>
                            <span class="error-input error-lat">{{ $errors->first('lat') }}</span>
                            <span class="error-input error-lng">{{ $errors->first('lng') }}</span>
                            <div class="row">
                                <div class="col-md-4">
                            <!-- COUNTRY -->
                            <div class="form-group">
                                <label for="select-country">{{ __('admin.country') }}</label>
                                <select class="form-control select2" id="country" name="country">
                                    <option value="">{{ __('admin.choose_country') }}</option>

                                    @foreach($countries as $key => $country)

                                        <option value="{{ $key }}" @if($key == $project->country) selected @endif>{{ $country }}</option>

                                    @endforeach
                                </select>
                                <span class="error-input error-country">{{ $errors->first('country') }}</span>
                            </div>
                                </div>
                            
                            <div class="col-md-4">
                                      <!-- CITY -->
                            <div class="form-group">
                                <label class="form-label" for="city">{{ __('admin.city') }}</label>
                                <input type="text" value="{{$project->city}}" class="form-control" name="city" id="city" />
                                <span class="error-input error-city">{{ $errors->first('city') }}</span>
                            </div>
                            </div>

                            <div class="col-md-4">
                                 <!-- BENEF NUM -->
                            <div class="form-group">
                                <label class="form-label" for="dob">{{ __('admin.benef_num') }}</label>
                                <input type="number" min="0" step="1" class="form-control" name="benef_num" value="{{$project->benef_num}}" id="benef_num" />
                                <span class="error-input error-benef_num">{{ $errors->first('benef_num') }}</span>
                            </div>
                            </div>

                            <!--  BEGIN: goals -->

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="goals_ar">{{ __('admin.goals_ar') }}</label>
                                        <textarea class="form-control" id="goals_ar" name="goals_ar" rows="3">{{$project->goals_ar}}</textarea>
                                        <span class="error-input error-goals_ar">{{ $errors->first('goals_ar') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="goals_en">{{ __('admin.goals_en') }}</label>
                                        <textarea class="form-control" id="goals_en" name="goals_en" rows="3">{{$project->goals_en}}</textarea>
                                        <span class="error-input error-goals_en">{{ $errors->first('goals_en') }}</span>
                                    </div>
                                </div>  

                            <!--  END: goals -->

                            <!--  BEGIN: bio -->

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="desc_ar">{{ __('admin.desc_ar') }}</label>
                                        <textarea class="form-control" id="desc_ar" name="desc_ar" rows="3" required>{{$project->desc_ar}}</textarea>
                                        <span class="error-input error-desc_ar">{{ $errors->first('desc_ar') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="desc_en">{{ __('admin.desc_en') }}</label>
                                        <textarea class="form-control" id="desc_en" name="desc_en" rows="3" required>{{$project->desc_en}}</textarea>
                                        <span class="error-input error-desc_en">{{ $errors->first('desc_en') }}</span>
                                    </div>
                                </div>  

                            <!--  END: bio -->

                            <!--  BEGIN: desc -->

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="long_desc_ar">{{ __('admin.long_desc_ar') }}</label>
                                        <textarea class="form-control" id="long_desc_ar" name="long_desc_ar" rows="3">{{$project->long_desc_ar}}</textarea>
                                        <span class="error-input error-long_desc_ar">{{ $errors->first('long_desc_ar') }}</span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="long_desc_en">{{ __('admin.long_desc_en') }}</label>
                                        <textarea class="form-control" id="long_desc_en" name="long_desc_en" rows="3">{{$project->long_desc_en}}</textarea>
                                        <span class="error-input error-long_desc_en">{{ $errors->first('long_desc_en') }}</span>
                                    </div>
                                </div>  

                            <!--  END: desc -->                            
                            
                            
                            <div class="col-md-6">
                                <div class="form-group">
                                <label for="select-country">{{ __('admin.currency') }}</label>
                                <select class="form-control select2"  id="currency_id" name="currency_id">
                                    <option value="">{{ __('admin.choose_currency') }}</option>

                                    @foreach($currencies as $currency)

                                        <option value="{{ $currency->id }}" @if($project->currency_id == $currency->id) selected @endif>{{ $currency->name }}</option>

                                    @endforeach
                                </select>
                                <span class="error-input error-currency_id">{{ $errors->first('currency_id') }}</span>
                            </div>    
                            </div>                                                   

                            <div class="col-md-6">
                                <!-- COST -->
                            <div class="form-group">
                                <label class="form-label" for="dob">{{ __('admin.project_cost') }}</label>
                                <input type="number" step="0.1" class="form-control" value="{{$project->cost}}" name="cost" id="cost" />
                                <span class="error-input error-cost">{{ $errors->first('cost') }}</span>
                            </div>  
                            </div>            

                            <!-- image -->
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('admin.project_main_image') }}
                                    (Height:200px / Min-width:400px)
                                </label>
                                <div class="custom-file">
                                    <input type="file" class="form-control image" id="image" accept=".jpg,.jpeg,.png" name="image" />
                                    <small>(Max size: 20MG)</small>
                                    <span class="error-input error-image">{{ $errors->first('image') }}</span>
                                </div>
                                <div class="form-group prev" style="display: inline-block">
                                    <img src="{{asset('storage/'.$project->image)}}"  height="100" class="img-thumbnail preview-image" alt="">
                                </div>                                
                            </div>
                            </div>  

                            <!-- IMAGES -->
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('admin.project_images') }}</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control" id="images" name="images[]" accept=".jpg,.jpeg,.png" multiple />
                                    <small>(Max size: 20MG)</small>
                                    <span class="error-input error-images">{{ $errors->first('images') }}</span>
                                    {{-- <div id="db_images" class="my-1">
                                        @if(count($project->images) > 0)
                                            @foreach ($project->images as $image)
                                                <img src="{{asset('storage/'.$image->path)}}" width="100" height="100" class="img-thumbnail preview-image" alt="">
                                            @endforeach
                                        @endif
                                    </div> --}}
                                </div>
                            </div>
                            </div>
                            <!-- ATTACH -->
                            <div class="col-md-6">
                                <div class="form-group">
                                <label>{{ __('admin.attach') }}</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control" accept="application/pdf" id="attach" name="attach" />
                                    <small>(Max size: 50MG)</small>
                                    <span class="error-input error-attach">{{ $errors->first('attach') }}</span>
                                    <a href="{{asset('storage/'.$project->attach)}}" class="my-1 d-block">{{trans('admin.attach')}}</a>
                                </div>
                            </div>                          
                            </div>
                          
                            <div class="col-12 error text-center general_error"></div>
                                <div class="col-12" id="loading">
                                    <button type="button" class="btn btn-primary submit_button"> {{ __('admin.create') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /jQuery Validation -->


        </div>
    </section>
    <!-- /Validation -->





@endsection

@section('scripts')

<script src="{{ url('charity/js/projects/map.js') }}"></script>
<script src="{{ url('charity/js/projects/edit-project.js') }}"></script>
<script src="{{ url('charity/js/projects/submit_forms.js') }}"></script>
<script src="{{ url('custom/preview-image.js') }}"></script>
<script src="{{ url('custom/preview-multible-image.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBdarVlRZOccFIGWJiJ2cFY8-Sr26ibiyY&libraries=places&callback=initAutocomplete&language=<?php echo e('ar'); ?>"
    defer></script>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>
    
@if(\App::getLocale() == 'ar')
    <!--for arabic-only-->
    <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
    <!--end-->
@endif     
<script>
     $('.date-picker').datepicker({
        changeYear: true,
        dateFormat: 'yy-mm-dd',
    })
</script>
<script>

    // ckeditor custom

    CKEDITOR.replace( 'goals_ar' );
    CKEDITOR.replace( 'goals_en' );
    CKEDITOR.replace( 'long_desc_ar' );
    CKEDITOR.replace( 'long_desc_en' );    

    
</script>

@endsection






