<div class="col-md-12">
    <div class="form-group">
        <label for="select-country">{{ __('admin.city') }}</label>
        <select class="form-control select2" id="city_id" name="city_id">
            @foreach($cities as $city)

                <option value="{{ $city->id }}" {{ old('city_id') }}>{{ $city->name }}</option>

            @endforeach
        </select>
    </div>
</div> 