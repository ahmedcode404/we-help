@if($edus->isNotEmpty())
    <div class="form-group">
        <label for="select-country">{{ __('admin.servies_edu') }}</label>
        <select class="form-control edu_service_id" id="edu_service_id" name="edu_service_id">
        <option value="">{{ __('admin.choose_service') }}</option>

            @foreach($edus as $edu)

                <option value="{{ $edu->id }}">{{ $edu->name }}</option>

            @endforeach
            <option value="other">{{ __('admin.other_service') }}</option>
        </select>
        <span class="error-input"></span>
    </div>
@endif 