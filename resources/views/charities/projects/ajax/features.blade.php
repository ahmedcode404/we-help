@if(count($data['features']) > 0)
    <label for="select-country">{{ __('admin.feature') }}</label>
    <select class="form-control select2" id="service_feature_id" name="service_feature_id" required>
        <option value="">{{trans('admin.select')}}...</option>
        @foreach($data['features'] as $feature)
            <option value="{{ $feature->id }}">{{ $feature->name }}</option>
        @endforeach
        <option value="other">{{trans('admin.other')}}</option>
    </select>
    @if ($errors->has('service_feature_id'))
        <span class="error-input">{{ $errors->first('service_feature_id') }}</span>
    @endif
@endif