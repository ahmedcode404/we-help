@if(count($data['services']) > 0)
    <label for="select-country">{{ trans('admin.service') }}</label>
    <select class="form-control select2" id="service_option_id" data-action="{{route('get-service-features')}}" 
        data-method="POST" name="service_option_id" required>
        <option value="">{{trans('admin.select')}}...</option>
        @foreach($data['services'] as $service)
            <option value="{{ $service->id }}">{{ $service->name }}</option>
        @endforeach
        <option value="other">{{trans('admin.other')}}</option>
    </select>
    @if ($errors->has('service_option_id'))
        <span class="error-input">{{ $errors->first('service_option_id') }}</span>
    @endif
@endif