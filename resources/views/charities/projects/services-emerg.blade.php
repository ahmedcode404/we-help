@if($emergs->isNotEmpty())
    <div class="form-group">
        <label for="select-country">{{ __('admin.servies_emerg') }}</label>
        <select class="form-control emerg_service_id" id="emerg_service_id" name="emerg_service_id">
        <option value="">{{ __('admin.choose_service') }}</option>

            @foreach($emergs as $emerg)

                <option value="{{ $emerg->id }}">{{ $emerg->name }}</option>

            @endforeach
            <option value="other">{{ __('admin.other_service') }}</option>
        </select>
        <span class="error-input"></span>
    </div>
@endif 