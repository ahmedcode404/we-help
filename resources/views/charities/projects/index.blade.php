@extends('layout.app')
@section('title')
{{ __('admin.projects') }}
@endsection
@section('content')


<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <a href="{{route('charity.projects.create')}}" class="btn btn-primary">{{trans('admin.create_project')}}</a>
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.project_num')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.category')}}</th>
                            <th>{{trans('admin.active_status')}}</th>
                            <th>{{trans('admin.create_phase')}}</th>
                            <th>{{trans('admin.create_report')}}</th>
                            <th>{{trans('admin.status')}}</th>
                            <th>{{trans('admin.notes')}}</th>
                            {{-- <th>{{ trans('admin.active') }}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($projects as $project)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$project->project_num}}</td>
                                <td>
                                    <a href="{{route('charity.projects.show', $project->slug)}}">
                                        {{$project->name}}
                                    </a>
                                </td>
                                <td>{{$project->category ? $project->category->name : ' - '}}</td>
                                <td>
                                    @if($project->active == 1)
                                        <div class="badge badge-success">{{ trans('admin.active') }}</div>
                                    @else
                                        <div class="badge badge-danger">{{ trans('admin.not_active') }}</div>
                                    @endif
                                </td>
                                <td>    
                                    <a href="{{ route('charity.phases.create') }}">{{ trans('admin.create_phase') }}</a>                                    
                                </td>

                                <td>
                                    <a href="{{route('charity.phase', ['id' => $project->slug, 'type' => 'project'])}}">{{ trans('admin.create_report') }}</a> 
                                </td>

                                @if($project->status == 'waiting')

                                    <td> <div class="badge badge-secondary">{{ __('admin.waiting') }}</div> </td>

                                @elseif($project->status == 'approved')

                                    <td> <div class="badge badge-success">{{ __('admin.approved') }}</div> </td>

                                @elseif($project->status == 'rejected')

                                    <td> <div class="badge badge-info">{{ __('admin.rejected') }}</div> </td>

                                @elseif($project->status == 'hold')

                                    <td> <div class="badge badge-primary">{{ __('admin.hold') }}</div> </td>

                                @elseif($project->status == 'closed')

                                    <td> <div class="badge badge-danger">{{ __('admin.closed') }}</div> </td>

                                @endif

                                <td>
                                    @if($project->notes)
                                        <a href="#" data-toggle="modal" data-target="#info-{{ $project->id }}" title="{{trans('admin.show')}}">
                                        {{trans('admin.show')}}
                                        </a>
                                    @else
                                        {{ trans('admin.no_data') }}
                                    @endif

                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="info-{{ $project->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div>
                                                        {{ $project->notes }}
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-info" data-dismiss="modal">{{trans('admin.close')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <a href="{{route('charity.projects.show', $project->slug)}}" title="{{trans('admin.show')}}">
                                        <i data-feather="eye" class="mr-50"></i>
                                    </a>
                                    
                                    @if($project->status == 'waiting' || checkPermissions(['edit_project_' . $project->id]))
                                       
                                        @if($project->status == 'waiting' || checkPermissions(['edit_project_' . $project->id]))
                                            <a href="{{route('charity.projects.edit', $project->slug)}}" title="{{trans('admin.edit')}}">
                                                <i data-feather="edit-2" class="mr-50"></i>
                                            </a>
                                        @endif

                                        @if($project->status == 'waiting' && $project->active == 1)
                                            <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('charity.projects.destroy', $project->id)}}">
                                                <i data-feather="trash" class="mr-50"></i>
                                            </a>                                            
                                        @endif 

                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{ $projects->links() }}
        </div>
    </div>
    
</section>
<!--/ Basic table -->


@endsection

@section('scripts')

<script src="{{ url('custom/custom-sweetalert.js') }}"></script>


@endsection






