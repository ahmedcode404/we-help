@extends('layout-login.app')
@section('title')
{{ __('admin.signup') }}
@endsection
@section('content')


    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <div class="auth-wrapper auth-v2">
                    <div class="auth-inner row m-0">
                        <!-- Brand logo--><a class="brand-logo" href="javascript:void(0);">
                            <svg viewBox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="28">
                                <defs>
                                    <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%" y2="89.4879456%">
                                        <stop stop-color="#000000" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                    <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%" y2="100%">
                                        <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                        <stop stop-color="#FFFFFF" offset="100%"></stop>
                                    </lineargradient>
                                </defs>
                                <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                        <g id="Group" transform="translate(400.000000, 178.000000)">
                                            <path class="text-primary" id="Path" d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z" style="fill: currentColor"></path>
                                            <path id="Path1" d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z" fill="url(#linearGradient-1)" opacity="0.2"></path>
                                            <polygon id="Path-2" fill="#000000" opacity="0.049999997" points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325"></polygon>
                                            <polygon id="Path-21" fill="#000000" opacity="0.099999994" points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338"></polygon>
                                            <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994" points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <h2 class="brand-text text-primary ml-1">Vuexy</h2>
                        </a>
                        <!-- /Brand logo-->
                        <!-- Left Text-->
                        <div class="d-none d-lg-flex col-lg-4 align-items-center p-5">
                            <div class="w-100 d-lg-flex align-items-center justify-content-center px-5"><img class="img-fluid" src="{{ url('dashboard/app-assets/images/pages/register-v2.svg') }}" alt="Register V2" /></div>
                        </div>
                        <!-- /Left Text-->
                        <!-- Register-->
                        <div class="d-flex col-lg-8 align-items-center auth-bg px-2 p-lg-5">
                            <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">

                                <h2 class="card-title font-weight-bold mb-1">Adventure starts here 🚀</h2>
                                <p class="card-text mb-2">Make your app management easy and fun!</p>

                                <form class="auth-register-form mt-2" action="index.html" method="POST">

                                    <div class="form-group">
                                        <label class="form-label" for="type">{{ __('admin.type') }}</label>
                                        <select name="type" id="type" class="form-control">
                                            <option value="">{{ __('admin.choose_type') }}</option>
                                            @foreach($types as $key=>$type)
                                            <option value="{{ $type_en[$key] }}">{{ $type }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="name">{{ __('admin.name') }}</label>
                                        <input class="form-control" id="name" type="text" name="name" placeholder="{{ __('admin.name') }}" />
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="form-label" for="license_number">{{ __('admin.license_number') }}</label>
                                        <input class="form-control" id="license_number" type="number" name="license_number" placeholder="{{ __('admin.license_number') }}" />
                                    </div>    
                                    
                                    <div class="form-group">
                                        <label class="form-label" for="license_start_date">{{ __('admin.license_start_date') }}</label>
                                        <input class="form-control" id="license_start_date" type="date" name="license_start_date" placeholder="{{ __('admin.license_start_date') }}" />
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="form-label" for="license_end_date">{{ __('admin.license_end_date') }}</label>
                                        <input class="form-control" id="license_end_date" type="date" name="license_end_date" placeholder="{{ __('admin.license_end_date') }}" />
                                    </div>   
                                    
                                    <div class="form-group">
                                        <label class="form-label" for="establish_date">{{ __('admin.establish_date') }}</label>
                                        <input class="form-control" id="establish_date" type="date" name="establish_date" placeholder="{{ __('admin.establish_date') }}" />
                                    </div>  
                                    
                                    <div class="form-group">
                                        <label class="form-label" for="branches_num">{{ __('admin.branches_num') }}</label>
                                        <input class="form-control" id="branches_num" type="number" name="branches_num" placeholder="{{ __('admin.branches_num') }}" />
                                    </div>     
                                    
                                    <div class="form-group">
                                        <label class="form-label" for="members_num">{{ __('admin.members_num') }}</label>
                                        <input class="form-control" id="members_num" type="number" name="members_num" placeholder="{{ __('admin.members_num') }}" />
                                    </div>                                         

                                    <div class="form-group">
                                        <label class="form-label" for="contract_date">{{ __('admin.contract_date') }}</label>
                                        <input class="form-control" id="contract_date" type="date" name="contract_date" placeholder="{{ __('admin.branches_num') }}" />
                                    </div>                                         

                                    
                                    <!--  BEGIN: STARTIGY -->
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="d-block" for="strategy_ar">{{ __('admin.strategy_ar') }}</label>
                                                <textarea class="form-control" id="strategy_ar" name="strategy_ar" rows="3"></textarea>
                                                <span class="error-input"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="d-block" for="strategy_en">{{ __('admin.strategy_en') }}</label>
                                                <textarea class="form-control" id="strategy_en" name="strategy_en" rows="3"></textarea>
                                                <span class="error-input"></span>
                                            </div>
                                        </div>                                        

                                    </div>                                        
                                    <!--  END: STARTIGY  -->                                      



                                    <!-- ATTACH -->
                                    <div class="form-group">
                                        <label>{{ __('admin.attach_charity') }}</label>
                                        <div class="custom-file">
                                            <input type="file" class="form-control image" id="attach" name="attach" />
                                            <span class="error-input"></span>
                                        </div>
                                    </div>







                                    <div class="form-group">
                                        <label class="form-label" for="register-email">Email</label>
                                        <input class="form-control" id="register-email" type="text" name="register-email" placeholder="john@example.com" aria-describedby="register-email" tabindex="2" />
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="register-password">Password</label>
                                        <div class="input-group input-group-merge form-password-toggle">
                                            <input class="form-control form-control-merge" id="register-password" type="password" name="register-password" placeholder="············" aria-describedby="register-password" tabindex="3" />
                                            <div class="input-group-append"><span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input class="custom-control-input" id="register-privacy-policy" type="checkbox" tabindex="4" />
                                            <label class="custom-control-label" for="register-privacy-policy">I agree to<a href="javascript:void(0);">&nbsp;privacy policy & terms</a></label>
                                        </div>
                                    </div>

                                    <button class="btn btn-primary btn-block" tabindex="5">Sign up</button>
                                
                                </form>
                                <p class="text-center mt-2"><span>Already have an account?</span><a href="page-auth-login-v2.html"><span>&nbsp;Sign in instead</span></a></p>
                                <div class="divider my-2">
                                    <div class="divider-text">or</div>
                                </div>
                                <div class="auth-footer-btn d-flex justify-content-center"><a class="btn btn-facebook" href="javascript:void(0)"><i data-feather="facebook"></i></a><a class="btn btn-twitter white" href="javascript:void(0)"><i data-feather="twitter"></i></a><a class="btn btn-google" href="javascript:void(0)"><i data-feather="mail"></i></a><a class="btn btn-github" href="javascript:void(0)"><i data-feather="github"></i></a></div>
                            </div>
                        </div>
                        <!-- /Register-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->


@endsection

@section('script_auth')

<!-- <script src="{{ url('charity/js/reports/create.js') }}"></script>
<script src="{{ url('custom/custom-validate.js') }}"></script>
<script src="{{ url('custom/preview-multible-image.js') }}"></script> -->
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>

    // ckeditor custom

    CKEDITOR.replace( 'strategy_ar' );
    CKEDITOR.replace( 'strategy_en' );
    

    
</script>

@endsection


