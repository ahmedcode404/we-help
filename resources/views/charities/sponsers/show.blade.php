@extends('layout.app')
@section('title')
{{ __('admin.sponsers') }}
@endsection
@section('content')


<section class="app-user-view">

    <!-- User Card & Plan Starts -->
    <div class="row">
        <!-- User Card starts-->
        <div class="col-xl-12">
            <div class="card user-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                            <div class="user-avatar-section">
                                <div class="d-flex justify-content-start">
                                    <div class="d-flex flex-column ml-1">
                                        <div class="user-info mb-1">
                                            <h4 class="mb-0">{{ $sponser_one->name }}</h4>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                        
                                            @if(checkPermissions(['edit_sponser' . $sponser_one->id]))
                                                <a href="{{route('charity.phases.edit', $sponser_one->id)}}" class="btn btn-primary">{{ __('admin.edit') }}</a>
                                            @endif
                                            <!-- <a href="" class="btn btn-outline-danger ml-1">Edit</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                            <div class="user-info-wrapper">
                                <div class="d-flex flex-wrap">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.name_sponser_ar') }}</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $sponser_one->name_ar }}</p>
                                </div>
                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.name_sponser_en') }}  </span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">  {{ $sponser_one->name_en }} </p>
                                </div>
                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.work') }}  </span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">  {{ $sponser_one->work_field->name }} </p>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /User Card Ends-->

    </div>
    <!-- User Card & Plan Ends -->

</section>


@endsection

@section('scripts')

<script>

    
</script>

@endsection






