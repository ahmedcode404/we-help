@extends('layout.app')
@section('title')
{{ __('admin.add_sponser') }}
@endsection
@section('content')


    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">

            <!-- jQuery Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('admin.create') }}</h4>
                    </div>
                    <div class="card-body">

                        <form id="form-sponser" method="post" enctype="multipart/form-data">

                            <!-- NAME PROJECT -->
                            <div class="form-group">
                                <label for="select-country">{{ __('admin.num_project') }}</label>
                                <select class="form-control" id="project_id" url="{{ route('charity.get.start.date.project') }}" name="project_id" required>
                                    <option value="">{{ __('admin.choose_project') }}</option>

                                    @foreach($projects as $project)

                                        <option value="{{ $project->id }}">{{ $project->project_num }}</option>

                                    @endforeach
                                </select>
                                <span class="error-input"></span>
                            </div>
                            

                                
                            <!-- ORDER -->
                            <div class="form-group">
                                <label for="select-country">{{ __('admin.work') }}</label>
                                <select class="form-control" id="work_field_id" name="work_field_id" required>
                                    <option value="">{{ __('admin.choose_work') }}</option>

                                    @foreach($works as $key=>$work)

                                        <option value="{{ $work->id }}">{{ $work->name }}</option>

                                    @endforeach

                                </select>
                                <span class="error-input"></span>
                            </div>


                            <div class="row">


                                <!-- NAME AR -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_ar">{{ __('admin.name_sponser_ar') }}</label>
                                        <input type="text" id="name_sponser_ar" name="name_sponser_ar" class="form-control" placeholder="{{ __('admin.name_sponser_ar') }}" />
                                        <span class="error-input"></span>
                                    </div> 
                                </div> 
                                
                                <!-- NAME EN -->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="name_sponser_en">{{ __('admin.name_sponser_en') }}</label>
                                        <input type="text" id="name_sponser_en" name="name_sponser_en" class="form-control" placeholder="{{ __('admin.name_sponser_en') }}" />
                                        <span class="error-input"></span>
                                    </div> 
                                </div>                             

                            </div>                                                       

                        
                            <div class="row">
                                <div class="col-12">
                                    <button type="button" id="click-form-sponser" redirect="" url="{{ route('charity.sponsers.store') }}" class="btn btn-primary" name="submit" value="Submit">{{ __('admin.create') }} </button>
                                </div>
                                                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /jQuery Validation -->


        </div>
    </section>
    <!-- /Validation -->





@endsection

@section('scripts')

    <script src="{{ url('charity/js/sponsers/create.js') }}"></script>
    <script src="{{ url('custom/custom-validate.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

@endsection






