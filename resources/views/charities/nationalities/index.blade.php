@extends('layout.app')

@section('title', trans('admin.nationalities'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">

                @php
                    if(auth()->user()->hasRole('charity_employee')){
                        $user = auth()->user()->charity;
                    }
                    else{
                        $user = auth()->user();
                    }
                @endphp

                @if(checkPermissions(['create_nationalities_'.$user->id]))
                    <a href="{{route('charity.nationalities.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif

                <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_ar')}}</th>
                            <th>{{trans('admin.name_en')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['nationalities'] as $nationality)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$nationality->name_ar}}</td>
                                <td>{{$nationality->name_en}}</td>
                                <td>
                                            @if(checkPermissions(['show_nationalities_'.$user->id]))
                                                <a href="{{route('charity.nationalities.show', $nationality->slug)}}" title="{{trans('admin.show')}}">
                                                    <i data-feather="eye" class="mr-50"></i>
                                                </a>
                                            @endif
                                            @if(checkPermissions(['edit_nationalities_'.$user->id]))
                                                <a href="{{route('charity.nationalities.edit', $nationality->slug)}}" title="{{trans('admin.edit')}}">
                                                    <i data-feather="edit-2" class="mr-50"></i>
                                                </a>
                                            @endif
                                            @if(checkPermissions(['delete_nationalities_'.$user->id]))
                                                <a class="remove-table" href="javascript:void(0);" data-url="{{route('charity.nationalities.destroy', $nationality->id)}}" title="{{trans('admin.delete')}}">
                                                    <i data-feather="trash" class="mr-50"></i>
                                                </a>
                                            @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{$data['nationalities']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection