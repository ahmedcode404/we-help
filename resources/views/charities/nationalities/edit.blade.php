@extends('layout.app')

@section('title', trans('admin.edit_nationality'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{trans('admin.edit')}}</h4>
                    </div>
                    <div class="card-body">
                        <form id="job-categories" method="POST" action="{{route('charity.nationalities.update', $data['nationality']->id)}}">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label class="form-label" for="name_ar">{{trans('admin.name_ar')}}</label>
                                <input type="text" class="form-control" id="name_ar" name="name_ar" value="{{$data['nationality']->name_ar ? $data['nationality']->name_ar : old('name_ar')}}" placeholder="{{trans('admin.name_ar')}}" required />
                            </div>
                            <div class="form-group">
                                <label class="form-label" for="name_en">{{trans('admin.name_en')}}</label>
                                <input type="text" id="name_en" name="name_en" class="form-control" value="{{$data['nationality']->name_en ? $data['nationality']->name_en : old('name_en')}}" placeholder="{{trans('admin.name_en')}}" required />
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary" >{{trans('admin.save')}}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection

@push('js')

    <script src="{{asset('dashboard/forms/natinalities/create.js')}}"></script>
    
@endpush