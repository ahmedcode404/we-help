@extends('layout.app')

@section('title', trans('admin.employees'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_employees_'.auth()->user()->id]))
                    <a href="{{route('charity.employees.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif            
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_employee_ar')}}</th>
                            <th>{{trans('admin.name_employee_en')}}</th>
                            <th>{{trans('admin.nation_number')}}</th>
                            <th>{{trans('admin.country')}}</th>
                            {{-- <th>{{ trans('admin.show') }}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($employees as $employee)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $employee->name_ar }}</td>
                                <td>{{ $employee->name_en }}</td>
                                <td>{{ $employee->nation_number }}</td>
                                <td>{{ Countries::getOne($employee->country, \App::getLocale()) }}</td>
                                <td>
                                    @if(checkPermissions(['show_employees_'.auth()->user()->id]))
                                        <a  href="{{route('charity.employees.show', $employee->slug)}}" title="{{trans('admin.show')}}">
                                            <i data-feather="eye" class="mr-50"></i>
                                        </a>
                                    @endif
                               
                                    @if(checkPermissions(['edit_employees_'.auth()->user()->id]) || checkPermissions(['delete_employees_'.auth()->user()->id]))
                                 
                                                @if(checkPermissions(['edit_employees_'.auth()->user()->id]))
                                                    <a  href="{{route('charity.employees.edit', $employee->slug)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                     </a>
                                                @endif
                                                @if(checkPermissions(['delete_employees_'.auth()->user()->id]))
                                                    <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('charity.employees.destroy', $employee->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
                {{$employees->links()}}
            </div>
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection


@push('js')
    <script src="{{asset('dashboard/forms/main/change-status.js')}}"></script>
@endpush