
@if($phaseFirstProject->isNotEmpty()) 

    <div class="col-md-12">
        <div class="form-group">
            <label class="form-label" for="basic-default-password">{{ __('admin.start_date') }}</label>
            <input type="text" id="start_date" name="start_date" class="form-control calculate date-picker" readonly  />
            <span class="error-input"></span>
        </div>    
    </div>

@else
    <div class="col-md-12">
        <div class="form-group">
            <label class="form-label" for="basic-default-password">{{ __('admin.start_date') }}</label>
            <input type="text" id="start_date" readonly value="{{ $project->start_date }}" name="start_date" class="form-control calculate date-picker" readonly  />
            <span class="error-input"></span>
        </div>    
    </div>
@endif



