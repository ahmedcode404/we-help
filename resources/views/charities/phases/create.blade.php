@extends('layout.app')

@section('title')
{{ __('admin.add_phases') }}
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
     
@endpush

@section('content')


    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">

            <!-- jQuery Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('admin.create') }}</h4>
                    </div>
                    <div class="card-body">

                        <form id="form-phases" method="post" enctype="multipart/form-data">



                        <!-- NAME PROJECT -->
                        <div class="form-group">
                            <label for="select-country">{{ __('admin.num_project') }}</label>
                            <select class="form-control select2" id="project_id" url="{{ route('charity.get.start.date.project') }}" name="project_id">
                                <option value="">{{ __('admin.choose_project') }}</option>

                                @foreach($projects as $project)

                                    <option value="{{ $project->id }}">{{ $project->project_num }} - {{ $project->name }}</option>

                                @endforeach
                            </select>
                            <span class="error-input"></span>
                        </div>
                        

                        <div class="row">


                            <!-- NAME AR -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="name_ar">{{ __('admin.name_ar') }}</label>
                                    <input type="text" id="name_ar" name="name_ar" class="form-control" placeholder="{{ __('admin.name_ar') }}" />
                                    <span class="error-input"></span>
                                </div> 
                            </div> 
                            
                            <!-- NAME EN -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="name_en">{{ __('admin.name_en') }}</label>
                                    <input type="text" id="name_en" name="name_en" class="form-control" placeholder="{{ __('admin.name_en') }}" />
                                    <span class="error-input"></span>
                                </div> 
                            </div>                             

                        </div>

                        <!-- STRAT DATE -->
                        <div class="row" id="input-created-at"></div>                        


                        <div class="row">


                            <!-- END DATE -->
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label" for="basic-default-password">{{ __('admin.end_date') }}</label>
                                    <input type="text" id="end_date" name="end_date"  class="form-control calculate date-picker" readonly   autocomplete="off"  />
                                    <span class="error-input"></span>
                                </div>    
                            </div>                            

                        </div>

                        

                            <!-- DURATION -->
                            <div class="form-group">
                                <label class="form-label" for="dob">{{ __('admin.duration_work') }}</label>
                                <input type="number" readonly class="form-control" name="duration" id="duration" />
                                <span class="error-input"></span>
                            </div>

                            <!-- COST -->
                            <div class="form-group">
                                <label class="form-label" for="dob">{{ __('admin.cost') }}</label>
                                <input type="number" min="0" step="1" class="form-control" name="cost" id="cost" />
                                <span class="error-input"></span>
                            </div> 
                            
                            <!-- ORDER -->
                            <div class="form-group">
                                <label for="select-country">{{ __('admin.order') }}</label>
                                <select class="form-control select2" id="order" name="order">
                                    <option value="">{{ __('admin.choose_order') }}</option>

                                    @foreach($orders as $key=>$order)

                                        <option value="{{ $order_en[$key] }}">{{ $order }}</option>

                                    @endforeach
                                </select>
                                <span class="error-input"></span>
                            </div>                           

                            <!--  BEGIN: goals -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="output_ar">{{ __('admin.output_ar') }}</label>
                                        <textarea class="form-control" id="output_ar" name="output_ar" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="output_en">{{ __('admin.output_en') }}</label>
                                        <textarea class="form-control" id="output_en" name="output_en" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>  

                            </div>
                            <!--  END: goals -->

                            <!--  BEGIN: bio -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="desc_ar">{{ __('admin.desc_ar') }}</label>
                                        <textarea class="form-control" id="desc_ar" name="desc_ar" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="desc_en">{{ __('admin.desc_en') }}</label>
                                        <textarea class="form-control" id="desc_en" name="desc_en" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>  

                            </div>                                        
                            <!--  END: bio -->

                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="cost_details_ar">{{ __('admin.cost_details_ar') }}</label>
                                        <textarea class="form-control" id="cost_details_ar" name="cost_details_ar" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="d-block" for="cost_details_en">{{ __('admin.cost_details_en') }}</label>
                                        <textarea class="form-control" id="cost_details_en" name="cost_details_en" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>  

                            </div>                             
                                       
                            <!--  END: desc -->                            

                            <div class="row">
                                <div class="col-12">
                                    <button type="button" id="click-form-phases" redirect="" url="{{ route('charity.phases.store') }}" class="btn btn-primary" name="submit" value="Submit">{{ __('admin.create') }} </button>
                                </div>
                                                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /jQuery Validation -->


        </div>
    </section>
    <!-- /Validation -->





@endsection

@section('scripts')

<script src="{{ url('charity/js/phases/create.js') }}"></script>
<script src="{{ url('custom/custom-validate.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>
<script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>
    
@if(\App::getLocale() == 'ar')
    <!--for arabic-only-->
    <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
    <!--end-->
@endif     
<script>
     $('.date-picker').datepicker({
        changeYear: true,
        dateFormat: 'yy-mm-dd',
    })
</script>
<script>

    // ckeditor custom

    CKEDITOR.replace( 'output_ar' );
    CKEDITOR.replace( 'output_en' );
    CKEDITOR.replace( 'desc_ar' );
    CKEDITOR.replace( 'desc_en' );
    CKEDITOR.replace( 'cost_details_ar' );
    CKEDITOR.replace( 'cost_details_en' );    

    
</script>

@endsection






