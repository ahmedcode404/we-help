@extends('layout.app')
@section('title')
{{ __('admin.phases') }}
@endsection
@section('content')


<section class="app-user-view">

    <!-- User Card & Plan Starts -->
    <div class="row">
        <!-- User Card starts-->
        <div class="col-xl-12">
            <div class="card user-card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-6 col-lg-12 d-flex flex-column justify-content-between border-container-lg">
                            <div class="user-avatar-section">
                                <div class="d-flex justify-content-start">
                                    <div class="d-flex flex-column">
                                        <div class="user-info mb-1">
                                            <h4 class="mb-0">{{ $phase_one->name }}</h4>
                                        </div>
                                        <div class="d-flex flex-wrap">
                                        
                                            @if(checkPermissions(['edit_project_data']))
                                                <a href="{{route('charity.phases.edit', $phase_one->id)}}" class="btn btn-primary">{{ __('admin.edit') }}</a>
                                            @endif
                                            <!-- <a href="" class="btn btn-outline-danger ml-1">Edit</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-xl-6 col-lg-12 mt-2 mt-xl-0">
                            <div class="user-info-wrapper">
                                <div class="d-flex flex-wrap">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.start_date') }}</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $phase_one->start_date }}</p>
                                </div>
                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.end_date') }}  </span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">  {{ $phase_one->end_date }} </p>
                                </div>
                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.duration_work') }}</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $phase_one->duration }} @if($phase_one->duration <= 2) {{ __('admin.day') }} @elseif($phase_one->duration > 2 && $phase_one->duration <= 10) {{ __('admin.days') }} @else {{ __('admin.day') }} @endif </p>
                                </div>
                                <div class="d-flex flex-wrap my-50">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.cost') }}</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $phase_one->cost }}</p>
                                </div>
                                <div class="d-flex flex-wrap">
                                    <div class="user-info-title">
                                        <i data-feather="check" class="mr-1"></i>
                                        <span class="card-text user-info-title font-weight-bold mb-0">{{ __('admin.order') }}</span>
                                    </div>
                                    <p class="card-text mb-0 ml-2">{{ $phase_one->order }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /User Card Ends-->

    </div>
    <!-- User Card & Plan Ends -->

    <div class="row match-height">


        <!-- Company Table Card -->
        <div class="col-lg-12 col-12">
            <div class="card card-company-table">
                <div class="card-body p-0">
                    <h1 style="margin: 12px;">{{ __('admin.reports') }}</h1>
                    <div class="table-responsive">
                        <table class="datatables-basic table">
                            <thead>
                                <tr>
                                    <th>{{ __('admin.phase_name') }}</th>
                                    <th>{{ __('admin.project_name') }}</th>
                                    <th>{{ __('admin.vedio') }}</th>
                                    <th>{{ __('admin.status') }}</th>
                                    {{-- <th>{{ __('admin.show') }}</th> --}}
                                    <th>{{ __('admin.actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($phase_one->reports as $key=>$report)
                                    <tr>
                                        <td>{{ $report->phase->name }}</td>
                                        <td>{{ $report->project->name }}</td>
                                        <td><a href="{{ asset('storage/'.$report->vedio) }}" target="_blank">{{trans('admin.show')}}</a></td>

                                        @if($report->status == 'waiting')

                                            <td> <div class="badge badge-secondary">{{ __('admin.waiting') }}</div> </td>

                                        @elseif($report->status == 'approved')

                                            <td> <div class="badge badge-primary">{{ __('admin.approved') }}</div> </td>
                                            
                                        @elseif($report->status == 'rejected')

                                            <td> <div class="badge badge-primary">{{ __('admin.rejected') }}</div> </td>

                                        @elseif($report->status == 'hold')

                                            <td> <div class="badge badge-primary">{{ __('admin.hold') }}</div> </td>

                                        @elseif($report->status == 'closed')

                                            <td> <div class="badge badge-primary">{{ __('admin.closed') }}</div> </td>

                                        @endif

                                        <td>
                                            <a  href="{{route('charity.reports.show', $report->slug)}}" title="{{trans('admin.show')}}">
                                                <i data-feather="eye" class="mr-50"></i>
                                            </a>
                                       
                                            @if($report->status == 'waiting' || 
                                            (checkPermissions(['edit_reports_' . $report->id]) && checkPermissions(['delete_reports_' . $report->id])))
                                                        @if($report->status == 'waiting' || checkPermissions(['edit_reports_' . $report->id]))
                                                            <a href="{{route('charity.reports.edit', $report->slug)}}" title="{{trans('admin.edit')}}">
                                                                <i data-feather="edit-2" class="mr-50"></i>
                                                            </a>
                                                        @endif
                                                        
                                                        @if($report->status == 'waiting' || checkPermissions(['delete_reports_' . $report->id]))
                                                            <a class="remove-table" href="javascript:void(0);" data-url="{{route('charity.reports.destroy', $report->id)}}" title="{{trans('admin.delete')}}">
                                                                <i data-feather="trash" class="mr-50"></i>
                                                            </a>
                                                        @endif    
                                            @endif                                         
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!--/ Company Table Card -->


    </div>


</section>


@endsection

@section('scripts')

<script>

    
</script>

@endsection






