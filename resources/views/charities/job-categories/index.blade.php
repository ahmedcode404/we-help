@extends('layout.app')

@section('title', trans('admin.job-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_job_categories_'.auth()->user()->id]))
                    <a href="{{route('charity.job-categories.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif
                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name_en')}}</th>
                            <th>{{trans('admin.name_ar')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['job-categories'] as $cat)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$cat->name_en}}</td>
                                <td>{{$cat->name_ar}}</td>
                                {{-- <td>{{$cat->nation->code}}</td> --}}
                                <td>
                                    @if(checkPermissions(['show_job_categories_'.auth()->user()->id]) || checkPermissions(['edit_job_categories_'.auth()->user()->id])
                                        || checkPermissions(['delete_job_categories_'.auth()->user()->id]))
                                                @if(checkPermissions(['show_job_categories_'.auth()->user()->id]))
                                                    <a  href="{{route('charity.job-categories.show', $cat->slug)}}" title="{{trans('admin.show')}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['edit_job_categories_'.auth()->user()->id]))
                                                    <a  href="{{route('charity.job-categories.edit', $cat->slug)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_job_categories_'.auth()->user()->id]))
                                                    <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('charity.job-categories.destroy', $cat->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            {{$data['job-categories']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection