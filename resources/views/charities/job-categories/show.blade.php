@extends('layout.app')

@section('title', trans('admin.show-job-categories'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <section id="basic-horizontal-layouts">

        <div class="row justify-content-md-center">

            {{-- jobs start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="user-info">
                        <h4 class="mb-1">{{trans('admin.name_en')}} : {{$data['cat']->name_en}}</h4>
                        <h4 class="mb-1">{{trans('admin.name_ar')}} : {{$data['cat']->name_ar}}</h4>     
                        @if(checkPermissions(['edit_job_categories_'.auth()->user()->id]))
                        <a class="btn btn-primary" href="{{route('charity.job-categories.edit', $data['cat']->slug)}}">
                            <i data-feather="edit-2" class="mr-50"></i>
                          {{trans('admin.edit')}}
                        </a>
                    @endif
                    </div>
                    <hr>
                    <h4 class="card-title">{{trans('admin.jobs')}}</h4>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data['cat']->jobs as $job)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$job->name}}</td>
                                        <td>
                                                  @if(checkPermissions(['show_jobs_'.auth()->user()->id]))
                                                        <a href="{{route('charity.jobs.show', $job->slug)}}" title="{{trans('admin.show')}}"> 
                                                            <i data-feather="eye" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                                    @if(checkPermissions(['edit_jobs_'.auth()->user()->id]))
                                                        <a href="{{route('charity.jobs.edit', $job->slug)}}" title="{{trans('admin.edit')}}">
                                                            <i data-feather="edit-2" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                                    @if(checkPermissions(['delete_jobs_'.auth()->user()->id]))
                                                        <a class="remove-table" title="{{trans('admin.delete')}}" href="javascript:void(0);" data-url="{{route('charity.jobs.destroy', $job->id)}}">
                                                            <i data-feather="trash" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                        </td>
                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            {{-- jobs end --}}
        </div>

    </section>

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection