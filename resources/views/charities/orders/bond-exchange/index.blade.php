@extends('layout.app')
@section('title')
{{ __('admin.order_exchange') }}
@endsection
@section('content')


<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @php
                    $session_currency = currencySymbol(session('currency'));
                @endphp
               <div class="card-datatable table-responsive">

                <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.project_name')}}</th>
                            <th>{{trans('admin.project_cost')}}</th>
                            <th>{{trans('admin.total_bills')}}</th>
                            <th>{{trans('admin.voucher_cost')}}</th>
                            <th>{{trans('admin.recieve_status')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($financial_requests as $request)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$request->project->name}}</td>
                                <td>{{$request->project ? generalExchange($request->project->get_total, $request->currency->symbol, $session_currency).' '.$session_currency : 0.0}}</td>
                                <td>
                                    @if($request->type == 'exchange' && $request->requestable && $request->requestable->getTotalCharityExchangeVouchers())
                                        {{$request->requestable->getTotalCharityExchangeVouchers().' '.$session_currency}}
                                    @elseif($request->project && $request->project->charity)
                                        {{$request->project->charity->getTotalCharityExchangeVouchers().' '.$session_currency}}
                                    @endif
                                </td>
                                <td>{{generalExchange($request->out_to_charity, $request->currency->symbol, $session_currency)}} {{$session_currency}}</td>
                                <td>

                                    <select name="recieve_status" id="recieve_status{{$request->id}}" class="form-control recieve_status" 
                                        data-action="{{route('charity.accept.bond')}}" data-method="POST" data-id="{{$request->id}}">
                                        <option value="">{{trans('admin.select')}}</option>
                                        <option value="recieved" @if($request->recieve_status == 'recieved') selected @endif>{{trans('admin.recieved')}}</option>
                                        <option value="not_transfered" @if($request->recieve_status == 'not_transfered') selected @endif>{{trans('admin.not_transfered')}}</option>
                                    </select>

                                    {{-- @if($request->received == 1)
                                        <div class="badge badge-info">
                                            <i data-feather="star" class="mr-25"></i>
                                            <span>{{ __('admin.accept_order') }}</span>
                                        </div>
                                    @else 

                                    <div class="d-inline-block">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#primary">
                                            {{ __('admin.accept_order') }}
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade text-left modal-warning" id="primary" tabindex="-1" role="dialog" aria-labelledby="myModalLabel160" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="myModalLabel160">{{ __('admin.notes') }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{ __('admin.accept_order_text') }}
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="{{ route('charity.accept.bond' , $request->id) }}" class="btn btn-warning">{{ __('admin.accept') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif --}}

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
               </div>
            </div>
        </div>
    </div>
    
</section>
<!--/ Basic table -->


@endsection

@section('scripts')

    <script src="{{ url('custom/custom-sweetalert.js') }}"></script>
    <script src="{{ url('charity/js/requests/accept-request.js') }}"></script>


@endsection






