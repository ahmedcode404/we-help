@extends('layout.app')
@section('title')
{{ __('admin.order_exchange') }}
@endsection
@section('content')


<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-datatable table-responsive">
                <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.project')}}</th>
                            <th>{{trans('admin.project_cost')}}</th>
                            <th>{{trans('admin.out_to_charity')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($financial_requests as $request)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$request->project->name}}</td>
                                <td>{{$request->project ? exchange($request->project->cost, $request->project->currency_id, session('currency')).' '.currencySymbol(session('currency')) : 0.0}}</td>
                                <td>{{$request->requestable && $request->requestable->getTotalOutToCharity() ? $request->requestable->getTotalOutToCharity().' '.currencySymbol(session('currency')) : 0.0}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
    
</section>
<!--/ Basic table -->


@endsection

@section('scripts')

<script src="{{ url('custom/custom-sweetalert.js') }}"></script>


@endsection






