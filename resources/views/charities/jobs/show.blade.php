@extends('layout.app')

@section('title', trans('admin.show_job'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
          
            {{-- job emps start --}}
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="user-info">
                        <h4 class="mb-1">{{trans('admin.name')}} : {{$data['job']->name}}</h4>
                        <h4 class="mb-1">{{trans('admin.category')}} : {{$data['job']->category ? $data['job']->category->name : ''}}</h4>
                        @if(checkPermissions(['edit_jobs_'.auth()->user()->id]))
                            <a  href="{{route('charity.jobs.edit', $data['job']->slug)}}" class="btn btn-primary" title="{{trans('admin.edit')}}">
                                <i data-feather="edit-2" class="mr-50"></i>{{trans('admin.edit')}}
                            </a>
                        @endif
                    </div>
                    <hr>

                    <h4 class="card-title">{{trans('admin.emps')}}</h4>
                    <div class="card-datatable table-responsive">
                        <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.email')}}</th>
                                <th>{{trans('admin.phone')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data['job']->charity_emp as $emp)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$emp->emp_name}}</td>
                                    <td>{{$emp->email}}</td>
                                    <td>{{$emp->phone}}</td>
                                    <td>
                                                @if(checkPermissions(['show_employees_'.auth()->user()->id]))
                                                    <a  href="{{route('charity.employees.show', $emp->slug)}}" title="{{trans('admin.show')}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['edit_employees_'.auth()->user()->id]))
                                                    <a  href="{{route('charity.employees.edit', $emp->slug)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_employees_'.auth()->user()->id]))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('charity.employees.destroy', $emp->id)}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                        <span>{{trans('admin.delete')}}</span>
                                                    </a>
                                                @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            {{-- job emps end --}}
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{asset('dashboard/forms/cities/create.js')}}"></script>
    @endpush
@endsection