@extends('layout.app')

@section('title', trans('admin.jobs'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">
                @if(checkPermissions(['create_jobs_'.auth()->user()->id]))
                    <a href="{{route('charity.jobs.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif
                <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name')}}</th>
                            <th>{{trans('admin.category')}}</th>
                            {{-- <th>{{trans('admin.nation')}}</th> --}}
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data['jobs'] as $job)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$job->name}}</td>
                                <td>{{$job->category ? $job->category->name : ''}}</td>
                                {{-- <td>{{$job->category ? $job->category->nation->code : ''}}</td> --}}
                                <td>
                                    @if(checkPermissions(['show_jobs_'.auth()->user()->id]) || checkPermissions(['edit_jobs_'.auth()->user()->id])
                                        || checkPermissions(['delete_jobs_'.auth()->user()->id]))

                                                @if(checkPermissions(['show_jobs_'.auth()->user()->id]))
                                                    <a  href="{{route('charity.jobs.show', $job->slug)}}" title="{{trans('admin.show')}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['edit_jobs_'.auth()->user()->id]))
                                                    <a  href="{{route('charity.jobs.edit', $job->slug)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_jobs_'.auth()->user()->id]))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('charity.jobs.destroy', $job->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{$data['jobs']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection