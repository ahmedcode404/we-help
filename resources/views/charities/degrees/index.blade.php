@extends('layout.app')

@section('title', trans('admin.degrees'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

<!-- Basic table -->
<section id="basic-datatable">
    <div class="row">
        <div class="col-12">
            <div class="card">

                @if(checkPermissions(['create_degrees_'.auth()->user()->id]))
                    <a href="{{route('charity.degrees.create')}}" class="btn btn-primary">{{trans('admin.create')}}</a>
                @endif

                <div class="card-datatable table-responsive">
                    <table class="datatables-basic table">
                    <thead>
                        <tr>
                            <th>{{trans('admin.id')}}</th>
                            <th>{{trans('admin.name')}}</th>
                            <th>{{trans('admin.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            if(auth()->user()->hasRole('charity_employee')){
                                $user = auth()->user()->charity;
                            }
                            else{
                                $user = auth()->user();
                            }
                        @endphp
                        @foreach ($data['degrees'] as $degree)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$degree->name}}</td>
                                <td>
                                    @if(checkPermissions(['show_degrees_'.$user->id]) || checkPermissions(['edit_degrees_'.$user->id])
                                        || checkPermissions(['delete_degrees_'.$user->id]))
                                                @if(checkPermissions(['show_degrees_'.$user->id]))
                                                    <a  href="{{route('charity.degrees.show', $degree->slug)}}" title="{{trans('admin.show')}}">
                                                        <i data-feather="eye" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['edit_degrees_'.$user->id]))
                                                    <a  href="{{route('charity.degrees.edit', $degree->slug)}}" title="{{trans('admin.edit')}}">
                                                        <i data-feather="edit-2" class="mr-50"></i>
                                                    </a>
                                                @endif
                                                @if(checkPermissions(['delete_degrees_'.$user->id]))
                                                    <a class="remove-table" href="javascript:void(0);" data-url="{{route('charity.degrees.destroy', $degree->id)}}" title="{{trans('admin.delete')}}">
                                                        <i data-feather="trash" class="mr-50"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
            {{$data['degrees']->links()}}
        </div>
    </div>
    
</section>
<!--/ Basic table -->

@endsection