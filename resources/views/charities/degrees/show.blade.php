@extends('layout.app')

@section('title', trans('admin.show_degrees'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="user-info">
                        <h4 class="mb-1">{{trans('admin.name_en')}} : {{$data['degree']->name_en}}</h4>
                        <h4 class="mb-1">{{trans('admin.name_ar')}} : {{$data['degree']->name_ar}}</h4>                        
                        @if(checkPermissions(['edit_degrees_'.auth()->user()->id]))
                        <a href="{{route('charity.degrees.edit', $data['degree']->slug)}}" class="btn btn-primary" title="{{trans('admin.edit')}}">
                            <i data-feather="edit-2" class="mr-50"></i>{{trans('admin.edit')}}
                        </a>
                       @endif
                    </div>
                    <hr>
                        <h4 class="card-title">{{trans('admin.employees')}}</h4>
                        <div class="card-datatable table-responsive">
                            <table class="datatables-basic table">
                            <thead>
                                <th>{{trans('admin.id')}}</th>
                                <th>{{trans('admin.name')}}</th>
                                <th>{{trans('admin.email')}}</th>
                                <th>{{trans('admin.phone')}}</th>
                                <th>{{trans('admin.actions')}}</th>
                            </thead>
                            <tbody>
                                @foreach($data['degree']->charity_emp as $user)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{$user->emp_name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->phone}}</td>
                                        <td>
                                           
                                                    @if(checkPermissions(['show_employees_'.auth()->user()->id]))
                                                        <a href="{{route('charity.employees.show', $user->slug)}}" title="{{trans('admin.show')}}">
                                                            <i data-feather="eye" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                                    @if(checkPermissions(['edit_employees_'.auth()->user()->id]))
                                                        <a href="{{route('charity.employees.edit', $user->slug)}}" title="{{trans('admin.edit')}}">
                                                            <i data-feather="edit-2" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                                    @if(checkPermissions(['delete_employees_'.auth()->user()->id]))
                                                        <a class="remove-table" href="javascript:void(0);" title="{{trans('admin.delete')}}" data-url="{{route('charity.employees.destroy', $user->id)}}">
                                                            <i data-feather="trash" class="mr-50"></i>
                                                        </a>
                                                    @endif
                                            
                                        </td>
                                    </tr>
                                @endforeach
                        </tbody>
                        </table>
                        </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->
@endsection