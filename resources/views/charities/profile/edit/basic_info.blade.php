<form id="basic_info_form" action="{{route('charity.charities.update', $charity_one->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <!-- type request -->
    <input type="hidden" class="type_form" value="basic-info" name="type_form" id="basic-info" />

    <!-- charity edit media object start -->
    <div class="media mb-2 align-items-center">
        <img src="{{asset('storage/'.$charity_one->logo)}}" alt="{{$charity_one->name}}" class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer preview-logo" style="max-height:70px" width="70" />
        <div class="media-body mt-50">
            <div class="col-12 d-flex mt-1 px-0">
                <label class="btn btn-primary mr-75 mb-0" for="logo" style="margin-top: 10px;">
                    <span>{{trans('admin.uploade_image')}}</span>
                    <input class="form-control image" type="file" name="logo" id="logo" hidden accept="image/png, image/jpeg, image/jpg" />
                    <small>(Max size: 20MB)</small>
                </label>
            </div>
        </div>
    </div>
    <!-- charity edit media object ends -->
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="name">{{trans('admin.name')}}</label>
                <input type="text" class="form-control" placeholder="{{trans('admin.name')}}" value="{{$charity_one->name ? $charity_one->name : old('name')}}" name="name" id="name" required />
                <span class="error-input"></span>            
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label for="email">{{trans('admin.email')}}</label>
                <input type="email" class="form-control" placeholder="{{trans('admin.email')}}" value="{{$charity_one->email ? $charity_one->email : old('email')}}" name="email" id="email" required />
                <span class="error-input"></span>            
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="type">{{trans('admin.type')}}</label>
                <select class="form-control" id="type" name="type">
                    <option value="" disabled>{{trans('admin.select')}}</option>
                    <option value="foundation" @if($charity_one->type == 'foundation' || old('type') == 'foundation') selected @endif>{{trans('admin.foundation')}}</option>
                    <option value="organization" @if($charity_one->type == 'organization' || old('type') == 'organization') selected @endif>{{trans('admin.organization')}}</option>
                    <option value="charity" @if($charity_one->type == 'charity' || old('type') == 'charity') selected @endif>{{trans('admin.charity')}}</option>
                </select>
                <span class="error-input"></span>                
            </div>
        </div>
        
        <div class="col-md-6">
            <div class="form-group">
                <label for="country">{{trans('admin.country')}}</label>
                <select id="country" class="form-control select2" url="{{ route('charity.cities') }}" name="country">
                    <option value="" disabled>{{trans('admin.select')}}</option>
                    @foreach ($countries as $key => $country)
                        <option value="{{$key}}" {{ $charity_one->country == $key || old('country') ? 'selected' : ''}}>{{$country}}</option>
                    @endforeach
                </select>
                <span class="error-input"></span>                
            </div>
        </div>


        <div class="col-md-6">
            <div class="form-group">
                <label for="city">{{trans('admin.city')}}</label>
                <input type="text" name="city" id="city" value="{{$charity_one->city ? $charity_one->city : old('city')}}" class="form-control">
                <span class="error-input"></span>                
            </div>
        </div>

        {{-- <div class="col-md-12 append-city">
        
            <div class="form-group">
                <label for="city_id">{{trans('admin.city')}}</label>
                <select id="city_id" class="form-control" url="{{ route('charity.cities') }}" name="city_id">
                    <option value="" disabled>{{trans('admin.select')}}</option>
                    @foreach ($cities as $city)
                        <option value="{{$city->id}}" {{ $charity_one->city_id == $city->id || old('city_id') ? 'selected' : '' }}> {{ $city->name }} </option>
                    @endforeach
                </select>
                <span class="error-input"></span>                
            </div>        

        </div> --}}


        <div class="col-md-6">
            <div class="form-group">
                <label for="phone">{{trans('admin.phone')}}</label>
                {{-- <input type="number" min="1" step="1" class="form-control" value="{{$charity_one->phone ? $charity_one->phone : old('phone')}}" name="phone_num" placeholder="{{trans('admin.phone')}}" id="phone" /> --}}
                <input type="tel" class="form-control" value="{{$charity_one->phone ? $charity_one->phone : old('phone')}}" name="phone_num" placeholder="{{trans('admin.phone')}}" id="phone" />
                <span class="error-input"></span>            
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="mobile">{{trans('admin.mobile')}}</label>
                {{-- <input type="number" min="1" step="1" class="form-control" value="{{$charity_one->mobile ? $charity_one->mobile : old('mobile')}}" name="mobile_num" placeholder="{{trans('admin.mobile')}}" id="mobile" /> --}}
                <input type="tel" class="form-control" value="{{$charity_one->mobile ? $charity_one->mobile : old('mobile')}}" name="mobile_num" placeholder="{{trans('admin.mobile')}}" id="mobile" />
                <span class="error-input"></span>            
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="address_ar">{{trans('admin.address_ar')}}</label>
                <input type="text" class="form-control" placeholder="{{trans('admin.address_ar')}}" value="{{$charity_one->address_ar ? $charity_one->address_ar : old('address_ar')}}" name="address_ar" id="address_ar" required />
                <span class="error-input"></span>            
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label for="address_en">{{trans('admin.address_en')}}</label>
                <input type="text" class="form-control" placeholder="{{trans('admin.address_en')}}" value="{{$charity_one->address_en ? $charity_one->address_en : old('address_en')}}" name="address_en" id="address_en" required />
                <span class="error-input"></span>            
            </div>
        </div>        

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="submit" url="{{ route('charity.charities.update' , $charity_one->id) }}" id="click_basic_info_form" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
        </div>
    </div>
</form>