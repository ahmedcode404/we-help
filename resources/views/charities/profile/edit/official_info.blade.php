a<form id="official_info_data" action="{{route('admin.charities.update', $charity_one->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <!-- type request -->
    <input type="hidden" class="type_form" value="official-info" name="type_form" id="official-info" />

    <div class="row mt-1">
        <div class="col-12">
            <h4 class="mb-1">
                <i data-feather="user" class="font-medium-4 mr-25"></i>
                <span class="align-middle">{{trans('admin.official_info')}}</span>
            </h4>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="license_number">{{trans('admin.license_number')}}</label>
                <input id="license_number" type="number" min="0" minlength="5" maxlength="12" step="1" class="form-control" value="{{$charity_one->license_number ? $charity_one->license_number : old('license_number')}}" name="license_number" placeholder="{{trans('admin.license_number')}}" required />
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="license_start_date">{{trans('admin.license_start_date')}}</label>
                <input id="license_start_date" type="text" class="form-control date-picker" readonly  value="{{$charity_one->license_start_date ? $charity_one->license_start_date : old('license_start_date')}}" name="license_start_date" placeholder="{{$charity_one->license_start_date}}" required />
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="license_end_date">{{trans('admin.license_end_date')}}</label>
                <input id="license_end_date" type="text" class="form-control date-picker" readonly  value="{{$charity_one->license_end_date ? $charity_one->license_end_date : old('license_end_date')}}" name="license_end_date" placeholder="{{$charity_one->license_end_date}}" required />
            </div>
        </div>
        <div class="col-lg-4 col-md-6 media">
            <div class="form-group">
                <label for="license_file">{{trans('admin.license_file')}}</label>
                <input id="license_file" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="license_file" placeholder="{{trans('admin.license_file')}}" />
                <small>(Max size: 50MB)</small>
            </div>
            <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$charity_one->license_file)}}" target="_blank">{{trans('admin.show')}}</a></div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="establish_date">{{trans('admin.establish_date')}}</label>
                <input id="establish_date" type="text" class="form-control date-picker" readonly  value="{{$charity_one->establish_date ? $charity_one->establish_date : old('establish_date')}}" name="establish_date" placeholder="{{$charity_one->establish_date}}" required />
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="branches_num">{{trans('admin.branches_num')}}</label>
                <input id="branches_num" type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.branches_num')}}" value="{{$charity_one->branches_num ? $charity_one->branches_num : old('branches_num')}}" name="branches_num" required />
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="members_num">{{trans('admin.members_num')}}</label>
                <input id="members_num" type="number" min="1" step="1" class="form-control" placeholder="{{trans('admin.members_num')}}" value="{{$charity_one->members_num ? $charity_one->members_num : old('members_num')}}" name="members_num" required />
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="contract_date">{{trans('admin.contract_date')}}</label>
                <input id="contract_date" type="text" class="form-control date-picker" readonly  name="contract_date" placeholder="{{trans('admin.contract_date')}}" value="{{$charity_one->contract_date ? $charity_one->contract_date : old('contract_date')}}" required readonly />
            </div>
            @if($charity_one->contract_after_sign))
                <div class="media align-items-center">
                    <label for="contract_date">{{trans('admin.show_contract')}}</label>
                    <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                    <div class="media-body"><a href="{{asset('storage/'.$charity_one->contract_after_sign)}}" target="_blank">{{trans('admin.show')}}</a></div>
                </div>
            @endif
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="form-group">
                <label for="attach">{{trans('admin.attach_text')}}</label>
                <input id="attach" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="attach" placeholder="{{trans('admin.attach')}}" />
                <small>(Max size: 50MB)</small>
            </div>
            <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show_attach')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$charity_one->attach)}}" target="_blank">{{trans('admin.show')}}</a></div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="year_report">{{trans('admin.year_report')}}</label>
                <input id="year_report" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="year_report" placeholder="{{trans('admin.year_report')}}" value="{{$charity_one->year_report ? $charity_one->year_report : old('year_report')}}" />
                <small>(Max size: 50MB)</small>
            </div>
            <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show_year_report')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$charity_one->year_report)}}" target="_blank">{{trans('admin.show')}}</a></div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="years">{{trans('admin.for')}} {{trans('admin.last')}}</label>
                <select id="years" class="form-control" name="years">
                    <option value="one" @if($charity_one->years == 'one' || old('years') == 'one') selected @endif>{{trans('admin.year')}}</option>
                    <option value="two" @if($charity_one->years == 'two' || old('years') == 'two') selected @endif>{{trans('admin.2')}} {{trans('admin.years')}}</option>
                    <option value="three" @if($charity_one->years == 'three' || old('years') == 'three') selected @endif>{{trans('admin.3')}} {{trans('admin.years')}}</option>
                </select>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="form-group">
                <label for="internal_image">{{trans('admin.internal_image')}}</label>
                <input id="internal_image" type="file" accept=".png,.jpeg,.jpg,.pdf" class="form-control" name="internal_image" placeholder="{{trans('admin.internal_image')}}" value="{{$charity_one->internal_image ? $charity_one->internal_image : old('internal_image')}}" />
                <small>(Max size: 50MB)</small>
            </div>
            <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show_internal_image')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$charity_one->internal_image)}}" target="_blank">{{trans('admin.show')}}</a></div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12">
            <div class="form-group">
                <label for="work_fields">{{trans('web.work_fields')}}</label>
                <textarea id="work_fields" rows="6" class="form-control" name="work_fields" placeholder="{{trans('web.work_fields')}}" required >{{$charity_one->work_fields ? $charity_one->work_fields : old('work_fields')}}</textarea>
                {{-- <select name="work_fields" id="work_fields" class="form-control" multiple>
                    @foreach ($data['work_fields'] as $field)
                        <option value="{{$field->id}}" @if($charity_one->work_fields->contains('id', $field->id)) selected @endif>{{$field->name}}</option>
                    @endforeach
                </select> --}}
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="strategy_ar">{{trans('admin.charity_strategy_ar')}}</label>
                <textarea id="strategy_ar" class="form-control" name="strategy_ar" placeholder="{{trans('admin.charity_strategy_ar')}}" >{{$charity_one->strategy_ar ? $charity_one->strategy_ar : old('strategy_ar')}}</textarea>
            </div>
        </div>

        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="strategy_en">{{trans('admin.charity_strategy_en')}}</label>
                <textarea id="strategy_en" class="form-control" name="strategy_en" placeholder="{{trans('admin.charity_strategy_en')}}" required >{{$charity_one->strategy_en ? $charity_one->strategy_en : old('strategy_en')}}</textarea>
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="button" url="{{ route('charity.charities.update' , $charity_one->id) }}" id="click_official_info_data" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
        </div>
    </div>
</form>