<form id="representer_form" action="{{route('admin.charities.update', $charity_one->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')

    <!-- type request -->
    <input type="hidden" class="type_form" value="representer-form" name="type_form" id="representer-form" />

    <div class="row mt-1">
        <div class="col-12">
            <h4 class="mb-1">
                <i data-feather="user" class="font-medium-4 mr-25"></i>
                <span class="align-middle">{{trans('admin.representer')}}</span>
            </h4>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_name_ar">{{trans('admin.representer_name_ar')}}</label>
                <input id="representer_name_ar" type="text" class="form-control" value="{{$charity_one->representer_name_ar ? $charity_one->representer_name_ar : old('representer_name_ar')}}" name="representer_name_ar" placeholder="{{trans('admin.representer_name_ar')}}" />
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_name_en">{{trans('admin.representer_name_en')}}</label>
                <input id="representer_name_en" type="text" class="form-control" value="{{$charity_one->representer_name_en ? $charity_one->representer_name_en : old('representer_name_en')}}" name="representer_name_en" placeholder="{{trans('admin.representer_name_en')}}" required />
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_title_ar">{{trans('admin.representer_title_ar')}}</label>
                <input id="representer_title_ar" type="text" class="form-control" value="{{$charity_one->representer_title_ar ? $charity_one->representer_title_ar : old('representer_title_ar')}}" name="representer_title_ar" placeholder="{{trans('admin.representer_title_ar')}}" />
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_title_en">{{trans('admin.representer_title_en')}}</label>
                <input id="representer_title_en" type="text" class="form-control" value="{{$charity_one->representer_title_en ? $charity_one->representer_title_en : old('representer_title_en')}}" name="representer_title_en" placeholder="{{trans('admin.representer_title_en')}}" required />
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_title_file">{{trans('admin.representer_title_file')}} ({{trans('admin.representer_title_text')}})</label>
                <input id="representer_title_file" type="file" accept=".jpeg,.jpg,.png,.pdf" class="form-control" name="representer_title_file" placeholder="{{trans('admin.representer_title_file')}}" />
                <small>(Max size: 50MB)</small>
            </div>
            <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$charity_one->representer_title_file)}}" target="_blank">{{trans('admin.show')}}</a></div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_email">{{trans('admin.representer_email')}}</label>
                <input id="representer_email" type="text" class="form-control" value="{{$charity_one->representer_email ? $charity_one->representer_email : old('representer_email')}}" name="representer_email" placeholder="{{trans('admin.representer_email')}}" required />
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <table>
                    <tbody>
                        <td>
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="international_member" value="1" name="international_member" @if($charity_one->international_member == 1) checked @endif />
                                <label class="custom-control-label" for="international_member">{{trans('admin.international_member')}}</label>
                            </div>
                        </td>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="@if($charity_one->international_member == 0) hidden @endif" id="international_name_container">
            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <label for="international_name_ar">{{trans('admin.international_name_ar')}}</label>
                    <input type="string" class="form-control" name="international_name_ar" id="international_name_ar" placeholder="{{trans('admin.international_name_ar')}}" value="{{$charity_one->international_name_ar ? $charity_one->international_name_ar : old('international_name_ar')}}">
                </div>
            </div>

            <div class="col-lg-12 col-md-12">
                <div class="form-group">
                    <label for="international_name_en">{{trans('admin.international_name_en')}}</label>
                    <input type="string" class="form-control" name="international_name_en" id="international_name_en" placeholder="{{trans('admin.international_name_en')}}" value="{{$charity_one->international_name_en ? $charity_one->international_name_en : old('international_name_en')}}" required>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_passport_image">{{trans('admin.representer_passport_image')}} ({{trans('admin.representer_title_text')}})</label>
                <input id="representer_passport_image" type="file" accept=".png,.jpg,.jpeg,.pdf" class="form-control" name="representer_passport_image" placeholder="{{trans('admin.representer_passport_image')}}" />
                <small>(Max size: 50MB)</small>
            </div>
            <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$charity_one->representer_passport_image)}}" target="_blank">{{trans('admin.show')}}</a></div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6">
            <div class="form-group">
                <label for="representer_nation_image">{{trans('admin.representer_nation_image')}} ({{trans('admin.representer_title_text')}})</label>
                <input id="representer_nation_image" type="file" accept=".png,.jpg,.jpeg,.pdf" class="form-control" name="representer_nation_image" placeholder="{{trans('admin.representer_nation_image')}}" />
                <small>(Max size: 50MB)</small>
            </div>
            <div class="media align-items-center">
                <label for="contract_date">{{trans('admin.show')}}</label>
                <img class="mr-1" src="{{asset('dashboard/app-assets/images/icons/file-icons/pdf.png')}}" alt="invoice" height="23" />
                <div class="media-body"><a href="{{asset('storage/'.$charity_one->representer_nation_image)}}" target="_blank">{{trans('admin.show')}}</a></div>
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="button" url="{{ route('charity.charities.update' , $charity_one->id) }}" id="click_representer_form" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
              
        </div>
    </div>
</form>