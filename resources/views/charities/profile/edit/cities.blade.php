@if($country->cities->isNotEmpty())
    <div class="col-lg-12">
        <div class="form-group">
            <label for="select-country">{{ __('admin.city') }}</label>
            <select class="form-control select2" id="city_id" name="city_id">
                @foreach($country->cities as $city)

                    <option value="{{ $city->id }}">{{ $city->name }}</option>

                @endforeach
            </select>
        </div>
    </div>
@endif     