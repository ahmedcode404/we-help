<form id="socials_form" action="{{route('admin.charities.update', $charity_one->id)}}" method="POST">
    @csrf
    @method('PUT')

    <!-- type request -->
    <input type="hidden" class="type_form" value="socials-form" name="type_form" id="socials-form" />

    <div class="row">
        <div class="col-lg-4 col-md-6 form-group">
            <label for="website">{{trans('admin.website')}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon3">
                        <i data-feather="chrome" class="font-medium-2"></i>
                    </span>
                </div>
                <input id="website" type="text" class="form-control" name="website" value="{{$charity_one->website ? $charity_one->website : old('website')}}" placeholder="https://www.example.com/" aria-describedby="basic-addon3" />
                <span class="testq error-input"></span>            
            </div>
        </div>
        <div class="col-lg-4 col-md-6 form-group">
            <label for="facebook_link">{{trans('admin.facebook_link')}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon4">
                        <i data-feather="facebook" class="font-medium-2"></i>
                    </span>
                </div>
                <input id="facebook_link" type="text" name="facebook_link" class="form-control" value="{{$charity_one->facebook_link ? $charity_one->facebook_link : old('facebook_link')}}" placeholder="https://www.facebook.com/" aria-describedby="basic-addon4" />
                <span class="testq error-input"></span>               
            </div>
        </div>
        <div class="col-lg-4 col-md-6 form-group">
            <label for="twitter_link">{{trans('admin.twitter_link')}}</label>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon5">
                        <i data-feather="twitter" class="font-medium-2"></i>
                    </span>
                </div>
                <input id="twitter_link" type="text" name="twitter_link" class="form-control" value="{{$charity_one->twitter_link ? $charity_one->twitter_link : old('twitter_link')}}" placeholder="https://www.twitter.com/" aria-describedby="basic-addon5" />
                <span class="testq error-input"></span>   
            </div>
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="button" url="{{ route('charity.charities.update' , $charity_one->id) }}" id="click_socials_form" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
        </div>
    </div>
</form>