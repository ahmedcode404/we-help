<form id="bank_data_form" action="{{route('admin.charities.update', $charity_one->id)}}" method="POST">
    @csrf
    @method('PUT')

    <!-- type request -->
    <input type="hidden" class="type_form" value="bank-data-form" name="type_form" id="bank-data-form" />

    <div class="row">

        <div class="col-lg-4 col-md-6 form-group">
            <label for="iban">{{trans('admin.iban')}}</label>
            <input id="iban" type="text" class="form-control" name="iban" value="{{$charity_one->iban ? $charity_one->iban : old('iban')}}" placeholder="{{trans('admin.iban')}}" />
            <span class="error-input"></span>        
        </div>

        <div class="col-lg-4 col-md-6 form-group">
            <label for="bank_name">{{trans('admin.bank_name')}}</label>
            <input id="bank_name" type="text" class="form-control" name="bank_name" value="{{$charity_one->bank_name ? $charity_one->bank_name : old('bank_name')}}" placeholder="{{trans('admin.bank_name')}}" />
            <span class="error-input"></span>        
        </div>

        <div class="col-lg-4 col-md-6 form-group">
            <label for="bank_address">{{trans('admin.bank_adress')}}</label>
            <input id="bank_address" type="text" class="form-control" name="bank_address" value="{{$charity_one->bank_address ? $charity_one->bank_address : old('bank_address')}}" placeholder="{{trans('admin.bank_adress')}}" />
            <span class="error-input"></span>        
        </div>

        <div class="col-lg-3 col-md-6 form-group">
            <label for="bank_city">{{trans('admin.city')}}</label>
            <input id="bank_city" type="text" class="form-control" name="bank_city" value="{{$charity_one->bank_city ? $charity_one->bank_city : old('bank_city')}}" placeholder="{{trans('admin.city')}}" />
            <span class="error-input"></span>       
        </div>

        <div class="col-lg-3 col-md-6 form-group">
            <label for="bank_phone">{{trans('admin.phone')}}</label>
            <input id="bank_phone" type="tel" min="1" step="1" class="form-control" name="bank_phone" value="{{$charity_one->bank_phone ? $charity_one->bank_phone : old('bank_phone')}}" placeholder="{{trans('admin.phone')}}" />
            <span class="error-input"></span> 
        </div>
        
        <div class="col-lg-6 col-md-6 form-group">
            <label for="swift_code">{{trans('admin.swift_code')}}</label>
            <input id="swift_code" type="text" class="form-control" name="swift_code" value="{{$charity_one->swift_code ? $charity_one->swift_code : old('swift_code')}}" placeholder="{{trans('admin.swift_code')}}" />
            <span class="error-input"></span>        
        </div>

        <div class="col-12 d-flex flex-sm-row flex-column mt-2">
            <button type="button" url="{{ route('charity.charities.update' , $charity_one->id) }}" id="click_bank_data_form" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1"><i data-feather="edit"></i> {{trans('admin.edit')}}</button>
                 
        </div>
    </div>
</form>