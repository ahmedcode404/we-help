@extends('layout.app')

@section('title')
{{ __('admin.edit_profile') }}
@endsection


@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('web/css/date.css')}}">
    
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" />    
@endpush

@section('content')

    <!-- charities edit start -->
    <section class="app-user-edit">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center active" id="basic_info-tab" data-toggle="tab" href="#basic_info" aria-controls="basic_info" role="tab" aria-selected="true">
                            <i data-feather="user"></i><span>{{trans('admin.basic_info')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="official_info-tab" data-toggle="tab" href="#official_info" aria-controls="official_info" role="tab" aria-selected="false">
                            <i data-feather="info"></i><span>{{trans('admin.official_info')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="representer-tab" data-toggle="tab" href="#representer" aria-controls="representer" role="tab" aria-selected="false">
                            <i data-feather="share-2"></i><span>{{trans('admin.representer')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="bank_data-tab" data-toggle="tab" href="#bank_data" aria-controls="bank_data" role="tab" aria-selected="false">
                            <i data-feather="share-2"></i><span>{{trans('admin.bank_data')}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center" id="socials-tab" data-toggle="tab" href="#socials" aria-controls="socials" role="tab" aria-selected="false">
                            <i data-feather="share-2"></i><span>{{trans('admin.socials')}}</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <!-- basic info Tab starts -->
                    <div class="tab-pane active" id="basic_info" aria-labelledby="basic_info-tab" role="tabpanel">
                        
                        @include('charities.profile.edit.basic_info')

                    </div>
                    <!-- basic info Tab ends -->

                    <!-- Official info Tab starts -->
                    <div class="tab-pane" id="official_info" aria-labelledby="official_info-tab" role="tabpanel">
                        
                       @include('charities.profile.edit.official_info')
                        
                    </div>
                    <!-- Official info Tab ends -->

                    <!-- Representer Tab starts -->
                    <div class="tab-pane" id="representer" aria-labelledby="representer-tab" role="tabpanel">
                        
                       @include('charities.profile.edit.representer')
                        
                    </div>
                    <!-- Representer Tab ends -->

                    <!-- bank data Tab starts -->
                    <div class="tab-pane" id="bank_data" aria-labelledby="bank_data-tab" role="tabpanel">
                        
                        @include('charities.profile.edit.bank_data')
                        
                    </div>
                    <!-- bank data Tab ends -->

                    <!-- Social Tab starts -->
                    <div class="tab-pane" id="socials" aria-labelledby="socials-tab" role="tabpanel">
                        
                        @include('charities.profile.edit.socials')
                        
                    </div>
                    <!-- Social Tab ends -->
                </div>
            </div>
        </div>
    </section>
    <!-- charities edit ends -->

    @push('js')
        <script src="{{ url('custom/custom-validate.js') }}"></script>
        <script src="{{asset('charity/js/charities/edit.js')}}" type="text/javascript"></script>
        <script src="{{asset('dashboard/js/intlTelInput.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('charity/js/charities/phone-key.js')}}" type="text/javascript"></script>
        <script src="{{url('custom/preview-image.js')}}" type="text/javascript"></script>
        <script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

        <script>
        
            // ckeditor custom
            CKEDITOR.replace( 'strategy_ar' );   
            CKEDITOR.replace( 'strategy_en' );   

            
        </script>

        {{-- <script src="{{asset('dashboard/forms/charities/edit.js')}}"></script> --}}
    @endpush
@endsection