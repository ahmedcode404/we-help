@extends('layout.app')

@section('title', trans('admin.profile'))
@section('description', trans('admin.home'))
@section('image', asset('dashboard/app-assets/images/logo.png'))

@push('css')
    <link rel="stylesheet" href="{{ asset('dashboard/css/intlTelInput.min.css') }}" type="text/css" />
@endpush

@section('content')

    <!-- Basic Horizontal form layout section start -->
    <section id="basic-horizontal-layouts">
        <div class="row justify-content-md-center">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ trans('admin.edit') }}</h4>
                    </div>

                    <div class="card-body">
                        <form id="form-employee" method="POST" action="{{ route('charity.emp-profile.update') }}">
                            @csrf
                            @method('PUT')

                            <div class="row">

                                {{-- name_employee_ar start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="name_ar">{{ trans('admin.name_ar') }}</label>
                                        <input type="text" class="form-control" id="name_ar"
                                            name="name_ar"
                                            value="{{ auth()->user()->name_ar ? auth()->user()->name_ar : old('name_ar') }}"
                                            placeholder="{{ trans('admin.name_ar') }}" />
                                        @if ($errors->has('name_ar'))
                                            <div class="error">{{ $errors->first('name_ar') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- name_employee_ar end --}}

                                {{-- name_employee_en start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="name_en">{{ trans('admin.name_en') }}</label>
                                        <input type="text" class="form-control" id="name_en"
                                            name="name_en"
                                            value="{{ auth()->user()->name_en ? auth()->user()->name_en : old('name_en') }}"
                                            placeholder="{{ trans('admin.name_en') }}" />
                                        @if ($errors->has('name_en'))
                                            <div class="error">{{ $errors->first('name_en') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- name_en end --}}

                                {{-- phase country start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">{{ trans('admin.country') }}</label>
                                        <select name="country" id="country" data-action="{{ route('admin.city.country') }}"
                                            class="form-control">
                                            <option value=""></option>
                                            @foreach ($data['countries'] as $key => $country)
                                                <option value="{{ $key }}"
                                                    {{ auth()->user()->country == $key || auth()->user()->country == old('country') ? 'selected' : old('country') }}>
                                                    {{ $country }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('country'))
                                            <div class="error">{{ $errors->first('country') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- phase country end --}}

                                {{-- city start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="city">{{ trans('admin.city') }}</label>
                                        <input type="text" class="form-control" id="city" name="city"
                                            value="{{ auth()->user()->city }}" placeholder="{{ trans('admin.city') }}" />
                                        @if ($errors->has('city'))
                                            <div class="error">{{ $errors->first('city') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- city end --}}

                                {{-- region start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="region">{{ trans('admin.region') }}</label>
                                        <input type="text" class="form-control" id="region" name="region"
                                            value="{{ auth()->user()->region ? auth()->user()->region : old('region') }}"
                                            placeholder="{{ trans('admin.region') }}" />
                                        @if ($errors->has('region'))
                                            <div class="error">{{ $errors->first('region') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- region end --}}


                                {{-- street start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="street">{{ trans('admin.street') }}</label>
                                        <input type="text" class="form-control" id="street" name="street"
                                            value="{{ auth()->user()->street ? auth()->user()->street : old('street') }}"
                                            placeholder="{{ trans('admin.street') }}" />
                                        @if ($errors->has('street'))
                                            <div class="error">{{ $errors->first('street') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- street end --}}


                                {{-- unit_number start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="unit_number">{{ trans('admin.unit_number') }}</label>
                                        <input type="number" step="1" min="0" class="form-control" id="unit_number"
                                            name="unit_number"
                                            value="{{ auth()->user()->unit_number ? auth()->user()->unit_number : old('unit_number') }}"
                                            placeholder="{{ trans('admin.unit_number') }}" />
                                        @if ($errors->has('unit_number'))
                                            <div class="error">{{ $errors->first('unit_number') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- unit_number end --}}


                                {{-- mail_box start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="mail_box">{{ trans('admin.mail_box') }}</label>
                                        <input type="text" class="form-control" id="mail_box" name="mail_box"
                                            value="{{ auth()->user()->mail_box ? auth()->user()->mail_box : old('mail_box') }}"
                                            placeholder="{{ trans('admin.mail_box') }}" />
                                        @if ($errors->has('mail_box'))
                                            <div class="error">{{ $errors->first('mail_box') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- mail_box end --}}

                                {{-- postal_code start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label"
                                            for="postal_code">{{ trans('admin.postal_code') }}</label>
                                        <input type="text" minlength="2" maxlength="5" class="form-control" id="postal_code"
                                            name="postal_code"
                                            value="{{ auth()->user()->postal_code ? auth()->user()->postal_code : old('postal_code') }}"
                                            placeholder="{{ trans('admin.postal_code') }}" />
                                        @if ($errors->has('postal_code'))
                                            <div class="error">{{ $errors->first('postal_code') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- postal_code end --}}

                                {{-- phone start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="phone">{{ trans('admin.phone') }}</label>
                                        <input type="tel" minlength="8" maxlength="10" class="form-control" id="phone"
                                            name="phone_num" value="{{ auth()->user()->phone ? auth()->user()->phone : old('phone') }}"
                                            placeholder="{{ trans('admin.phone') }}" />
                                        @if ($errors->has('phone_num'))
                                            <div class="error">{{ $errors->first('phone_num') }}</div>
                                        @endif
                                        @if ($errors->has('phone'))
                                            <div class="error">{{ $errors->first('phone') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- phone end --}}

                                {{-- email start --}}
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-label" for="email">{{ trans('admin.email') }}</label>
                                        <input type="email" class="form-control" name="email"
                                            value="{{ auth()->user()->email ? auth()->user()->email : old('email') }}"
                                            placeholder="{{ trans('admin.email') }}" />
                                        @if ($errors->has('email'))
                                            <div class="error">{{ $errors->first('email') }}</div>
                                        @endif
                                    </div>
                                </div>
                                {{-- email end --}}

                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary"> <i data-feather="edit"
                                            class="mr-50"></i> {{ trans('admin.save') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

    @push('js')
        <script src="{{ asset('dashboard/js/intlTelInput.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('dashboard/forms/employees/phone_key.js') }}"></script>
    @endpush
@endsection
