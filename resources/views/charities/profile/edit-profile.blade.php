@extends('layout.app')

@section('title')
{{ __('admin.edit_profile') }}
@endsection


@push('css')
    <link rel="stylesheet" href="{{asset('dashboard/css/intlTelInput.min.css')}}" type="text/css" />    
@endpush

@section('content')
<div class="card">
    <form id="basic_info_form" action="{{route('charity.dashboard.profile.update')}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        
        <!-- charity edit media object start -->
        <div class="media mb-2 align-items-center">
            <img src="{{asset('storage/'.auth()->user()->logo)}}" alt="{{trans('admin.logo')}}" class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" style="max-height:70px" width="70" />
            <div class="media-body mt-50">
                <h4>{{auth()->user()->name}}</h4>
                <div class="col-12 d-flex mt-1 px-0">
                    <label class="btn btn-primary mr-75 mb-0" for="logo">
                        <span>{{trans('admin.change')}}</span>
                        <input class="form-control" type="file" name="logo" id="logo" hidden accept="image/png, image/jpeg, image/jpg" />
                    </label>
                </div>
            </div>
        </div>
        <!-- charity edit media object ends -->
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label for="email">{{trans('admin.email')}}</label>
                    <input type="email" class="form-control" placeholder="{{trans('admin.email')}}" value="{{auth()->user()->email ? auth()->user()->email : old('email')}}" name="email" id="email" required />
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label for="phone">{{trans('admin.phone')}}</label>
                    <input type="tel" class="form-control" value="{{auth()->user()->phone ? auth()->user()->phone : old('phone')}}" name="phone_num" placeholder="{{trans('admin.phone')}}" id="phone" />
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="form-group">
                    <label for="mobile">{{trans('admin.mobile')}}</label>
                    <input type="tel" class="form-control" value="{{auth()->user()->mobile ? auth()->user()->mobile : old('mobile')}}" name="mobile_num" placeholder="{{trans('admin.mobile')}}" id="mobile" />
                </div>
            </div>

            <div class="col-lg-4 col-md-6 form-group">
                <label for="website">{{trans('admin.website')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon3">
                            <i data-feather="chrome" class="font-medium-2"></i>
                        </span>
                    </div>
                    <input id="website" type="text" class="form-control" name="website" value="{{auth()->user()->website ? auth()->user()->website : old('website')}}" placeholder="https://www.example.com/" aria-describedby="basic-addon3"/>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label for="facebook_link">{{trans('admin.facebook_link')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon4">
                            <i data-feather="facebook" class="font-medium-2"></i>
                        </span>
                    </div>
                    <input id="facebook_link" type="text" name="facebook_link" class="form-control" value="{{auth()->user()->facebook_link ? auth()->user()->facebook_link : old('facebook_link')}}" placeholder="https://www.facebook.com/" aria-describedby="basic-addon4" />
                </div>
            </div>
            <div class="col-lg-4 col-md-6 form-group">
                <label for="twitter_link">{{trans('admin.twitter_link')}}</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon5">
                            <i data-feather="twitter" class="font-medium-2"></i>
                        </span>
                    </div>
                    <input id="twitter_link" type="text" name="twitter_link" class="form-control" value="{{auth()->user()->twitter_link ? auth()->user()->twitter_link : old('twitter_link')}}" placeholder="https://www.twitter.com/" aria-describedby="basic-addon5" />
                </div>
            </div>
            
            <div class="col-12 d-flex flex-sm-row flex-column mt-2">
                <button type="submit" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1">{{trans('admin.save')}}</button>
                <button type="reset" class="btn btn-outline-secondary">{{trans('admin.reset')}}</button>
            </div>
        </div>

        
    </form>
</div>
@endsection

@push('js')
    <script src="{{asset('dashboard/js/intlTelInput.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('charity/js/charities/edit-profile.js')}}" type="text/javascript"></script>
@endpush