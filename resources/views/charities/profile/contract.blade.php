@extends('web.layout.app')

@section('title', __('web.contract') )
@section('description', __('web.contract') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')




    <!-- start pages-header
         ================ -->
         <section class="pages-header text-center" style="background-image:url({{ url('web/images/slider/1.png') }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{ __('web.Memorandum of Understanding') }}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->


    <div class="main-content">
        {{-- <div class="hidden-title print-none">
            <h2 class="white-text bold-text text-upper text-center page-title">{{ __('web.Memorandum of Understanding') }}</h2>
        </div> --}}


        <!-- start agreement-pg
         ================ -->
        <section class="margin-top-div agreement-pg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/agreement.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ __('web.Memorandum of Understanding') }}</h3>
                            <h4 class="text-upper bold-text">{{ __('web.Memorandum of Understanding') }}</h4>
                        </div>

                        <div class="agreement-div">
                            <div class="agreement-user-details second_color" data-aos="fade-in">
                                {{ __('web.Memorandum of Understanding between') }}
                                ( {{ __('web.International Corporation for Humanitarian Services (ICH)') }} )

                                {{ __('web.And Association / Foundation') }} <span class="first_color">( {{ auth()->guard()->user()->name }} )
                                </span>{{ __('web.Humanitarian') }}

                                <br>
                                {{ __('web.Date') }}:
                                <span class="first_color">{{ $date_higri }} {{ __('web.AH') }} / {{ $date }} {{ __('web.AD') }}</span>
                            </div>

                            <div class="agreemt-introduction" data-aos="fade-in">
                                <h2 class="second_color bold-text text-cap">{{ __('web.Introduction') }}:</h2>
                                    {{ __('web.text_intro' , ['name' => auth()->guard('charity')->user()->name]) }}
                                <br> <br>


                                <ol>
                                    <li>
                                    
                                        <!-- BEGIN: get data second one -->
                                        {!! __('web.first_party' , [

                                            'registration_number' => getSettingData('license_number'),
                                            'address'             => getSettingData('address'),
                                            'electronic_platform' => getSettingData('website'),
                                            'contract_by'         => getSettingMsg('representer_name' , getNationId())->value,
                                            'email'               => getSettingData('email'),
                                        
                                        ]) !!}
                                        <!-- END: get data second one -->

                                    </li>
                                    <li>
                                    
                                        <!-- BEGIN: get data second one -->
                                        {!! __('web.second_party' , [

                                            'name' => auth()->guard('charity')->user()->name,
                                            'commercial_registry'             => auth()->guard('charity')->user()->license_number,
                                            'address' => auth()->guard('charity')->user()->country,
                                            // 'address'         => auth()->guard('charity')->user()->country->name,
                                            'address'         => Countries::getOne(auth()->guard('charity')->user()->country, \App::getLocale()),
                                            // 'country'               => auth()->guard('charity')->user()->country->name,
                                            'country'               => Countries::getOne(auth()->guard('charity')->user()->country, \App::getLocale()),
                                            // 'address_city'               => auth()->guard('charity')->user()->country->name,
                                            'address_city'               => auth()->guard('charity')->user()->city,
                                            'telephone' => auth()->guard('charity')->user()->phone,
                                            'address' => auth()->guard('charity')->user()->address,
                                            'phone' => auth()->guard('charity')->user()->mobile,
                                            'email' => auth()->guard('charity')->user()->email,
                                            'signature' => auth()->guard('charity')->user()->name,
                                            'as' => 'جمعية',

                                        ]) !!}
                                        <!-- END: get data second one -->

                                    </li>
                                </ol>
                                
                                    {{ __('web.text_contract') }}

                                <!-- BRGIN: GET CONTRACT -->
                                @foreach($contracts as $contract)    

                                    <h3 class="second_color">{{ $contract->title }}:</h3>

                                    {!! $contract->content !!}

                                @endforeach
                                <!-- END: GET CONTRACT -->

                                <table class="table text-center">
                                    <thead>
                                        <tr class="second_color">
                                            <th class="text-center">{{ __('web.category') }}</th>
                                            <th>{{ __('web.commission') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $category)
                                            <tr>
                                                <td>{{ $category->name }}</td>
                                                <td>{{ $category->superadmin_ratio }}%</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <h2 class="text-center second_color bold-text">
                                    <br><br>
                                            {{ __('web.God Grants Success') }}
                                    <br><br>
                                </h2>
                            </div>
                        </div>


                        <div class="sigture-div">
                            <div class="section-title first_color" data-aos="fade-in">
                                <div class="side-img"><img src="{{ url('web/images/icons/sign.png') }}" alt="icon"></div>
                                <h3 class="second_color text-cap bold-text">{{ __('web.Signature') }}</h3>
                                <h4 class="text-upper bold-text">{{ __('web.Signature') }}</h4>
                            </div>

                            <div class="signiture-users" data-aos="fade-in">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3 class="second_color bold-text">{{ __('web.The First Side') }}:</h3>
                                        <ul class="list-unstyled">
                                            <li><span class="second_color">{{ __('web.name') }}:</span>{{ getSettingMsg('org_name' , getNationId())->value }}</li>
                                            <li><span class="second_color">{{ __('web.Signature') }}:</span>{{ getSettingMsg('representer_name' , getNationId())->value }}</li>
                                            <li><span class="second_color">{{  __('web.stamp') }}:</span><img
                                                    src="{{ url('storage/' . getSettingData('stamp')) }}" alt="stamp"></li>
                                        </ul>
                                    </div>

                                    <div class="col-md-6">
                                        <h3 class="second_color bold-text">{{ __('web.The Second Side') }}:</h3>
                                        <ul class="list-unstyled">
                                            <li><span class="second_color">{{ __('web.name') }}:</span>{{ auth()->guard('charity')->user()->name }}</li>
                                            <li><span class="second_color">{{ __('web.Signature') }}:</span>{{ auth()->guard('charity')->user()->representer_name }}</li>
                                            <li><span class="second_color">{{  __('web.stamp') }}:</span> <small class="print-none"> ( {{ __('web.added in the case of a paper signature') }} )</small></li>
                                        </ul>
                                    </div>
                                </div>

                                <form action="{{ route('charity.signature') }}" class="e3tmad_form  print-none" data-aos="fade-in" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="sign-as" id="check-1">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="custom-checkbox">
                                                    <input type="radio" class="hidden-input" value="pdf" id="check_1"
                                                        name="e3tmad_input">
                                                    <label for="check_1" datacheck="check-1" class="second_color">
                                                        {{ __('web.Sign as PDF') }}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-6  text-center-dir hide-div">
                                                <a href="#" class="gray-color second_color_hover" id="print-pg" ><i class="fa fa-file-download"></i> {{ __('web.Download File to Sign') }}</a>
                                            </div>

                                            <div class="col-md-4 col-sm-6  text-right-dir hide-div">
                                                <div class="custom-file-div">
                                                    <label for="file-input" class="gray-color second_color_hover"><i
                                                            class="fa fa-file-upload"></i> {{ __('web.Upload Signature File') }}</label>
                                                    <input type="file" id="file-input" class="hidden-input"
                                                        name="file_input" accept=".pdf">
                                                </div>
                                                <div class="file-name second_color"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="sign-as" id="check-2">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="custom-checkbox">
                                                    <input type="radio" class="hidden-input" value="sinagature" id="check_2"
                                                        name="e3tmad_input">
                                                    <label for="check_2" datacheck="check-2" 
                                                        class="second_color">{{ __('web.Electronic Sinagature') }}</label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12  hide-div">
                                                <input type="hidden" name="canvas_convert" class="canvas_convert" />
                                                <canvas id="signature-pad" class="signature-pad" width="600px"
                                                    height="300" style="border: 1px solid #65CBD5;"></canvas>
                                                <br>
                                                <div class="error" id="sig-error" style="display: none;"></div>
                                                <button type="button" id="clear" class="simple-btn">{{ __('web.clear') }}</button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="btn-div text-center print-none margin-bottom-div">
                                        <button type="submit"
                                            class="custom-btn sm-raduis white-btn"><span>{{ __('web.save') }}</span></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end agreement-pg-->

@endsection

@push('js')

    <!--for this page only-->
    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif

    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
    <script src="{{ url('web/js/agreement.js') }}" type="text/javascript"></script>
    <script src="{{ url('web/js/canvas_convert.js') }}" type="text/javascript"></script>


@endpush