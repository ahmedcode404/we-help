@extends('layout.app')
@section('title')
{{ __('admin.profile') }}
@endsection
@section('content')

<div class="content-body">
    <div id="user-profile">

        <!-- profile info section -->
        <section id="profile-info">
            <div class="row">

                <!-- left profile info section -->
                <div class="col-lg-12 col-12">
                    <!-- about -->
                    <div class="card">
                        <div class="card-body">
                            <div class="text-right mb-3">
                                @if(auth()->user()->hasRole('charity_employee'))
                                @if(!checkPermissions(['edit_charity_' . Auth::user()->charity->id]))

                                <a data-toggle="modal" data-target="#edit_notes" title="{{trans('admin.notes')}}"
                                    href="javascript:void(0);"
                                    class="btn {{ $edit_request != null ? 'btn-outline-primary disabled' : 'btn-outline-info' }}">
                                    <i data-feather="send" class="mr-50"></i>
                                    <span class="text-send">{{ $edit_request != null ? trans('admin.send_success') :
                                        trans('admin.request_edit') }}</span>
                                </a>

                                <!-- Modal -->
                                <div class="modal fade modal-info text-left" id="edit_notes" tabindex="-1" role="dialog"
                                    aria-labelledby="myModalLabel130" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered" role="document">

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="myModalLabel130">{{trans('admin.notes')}}
                                                </h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                    aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <textarea id="notes" name="notes" class="form-control"
                                                        placeholder="{{trans('admin.notes_text')}}" cols="30" rows="6"
                                                        required></textarea>
                                                    <span class="error error-notes"></span>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button"
                                                    class="btn {{ $edit_request != null ? 'btn-outline-primary disabled' : 'btn-outline-info' }} send-request"
                                                    id="{{ Auth::user()->charity->id }}" sure="{{ __('admin.sure') }}"
                                                    text-sure="{{ __('admin.text_sure') }}"
                                                    data-url="{{route('charity.send.request.edit.charity')}}">{{trans('admin.save')}}</button>
                                            </div>
                                        </div>
                                    </div>

                                    @else
                                    <a href="{{ route('charity.charities.edit' , Auth::user()->charity->id ) }}"
                                        class="btn btn-primary">
                                        <span class="font-weight-bold d-none d-md-block"><i data-feather="edit"
                                                class="mr-1"></i>{{ __('admin.edit') }} </span>
                                    </a>
                                    @endif
                                    @else

                                    @if(!checkPermissions(['edit_charity_' . Auth::user()->id]))

                                    <a data-toggle="modal" data-target="#edit_notes" title="{{trans('admin.notes')}}"
                                        href="javascript:void(0);"
                                        class="btn {{ $edit_request != null ? 'btn-outline-primary disabled' : 'btn-outline-info' }}">
                                        <i data-feather="send" class="mr-50"></i>
                                        <span class="text-send">{{ $edit_request != null ? trans('admin.send_success') :
                                            trans('admin.request_edit') }}</span>
                                    </a>

                                    <!-- Modal -->
                                    <div class="modal fade modal-info text-left" id="edit_notes" tabindex="-1"
                                        role="dialog" aria-labelledby="myModalLabel130" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="myModalLabel130">
                                                        {{trans('admin.notes')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <textarea id="notes" name="notes" class="form-control"
                                                            placeholder="{{trans('admin.notes_text')}}" cols="30"
                                                            rows="6" required></textarea>
                                                        <span class="error error-notes"></span>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button"
                                                        class="btn {{ $edit_request != null ? 'btn-outline-primary disabled' : 'btn-outline-info' }} send-request"
                                                        id="{{ Auth::user()->id }}" sure="{{ __('admin.sure') }}"
                                                        text-sure="{{ __('admin.text_sure') }}"
                                                        data-url="{{route('charity.send.request.edit.charity')}}">{{trans('admin.save')}}</button>
                                                </div>
                                            </div>
                                        </div>

                                        @else
                                        <a href="{{ route('charity.charities.edit' , Auth::user()->id ) }}"
                                            class="btn btn-primary">
                                            <span class="font-weight-bold d-none d-md-block"><i data-feather="edit"
                                                    class="mr-1"></i>{{ __('admin.edit') }} </span>
                                        </a>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="position-relative mb-2">
                                    @if(auth()->user()->hasRole('charity_employee'))
                                    <div class="user-avatar-section">
                                        <div class="d-flex justify-content-start">
                                            <div class="d-flex flex-column">
                                                <div class="user-info mb-1">
                                                    <h4 class="mb-0">{{ Auth::user()->emp_name }}</h4>
                                                </div>
                                                <div class="d-flex flex-wrap">
                                                    <a href="{{route('charity.dashboard.profile.edit')}}"
                                                        class="btn btn-primary">{{trans('admin.edit_profile')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <div class="user-avatar-section">
                                        <div class="d-flex justify-content-start">
                                            <img class="img-fluid rounded"
                                                src="{{ url('storage/' . Auth::user()->logo ) }}" height="70" width="70"
                                                alt="img" />
                                            <div class="d-flex flex-column ml-1">
                                                <div class="user-info mb-1">
                                                    <h4 class="mb-0">{{ Auth::user()->name }}</h4>
                                                </div>
                                                <div class="d-flex flex-wrap">
                                                    <a href="{{route('charity.dashboard.profile.edit')}}"
                                                        class="btn btn-primary">{{trans('admin.edit_profile')}}</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <hr>
                                </div>



                                @if(auth()->user()->hasRole('charity_employee'))
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.name') }}: </strong>{{
                                                Auth::user()->emp_name }}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.country') }}: </strong>{{
                                                Countries::getOne(Auth::user()->country, \App::getLocale()) }}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.city') }}: </strong>{{
                                                Auth::user()->city }}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.email') }}: </strong>{{
                                                Auth::user()->email}}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.phone') }}: </strong><span
                                                    dir="ltr">{{ Auth::user()->phone}}</span></h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-50"><strong>{{ __('admin.nation_number') }}: </strong>{{
                                                Auth::user()->nation_number}}</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.region') }}: </strong>{{
                                                Auth::user()->region }}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.street') }}: </strong>{{
                                                Auth::user()->street }}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.unit_number') }}: </strong>{{
                                                Auth::user()->unit_number}}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-50"><strong>{{ __('admin.mail_box') }}: </strong>{{
                                                Auth::user()->mail_box}}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-50"><strong>{{ __('admin.nationality') }}: </strong>{{
                                                Auth::user()->nationality ? Auth::user()->nationality->name : '-'}}</h5>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.postal_code') }}: </strong>{{
                                                Auth::user()->postal_code }}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.contract_start_date') }}: </strong>{{
                                                Auth::user()->contract_start_date }}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-75"><strong>{{ __('admin.contract_end_date') }}: </strong>{{
                                                Auth::user()->contract_end_date}}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-50"><strong>{{ __('admin.mail_box') }}: </strong>{{
                                                Auth::user()->mail_box}}</h5>
                                        </div>
                                        <div class="mb-2">
                                            <h5 class="mb-50"><strong>{{ __('admin.jop') }}: </strong>
                                                @if(Auth::user()->job)
                                                @if(Auth::user()->job->category)
                                                {{Auth::user()->job->category->name.' - '.Auth::user()->job->name}}
                                                @else
                                                {{Auth::user()->job->name}}
                                                @endif
                                                @else
                                                -
                                                @endif
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                        <a href="{{route('charity.emp-profile.edit')}}"
                                            class="btn btn-primary">{{trans('admin.edit')}}</a>

                                    </div>
                                </div>
                                @else
                                <h3 class="card-title">{{ __('admin.about') }}</h3>
                                <table class="table">
                                    <tr>
                                        <td>{{ __('admin.date_register') }}</td>
                                        <td>{{ Auth::user()->created_at->format('Y-m-d') }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('admin.country') }}</td>
                                        <td>{{ Countries::getOne(Auth::user()->country, \App::getLocale()) }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('admin.city') }}</td>
                                        <td>{{ Auth::user()->city }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('admin.email') }}</td>
                                        <td>{{ Auth::user()->email }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('admin.phone') }}</td>
                                        <td><span dir="ltr">{{ Auth::user()->phone }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('admin.mobile') }}</td>
                                        <td><span dir="ltr">{{ Auth::user()->mobile }}</span></td>
                                    </tr>
                                    <tr>
                                        <td>{{ __('admin.logo') }}</td>
                                        <td><img src="{{asset('storage/'.Auth::user()->logo)}}" width="50"
                                                height="50" /></td>
                                    </tr>
                                    @if(Auth::user()->website)
                                    <tr>
                                        <td>{{ __('admin.website') }}</td>
                                        <td>{{ Auth::user()->website }}</td>
                                    </tr>
                                    @endif
                                    @if(Auth::user()->facebook_link)
                                    <tr>
                                        <td>{{ __('admin.facebook_link') }}</td>
                                        <td>{{ Auth::user()->facebook_link }}</td>
                                    </tr>
                                    @endif
                                    @if(Auth::user()->twitter_link)
                                    <tr>
                                        <td>{{ __('admin.twitter_link') }}</td>
                                        <td>{{ Auth::user()->twitter_link }}</td>
                                    </tr>
                                    @endif
                                </table>
                                @endif
                            </div>

                        </div>
                        <!--/ about -->


                    </div>
                    <!--/ left profile info section -->

                    <!-- center profile info section -->
                    <div class="col-12">

                        @php
                        $projects = Auth::user()->projects()->where('profile', 1)->get();
                        @endphp

                        @if(count($projects) > 0)
                            <div class="card">
                                <!-- post 1 -->

                                <div class="card-body">
                                    <h3 class="card-title">{{trans('admin.prev_projects')}}</h3>
                                    @foreach($projects as $project)
                                        <div class="my-2">
                                            <div class="user-avatar-section">
                                                <div class="d-flex justify-content-start">
                                                    <img class="rounded"
                                                        src="{{ count($project->images) > 0 ? url('storage/'.$project->images[0]->path) : url('dashboard/images/avatar.png') }}"
                                                        height="70" width="70" alt="img" />
                                                    <div class="d-flex flex-column ml-1">
                                                        <div class="user-info mb-1">
                                                            @if($project->image)
                                                            <img class="rounded" src="{{ asset('storage/'.$project->image) }}"
                                                                alt="avatar img" height="50" width="50" />
                                                            @else
                                                            <img class="rounded"
                                                                src="{{ asset('dashboard/images/avatar.png') }}"
                                                                alt="avatar img" height="50" width="50" />
                                                            @endif
                                                            <h4 class="mb-0">
                                                                {{ $project->name }}</h4>
                                                        </div>
                                                        <div class="d-flex flex-wrap">
                                                            <h6 class="mb-0">{{ $project->type }}</h6>
                                                            <small class="text-muted mb-1">{{
                                                                $project->created_at->format('Y-m-d') }}</small><br>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endif

                    </div>
                    <!--/ center profile info section -->
                </div>

            </div>

            <!-- reload button -->
            <!--<div class="row">-->
            <!--    <div class="col-12 text-center">-->
            <!--        <button type="button" class="btn btn-sm btn-primary block-element border-0 mb-1">Load More</button>-->
            <!--    </div>-->
            <!--</div>-->
            <!--/ reload button -->
        </section>
        <!--/ profile info section -->
    </div>
</div>

@endsection