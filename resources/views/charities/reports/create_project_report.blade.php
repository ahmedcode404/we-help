@extends('layout.app')
@section('title')
{{ __('admin.add_report') }}
@endsection
@section('content')


    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">

            <!-- jQuery Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('admin.create') }}</h4>
                    </div>
                    <div class="card-body">

                        <form id="form-report" method="post" enctype="multipart/form-data">


                            <input type="hidden" id="project_id" value="{{ $project->id }}" name="project_id" />
                            <input type="hidden" id="type" value="{{ $type}}" name="type" />
                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block" for="report_ar">{{ __('admin.report_ar') }}</label>
                                        <textarea class="form-control" id="report_ar" name="report_ar" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>
 
                            </div>                                        
                            <!--  END: desc -->  
                            
                            
                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block" for="report_en">{{ __('admin.report_en') }}</label>
                                        <textarea class="form-control" id="report_en" name="report_en" rows="3"></textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>

                            </div>                                        
                            <!--  END: desc -->  


                            <!-- IMAGES -->
                            <div class="form-group">
                                <label>{{ __('admin.images') }}</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control" id="images" name="images[]" accept=".gif, .jpg, .png, .webp" multiple />
                                    <span class="error-input"></span>
                                </div>
                            </div>  
                            <small>(Max size: 20MB)</small>
                            
                            <!-- vedio -->
                            <div class="form-group">
                                <label>{{ __('admin.vedio') }}</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control" accept="video/mp4,video/ogg,video/webm,video/mov" id="vedio" name="vedio" />
                                    <small>(Max size: 50MB)</small>
                                    <span class="error-input"></span>
                                </div>
                                <div id="progress_bar" style="text-align: center;">
                                    <progress id="progressBar" value="0" max="100" style="width:1065px;display:none;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>                                
                                </div>

                            </div>

                        

                            <div class="row">
                                <div class="col-12">
                                    <button type="button" id="click-form-report" redirect="{{ route('charity.projects.show' , $project->id) }}" url="{{ route('charity.reports.store') }}" class="btn btn-primary" name="submit" value="Submit"> {{ __('admin.create') }} </button>
                                </div>
                                                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /jQuery Validation -->


        </div>
    </section>
    <!-- /Validation -->





@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.3.0/jquery.form.min.js" integrity="sha384-qlmct0AOBiA2VPZkMY3+2WqkHtIQ9lSdAsAn5RUJD/3vA5MKDgSGcdmIv4ycVxyn" crossorigin="anonymous"></script>

<script src="{{ url('charity/js/reports/create.js') }}"></script>
<script src="{{ url('custom/custom-validate.js') }}"></script>
<script src="{{ url('custom/preview-multible-image.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>

    // ckeditor custom

    CKEDITOR.replace( 'report_ar' );
    CKEDITOR.replace( 'report_en' );
    

    
</script>

@endsection






