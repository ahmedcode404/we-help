@extends('layout.app')
@section('title')
{{ __('admin.edit_report') }}
@endsection
@section('content')


    <!-- Validation -->
    <section class="bs-validation">
        <div class="row">

            <!-- jQuery Validation -->
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ __('admin.edit') }}</h4>
                    </div>
                    <div class="card-body">

                        <form id="edit-report" method="post" enctype="multipart/form-data">


                            <input type="hidden" class="" id="project_id" value="{{ $project->id }}" name="project_id" />
                            <input type="hidden" class="" id="phase_id" value="{{ isset($phaseOne) ? $phaseOne->id : null }}" name="phase_id" />
                            <input type="hidden" class="" id="report_id" value="{{ $report_one->id }}" name="report_id" />

                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block" for="report_ar">{{ __('admin.report_ar') }}</label>
                                        <textarea class="form-control" id="report_ar" name="report_ar" rows="3">{{ $report_one->report_ar }}</textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>
 
                            </div>                                        
                            <!--  END: desc -->  
                            
                            
                            <!--  BEGIN: desc -->
                            <div class="row">

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="d-block" for="report_en">{{ __('admin.report_en') }}</label>
                                        <textarea class="form-control" id="report_en" name="report_en" rows="3">{{ $report_one->report_en }}</textarea>
                                        <span class="error-input"></span>
                                    </div>
                                </div>

                            </div>                                        
                            <!--  END: desc -->  


                            <!-- IMAGES -->
                            <div class="form-group">
                                <label>{{ __('admin.images') }}</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control" id="images" name="images[]" accept=".gif, .jpg, .png, .webp" multiple />
                                    <span class="error-input"></span>
                                </div>
                            </div>  
                            <small>(Max size: 20MB)</small>


                            @foreach ($report_one->images as $image)

                                <div class="preview">

                                    <img src="{{ url('storage/' . $image->path ) }}" style="width:110px;padding:0px;height: 100px;margin-top: 11px;" class="imageThumb image" />
                                    <br/>
                                    <button style="padding:0px;" url="{{ route('charity.delete.image') }}" data-id="{{ $image->id }}"  class="btn btn-danger btn-sm delete-image">x</button>

                                </div>

                            @endforeach                             
                            
                            <!-- vedio -->
                            <div class="form-group">
                                <label>{{ __('admin.vedio') }}</label>
                                <div class="custom-file">
                                    <input type="file" class="form-control" accept="video/mp4,video/ogg, video/webm, video/mov" id="vedio" name="vedio" />
                                    <small>(Max size: 50MB)</small>
                                    <span class="error-input"></span>
                                </div>
                                <div id="progress_bar" style="text-align: center;">
                                    <progress id="progressBar" value="0" max="100" style="width:1065px;display:none;"></progress>
                                    <h3 id="status"></h3>
                                    <p id="loaded_n_total"></p>                                
                                </div>
                                @if($report_one->vedio)
                                <video height="300px" style="max-width: 100%" controls>
                                    <source src="{{ url('storage/' . $report_one->vedio ) }}" type="video/mp4">
                                    <source src="{{ url('storage/' . $report_one->vedio ) }}" type="video/mov">
                                    <source src="{{ url('storage/' . $report_one->vedio ) }}" type="video/ogg">
                                    Your browser does not support the video tag.
                                </video>  
                                @endif                              
                            </div>
                         

                            <div class="row">
                                <div class="col-12">
                                    @if(isset($phaseOne))
                                        <button type="button" id="edit-form-report" redirect="{{ route('charity.phases.show' , $phaseOne->slug) }}" url="{{ route('charity.reports.update' , $report_one->id) }}" class="btn btn-primary" name="submit" value="Submit"><i data-feather="edit" class="mr-50"></i> {{ __('admin.edit') }} </button>
                                    @else
                                        <button type="button" id="edit-form-report" redirect="{{ route('charity.projects.show' , $project->slug) }}" url="{{ route('charity.reports.update' , $report_one->id) }}" class="btn btn-primary" name="submit" value="Submit"><i data-feather="edit" class="mr-50"></i> {{ __('admin.edit') }} </button>
                                    @endif
                                </div>
                                                                
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /jQuery Validation -->


        </div>
    </section>
    <!-- /Validation -->





@endsection

@section('scripts')

<script src="{{ url('charity/js/reports/edit.js') }}"></script>
<script src="{{ url('charity/js/reports/delete-image.js') }}"></script>
<script src="{{ url('custom/custom-validate.js') }}"></script>
<script src="{{ url('custom/preview-multible-image.js') }}"></script>
<script src="https://cdn.ckeditor.com/4.16.0/standard/ckeditor.js"></script>

<script>

    // ckeditor custom

    CKEDITOR.replace( 'report_ar' );
    CKEDITOR.replace( 'report_en' );
    

    
</script>

@endsection






