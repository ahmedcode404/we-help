@extends('layout.app')
@section('title')
{{ __('admin.reports') }}
@endsection
@section('content')

    <!-- Blog Detail -->
    <div class="blog-detail-wrapper">
        <div class="row">
            <!-- Blog -->
            <div class="col-12">
                <div class="card">
                    @foreach($report_one->images as $image)
                        <img src="{{ url('storage/' . $image->path) }}" class="img-fluid card-img-top" alt="Blog Detail Pic" />
                    @endforeach
                    <div class="card-body">
                        <h4 class="card-title">{{ $report_one->phase ? $report_one->phase->name : trans('admin.project_report') }}</h4>
                    
                        <div class="media"> 
                            <div class="media-body">
                                <div class="mb-1">
                                    <h5 class="text-body">{{ __('admin.charity') }} : {{ $report_one->charity->name }}</h5>

                                </div>
                                <span>
                                    @if($report_one->status == 'waiting')
        
                                    <td> <div class="badge badge-secondary">{{ __('admin.waiting') }}</div> </td>
        
                                    @elseif($report_one->status == 'approved')
        
                                    <td> <div class="badge badge-success">{{ __('admin.approved') }}</div> </td>
        
                                    @elseif($report_one->status == 'rejected')
        
                                    <td> <div class="badge badge-danger">{{ __('admin.rejected') }}</div> </td>
        
                                    @elseif($report_one->status == 'hold')
        
                                    <td> <div class="badge badge-primary">{{ __('admin.hold') }}</div> </td>
        
                                    @elseif($report_one->status == 'closed')
        
                                    <td> <div class="badge badge-Warning">{{ __('admin.closed') }}</div> </td>
        
                                    @endif
                                </span>
                                <span class="text-muted ml-50 mr-25">|</span>
                                <small class="text-muted">{{ $report_one->created_at->format('Y-m-d') }}</small>
                            </div>
                        </div>
                      <hr>
                      
                        @if($report_one->vedio)
                            <div class="my-1 py-25">
                                <h4>{{ __('admin.vedio') }}</h4>
                                <div class="form-group">
                                    <video height="300px" style="max-width: 100%" controls>
                                        <source src="{{ asset('storage/'.$report_one->vedio) }}" type="video/mp4">
                                        <source src="{{ asset('storage/'.$report_one->vedio) }}" type="video/mov">
                                        <source src="{{ asset('storage/'.$report_one->vedio) }}" type="video/ogg">
                                        <source src="{{ asset('storage/'.$report_one->vedio) }}" type="video/webm">
                                    </video>
                                </div>

                                {{-- <a href="{{ asset('storage/'.$report_one->vedio) }}" target="_blank">
                                    <div class="badge badge-pill badge-light-warning">{{ __('admin.vedio') }}<i data-feather="youtube" class="ml-50"></i></div>
                                </a> --}}
                            </div>
                            <hr>
                        @else
                        {{ __('admin.no_vedio') }}
                        @endif
                        <p class="card-text mb-2">{!! $report_one->report !!} </p>

                    </div>
                </div>
            </div>
            <!--/ Blog -->
        </div>
    </div>
    <!--/ Blog Detail -->


@endsection

@section('scripts')

<script>

    
</script>

@endsection






