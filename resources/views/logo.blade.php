<!DOCTYPE html>

<html dir="ltr">

<head>
    <title>we help :)</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('web/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('web/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('web/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('web/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('web/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('web/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('web/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('web/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('web/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('web/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('web/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('web/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('web/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('web/images/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#000">
    <meta name="msapplication-TileImage" content="{{asset('web/images/favicon/ms-icon-144x144.png')}}">


    <!-- Style sheet
         ================ -->

    <link rel="stylesheet" href="{{asset('web/css/styles.css" type="text/css')}}" />

    <!--for english-->
    <link rel="stylesheet" href="{{asset('web/css/en.css" type="text/css')}}" />
    <!--end-->
    <link rel="stylesheet" href="{{asset('web/css/responsive.css" type="text/css')}}" />
    <script type="text/javascript" src="{{asset('web/js/jquery-3.4.1.min.js')}}"></script>

</head>

<body style="text-align: center">
    <object data="{{asset('web/images/main/logo-loading.svg')}}">
        <img src="{{asset('web/images/main/logo.png')}}" alt="logo">
    </object>
</body>

</html>