<!--ambassador modal-->
<div class="modal fade select-modal ambassador-modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div class="modal-header col-12">
                    <h5 class="modal-title white-text text-cap">{{ __('web.Be an Ambassador') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="user-div">
                    <form action="#" class="gray-form" id="form-donate-ambassador">

                        <input type="hidden" name="project_id_ambassador" id="project_id_ambassador" value="">

                        <div class="form-group">
                            <input type="text" class="form-control" name="ambassador_name" id="ambassador_name" required />
                            <label class="moving-label">{{ __('web.Ambassador Name') }}</label>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" name="campaign_name" id="campaign_name" required />
                            <label class="moving-label">{{ __('web.Campaign Name') }}</label>
                        </div>

                        <div class="form-group">
                            <select class="form-control select-input" name="campaign_goal_id" id="campaign_goal_id" required>
                                <option selected disabled></option>
                                @foreach($campaign_goals as $campaign_goal)
                                    <option value="{{ $campaign_goal->id }}">{{ $campaign_goal->name }}</option>
                                @endforeach
                            </select>
                            <label class="moving-label">Campaign Goal</label>
                        </div>


                        {{-- <div>
                            <div class="custom-checkbox">
                                <input type="checkbox" class="hidden-input" id="amb_check_1" name="amb_check_1"
                                     required>
                                <label for="amb_check_1" class="dark-text"> {{ __('web.I agree to the') }} 
                                    <a href="{{ route('web.pages', 'supprot_contract') }}"
                                        class="first_color_hover dark_second_color decorate_link"
                                        target="_blank">{{  __('web.Support Contract') }}
                                    </a>
                                </label>
                            </div>
                        </div> --}}

                        <div class="check-donate-amb">
                            <div class="custom-checkbox">
                                <!--don`t change id-->
                                <input type="checkbox" class="hidden-input" id="amb_pay" name="amb_check_1">
                                <label for="amb_pay" class="dark-text">{{ __('web.do you want to donate') }} ?</label>
                            </div>
                        </div>

                        <div class="btn-div text-center">
                            <button type="button" class="custom-btn sm-raduis white-btn amb-btn add-campaign-goal" data-url="{{ route('web.campaign.store') }}"><span>{{ __('web.Create Campaign Page') }}</span></button>
                        </div>

                        <div class="form-group share-url-div auto-icon sm-center">

                            <input type="text" class="form-control" id="affiliate-url" value="https://www.shutterstock.com/g/Mayer.com"
                                readonly>
                            <button type="button" class="custom-btn sm-raduis reverse-btn"><span>{{ __('web.Copy Link') }}</span></button>
                            <div class="share-url" data-href="{{ Request::url() }}"><i class="fa fa-share-alt"></i>
                                <div class="fb soc-but">
                                    <a class="back" href="#" data-network="facebook"><i class="fab fa-facebook-f"></i></a>
                                </div>
                                <div class="tw soc-but">
                                    <a class="back" href="#" data-network="twitter"><i class="fab fa-twitter"></i></a>
                                </div>
                                <div class="whatsapp soc-but">
                                    <a class="back" href="#" data-network="whatsapp"><i class="fab fa-whatsapp"></i></a>
                                </div>
                                <div class="mail soc-but">
                                    <a class="back" href="#" data-network="email"><i class="fa fa-envelope"></i></a>
                                </div>
                            </div>

                        </div>
                    </form>



                    <div class="ambasador-donate sm-raduis">
                        <form action="" class="donate-ambsaador-form" id="donate-ambsaador-form">
                            <div class="form-group number-input-div">
                                <span class="minus-num  numb-control">-</span>
                                <input type="number" step=".01" class="form-control no_apperance_number number-input"
                                    minlength="2" maxlength="7" id="price_ambsaador" value="{{ $cost_min }}" pattern="\d{5}">
                                <span class="plus-num numb-control">+</span>

                            </div>

                            <div class="form-group">
                                <select class="form-control select-input" name="currency_ambsaador" id="currency_ambsaador">
                                    @foreach($currencies as $currency)
                                        <option value="{{ $currency->id }}" {{ $currency->id == session('currency') ? 'selected' : '' }}>{{ $currency->name }}</option>
                                    @endforeach                                
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="button"
                                    class="custom-btn sm-raduis white-btn add-donate-ambassador" data-url="{{ route('web.project.store') }}" data-type="ambassador" data-projectuserid=""><span>{{ __('web.Donate') }}</span></button>
                            </div>
                        </form>

                        <span class="custom-error error"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>