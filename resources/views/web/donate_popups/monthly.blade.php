<!--month modal-->
<div class="modal fade select-modal month-modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div class="modal-header col-12">
                    <h5 class="modal-title white-text text-cap">{{ __('web.Monthly Deduction') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="user-div">
                    <form action="#" class="gray-form" id="form-donate-month">

                        <input type="hidden" name="project_id_month" id="project_id_month" value="">
                        <input type="hidden" name="price_month" id="price_month" value="">
                        <input type="hidden" name="currency_month" id="currency_month" value="">


                        <div class="form-group">
                            <h2 class="second_color text-cap">{{ __('web.Donate monthly') }}?</h2>
                            <p>{{ __('web.You can cancel at any time') }}</p>
                        </div>
                        <div class="form-group">
                            <h2 class="second_color text-cap">{{ __('web.you will donate') }} <span id="price_in_text_month"></span> <span id="currency_in_text_month"></span> / {{ __('web.Month') }}  </h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="custom-checkbox">
                                    <input type="checkbox" class="hidden-input" id="month_check_1"
                                        name="month_check_1">
                                    <label for="month_check_1" class="dark-text"> {{ __('web.Donate Anonymous') }}</label>
                                </div>
                            </div>

                            {{-- <div class="col-lg-6">
                                <div class="custom-checkbox">
                                    <span class="custom-error error"></span>
                                    <input type="checkbox" class="hidden-input" id="month_check_2"
                                        name="month_check_2" required>
                                    <label for="month_check_2" class="dark-text"> {{ __('web.I agree to the') }} 
                                        <a href="{{ route('web.pages', 'supprot_contract') }}"
                                            class="first_color_hover dark_second_color decorate_link"
                                            target="_blank">{{ __('web.Support Contract') }}
                                        </a>
                                    </label>
                                </div>
                            </div> --}}
                        </div>
                        <div class="btn-div text-center">
                            <button type="button"
                                class="custom-btn sm-raduis white-btn add-donate-month" data-url="{{ route('web.project.store') }}" data-projectuserid="" data-type="monthly"><span>{{  __('web.confirm') }}</span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>