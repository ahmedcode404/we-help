<!--gift modal-->
<div class="modal fade select-modal gift-modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">                    
            <div class="modal-body">
                <div class="modal-header col-12">
                    <h5 class="modal-title white-text text-cap">{{ __('web.Donate as a Gift') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="user-div">
                    <form action="#" class="gray-form" id="form-donate-gift">


                        <input type="hidden" name="project_id_gift" id="project_id_gift" value="">
                        <input type="hidden" name="price_gift" id="price_gift" value="">
                        <input type="hidden" name="currency_gift" id="currency_gift" value="">

                        <h3 class="first_color text-cap">{{ __('web.Donor Data') }}:</h3>
                        <div class="form-group">
                            <span class="custom-error error"></span>
                            <input type="text" class="form-control input-valid" name="name" id="name" />
                            <label class="moving-label">{{ __('web.Name') }}</label>
                        </div>

                        <div class="form-group">
                            <div class="custom-checkbox">
                                <input type="checkbox" class="hidden-input" id="check_14">
                                <label for="check_14" class="dark-text">{{ __('web.Donate Anonymous') }}</label>
                            </div>
                        </div>

                        <h3 class="first_color text-cap">{{ __('web.Gifts Owner') }}:</h3>
                        <div class="repeat-div">

                            <div class="main-repeate">
                                <div class="form-group">
                                    <span class="custom-error error"></span>
                                    <input type="text" class="form-control name2" name="name2[]" id="name2_0" value="" multiple required />
                                    <label class="moving-label">{{ __('web.Name2') }}</label>
                                </div>

                                <div class="form-group">
                                    <span class="custom-error error"></span>
                                    <input type="number" step="1" class="form-control tel2" minlength="9" id="tel2_0" maxlength="14" value="" name="tel2[]" multiple
                                        required />
                                    <label class="moving-label">{{ __('web.Mobile no') }}.</label>
                                </div>

                                <div class="form-group">
                                    <span class="custom-error error"></span>
                                    <input type="email" class="form-control email2" name="email2[]" id="email2_0" value="" multiple required />
                                    <label class="moving-label">{{ __('web.E-mail') }}</label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <button type="button" class="custom-btn sm-raduis repeate-btn"><span>{{ __('web.add more') }}</span></button>
                        </div>
                        {{-- <div class="form-group">
                            <div class="custom-checkbox">
                                <span class="custom-error error"></span>
                                <input type="checkbox" class="hidden-input input-valid" id="gift_check_1" name="gift_check_1"
                                     required>
                                <label for="gift_check_1" class="dark-text"> {{ __('web.I agree to the') }}
                                    <a href="{{ route('web.pages', 'supprot_contract') }}"
                                        class="first_color_hover dark_second_color decorate_link"
                                        target="_blank">{{ __('web.Support Contract') }}
                                    </a>
                                </label>
                            </div>
                        </div> --}}
                        <div class="btn-div text-center">
                            <button type="button"
                                class="custom-btn sm-raduis white-btn add-donate-gift" data-url="{{ route('web.project.store') }}" data-projectuserid="" data-type="gift"><span>{{ __('web.confirm') }}</span>                         
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>