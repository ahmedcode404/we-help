<!--once modal-->
<div class="modal fade select-modal once-modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-body">
                <div class="modal-header col-12">
                    <h5 class="modal-title white-text text-cap">{{ __('web.Donate once') }}‏</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div class="user-div">
                    <form action="#" class="gray-form once" id="form-donate-once">

                        <input type="hidden" name="project_id" id="project_id" value="">
                        <input type="hidden" name="price" id="price" value="">
                        <input type="hidden" name="currency" id="currency" value="">

                        <div class="form-group">
                            <h2 class="second_color text-cap">{{ __('web.you will donate') }} <span id="price_in_text"></span> <span id="currency_in_text"></span> {{ __('web.for one time.') }}</h2>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="custom-checkbox">
                                    <input type="checkbox" class="hidden-input" name="once_check_1" id="once_check_1"
                                        >
                                    <label for="once_check_1" class="dark-text">{{ __('web.Donate Anonymous') }}</label>
                                </div>
                            </div>

                            {{-- <div class="col-lg-6">
                                <div class="custom-checkbox">
                                    <span class="custom-error error"></span>
                                    <input type="checkbox" class="hidden-input" name="once_check_2" id="once_check_2"
                                         required>
                                    <label for="once_check_2" class="dark-text"> {{ __('web.I agree to the') }} 
                                        <a href="{{ route('web.pages', 'supprot_contract') }}"
                                            class="first_color_hover dark_second_color decorate_link"
                                            target="_blank">{{ __('web.Support Contract') }}
                                        </a>
                                    </label>
                                </div>
                            </div> --}}
                        </div>
                        <div class="btn-div text-center">
                            <button type="button"
                                class="custom-btn sm-raduis white-btn add-donate-once" data-url="{{ route('web.project.store') }}" data-projectuserid="" data-type="once"><span>{{  __('web.confirm') }}</span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>