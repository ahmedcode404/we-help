@extends('web.layout.app')

@section('title', __('web.donate_now') )
@section('description', __('web.donate_now') )
@section('image', asset('web/images/main/white-logo.png'))


@section('content')


<!-- BEDIN: header -->

@include('web.donates.header')

<!-- END: header -->

<div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{ __('web.donate_now') }}</h2>
        </div> --}}
        <!-- start donate-pg
         ================ -->
         <section class="faq-page margin-div">
            <div class="container">
                <div class="row">
                    <div class="col-12">

                        @php
                            if(auth()->check()){

                                $donor_total_supports = auth()->user()->donor_total_supports;
                            }
                            else{
                                $donor_total_supports = 0;
                            }
                        @endphp

                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/menu/8.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ __('web.donate_now') }}</h3>
                            <h4 class="text-upper bold-text">{{ __('web.donate_now') }}</h4>
                        </div>
                        <div class="donate-div text-center margin-div side-section">

                            {{-- <span style="color: #3dbecb;">{{__('web.cost_min')}}: {{$cost_min}} {{currencySymbol(session('currency'))}}</span> --}}
                            
                            <form action="#" class="donate-form" id="donate-cost-error-1">

                                <div class="donate-quantity" data-aos="zoom-out">
                                    <div class="form-group number-input-div">
                                        <span class="minus-num  numb-control">-</span>
                                        <input type="number" step=".01"
                                            class="form-control no_apperance_number number-input sm-raduis-input"
                                            name="number-input" minlength="2" maxlength="7" value="{{ $cost_min }}" pattern="\d{5}"
                                            required>
                                        <span class="plus-num numb-control">+</span>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control select-input sm-raduis-input currency_id" name="currency"
                                            required>
                                                @foreach($currencies as $currency)
                                                    <option value="{{ $currency->id }}" {{ $currency->id == session('currency') ? 'selected' : '' }}>{{ $currency->name }}</option>
                                                @endforeach
                                        </select>
                                    </div>

                                </div>
                                <span class="custom-error error"></span>

                                <div class="multi-checkboxes" data-aos="zoom-out">
                                    <div class="row justify-content-center">

                                        <div class="col-xl-3 col-sm-6 md-center">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <input type="radio" class="hidden-input type-donate" name="donate_time" required
                                                        value="once" id="check_1">
                                                    <label for="check_1" class="dark-text"> <span>{{ __('web.once') }}
                                                        </span></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-3 col-sm-6 md-center">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <input type="radio" class="hidden-input type-donate" name="donate_time" required
                                                        value="month" id="check_2">
                                                    <label for="check_2" class="dark-text"><span>{{ __('web.monthly_deduction') }}</span></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-3 col-sm-6 md-center">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <!--don`t change id for that-->
                                                    <input type="radio" class="hidden-input type-donate" name="donate_time" required
                                                        value="gift" id="check_3">
                                                    <label for="check_3" class="dark-text"><span> {{ __('web.Donate as a Gift') }}</span></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-3 col-sm-6 md-center">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <input type="radio" class="hidden-input type-donate" name="donate_time" required
                                                        value="ambassador" id="check_4">
                                                    <label for="check_4" class="dark-text"> <span>{{ __('web.Be an Ambassador') }}</span></label>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="donate-cat gray-form text-left-dir">

                                    <div data-aos="zoom-out">
                                        <div class="form-group">
                                            <select class="form-control select-input" data-action="{{ route('web.get.project') }}" id="get_project" name="categories" required>
                                                <option selected disabled></option>
                                                    @foreach($charitycategories as $charitycategory)
                                                        <option value="{{ $charitycategory->id }}">{{ $charitycategory->name }}</option>
                                                    @endforeach
                                            </select>
                                            <label class="moving-label">{{ __('web.categories') }}</label>
                                        </div>


                                        <div class="form-group" id="append_project">

                                        </div>


                                        <div class="btn-div text-center">
                                        
                                            @if(auth()->guard('charity')->user())
                                                <button type="button" class=" sm-raduis white-btn auth-charity"><span>{{ __('web.donate_now') }}</span></button>
                                            @else
                                                <button 
                                                    type="button" 
                                                    id="donate-now" 
                                                    data-id="1" 
                                                    @if($donor_total_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                        data-action="{{ route('web.price.donate') }}" 
                                                    @endif
                                                    class="donate-prog-btn custom-btn sm-raduis white-btn
                                                    @if($donor_total_supports >= 10000) user_donation_full @endif"><span>{{ __('web.donate_now') }}</span></button>
                                            @endif
                                            {{-- <button type="button" class="simple-btn tooltip-link add-to-cart-num"
                                                title="add to cart"><img src="{{ url('web/images/icons/menu/shopping-cart.png') }}"
                                                    alt="img"></button> --}}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end donate-->


<!--start select-modals-->

<!--once modal-->
@include('web.donate_popups.once')

<!--gift modal-->
@include('web.donate_popups.gift')

<!--month modal-->
@include('web.donate_popups.monthly')

<!--ambassador modal-->
@include('web.donate_popups.ambassador')
<!--end select-modal-->
</div>
@endsection

@push('js')


    
    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif

    <!--for this page only-->
    <script type="text/javascript" src="{{ asset('web/js/anime.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/custom-pages.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/donate.js') }}"></script>

    <!-- validation laravel -->
    <script src="{{ url('custom/custom-validate.js') }}"></script>   

@endpush