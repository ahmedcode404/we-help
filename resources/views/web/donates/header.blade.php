    <!-- start pages-header
         ================ -->
         <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{ __('web.donate_now') }}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->