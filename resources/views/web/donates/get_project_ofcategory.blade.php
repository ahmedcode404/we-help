
    <select class="form-control select-input project_id" data-action="{{ route('web.check.project') }}" name="projects" id="check_project" required>
        <option selected disabled></option>
        @foreach($projects as $project)
            <option value="{{ $project->id }}">{{ $project->name }} - {{ $project->project_num }}</option>
        @endforeach
    </select>
    <label class="moving-label">{{ __('web.project_name') }}</label>