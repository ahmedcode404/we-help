@extends('web.layout.app')

@section('title', __('web.notifications'))
@section('description', __('web.notifications'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')
    <!-- start pages-header
        ================ -->
    <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{__('web.notifications')}}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{__('web.notifications')}}</h2>
        </div> --}}



        <!-- start Notification-sec
         ================ -->
        <section class="margin-div Notification-pg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{asset('web/images/icons/bell2.png')}}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{__('web.notifications')}}</h3>
                            <h4 class="text-upper bold-text">{{__('web.notifications')}}</h4>
                        </div>

                        <div class="inner-notification auto-icon" data-aos="zoom-out">
                            <ul class="list-unstyled">
                                <!-- if notification is new add class new -->
                                @forelse($data['notifications'] as $not)
                                    @if($not->project)
                                        <li class="@if($not->read == 0) new @endif remove-item">
                                            <a href="{{route('web.projects.show', $not->project->slug)}}" class="section-title first_color">
                                                <div class="side-img"><img src="{{asset('storage/'.$not->project->image)}}" alt="img"></div>
                                                <h3 class="second_color text-cap bold-text">{{$not->project->name}}</h3>
                                                <p class="dark-text">
                                                    {{$not->message}}
                                                </p>
                                                <div class="side-notify">
                                                    <span class="noti-time third_color">{{\Carbon\Carbon::createFromTimeStamp(strtotime($not->created_at))->diffForHumans()}}</span>
                                                    <button class="simple-btn remove-btn tooltip-link" data-action="{{route('web.notifications.remove-item', $not->id)}}" data-method="POST" title="{{__('web.delete')}}"><i
                                                            class="fa fa-times"></i></button>
                                                </div>
                                            </a>
                                        </li>
                                    @endif
                                @empty
                                    <li>{{__('web.no_notifications')}}</li>
                                @endforelse
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!--end Notification-sec-->

    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/remove-item.js')}}"></script>
@endpush