    <!-- start profile-pg
        ================ -->
        <section class="profile-pg margin-div">
        <div class="container">
            <div class="row">
                <div class="col-12" data-aos="fade-in">
                    <h3 class="second_color text-cap profile-title">{{ __('web.projects') }} :</h3>
                </div>
            </div>
            <div class="reports-div projects-profile">
                <div class="profile-tabs">

                    @php
                        if(auth()->check()){

                            $donor_total_supports = auth()->user()->donor_total_supports;
                        }
                        else{
                            $donor_total_supports = 0;
                        }
                    @endphp
                    
                    <ul class="nav nav-tabs justify-content-center text-center text-cap" id="myTab" role="tablist">

                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tab_1" role="tab"
                                aria-controls="tab_1" aria-selected="true">{{ __('web.one_time') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_2" role="tab" aria-controls="tab_2"
                                aria-selected="false">{{ __('web.monthly_deduction') }}</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_3" role="tab" aria-controls="tab_3"
                                aria-selected="false">{{ __('web.gifted') }}</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab_4" role="tab" aria-controls="tab_4"
                                aria-selected="false">{{ __('web.ambassador') }}</a>
                        </li>

                    </ul>


                </div>
                <div class="tab-content" id="myTabContent">

                    <!--start tab-pane-->
                    <div class="tab-pane show active" id="tab_1" role="tabpanel" aria-labelledby="tab_1">
                        <div class="row">
                            
                            @forelse(auth()->user()->donates()->onces() as $once)
                                <!--start project-->
                                <div class="col-lg-6 prof-project-grid">
                                    <div class="prof-project">
                                        <div class="cart-data text-left-dir">
                                            <a href="{{ route('web.projects.show' , $once->project->slug) }}" target="_blank">
                                                <div class="cart-img sm-raduis">
                                                    <img src="{{ url('storage/' . $once->project->image) }}" alt="projects">
                                                </div>
                                                <h3 class="text-cap project-title">{{ $once->project ? $once->project->name : '-' }}</h3>
                                                <h4 class="text-cap first_color">{{ $once->project && $once->project->charity ? $once->project->charity->name : '-' }}</h4>
                                                <div class="stars auto-icon">
                                                    @for($i = 0; $i < 5; $i++)
                                                        <i class="fa fa-star {{ $i < $once->project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                    @endfor
                                                    <span class="first_color d-inline-block"> | {{ $once->cost }} {{ $once->currency->name }} </span>

                                                </div>
                                                <p class="first_color">
                                                    {!! $once->project->desc !!}
                                                </p>
                                            </a>
                                        </div>

                                        <div class="prof-project-btn">
                                            @if($once->project->active == 0 || $once->project->charity_id == null)

                                                <button class="custom-btn sm-raduis white-btn no-charity-project"><span>{{ __('web.Donate Again') }}</span></button>

                                                <form action="" method="" style="display: inline-block">
                                                    <button type="button" class="simple-btn tooltip-link big-btn no-charity-project"
                                                    data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                        src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img"></button>
                                                </form>

                                            @else 

                                                <button 
                                                    data-id="{{ $once->id }}" 
                                                    @if($donor_total_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                        data-url="{{ route('web.donate.again') }}"
                                                    @endif
                                                    data-type="once" 
                                                    class="custom-btn sm-raduis white-btn donate-again
                                                    @if($donor_total_supports >= 10000) user_donation_full @endif" 
                                                    ><span>{{ __('web.Donate Again') }}</span></button>

                                                <form action="{{route('web.profile.add-to-cart')}}" method="POST" style="display: inline-block">
                                                    @csrf
                                                    <input type="hidden" name="project_id" value="{{$once->project->id}}">
                                                    <button type="submit" class="simple-btn tooltip-link big-btn add-to-cart2"
                                                    data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                        src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img"></button>
                                                </form>

                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--end project-->
                                @empty
                                    <div class="col-lg-12 prof-project-grid">
                                        <div class="prof-project">
                                            <p>{{ __('web.no_data') }}</p>
                                        </div>
                                    </div>
                                @endforelse    

                        </div>
                    </div>
                    <!--end tab-pane-->

                    <!--start tab-pane-->
                    <div class="tab-pane" id="tab_2" role="tabpanel" aria-labelledby="tab_2">
                        <div class="row month-deducation">
                            
                            @forelse(auth()->user()->donates()->months() as $month)
                                <!--start project-->
                                <div class="col-lg-6 prof-project-grid remove-item">
                                    <div class="prof-project">
                                        <div class="cart-data text-left-dir">
                                            <a href="{{ route('web.projects.show' , $month->project->slug) }}" target="_blank">
                                                <div class="cart-img sm-raduis">
                                                    <img src="{{ url('storage/' . $month->project->image) }}" alt="projects">
                                                </div>
                                                <h3 class="text-cap project-title">{{ $month->project ? $month->project->name : '-' }}</h3>
                                                <h4 class="text-cap first_color">{{ $month->project && $month->project->charity ? $month->project->charity->name : '-' }}</h4>
                                                 <div class="stars auto-icon">
                                                    @for($i = 0; $i < 5; $i++)
                                                        <i class="fa fa-star {{ $i < $month->project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                    @endfor
                                                    <span class="first_color d-inline-block"> | {{ $month->cost }} {{ $month->currency->name }} </span>
                                                </div>
                                                <p class="first_color">
                                                    {!! $month->project->desc !!}
                                                </p>
                                            </a>
                                        </div>

                                        {{--<div class="prof-project-btn">
                                            <button class="custom-btn sm-raduis white-btn donate-again" data-id="{{ $once->id }}" data-url="{{ route('web.donate.again') }}"><span>{{ __('web.Donate Again') }}</span></button>
                                            <button type="button" class="simple-btn tooltip-link big-btn add-to-cart2"
                                                data-tippy-placement="top" title="add to cart"><img
                                                    src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img"></button>

                                        </div>--}}

                                        <div class="prof-month-edit auto-icon">
                                            @if($month->project->active == 0 || $month->project->charity_id == null)

                                                <button type="button" class="simple-btn no-charity-project">
                                                    <i class="fa fa-pen"></i>
                                                </button>

                                            @else

                                                <button type="button" class="simple-btn update-donate-month" data-type="month" data-id="{{ $month->id }}" data-url="{{ route('web.check.project') }}" data-project="{{ $month->project->id }}">
                                                    <i class="fa fa-pen"></i>
                                                </button>

                                            @endif
                                            <button class="simple-btn remove-project-month" data-id="{{ $month->id }}" data-url="{{ route('web.delete.project') }}">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <!--end project-->
                                @empty
                                    <div class="col-lg-12 prof-project-grid">
                                        <div class="prof-project">
                                            <p>{{ __('web.no_data') }}</p>
                                        </div>
                                    </div>
                                @endforelse    

                        </div>
                    </div>
                    <!--end tab-pane-->

                    <!--start tab-pane-->
                    <div class="tab-pane" id="tab_3" role="tabpanel" aria-labelledby="tab_3">
                        <div class="row">

                        @forelse(auth()->user()->donates()->gifts() as $gift)
                                <!--start project-->
                                <div class="col-lg-6 prof-project-grid">
                                    <div class="prof-project">
                                        <div class="cart-data text-left-dir">
                                            <a href="{{ route('web.projects.show' , $gift->project->slug) }}" target="_blank">
                                                <div class="cart-img sm-raduis">
                                                    <img src="{{ url('storage/' . $gift->project->image) }}" alt="projects">
                                                </div>
                                                <h3 class="text-cap project-title">{{ $gift->project ? $gift->project->name : '-' }}</h3>
                                                <h4 class="text-cap first_color">{{ $gift->project && $gift->project->charity ? $gift->project->charity->name : '-' }}</h4>
                                                <div class="stars auto-icon">
                                                    @for($i = 0; $i < 5; $i++)
                                                        <i class="fa fa-star {{ $i < $gift->project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                    @endfor
                                                    <span class="first_color d-inline-block"> | {{ $gift->cost }} {{ $gift->currency->name }} </span>
                                                </div>
                                                <p class="first_color">
                                                    {!! $gift->project->desc !!}
                                                </p>
                                            </a>
                                        </div>

                                        <div class="prof-project-btn">
                                            @if($gift->project->active == 0 || $gift->project->charity_id == null)

                                                <button class="custom-btn sm-raduis white-btn no-charity-project"><span>{{ __('web.Donate Again') }}</span></button>

                                                <form action="" method="" style="display: inline-block">
                                                    <button type="button" class="simple-btn tooltip-link big-btn no-charity-project"
                                                    data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                        src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img"></button>
                                                </form>

                                            @else 

                                                <button 
                                                    data-type="gift" 
                                                    data-id="{{ $gift->id }}" 
                                                    @if($donor_total_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                        data-url="{{ route('web.donate.again') }}"
                                                    @endif
                                                    class="custom-btn sm-raduis white-btn donate-again
                                                    @if($donor_total_supports >= 10000) user_donation_full @endif" 
                                                    ><span>{{ __('web.Donate Again') }}</span></button>
                                                <form action="{{route('web.profile.add-to-cart')}}" method="POST" style="display: inline-block">
                                                    @csrf
                                                    <input type="hidden" name="project_id" value="{{$gift->project->id}}">
                                                    <button type="submit" class="simple-btn tooltip-link big-btn add-to-cart2"
                                                    data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                        src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img"></button>
                                                </form>

                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <!--end project-->
                                @empty
                                    <div class="col-lg-12 prof-project-grid">
                                        <div class="prof-project">
                                            <p>{{ __('web.no_data') }}</p>
                                        </div>
                                    </div>
                                @endforelse                      


                        </div>
                    </div>
                    <!--end tab-pane-->

                    <!--start tab-pane-->
                    <div class="tab-pane" id="tab_4" role="tabpanel" aria-labelledby="tab_4">
                        <div class="row">


                        @forelse(auth()->user()->ambassador()->get() as $key=>$ambassador)
                                @php
                                    $donate = auth()->user()->donates()->where([['project_id', $ambassador->project->id], ['type', 'ambassador']])->first();
                                    if($donate){

                                        $support = $donate->cost.' '.$donate->currency->name;
                                    }
                                    else{

                                        $support = trans('web.no_support_');
                                    }
                                @endphp
                                <!--start project-->
                                <div class="col-lg-6 prof-project-grid">
                                    <div class="prof-project">
                                        <div class="cart-data text-left-dir">
                                            <a href="{{ route('web.projects.show' , $ambassador->project->slug) }}" target="_blank">
                                                <div class="cart-img sm-raduis">
                                                    <img src="{{ url('storage/' . $ambassador->project->image) }}" alt="projects">
                                                </div>
                                                <h3 class="text-cap project-title">{{ $ambassador->project ? $ambassador->project->name : '-' }}</h3>
                                                <h4 class="text-cap first_color">{{ $ambassador->project && $ambassador->project->charity ? $ambassador->project->charity->name : '-' }}</h4>
                                                <div class="stars auto-icon">
                                                    @for($i = 0; $i < 5; $i++)
                                                        <i class="fa fa-star {{ $i < $ambassador->project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                    @endfor
                                                    <span class="first_color d-inline-block"> | {{ $support }} </span>
                                                </div>
                                                <p class="first_color">
                                                    {!! $ambassador->project->desc !!}
                                                </p>
                                            </a>
                                        </div>

                                        <div class="prof-project-btn">
                                            @if($ambassador->project->active == 0 || $ambassador->project->charity_id == null)

                                                <button class="custom-btn sm-raduis white-btn no-charity-project"><span>{{ __('web.Donate Again') }}</span></button>

                                                <form action="" method="" style="display: inline-block">
                                                    <button type="button" class="simple-btn tooltip-link big-btn no-charity-project"
                                                    data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                        src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img"></button>
                                                </form>

                                            @else 

                                                @if(in_array($ambassador->project->id , auth()->user()->donates()->where('type' , 'ambassador')->pluck('project_id')->toArray()))
                                                    <button 
                                                        data-type="ambassador" 
                                                        data-id="{{ auth()->user()->donates()->ambassadors()->last()['id'] }}" 
                                                        @if($donor_total_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                            data-url="{{ route('web.donate.again') }}"
                                                        @endif
                                                        class="custom-btn sm-raduis white-btn donate-again
                                                        @if($donor_total_supports >= 10000) user_donation_full @endif" 
                                                    ><span>{{ __('web.Donate Again') }}</span></button>
                                                @else
                                                <button class="custom-btn sm-raduis white-btn donate-now" data-id="{{ $ambassador->project->id }}" data-toggle="modal" data-target="#donate_again"><span>Donate</span></button>                                            
                                                @endif
                                                <form action="{{route('web.profile.add-to-cart')}}" method="POST" style="display: inline-block">
                                                    @csrf
                                                    <input type="hidden" name="project_id" value="{{$ambassador->project->id}}">
                                                    <button type="submit" class="simple-btn tooltip-link big-btn add-to-cart2"
                                                    data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                        src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img"></button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <!--end project-->
                                @empty
    
                                <div class="col-lg-12 prof-project-grid">
                                    <div class="prof-project">
                                        <p>{{ __('web.no_data') }}</p>
                                    </div>
                                </div>

                                @endforelse                       


                        </div>
                    </div>
                    <!--end tab-pane-->

                </div>
            </div>
        </div>
    </section>
    <!--end profile-pg-->



    <!-- BEGIN: month modal-->
    <div class="modal fade select-modal month-modal" id="modal-month" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="modal-header col-12">
                        <h5 class="modal-title white-text text-cap">{{ __('web.Deduction Ratio') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="user-div text-center">
                        <form action="#" class="donate-form">

                            <input type="hidden" name="project_id" id="project_id">
                            <input type="hidden" name="month_id" id="month_id">

                            <div class="form-group has-side  text-left-dir">
                                <h2 class="second_color text-cap">{{ __('web.Donate monthly') }}?</h2>
                                <p>{{ __('web.You can cancel at any time') }}</p>

                                <div class="switch-container">
                                    <div class="switch_box">
                                        <input type="checkbox" checked id="switch_1" value="month" name="switch_1" class="switch_1">
                                    </div>
                                </div>
                            </div>
                            <span class="custom-error error"> </span>
                            <div class="donate-quantity text-center form-group">
                                <div class="form-group number-input-div">
                                    <span class="minus-num  numb-control">-</span>
                                    <input type="number" step=".01"
                                        class="form-control no_apperance_number number-input sm-raduis-input" id="number-input"
                                        name="donate_price" minlength="2" maxlength="7" value="{{ $cost_min }}" pattern="\d{5}"
                                        required>
                                    <span class="plus-num numb-control">+</span>
                                </div>

                                <div class="form-group">
                                    <select class="form-control select-input sm-raduis-input " id="currency_id" name="currency_id" required>
                                        @foreach($currencies as $currency)    
                                            <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            
                            </div>

                            <div class="text-center">
                                <button type="button" id="add-class" class="custom-btn sm-raduis white-btn update-month" data-url="{{ route('web.donate.month.update') }}"><span>{{ __('web.save') }}</span></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END: month modal-->

    <!--donate again modal-->
    <div class="modal fade" id="donate_again" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
     <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
         <div class="modal-content">

             <div class="modal-body">
                 <div class="modal-header col-12">
                     <h5 class="modal-title white-text text-cap">{{ __('web.donate_now') }}</h5>
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                         <span aria-hidden="true">&times;</span>
                     </button>
                 </div>
                 <div class="user-div">
                        <div class="ambasador-donate sm-raduis" style="display: block;">
                            <form action="" class="donate-ambsaador-form">

                                <input type="hidden" name="project" id="project">

                                <div class="form-group number-input-div">
                                    <span class="minus-num  numb-control">-</span>
                                    <input type="number" step=".01" class="form-control no_apperance_number number-input donate_price_ambassador"
                                        minlength="2" maxlength="7" id="price" value="{{ $cost_min }}" name="donate_price" pattern="\d{5}" required>
                                    <span class="plus-num numb-control">+</span>

                                </div>

                                <div class="form-group">
                                    <select class="form-control select-input" name="currency" id="currency">

                                        @foreach ($currencies as $currency)
                                                            
                                            <option value="{{$currency->id}}" @if(session('currency') == $currency->id) selected @endif>{{$currency->symbol}}</option>
                                        
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <button type="button"
                                    class="custom-btn sm-raduis white-btn send-donate" data-url="{{ route('web.project.store') }}"><span>{{ __('web.donate') }}</span></button>
                                    </div>
                                </form>
                                <span class="custom-error error"> </span>
                        </div>
                 </div>
             </div>
         </div>
     </div>
 </div>
 <!--end donate-again-->