<!-- start profile-pg
    ================ -->
<section class="profile-pg margin-div">
    <div class="container">
        <div class="row">
            <div class="col-lg-6" data-aos="fade-in">
                <h3 class="second_color text-cap profile-title">{{ __('web.personal_information') }} :</h3>
                <form action="{{ route('web.update.profile') }}" method="POST" class="gray-form login-form">
                    @csrf

                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="{{  auth()->user()->name }}">
                        @if($errors->has('name'))
                            <div class="error">{{ $errors->first('name') }}</div>
                        @endif
                        <label class="moving-label">{{ __('web.name') }}</label>
                    </div>


                    <div class="form-group">
                        <input type="email" class="form-control" name="email" value="{{  auth()->user()->email }}">
                       @if($errors->has('email'))
                            <div class="error">{{ $errors->first('email') }}</div>
                        @endif
                        <label class="moving-label">{{ __('web.email') }}</label>
                    </div>


                    <div class="form-group">
                        <select class="form-control select-input" name="country">
                            <option disabled></option>
                            @foreach($countries as $key => $country)
                            <option value="{{$key}}" @if(auth()->user()->country == $key) selected @endif>{{ $country }}
                            </option>
                            @endforeach
                        </select>
                        @if($errors->has('country'))
                            <div class="error">{{ $errors->first('country') }}</div>
                        @endif
                        <label class="moving-label">{{ __('web.country') }}</label>
                       
                    </div>


                    <div class="form-group">
                        <input type="phone" class="form-control" id="phone" name="phone_num" placeholder="{{ __('web.phone') }}" value="{{  auth()->user()->phone }}">
                       @if($errors->has('phone'))
                            <div class="error">{{ $errors->first('phone') }}</div>
                        @endif
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password" value="">
                        @if($errors->has('password'))
                            <div class="error">{{ $errors->first('password') }}</div>
                        @endif
                        <label class="moving-label">{{ __('web.password') }}</label>
                    </div>

                    <div class="form-group">
                        <input type="password" class="form-control" id="password_confirmation"
                            name="password_confirmation" value="">
                        @if($errors->has('password_confirmation'))
                            <div class="error">{{ $errors->first('password_confirmation') }}</div>
                        @endif
                        <label class="moving-label">{{ __('admin.password_confirmation') }}</label>
                      
                    </div>

                    <div class="btn-div">
                        <button type="submit"
                            class="custom-btn sm-raduis white-btn"><span>{{ __('web.save') }}</span></button>
                    </div>

                </form>
            </div>

        </div>
    </div>
</section>
<!--end profile-pg-->

@push('js')
    <script type="text/javascript" src="{{asset('web/js/intlTelInput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/profile.js')}}"></script>
@endpush