
   
   <!-- start profile-pg
        ================ -->
        <section class="profile-pg margin-div">
            <div class="container">
                <div class="row">
                    <div class="col-12" data-aos="fade-in">
                        <h3 class="second_color text-cap profile-title">{{__('web.reports')}} :</h3>
                    </div>
                </div>
                <div class="reports-div">
                    <div class="row">
                        @php
                            $donates =  auth()->user()->donates()->whereHas('project', function($q){
                                $q->where('status', 'approved');
                            })->groupBy('project_id')->get();
                        @endphp
                        <!--start report-->
                        @forelse($donates as $donate)
    
                            @if($donate->project && $donate->project->status == 'approved' && count($donate->project->reports()->where('status', 'approved')->get()) > 0)
                                <div class="col-xl-4 col-md-6" data-aos="fade-in">
                                    <div class="report">
                                        <div class="cart-data text-left-dir">
                                            <a href="{{ route('web.report' , $donate->project->slug) }}" target="_blank">
                                                <div class="cart-img sm-raduis">
                                                    <img src="{{ url('storage/' . $donate->project->image) }}" alt="projects">
                                                </div>
                                                <h3 class="text-cap project-title">{{ $donate->name }}</h3>
                                                <div class="stars auto-icon">
                                                    @for($i = 0; $i < 5; $i++)
                                                        <i class="fa fa-star {{ $i < $donate->project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                    @endfor
                                                </div>
                                                <p class="first_color">
                                                    {{ __('web.Reports no') }}. : {{ count($donate->project->reports) }}
                                                </p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-xl-4 col-md-6" data-aos="fade-in">
                                    <div class="report">
                                        {{__('web.no_data')}}
                                    </div>
                                </div>
                            @endif
    
                        @empty
                            <div class="col-xl-4 col-md-6" data-aos="fade-in">
                                <div class="report">
                                    {{__('web.no_data')}}
                                </div>
                            </div>
                        @endforelse
                        <!--end report-->
    
                    </div>
                </div>
            </div>
        </section>
        <!--end profile-pg-->