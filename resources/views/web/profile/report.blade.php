    
    <!-- start profile-pg
        ================ -->
        <section class="profile-pg margin-div">
        <div class="container">
            <div class="row">
                <div class="col-12" data-aos="fade-in">
                    <h3 class="second_color text-cap profile-title">{{ __('web.reports') }} :</h3>
                </div>
            </div>
            <div class="reports-div">
                <div class="row">
                    <!--start report-->
                    <div class="col-xl-9 col-sm-8">
                        <div class="report">
                            <div class="cart-data text-left-dir">
                                <a href="{{ route('web.report' , $project->slug) }}" target="_blank">
                                    <div class="cart-img sm-raduis">
                                        <img src="{{ url('storage/' . $project->image) }}" alt="projects">
                                    </div>
                                    <h3 class="text-cap project-title">{{ $project->name }}</h3>
                                    <div class="stars auto-icon">
                                        @for($i = 0; $i < 5; $i++)
                                            <i class="fa fa-star {{ $i < $project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                        @endfor
                                    </div>
                                    <p class="first_color d-sm-none d-block">
                                        {{ __('web.Reports no') }}. : {{ $project->reports()->count() }}
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-4 text-right-dir d-sm-block d-none">
                        <p class="first_color">
                            {{ __('web.Reports no') }}. : {{ $project->reports()->where('status', 'approved')->count() }}
                        </p>
                    </div>
                    <!--end report-->
                </div>

                <div class="personal-information first_color text-cap">

                    @foreach($project->reports()->where('status', 'approved')->get() as  $report)

                        <div class="form-group" data-aos="fade-up">
                            <a href="{{route('web.profile.get-report-details', $report->slug)}}" class="sm-raduis">{{ $report->phase ? $report->phase->name : trans('admin.project_report') }}</a>
                        </div>

                    @endforeach
                </div>

            </div>
        </div>
    </section>
    <!--end profile-pg-->