        <!-- start profile-pg
         ================ -->
         <section class="profile-pg margin-div">
            <div class="container">
                <div class="row">

                        <div class="col-12" data-aos="fade-in">
                            <h3 class="second_color text-cap profile-title has-side">{{ __('web.personal_information') }} :<a
                                    href="{{ route('web.edit.profile') }}"><i class="fa fa-edit"></i></a></h3>
                            <div class="personal-information first_color">
                                <div class="form-group" data-aos="fade-up">
                                    <div class="sm-raduis"> <span class="second_color text-cap">{{ __('web.name') }} :</span> {{  auth()->user()->name }} </div>
                                </div>

                                <div class="form-group" data-aos="fade-up">
                                    <div class="sm-raduis"> <span class="second_color text-cap">{{ __('web.email') }} :</span>
                                    {{  auth()->user()->email }} </div>
                                </div>

                                <div class="form-group" data-aos="fade-up">
                                    <div class="sm-raduis"> <span class="second_color text-cap">{{ __('web.country') }} :</span> {{  auth()->user()->country ? Countries::getOne(auth()->user()->country, \App::getLocale()) : '' }} </div>
                                </div>

                            </div>
                        </div>                    

                </div>
            </div>
        </section>
        <!--end profile-pg-->