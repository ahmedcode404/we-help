    
    <!-- start profile-pg
        ================ -->
        <section class="profile-pg margin-div">
        <div class="container">
            <div class="row">
                <div class="col-12" data-aos="fade-in">
                    <h3 class="second_color text-cap profile-title">{{ __('web.settings') }}:</h3>
                </div>
            </div>

            <div class="setting-div">
                <form class="setting-form">
                    <!--start remove pop-->
                    <div class="modal fade" id="remove-modal" tabindex="-1" role="dialog"
                        aria-labelledby="remove-mod" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">

                                <div class="modal-body text-center">
                                    <div class="modal-header col-12">
                                        <h5 class="modal-title white-text text-cap" id="remove-mod">{{ __('web.Delete Account') }}
                                        </h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="login-pop-div  text-cap text-center">
                                        <div class="form-group second_color">
                                            {{ __('web.Are you want to delete Account') }}
                                        </div>

                                        <div class="text-center"><button type="button"
                                                class="custom-btn sm-raduis white-btn delete-account" url="{{ route('web.delete.account') }}" redirect="{{ route('web.home') }}"><span>{{ __('web.Delete') }}</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end remove pop-->

                    <div class="row">

                        <div class="col-lg-6" data-aos="fade-in">
                            <div class="border-setting sm-raduis">
                                <h3 class="arrow-profile-text first_color"><i
                                        class="fa fa-globe second_color"></i>English <span class="gray-color">(
                                        Default )</span></h3>
                                <div class="slide-profile">
                                    <div class="custom-checkbox full-width-checkbox white-checkbox">
                                        <input type="radio" name="radio_1" class="hidden-input change-langouage" url="{{route('lang', 'en')}}" {{ \App::getLocale() == 'en' ? 'checked' : '' }} id="check_1"
                                            checked>
                                        <label for="check_1">English</label>
                                    </div>
                                    <div class="custom-checkbox full-width-checkbox white-checkbox">
                                        <input type="radio" name="radio_1" class="hidden-input change-langouage" url="{{route('lang', 'ar')}}" {{ \App::getLocale() == 'ar' ? 'checked' : '' }} id="check_2">
                                        <label for="check_2">اللغة العربية</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6" data-aos="fade-in">
                            <div class="border-setting sm-raduis">
                                <div class="first_color switch-container">
                                    <i class="fa fa-bell second_color"></i>{{ __('web.Notification') }}
                                    <div class="switch_box">
                                        <input type="checkbox" id="active-notification" url="{{ route('web.active.notification') }}" {{ auth()->user()->is_notify == 1 ? 'checked' : '' }} class="switch_1">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6" data-aos="fade-in">
                            <div class="border-setting sm-raduis">
                                <a href="{{ route('web.profile.projects') }}" class="first_color full-width-btn"><i
                                        class="fa fa-file-invoice-dollar second_color"></i>{{ __('web.Edit Monthly Deduction') }}</a>
                            </div>
                        </div>

                        @if(count(auth()->user()->donates) == 0)
                            <div class="col-lg-6" data-aos="fade-in">
                                <div class="border-setting sm-raduis">
                                    <button type="button"
                                        class="simple-btn first_color full-width-btn text-left-dir remove-account"
                                        data-target="#remove-modal" data-toggle="modal"><i
                                            class="fa fa-trash-alt second_color"></i>{{ __('web.Delete Account') }}</button>
                                </div>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
            <!--end div-->
        </div>
    </section>
    <!--end profile-pg-->