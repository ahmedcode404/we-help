        <!-- start profile-pg
         ================ -->
         <section class="profile-pg margin-div">
            <div class="container">
                <div class="row">
                    <div class="col-12" data-aos="fade-in">
                        {{-- <h3 class="second_color text-cap profile-title">{{ __('web.Donated Money') }} :</h3> --}}
                        <table class=" table text-center" style="width:100%">
                            <thead>
                                <tr class="text-cap">
                                    <th>{{ __('web.projects') }}</th>
                                    <th>{{ __('web.project_goal') }}</th>
                                    <th>{{ __('web.Donated Money') }}</th>
                                    <th>{{ __('web.voucher') }}</th>
                                </tr>
                            </thead>
                            <tbody>

                                {{-- {{ dd(auth()->user()->catch_vouchers) }} --}}
                                    @forelse(auth()->user()->donates as $donate)
                                        <tr>
                                            <td>
                                                <div class="cart-data text-left-dir">
                                                    <a href="{{ route('web.projects.show' , $donate->project->slug) }}" target="_blank">
                                                        <div class="cart-img sm-raduis">
                                                            <img src="{{ url('storage/' . $donate->project->image) }}" alt="projects">
                                                        </div>
                                                        <h3 class="text-cap project-title">{{ $donate->project->name }}</h3>
                                                        <div class="stars auto-icon">
                                                            @for($i = 0; $i < 5; $i++)
                                                                <i class="fa fa-star {{ $i < $donate->project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                            @endfor
                                                        </div>
                                                        <p class="first_color">
                                                            {{ $donate->project->desc }}
                                                        </p>
                                                    </a>
                                                </div>
                                            </td>

                                            <td>
                                                <span class="responsive-table-title">Project Goal</span>
                                                {{exchange($donate->project->get_total, $donate->project->currency_id, session('currency'))}} {{currencySymbol(session('currency'))}}
                                            </td>

                                            <td>
                                                <span class="responsive-table-title">Donated Money</span>
                                                {{exchange($donate->cost, $donate->currency_id, $donate->currency_id)}} {{currencySymbol($donate->currency_id)}}
                                            </td>
                                            <td>
                                                @php
                                                    $vouchers =  auth()->user()->catch_vouchers()->where([['project_id', $donate->project_id], ['out_to_charity', $donate->cost]])->get();
                                                @endphp

                                                @if(count($vouchers) == 1)
                                                    @foreach ($vouchers as $voucher)
                                                        <a class="second_color first_color_hover" href="{{ route('get-voucher', $voucher->slug) }}" target="_blank">{{ trans('admin.show') }}</a>
                                                    @endforeach
                                                @elseif(count($vouchers) > 1)
                                                    <button class="simple-btn second_color first_color_hover" data-toggle="modal" data-target="#modal-voucher">{{ trans('web.show_all_vouchers') }}</button>
                                                    
                                                    <div class="modal fade" id="modal-voucher" tabindex="-1" role="dialog" aria-labelledby="voucher-mod" aria-hidden="true">
                                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                                            <div class="modal-content">
                                                    
                                                                <div class="modal-body">
                                                                    <div class="modal-header col-12">
                                                                        <h5 class="modal-title white-text text-cap" id="voucher-mod">{{ trans('web.all_vouchers') }}</h5>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                        </button>
                                                                    </div>
                                                                    <div class="voucher-pop-div text-left-dir">
                                                                        @foreach ($vouchers as $key =>$voucher)
                                                                            <a href="{{ route('get-voucher', $voucher->slug) }}" class="second_color first_color_hover" target="_blank">فاتورة رقم ({{$key+1}})</a>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>

                                        </tr>
                                        @empty
                                            {{__('web.no_data')}}
                                        @endforelse

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </section>
        <!--end profile-pg-->