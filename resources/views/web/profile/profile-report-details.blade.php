<!-- start profile-pg
         ================ -->
<section class="profile-pg margin-div">
    <div class="container">
        <div class="row">
            @if($report)
                <div class="col-12" data-aos="fade-in">
                    <h3 class="second_color text-cap profile-title">{{$report->phase ? $report->phase->name : trans('admin.project_report')}} :</h3>

                    <div class="report-details">
                        @if($report->video)
                            <video width="100%" controls>
                                <source src="{{asset('storage/'.$report->video)}}" type="video/mp4">
                                    {{__('web.Your browser does not support the video tag')}}
                            </video>
                        @endif
                        
                        {!! $report->report !!}


                    </div>
                </div>
            @else
                {{ trans('admin.no_data') }}
            @endif
        </div>
    </div>
</section>
<!--end profile-pg-->