@extends('web.layout.app')

@section('title', __('web.profile') )
@section('description', __('web.profile') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

    <!--start select-modals-->
    <!--once modal-->
    @include('web.donate_popups.once')

    <!--gift modal-->
    @include('web.donate_popups.gift')

    <!--month modal-->
    @include('web.donate_popups.monthly')

    <!--ambassador modal-->
    @include('web.donate_popups.ambassador')
    
    <!--end select-modals-->

    <!-- start pages-header
     ================ -->
    <section class="pages-header text-center profile-header" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{ __('web.my account') }}</h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="profile-user">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="profile-img  third_color text-left-dir sm-center sm-raduis">
                            @if(auth()->guard('charity')->check())  

                                <form action="" class="uploade-file">
                                    @csrf
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type="file" id="imageUpload" data-url="{{ route('web.upload.image') }}" class="hidden-input"
                                                accept=".png, .jpg, .jpeg">
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview" style="background-image: url({{ url('storage/' . auth()->guard('charity')->user()->logo) }})">
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <h3 class="white-text">{{ auth()->guard('charity')->user()->name }}</h3>
                                {{ auth()->guard('charity')->user()->email }}

                            @else

                                <form action="" class="uploade-file">
                                    @csrf
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type="file" id="imageUpload" data-url="{{ route('web.upload.image') }}" class="hidden-input"
                                                accept=".png, .jpg, .jpeg">
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            @if(auth()->check() && auth()->user()->image)
                                                <div id="imagePreview" style="background-image: url({{ url('storage/' . auth()->user()->image) }});">
                                                </div>
                                            @else
                                                <div id="imagePreview" style="background-image: url({{ url('web/images/main/user.png') }});">
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                <h3 class="white-text">{{ auth()->user()->name }}</h3>
                                {{ auth()->user()->email }}

                            @endif
                        </div>
                    </div>

                    <div class="col-md-4 text-right-dir sm-center">
                        <a href="{{ route('web.setting') }}" class="custom-btn sm-raduis transparent-btn"><span><i
                                    class="fa fa-cog"></i>{{ __('web.settings') }}</span></a>
                    </div>

                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->


    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text d-sm-none d-block text-upper text-center page-title">{{ __('web.my account') }}</h2>
        </div> --}}


        <!--end div-->


        <!-- start profile-list
         ================ -->
        <section class="profile-list text-center text-cap">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <ul class="list-inline">
                            <li class="@if(\Route::currentRouteName() == 'web.profile') active @endif"><a href="{{ route('web.profile') }}"><i class="fa fa-file-alt"></i>{{ __('web.personal_information') }}</a></li>
                            <li class="@if(\Route::currentRouteName() == 'web.profile.money') active @endif"><a href="{{ route('web.profile.money') }}"><i class="fa fa-dollar-sign"></i>{{ __('web.donated_amounts') }}</a></li>
                            <li class="@if(\Route::currentRouteName() == 'web.profile.projects') active @endif"><a href="{{ route('web.profile.projects') }}"><i class="fa fa-box"></i>{{ __('web.projects') }}</a></li>
                            <li class="@if(\Route::currentRouteName() == 'web.profile.report') active @endif"><a href="{{ route('web.profile.report') }}"><i class="fa fa-file-invoice"></i>{{ __('web.reports') }}</a></li>
                            <li><a href="{{route('web.profile.cart')}}"><i class="fa fa-shopping-cart"></i>{{ __('web.cart') }}</a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </section>
        <!--end profile-list-->

        <!-- pesonal data -->
            @if(\Request::route()->getName() == "web.profile")
                @include('web.profile.personal-info')
            @endif
        <!-- pesonal data -->

        <!-- edit profile -->
            @if(\Request::route()->getName() == "web.edit.profile")
                @include('web.profile.edit-profile')
            @endif
        <!-- edit profile -->   
        
        <!-- profile money -->
            @if(\Request::route()->getName() == "web.profile.money")
                @include('web.profile.profile-money')
            @endif
        <!-- profile money -->   
        
        <!-- projects -->
            @if(\Request::route()->getName() == "web.profile.projects")
                @include('web.profile.profile-project')
            @endif
        <!-- projects -->   

        <!-- cart -->
            @if(\Request::route()->getName() == "web.profile.cart")
                @include('web.profile.cart')
            @endif
        <!-- cart --> 
    
        <!-- projects -->     
        
        <!-- profile report -->
            @if(\Request::route()->getName() == "web.profile.report")
                @include('web.profile.profile-report')
            @endif
        <!-- profile report -->   
        
        <!--  report -->
            @if(\Request::route()->getName() == "web.report")
                @include('web.profile.report')
            @endif
        <!--  report -->  
        
        <!--  report-details -->
        @if(\Request::route()->getName() == "web.profile.get-report-details")
            @include('web.profile.profile-report-details')
        @endif
        <!--  report-details --> 
        
        <!--  setting -->
            @if(\Request::route()->getName() == "web.setting")
                @include('web.profile.setting')
            @endif
        <!--  setting -->            


    </div>



@endsection

@push('js')

    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif

    <!--for this page only-->
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{ url('web/js/profile.js') }}"></script>   
    <script type="text/javascript" src="{{asset('web/js/cart.js')}}"></script> 
    <script type="text/javascript" src="{{asset('web/backend/cart.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/update-cart-price.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/remove-item.js')}}"></script>
    <script type="text/javascript" src="{{ asset('web/forms/donate-cart.js') }}"></script>
    
    <!-- validation laravel -->
    <script src="{{ url('custom/custom-validate.js') }}"></script>   


@endpush