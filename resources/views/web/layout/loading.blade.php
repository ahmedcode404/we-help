<!--start loading-->
<div class="loading">
    <div class="inner-loader">
        <object data="{{asset('web/images/main/logo-loading.svg')}}">
            <img src="{{asset('web/images/main/logo.png')}}" alt="logo">
        </object>

        <div class="loader">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
</div>
<!--end loading-->