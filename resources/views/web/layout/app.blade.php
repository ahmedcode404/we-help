<!DOCTYPE html>

@if(\App::getLocale() == 'ar')
    <html dir="rtl" class="language" lang="ar">
@else
    <html dir="ltr" class="language" lang="en">
@endif

<head>

    @yield('meta')

    <title>{{__('web.we_help')}} | @yield('title')</title>
    <meta name="description" content="@yield('description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta name="image" content="@yield('image')" />
    <meta name="author" content="{{__('web.we_help')}}" />

    <meta name="csrf-token" id="token" content="{{ csrf_token() }}" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('web/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('web/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('web/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('web/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('web/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('web/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('web/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('web/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('web/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('web/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('web/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('web/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('web/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('web/images/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#000">
    <meta name="msapplication-TileImage" content="{{asset('web/images/favicon/ms-icon-144x144.png')}}">


    @include('web.layout.styles')

    {{-- Social media share start --}}
    @if(\Route::currentRouteName() == 'web.projects.show')

        <script type="text/javascript"
        src="//platform-api.sharethis.com/js/sharethis.js#property=58b550fc949fce00118ce697&product=inline-share-buttons"
        async="async"></script>

    @endif
    {{-- Social media share end --}}

</head>

<body class="no-trans">
    <input type="hidden" id="lang" value="{{\App::getLocale()}}">
    <input type="hidden" name="auth" id="auth" value="{{ auth()->check() ? auth()->user()->id : '0'}}">     
    <input type="hidden" id="tel_url" value="{{asset('web/js/utils.js')}}">
    <input type="hidden" id="url-save-token" value="{{  route('save-token') }}">
    <!--start login pop-->
    @include('web.auth.login_popup.select-type')
    @include('web.auth.register_popup.select-type')
    <!--end login pop-->

    <!--start theme-colors-->
    {{-- <div class="custom-colors auto-icon">
        <span class="theme_color_icon" data-aos="fade-left"><i class="fa fa-cog"></i></span>
    </div>

    <div class="theme_config hirozintal-scroll">
        <div class="header-option theme-option">
            <h3>{{ __('web.header options') }}</h3>
            <div class="color-boxes">
                <h4 class="second_color">colors:</h4>
                <span data-theme-color="rgba(255, 255, 255, .7)">{{ __('web.light') }}</span>
                <span data-theme-color="#fff">{{ __('web.white') }}</span>
            </div>

            <div class="position-boxes">
                <h4 class="second_color">{{ __('web.position') }}:</h4>
                <span data-theme-position="fixed">{{ __('web.fixed') }}</span>
                <span data-theme-position="absolute">{{ __('web.relative') }}</span>
            </div>
        </div>

        <div class="footer-option theme-option">
            <h3>footer options</h3>
            <div class="color-boxes muti-colors">
                <h4 class="second_color">{{ __('web.colors') }}:</h4>
                <span data-theme-color="#000" style="background-color:#000;"></span>
                <span data-theme-color="#222" style="background-color:#222;"></span>
                <span data-theme-color="#333" style="background-color:#333;"></span>
                <span data-theme-color="#03abbb" style="background-color:#03abbb"></span>
                <span data-theme-color="#9c9c9c" style="background-color:#9c9c9c;"></span>
            </div>
        </div>
        <div class="body-option theme-option">
            <h3>{{ __('web.body backgrounds') }}</h3>
            <div class="color-boxes muti-colors">
                <h4 class="second_color">{{ __('web.colors') }}:</h4>
                <span data-theme-color="#fff2d8" style="background-color:#fff2d8"></span>
                <span data-theme-color="#ece1ff" style="background-color:#ece1ff"></span>
                <span data-theme-color="#faf3e4" style="background-color:#faf3e4;"></span>
                <span data-theme-color="#eee" style="background-color:#eee;"></span>
            </div>
        </div>

        <div class="text-center"><button class="reset-color custom-btn big-raduis white-btn"><span>{{ __('web.reset to default') }}</span></button></div>

        <button class="simple-btn auto-icon close-theme-config"><i class="fa fa-times"></i></button>
    </div> --}}
    <!--end theme-colors-->


    <div class="scroll_progress">
        <div></div>
    </div>


    <!--for index-only-->
    <div class="fast-scroll">
        <a href="#sec_1" class="tooltip-link active" title="projects"><img src="{{asset('web/images/icons/menu/3.png')}}" alt="icon"></a>
        <a href="#sec_2" class="tooltip-link" title="statistics"><img src="{{asset('web/images/icons/menu/4.png')}}" alt="icon"></a>
        <a href="#sec_3" class="tooltip-link" title="parteners"><img src="{{asset('web/images/icons/menu/11.png')}}" alt="icon"></a>
        <a href="#sec_4" class="tooltip-link" title="about us"><img src="{{asset('web/images/icons/menu/5.png')}}" alt="icon"></a>
        <a href="#sec_5" class="tooltip-link" title="contact us"><img src="{{asset('web/images/icons/menu/6.png')}}" alt="icon"></a>
    </div>
    <!--end-->
    
    @include('web.layout.header')

    <div class="main-content"> 

        @yield('content')
            <div class="bg_spinner" style="display: none;">
                <div class="spinner-border text-info process" role="status">
                    <span class="sr-only">Loading...</span>
                </div> 
            </div>
        @include('web.layout.footer')

        <!--start copyright-->
        <div class="copyright text-center">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        {{ __('web.All Rights Reserved by') }} We Help © 2021
                    </div>
                </div>
            </div>
        </div>
        <!--end copyright-->
    </div>

    <div class="fixed-menu text-center auto-icon">
        <div class="row align-content-center no-marg-row">
            <div class="col active">
                <a href="{{route('web.home')}}"><i class="fa fa-home"></i></a>
            </div>

            <div class="col cart-count">
                <a href="{{route('web.carts.index')}}"> 
                    <span class="num">{{ getCart('count') }}</span>
                    <i class="fa fa-shopping-cart"></i>
                </a>
            </div>

            <div class="col">
                @if(\App::getLocale() == 'ar')
                    <a href="" class="change-langouage" url="{{route('lang', 'en')}}"><i class="fa fa-globe"></i></a>
                @else
                    <a href="" class="change-langouage" url="{{route('lang', 'ar')}}"><i class="fa fa-globe"></i></a>
                @endif
            </div>

            <div class="col">
                <a href="{{route('web.notifications.index')}}">
                    <span class="num">@if(auth()->check()) {{auth()->user()->notificationsCount()}} @else 0 @endif</span>
                    <i class="far fa-bell"></i></a>
            </div>
            <div class="col responsive-search">
                <button><i class="fa fa-search"></i></button>
            </div>
        </div>
    </div>

    @include('web.layout.scripts')
     
    </body>
     
</html>