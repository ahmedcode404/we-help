<!-- scripts
================ -->
<script type="text/javascript" src="{{asset('web/js/html5shiv.min.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/respond.min.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/aos.min.js')}}"></script>
<script type="text/javascript" src="https://unpkg.com/tippy.js@2.5.4/dist/tippy.all.min.js"></script>
<script type="text/javascript" src="{{asset('web/js/jquery.validate.min.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/sweetalert2.all.min.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/custom-sweetalert.js')}}"></script>
<script type="text/javascript" src="{{asset('web/js/select2.min.js')}}"></script>

@if(\App::getLocale() == 'ar')
    <!--for arabic-only-->
    <script type="text/javascript" src="{{asset('web/js/select-ar.js')}}"></script>
@endif
<script type="text/javascript" src="{{asset('web/js/custom.js')}}"></script>
{{-- <script type="text/javascript" src="{{asset('web/js/theme-options.js')}}"></script> --}}
<script type="text/javascript" src="{{asset('web/js/change-currency.js')}}"></script>

<script type="text/javascript" src="{{asset('web/js/submit_simple_forms.js')}}"></script>

<!-- BEGIN: firebase -->
<!-- <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-analytics.js"></script> -->
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js"></script>
<script src="{{ url('dashboard/notifications/take-token.js') }}"></script>
<!-- <script src="{{ url('dashboard/notifications/notification.js') }}"></script> -->
<!-- <script src="{{ url('firebase-messaging-sw.js') }}"></script> -->
<!-- END: firebase -->

<script>
    // filter projects by country selected from header
    $('.filter-projects').change(function(){

        window.location.href = $(this).val();
    })
</script>

@if(\App::getLocale() == 'ar')
    <!--for arabic-only-->
    <script type="text/javascript" src="{{asset('web/js/messages_ar.min.js')}}"></script>
@endif

{{-- Sweet Alerts Start --}}
@if(session()->has('success'))
    <script>
        
        // setTimeout(function(){
            general_alert("{{ session()->get('success') }}" , "{{ session()->get('html') }}", 'success');
        // }, 2000);

        '{{session()->forget("success")}}';
    </script>
@endif

@if(session()->has('error'))
    <script>
        // setTimeout(function(){
            general_alert("{{ session()->get('error')}}" , "{!! session()->get('html') !!}", 'error');
        // }, 2000);
        '{{session()->forget("error")}}';
    </script>
@endif

@if(session()->has('warning'))
    <script>
        // setTimeout(function(){
            general_alert("{{ session()->get('warning')}}" , "{{ session()->get('html') }}", 'warning');
        // }, 2000);
       
        '{{session()->forget("warning")}}';
    </script>
@endif

{{-- Sweet Alerts End --}}

@stack('js')

