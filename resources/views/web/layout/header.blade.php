<!-- start header
    ================ -->
    <header>
        <div class="container">
    
            <!-- start sidemenu -->
            <div class="side-menu">
                <div class="inner-side-menu">
                    <div class="side-menu-title text-center">
                        <img src="{{asset('storage/' . $header_logo) }}" alt="logo">
                        <button class="simple-btn auto-icon close-menu"><i class="fa fa-times"></i></button>
                    </div>
                    <div class="slide-menu-content hirozintal-scroll">
                        <ul class="list-unstyled text-cap">
    
                            @if(!Auth::check() && !Auth::guard('charity')->check())
                                <li>
                                    <a href="javascript.void();" data-toggle="modal" data-target="#modal-login"><img
                                            src="{{asset('web/images/icons/menu/1.png')}}" alt="icon">{{__('web.login')}} / {{__('web.register')}}</a>
                                </li>
                            @endif
    
                            @if((Auth::check()) || Auth::guard('charity')->check())
                                @php
                                    if(auth()->check() && ((auth()->user()->hasRole('supporter')) || (auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee')))){
                                        $url = route('web.profile');
                                    }
                                    else if(auth()->guard('charity')->check()){
                                        $url = route('charity.dashboard');
                                    }
                                    else if(auth()->check() && auth()->user()->hasRole('charity_employee')){
                                        $url = route('charity.dashboard');
                                    }
                                @endphp
                                <li>
                                    <a href="{{ $url }}">
                                        <img src="{{asset('web/images/icons/menu/1.png')}}" alt="icon">{{__('web.profile')}}
                                    </a>
                                </li> 
    
    
                                @if(auth()->check() && ((auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee'))))
                                    <li>
                                        <a href="{{ route('admin.home') }}">
                                            <img src="{{asset('web/images/icons/menu/1.png')}}" alt="icon">{{__('web.dashboard')}}
                                        </a>
                                    </li> 
                                @endif
                                
                            @endif
                            
                            <li class="@if(\Route::currentRouteName() == 'web.home') active @endif"><a href="{{route('web.home')}}"><img src="{{asset('web/images/icons/menu/2.png')}}" alt="icon">{{__('web.home')}}</a></li>
                            <li class="@if(\Route::currentRouteName() == 'web.get-category-projects') active @endif"><a href="{{route('web.get-category-projects', 'all')}}"><img src="{{asset('web/images/icons/menu/3.png')}}" alt="icon">{{__('web.projects')}}</a></li>
                            <li><a href="{{ route('web.faq') }}"><img src="{{ url('web/images/icons/menu/4.png') }}" alt="icon">{{__('web.FAQ')}}</a>
                            <li><a href="{{ route('web.statistics') }}"><img src="{{asset('web/images/icons/menu/4.png')}}" alt="icon">{{__('web.statistics')}}</a>
                            </li>
                            <li><a href="{{ route('web.about') }}"><img src="{{asset('web/images/icons/menu/5.png')}}" alt="icon">{{__('web.about_us')}}</a></li>
                            <li><a href="{{ route('web.contact') }}"><img src="{{asset('web/images/icons/menu/6.png')}}" alt="icon">{{__('web.contact_us')}}</a></li>
                            <li><a href="{{ route('web.pages', 'technical_support') }}"><img src="{{asset('web/images/icons/menu/7.png')}}" alt="icon">{{__('web.technical_support')}}</a></li>
    
                            @if(auth()->check() && !auth()->user()->blocked)
                                <li><a href="{{ route('web.donate') }}"><img src="{{asset('web/images/icons/menu/8.png')}}" alt="icon">{{__('web.donate_now')}}</a></li>
                            @endif
    
                            @if(Auth::check() || Auth::guard('charity')->check())
                                <li><a href="{{route('admin.logout')}}"><img src="{{asset('web/images/icons/menu/logout.png')}}" alt="icon">{{__('web.logout')}}</a></li>
                            @endif
                        </ul>
    
                        <div class="side-menu-select-boxs text-cap">
                            {{-- <div class="select-box">
                                <img src="{{asset('web/images/icons/menu/9.png')}}" alt="icons">
                                <select class="form-control select-input filter-projects" id="filter-projects">
                                    <option disabled selected>{{__("web.choose_country")}}</option>
                                    <option value="{{route('web.projects.filter-projects-country', 'reset')}}" {{ session('session-country') ? '' : 'selected' }}>{{ __('web.reset') }}</option>
                                    @foreach ($countries as $key => $country)
                                        <option value="{{route('web.projects.filter-projects-country', $key)}}" {{ $key == session('session-country') ? 'selected' : '' }}>{{$country}}</option>
                                    @endforeach
                                </select>
                            </div> --}}
    
                            <div class="select-box">
                                <img src="{{asset('web/images/icons/menu/10.png')}}" alt="icons">
                                <select class="form-control select-input" name="currency-default" id="currency-default" data-action="{{ route('web.currency') }}">
                                    <option disabled selected>{{ __('web.Choose Currency') }}</option>
                                    @foreach($currencies as $currency_provider)                                
                                        <option value="{{ $currency_provider->id }}" {{ $currency_provider->id == session('currency') ? 'selected' : '' }}>{{ $currency_provider->name }}</option>
                                    @endforeach
    
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end sidemenu-->
    
            <div class="inner-header sm-raduis">
                <div class="row align-items-center">
                    <!--start col-->
                    <div class="col-xl-2 col-lg-3 col-md-5 col-sm-4 col-9  logo-grid">
                        <!--start main-logo-->
                        <div class="main-logo">
                            <div class="nav-icon">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
    
                            <a href="{{route('web.home')}}">
                                @if($enable_logo_svg == 1)
                                    <object data="{{$header_logo ? asset('storage/' . $header_logo) : asset('web/images/main/logo.svg') }}">
                                        <img src="{{$header_logo ? asset('storage/' . $header_logo) :  asset('web/images/main/logo.png')}}" alt="logo">
                                    </object>
                                @else
                                    <object data="{{asset('web/images/main/logo.svg') }}">
                                        <img src="{{asset('web/images/main/logo.png') }}" alt="logo">
                                    </object>
                                @endif
    
                            </a>
                        </div>
                        <!--end main-logo-->
                    </div>
                    <!--end col-->

                    <!--start col-->
                    <div class="d-sm-none d-block col-3 text-right-dir user-res-link-grid">
                        <!--start user-res-link-->
                        <div class="user-res-link text-center auto-icon @if(auth()->check() || auth()->guard('charity')->check()) active @endif">
                            @php
                                if(auth()->check() && auth()->user()->hasRole('admin')){
                                    $url = route('admin.home');
                                }
                                else if(auth()->check() && auth()->user()->hasRole('supporter')){
                                    $url = route('web.profile');
                                }
                                else if(auth()->guard('charity')->check()){
                                    $url = route('charity.dashboard');
                                }
                                else{
                                    $url = 'javascript.void();';
                                }
                            @endphp
                            
                          <a href="{{ $url }}" @if(!auth()->check() && !auth()->guard('charity')->check()) data-toggle="modal" data-target="#modal-login" @endif>
                            @if(!auth()->check() && !auth()->guard('charity')->check())
                                <i class="fa fa-user"></i>
                            @else
                                <img src="{{asset('web/images/main/avater.jpg')}}" alt="user"/>
                            @endif
                        </a>
                        </div>
                        <!--end user-res-link-->
                    </div>
                    <!--end col-->

                       <!--start col-->
                       <div class="col-xl-6 col-lg-6 col-md-5 col-sm-4 text-right-dir main-header-links-grid d-lg-block d-none">
                        <!--start main-header-links-->
                        <div class="main-header-links">    
                            <a class="@if(\Route::currentRouteName() == 'web.home') active @endif" href="{{route('web.home')}}"><img src="{{asset('web/images/icons/menu/2.png')}}" alt="icon">{{__('web.home')}}</a>
                            <a class="@if(\Route::currentRouteName() == 'web.projects') active @endif" href="{{route('web.get-category-projects', 'all')}}"><img src="{{asset('web/images/icons/menu/3.png')}}" alt="icon">{{__('web.projects')}}</a>
                            @if(!auth()->check() && !auth()->guard('charity')->check())
                                <a class="" href="javascript.void();" data-toggle="modal" data-target="#modal-login"><img src="{{asset('web/images/icons/menu/1.png')}}" alt="icon">{{__('web.login')}}</a>
                                <a class="" href="javascript.void();" data-toggle="modal" data-target="#modal-register">{{__('web.register')}}</a>
                            @else
                                @if(auth()->check() && auth()->user()->hasRole('admin'))
                                    <a href="{{ route('admin.home') }}"><img src="{{asset('web/images/icons/menu/1.png')}}" alt="icon">{{__('web.dashboard')}}</a>
                                @elseif(auth()->check() && auth()->user()->hasRole('supporter'))
                                    <a href="{{ route('web.profile') }}"><img src="{{asset('web/images/icons/menu/1.png')}}" alt="icon">{{__('web.profile')}}</a>
                                @elseif(auth()->guard('charity')->check())
                                    <a href="{{ route('charity.dashboard') }}"><img src="{{asset('web/images/icons/menu/1.png')}}" alt="icon">{{__('web.dashboard')}}</a>
                                @endif
                                <a href="{{route('admin.logout')}}">{{__('web.logout')}}</a>
                            @endif
                        </div>
                        <!--end main-header-links--->
                    </div>
    
                    <!--end col-->
    
                    <!--start col-->
                    <div class="col-xl-4 col-lg-3 col-md-7 col-sm-8  col-7 text-right-dir  search-grid">
    
    
    
                        <div class="main-icons-grid">
    
    
                            <!--start main-header-icons-->
                            <div class="main-header-icons auto-icon">
    
    
                                <!--start search-->
                                <div class="header-icon search-main-div">
                                    <div class="search-bg">
                                        <div class="main-search">
                                            <form action="{{route('web.search')}}" method="POST" class="search-form" id="searchform">
                                                @csrf
    
                                                <input type="search" placeholder="{{__('web.search_here')}}"  name="token" class="form-control" required>
                                                <button><img src="{{asset('web/images/icons/search.png')}}" alt="search"></button>
                                            </form>
                                        </div>
                                        <button class="close-search d-xl-none d-inline-block auto-icon"><i
                                                class="fa fa-times"></i></button>
                                    </div>
                                </div>
                                <!--end search-->
    
                                <!--start languages-->
                                <div class="languages-choose d-sm-inline-block d-none">
                                    <div class="header-icon">
                                            @if(\App::getLocale() == 'en')
                                                <a href="" class="change-langouage icon-img {{ \App::getLocale() == 'ar' ? 'active' : '' }}" url="{{route('lang', 'ar')}}">ع</a>
                                            @else
                                                <a href="" class="change-langouage icon-img {{ \App::getLocale() == 'en' ? 'active' : '' }}" url="{{route('lang', 'en')}}">EN</a>
                                            @endif
                                    </div>
                                </div>
                                <!--end languages-->
    
                                <!--start cart-->
                                <div class="header-icon d-sm-inline-block d-none">
                                    <div class="icon-img  slide-link tooltip-link cart-count" data-tippy-placement="top"
                                        title="{{__('web.cart')}}">
                                        <img src="{{asset('web/images/icons/cart.png')}}" alt="cart">
                                        <span class="num">{{ getCart('count') }}</span>
                                    </div>
                                    <div class="slide-menu">
                                        <ul class="list-unstyled hirozintal-scroll">
                                            
                                            <div id="cart_holder">
                                                @php
                                                    $carts = getCart('limit');
                                                @endphp
                                                @foreach ($carts as $cart)
                                                    <li>
                                                        <a href="{{route('web.projects.show', $cart->project->slug)}}">
                                                            <img src="{{asset('storage/'.$cart->project->image)}}" alt="{{$cart->project->name }}">
                                                            <h3>{{$cart->project->name}}</h3>
                                                            {{$cart->cost}} {{$cart->currency->name}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </div>
                                            
    
                                        </ul>
    
                                        @if(getCart('count') > 0)
                                            <a href="{{route('web.carts.index')}}" class="more-slide-menu">{{__('web.show_all')}}</a>
                                        @else
                                            <a href="#" class="more-slide-menu">{{__('web.no_data')}}</a>
                                        @endif
    
                                    </div>
                                </div>
                                <!--end cart-->
    
    
                                <!--start notifications-->
                                <div class="header-icon d-sm-inline-block d-none">
                                    <div class="icon-img  slide-link tooltip-link" data-tippy-placement="top"
                                        title="{{__('web.notifications')}}">
                                        <img src="{{asset('web/images/icons/bell.png')}}" alt="bell">
                                        <span class="num">@if(auth()->check()) {{auth()->user()->notificationsCount()}} @else 0 @endif</span>
                                    </div>

                                    @if(auth()->check())
                                        <div class="slide-menu">
                                            <ul class="list-unstyled hirozintal-scroll">
                                                @php
                                                    $notifications = auth()->user()->getUnreadNotifications();
                                                @endphp
                                                @foreach($notifications as $not)
                                                    @php
                                                        if($not->title == 'new_voucher' || $not->title == 'transfered_donates' ||
                                                            $not->title == 'stop_montly_deduction'){
                                                                
                                                            $route = route('web.profile.money');
                                                        }
                                                        elseif($not->title == 'account_blocked' || $not->title == 'account_unblocked'){

                                                            $route = route('web.profile');
                                                        }
                                                        
                                                        if($not->voucher && $not->voucher->project){
                                                            $src = asset('storage/'.$not->voucher->project->image);
                                                            $name = $not->voucher->project->name;
                                                        }
                                                        else{

                                                            $src = asset('storage/' . $header_logo);
                                                            $name = '';
                                                        }
                                                        // else{
                                                        //     if($not->project){

                                                        //         $route = route('web.projects.show', $not->project->slug);
                                                        //     }
                                                        //     else{
                                                        //         $route="#";
                                                        //     }
                                                        // }
                                                    @endphp
                                                    <li>
                                                        <a href="{{$route}}">
                                                            <img src="{{ $src }}" alt="project">
                                                            <h3>{{ $name }}</h3>
                                                            {{$not->message}}
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            <a href="{{route('web.notifications.index')}}" class="more-slide-menu">{{__('web.show_all')}}</a>
                                        </div>
                                   @endif
    
                                </div>
                                <!--end notifications-->
    
                                   <!--start currency-->
                                   <div class="header-icon d-sm-inline-block d-none">
                                    <div class="icon-img  slide-link tooltip-link">
                                        <img src="{{asset('web/images/icons/menu/10.png')}}" alt="currency">
                                    </div>
                                        <div class="slide-menu currency-menu">
                                            <ul class="list-unstyled hirozintal-scroll">
                                                @foreach($currencies as $currency_provider)                                
                                                    <li>
                                                        <a class="currency-default" data-action="{{ route('web.currency') }}" data-id="{{ $currency_provider->id }}">
                                                            <h3>{{ $currency_provider->name }}</h3>
                                                        </a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
    
                                </div>
                                <!--end currency-->

                                <!--start responsive-search-->
                                <div class="header-icon header-search">
                                    <button class="icon-img responsive-search  slide-link">
                                        <img src="{{asset('web/images/icons/search.png')}}" alt="search">
                                    </button>
                                </div>
                                <!--end responsive-search-->
                            </div>
                            <!--end main-header-icons-->
    
                           
                        </div>
                    </div>
                    <!--end col-->
                </div>
            </div>
        </div>
    </header>
    <!--end header-->