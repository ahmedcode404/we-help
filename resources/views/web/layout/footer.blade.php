<!-- start  footer-->
<footer data-aos="fade-in" class="sm-center">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-8 footer-logo-grid white-text">
                <div class="footer-logo">
                    <a href="{{ route('web.home') }}">
                        <img src="{{asset('storage/' . $footer_logo) }}" alt="logo">
                    </a>
                    
                        {!! $footer_about_us !!}


                    <div class="news-letter">
                        <form action="{{ route('web.subscribe') }}" method="POST" class="newsletter-form request_form">
                            @csrf
                            <div class="form-group">
                                <input type="email" id="email" placeholder="{{__('web.subscribe to newletter')}}" name="email-subscribe" class="form-control"
                                    required>
                                <button type="button" class="submit_request_form_button"><i class="fa fa-paper-plane"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="socialmedia auto-icon xs-center text-cap">

                        <ul class="accordion">
                            <li class="twitter-icon">
                                <div class="social-links">
                                    <a href="{{ $twitter }}" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </div>
                                <div class="paragraph">
                                    {{__('web.twitter')}}
                                </div>
                            </li>
                            <li class="facebook-icon">
                                <div class="social-links">
                                    <a href="{{ $facebook }}" target="_blank">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                </div>
                                <div class="paragraph">
                                    {{__('web.facebook')}}
                                </div>
                            </li>

                            <li class="insta-icon">
                                <div class="social-links">
                                    <a href="{{ $insta }}" target="_blank">
                                        <i class="fa fa-camera-retro"></i>
                                    </a>
                                </div>
                                <div class="paragraph">
                                    {{__('web.instagram')}}
                                </div>
                            </li>

                            <li class="snapchat-icon">
                                <div class="social-links">
                                    <a href="{{ $snapchat }}" target="_blank">
                                        <i class="fab fa-snapchat-ghost"></i>
                                    </a>
                                </div>
                                <div class="paragraph">
                                    {{__('web.snapchat')}}
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 footer-img-grid text-center">
                <div class="footer-img">
                    <object data="{{asset('web/images/main/hearts.svg')}}">
                        <img src="{{asset('web/images/main//hearts.png')}}" alt="hearts">
                    </object>
                </div>
            </div>


            <div class="col-lg-2 col-md-4 footer-lists-grid">
                <button class="simple-btn top-btn auto-icon"><i class="fa fa-long-arrow-alt-up"></i> <span>{{__('web.To the Top')}}</span></button>
                <div class="footer-list cap-text">
                    <ul class="list-unstyled">
                        @if(!auth()->check())
                            <li><a href="{{ route('admin.login' , 'association') }}">{{__('web.Join As association')}}</a></li>
                        @endif

                        <li><a href="{{route('web.pages', 'help_center')}}">{{__('web.Help Center')}} </a></li>
                        <li><a href="{{route('web.pages', 'privacy_policy')}}">{{__('web.Privacy Policy')}}</a></li>
                        <li><a href="{{route('web.pages', 'terms_and_conditions')}}">{{__('web.Terms & Conditions')}} </a></li>
                        <li><a href="{{ route('web.faq') }}">{{__('web.FAQ')}}</a></li>
                        <li><a href="{{ route('web.pages', 'technical_support') }}">{{ __('web.technical_support') }}</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</footer>
<!--end footer-->