@php
    $carts = getCart('limit');
@endphp
@foreach ($carts as $cart)
    <li>
        <a href="{{route('web.projects.show', $cart->project->slug)}}">
            <img src="{{asset('storage/'.$cart->project->image)}}" alt="{{$cart->project->name }}">
            <h3>{{$cart->project->name}}</h3>
            {{$cart->cost}} {{currencySymbol($cart->currency_id)}}
        </a>
    </li>
@endforeach