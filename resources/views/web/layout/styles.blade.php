<!-- Style sheet
================ -->

<link rel="stylesheet" href="{{asset('web/css/styles.css')}}" type="text/css" />

@if(\App::getLocale() == 'ar')

    <!--for arabic-->
    <link rel="stylesheet" href="{{asset('web/css/ar.css')}}" type="text/css" />

@else
    <!--for english-->
    <link rel="stylesheet" href="{{asset('web/css/en.css')}}" type="text/css" />

@endif
<!--end-->

<link rel="stylesheet" href="{{asset('web/css/responsive.css')}}" type="text/css" />
<script type="text/javascript" src="{{asset('web/js/jquery-3.4.1.min.js')}}"></script>

@stack('css')