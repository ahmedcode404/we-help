@extends('web.layout.app')

@section('title', $amdassador->campaign_name )
@section('description', $amdassador->campaign_name )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')


    <!-- BEDIN: header -->

    @include('web.campaigns.header')

    <!-- END: header -->

    <!-- BEGIN: content -->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{ $amdassador->campaign_name }}</h2>
        </div> --}}

            <!-- start campain-pg
            ================ -->
            <section class="campain-sec margin-div" data-aos="fade-in">
                <div class="container">
                    <!--start campain-title-->
                    <div class="campain-title">
                        <div class="row">
                            <div class="col-lg-8 col-md-7 campain-title-grid xs-center">
                                <div class="section-title">
                                    <div class="side-img"><img src="{{$amdassador->project->charity ? url('storage/' . $amdassador->project->charity->logo) : url('storage/user/default.png') }}" alt="icon"></div>
                                    <h3 class="text-cap">{{$amdassador->project->charity ? $amdassador->project->charity->name : __('no_charity')}}</h3>
                                    <div class="stars auto-icon">
                                        @for($i = 0; $i < 5; $i++)
                                            <i class="fa fa-star {{ $i < $amdassador->project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                        @endfor
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-5 col-sm-5 text-right-dir xs-center campain-goal-grid">
                                <div class="campain-goal text-cap first_color text-center sm-raduis">{{ $amdassador->campaign->name }}
                                </div>
                            </div>

                            @php

                                $supports = $amdassador->project->total_supports_for_project;

                                $cost_o = generalExchange($amdassador->project->get_total, $amdassador->project->currency->symbol, $session_currency);

                                if(auth()->check()){

                                    $donor_total_supports = auth()->user()->donor_total_supports;
                                }
                                else{
                                    $donor_total_supports = 0;
                                }

                                $remaining_amount =  $cost_o - $supports;

                                if($supports > 0){

                                    $progress = round($supports / $cost_o * 100, 2);
                                }
                                else{

                                    $progress = 0;
                                } 
                                
                                if($progress > 100){

                                    $progress = 100;
                                }

                                if($progress < 0){

                                    $progress = 0;
                                }

                            @endphp                          

                            <div class="col-lg-8 col-md-8 col-sm-7  progress-grid campain-progress">
                                <div class="progress-div">
                                    <div class="full-progress big-raduis">
                                        <span class="progress-ratio">{{$progress}}%
                                            <span class="arrow-triangle"></span>
                                        </span>
                                        <div class="progress-bar-ratio big-raduis" data-progress="{{ $progress }}"></div>
                                    </div>

                                </div>
                                <div class="progress-min-max">
                                    @php
                                        $total_donates = getTotalSupportsForProject($amdassador->project);
                                        $total_cost = exchange($amdassador->project->get_total, $amdassador->project->currency_id, session('currency'));
                                        $remaining_donates = $total_cost - $total_donates;
                                    @endphp
                                    <span>{{$total_donates}} {{currencySymbol(session('currency'))}}</span>
                                    <span>{{$remaining_donates}} {{currencySymbol(session('currency'))}}</span>     
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end campain-title-->

            </section>
            <!--end campain-sec-->

        <!-- start campain-slider
         ================ -->
        <section class="campain-slider margin-bottom-div">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!--start slider-->
                        <div id="campain-owl" class="owl-carousel owl-theme center-dots main-campain-owl dark-dots"
                            data-aos="fade-in">


                            <!--start item-->
                            @foreach($amdassador->project->images as $image)
                                <div class="item">
                                    <div class="full-width-img main-campain-item"
                                        style="background-image:url({{ url('storage/' . $image->path) }})">
                                    </div>
                                </div>
                            @endforeach
                            <!--end item-->


                        </div>
                        <!--end slider-->

                        <!--start campain-description-->
                        <div class="campain-description md-raduis" data-aos="fade-in">
                            {!! $amdassador->project->desc !!}
                        </div>
                        <!--end campain-description-->
                        
                        <!--start share-social-->
                        <div class="share-social text-right-dir" data-href="{{ Request::url() }}" data-aos="zoom-in">
                            <button data-network="facebook"
                                class="st-custom-button share-soc tooltip-link fab fa-facebook-f"
                                title="facebook"></button>

                            <button data-network="twitter"
                                class="st-custom-button share-soc tooltip-link fab fa-twitter" title="twitter"></button>

                            <button data-network="snapchat"
                                class="st-custom-button share-soc tooltip-link fab fa-snapchat-ghost"
                                title="snapchat"></button>

                            <button data-network="whatsapp"
                                class="st-custom-button share-soc tooltip-link fab fa-whatsapp"
                                title="whatsapp"></button>


                            <button data-network="sharethis"
                                class="st-custom-button share-soc tooltip-link fa fa-share-alt" title="share"></button>
                        </div>
                        <!--end share-social-->


                        <!--start comments-->
                        <div class="comments" data-aos="fade-in">
                            <h3 class="second_color text-cap has-side">
                                @if(count($amdassador->project->getProjectRatings()) > 0)
                                    {{ __('web.comments') }} : 
                                @endif

                                @auth
                                    @if($amdassador->project->charity_id == null)
                                        <button
                                            class="simple-btn text-cap second_color no-charity-project">{{__('web.rate_us')}} <i class="fa fa-star"></i>
                                        </button>                                        
                                    @else 
                                    <button class="simple-btn text-cap second_color" data-target="#rate-modal"
                                            data-toggle="modal">{{ __('web.rate_us') }} <i class="fa fa-star"></i>
                                    </button>
                                    @endif
                                @endauth

                            </h3>
                            
                            @if(count($amdassador->project->getProjectRatings()) > 0)
                                <div class="inner-comments">
                                    <ul class="list-unstyled">
                                    
                                        @if($amdassador->project->getProjectRatings())
                                            @foreach($amdassador->project->getProjectRatings() as $comment)
                                                
                                                <li>
                                                    <span class="comment-time">{{$comment->date($comment->user_id, $comment->project_id)}}</span>
                                                    <span class="img"><img src="{{url('storage/' . $comment->user->image )}}" alt="avatar"></span>
                                                    <h4 class="second_color text-cap">{{ $comment->user->name }}</h4>
                                                    <div class="stars auto-icon">
                                                        
                                                        @for($i = 0; $i < 5; $i++)
                                                            <i class="fa fa-star {{ $i < $comment->avg_grade ? 'yellow-star' : ''  }}"></i>
                                                        @endfor

                                                    </div>
                                                    <p class="first_color">
                                                        {{ $comment->getComment($comment->user_id, $comment->project_id) ? $comment->getComment($comment->user_id, $comment->project_id)->comment : ''  }}
                                                    </p>
                                                </li>

                                            @endforeach
                                        @else
                                            <p class="first_color">
                                                {{__('web.no_comments')}}
                                            </p>
                                        @endif

                                    </ul>
                                </div>
                                <div class="text-right-dir second_color">
                                    <span class="loadmore">{{ __('web.show_all') }}</span><i class="fa fa-caret-down"></i>
                                </div>
                            @endif
                        </div>
                        <!--end comments-->

                    </div>
                </div>
            </div>
        </section>
        <!--end campain-slider-->



        <!-- start campain-icons
         ================ -->
        <section class="about-sec-4 campain-icons margin-div">
            <div class="container">
                <div class="row justify-content-center">
                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{ url('web/images/campain/icons/1.png') }}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{ __('web.Target') }}</h3>
                                <p class="first_color" data-count="{{exchange($amdassador->project->get_total, $amdassador->project->currency_id, session('currency'))}}">
                                    <span> {{exchange($amdassador->project->get_total, $amdassador->project->currency_id, session('currency'))}} </span> {{currencySymbol(session('currency'))}}
                                </p>                               
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->

                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{ url('web/images/campain/icons/2.png') }}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{ __('web.beneficiaries') }}</h3>
                                <p class="first_color" data-count="{{ $amdassador->project->benef_num }}">
                                    <span> {{ $amdassador->project->benef_num }}</span> {{ __('web.beneficiary') }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->



                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{ url('web/images/campain/icons/3.png') }}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{ __('web.Donated Amount') }}</h3>
                                <p class="first_color" data-count="{{ $supports }}">
                                    <span> {{getTotalSupportsForProject($amdassador->project)}}</span> {{currencySymbol(session('currency'))}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->

                    @php

                    $cost = exchange($amdassador->project->get_total, $amdassador->project->currency_id, session('currency'));
                    $donated = getTotalSupportsForProject($amdassador->project);

                    $reamining = $cost - $donated;

                    @endphp  

                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{ url('web/images/campain/icons/4.png') }}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{ __('web.Remaining Amount') }}</h3>
                                <p class="first_color" data-count="{{ $reamining }}">
                              
                                    <span>{{$reamining}}</span> {{currencySymbol(session('currency'))}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->


                </div>
            </div>
        </section>
        <!--end campain-icons-->

        <!-- start donate-pg
         ================ -->
        <section class="faq-page margin-div campain-cart" data-aos="fade-in">
            <div class="container">
                <form action="#" class="donate-form">

                    
                    <div class="row" id="donate-cost-error-{{$amdassador->project->id}}">
                        
                        <input type="hidden" name="project_id" class="project_id" id="{{ $amdassador->project->id }}" value="{{ $amdassador->project->id }}">
                        
                        <div class="col-lg-9 col-md-8">

                            <div class="donate-div sm-center">

                                <div class="donate-quantity sm-center">
                                    <div class="form-group number-input-div">
                                        <span class="minus-num  numb-control">-</span>
                                        <input type="number" step=".01"
                                            class="form-control no_apperance_number number-input sm-raduis-input"
                                            name="donate_price" minlength="2" maxlength="7" value="{{ $cost_min }}" pattern="\d{5}" id="cost-{{$amdassador->project->id}}"
                                            required>
                                        <span class="plus-num numb-control">+</span>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control select-input currency_id sm-raduis-input" name="currency"
                                            id="currency-{{$amdassador->project->id}}"
                                            required>
                                            @foreach($currencies as $currency)
                                                <option value="{{ $currency->id }}" {{ $currency->id == session('currency') ? 'selected' : '' }}>{{ $currency->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <span class="custom-error error"></span>

                                <input type="hidden" id="select-type-{{$amdassador->project->id}}" value="radio">

                                <div class="multi-checkboxes">
                                    <div class="row ">

                                        <div class="col-xl-6 col-sm-6 md-center">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <input type="radio" class="hidden-input type-donate" checked name="donate_time"
                                                        value="once" id="check_1">
                                                    <label for="check_1" class="dark-text"> <span>{{__('web.once')}}
                                                        </span></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-6 col-sm-6 md-center">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <input type="radio" class="hidden-input type-donate" name="donate_time"
                                                        value="month" id="check_2">
                                                    <label for="check_2" class="dark-text"><span>{{__('web.monthly_deduction')}}</span></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-xl-6 col-sm-6 md-center">
                                            <div class="form-group">
                                                <div class="custom-checkbox circle-checkbox">
                                                    <!--don`t change id for that-->
                                                    <input type="radio" class="hidden-input type-donate" name="donate_time"
                                                        value="gift" id="check_3">
                                                    <label for="check_3" class="dark-text"><span>{{ __('web.Donate as a Gift') }}</span></label>
                                                </div>
                                            </div>
                                        </div>

                                    
                                    </div>
                                </div>

                            </div>
                        </div>

                        @php
                            $cost = exchange($amdassador->project->get_total, $amdassador->project->currency_id, session('currency'));
                        @endphp

                        <div class="col-lg-3 col-md-4">
                            <div class="big-btns sm-center text-right-dir">
                                @if(auth()->check() && auth()->user()->blocked)
                                    <button type="submit" class="tooltip-link big-btn first_color user-bocked"
                                                data-tippy-placement="top" title="{{__('web.donate_now')}}"><img
                                                    src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{__('web.donate_now')}}</button>
                                @else
                                    @if($amdassador->project->charity_id == null || $amdassador->project->active == 0)
                                        <button type="submit" class="tooltip-link big-btn first_color no-charity-project"
                                            data-tippy-placement="top" title="{{__('web.donate_now')}}"><img
                                                src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{__('web.donate_now')}}</button>                                    
                                    @else  
                                        @if(auth()->guard('charity')->user())
                                            <button type="submit" class="tooltip-link big-btn first_color auth-charity"
                                                data-tippy-placement="top" title="Donate Now"><img
                                                    src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{ __('web.donate_now') }}</button>                                                                     
                                        @else  
                                            <button type="submit" data-id="{{$amdassador->project->id}}" data-url="{{ route('web.price.donate') }}" class="tooltip-link big-btn {{ getTotalSupportsForProject($amdassador->project) == $cost ? 'complate-cost' : 'donate-prog-btn' }} first_color"
                                                data-tippy-placement="top" title="Donate Now"><img
                                                    src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{ __('web.donate_now') }}</button>
                                        @endif
                                    @endif
                                @endif                                                    

                                @if($amdassador->project->charity_id == null || $amdassador->project->active == 0)
                                        <button type="button" class="simple-btn tooltip-link big-btn first_color no-charity-project" data-method="POST"
                                            data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                            src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img">{{__('web.add_to_cart')}}</button>                                 
                                    @else
                                        <button type="button" class="simple-btn tooltip-link big-btn first_color add-to-cart" data-project_id="{{$amdassador->project->id}}" data-url="{{ route('web.add-to-cart') }}" data-method="POST"
                                            data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img">{{__('web.add_to_cart')}}</button>
                                    @endif

                                {{-- @if($amdassador->project->charity_id == null)
                                    <button type="button" class="simple-btn tooltip-link big-btn first_color no-charity-project" data-method="POST"
                                        data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                        src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img">{{__('web.add_to_cart')}}</button>                                 
                                @else
                                    <button type="button" class="simple-btn tooltip-link big-btn first_color add-to-cart2"
                                        data-tippy-placement="top" title="add to cart"><img
                                            src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img">{{ __('web.add_to_cart') }}</button>
                                @endif --}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!--end donate-->

    </div>


    <!-- END: content -->


    <!--start rate pop-->
    <div class="modal" id="rate-modal" tabindex="-1" role="dialog" aria-labelledby="rate-mod" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">

                <div class="modal-body text-center">
                
                    <div class="modal-header col-12">
                        <h5 class="modal-title white-text text-cap" id="rate-mod">{{ __('web.rate_project') }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="rate-sec">
                    
                        <form action="{{route('web.projects.rate', $amdassador->project->slug)}}" method="POST" class="rate-form">
                            @csrf
                            <!--start slider-->
                            <div id="rate-owl"
                                class="owl-carousel owl-theme center-dots rate-owl square-dots dark-dots">

                               
                                    @if(count($comments) > 0)
                                        
                                        @foreach($rating_criteriars as $key=>$rating_criteriar)

                                            <!--start item-->
                                            <div class="item">
                                                <div class="rate-div">
                                                    <div class="inner-rate-div">
                                                        <h3 class="second_color text-cap">{{$rating_criteriar->name}}:</h3>
                                                        <div class="inner-rate sm-raduis">
                                                            <div class="static-stars text-center">
                                                                <input type="hidden" name="item[]" value="{{ $amdassador->project->ratings[$key]->id }}" id="item-{{$rating_criteriar->id}}">
                                                                <input class="star" value="5" id="rate{{$rating_criteriar->id}}-1" {{ updateRatingUser($amdassador->project->id  , $amdassador->project->ratings[$key]->id , 'grade') == 5 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-1"></label>
                                                                <input class="star" value="4" id="rate{{$rating_criteriar->id}}-2" {{ updateRatingUser($amdassador->project->id  , $amdassador->project->ratings[$key]->id , 'grade') == 4 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-2"></label>
                                                                <input class="star" value="3" id="rate{{$rating_criteriar->id}}-3" {{ updateRatingUser($amdassador->project->id  , $amdassador->project->ratings[$key]->id , 'grade') == 3 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-3"></label>
                                                                <input class="star" value="2" id="rate{{$rating_criteriar->id}}-4" {{ updateRatingUser($amdassador->project->id  , $amdassador->project->ratings[$key]->id , 'grade') == 2 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-4"></label>
                                                                <input class="star" value="1" id="rate{{$rating_criteriar->id}}-5" {{ updateRatingUser($amdassador->project->id  , $amdassador->project->ratings[$key]->id , 'grade') == 1 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-5"></label>

                                                            </div>
                                                        </div>
                                                        <button type="button"
                                                            class="custom-btn white-btn sm-raduis next-owl"><span>{{__('web.next')}}</span></button>
                                                        <span class="done-rate"><i class="fa fa-check"></i></span>

                                                    </div>
                                                </div>
                                            </div>
                                            <!--end item-->
                                            
                                        @endforeach

                                        <!--start item-->
                                        <div class="item">
                                            <div class="rate-div">
                                                <div class="inner-rate-div">
                                                    <h3 class="second_color text-cap">{{__('web.rate_project_text')}}:</h3>
                                                    <div class="form-group">
                                                        <textarea placeholder="Leave your comment" class="form-control"
                                                            name="comment" required>{{ updateRatingUser($amdassador->project->id , null , 'comment') }}</textarea>
                                                    </div>
                                                    <button class="custom-btn white-btn sm-raduis"><span>{{__('web.submit')}}</span></button>
                                                    <span class="done-rate last-done"><i class="fa fa-check"></i></span>

                                                </div>
                                            </div>
                                        </div>
                                        <!--end item-->
                                    @else

                                        @foreach($rating_criteriars as $rating_criteriar)
            
                                            <!--start item-->
                                            <div class="item">
                                                <div class="rate-div">
                                                    <div class="inner-rate-div">
                                                        <h3 class="second_color text-cap">{{$rating_criteriar->name}}:</h3>
                                                        <div class="inner-rate sm-raduis">
                                                            <div class="static-stars text-center">
        
                                                                <input class="star" value="5" id="rate{{$rating_criteriar->id}}-1" type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-1"></label>
                                                                <input class="star" value="4" id="rate{{$rating_criteriar->id}}-2" type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-2"></label>
                                                                <input class="star" value="3" id="rate{{$rating_criteriar->id}}-3" type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-3"></label>
                                                                <input class="star" value="2" id="rate{{$rating_criteriar->id}}-4" type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-4"></label>
                                                                <input class="star" value="1" id="rate{{$rating_criteriar->id}}-5" type="radio" name="{{$rating_criteriar->id}}">
                                                                <label class="star" for="rate{{$rating_criteriar->id}}-5"></label>
        
                                                            </div>
                                                        </div>
                                                        <button type="button"
                                                            class="custom-btn white-btn sm-raduis next-owl"><span>{{__('web.next')}}</span></button>
                                                        <span class="done-rate"><i class="fa fa-check"></i></span>
        
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end item-->
        
                                        @endforeach
        
                                            <!--start item-->
                                            <div class="item">
                                            <div class="rate-div">
                                                <div class="inner-rate-div">
                                                    <h3 class="second_color text-cap">{{__('web.rate_project_text')}}:</h3>
                                                    <div class="form-group">
                                                        <textarea placeholder="Leave your comment" class="form-control"
                                                            name="comment" required></textarea>
                                                    </div>
                                                    <button class="custom-btn white-btn sm-raduis"><span>{{__('web.submit')}}</span></button>
                                                    <span class="done-rate last-done"><i class="fa fa-check"></i></span>
        
                                                </div>
                                            </div>
                                            </div>
                                            <!--end item-->

                                    @endif


                            </div>
                            <!--end slider-->
                        </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end rate pop-->

    <!--start select-modals-->

 <!--start select-modals-->
 
    <!--once modal-->
    @include('web.donate_popups.once')

    <!--gift modal-->
    @include('web.donate_popups.gift')

    <!--month modal-->
    @include('web.donate_popups.monthly')

    <!--ambassador modal-->
    @include('web.donate_popups.ambassador')
  <!--end select-modal-->



@endsection

@push('js')

    @if(\App::getLocale() == 'ar')

    <!--for arabic-only-->
    <script type="text/javascript" src="{{ asset('web/js/select-ar.js') }}"></script> -->
    <script type="text/javascript" src="{{ asset('web/js/messages_ar.min.js') }}"></script>

    @endif
    <!--for this page only-->
    <script type="text/javascript" src="{{ asset('web/js/anime.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/custom-pages.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{asset('web/backend/cart.js')}}"></script>
    <script type="text/javascript" src="{{ asset('web/js/campain.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/forms/donate.js') }}"></script>

    <!-- validation laravel -->
    <script src="{{ url('custom/custom-validate.js') }}"></script>    

@endpush