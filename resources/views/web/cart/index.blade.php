
@extends('web.layout.app')

@section('title', __('web.cart'))
@section('description', __('web.cart'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

    <!-- start pages-header
    ================ -->
    <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{__('web.cart')}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{__('web.cart')}}</h2>
        </div> --}}

        <!-- start main-projects-sec
        ================ -->
        <section class="main-projects-sec margin-div project-pg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title first_color has-side" data-aos="fade-in">
                            <div class="side-img"><img src="{{asset('web/images/icons/menu/shopping-cart.png')}}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{__('web.cart')}} </h3>
                            <h4 class="text-upper bold-text">{{__('web.cart')}}</h4>
                            <a href="{{route('web.get-category-projects', 'all')}}" class="first_color decorate_link">{{__('web.back_to_projects')}}</a>

                        </div>


                        <div class="margin-div cart-pg">

                            <form action="" class="cart-form donate-form">

                                <table class="cart-table table text-center" style="width:100%">
                                    <thead>
                                        <tr class="text-cap">
                                            <th>{{__('web.projects')}}</th>
                                            <th>{{__('web.donation_amount')}}</th>
                                            <th>{{__('web.total')}}</th>
                                            <th><span class="cart_items">{{getCart('count')}}</span> {{__('web.items')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        @foreach ($data['carts'] as $cart)
                                            <tr class="remove-item" id="donate-cost-error-{{ $cart->id }}">
                                                <td>
                                                    <div class="cart-data text-left-dir">
                                                        <a href="{{route('web.projects.show', $cart->project->slug)}}" target="_blank">
                                                            <div class="cart-img sm-raduis">
                                                                <img src="{{asset('storage/'.$cart->project->image)}}" alt="{{$cart->project->name}}">
                                                            </div>
                                                            <h3 class="text-cap project-title">{{$cart->project->name}}</h3>
                                                            <div class="stars auto-icon">
                                                                <i class="fa fa-star @if($cart->project->getRatings() >= 1) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($cart->project->getRatings() >= 2) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($cart->project->getRatings() >= 3) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($cart->project->getRatings() >= 4) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($cart->project->getRatings() >= 5) yellow-star @endif"></i>
                                                            </div>
                                                            <p class="first_color">
                                                                {{$cart->project->desc}}
                                                            </p>
                                                        </a>
                                                    </div>
                                            
                                            
                                                    <button class="simple-btn remove-btn tooltip-link text-center"
                                                        data-action="{{route('web.cart.remove-item', $cart->id)}}" data-method="POST"
                                                        data-model="cart"
                                                        title="{{__('web.delete')}}">x</button>
                                                </td>

                                                <td>
                                                    @php
                                                        if(auth()->check()){

                                                            $total_donor_supports = auth()->user()->donor_total_supports;
                                                        }
                                                        else{

                                                            $total_donor_supports = 0;
                                                        }
                                                    @endphp

                                                    {{-- <span style="color: #3dbecb;">{{__('web.cost_min')}}: {{$cost_min}} {{currencySymbol(session('currency'))}}</span> --}}
                                                    <input type="hidden" name="project_id" class="project_id" value="{{ $cart->project_id }}">
                                                    <div class="cart-quantity">
                                                        <div class="form-group number-input-div">
                                                            <span class="minus-num  numb-control" data-project_id="{{$cart->project->id}}">-</span>
                                                            <input type="number" step=".01"
                                                                class="form-control no_apperance_number number-input"
                                                                data-id="{{ $cart->id }}"
                                                                name="num_1" minlength="2" maxlength="7" value="{{$cart->cost}}"
                                                                pattern="\d{5}" id="price-{{$cart->project->id}}" required>
                                                            <span class="plus-num numb-control" data-project_id="{{$cart->project->id}}">+</span>
                                                        </div>


                                                        <div class="form-group">
                                                            <select class="form-control select-input currency_id" name="currency_id">
                                                                @foreach($currencies as $currency)
                                                                    <option value="{{ $currency->id }}" {{ $currency->id == $cart->currency_id ? 'selected' : '' }}>{{ $currency->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                        
                                                        <div class="form-group">
                                                            <select class="form-control select-input2 pop-select" name="pop-select">
                                                                <option selected value="once">{{__('web.once')}}</option>
                                                                <option value="month">{{__('web.monthly_deduction')}}</option>
                                                                <option value="gift">{{__('web.gift')}}</option>
                                                                <option value="ambassador">{{__('web.ambassador')}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <span class="custom-error error"></span>
                                                </td>

                                                <td>
                                                    <div class="cart-total">
                                                        <div class="third_color"><span id="cart_cost{{ $cart->id }}" >{{$cart->cost}}</span> {{currencySymbol($cart->currency_id)}}</div>
                                                    </div>

                                                    @if(auth()->check() && auth()->user()->blocked)
                                                        <button type="button" class="custom-btn cart-btn sm-raduis white-btn user-bocked"><span>{{ __('web.Donate') }}</span></button>
                                                    @else
                                                        @if($cart->project->charity_id == null || $cart->project->active == 0)
                                                            <button type="button" class="custom-btn cart-btn sm-raduis white-btn no-charity-project"><span>{{__("web.checkout")}}</span></button>
                                                        @else
                                                            <button 
                                                            type="button" 
                                                            data-id="{{ $cart->id }}" data-puid="{{ $cart->id }}" 
                                                            @if($total_donor_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                                data-url="{{ route('web.price.donate') }}"
                                                            @endif
                                                            class="custom-btn cart-btn sm-raduis white-btn donate-prog-btn
                                                            @if($total_donor_supports >= 10000) user_donation_full @endif" 
                                                            ><span>{{__("web.checkout")}}</span></button>
                                                        @endif
                                                    @endif
                                                </td>

                                                <td>
                                                    <div class="cart-actions">
                                                        <button class="simple-btn remove-btn tooltip-link auto-icon remove-cart-item"
                                                            data-action="{{route('web.cart.remove-item', $cart->id)}}" data-method="POST"
                                                            data-model="cart" title="{{__('web.delete')}}"><i class="fa fa-times"></i></button>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        

                                    </tbody>
                                </table>

                                <div class="cart-full-total">
                                    <div>
                                        <span class="text-left-dir">
                                            {{__('web.total')}}:
                                        </span>
                                        <span class="text-right-dir">
                                            <div class="second_color"><span id="cart_total">{{getCartTotalCost($data['carts'])}}</span> {{currencySymbol(session('currency'))}}</div>
                                        </span>
                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end main-projects-sec-->

    </div>




<!--start select-modals-->
    <!--once modal-->
    @include('web.donate_popups.once')

    <!--gift modal-->
    @include('web.donate_popups.gift')

    <!--month modal-->
    @include('web.donate_popups.monthly')

    <!--ambassador modal-->
    @include('web.donate_popups.ambassador')
    
<!--end select-modals-->


@endsection

@push('js')

    <!-- validation laravel -->
    <script src="{{ url('custom/custom-validate.js') }}"></script>  

    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/cart.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/cart.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/update-cart-price.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/remove-item.js')}}"></script>
    <script type="text/javascript" src="{{ asset('web/forms/donate-cart.js') }}"></script>

@endpush