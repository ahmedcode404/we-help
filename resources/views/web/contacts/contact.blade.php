@extends('web.layout.app')

@section('title', __('web.contact_us') )
@section('description', __('web.contact_us') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')


<!-- BEGIN: header -->
@include('web.contacts.header')
<!-- END: header -->

<div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{ __('web.contact_us') }}</h2>
        </div> --}}

        <!-- start contact-pg
         ================ -->
        <section class="margin-top-div contact-pg">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/menu/6.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ __('web.contact_us') }}</h3>
                            <h4 class="text-upper bold-text">{{ __('web.contact_us') }}</h4>
                        </div>
                        <p class="section-pragraph dark-text" data-aos="fade-in">
                            {{ __('web.text_contact') }}
                        </p>
                    </div>
                    <div class="col-12">
                        <div class="map-sec sm-center" data-aos="fade-in">
                            <img src="{{ url('web/images/main/map.png') }}" width="100%" class="map-img" alt="map">
                            <img src="{{ url('web/images/main/pin.png') }}" class="marker" alt="marker">
                            <a href="https://www.google.com/maps/search/?api=1&query={{ $lat->value }},{{ $lang->value }}" class="white-text sm-raduis" target="_blank">{{ __('web.look_map') }} <i class="fa fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-6">
                        <div class="contact-div sm-raduis" data-aos="zoom-out">
                            <form action="{{ route('web.store-contact-us') }}" method="POST" class="border-form contact-form">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="{{ __('web.name') }}" required>
                                    <label></label>
                                    @if($errors->has('name'))
                                        <div class="custom-error error">{{ $errors->first('name') }}</div>
                                    @endif 
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="{{ __('web.email') }}" required>
                                    <label></label>
                                    @if($errors->has('email'))
                                        <div class="custom-error error">{{ $errors->first('email') }}</div>
                                    @endif 
                                </div>

                                <div class="form-group phone-input">
                                    <input type="tel" class="form-control" placeholder="{{ trans('web.phone') }}" name="contact_number" minlength="8" maxlength="10" id="contact_number" required>
                                    <label></label>
                                    @if($errors->has('contact_number'))
                                        <div class="custom-error error">{{ $errors->first('contact_number') }}</div>
                                    @endif 
                                </div>


                                <div class="form-group">
                                    <textarea class="form-control" name="content" placeholder="{{ __('web.contact_us') }}"
                                        required></textarea>
                                    <label></label>
                                    @if($errors->has('content'))
                                        <div class="custom-error error">{{ $errors->first('content') }}</div>
                                    @endif 
                                </div>

                                <div class="text-center btn-div">
                                    <button type="submit" class="custom-btn sm-raduis"><span>{{ __('web.send_message') }}</span></button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <input type="hidden" id="tel_url" value="{{ url('web/js/utils.js') }}">

                    <div class="col-lg-5 col-md-6">
                        <div class="contact-info" data-aos="zoom-out">
                            <h2 class="second_color bold-text text-cap">{{ __('web.contact_information') }} :</h2>
                            <a href="tel:{{ $phone->value }}" class="section-title first_color" dir="ltr">
                                <div class="side-img"><img src="{{ __('web/images/icons/contact/1.png') }}" alt="icon"></div>
                                {{ $phone->value }}
                            </a>
                            <a href="tel:{{ $phone->value }}" class="section-title first_color" dir="ltr">
                                <div class="side-img"><img src="{{ url('web/images/icons/contact/2.png') }}" alt="icon"></div>
                                {{ $phone->value }}
                            </a>

                            <a href="mailto:{{ $email->value }}" class="section-title first_color">
                                <div class="side-img"><img src="{{ url('web/images/icons/contact/3.png') }}" alt="icon"></div>
                                {{ $email->value }}
                            </a>

                            <a href="https://www.google.com/maps/search/?api=1&query={{ $lat->value }},{{ $lang->value }}" class="section-title first_color" target="_blank">
                                <div class="side-img"><img src="{{ url('web/images/icons/contact/4.png') }}" alt="icon"></div>
                                {{ $address->value }}
                            </a>
                        </div>
                    </div>
                </div>


            </div>
        </section>
        <!--end contact-pg-->

    </div>

@endsection

@push('js')

    <!--for this page only-->
    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif

    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{ url('web/js/intlTelInput.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('web/js/contact.js') }}"></script>    

@endpush