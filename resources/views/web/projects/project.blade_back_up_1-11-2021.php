@extends('web.layout.app')

@section('title', __('web.Our Projects') )
@section('description', __('web.Our Projects') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')


<!-- BEDIN: header -->

@include('web.projects.header')

<!-- END: header -->

<div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{ __('web.Our Projects') }}</h2>
        </div> --}}
<!-- start main-projects-sec
    ================ -->
    <section class="main-projects-sec margin-div project-pg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title first_color" data-aos="fade-in">
                    <div class="side-img"><img src="{{ url('web/images/icons/menu/3.png') }}" alt="icon"></div>
                    <h3 class="second_color text-cap bold-text">{{ __('web.Our Projects') }}</h3>
                    {{ __('web.Development projects with impact in ways Effective and professional tracking tools.') }}
                </div>
            </div>
        </div>

        <div class="project-tabs big-raduis" data-aos="fade-in">
            <button class="simple-btn d-md-none d-block filter-tabs-btn sm-raduis text-cap">{{ __('web.filter donation') }} <i
                    class="fa fa-filter"></i></button>
            <div class="filter-tabs">
                <button class="simple-btn auto-icon d-md-none d-inline-block close-tabs"><i
                        class="fa fa-times"></i></button>

                <ul class="nav nav-tabs justify-content-center text-cap" id="myTab" role="tablist">

                    <!-- BEGIN: foreach charity category -->
                    <li  class="nav-item col">
                        <a class="nav-link project_slug @if((isset($slug) && $slug == 'all')) active @endif" data-toggle="tab" href="#tab_all" role="tab"
                            aria-controls="tab_all" aria-selected="true"
                            data-action="{{route('web.get-category-projects', 'all')}}" data-method="GET">{{ trans('admin.all') }}</a>
                    </li>
                    @foreach($categories as $category)

                        <li class="nav-item col" data-light-color="{{ $category->sub_color }}" data-main-color="{{ $category->main_color }}">
                            <a class="nav-link project_slug @if((isset($slug) && $slug == $category->slug)) active @endif" data-toggle="tab" href="#tab_{{ $category->id }}" role="tab"
                                aria-controls="tab_{{ $category->id }}" aria-selected="{{ isset($slug) && $slug == $category->slug ? 'true' : 'false' }}"
                                data-action="{{route('web.get-category-projects', $category->slug)}}" data-method="GET">{{ $category->name }}</a>
                        </li>

                    @endforeach
                    <!-- END: foreach charity category -->

                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                
                <!-- END: foreach tab charity category -->
                @if(isset($slug) && $slug == 'all')
                    <div class="tab-pane @if(isset($slug) && $slug == 'all') show active @endif" id="tab_all" role="tabpanel" aria-labelledby="tab_all">
                        <div class="inner-tab">
                            <div class="row">
                                <!--start grid-->
                                @forelse($projects as $project)

                                    @if($project->charity && $project->charity->status == 'approved' && $project->status == 'approved')

                                        <div class="col-xl-4 col-md-6 project-grid" data-aos="fade-in">
                                            <div class="project-div md-raduis @if($project->donation_complete == 1) gray-project-div @endif">
                                                <a href="{{route('web.projects.show', $project->slug)}}" target="_blank">
                                                    <div class="project-img md-raduis">
                                                        <img src="{{ url('storage/' . $project->image) }}" alt="projects">
                                                    </div>
                                                    <div class="padding-div">
                                                        <h3 class="text-cap project-title">{{ $project->name  }}</h3>
                                                        <p class="first_color">
                                                            {{\Illuminate\Support\Str::limit( $project->desc , 50)}}
                                                        </p>
                                                    </div>
                                                </a>

                                                @php

                                                    $supports = $project->total_supports_for_project;

                                                    $cost = generalExchange($project->get_total, $project->currency->symbol, $session_currency);

                                                    if(auth()->check()){

                                                        $donor_total_supports = auth()->user()->donor_total_supports;
                                                    }
                                                    else{
                                                        $donor_total_supports = 0;
                                                    }

                                                    if($supports > 0){

                                                        $progress = round($supports / $cost * 100, 2);
                                                    }
                                                    else{

                                                        $progress = 0;
                                                    }  
                                                    
                                                    if($progress > 100){
                                                        $progress = 100;
                                                    }

                                                    if($progress < 0){
                                                        $progress = 0;
                                                    }

                                                @endphp

                                                <div class="padding-div">
                                                    <div class="row align-items-center">
                                                        <div class="col-lg-6 col-md-12 col-sm-6">
                                                            <div class="section-title">
                                                                <div class="side-img">
                                                                    @if($project->charity)
                                                                        <img src="{{asset('storage/'.$project->charity->logo)}}" alt="icon">
                                                                    @else
                                                                    <img src="{{asset('web/images/projects/logo.png')}}" alt="icon">
                                                                    @endif
                                                                </div>
                                                                <h3 class="text-cap">{{$project->charity ? $project->charity->name : __('web.not_available')}}</h3>
                                                                <div class="stars auto-icon">
                                                                    @for($i = 0; $i < 5; $i++)
                                                                        <i class="fa fa-star {{ $i < $project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                                    @endfor
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div
                                                            class="col-lg-6 col-md-12 col-sm-6 d-lg-none d-md-block projects-money-grid">
                                                            <div class="projects-money first_color text-center-dir">
                                                                <div>
                                                                    <span class="sm-raduis">{{ $project->benef_num }} {{ __('web.Beneficiaries') }}</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6  progress-grid">
                                                            <div class="total-donation d-lg-none d-md-block">{{ __('web.Total') }}:{{$progress}}
                                                            {{$session_currency}}</div>
                                                            <div class="progress-div">
                                                                <div class="full-progress big-raduis">
                                                                    <span class="progress-ratio">{{$progress}}%
                                                                        <span class="arrow-triangle"></span>
                                                                    </span>
                                                                    <div class="progress-bar-ratio big-raduis"
                                                                        data-progress="{{$progress}}"></div>
                                                                </div>

                                                            </div>
                                                            <div class="progress-min-max">
                                                                @php
                                                                    $total_donates = $supports;
                                                                    $total_cost = $cost;
                                                                    $remaining_donates = $total_cost - $total_donates;

                                                                    if($remaining_donates < 0){
                                                                        $remaining_donates = 0;
                                                                    }
                                                                @endphp
                                                                <span>{{$total_donates}} {{$session_currency}}</span>
                                                                <span>{{$remaining_donates}} {{$session_currency}}</span>
                                                        
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="projects-money first_color text-center-dir d-lg-block d-none">
                                                        <div class="row">
                                                            <div class="col-6">

                                                                {{ __('web.Beneficiaries') }}

                                                                <br>
                                                                <span class="sm-raduis">{{ $project->benef_num }} {{ __('web.Beneficiaries') }}</span>
                                                            </div>
                                                            <div class="col-6">

                                                                {{ __('web.Total donations') }}                                                        
                                                                
                                                                <br>
                                                                
                                                                <span class="sm-raduis">{{$supports}} {{$session_currency}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="project-shape"><span>
                                                        <img src="{{ url('web/images/icons/menu/3.png') }}" alt="img"></span>
                                                </div>

                                                @if($project->nation_id == $nation_id)
                                                    <div class="project-select text-center">
                                                        <div class="padding-div">

                                                            <span style="color: #3dbecb;">{{__('web.cost_min')}}: {{$cost_min}} {{$session_currency}}</span>
                                                            
                                                            <form action="" class="donate-sm-form donate-cost-error-{{ $project->id }}">
                                                            
                                                                <input type="hidden" name="project_id" class="project_id" id="{{ $project->id }}" value="{{ $project->id }}">
                                                                <input type="hidden" id="select-type-{{$project->id}}" value="select">

                                                                <div class="form-group">
                                                                    <select name="type" class="form-control select-input2 pop-select" id="type-{{$project->id}}">
                                                                        <option selected value="once">{{ __('web.once') }}</option>
                                                                        <option value="month">{{ __('web.monthly_deduction') }}</option>
                                                                        <option value="gift">{{ __('web.Gift') }}</option>
                                                                        <option value="ambassador">{{ __('web.ambassador') }}</option>
                                                                    </select>
                                                                </div>

                                                                <div class="form-group number-input-div">
                                                                    <span class="minus-num  numb-control">-</span>
                                                                    
                                                                    <input type="text" step="1"
                                                                        class="form-control no_apperance_number number-input"
                                                                        minlength="2" maxlength="5" value="{{ $cost_min }}" data-id="{{ $project->id }}" pattern="\d{5}" id="cost-{{$project->id}}">
                                                                    <span class="plus-num numb-control">+</span>
                                                                    
                                                                </div>

                                                                <div class="form-group">
                                                                    <select name="currency_id" class="form-control select-input currency_id" id="currency-{{$project->id}}">
                                                                        @foreach($currencies as $currency)
                                                                            <option value="{{ $currency->id }}" {{ $currency->id == session('currency') ? 'selected' : '' }}>{{ $currency->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    
                                                                </div>


                                                                <div class="btn-div">
                                                                    @if(auth()->check() && auth()->user()->blocked)
                                                                        <button type="button" class="custom-btn big-raduis white-btn user-bocked"><span>{{ __('web.Donate') }}</span></button>
                                                                    @else
                                                                        @if($project->charity_id == null || $project->active == 0)
                                                                            <button type="button" class="custom-btn big-raduis white-btn no-charity-project"><span>{{ __('web.Donate') }}</span></button>
                                                                        @else 
                                                                            @if(auth()->guard('charity')->user())
                                                                                <button type="button" class="custom-btn big-raduis white-btn auth-charity"><span>{{ __('web.Donate') }}</span></button>
                                                                            @else
                                                                                <button 
                                                                                type="button" 
                                                                                data-id="{{$project->id}}" 
                                                                                @if($donor_total_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                                                    data-url="{{ route('web.price.donate') }}"
                                                                                @endif
                                                                                class="{{ $supports == $cost ? 'complate-cost' : 'donate-project' }} big-raduis white-btn custom-btn
                                                                                @if($donor_total_supports >= 10000) user_donation_full @endif"" 
                                                                                ><span>{{ __('web.Donate') }}</span></button>
                                                                            @endif
                                                                        @endif
                                                                    @endif


                                                                </div>
                                                                <span class="custom-error error"> </span>
                                                            </form>

                                                        </div>

                                                    </div>
                                                @endif


                                                <div class="project-btns text-center">

                                                    @if($project->nation_id == $nation_id)
                                                        @if($project->charity_id == null)
                                                            <button class="simple-btn tooltip-link col first_color no-charity-project" data-method="POST"
                                                                title="{{ __('web.Add to') }}"><img src="{{ url('web/images/icons/menu/shopping-cart.png') }}"
                                                                    alt="img">{{ __('web.Add to') }}
                                                            </button>
                                                        @else 
                                                            <button class="simple-btn tooltip-link col first_color add-to-cart" data-project_id="{{$project->id}}" data-url="{{ route('web.add-to-cart') }}" data-method="POST" id="{{ $project->id }}"
                                                                title="{{ __('web.Add to') }}"><img src="{{ url('web/images/icons/menu/shopping-cart.png') }}"
                                                                    alt="img">{{ __('web.Add to') }}
                                                            </button>
                                                        @endif
                                                    @endif

                                                    <a href="{{ route('web.projects.show' , $project->slug) }}" class="tooltip-link col first_color"
                                                        title="{{ __('web.More Details') }}"><img src="{{ url('web/images/icons/menu/donate.png') }}"
                                                            alt="img">{{ __('web.More Details') }}</a>
                                                </div>
                                            </div>
                                        </div>

                                    @endif
                                @empty
                                    {{ trans('admin.no_data') }}        
                                @endforelse

                                <div class="col-12">
                                    <div class="custom-pagination text-center">
                                        {{ $projects->links() }}
                                    </div>
                                </div>
                            
                                <!--end grid-->

                            </div>
                        </div>
                    </div>
                @else
                    @forelse($categories as $cat)

                        <!--start tab-pane-->
                        @php
                            $projects = $cat->projects()->orderBy('donation_complete', 'ASC')->paginate(6);
                        @endphp
                        
                        @if(count($projects) > 0)
                            <div class="tab-pane @if((isset($slug) && $slug == $cat->slug) || $loop->iteration == 1) show active @endif" id="tab_{{ $cat->id }}" role="tabpanel" aria-labelledby="tab_{{ $cat->id }}">
                                <div class="inner-tab">
                                    <div class="row">
                                        <!--start grid-->
                                        @foreach($projects as $project)

                                            @if($project->charity && $project->charity->status == 'approved' && $project->status == 'approved')

                                                <div class="col-xl-4 col-md-6 project-grid" data-aos="fade-in">
                                                    <div class="project-div md-raduis @if($project->donation_complete == 1) gray-project-div @endif">
                                                        <a href="{{route('web.projects.show', $project->slug)}}" target="_blank">
                                                            <div class="project-img md-raduis">
                                                                <img src="{{ url('storage/' . $project->image) }}" alt="projects">
                                                            </div>
                                                            <div class="padding-div">
                                                                <h3 class="text-cap project-title">{{ $project->name  }}</h3>
                                                                <p class="first_color">
                                                                    {{\Illuminate\Support\Str::limit( $project->desc , 50)}}
                                                                </p>
                                                            </div>
                                                        </a>

                                                        @php

                                                            $supports = $project->total_supports_for_project;

                                                            $cost = generalExchange($project->get_total, $project->currency->symbol, $session_currency);

                                                            if(auth()->check()){

                                                                $donor_total_supports = auth()->user()->donor_total_supports;
                                                            }
                                                            else{
                                                                $donor_total_supports = 0;
                                                            }

                                                            if($supports > 0){

                                                                $progress = round($supports / $cost * 100, 2);
                                                            }
                                                            else{

                                                                $progress = 0;
                                                            }  
                                                            
                                                            if($progress > 100){
                                                                $progress = 100;
                                                            }

                                                            if($progress < 0){
                                                                $progress = 0;
                                                            }

                                                        @endphp

                                                        <div class="padding-div">
                                                            <div class="row align-items-center">
                                                                <div class="col-lg-6 col-md-12 col-sm-6">
                                                                    <div class="section-title">
                                                                        <div class="side-img">
                                                                            @if($project->charity)
                                                                                <img src="{{asset('storage/'.$project->charity->logo)}}" alt="icon">
                                                                            @else
                                                                            <img src="{{asset('web/images/projects/logo.png')}}" alt="icon">
                                                                            @endif
                                                                        </div>
                                                                        <h3 class="text-cap">{{$project->charity ? $project->charity->name : __('web.not_available')}}</h3>
                                                                        <div class="stars auto-icon">
                                                                            @for($i = 0; $i < 5; $i++)
                                                                                <i class="fa fa-star {{ $i < $project->getRatings() ? 'yellow-star' : ''  }}"></i>
                                                                            @endfor
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="col-lg-6 col-md-12 col-sm-6 d-lg-none d-md-block projects-money-grid">
                                                                    <div class="projects-money first_color text-center-dir">
                                                                        <div>
                                                                            <span class="sm-raduis">{{ $project->benef_num }} {{ __('web.Beneficiaries') }}</span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-6  progress-grid">
                                                                    <div class="total-donation d-lg-none d-md-block">{{ __('web.Total') }}:{{$progress}}
                                                                    {{$session_currency}}</div>
                                                                    <div class="progress-div">
                                                                        <div class="full-progress big-raduis">
                                                                            <span class="progress-ratio">{{$progress}}%
                                                                                <span class="arrow-triangle"></span>
                                                                            </span>
                                                                            <div class="progress-bar-ratio big-raduis"
                                                                                data-progress="{{$progress}}"></div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="progress-min-max">
                                                                        @php
                                                                            $total_donates = $supports;
                                                                            $total_cost = $cost;
                                                                            $remaining_donates = $total_cost - $total_donates;

                                                                            if($remaining_donates < 0){
                                                                                $remaining_donates = 0;
                                                                            }
                                                                        @endphp
                                                                        <span>{{$total_donates}} {{$session_currency}}</span>
                                                                        <span>{{$remaining_donates}} {{$session_currency}}</span>
                                                                
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="projects-money first_color text-center-dir d-lg-block d-none">
                                                                <div class="row">
                                                                    <div class="col-6">

                                                                        {{ __('web.Beneficiaries') }}

                                                                        <br>
                                                                        <span class="sm-raduis">{{ $project->benef_num }} {{ __('web.Beneficiaries') }}</span>
                                                                    </div>
                                                                    <div class="col-6">

                                                                        {{ __('web.Total donations') }}                                                        
                                                                        
                                                                        <br>
                                                                        
                                                                        <span class="sm-raduis">{{$supports}} {{$session_currency}}</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="project-shape"><span>
                                                                <img src="{{ url('web/images/icons/menu/3.png') }}" alt="img"></span>
                                                        </div>

                                                        @if($project->nation_id == $nation_id)
                                                            <div class="project-select text-center">
                                                                <div class="padding-div">

                                                                    <span style="color: #3dbecb;">{{__('web.cost_min')}}: {{$cost_min}} {{$session_currency}}</span>
                                                                    
                                                                    <form action="" class="donate-sm-form donate-cost-error-{{ $project->id }}">
                                                                    
                                                                        <input type="hidden" name="project_id" class="project_id" id="{{ $project->id }}" value="{{ $project->id }}">
                                                                        <input type="hidden" id="select-type-{{$project->id}}" value="select">

                                                                        <div class="form-group">
                                                                            <select name="type" class="form-control select-input2 pop-select" id="type-{{$project->id}}">
                                                                                <option selected value="once">{{ __('web.once') }}</option>
                                                                                <option value="month">{{ __('web.monthly_deduction') }}</option>
                                                                                <option value="gift">{{ __('web.Gift') }}</option>
                                                                                <option value="ambassador">{{ __('web.ambassador') }}</option>
                                                                            </select>
                                                                        </div>

                                                                        <div class="form-group number-input-div">
                                                                            <span class="minus-num  numb-control">-</span>
                                                                            
                                                                            <input type="text" step="1"
                                                                                class="form-control no_apperance_number number-input"
                                                                                minlength="2" maxlength="5" value="{{ $cost_min }}" data-id="{{ $project->id }}" pattern="\d{5}" id="cost-{{$project->id}}">
                                                                            <span class="plus-num numb-control">+</span>
                                                                            
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <select name="currency_id" class="form-control select-input currency_id" id="currency-{{$project->id}}">
                                                                                @foreach($currencies as $currency)
                                                                                    <option value="{{ $currency->id }}" {{ $currency->id == session('currency') ? 'selected' : '' }}>{{ $currency->name }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                            
                                                                        </div>


                                                                        <div class="btn-div">
                                                                            @if(auth()->check() && auth()->user()->blocked)
                                                                                <button type="button" class="custom-btn big-raduis white-btn user-bocked"><span>{{ __('web.Donate') }}</span></button>
                                                                            @else
                                                                                @if($project->charity_id == null || $project->active == 0)
                                                                                    <button type="button" class="custom-btn big-raduis white-btn no-charity-project"><span>{{ __('web.Donate') }}</span></button>
                                                                                @else 
                                                                                    @if(auth()->guard('charity')->user())
                                                                                        <button type="button" class="custom-btn big-raduis white-btn auth-charity"><span>{{ __('web.Donate') }}</span></button>
                                                                                    @else
                                                                                        <button 
                                                                                        type="button" 
                                                                                        data-id="{{$project->id}}" 
                                                                                        @if($donor_total_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                                                            data-url="{{ route('web.price.donate') }}"
                                                                                        @endif
                                                                                        class="{{ $supports == $cost ? 'complate-cost' : 'donate-project' }} big-raduis white-btn custom-btn
                                                                                        @if($donor_total_supports >= 10000) user_donation_full @endif"" 
                                                                                        ><span>{{ __('web.Donate') }}</span></button>
                                                                                    @endif
                                                                                @endif
                                                                            @endif


                                                                        </div>
                                                                        <span class="custom-error error"> </span>
                                                                    </form>

                                                                </div>

                                                            </div>
                                                        @endif


                                                        <div class="project-btns text-center">

                                                            @if($project->nation_id == $nation_id)
                                                                @if($project->charity_id == null)
                                                                    <button class="simple-btn tooltip-link col first_color no-charity-project" data-method="POST"
                                                                        title="{{ __('web.Add to') }}"><img src="{{ url('web/images/icons/menu/shopping-cart.png') }}"
                                                                            alt="img">{{ __('web.Add to') }}
                                                                    </button>
                                                                @else 
                                                                    <button class="simple-btn tooltip-link col first_color add-to-cart" data-project_id="{{$project->id}}" data-url="{{ route('web.add-to-cart') }}" data-method="POST" id="{{ $project->id }}"
                                                                        title="{{ __('web.Add to') }}"><img src="{{ url('web/images/icons/menu/shopping-cart.png') }}"
                                                                            alt="img">{{ __('web.Add to') }}
                                                                    </button>
                                                                @endif
                                                            @endif

                                                            @if(count($projects) > 0)
                                                                <a href="{{ route('web.projects.show' , $project->slug) }}" class="tooltip-link col first_color"
                                                                    title="{{ __('web.More Details') }}"><img src="{{ url('web/images/icons/menu/donate.png') }}"
                                                                        alt="img">{{ __('web.More Details') }}
                                                                </a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>

                                            @endif

                                        @endforeach

                                        <div class="col-12">
                                            <div class="custom-pagination text-center">
                                                {{ $projects->links() }}
                                            </div>
                                        </div>
                                    
                                        <!--end grid-->

                                    </div>
                                </div>
                            </div>
                        @else
                            {{ trans('admin.no_data') }}
                        @endif
                        <!--end tab-pane-->
                    @empty
                        {{__('web.no_data')}}
                    @endforelse
                @endif
                <!-- BEGIN: foreach tab charity category -->

            </div>
        </div>
    </div>
</section>
<!--end main-projects-sec-->


<!--start select-modals-->

<!--once modal-->
@include('web.donate_popups.once')

<!--gift modal-->
@include('web.donate_popups.gift')

<!--month modal-->
@include('web.donate_popups.monthly')

<!--ambassador modal-->
@include('web.donate_popups.ambassador')
<!--end select-modal-->
</div>
@endsection

@push('js')

    <!--for this page only-->
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/cart.js')}}"></script>  
    <script type="text/javascript" src="{{asset('web/js/projects.js')}}"></script>
    
    <!-- validation laravel -->
    <script src="{{ url('custom/custom-validate.js') }}"></script>   

@endpush