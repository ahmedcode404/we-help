
@extends('web.layout.app')


@section('meta')
    {{-- Faceboon meta tages start --}}
    <meta property="og:title" content="{{$data['project']->name}}" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{asset('storage/'.$data['project']->image)}}" />
    <meta property="og:image:url"  content="{{asset('storage/'.$data['project']->image)}}" />
    <meta property="og:image:width"  content="500" />
    <meta property="og:image:height"  content="314" />
    <meta property="og:description" content="<?=strip_tags($data['project']->desc )?>" />
    <meta property="og:site_name" content="{{trans('admin.we_help')}}" />
    <meta property="og:url" content="{{route('web.projects.show',$data['project']->slug)}}" />
    {{-- Facebook meta tage end --}}

    {{-- Twitter meta tags start --}}
    <meta name="twitter:title" content="{{$data['project']->name}}">
    <meta name="twitter:description" content="<?=strip_tags($data['project']->desc )?>">
    <meta name="twitter:image" content="{{asset('storage/'.$data['project']->image)}}">
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="{{trans('admin.we_help')}}" />
    <meta name="twitter:creator" content="www.jaadara.com" />
    {{-- Twitter meta tags end --}}
@endsection


@section('title', __('web.project_details'))
@section('description', __('web.project_details'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

 <!--start select-modals-->
    <!--once modal-->
    @include('web.donate_popups.once')

    <!--gift modal-->
    @include('web.donate_popups.gift')

    <!--month modal-->
    @include('web.donate_popups.monthly')

    <!--ambassador modal-->
    @include('web.donate_popups.ambassador')
    <!--end select-modal-->

    <!--start rate pop-->
    @include('web.projects.popups.rate')
    <!--end rate pop-->


    <!-- start pages-header
        ================ -->
    <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{$data['project']->name }}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{$data['project']->name}}</h2>
        </div> --}}


        <!-- start campain-pg
         ================ -->
        <section class="campain-sec margin-div" data-aos="fade-in">
            <div class="container">
                <!--start campain-title-->
                <div class="campain-title">
                    @if($data['donations_cost'] > 0)
                        <h2 class="text-center margin-bottom-div second_color">{{ trans('web.donation_text') }}<b style="display: inline-block">{{ $data['donations_cost'] }}</b> {{ $data['currency_symbol'] }}</h2>
                    @endif
                    <div class="row">
                        <div class="col-lg-6 col-md-6 campain-title-grid xs-center">
                            <div class="section-title">
                                <div class="side-img">
                                    @if($data['project']->charity)
                                        <img src="{{asset('storage/'.$data['project']->charity->logo)}}" alt="icon">
                                    @else
                                        <img src="{{asset('web/images/projects/logo.png')}}" alt="icon">
                                    @endif
                                </div>
                                <h3 class="text-cap">{{$data['project']->charity ? $data['project']->charity->name : __('web.not_available')}}</h3>
                                <div class="stars auto-icon">
                                    <i class="fa fa-star @if($data['project']->getRatings() >= 1) yellow-star @endif"></i>
                                    <i class="fa fa-star @if($data['project']->getRatings() >= 2) yellow-star @endif"></i>
                                    <i class="fa fa-star @if($data['project']->getRatings() >= 3) yellow-star @endif"></i>
                                    <i class="fa fa-star @if($data['project']->getRatings() >= 4) yellow-star @endif"></i>
                                    <i class="fa fa-star @if($data['project']->getRatings() >= 5) yellow-star @endif"></i>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 progress-grid campain-progress">
                            <div class="progress-div">
                                <div class="full-progress big-raduis">
                                    @php

                                        $supports = $data['project']->total_supports_for_project;

                                        $cost = exchange($data['project']->get_total, $data['project']->currency_id, session('currency'));

                                        if(auth()->check()){

                                            $donor_total_supports = auth()->user()->donor_total_supports;
                                        }
                                        else{
                                            $donor_total_supports = 0;
                                        }

                                        if($supports > 0){

                                            $progress = round($supports / $cost * 100, 2);
                                        }
                                        else{

                                            $progress = 0;
                                        }

                                        if($progress > 100){
                                            $progress = 100;
                                        }

                                        if($progress < 0){
                                            $progress = 0;
                                        }

                                    @endphp
                                    <span class="progress-ratio">{{$progress}}%
                                        <span class="arrow-triangle"></span>
                                    </span>
                                    <div class="progress-bar-ratio big-raduis" data-progress="{{$progress}}"></div>
                                </div>

                            </div>
                            <div class="progress-min-max">
                                @php
                                    $total_donates = $supports;

                                    $total_cost = $cost;

                                    // dd($total_cost, $total_donates);

                                    $remaining_donates = round($total_cost - $total_donates, 2);
                                    
                                    if($remaining_donates < 0){
                                        $remaining_donates = 0;
                                    }
                                @endphp
                                <span>{{$total_donates}} {{$data['currency_symbol']}}</span>
                                <span>{{$remaining_donates}} {{$data['currency_symbol']}}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end campain-title-->

        </section>
        <!--end campain-sec-->



        <!-- start campain-slider
         ================ -->
        <section class="campain-slider margin-bottom-div">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <!--start slider-->
                        <div id="campain-owl" class="owl-carousel owl-theme center-dots main-campain-owl dark-dots"
                            data-aos="fade-in">

                            @foreach ($data['project']->images as $image)
                                <!--start item-->
                                <div class="item">
                                    <div class="full-width-img main-campain-item"
                                        style="background-image:url({{asset('storage/'.$image->path)}})">
                                    </div>
                                </div>
                                <!--end item-->    
                            @endforeach
                            
                        </div>
                        <!--end slider-->

                        <!--start campain-description-->
                        <div class="campain-description md-raduis" data-aos="fade-in">
                            {{$data['project']->desc}}
                        </div>
                        <!--end campain-description-->

                        <!--start share-social-->
                        <div class="share-social text-right-dir" data-aos="zoom-in">

                            <button data-network="facebook"
                                class="st-custom-button share-soc tooltip-link fab fa-facebook-f"
                                title="facebook"></button>

                            <button data-network="twitter"
                                class="st-custom-button share-soc tooltip-link fab fa-twitter" title="twitter"></button>

                            <button data-network="snapchat"
                                class="st-custom-button share-soc tooltip-link fab fa-snapchat-ghost"
                                title="snapchat"></button>

                            <button data-network="whatsapp"
                                class="st-custom-button share-soc tooltip-link fab fa-whatsapp"
                                title="whatsapp"></button>


                            <button data-network="sharethis"
                                class="st-custom-button share-soc tooltip-link fa fa-share-alt" title="share"></button>
                        </div>
                        <!--end share-social-->

                        <!--end comments-->
                        <!--start comments-->
                        <div class="comments" data-aos="fade-in">
                            <h3 class="second_color text-cap has-side">
                                @if(count($data['project']->getProjectRatings()) > 0)
                                    {{__('web.comments')}}:
                                @endif 

                                {{-- prevent un auth user from rating start --}}
                                    @auth
                                        @if($data['project']->charity_id == null)
                                            <button
                                                class="simple-btn text-cap second_color no-charity-project">{{__('web.rate_us')}} <i class="fa fa-star"></i>
                                            </button>                                        
                                        @else 
                                            <button
                                                class="simple-btn text-cap second_color @if(count($data['comment']) > 0) rate_exist @else rate_us @endif" 
                                                @if(count($data['comment']) == 0) data-target="#rate-modal" data-toggle="modal" @endif>{{__('web.rate_us')}} <i class="fa fa-star"></i>
                                            </button>
                                        @endif
                                    @endauth
                                {{-- prevent un auth user from rating end --}}

                            </h3>

                            @if(count($data['project']->getProjectRatings()) > 0)
                                <div class="inner-comments">
                                    <ul class="list-unstyled">

                                        @forelse ($data['project']->getProjectRatings() as $rate)

                                            <li>
                                                <span class="comment-time">{{$rate->date($rate->user_id, $rate->project_id)}}</span>
                                                <span class="img"><img src="{{$rate->user->image ? asset('storage/'.$rate->user->image) : asset('web/images/campain/avatar.png')}}" alt="avatar"></span>
                                                <h4 class="second_color text-cap">{{$rate->user->name}}</h4>
                                                <div class="stars auto-icon">
                                                    <i class="fa fa-star @if($rate->avg_grade >= 1) yellow-star @endif"></i>
                                                    <i class="fa fa-star @if($rate->avg_grade >= 2) yellow-star @endif"></i>
                                                    <i class="fa fa-star @if($rate->avg_grade >= 3) yellow-star @endif"></i>
                                                    <i class="fa fa-star @if($rate->avg_grade >= 4) yellow-star @endif"></i>
                                                    <i class="fa fa-star @if($rate->avg_grade >= 5) yellow-star @endif"></i>
                                                </div>
                                                <p class="first_color">
                                                    @if($rate->getComment($rate->user_id, $rate->project_id))
                                                        {{$rate->getComment($rate->user_id, $rate->project_id)->comment}}
                                                    @endif
                                                </p>
                                            </li>
                                        @empty
                                            <li>
                                                <span class="comment-time"></span>
                                                <span class="img"><img src="{{asset('web/images/campain/avatar.png')}}" alt="avatar"></span>
                                                <h4 class="second_color text-cap"></h4>
                                                <div class="stars auto-icon">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </div>
                                                <p class="first_color">
                                                    {{__('web.no_comments')}}
                                                </p>
                                            </li>
                                        @endforelse
                                    
                                    </ul>
                                </div>
                                <div class="text-right-dir second_color">
                                    <span class="loadmore">{{__('web.show_all')}}</span><i class="fa fa-caret-down"></i>
                                </div>
                            @endif
                        </div>
                        <!--end comments-->

                    </div>
                </div>
            </div>
        </section>
        <!--end campain-slider-->

        <!-- start campain-icons
         ================ -->
        <section class="about-sec-4 campain-icons margin-div">
            <div class="container">
                <div class="row justify-content-center">
                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{asset('web/images/campain/icons/1.png')}}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{__('web.target')}}</h3>
                                {{-- <p class="first_color" data-count="{{exchange($data['project']->get_total, $data['project']->currency_id, session('currency'))}}">
                                    <span> {{exchange($data['project']->get_total, $data['project']->currency_id, session('currency'))}}</span> {{currencySymbol(session('currency'))}}
                                </p> --}}

                                <p class="first_color" data-count="{{$cost}}">
                                    <span> {{$cost}}</span> {{$data['currency_symbol']}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->

                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{asset('web/images/campain/icons/2.png')}}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{__('web.beneficiaries')}}</h3>
                                <p class="first_color" data-count="{{$data['project']->benef_num}}">
                                    <span> {{$data['project']->benef_num}}</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->

                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{asset('web/images/campain/icons/3.png')}}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{__('web.donated_amounts')}}</h3>
                                <p class="first_color" data-count="{{$supports}}">
                                    <span> {{$supports}}</span>  {{$data['currency_symbol']}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->


                    <!--start about-icons-->
                    <div class="col-lg-3 col-md-4 col-sm-6 text-center aos-init aos-animate" data-aos="zoom-out">
                        <div class="about-icons-div">
                            <div class="img">
                                <img src="{{asset('web/images/campain/icons/4.png')}}" alt="icon">
                            </div>
                            <div class="campain-icon-description sm-raduis">
                                <h3 class="second_color text-cap">{{__('web.remaining_amount')}}</h3>
                                <p class="first_color" data-count="{{$remaining_donates}}">
                                    <span>{{$remaining_donates}}</span> {{$data['currency_symbol']}}
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end about-icons-->


                </div>
            </div>
        </section>
        <!--end campain-icons-->


        <!-- start donate-pg
         ================ -->
         @if($data['project']->nation_id == $nation_id && $data['project']->donation_complete == 0)
            <section class="faq-page margin-div campain-cart" data-aos="fade-in">
                <div class="container">

                    {{-- <h3 class="second_color sm-center">{{__('web.cost_min')}}: {{$cost_min}} {{$data['currency_symbol']}}</h3> --}}
                    
                    <form action="#" class="donate-form">

                        
                        <div class="row" id="donate-cost-error-{{$data['project']->id}}">
                            
                            <input type="hidden" name="project_id" class="project_id" id="{{ $data['project']->id }}" value="{{ $data['project']->id }}">
                            
                            <div class="col-lg-9 col-md-8">

                                <div class="donate-div sm-center">

                                    <div class="donate-quantity sm-center">
                                        <div class="form-group number-input-div">
                                            <span class="minus-num  numb-control">-</span>
                                            <input type="number" step=".01"
                                                class="form-control no_apperance_number number-input sm-raduis-input"
                                                name="donate_price" minlength="2" maxlength="7" value="{{ $cost_min }}" pattern="\d{5}" id="cost-{{$data['project']->id}}"
                                                required>
                                            <span class="plus-num numb-control">+</span>
                                        </div>

                                        <div class="form-group">
                                            <select class="form-control select-input currency_id sm-raduis-input" name="currency" id="currency-{{$data['project']->id}}"
                                                required>
                                                @foreach($currencies as $currency)
                                                    <option value="{{ $currency->id }}" {{ $currency->id == session('currency') ? 'selected' : '' }}>{{ $currency->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <span class="custom-error error"></span>

                                    <input type="hidden" id="select-type-{{$data['project']->id}}" value="radio">

                                    <div class="multi-checkboxes">
                                        <div class="row ">

                                            <div class="col-xl-6 col-sm-6 md-center">
                                                <div class="form-group">
                                                    <div class="custom-checkbox circle-checkbox">
                                                        <input type="radio" class="hidden-input type-donate" checked name="donate_time"
                                                            value="once" id="check_1">
                                                        <label for="check_1" class="dark-text"> <span>{{ __('web.Donate once') }}
                                                            </span></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xl-6 col-sm-6 md-center">
                                                <div class="form-group">
                                                    <div class="custom-checkbox circle-checkbox">
                                                        <input type="radio" class="hidden-input type-donate" name="donate_time"
                                                            value="month" id="check_2">
                                                        <label for="check_2" class="dark-text"><span> {{ __('web.monthly_deduction') }}</span></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xl-6 col-sm-6 md-center">
                                                <div class="form-group">
                                                    <div class="custom-checkbox circle-checkbox">
                                                        <!--don`t change id for that-->
                                                        <input type="radio" class="hidden-input type-donate" name="donate_time"
                                                            value="gift" id="check_3">
                                                        <label for="check_3" class="dark-text"><span> {{ __('web.Donate as a Gift') }}</span></label>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-xl-6 col-sm-6 md-center">
                                                <div class="form-group">
                                                    <div class="custom-checkbox circle-checkbox">
                                                        <input type="radio" class="hidden-input type-donate" name="donate_time"
                                                        value="ambassador" id="check_4">
                                                        <label for="check_4" class="dark-text"> <span>{{__('web.Be an Ambassador')}}</span></label>
                                                    </div>
                                                </div>
                                            </div>                                        

                                        
                                        </div>
                                    </div>

                                </div>
                            </div>

                        <div class="col-lg-3 col-md-4">
                                <div class="big-btns sm-center text-right-dir">
                                    @if(auth()->check() && auth()->user()->blocked)
                                    <button type="submit" class="tooltip-link big-btn first_color user-bocked"
                                                data-tippy-placement="top" title="{{__('web.donate_now')}}"><img
                                                    src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{__('web.donate_now')}}</button> 
                                    @else
                                        @if($data['project']->charity_id == null || $data['project']->active == 0)
                                            <button type="submit" class="tooltip-link big-btn first_color no-charity-project"
                                                data-tippy-placement="top" title="{{__('web.donate_now')}}"><img
                                                    src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{__('web.donate_now')}}</button>                                    
                                        @else                                 
                                            @if(auth()->guard('charity')->user())
                                            <button type="submit" class="tooltip-link big-btn first_color auth-charity"
                                                data-tippy-placement="top" title="{{__('web.donate_now')}}"><img
                                                    src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{__('web.donate_now')}}</button>                                
                                            @else                            
                                            <button 
                                                type="submit" 
                                                data-id="{{$data['project']->id}}" 
                                                @if($donor_total_supports < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                    data-url="{{ route('web.price.donate') }}" 
                                                @endif
                                                class="tooltip-link big-btn {{ $supports == $cost ? 'complate-cost' : 'donate-prog-btn' }} first_color
                                                @if($donor_total_supports >= 10000) user_donation_full @endif"
                                                data-tippy-placement="top" title="{{__('web.donate_now')}}"><img
                                                    src="{{ url('web/images/icons/menu/donate.png') }}" alt="img">{{__('web.donate_now')}}</button>
                                            @endif
                                        @endif
                                    @endif

                                    @if($data['project']->charity_id == null || $data['project']->active == 0)
                                        <button type="button" class="simple-btn tooltip-link big-btn first_color no-charity-project" data-method="POST"
                                            data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                            src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img">{{__('web.add_to_cart')}}</button>                                 
                                    @else
                                        <button type="button" class="simple-btn tooltip-link big-btn first_color add-to-cart" data-project_id="{{$data['project']->id}}" data-url="{{ route('web.add-to-cart') }}" data-method="POST"
                                            data-tippy-placement="top" title="{{__('web.add_to_cart')}}"><img
                                                src="{{ url('web/images/icons/menu/shopping-cart.png') }}" alt="img">{{__('web.add_to_cart')}}</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        @endif
        <!--end donate-->        



    </div>



@endsection

@push('js')

    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{asset('web/js/select-ar.js')}}"></script>
        <script type="text/javascript" src="{{asset('web/js/messages_ar.min.js')}}"></script>
    @endif

    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/cart.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/campain.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/backend/project-details.js')}}"></script>
    <script type="text/javascript" src="{{ asset('web/forms/donate.js') }}"></script>
    
    <!-- validation laravel -->
    <script src="{{ url('custom/custom-validate.js') }}"></script>    

@endpush