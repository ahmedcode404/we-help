<div class="modal" id="rate-modal" tabindex="-1" role="dialog" aria-labelledby="rate-mod" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body text-center">
                <div class="modal-header col-12">
                    <h5 class="modal-title white-text text-cap" id="rate-mod">{{__('web.rate_project')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="rate-sec">
                
                    <form action="{{route('web.projects.rate', $data['project']->slug)}}" method="POST" class="rate-form">
                        @csrf

                        <!--start slider-->
                        <div id="rate-owl"
                            class="owl-carousel owl-theme center-dots rate-owl square-dots dark-dots">

                            @if( count($data['comments']) > 0)

                                @foreach ($data['criterias'] as  $key => $rating_criteriar)
                                    
                                    <!--start item-->
                                    <div class="item">
                                        <div class="rate-div">
                                            <div class="inner-rate-div">
                                                <h3 class="second_color text-cap">{{$rating_criteriar->name}}:</h3>
                                                <div class="inner-rate sm-raduis">
                                                    <div class="static-stars text-center">
                                                        <input type="hidden" name="item[]" value="{{ $data['project']->ratings[$key]->id }}" id="item-{{$rating_criteriar->id}}">
                                                        <input class="star" value="5" id="rate{{$rating_criteriar->id}}-1" {{ updateRatingUser($data['project']->id  , $data['project']->ratings[$key]->id , 'grade') == 5 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                        <label class="star" for="rate{{$rating_criteriar->id}}-1"></label>
                                                        <input class="star" value="4" id="rate{{$rating_criteriar->id}}-2" {{ updateRatingUser($data['project']->id  , $data['project']->ratings[$key]->id , 'grade') == 4 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                        <label class="star" for="rate{{$rating_criteriar->id}}-2"></label>
                                                        <input class="star" value="3" id="rate{{$rating_criteriar->id}}-3" {{ updateRatingUser($data['project']->id  , $data['project']->ratings[$key]->id , 'grade') == 3 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                        <label class="star" for="rate{{$rating_criteriar->id}}-3"></label>
                                                        <input class="star" value="2" id="rate{{$rating_criteriar->id}}-4" {{ updateRatingUser($data['project']->id  , $data['project']->ratings[$key]->id , 'grade') == 2 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                        <label class="star" for="rate{{$rating_criteriar->id}}-4"></label>
                                                        <input class="star" value="1" id="rate{{$rating_criteriar->id}}-5" {{ updateRatingUser($data['project']->id  , $data['project']->ratings[$key]->id , 'grade') == 1 ? 'checked' : '' }} type="radio" name="{{$rating_criteriar->id}}">
                                                        <label class="star" for="rate{{$rating_criteriar->id}}-5"></label>
                                                    </div>
                                                </div>
                                                <button type="button"
                                                    class="custom-btn white-btn sm-raduis next-owl"><span>{{__('web.next')}}</span></button>
                                                <span class="done-rate"><i class="fa fa-check"></i></span>

                                            </div>
                                        </div>
                                    </div>
                                    <!--end item-->
                                    
                                @endforeach

                                <!--start item-->
                                <div class="item">
                                    <div class="rate-div">
                                        <div class="inner-rate-div">
                                            <h3 class="second_color text-cap">{{__('web.rate_project_text')}}:</h3>
                                            <div class="form-group">
                                                <textarea placeholder="{{ __('web.Leave your comment') }}" class="form-control"
                                                    name="comment" required>{{ updateRatingUser($data['project']->id , null , 'comment') }}</textarea>
                                            </div>
                                            <button class="custom-btn white-btn sm-raduis"><span>{{__('web.submit')}}</span></button>
                                            <span class="done-rate last-done"><i class="fa fa-check"></i></span>
    
                                        </div>
                                    </div>
                                </div>
                                <!--end item-->

                            @else

                                @foreach ($data['criterias'] as $criteria)
                                        
                                    <!--start item-->
                                    <div class="item">
                                        <div class="rate-div">
                                            <div class="inner-rate-div">
                                                <h3 class="second_color text-cap">{{$criteria->name}}:</h3>
                                                <div class="inner-rate sm-raduis">
                                                    <div class="static-stars text-center">
                                                        <input class="star" value="5" id="rate{{$criteria->id}}-1" type="radio" name="{{$criteria->id}}" checked required>
                                                        <label class="star" for="rate{{$criteria->id}}-1"></label>
                                                        <input class="star" value="4" id="rate{{$criteria->id}}-2" type="radio" name="{{$criteria->id}}" required>
                                                        <label class="star" for="rate{{$criteria->id}}-2"></label>
                                                        <input class="star" value="3" id="rate{{$criteria->id}}-3" type="radio" name="{{$criteria->id}}" required>
                                                        <label class="star" for="rate{{$criteria->id}}-3"></label>
                                                        <input class="star" value="2" id="rate{{$criteria->id}}-4" type="radio" name="{{$criteria->id}}" required>
                                                        <label class="star" for="rate{{$criteria->id}}-4"></label>
                                                        <input class="star" value="1" id="rate{{$criteria->id}}-5" type="radio" name="{{$criteria->id}}" required>
                                                        <label class="star" for="rate{{$criteria->id}}-5"></label>
                                                    </div>
                                                </div>
                                                <button type="button"
                                                    class="custom-btn white-btn sm-raduis next-owl"><span>{{__('web.next')}}</span></button>
                                                <span class="done-rate"><i class="fa fa-check"></i></span>

                                            </div>
                                        </div>
                                    </div>
                                    <!--end item-->
                                    
                                @endforeach

                                <!--start item-->
                                <div class="item">
                                    <div class="rate-div">
                                        <div class="inner-rate-div">
                                            <h3 class="second_color text-cap">{{__('web.rate_project_text')}}:</h3>
                                            <div class="form-group">
                                                <textarea placeholder="Leave your comment" class="form-control"
                                                    name="comment" required></textarea>
                                            </div>
                                            <button class="custom-btn white-btn sm-raduis"><span>{{__('web.submit')}}</span></button>
                                            <span class="done-rate last-done"><i class="fa fa-check"></i></span>

                                        </div>
                                    </div>
                                </div>
                                <!--end item-->

                            @endif


                        </div>
                        <!--end slider-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>