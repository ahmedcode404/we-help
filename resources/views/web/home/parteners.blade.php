<section class="partenter-sec margin-div slide_section" id="sec_3">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="section-title first_color" data-aos="fade-in">
                    <div class="side-img"><img src="{{asset('web/images/icons/menu/11.png')}}" alt="icon"></div>
                    <h3 class="second_color text-cap bold-text">{{trans('web.our_partners')}}</h3>
                    {{getWebSettingMsg('parteners_text')->value}}
                </div>
            </div>

            <div class="col-12 text-center" data-aos="fade-in">
                <div id="partener-owl" class="owl-carousel owl-theme">

                    @foreach ($data['partners'] as $partner)

                        <!--start item-->
                        <div class="item">
                            <a href="@if($partner->link){{$partner->link}}@else#@endif" target="_blank">
                                <div class="partener-div">
                                    <div class="partener-img">
                                        <img src="{{asset('storage/'.$partner->logo)}}" alt="{{trans($partner->name)}}">
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!--end item-->

                    @endforeach

                </div>
            </div>
        </div>
    </div>
</section>