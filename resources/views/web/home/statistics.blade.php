<section class="statistics-sec has_seudo margin-div slide_section" id="sec_2">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title first_color" data-aos="fade-in">
                    <div class="side-img"><img src="{{asset('web/images/icons/menu/4.png')}}" alt="icon"></div>
                    <h3 class="second_color text-cap bold-text">{{trans('web.statistics')}}</h3>
                    {{getWebSettingMsg('statistics_text')->value}}
                </div>
            </div>
        </div>

        <div class="row justify-content-center text-center first_color text-cap" id="counter">
            <!--start count-div-->
            <div class="col-md-4  col-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{asset('web/images/icons/statistics/1.png')}}" alt="icon">
                    <h3>{{trans('web.projects')}}</h3>
                    <div class="counter-value big-raduis" data-count="{{$data['projects_count']}}"><span>0</span></div>
                </div>
            </div>
            <!--end count-div-->

            <!--start count-div-->
            <div class="col-md-4  col-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{asset('web/images/icons/statistics/2.png')}}" alt="icon">
                    <h3>{{trans('web.partners')}}</h3>
                    <div class="counter-value big-raduis" data-count="{{$data['parteners_count']}}"><span>0</span></div>
                </div>
            </div>
            <!--end count-div-->


            <!--start count-div-->
            <div class="col-md-4  col-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{asset('web/images/icons/statistics/3.png')}}" alt="icon">
                    <h3>{{trans('web.donated_amounts')}}</h3>
                    <div class="counter-value big-raduis" data-count="{{$data['donation_ammounts']}}"><span>0</span> {{ $data['session_currency'] }}</div>
                </div>
            </div>
            <!--end count-div-->


            <!--start count-div-->
            <div class="col-md-4  col-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{asset('web/images/icons/statistics/4.png')}}" alt="icon">
                    <h3>{{trans('web.countries')}}</h3>
                    <div class="counter-value big-raduis" data-count="{{$data['countries']}}"><span>0</span></div>
                </div>
            </div>
            <!--end count-div-->


            <!--start count-div-->
            <div class="col-md-4  col-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{asset('web/images/icons/statistics/5.png')}}" alt="icon">
                    <h3>{{trans('web.beneficiaries')}}</h3>
                    <div class="counter-value big-raduis" data-count="{{$data['benef_count']}}"><span>0</span></div>
                </div>
            </div>
            <!--end count-div-->

        </div>
    </div>
</section>