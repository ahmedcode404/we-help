<section class="main-slider">
    <div class="container-fluid">
        <div class="row">
            <!--start col-->
            <div class="col-lg-12 no-marg-row">
                <div class="slider">

                    <div id="main-owl" class="owl-carousel owl-theme main-slider-owl">

                        @foreach ($data['slider'] as $slider)
                            
                            <!--start item-->
                            <div class="item">
                                <div class="full-width-img main-slide-item"
                                    style="background-image:url({{asset('storage/'.$slider->path)}})">
                                    <div class="slider-caption">
                                        <div>
                                            <h3 class="second_color bold-text">{{$slider->title}}</h3>
                                            <p>{{$slider->text}}</p>
                                            <a href="{{route('web.donate')}}">{{trans('web.donate_now')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end item-->

                        @endforeach

                    </div>


                </div>
            </div>
        </div>
    </div>
</section>