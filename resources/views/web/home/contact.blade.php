<section class="contact-sec margin-div slide_section" id="sec_5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title first_color" data-aos="fade-in">
                    <div class="side-img"><img src="{{asset('web/images/icons/menu/6.png')}}" alt="icon"></div>
                    <h3 class="second_color text-cap bold-text">{{trans('admin.contact_us')}}</h3>
                    {{getWebSettingMsg('contact_us_text')->value}}
                </div>
            </div>

            <div class="col-12">
                <div class="map-sec sm-center" data-aos="fade-in">
                    <img src="{{asset('web/images/main/map.png')}}" width="100%" class="map-img d-md-block d-none" alt="map">
                    <img src="{{asset('web/images/main/pin.png')}}" class="marker d-md-block d-none" alt="marker">
                    <a href="https://www.google.com/maps/search/?api=1&query={{ $data['lat']->value }},{{ $data['lang']->value }}" class="white-text sm-raduis" target="_blank">{{ __('web.look_map') }} <i class="fa fa-long-arrow-alt-right"></i></a>
                </div>

                <div class="contact-div sm-raduis" data-aos="zoom-out">
                    <form action="{{route('web.store-contact-us')}}" method="POST" class="border-form contact-form">
                        @csrf

                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="{{__('web.name')}}" value="{{old('name')}}" required>
                            <label></label>
                            @if($errors->has('name'))
                                <div class="custom-error error">{{ $errors->first('name') }}</div>
                            @endif 
                        </div>

                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="{{__('web.email')}}" value="{{old('email')}}" required>
                            <label></label>
                            @if($errors->has('email'))
                                <div class="custom-error error">{{ $errors->first('email') }}</div>
                            @endif 
                        </div>

                        <div class="form-group phone-input">
                            <input type="tel" minlength="8" maxlength="10" class="form-control" name="contact_number" placeholder="{{__('web.phone')}}" id="contact_number" value="{{old('phone')}}" required>
                            <label></label>
                            @if($errors->has('contact_number'))
                                <div class="custom-error error">{{ $errors->first('contact_number') }}</div>
                            @endif 
                            @if($errors->has('phone'))
                                <div class="custom-error error">{{ $errors->first('phone') }}</div>
                            @endif 
                        </div>

                        <div class="form-group">
                            <textarea class="form-control" name="content" placeholder="{{__('web.content')}}" required>{{old('content')}}</textarea>
                            <label></label>
                            @if($errors->has('content'))
                                <div class="custom-error error">{{ $errors->first('content') }}</div>
                            @endif 
                        </div>

                        <div class="text-center btn-div">
                            <button type="submit" class="custom-btn sm-raduis"><span>{{__('web.send_message')}}</span></button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>
