
@extends('web.layout.app')

@section('title', __('web.home'))
@section('description', __('web.home'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

        <div class="index-pg">
        <!-- loading -->
        @include('web.layout.loading')
        <!-- loading -->

        <!-- start main-slider
         ================ -->
         @include('web.home.slider')
        <!--end main slider-->

        <!-- start main-projects-sec
         ================ -->
        @include('web.home.projects')
        <!--end main-projects-sec-->

        <!-- start statistics-sec
         ================ -->
        @include('web.home.statistics')
        <!--end statistics-sec-->

        <!-- start partenter-sec
         ================ -->
        @include('web.home.parteners')
        <!--end partenter-sec-->

        <!-- start about-sec
         ================ -->
        @include('web.home.about_us')
        <!--end about-sec-->

        <!-- start contact-sec
         ================ -->
        @include('web.home.contact')
        <!--end contact-sec-->
        </div>
@endsection

@push('js')

    <!--for this page only-->
    <script type="text/javascript" src="{{asset('web/js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/intlTelInput.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('web/js/index.js')}}"></script>    
    <script type="text/javascript" src="{{asset('web/backend/cart.js')}}"></script>   

    <script type="text/javascript" src="{{asset('web/js/projects.js')}}"></script>

    <!-- validation laravel -->
    <script src="{{ url('custom/custom-validate.js') }}"></script>  

@endpush



        





    