<section class="about-sec margin-div slide_section" id="sec_4">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12">
                <div class="section-title first_color" data-aos="fade-in">
                    <div class="side-img"><img src="{{asset('web/images/icons/menu/5.png')}}" alt="icon"></div>
                    <h3 class="second_color text-cap bold-text">{{$data['about_us']->title}}</h3>
                    {{getWebSettingMsg('about_us_text')->value}}
                </div>
            </div>

            <div class="col-lg-6 about-text-grid" data-aos="fade-in">
                {!! $data['about_us']->content !!}

                <div class="text-left-dir btn-div">
                    <a href="{{ route('web.about') }}" class="custom-btn big-raduis reverse-btn"><span>{{trans('web.read_more')}}</span></a>
                </div>
            </div>

            <div class="col-lg-6 about-img-grid">
                <img src="{{asset('storage/'.$data['about_us']->image)}}" alt="img" data-aos="zoom-in">
            </div>

        </div>
    </div>
</section>