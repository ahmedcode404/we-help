 <!--start select-modals-->
    <!--once modal-->
    @include('web.donate_popups.once')

    <!--gift modal-->
    @include('web.donate_popups.gift')

    <!--month modal-->
    @include('web.donate_popups.monthly')

    <!--ambassador modal-->
    @include('web.donate_popups.ambassador')
  <!--end select-modal-->


<section class="main-projects-sec margin-div slide_section" id="sec_1">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title first_color proje-all-title" data-aos="fade-in">
                    <div class="side-img"><img src="{{asset('web/images/icons/menu/3.png')}}" alt="icon"></div>
                    <h3 class="second_color text-cap bold-text has-side">{{trans('web.our_projects')}}
                        {{-- <a href="{{route('web.projects')}}">{{trans('web.show_all')}}<i class="fa fa-long-arrow-alt-right"></i></a> --}}
                        <a href="{{route('web.get-category-projects', 'all')}}" class="custom-btn big-raduis reverse-btn"><span>{{trans('web.show_all')}}</span></a>
                    </h3>
                    {{getWebSettingMsg('our_projects_text')->value}}
                </div>
            </div>
        </div>

        <div class="project-tabs big-raduis">
            <button class="simple-btn d-md-none d-block filter-tabs-btn sm-raduis text-cap">{{trans('web.filter_donation')}}<i
                    class="fa fa-filter"></i></button>
            <div class="filter-tabs">
                <button class="simple-btn auto-icon d-md-none d-inline-block close-tabs"><i
                        class="fa fa-times"></i></button>

                <ul class="nav nav-tabs justify-content-center text-cap" id="myTab" role="tablist">

                    @foreach ($data['categories'] as $category)

                        <li class="nav-item col" data-light-color="{{$category->sub_color}}" data-main-color="{{$category->main_color}}">
                            <a class="nav-link @if($loop->iteration == 1) active @endif" data-toggle="tab" href="#tab_{{$category->id}}" role="tab"
                                aria-controls="tab_{{$category->id}}" aria-selected="@if($category->id == 1) true @else false @endif">{{$category->name}}</a>
                        </li>

                    @endforeach
                    
                </ul>
            </div>
            
            <div class="tab-content" id="myTabContent">

                @foreach ($data['categories'] as $category)
                    <!--start tab-pane-->
                    <div class="tab-pane @if($loop->iteration == 1) show active @endif" id="tab_{{$category->id}}" role="tabpanel" aria-labelledby="tab_{{$category->id}}">
                        <div class="inner-tab">
                            {{-- min cost for support - value comes from settings--}}
                                {{-- <input type="hidden" id="cost_min" value="{{$cost_min}}"> --}}

                            <div id="projects-owl-{{$category->id}}" class="owl-carousel owl-theme projects-owl no-full-images">
                             
                                <!--start item projects-->
                                {{-- completed projects --}}
                                @php
                                    $project_exists = false;
                                @endphp

                                @forelse ($category->projects as $project)
                                    @php
                                        $project_exists = true;
                                    @endphp
                                    <div class="item">
                                        <div class="project-div md-raduis @if($project->donation_complete == 1) gray-project-div @endif">
                                            <a href="{{route('web.projects.show', $project->slug)}}" target="_blank">
                                                <div class="project-img md-raduis">
                                                    <img src="{{asset('storage/'.$project->image)}}" alt="{{$project->name}}">
                                                </div>
                                                <div class="padding-div">
                                                    <h3 class="text-cap project-title">{{$project->name}}</h3>
                                                    <p class="first_color">{{$project->desc}}</p>
                                                </div>
                                            </a>

                                            <div class="padding-div">
                                                <div class="row align-items-center">
                                                    <div class="col-lg-6  col-sm-6">
                                                        <div class="section-title">
                                                            <div class="side-img">
                                                                @if($project->charity)
                                                                    <img src="{{asset('storage/'.$project->charity->logo)}}" alt="icon">
                                                                @else
                                                                    <img src="{{asset('web/images/projects/logo.png')}}" alt="icon">
                                                                @endif
                                                            </div>
                                                            <h3 class="text-cap">{{$project->charity ? $project->charity->name : __('web.not_available')}}</h3>
                                                            @php
                                                                $ratings = $project->getRatings();
                                                            @endphp
                                                            <div class="stars auto-icon">
                                                                <i class="fa fa-star @if($ratings >= 1) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($ratings >= 2) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($ratings >= 3) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($ratings >= 4) yellow-star @endif"></i>
                                                                <i class="fa fa-star @if($ratings >= 5) yellow-star @endif"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6  col-sm-6 d-lg-none d-md-block">
                                                        <div class="projects-money first_color text-center">
                                                            <div>
                                                                <div class="res-bene"> {{trans('web.beneficiaries')}}
                                                                    <br>
                                                                </div>
                                                                <span class="sm-raduis">{{$project->benef_num}} {{trans('web.beneficiary')}}</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    @php

                                                        $supportRepository = \App::make('App\Repositories\SupportRepositoryInterface');
                                                        $supports_gbp = $supportRepository->getWhere([['cost', '!=', 0], ['status', 'support'], 
                                                                            ['nation_id', getNationId()], ['project_id', $project->id]])->sum('cost_gbp');

                                                        $supports = generalExchange($supports_gbp, 'GBP', $data['session_currency']);
                                                        
                                                        // $sub_sum = 0;

                                                        // foreach($project->donates as $support){

                                                        //     $sub_sum += generalExchange($support->pivot->cost_gbp , 'GBP', $data['session_currency']);

                                                        // }

                                                        // $supports = $sub_sum;



                                                        $cost = generalExchange($project->get_total, $project->currency->symbol, $data['session_currency']);

                                                        
                                                        if($supports > 0){

                                                            $progress = round($supports / $cost * 100, 2);
                                                        }
                                                        else{

                                                            $progress = 0;
                                                        }

                                                        if($progress > 100){
                                                            $progress = 100;
                                                        }

                                                        if($progress < 0){
                                                            $progress = 0;
                                                        }

                                                    @endphp

                                                    <div class="col-lg-6  progress-grid">
                                                        <div class="total-donation d-lg-none d-md-block">{{trans('web.total')}}:{{$supports}}
                                                            {{$data['session_currency']}}</div>
                                                        <div class="progress-div">
                                                            <div class="full-progress big-raduis">
                                                                <span class="progress-ratio">{{$progress}}%
                                                                    <span class="arrow-triangle"></span>
                                                                </span>
                                                                <div class="progress-bar-ratio big-raduis"
                                                                    data-progress="{{$progress}}"></div>
                                                            </div>

                                                    </div>
                                                    <div class="progress-min-max">
                                                        @php
                                                            $total_donates = $supports;
                                                            $total_cost = $cost;
                                                            $remaining_donates = $total_cost - $total_donates;

                                                            if($remaining_donates < 0){

                                                                $remaining_donates = 0;
                                                            }
                                                            
                                                        @endphp
                                                        <span>{{$total_donates}} {{$data['session_currency']}}</span>
                                                        <span>{{$remaining_donates}} {{$data['session_currency']}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="projects-money first_color text-center d-lg-block d-none">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            {{trans('web.beneficiaries')}}
                                                            <br>
                                                            <span class="sm-raduis">{{$project->benef_num}}</span>
                                                        </div>
                                                        <div class="col-6">
                                                            {{trans('web.total_donations')}}
                                                            <br>
                                                            <span class="sm-raduis">{{$supports}} {{$data['session_currency']}}</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="project-shape"><span>
                                                    <img src="{{asset('web/images/icons/menu/3.png')}}" alt="img"></span>
                                            </div>
                                            <div class="project-select text-center">
                                            
                                                @if($project->nation_id == $nation_id)
                                                    <div class="padding-div">

                                                        {{-- <span style="color: #3dbecb;">{{__('web.cost_min')}}: {{$cost_min}} {{$data['session_currency']}}</span> --}}
                                                        
                                                        <form action="" class="donate-sm-form donate-cost-error-{{ $project->id }}">

                                                            <input type="hidden" name="project_id" class="project_id" id="{{ $project->id }}" value="{{ $project->id }}">
                                                            <input type="hidden" id="select-type-{{$project->id}}" value="select">
                                                            
                                                            <div class="form-group">
                                                                <select class="form-control select-input2 pop-select" id="type-{{$project->id}}">
                                                                    <option selected value="once">{{trans('web.once')}}</option>
                                                                    <option value="month">{{trans('web.monthly_deduction')}}</option>
                                                                    <option value="gift">{{trans('web.gift')}}</option>
                                                                    <option value="ambassador">{{trans('web.ambassador')}}</option>
                                                                </select>
                                                            </div>

                                                            <div class="form-group number-input-div">
                                                                <span class="minus-num  numb-control">-</span>
                                                                <input type="number" step=".01"
                                                                    class="form-control no_apperance_number number-input"  id="cost-{{$project->id}}"
                                                                    minlength="1" maxlength="7" value="{{$cost_min}}" pattern="\d{5}">
                                                                <span class="plus-num numb-control">+</span>

                                                            </div>

                                                            <div class="form-group">
                                                                <select class="form-control select-input currency_id" name="currency_id" id="currency-{{$project->id}}">
                                                                    @foreach ($currencies as $currency)
                                                                    
                                                                        <option value="{{$currency->id}}" @if(session('currency') == $currency->id) selected @endif>{{$currency->symbol}}</option>
                                                                    
                                                                    @endforeach
                                                                </select>
                                                            </div>

                                                            <div class="btn-div">

                                                                @if(auth()->check() && auth()->user()->blocked)
                                                                    <button type="button" class="custom-btn big-raduis white-btn user-bocked"><span>{{ __('web.Donate') }}</span></button>
                                                                @else
                                                                    @if($project->charity_id == null || $project->active == 0)
                                                                        <button type="button" class="custom-btn big-raduis white-btn no-charity-project"><span>{{ __('web.Donate') }}</span></button>
                                                                    @else 
                                                                        @if(auth()->guard('charity')->user())

                                                                            <button type="button" class="custom-btn big-raduis white-btn auth-charity"><span>{{trans('web.donate')}}</span></button>

                                                                        @else
                                                                            <button 
                                                                                type="button" 
                                                                                data-id="{{$project->id}}" 
                                                                                @if($data['donor_total_supports'] < 10000)  {{-- user can not donate if hist total donations is 10000 GBP a year --}}
                                                                                    data-url="{{ route('web.price.donate') }}"
                                                                                @endif
                                                                                class="{{ $supports == $cost ? 'complate-cost' : 'donate-project' }} custom-btn big-raduis white-btn
                                                                                @if($data['donor_total_supports'] >= 10000) user_donation_full @endif"><span>{{trans('web.donate')}}</span></button>
                                                                        @endif    
                                                                    @endif 
                                                                @endif

                                                            </div>
                                                            <span class="custom-error error"> </span>                                                        
                                                        </form>
                                                    </div>
                                                @endif

                                            </div>
                                        
                                            <div class="project-btns text-center">
                                                @if($project->nation_id == $nation_id)
                                                    <button class="simple-btn tooltip-link col first_color add-to-cart" data-project_id="{{$project->id}}" data-url="{{ route('web.add-to-cart') }}" data-method="POST"
                                                        title="{{trans('web.add_to_cart')}}"><img src="{{asset('web/images/icons/menu/shopping-cart.png')}}"
                                                        alt="img">{{trans('web.add_to_cart')}}
                                                    </button>
                                                @endif

                                                @if(count($category->projects) > 0)
                                                    <a href="{{route('web.projects.show', $project->slug)}}" class="tooltip-link col first_color"
                                                        title="{{trans('web.more_details')}}"><img src="{{asset('web/images/icons/menu/donate.png')}}"
                                                            alt="img">{{trans('web.more_details')}}
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @empty
                                    <div class="text-left-dir margin-div gray-color col-12">
                                        {{ trans('admin.no_data') }}
                                    </div>
                                @endforelse
                                
                                <!--end item projects-->

                            </div>

                             @if(count($category->projects) > 0 && $project_exists == false)
                                <div class="text-left-dir margin-div gray-color col-12">
                                    {{ trans('admin.no_data') }}
                                </div>           
                            @endif
                            
                            <div class="text-center-dir btn-div">
                                <a href="{{route('web.get-category-projects', $category->slug)}}" class="custom-btn big-raduis reverse-btn"><span>{{ trans('admin.read_more') }}</span></a>
                            </div>
                        </div>
                    </div>
                    <!--end tab-pane-->
                @endforeach


            </div>
        </div>
    </div>
</section>