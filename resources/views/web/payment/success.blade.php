<!DOCTYPE html>

@if(\App::getLocale() == 'ar')
  <html dir="rtl">
@else
  <html dir="ltr">
@endif

<head>
    <title>{{trans('admin.we_help')}} :)</title>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content=" website" />
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('web/images/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('web/images/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('web/images/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('web/images/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('web/images/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('web/images/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('web/images/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('web/images/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('web/images/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('web/images/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('web/images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('web/images/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('web/images/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('web/images/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#000">
    <meta name="msapplication-TileImage" content="{{asset('web/images/favicon/ms-icon-144x144.png')}}">


    <!-- Style sheet
         ================ -->

    <link rel="stylesheet" href="{{asset('web/css/styles.css')}}" type="text/css" />

    @if(\App::getLocale() == 'ar')
      <!--for arabic-->
      <link rel="stylesheet" href="{{asset('web/css/ar.css')}}" type="text/css" />
    @endif

    <!--for english-->
    <link rel="stylesheet" href="{{asset('web/css/en.css')}}" type="text/css" />
    <!--end-->
    <link rel="stylesheet" href="{{asset('web/css/responsive.css')}}" type="text/css" />
    <script type="text/javascript" src="{{asset('web/js/jquery-3.4.1.min.js')}}"></script>

</head>

<body class="no-trans">
  <style>
    a{
      display: inline-block;
    }
    p{
      margin-bottom: 25px
    }
  </style>
  <div class="main-content">

        <!-- start agreement-pg
         ================ -->
        <section class="success-pg">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="success-payment text-cap sm-raduis text-center" data-aos="zoom-out">
                            <img src="{{asset('web/images/main/payment.png')}}" alt="payment">
                            <h1 class="bold-text second_color">{{__('web.payment_success')}}</h1>
                           <p>
                            {{ trans('web.donated_with') }}
                            <b> {{ $support->cost }} {{ $support->currency ? $support->currency->name : currencySymbol(session('currency')) }}</b>
                             {{ trans('web.for_project') }} <b>{{ $support->project ? $support->project->name : '-' }}</b> 
                             {{ trans('web.for_charity') }} <b>{{ $support->project->charity ? $support->project->charity->name : '-' }}</b>
                               {{ trans('web.you_can_see_voucher') }}
                              <a href="{{ route('get-voucher', $voucher->slug) }}" target="_blank" class="second_color first_color_hover">{{ route('get-voucher', $voucher->slug) }}</a>
                              <br>
                               {{ trans('web.thanks_for_help') }} ❤
                           </p>
                      
                            <a href="{{route('web.home')}}" class="custom-btn sm-raduis white-btn"><span>{{__('web.back_to_home')}}</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end agreement-pg-->
    </div>





    <!-- scripts
         ================ -->
    <script type="text/javascript" src="{{asset('web/js/html5shiv.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/respond.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/popper.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/aos.min.js')}}"></script>
    {{-- <script type="text/javascript" src="https://unpkg.com/tippy.js@2.5.4/dist/tippy.all.min.js"></script> --}}
    <script type="text/javascript" src="{{asset('web/js/jquery.validate.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/sweetalert2.all.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-sweetalert.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/select2.min.js')}}"></script>

    @if(\App::getLocale() == 'ar')
      <!--for arabic-only-->
      <script type="text/javascript" src="{{asset('web/js/select-ar.js')}}"></script>
    @endif
    
    <script type="text/javascript" src="{{asset('web/js/custom.js')}}"></script>
    {{-- <script type="text/javascript" src="{{asset('web/js/theme-options.js')}}"></script> --}}

    @if(\App::getLocale() == 'ar')
      <!--for arabic-only-->
      <script type="text/javascript" src="{{asset('web/js/messages_ar.min.js')}}"></script>
    @endif

</body>

</html>