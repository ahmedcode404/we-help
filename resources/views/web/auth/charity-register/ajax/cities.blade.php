<option selected disabled></option>
@foreach ($data['cities'] as $city)
    <option value="{{$city->id}}" @if(old('city_id') == $city->id) selected @endif>{{$city->name}}</option>
@endforeach