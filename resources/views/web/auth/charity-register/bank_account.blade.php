<div class="row align-items-end">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color">{{ __('web.bank_account_information') }}:</h3>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="bank_name" value="{{ old('bank_name') }}" required>
            <div class="error error-bank_name">{{ $errors->first('bank_name') }}</div>
            <label class="moving-label">{{ __('web.bank_name') }}</label>
        </div>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="bank_address" value="{{ old('bank_address') }}" required>
            <div class="error error-bank_address">{{ $errors->first('bank_address') }}</div>
            <label class="moving-label">{{ __('web.bank_address') }}</label>
        </div>
    </div>



    <div class="col-6">
        <div class="form-group phone-input" data-aos="fade-in">
            <input type="tel" class="form-control" name="contact_number" id="bank_phone"
                value="{{ session('bank_phone') ? session('bank_phone') : old('contact_number') }}" placeholder="{{ __('web.phone') }}" required>
            <div class="error error-contact_number">{{ $errors->first('contact_number') }}</div>
            <!--<label class="moving-label">{{ __('web.phone') }}</label>-->
        </div>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="bank_city" value="{{ old('bank_city') }}" required>
            <div class="error error-bank_city">{{ $errors->first('bank_city') }}</div>
            <label class="moving-label">{{ __('web.city') }}</label>
        </div>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="iban" value="{{ old('iban') }}" required>
            <div class="error error-iban">{{ $errors->first('iban') }}</div>
            <label class="moving-label">{{ __('web.iban') }}</label>
        </div>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="swift_code" value="{{ old('swift_code') }}" required>
            <div class="error error-swift_code">{{ $errors->first('swift_code') }}</div>
            <label class="moving-label">{{ __('web.swift_code') }}</label>
        </div>
    </div>

</div>
