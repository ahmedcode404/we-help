<div class="row align-items-end">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color">{{ __('web.association_details') }}:</h3>
    </div>


    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <textarea class="form-control" name="strategy_en" required>{{ old('strategy_en') }}</textarea>
            <div class="error error-strategy_en">{{ $errors->first('strategy_en') }}</div>
            <label class="moving-label">{{ __('web.strategy_en') }} ( {{ __('web.less_than_500_word') }} )</label>
        </div>
    </div>



    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <textarea class="form-control" name="strategy_ar">{{ old('strategy_ar') }}</textarea>
            <div class="error error-strategy_ar">{{ $errors->first('strategy_ar') }}</div>
            <label class="moving-label">{{ __('web.strategy_ar') }} ( {{ __('web.less_than_500_word') }} )</label>
        </div>
    </div>


    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <textarea class="form-control" name="work_fields" required>{{ old('work_fields') }}</textarea>
            <div class="error error-work_fields">{{ $errors->first('work_fields') }}</div>
            <label class="moving-label">{{ __('web.work_fields') }}</label>
        </div>
    </div>

    {{-- <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="work_fields[]" multiple required>
                <option selected disabled></option>
                @foreach ($data['work_fields'] as $field)
                    <option value="{{ $field->id }}">{{ $field->name }}</option>
                @endforeach
            </select>
            <div class="error error-work_fields">{{ $errors->first('work_fields') }}</div>
            <label class="moving-label">{{ __('web.work_fields') }}</label>
        </div>
    </div> --}}



    <div class="col-12">
        <div class="form-group input-has-icon single-file" data-aos="fade-in">
            <i class="far fa-image second_color"></i>
            <input type="file" class="form-control hidden-input" accept=".png,.jpg,.jpeg,.pdf" id="file_11" name="attach" required>
            <label class="moving-label">{{ __('web.attachments') }} (
                {{ __('web.photo_of_deed_of_ownership_or_lease') }} )</label>
            <label for="file_11" class="file-label form-control"></label>
            <small>(Max size: 50 MB)</small>
            <div class="error error-attach">{{ $errors->first('attach') }}</div>
        </div>
    </div>


    <div class="col-12">
        <div class="form-group input-has-icon single-file" data-aos="fade-in">
            <i class="far fa-image second_color"></i>
            <input type="file" class="form-control hidden-input" accept=".png,.jpg,.jpeg,.pdf" id="file_21" name="internal_image"
                required>
            <label class="moving-label">{{ __('web.photo_of_the_rules_of_procedure_of_the_association') }}</label>
            <label for="file_21" class="file-label form-control"></label>
            <small>(Max size: 50MB)</small>
            <div class="error error-internal_image">{{ $errors->first('internal_image') }}</div>
        </div>
    </div>


    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="years" required>
                <option selected disabled></option>
                <option value="one" @if (old('years') == 'one') selected @endif>{{ trans('admin.year') }}</option>
                <option value="two" @if (old('years') == 'two') selected @endif>{{ trans('admin.2') }} {{ trans('admin.years') }}</option>
                <option value="three" @if (old('years') == 'three') selected @endif>{{ trans('admin.3') }} {{ trans('admin.years') }}</option>
            </select>
            <div class="error error-years">{{ $errors->first('years') }}</div>

            <label class="moving-label">{{ __('web.annual_report') }} ( {{ __('web.select_year') }} )</label>
        </div>
    </div>

    <div class="col-12">
        <div class="form-group input-has-icon has-file-name single-file" data-aos="fade-in">
            <i class="far fa-file second_color"></i>
            <label for="file_report" class="file_name sm-raduis">{{ __('web.choose_file') }}</label>
            <input type="file" class="form-control hidden-input" accept=".png,.jpg,.jpeg,.pdf" id="file_report" name="year_report"
                required>
            <label class="moving-label">{{ __('web.annual_report') }}</label>
            <label for="file_report" class="file-label form-control"></label>
            <small>(Max size: 50MB)</small>
            <div class="error error-year_report">{{ $errors->first('year_report') }}</div>
        </div>
    </div>


    <div class="col-12">
        <div class="form-group input-has-icon single-file" data-aos="fade-in">
            <i class="far fa-image second_color"></i>
            <input type="file" class="form-control hidden-input" accept=".png,.jpg,.jpeg" id="file_13" name="logo"
                required>
            <label class="moving-label">{{ __('web.association_logo') }}</label>
            <label for="file_13" class="file-label form-control"></label>
            <small>(Max size: 20MB)</small>
            <div class="error error-logo">{{ $errors->first('logo') }}</div>
        </div>
    </div>

</div>
