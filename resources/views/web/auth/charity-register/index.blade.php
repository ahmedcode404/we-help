@extends('web.layout.app')

@section('title', __('web.register'))
@section('description', __('web.register'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

    <!-- start pages-header
        ================ -->
    <section class="pages-header text-center" style="background-image:url({{asset('web/images/pages-bg/7.png')}})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{__('web.join_as_association')}}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{__('web.join_as_association')}}</h2>
        </div> --}}


        <!-- start login-pg
         ================ -->
        <section class="margin-div login-pg">
            <div class="login-width">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title first_color" data-aos="fade-in">
                                <div class="side-img"><img src="{{asset('web/images/icons/association2.png')}}" alt="icon"></div>
                                <h3 class="second_color text-cap bold-text">{{__('web.join_as_association')}}</h3>
                                {{__('web.join_as_association_text')}}:
                            </div>

                            <form action="{{route('web.register-charity')}}" method="POST" class="gray-form association-form submit_form" enctype="multipart/form-data">
                                @csrf

                                <div class="association-div">
                                    {{-- Association information --}}
                                    @include('web.auth.charity-register.association_information')

                                </div>

                                <div class="association-div">
                                    {{-- Association password --}}
                                    @include('web.auth.charity-register.association_password')

                                </div>

                                <div class="association-div">
                                    {{-- Association address --}}
                                    @include('web.auth.charity-register.association_address')
                                    
                                </div>


                                <div class="association-div">
                                    {{-- Association representer --}}
                                    @include('web.auth.charity-register.association_representer')
                                    
                                </div>


                                <div class="association-div">
                                    {{-- Association details --}}
                                    @include('web.auth.charity-register.association_details')
                                    
                                </div>


                                <div class="association-div">
                                    {{-- Bank account --}}
                                    @include('web.auth.charity-register.bank_account')
                                    
                                </div>


                                <div class="association-div main-clone-sec">
                                    {{-- Previous projects --}}
                                    @include('web.auth.charity-register.prev_projects')
                                    
                                </div>

                                <input type="hidden" id="tel_url" value="{{asset('web/js/utils.js')}}">

                                <div class="form-group">
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" id="check_5" name="agree" required>
                                        <label for="check_5" class="dark-text">{{__('web.agree_terms_text')}} 
                                                <a href="{{route('web.pages', 'charity_contract')}}"
                                                class="first_color_hover dark_second_color decorate_link"
                                                target="_blank">{{__('web.charity_contract')}}
                                                </a>
                                        </label>
                                    </div>
                                </div>                                

                                <div class="error text-center" id="general_error"></div>
                                <div class="col-12" data-aos="fade-in">
                                    <div class="text-center btn-div">
                                        <button type="button" class="custom-btn sm-raduis white-btn submit_button">
                                            <span id="loading_image">{{__('web.join')}} </span>
                                        </button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end login-pg-->

   
    </div>

@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/date.js')}}"></script>
    
    @if(\App::getLocale() == 'ar')
        <!--for arabic-only-->
        <script type="text/javascript" src="{{asset('web/js/ar-date.js')}}"></script>
        <!--end-->
    @endif

    <script type="text/javascript" src="{{asset('web/js/intlTelInput.min.js')}}"></script>

    <script type="text/javascript" src="{{asset('web/js/association.js')}}"></script>
    
    <script>
        // clone div
        $(".copy-chart").click(function(){

            var images_counter = parseInt($('#images_counter').val());
            images_counter ++; 
            if(images_counter < 7){

                $('#images_counter').val(images_counter);

                var files_counter = parseInt($('#files_counter').val());

                var append_dt = `<div class="clone-div">
                                    <div class="row align-items-end">
                                    <div class="col-12" data-aos="fade-in">
                                        <h3 class="first_color">`+"{{__('web.add_another_project')}}"+` :
                                        <i class="fa fa-times-circle first_color remove-chart tooltip-link second_color_hover" data-tippy-placement="top" title="حذف الجمعيه "></i>
                                        </h3>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="projects_types[]">
                                            <label class="moving-label">`+"{{__('web.project_name')}}"+`</label>
                                        </div>
                                    </div>
                                    `+"<div class='error error-projects_types'>{{ $errors->first('projects_types') }}</div>"+`

                                    <div class="col-12" data-aos="fade-in">
                                        <div class="form-group">
                                            <input type="text" name="projects_names[]" class="form-control">
                                            <div class="error error-projects_names">{{ $errors->first('projects_names') }}</div>
                                            <label class="moving-label">{{ __('web.project_type') }}</label>
                                        </div>
                                    </div>

                                    `+"<div class='error error-projects_names'>{{ $errors->first('projects_names') }}</div>"+`

                                    <div class="col-12" data-aos="fade-in">
                                    <h4>{{__('web.max_images_text')}}</h4>
                                    <div class="row align-items-end files-show">
                                        <div class="col-lg-2 col-sm-4 col-6">
                                                <div class="form-group">
                                                    <div class="custom-file-div single-file">
                                                        <input type="file" class="hidden-input" name="images_`+images_counter+`[]" id="files_`+(files_counter+1)+`" accept=" .jpg, .png,.jpeg" />
                                                        <label for="files_`+(files_counter+1)+`"><img src="`+"{{asset('web/images/main/file.png')}}"+`" alt="`+"{{__('web.add_image')}}"+`"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-sm-4 col-6">
                                                <div class="form-group">
                                                    <div class="custom-file-div single-file">
                                                        <input type="file" class="hidden-input" name="images_`+images_counter+`[]" id="files_`+(files_counter+2)+`" accept=" .jpg, .png,.jpeg" />
                                                        <label for="files_`+(files_counter+2)+`"><img src="`+"{{asset('web/images/main/file.png')}}"+`" alt="`+"{{__('web.add_image')}}"+`"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-sm-4 col-6">
                                                <div class="form-group">
                                                    <div class="custom-file-div single-file">
                                                        <input type="file" class="hidden-input" name="images_`+images_counter+`[]" id="files_`+(files_counter+3)+`" accept=" .jpg, .png,.jpeg" />
                                                        <label for="files_`+(files_counter+3)+`"><img src="`+"{{asset('web/images/main/file.png')}}"+`" alt="`+"{{__('web.add_image')}}"+`"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-sm-4 col-6">
                                                <div class="form-group">
                                                    <div class="custom-file-div single-file">
                                                        <input type="file" class="hidden-input" name="images_`+images_counter+`[]" id="files_`+(files_counter+4)+`" accept=" .jpg, .png,.jpeg" />
                                                        <label for="files_`+(files_counter+4)+`"><img src="`+"{{asset('web/images/main/file.png')}}"+`" alt="`+"{{__('web.add_image')}}"+`"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-sm-4 col-6">
                                                <div class="form-group">
                                                    <div class="custom-file-div single-file">
                                                        <input type="file" class="hidden-input" name="images_`+images_counter+`[]" id="files_`+(files_counter+5)+`" accept=" .jpg, .png,.jpeg" />
                                                        <label for="files_`+(files_counter+5)+`"><img src="`+"{{asset('web/images/main/file.png')}}"+`" alt="`+"{{__('web.add_image')}}"+`"></label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-2 col-sm-4 col-6">
                                                <div class="form-group">
                                                    <div class="custom-file-div single-file">
                                                        <input type="file" class="hidden-input" name="images_`+images_counter+`[]" id="files_`+(files_counter+6)+`" accept=" .jpg, .png,.jpeg" />
                                                        <label for="files_`+(files_counter+6)+`"><img src="`+"{{asset('web/images/main/file.png')}}"+`" alt="`+"{{__('web.add_image')}}"+`"></label>
                                                    </div>
                                                </div>
                                            </div>


                                            `+"<div class='error error-images'>{{ $errors->first('images') }}</div>"+`
                                        </div>

                                    </div>
                                </div>`;
                $(".main-clone-sec").append(append_dt);
                $('.clone-div .select-input').select2({
                    theme: 'bootstrap4',
                    language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',
                    width: '100%'
                
                });

                $('#files_counter').val(files_counter+6);
            }

        });
    </script>
@endpush