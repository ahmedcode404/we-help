<div class="row align-items-end">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color">{{ __('web.bank_account_information') }}:</h3>
    </div>


    <!--<div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="Certified_Bank" required>
            <label class="moving-label">{{ __('web.certified_bank_account') }}</label>
        </div>
    </div>-->

    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="bank_id" required>
                <option selected disabled></option>
                @foreach ($data['banks'] as $bank)
                    <option value="{{ $bank->id }}" @if (old('bank_id') == $bank->id) selected @endif>{{ $bank->name }}</option>
                @endforeach
            </select>
            <label class="moving-label">{{ __('web.bank_name') }}</label>
        </div>
    </div>
    <div class="error error-bank_id">{{ $errors->first('bank_id') }}</div>

    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="tel" class="form-control" name="account_number" value="{{ old('account_number') }}" required>
            <label class="moving-label">{{ __('web.account_number') }} ( {{ __('web.account_number') }} )</label>
        </div>
    </div>
    <div class="error">{{ $errors->first('account_number') }}</div>

    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="iban" value="{{ old('iban') }}" required>
            <label class="moving-label">{{ __('web.iban') }} ( {{ __('web.iban') }} )</label>
        </div>
    </div>
    <div class="error">{{ $errors->first('iban') }}</div>


    {{-- <div class="col-lg-12">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="bank_adress" value="{{old('bank_adress')}}" required>
            <label class="moving-label">{{__('web.bank_adress')}}</label>
        </div>
    </div>
    <div class="error">{{ $errors->first('bank_adress') }}</div> --}}

    {{-- <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="bank_city_id" required>
                <option selected disabled></option>
                <option>city 1</option>
                <option>city 2</option>
            </select>
            <label class="moving-label">{{__('web.city')}}</label>
        </div>
    </div> --}}


    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="swift_code" value="{{ old('swift_code') }}" required>
            <label class="moving-label">{{ __('web.swift_code') }}</label>
        </div>
    </div>
    <div class="error">{{ $errors->first('swift_code') }}</div>


    {{-- <div class="col-6">
        <div class="form-group phone-input" data-aos="fade-in">
            <input type="tel" name="bank_phone" id="phone2" value="{{old('bank_phone')}}" class="form-control"
                minlength="9" maxlength="14" placeholder="{{__('web.phone_number')}}" required>
        </div>
    </div>
    <div class="error">{{ $errors->first('bank_phone') }}</div> --}}
</div>
