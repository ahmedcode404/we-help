<div class="row align-items-end">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color"> {{ __('web.association_address') }}:</h3>
    </div>

    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="address_en" value="{{ old('address_en') }}" required>
            <div class="error error-address_en"></div>
            <label class="moving-label">{{ __('web.address_en') }}</label>
        </div>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="address_ar" value="{{ old('address_ar') }}">
            <div class="error error-address_ar"></div>
            <label class="moving-label">{{ __('web.address_ar') }}</label>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="country" id="country" required>
                <option selected  disabled></option>
                @foreach ($data['countries'] as $key => $country)
                    <option value="{{ $key }}" @if (old('coutnry') == $key) selected @endif>
                        {{ $country }}</option>
                @endforeach
            </select>
            <div class="error error-country"></div>

            <label class="moving-label">{{ __('web.country') }}</label>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" name="city" id="city" value="{{old('city')}}" class="form-control" required>
            {{-- <select class="form-control select-input" name="city" id="city">
                <option selected disabled></option>
            </select> --}}
            <div class="error error-city"></div>
            <label class="moving-label">{{ __('web.city') }}</label>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="form-group phone-input" data-aos="fade-in">
            <input type="tel" name="phone" value="{{ session('charity_phone') ? session('charity_phone') : old('phone') }}" id="phone" class="form-control"
                minlength="9" maxlength="14" placeholder="{{ __('web.phone') }}" required>
            <div class="error error-phone"></div>
        </div>
    </div>



    <div class="col-lg-6">
        <div class="form-group phone-input" data-aos="fade-in">
            <input type="tel" name="mobile" value="{{ session('charity_mobile') ? session('charity_mobile') : old('mobile') }}" id="mobile" class="form-control"
                minlength="9" maxlength="14" placeholder="{{ __('web.mobile') }}" required>
            <div class="error error-mobile"></div>
        </div>
    </div>



    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="email" class="form-control" value="{{ old('email') }}" name="email" required>
            <div class="error"></div>
            <label class="moving-label">{{ __('web.email') }}</label>
        </div>
    </div>

</div>
