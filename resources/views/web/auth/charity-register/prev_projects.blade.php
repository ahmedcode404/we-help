<div class="row align-items-end">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color">{{ __('web.photos_of_previous_projects') }} :<i
                class="fa fa-paste first_color copy-chart tooltip-link second_color_hover" data-tippy-placement="top"
                title="{{ __('web.add_another_project') }}"></i></h3>
    </div>
</div>

<div class="main-clone-div">
    <div class="row align-items-end">
        <div class="col-12" data-aos="fade-in">
            <div class="form-group">
                <input type="text" class="form-control" name="projects_types[]">
                <div class="error error-projects_types">{{ $errors->first('projects_types') }}</div>
                <label class="moving-label">{{ __('web.project_name') }}</label>
            </div>
        </div>


        <div class="col-12" data-aos="fade-in">
            <div class="form-group">
                <input type="text" name="projects_names[]" class="form-control">
                <div class="error error-projects_names">{{ $errors->first('projects_names') }}</div>
                <label class="moving-label">{{ __('web.project_type') }}</label>
            </div>
        </div>

        {{-- <div class="col-12" data-aos="fade-in">
            <div class="form-group">
                <select name="projects_names[]" class="form-control select-input" required>
                    <option value="health" @if (old('project_name') == 'health') selected @endif>{{ __('admin.health') }}</option>
                    <option value="medical" @if (old('project_name') == 'medical') selected @endif>{{ __('admin.medical') }}</option>
                    <option value="drilling" @if (old('project_name') == 'drilling') selected @endif>{{ __('admin.drilling') }}</option>
                    <option value="sponsoring" @if (old('project_name') == 'sponsoring') selected @endif>{{ __('admin.sponsoring') }}</option>
                </select>
                <div class="error">{{ $errors->first('projects_names') }}</div>
                <label class="moving-label">{{ __('web.project_type') }}</label>
            </div>
        </div> --}}


        <input type="hidden" id="images_counter" value="1">

        <div class="col-12" data-aos="fade-in">
            <h4>{{ __('web.max_images_text') }} <small>(Max size: 20MB)</small></h4>
            
            <div class="row align-items-end files-show">
                <div class="col-lg-2 col-sm-4 col-6">
                    <div class="form-group">
                        <div class="custom-file-div single-file">
                            <input type="file" class="hidden-input" name="images_1[]" id="files_1"
                                accept=" .jpg, .png,.jpeg" />
                            <label for="files_1"><img src="{{ asset('web/images/main/file.png') }}"
                                    alt="{{ __('web.add_image') }}"></label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-4 col-6">
                    <div class="form-group">
                        <div class="custom-file-div single-file">
                            <input type="file" class="hidden-input" name="images_1[]" id="files_2"
                                accept=" .jpg, .png,.jpeg" />
                            <label for="files_2"><img src="{{ asset('web/images/main/file.png') }}"
                                    alt="{{ __('web.add_image') }}"></label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-4 col-6">
                    <div class="form-group">
                        <div class="custom-file-div single-file">
                            <input type="file" class="hidden-input" name="images_1[]" id="files_3"
                                accept=" .jpg, .png,.jpeg" />
                            <label for="files_3"><img src="{{ asset('web/images/main/file.png') }}"
                                    alt="{{ __('web.add_image') }}"></label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-4 col-6">
                    <div class="form-group">
                        <div class="custom-file-div single-file">
                            <input type="file" class="hidden-input" name="images_1[]" id="files_4"
                                accept=" .jpg, .png,.jpeg" />
                            <label for="files_4"><img src="{{ asset('web/images/main/file.png') }}"
                                    alt="{{ __('web.add_image') }}"></label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-4 col-6">
                    <div class="form-group">
                        <div class="custom-file-div single-file">
                            <input type="file" class="hidden-input" name="images_1[]" id="files_5"
                                accept=" .jpg, .png,.jpeg" />
                            <label for="files_5"><img src="{{ asset('web/images/main/file.png') }}"
                                    alt="{{ __('web.add_image') }}"></label>
                        </div>
                    </div>
                </div>

                <div class="col-lg-2 col-sm-4 col-6">
                    <div class="form-group">
                        <div class="custom-file-div single-file">
                            <input type="file" class="hidden-input" name="images_1[]" id="files_6"
                                accept=" .jpg, .png,.jpeg" />
                            <label for="files_6"><img src="{{ asset('web/images/main/file.png') }}"
                                    alt="{{ __('web.add_image') }}"></label>
                        </div>
                    </div>
                </div>

                {{-- <div class="col-md-3 col-6">
                    <div class="form-group">
                        <div class="custom-file-div multi-file">
                            <input type="file" class="hidden-input" name="images_1[]" multiple id="files_4"
                                accept=" .jpg, .png,.jpeg" />
                            <label for="files_4" class="row no-marg-row align-items-center hirozintal-scroll">
                                <img src="{{ asset('web/images/main/add.png') }}" alt="{{ __('web.add_image') }}">
                                <div class="col-12 images-multi"></div>
                            </label>
                        </div>
                    </div>
                </div> --}}


                <input type="hidden" id="files_counter" value="6">
            </div>
            <div class="error error-images">{{ $errors->first('images') }}</div>
            <div class="error error-images_1">{{ $errors->first('images_1') }}</div>
        </div>
    </div>

</div>
