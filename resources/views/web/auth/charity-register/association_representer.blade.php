<div class="row">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color">{{ __('web.Representative_of_the_authority') }}:</h3>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="representer_name_en" value="{{ old('representer_name_en') }}"
                required>
            <div class="error error-representer_name_en">{{ $errors->first('representer_name_en') }}</div>
            <label class="moving-label">{{ __('web.representative_name_en') }}</label>
        </div>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="representer_name_ar" value="{{ old('representer_name_ar') }}">
            <div class="error error-representer_name_ar">{{ $errors->first('representer_name_ar') }}</div>
            <label class="moving-label">{{ __('web.representative_name_ar') }}</label>
        </div>
    </div>



    <div class="col-lg-6">
        <div class="form-group input-has-icon single-file" data-aos="fade-in">
            <i class="far fa-image second_color"></i>
            <input type="file" class="form-control hidden-input" accept=".png,.jpg,.jpeg,.pdf" id="file_1"
                name="representer_passport_image" required>
            <label class="moving-label">{{ __('web.passport_photo') }}</label>
            <label for="file_1" class="file-label form-control"></label>
            <small>(Max size: 50MB)</small>
            <div class="error error-representer_passport_image">{{ $errors->first('representer_passport_image') }}</div>
        </div>
    </div>



    <div class="col-lg-6">
        <div class="form-group input-has-icon single-file" data-aos="fade-in">
            <i class="far fa-image second_color"></i>
            <input type="file" class="form-control hidden-input" accept=".png,.jpg,.jpeg,.pdf" id="file_2"
                name="representer_nation_image" required>
            <label class="moving-label">{{ __('web.national_identity_photo') }}</label>
            <label for="file_2" class="file-label form-control"></label>
            <small>(Max size: 50MB)</small>
            <div class="error error-representer_nation_image">{{ $errors->first('representer_nation_image') }}</div>
        </div>
    </div>




    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="representer_title_en"
                value="{{ old('representer_title_en') }}" required>
            <div class="error error-representer_title_en">{{ $errors->first('representer_title_en') }}</div>
            <label class="moving-label">{{ __('web.representative_title_en') }}</label>
        </div>
    </div>



    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="representer_title_ar"
                value="{{ old('representer_title_ar') }}">
            <div class="error error-representer_title_ar">{{ $errors->first('representer_title_ar') }}</div>
            <label class="moving-label">{{ __('web.representative_title_ar') }}</label>
        </div>
    </div>


    <div class="col-12">
        <div class="second_color">{{ __('web.representative_text') }}</div>
        <br>
        <div class="row align-items-end">
            <div class="col-lg-4">
                <div class="form-group" data-aos="fade-in">

                    <div class="error error-international_member">{{ $errors->first('international_member') }}</div>
                    <div class="inline-checkboxes">
                        <div class="custom-checkbox">
                            <!--don`t change id-->
                            <input type="radio" value="1" name="international_member" @if (old('international_member') == 'on') checked @endif
                                class="hidden-input" id="yes_check" checked>
                            <label for="yes_check" class="dark-text"> {{ __('web.yes') }}</label>
                        </div>

                        <div class="custom-checkbox">
                            <input type="radio" name="international_member" @if (old('international_member') == 'on') checked @endif
                                class="hidden-input" id="no_check">
                            <label for="no_check" class="dark-text"> {{ __('web.no') }}</label>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-lg-4 organization-div">
                <div class="form-group">
                    <!--don`t change name-->
                    <input type="text" class="form-control" name="international_name_en"
                        value="{{ old('international_name_en') }}">
                    <div class="error error-international_name_en">{{ $errors->first('international_name_en') }}</div>
                    <label class="moving-label">{{ __('web.international_name_en') }}</label>
                </div>
            </div>



            <div class="col-lg-4 organization-div">
                <div class="form-group">
                    <!--don`t change name-->
                    <input type="text" class="form-control" name="international_name_ar"
                        value="{{ old('international_name_ar') }}">
                    <div class="error error-international_name_ar">{{ $errors->first('international_name_ar') }}</div>
                    <label class="moving-label">{{ __('web.international_name_ar') }}</label>
                </div>
            </div>


        </div>

    </div>

    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="email" class="form-control" name="representer_email" value="{{ old('representer_email') }}"
                required>
            <div class="error error-representer_email">{{ $errors->first('representer_email') }}</div>

            <label class="moving-label">{{ __('web.email') }}</label>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="twitter_link" value="{{ old('twitter_link') }}">
            <div class="error error-twitter_link">{{ $errors->first('twitter_link') }}</div>

            <label class="moving-label">{{ __('web.twitter_account') }}</label>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="facebook_link" value="{{ old('facebook_link') }}">
            <div class="error error-facebook_link">{{ $errors->first('facebook_link') }}</div>

            <label class="moving-label">{{ __('web.facebook_account') }}</label>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="form-group input-has-icon" data-aos="fade-in">
            <i class="far fa-link second_color"></i>
            <input type="text" class="form-control" name="website" value="{{ old('website') }}">
            <div class="error error-website">{{ $errors->first('website') }}</div>
            <label class="moving-label">{{ __('web.website_url') }}</label>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="form-group input-has-icon single-file aos-init aos-animate" data-aos="fade-in">
            <i class="far fa-image second_color"></i>
            <input type="file" class="form-control hidden-input" accept=".png,.jpeg,.jpg,.pdf" id="file_12"
                name="representer_title_file" required>
            <label class="moving-label">{{ __('web.contract_upload') }}</label>
            <label for="file_12" class="file-label form-control"></label>
            <small>(Max size: 50MB)</small>
            <div class="error error-representer_title_file">{{ $errors->first('representer_title_file') }}</div>
        </div>
    </div>


</div>
