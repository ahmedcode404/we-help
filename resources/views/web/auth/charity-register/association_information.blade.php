<div class="row">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color"> {{ __('web.association_information') }} :</h3>
    </div>
    <div class="col-6">
        <div class="form-group" data-aos="fade-in">
            <select class="form-control select-input" name="type" required>
                <option selected disabled></option>
                <option value="foundation" @if (old('type') == 'foundation') selected @endif>{{ __('admin.foundation') }}</option>
                <option value="organization" @if (old('type') == 'organization') selected @endif>{{ __('admin.organization') }}</option>
                <option value="charity" @if (old('type') == 'charity') selected @endif>{{ __('admin.charity') }}</option>
            </select>
            <div class="error error-type">{{ $errors->first('type') }}</div>
            <label class="moving-label">{{ __('web.association_type') }}</label>
        </div>
    </div>




    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
            <div class="error error-name">{{ $errors->first('name') }}</div>
            <label class="moving-label">{{ __('web.association_name') }} </label>
        </div>
    </div>



    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <input type="number" step="1" min="0" class="form-control no_apperance_number" minlength="5" maxlength="12"
                value="{{ old('license_number') }}" name="license_number" required>
            <div class="error error-license_number">{{ $errors->first('license_number') }}</div>
            <label class="moving-label">{{ __('web.license_number') }}</label>
        </div>
    </div>



    <div class="col-lg-6">
        <div class="form-group input-has-icon" data-aos="fade-in">
            <i class="far fa-calendar-alt second_color"></i>
            <input type="text" class="form-control datepick" autocomplete="off" name="license_start_date"
                value="{{ old('license_start_date') }}" required>
            <div class="error error-license_start_date">{{ $errors->first('license_start_date') }}</div>
            <label class="moving-label">{{ __('web.license_date') }}</label>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="form-group input-has-icon single-file aos-init aos-animate" data-aos="fade-in">
            <i class="far fa-image second_color"></i>
            <input type="file" class="form-control hidden-input" accept=".png,.jpg,.jpeg,.pdf" id="file_22"
                value="{{ old('license_file') }}" name="license_file" required>
            <label class="moving-label">{{ __('web.license_upload') }}</label>
            <label for="file_22" class="file-label form-control"></label>
            <small>(Max size: 50 MB)</small>
            <div class="error error-license_file">{{ $errors->first('license_file') }}</div>
        </div>
    </div>



    <div class="col-lg-6">
        <div class="form-group input-has-icon" data-aos="fade-in">
            <i class="far fa-calendar-alt second_color"></i>
            <input type="text" class="form-control datepick" autocomplete="off" name="establish_date"
                value="{{ old('establish_date') }}" required>
            <div class="error error-establish_date">{{ $errors->first('establish_date') }}</div>
            <label class="moving-label">{{ __('web.founding_date') }}</label>
        </div>
    </div>




    <div class="col-lg-6">
        <div class="form-group input-has-icon" data-aos="fade-in">
            <i class="far fa-calendar-alt second_color"></i>
            <input type="text" class="form-control datepick" autocomplete="off" name="license_end_date"
                value="{{ old('license_end_date') }}" required>
            <div class="error error-license_end_date">{{ $errors->first('license_end_date') }}</div>
            <label class="moving-label">{{ __('web.completion_date') }}</label>
        </div>
    </div>

 
    <div class="col-lg-6">
        <div class="form-group input-has-icon" data-aos="fade-in">
            <i class="far fa-calendar-alt second_color"></i>
            <input type="text" class="form-control" autocomplete="off" name="contract_date"
                value="{{ now()->format('m/d/Y') }}" required readonly>
            <div class="error error-license_end_date">{{ $errors->first('license_end_date') }}</div>
        <label class="moving-label">{{ __('web.contract_date') }}</label>
        </div>
    </div>



    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <input type="number" step="1" class="form-control" min="0" name="branches_num"
                value="{{ old('branches_num') }}" required>
            <div class="error error-branches_num">{{ $errors->first('branches_num') }}</div>
            <label class="moving-label">{{ __('web.number_of_branches') }}</label>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="form-group" data-aos="fade-in">
            <input type="number" step="1" class="form-control" min="1" name="members_num"
                value="{{ old('members_num') }}" required>
            <div class="error error-members_num">{{ $errors->first('members_num') }}</div>
            <label class="moving-label">{{ __('web.number_of_members') }}</label>
        </div>
    </div>


</div>
