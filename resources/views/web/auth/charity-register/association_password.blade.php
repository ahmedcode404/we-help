<div class="row align-items-end">
    <div class="col-12" data-aos="fade-in">
        <h3 class="first_color">{{ __('web.password') }}:</h3>
    </div>


    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="password" class="form-control" name="password" id="password_id" required>
            <div class="error error-password">{{ $errors->first('password') }}</div>
            <label class="moving-label">{{ __('web.password') }}</label>
        </div>
    </div>


    <div class="col-12">
        <div class="form-group" data-aos="fade-in">
            <input type="password" class="form-control" name="password_confirmation" required>
            <div class="error error-password">{{ $errors->first('password') }}</div>
            <label class="moving-label">{{ __('web.password_confirm') }}</label>
        </div>
    </div>


</div>
