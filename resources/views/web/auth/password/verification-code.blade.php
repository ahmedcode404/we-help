@extends('web.layout.app')

@section('title', __('web.verification_code'))
@section('description', __('web.verification_code'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

    <!-- start pages-header
         ================ -->
    <section class="pages-header text-center" style="background-image:url({{asset('web/images/pages-bg/7.png')}})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{__("web.verification_code")}}</h1>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{__("web.verification_code")}}</h2>
        </div> --}}


        <!-- start login-pg
         ================ -->
        <section class="margin-div login-pg">
            <div class="login-width">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title first_color" data-aos="fade-in">
                                <div class="side-img"><img src="{{asset('web/images/icons/password.png')}}" alt="icon"></div>
                                <h3 class="second_color text-cap bold-text">{{__("web.verification_code")}}</h3>
                                    {{__("web.verification_code_text")}}:
                            </div>

                            <form action="{{route('web.verify')}}" method="POST" class="gray-form login-form" data-aos="fade-in">
                                @csrf

                                <div class="form-group">
                                    <div class="verfication-phone text-center" dir="ltr">
                                        <input type="text" class="numb-input form-control" name="code[]" maxlength="1"
                                            size="1" min="0" max="9" pattern="[0-9]{1}">

                                        <input type="text" class="numb-input form-control" name="code[]" maxlength="1"
                                            size="1" min="0" max="9" pattern="[0-9]{1}">

                                        <input type="text" class="numb-input form-control" name="code[]" maxlength="1"
                                            size="1" min="0" max="9" pattern="[0-9]{1}">

                                        <input type="text" class="numb-input form-control" name="code[]" maxlength="1"
                                            size="1" min="0" max="9" pattern="[0-9]{1}">

                                        <input type="hidden" name="email" value="{{$email}}">
                                        <input type="hidden" name="role" value="{{$role}}">
                                        <input type="hidden" name="name" value="{{$name}}">

                                    </div>
                                </div>

                                <div class="text-center btn-div">
                                    <button type="submit"
                                        class="custom-btn sm-raduis white-btn"><span>{{__("web.confirm")}}</span></button>
                                </div>

                                {{-- {{dd($user)}} --}}
                                <div class="form-group create-accout-div text-center dark_second_color">
                                    <a href="{{route('web.resend-code', ['email' => $email, 'name' => $name, 'role' => $role])}}"
                                        class="decorate_link first_color_hover dark_second_color text-cap">{{__('web.resend_code')}}
                                        <i class="fa fa-undo"></i></a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end login-pg-->
       
    </div>

@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/login.js')}}"></script>
@endpush