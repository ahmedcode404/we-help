@extends('web.layout.app')

@section('title', __('web.change_password'))
@section('description', __('web.change_password'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

    <!-- start pages-header
        ================ -->
    <section class="pages-header text-center" style="background-image:url({{asset('web/images/pages-bg/7.png')}})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{__("web.change_password")}}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{__("web.change_password")}}</h2>
        </div> --}}


        <!-- start login-pg
         ================ -->
        <section class="margin-div login-pg">
            <div class="login-width">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title first_color" data-aos="fade-in">
                                <div class="side-img"><img src="{{asset('web/images/icons/lock.png')}}" alt="icon"></div>
                                <h3 class="second_color text-cap bold-text">{{__("web.change_password")}}</h3>
                                {{__('web.change_password_text')}}:
                            </div>

                            <form action="{{route('web.change-password')}}" method="POST" class="gray-form login-form" data-aos="fade-in">

                                @csrf

                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" required>
                                    <label class="moving-label">{{__('web.password')}}</label>
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" name="password_confirmation" required>
                                    <label class="moving-label">{{__("web.password_confirm")}}</label>
                                </div>

                                <input type="hidden" name="email" value="{{$email}}">
                                <input type="hidden" name="role" value="{{$role}}">

                                <div class="text-center btn-div">
                                    <button type="submit"
                                        class="custom-btn sm-raduis white-btn"><span>{{__('web.save')}}</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end login-pg-->
     
    </div>

@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/login.js')}}"></script>
@endpush