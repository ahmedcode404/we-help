<div class="modal fade" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="login-mod" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

            <div class="modal-body text-center">
                <div class="modal-header col-12">
                    <h5 class="modal-title white-text text-cap" id="login-mod">{{__('web.login_as')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="login-pop-div  text-cap row justify-content-center text-center">

                    <div class="form-group col-6">
                        <a href="{{route('admin.login', 'donor')}}" class="first_color second_color_hover">
                            <div class="side-img"><img src="{{asset('web/images/icons/login.png')}}" alt="icon"></div>{{__('web.donor')}}
                        </a>
                    </div>
                    
                    <div class="form-group col-6">
                        <a href="{{route('admin.login', 'association')}}" class="first_color second_color_hover">
                            <div class="side-img"><img src="{{asset('web/images/icons/association.png')}}" alt="icon"></div>
                            {{__('web.association')}}
                        </a>
                    </div>

                    {{-- <div class="form-group col-4">
                        <a href="{{route('admin.login', 'admin')}}" class="first_color second_color_hover">
                            <div class="side-img"><img src="{{asset('web/images/icons/employee.png')}}" alt="icon"></div>{{__('web.employee')}}
                        </a>
                    </div> --}}

                </div>
            </div>
        </div>
    </div>
</div>