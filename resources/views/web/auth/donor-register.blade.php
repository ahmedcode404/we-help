@extends('web.layout.app')

@section('title', __('web.register'))
@section('description', __('web.register'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

    <!-- start pages-header
        ================ -->
    <section class="pages-header text-center" style="background-image:url({{asset('web/images/pages-bg/7.png')}})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{__('web.donor_register')}}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{__('web.donor_register')}}</h2>
        </div> --}}


        <!-- start login-pg
         ================ -->
        <section class="margin-div login-pg">
            <div class="login-width">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title first_color" data-aos="fade-in">
                                <div class="side-img"><img src="{{asset('web/images/icons/login.png')}}" alt="donor"></div>
                                <h3 class="second_color text-cap bold-text">{{__('web.donor_register')}}</h3>
                                {{__('web.donor_register_text')}}:
                            </div>

                            <form action="{{route('web.register-donor')}}" method="POST" class="gray-form login-form submit_form" data-aos="fade-in">
                                @csrf

                                <div class="form-group">    
                                    <input type="text" class="form-control" name="name" value="{{old('name')}}" required>
                                    <div class="error error-name">{{ $errors->first('name') }}</div>

                                    <label class="moving-label">{{__("web.full_name")}}</label>
                                </div>

                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" value="{{old('email')}}" required>
                                    <div class="error error-email">{{ $errors->first('email') }}</div>

                                    <label class="moving-label">{{__('web.email')}}</label>
                                </div>

                                <div class="form-group phone-input" data-aos="fade-in">
                                    <input type="tel" name="phone_num" value="{{session('donor_phone') ? session('donor_phone') : old('phone_num')}}" id="phone" class="form-control" minlength="9" maxlength="14" placeholder="{{__('web.phone')}}" required>

                                    <div class="error error-phone">{{ $errors->first('phone') }}</div>
                                    <div class="error error-phone_num">{{ $errors->first('phone') }}</div>

                                    <!--<label class="moving-label">{{__('web.phone')}}</label>-->
                                </div>
                                

                                <div class="form-group">
                                    <select class="form-control select-input" name="country" required>
                                        <option selected disabled></option>
                                        @foreach ($data['countries'] as $key => $country)
                                            <option value="{{$key}}" @if(old('country') == $key) selected @endif>{{$country}}</option>
                                        @endforeach
                                    </select>
                                    <div class="error error-country">{{ $errors->first('country') }}</div>
                                    <label class="moving-label">{{__('web.country')}}</label>
                                </div>

                                {{-- <div class="form-group">
                                    <select class="form-control select-input" name="country_id" required>
                                        <option selected disabled></option>
                                        @foreach ($data['countries'] as $country)
                                            @if($country->parent_id == null)
                                                <option value="{{$country->id}}" @if(old('country_id') == $country->id) selected @endif>{{$country->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                    <div class="error">{{ $errors->first('country_id') }}</div>
                                    <label class="moving-label">{{__('web.country')}}</label>
                                </div> --}}


                                <div class="form-group">
                                    <input type="password" class="form-control" id="password" name="password" required>
                                    <div class="error error-password">{{ $errors->first('password') }}</div>

                                    <label class="moving-label">{{__('web.password')}}</label>
                                </div>
                                
                               
                                <div class="form-group">
                                    <input type="password" class="form-control" name="password_confirmation" required>
                                    <div class="error error-password">{{ $errors->first('password') }}</div>
                                    <label class="moving-label">{{__('web.password_confirm')}}</label>
                                </div>


                                <div class="form-group">
                                    <div class="custom-checkbox">
                                        <input type="checkbox" class="hidden-input" id="check_5" name="agree" required>
                                        <label for="check_5" class="dark-text">{{__('web.agree_terms_text')}} <a href="{{route('web.pages', 'terms_and_conditions')}}"
                                                class="first_color_hover dark_second_color decorate_link"
                                                target="_blank">{{__('web.terms_and_conditions')}}</a> {{__('web.and')}} <a
                                                href="{{route('web.pages', 'privacy_policy')}}"
                                                class="first_color_hover dark_second_color decorate_link"
                                                target="_blank">{{__("web.privacy_policy")}}</a>.
                                        </label>
                                    </div>
                                </div>

                                <div class="text-center btn-div">
                                    <button type="button"
                                        class="custom-btn sm-raduis white-btn submit_button"><span>{{__('web.register')}}</span></button>
                                </div>

                                <div class="form-group create-accout-div text-center dark_second_color">
                                    {{__('web.already_have_account')}}
                                    <a href="{{route('admin.login', 'donor')}}"
                                        class="decorate_link first_color_hover dark_second_color text-cap">{{__('web.login_now')}}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end login-pg-->

    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/intlTelInput.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/register.js')}}"></script>
@endpush