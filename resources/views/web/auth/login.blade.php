@extends('web.layout.app')

@section('title', __('web.login'))
@section('description', __('web.login'))
@section('image', asset('web/images/main/white-logo.png'))

@section('content')

    <!-- start pages-header
            ================ -->
    <section class="pages-header text-center" style="background-image:url({{asset('web/images/pages-bg/7.png')}})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">
                            @if($role == 'admin')
                                {{__('web.emp_login')}}
                            @elseif($role == 'donor')
                                {{__('web.donor_login')}}
                            @else   
                                {{__('web.association_login')}}
                            @endif
                        </h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">
                @if($role == 'admin')
                    {{__('web.emp_login')}}
                @elseif($role == 'donor')
                    {{__('web.donor_login')}}
                @else   
                    {{__('web.association_login')}}
                @endif
            </h2>
        </div> --}}


        <!-- start login-pg
         ================ -->
        <section class="margin-div login-pg">
            <div class="login-width">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="section-title first_color" data-aos="fade-in">
                                <div class="side-img">
                                    @if($role == 'admin')
                                        <img src="{{asset('web/images/icons/employee.png')}}" alt="employee">
                                    @elseif($role == 'donor')
                                        <img src="{{asset('web/images/icons/login.png')}}" alt="donor">
                                    @else   
                                        <img src="{{asset('web/images/icons/association.png')}}" alt="association">
                                    @endif
                                    
                                </div>
                                <h3 class="second_color text-cap bold-text">
                                    @if($role == 'admin')
                                        {{__('web.emp_login')}}
                                    @elseif($role == 'donor')
                                        {{__('web.donor_login')}}
                                    @else   
                                        {{__('web.association_login')}}
                                    @endif
                                </h3>

                                @if($role == 'admin')
                                    {{__('web.emp_login_text')}}:
                                @elseif($role == 'donor')
                                    {{__('web.donor_login_text')}}:
                                @else   
                                    {{__('web.association_login_text')}}:
                                @endif

                            </div>

                            <form action="{{route('login.auth')}}" method="POST" class="gray-form login-form" data-aos="fade-in">
                                @csrf

                                <input type="hidden" name="role" value="{{$role}}">

                                {{-- All users login with email and password --}}
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" required>
                                    <label class="moving-label">{{__('web.email')}}</label>
                                </div>
                                @if($errors->has('email'))
                                    <div class="error">{{ $errors->first('email') }}</div>
                                @endif

                                <div class="form-group">
                                    <input type="password" class="form-control" name="password" required>
                                    <label class="moving-label">{{__("web.password")}}</label>
                                </div>
                                @if($errors->has('password'))
                                    <div class="error">{{ $errors->first('password') }}</div>
                                @endif

                                <div class="form-group">
                                    <a href="{{route('web.get-email-form', $role)}}"
                                        class="decorate_link first_color_hover dark_second_color">{{__('web.forget_password')}}</a>
                                </div>
                                <div class="text-center btn-div">
                                    <button type="submit"
                                        class="custom-btn sm-raduis white-btn"><span>{{__('web.login')}}</span></button>
                                </div>

                                {{-- empolyee can not register --}}
                                @if($role != 'admin')
                                    <div class="form-group create-accout-div text-center dark_second_color">
                                        {{__('web.do_not_have_account')}}
                                        <a href="{{route('web.get-register-form', $role)}}"
                                            class="decorate_link first_color_hover dark_second_color text-cap">{{__('web.register_now')}}</a>
                                    </div>
                                @endif

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--end login-pg-->

    </div>

      

@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/login.js')}}"></script>
@endpush