@extends('web.layout.app')

@section('title', __('web.Statistics') )
@section('description', __('web.Statistics') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')


<!-- BEDIN: header -->

@include('web.statistics.header')

<!-- END: header -->




<!-- start statistics-sec
    ================ -->
    <section class="statistics-sec has_seudo margin-div statistics-pg">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section-title first_color" data-aos="fade-in">
                    <div class="side-img"><img src="{{ url('web/images/icons/menu/4.png') }}" alt="icon"></div>
                    <h3 class="second_color text-cap bold-text">{{ __('web.Statistics') }}</h3>
                    <h4 class="text-upper bold-text">{{ __('web.Statistics') }}</h4>
                </div>
            </div>
        </div>

        <div class="row justify-content-center text-center first_color text-cap" id="counter">
            <!--start count-div-->
            <div class="col-sm-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{ url('web/images/icons/statistics/1.png') }}" alt="icon">
                    <h3>{{ __('web.projects') }}</h3>
                    <div class="counter-value big-raduis" data-count="{{ $project_count }}"><span>{{ $project_count }}</span></div>
                    <p>{!! $text_project->value !!}</p>
                </div>
            </div>
            <!--end count-div-->

            <!--start count-div-->
            <div class="col-sm-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{ url('web/images/icons/statistics/2.png') }}" alt="icon">
                    <h3>{{ __('web.partners') }}</h3>
                    <div class="counter-value big-raduis" data-count="{{ $partener_count }}"><span>{{ $partener_count }}</span></div>
                    <p>{!! $text_partener->value !!}</p>
                </div>
            </div>
            <!--end count-div-->


            <!--start count-div-->
            <div class="col-sm-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{ url('web/images/icons/statistics/3.png') }}" alt="icon">
                    <h3>{{ __('web.donated_amounts') }}</h3>
                    <div class="counter-value big-raduis" data-count="{{ $total_count }}"><span>{{ $total_count }} </span> {{currencySymbol(session('currency'))}}</div>
                    <p>{!! $text_amount->value !!}</p>
                </div>
            </div>
            <!--end count-div-->


            <!--start count-div-->
            <div class="col-sm-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{ url('web/images/icons/statistics/4.png') }}" alt="icon">
                    <h3>{{ __('web.countries') }}</h3>
                    <div class="counter-value big-raduis" data-count="{{ $country_count->count() }}"><span>{{ $country_count->count() }}</span></div>
                    <p>{!! $text_country->value !!}</p>
                </div>
            </div>
            <!--end count-div-->


            <!--start count-div-->
            <div class="col-sm-6" data-aos="zoom-in">
                <div class="count-div">
                    <img src="{{ url('web/images/icons/statistics/5.png') }}" alt="icon">
                    <h3>{{ __('web.Beneficiaries') }}</h3>
                    <div class="counter-value big-raduis" data-count="{{ $benef_count }}"><span>{{ $benef_count }}</span></div>
                    <p>{!! $text_beneficiary->value !!}</p>
                </div>
            </div>
            <!--end count-div-->

        </div>
    </div>
</section>
<!--end statistics-sec-->



@endsection

@push('js')

    <!--for arabic-only-->
    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif


    <script type="text/javascript" src="{{ asset('web/js/anime.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/custom-pages.js') }}"></script>
    <script type="text/javascript" src="{{ asset('web/js/statistics.js') }}"></script>

@endpush