@extends('web.layout.app')

@section('title', __('web.Statistics') )
@section('description', __('web.Statistics') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')


    <!-- start pages-header
         ================ -->
         <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{ __('web.frequently_asked_questions') }}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{ __('web.frequently_asked_questions') }}</h2>
        </div> --}}
    <!-- start faq
        ================ -->
        <section class="faq-page margin-div">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title first_color" data-aos="fade-in">
                        <div class="side-img"><img src="{{ url('web/images/icons/faq.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ __('web.faq') }}</h3>
                            <h4 class="text-upper bold-text">{{ __('web.faq') }}</h4>
                        </div>
                    </div>
                </div>

                <div class="faqs-sec margin-top-div">
                    <div class="row">

                        <!--start col-->
                        @foreach($questions as $question)
                        <div class="col-lg-12" data-aos="fade-in">
                            <div class="faq-div">
                                <h3>
                                    {{ $question->question }}
                                    <i class="fa fa-long-arrow-alt-right"></i>
                                </h3>
                                <div class="slide-faq">
                                    {{ $question->answer }}
                                </div> 
                            </div>
                        </div>
                        @endforeach
                        <!--end col-->

                    </div>
                </div>
            </div>
        </section>
        <!--end faq-->

    </div>

@endsection

@push('js')

    <!--for this page only-->
    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif

    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <!--end-->
    <script type="text/javascript" src="{{ url('web/js/faq.js') }}"></script> 

@endpush