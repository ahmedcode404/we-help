@extends('web.layout.app')

@section('title', __('web.Statistics') )
@section('description', __('web.Statistics') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')


    <!-- start pages-header
         ================ -->
    <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{ $support->title }}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{ $support->title }}</h2>
        </div> --}}
    <!-- start about-sec
        ================ -->
        <section class="static-pages margin-div" data-aos="zoom-out">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12">
                    {!! $support->content !!}
                </div>


            </div>
        </div>
    </section>
    <!--end about-sec-->    

    </div>
@endsection

@push('js')

    <!--for this page only-->
    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif

    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <!--end-->

@endpush