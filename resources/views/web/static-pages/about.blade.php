@extends('web.layout.app')

@section('title', __('web.Statistics') )
@section('description', __('web.Statistics') )
@section('image', asset('web/images/main/white-logo.png'))

@section('content')


<!-- BEDIN: header -->

<!-- start pages-header
        ================ -->
        <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
    
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="pages-header-content line-title">
                            <span class="line"></span>
                            <h1 class="white-text bold-text text-upper">{{ __('web.about_us') }}</h1>
                        </div>
                    </div>
                </div>
            </div>

        </section>
<!--end pages-header-->

<!-- END: header -->



<div class="main-content">
    {{-- <div class="hidden-title">
        <h2 class="white-text bold-text text-upper text-center page-title">{{ __('web.about_us') }}</h2>
    </div> --}}
    <!-- start about-sec
        ================ -->
        <section class="about-sec margin-div">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/menu/5.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ $about->title }}</h3>
                            <h4 class="text-upper bold-text">{{ $about->title }}</h4>
                        </div>
                    </div>

                    <div class="col-lg-6 about-text-grid" data-aos="fade-in">
                        {!! $about->content !!}
                    </div>


                    <div class="col-lg-6 about-img-grid">
                        <img src="{{ url('storage/' . $about->image) }}" alt="img" data-aos="zoom-in">
                    </div>

                </div>
            </div>
        </section>
    <!--end about-sec-->

    <!-- start about-sec-2
    ================ -->
        <section class="about-sec-2 margin-div light_bg">
            <div class="container">
                <div class="row align-items-center">

                    <div class="col-lg-3 col-md-4 sm-center">
                        <img src="{{ url('storage/' . $about_corporation->image) }}" alt="img" data-aos="zoom-in">
                    </div>


                    <div class="col-lg-9 col-md-8 sm-center" data-aos="fade-in">
                        <h2 class="second_color bold-text text-cap">{{ $about_corporation->title }}
                        </h2>
                        {!! $about_corporation->content !!}
                    </div>

                </div>
            </div>
        </section>
    <!--end about-sec-2-->

    <!-- start about-slider
    ================ -->
        <section class="about-slider margin-div">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/menu/5.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ $vision_and_mission->title }}</h3>
                            <h4 class="text-upper bold-text">{{ $vision_and_mission->title }}</h4>
                        </div>

                        <div id="about-owl" class="owl-carousel owl-theme main-about-owl dark-dots" data-aos="fade-in">
                            
                            <!--start item-->
                            @foreach($vision_and_mission->parent as $vision)
                            <div class="item">
                                <div class="full-width-img main-about-item"
                                    style="background-image:url({{asset('storage/'.$vision_and_mission->image)}})">
                                    <div class="slider-about-caption text-center hirozintal-scroll">
                                        <div>
                                            <div class="line-title">
                                                <h3 class="second_color text-upper bold-text">{{ $vision->title }}</h3>
                                            </div>
                                            <p style="color: #333333;">
                                                {!! $vision->content !!}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end item-->
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!--end about-slider-->

    <!-- start about-sec-3
    ================ -->
        <section class="about-sec-3 margin-div">
            <div class="container">
                <div class="row align-items-center">

                    <div class="col-12" data-aos="fade-in">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/menu/5.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ $our_goal->title }}</h3>
                            <h4 class="text-upper bold-text">{{ $our_goal->title }}</h4>
                        </div>
                        <div class="img sm-raduis" data-aos="fade-in">
                            <img src="{{ url('storage/' . $our_goal->image) }}" alt="goal">
                            <div class="marquee-div  has-quote row no-marg-row align-items-center hirozintal-scroll">
                                <div>
                                    {!! $our_goal->content !!}
                                </div>
                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </section>
    <!--end about-sec-3-->

    <!-- start about-sec-4
    ================ -->
        <section class="about-sec-4 margin-div">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-12" data-aos="fade-in">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/menu/5.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ $our_value->title }}</h3>
                            <h4 class="text-upper bold-text">{{ $our_value->title }}</h4>
                        </div>
                    </div>

                    <!--start about-icons-->
                        @foreach($our_value->parent as $value)
                            <div class="col-lg-3 col-md-4 col-sm-6 text-center" data-aos="zoom-out">
                                <div class="about-icons-div">
                                    <div class="img">
                                        <img src="{{ url('storage/' . $value->image) }}" alt="icon">
                                    </div>
                                    <h3 class="second_color">{{ $value->title }}</h3>
                                    <p class="first_color">
                                        {!! $value->content !!}
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    <!--end about-icons-->

                </div>
            </div>
        </section>
    <!--end about-sec-4-->

    <!-- start about-sec-5
    ================ -->
        <section class="about-sec-5 margin-div">
            <div class="container">
                <div class="row justify-content-center">

                    <div class="col-12" data-aos="fade-in">
                        <div class="section-title first_color" data-aos="fade-in">
                            <div class="side-img"><img src="{{ url('web/images/icons/menu/5.png') }}" alt="icon"></div>
                            <h3 class="second_color text-cap bold-text">{{ $our_service->title }}</h3>
                            <h4 class="text-upper bold-text">{{ $our_service->title }}</h4>
                        </div>

                        <h2 class="first_color text-center margin-bottom-div" data-aos="fade-in">
                            {!! $our_service->content !!}
                        </h2>
                        <br> <br>

                    </div>

                    <!--start about-service-->
                        @foreach($our_service->parent as $service)
                        <div class="col-xl-4 col-md-6 about-service-grid text-center" data-aos="zoom-out">
                            <div class="about-service-div">
                                <span class="counter text-left-dir bold-text">{{ $service->title }}</span>
                                <p class="first_color">
                                    {!! $service->content !!}
                                </p>

                                <div class="btn-div">
                                    <a href="{{ route('web.donate') }}" class="custom-btn big-raduis white-btn">
                                        <span>{{ __('web.donate_now') }}</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    <!--end about-service-->

                </div>
            </div>
        </section>
    <!--end about-sec-5-->


    <!-- start about-sec-6
    ================ -->

        <div class="container">
            <div class="row">
                <div class="col-12" data-aos="fade-in">
                    <div class="section-title first_color" data-aos="fade-in">
                        <div class="side-img"><img src="{{ url('web/images/icons/menu/5.png') }}" alt="icon"></div>
                        <h3 class="second_color text-cap bold-text">{{ $feature->title }}</h3>
                        <h4 class="text-upper bold-text">{{ $feature->title }}</h4>
                    </div>
                </div>
            </div>
        </div>

        <section class="about-sec-6 margin-div light_bg">
            <div class="container">
                <div class="white-bg md-raduis">

                    <div class="about-sec-desciption">
                        <div class="row align-items-center">
                            <div class="col-xl-2 col-lg-3 col-md-4 sm-center" data-aos="fade-in">
                                <div class="about-logo md-raduis">
                                    <object data="{{asset('web/images/main/logo2.svg')}}">
                                        <img src="{{ asset('web/images/main/white-logo.png') }}" alt="logo">
                                    </object>
                                </div>
                            </div>
                            <div class="col-xl-10 col-lg-9 col-md-8  sm-center" data-aos="fade-in">
                                {!! $feature->content !!}
                            </div>
                        </div>
                    </div>

                    <!--start about-features-->
                    <div class="about-features">
                        <div class="row">

                            <!--start about-feature-->
                            @foreach($feature->parent as $featu)
                                <div class="col-lg-6" data-aos="fade-in">
                                    <div class="about-feature-div sm-raduis">
                                        <span class="counter text-center white-text bold-text sm-raduis">{{ $featu->title }}</span>
                                            {!! $featu->content !!}
                                    </div>
                                </div>
                            @endforeach
                            <!--end about-feature-->

                        </div>
                        <!--end about-features-->
                    </div>
                </div>
            </div>
        </section>
    <!--end about-sec-6-->
</div>

@endsection

@push('js')

    <!--for this page only-->
    @if(\App::getLocale() == 'ar')
        <script type="text/javascript" src="{{ url('web/js/select-ar.js') }}"></script>
        <script type="text/javascript" src="{{ url('web/js/messages_ar.min.js') }}"></script>
    @endif

    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
    <script type="text/javascript" src="{{ url('web/js/owl.carousel.min.js') }}"></script>
    <!--end-->
    <script type="text/javascript" src="{{ url('web/js/about.js') }}"></script>    

@endpush