@extends('web.layout.app')

@section('title', $data['page']->title)
@section('description', $data['page']->title)
@section('image', asset('web/images/main/white-logo.png'))

@section('content')
    <!-- start pages-header
        ================ -->
    <section class="pages-header text-center" style="background-image:url({{ asset('storage/'.$pages_header) }})">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="pages-header-content line-title">
                        <span class="line"></span>
                        <h1 class="white-text bold-text text-upper">{{$data['page']->title}}</h1>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!--end pages-header-->

    <div class="main-content">
        {{-- <div class="hidden-title">
            <h2 class="white-text bold-text text-upper text-center page-title">{{$data['page']->title}}</h2>
        </div> --}}
        <!-- start about-sec
         ================ -->
        <section class="static-pages margin-div" data-aos="zoom-out">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12">
                        {!! $data['page']->content !!}
                    </div>

                </div>
            </div>
        </section>
        <!--end about-sec-->

    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{asset('web/js/anime.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('web/js/custom-pages.js')}}"></script>
@endpush