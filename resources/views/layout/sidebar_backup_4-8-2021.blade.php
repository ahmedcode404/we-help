
    <!-- BEGIN: Main Menu-->
    @if(auth()->check() && auth()->user()->hasRole('charity'))

        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ route('charity.dashboard') }}"><span class="brand-logo">
                        <div> 
                           
                          <img src="{{ url('dashboard/app-assets/images/new/02') }}"> 
                           
                           </div>
                    </li>
                    <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                    <li class="nav-item {{areActiveMainRoutes(['charity.dashboard'])}}"><a class="d-flex align-items-center" href="{{ route('charity.dashboard') }}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Dashboards">{{ __('admin.home') }}</span></a>
                    </li>
                    <li class=" navigation-header"><span data-i18n="Apps &amp; Pages"></span><i data-feather="more-horizontal"></i>
                    </li>

                    {{-- CHARITY SIDEBAR START --}}



                        @if((Auth::user()->contract_after_sign != NULL || Auth::user()->signature != NULL) && auth()->user()->status == 'approved')

                            <li class="nav-item {{areActiveMainRoutes(['charity.projects.index' , 'charity.projects.create' , 'charity.projects.edit'])}}">
                                <a class="d-flex align-items-center" href="#"><i data-feather="arrow-right"></i><span class="menu-title text-truncate" data-i18n="Invoice">{{ __('admin.projects') }}</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{ route('charity.projects.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{ __('admin.projects') }}</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="{{ route('charity.projects.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.add_projects') }}</span></a>
                                    </li>
                                </ul>
                            </li> 

                            <li class="nav-item {{areActiveMainRoutes(['charity.phases.index' , 'charity.phases.create' , 'charity.phases.edit'])}}">
                                <a class="d-flex align-items-center" href="{{ route('charity.phases.create') }}"><i data-feather="file-text"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.add_phases') }}</span></a>
                            </li> 
                            
                            <li class="nav-item {{areActiveMainRoutes(['charity.sponsers.index' , 'charity.sponsers.create' , 'charity.sponsers.edit'])}}">
                                <a class="d-flex align-items-center" href="{{ route('charity.sponsers.create') }}"><i data-feather="file-text"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.add_sponser') }}</span></a>
                            </li> 
                            
                            <li class="nav-item {{areActiveMainRoutes(['charity.bond.exchange'])}}">
                                <a class="d-flex align-items-center" href="#"><i data-feather="briefcase"></i><span class="menu-title text-truncate" data-i18n="Invoice">{{ __('admin.bond_exchange') }}</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{ route('charity.bond.exchange') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{ __('admin.bond_exchange') }}</span></a>
                                    </li>
                                </ul>
                            </li>  
                            
                            <li class="nav-item {{areActiveMainRoutes(['charity.order.exchange'])}}">
                                <a class="d-flex align-items-center" href="#"><i data-feather="briefcase"></i><span class="menu-title text-truncate" data-i18n="Invoice">{{ __('admin.order_exchange') }}</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{ route('charity.order.exchange') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{ __('admin.order_exchange') }}</span></a>
                                    </li>
                                </ul>
                            </li>                              

                        @endif 


                </ul>
            </div>
        </div>

    @endif  
    <!-- END: Main Menu-->

    {{-- ADMIN SIDEBAR START --}}

        @if(auth()->check() && (auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee')))

            <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
                <div class="navbar-header">
                    <ul class="nav navbar-nav flex-row">
                        {{-- <li class="nav-item mr-auto"><a class="navbar-brand" href="{{route('web.home')}}"><span class="brand-logo"> --}}
                            <li class="nav-item mr-auto"><a class="navbar-brand" href="/"><span class="brand-logo">
                           <div> 
                           
                          <img src="{{ url('dashboard/app-assets/images/new/02') }}"> 
                           
                           </div>
                         </li>
                        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
                    </ul>
                </div>
                <div class="shadow-bottom"></div>
                <div class="main-menu-content">
                    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        

                        <li class=" navigation-header"><span data-i18n="Apps &amp; Pages"></span><i data-feather="more-horizontal"></i>
                        </li>

                        <li class="nav-item {{areActiveMainRoutes(['admin.home'])}}">
                            <a class="d-flex align-items-center" href="{{route('admin.home')}}"><i data-feather="home"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.home')}}</span></a>
                        </li>

                        @if(checkPermissions(['list_charities', 'show_charity', 'accept_refuse_charities', 'edit_charity', 'list_charity_edit_requests', 'accept_refuse_charity_edit_requests']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.charities.index', 'admin.charities.show', 'admin.charities.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.charities')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item {{areActiveMainRoutes(['admin.charities.index', 'admin.charities.show', 'admin.charities.edit'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'waiting') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'waiting')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.waiting_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'approved') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'approved')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.approved_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'rejected') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'rejected')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.rejected_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'hold') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'hold')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.hold_charities')}}</span></a>
                                    </li>
                                    
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['list_projects', 'show_project', 'accept_refuse_projects', 'list_project_edit_requests', 'accept_refuse_project_edit_requests']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.projects.index', 'admin.projects.show', 'admin.projects.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.projects')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item {{areActiveMainRoutes(['admin.projects.index', 'admin.projects.show', 'admin.projects.edit'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'waiting') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'waiting')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.waiting_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'approved') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'approved')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.approved_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'rejected') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'rejected')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.rejected_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'hold') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'hold')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.hold_projects')}}</span></a>
                                    </li>                                  
                                    
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['list_project_edit_requests', 'accept_refuse_project_edit_requests']))
                        
                            <li class="nav-item">
                                    <a class="d-flex align-items-center" href="#">
                                        <i data-feather="circle"></i>
                                        <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.edit_request')}}</span>
                                    </a>
                                    <ul class="menu-content">
                                    <li class="nav-item @if(request()->segment(count(request()->segments())) == 'project') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.requests.index', 'project')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.edit_request_project')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments())) == 'charity') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.requests.index', 'charity')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.edit_request_charity')}}</span></a>
                                    </li>                                    
                                </ul>
                            </li>
                          
                        @endif

                        @if(checkPermissions(['list_exchange_requests', 'list_catch_requests']))
                        
                            <li class="nav-item {{areActiveMainRoutes(['admin.financial-requests.index', 'admin.financial-requests.show', 'admin.financial-requests.edit', 'admin.financial-requests.delete'])}}">
                                    <a class="d-flex align-items-center" href="#">
                                        <i data-feather="circle"></i>
                                        <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.financial_requests')}}</span>
                                    </a>
                                <ul class="menu-content">
                                    {{-- <li class="nav-item  {{areActiveMainRoutes(['admin.financial-requests.index', 'admin.financial-requests.create', 'admin.financial-requests.edit'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.financial-requests.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all')}}</span></a>
                                    </li> --}}
                                    <li class="nav-item  @if(request()->segment(count(request()->segments())-1) == 'fin-req' && request()->segment(count(request()->segments())) == 'exchange') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.fin-req', 'exchange')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.exchange_requests')}}</span></a>
                                    </li>
                                    {{-- <li class="nav-item @if(request()->segment(count(request()->segments())-1) == 'fin-req' && request()->segment(count(request()->segments())) == 'catch') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.fin-req', 'catch')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.catch_requests')}}</span></a>
                                    </li>                                     --}}
                                </ul>
                            </li>
                          
                        @endif

                        @if(checkPermissions(['list_exchange_vouchers', 'list_catch_vouchers']))
                        
                            <li class="nav-item {{areActiveMainRoutes(['admin.financial-bills.index', 'admin.financial-bills.show', 'admin.financial-bills.edit', 'admin.financial-bills.delete'])}}">
                                    <a class="d-flex align-items-center" href="#">
                                        <i data-feather="circle"></i>
                                        <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.financial_bills')}}</span>
                                    </a>
                                <ul class="menu-content">
                                    <li class="nav-item  {{areActiveMainRoutes(['admin.financial-bills.index', 'admin.financial-bills.create', 'admin.financial-bills.edit'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.financial-bills.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all')}}</span></a>
                                    </li>
                                    <li class="nav-item  @if(request()->segment(count(request()->segments())-1) == 'bill-req' && request()->segment(count(request()->segments())) == 'exchange') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.bill-req', 'exchange')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.exchange_bills')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments())-1) == 'bill-req' && request()->segment(count(request()->segments())) == 'catch') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.bill-req', 'catch')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.catch_bills')}}</span></a>
                                    </li>                                    
                                </ul>
                            </li>
                          
                        @endif

                        @if(checkPermissions(['list_reports', 'show_report', 'accept_refuse_report', 'edit_reports', 'delete_reports']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.reports.index', 'admin.reports.show', 'admin.reports.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.reports')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item {{areActiveMainRoutes(['admin.reports.index', 'admin.reports.show', 'admin.reports.edit'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'waiting') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'waiting')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.waiting_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'approved') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'approved')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.approved_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'rejected') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'rejected')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.rejected_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'hold') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'hold')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.hold_reports')}}</span></a>
                                    </li>
                                    
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['list_supporters', 'show_supporter', 'block_unblock_supporter']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.supporters.index', 'admin.supporters.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.supporters.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.supporters')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_supports']))

                            <li class="nav-item {{areActiveMainRoutes(['admin.supports.index'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.supports')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item @if(request()->segment(count(request()->segments())) == 'supports') active @endif"><a class="d-flex align-items-center" href="{{route('admin.supports.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.all')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'supports' && request()->segment(count(request()->segments())) == 'uk') active @endif"><a class="d-flex align-items-center" href="{{route('admin.supports.index', 'uk')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.uk')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'supports' && request()->segment(count(request()->segments())) == 'sa') active @endif"><a class="d-flex align-items-center" href="{{route('admin.supports.index', 'sa')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.sa')}}</span></a>
                                    </li>
                                </ul>
                            </li>

                        @endif

                        @if(checkPermissions(['list_edit_suspended_supports']))

                            <li class="nav-item {{areActiveRoutes(['admin.supports.get-suspended-supports' , 'admin.ratings.delete-comment'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.supports.get-suspended-supports')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.suspended-supports')}}</span></a>
                            </li>

                        @endif 

                        @if(checkPermissions(['list_ratings']))

                            <li class="nav-item {{areActiveRoutes(['admin.ratings.index' , 'admin.ratings.delete-comment'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.ratings.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.ratings')}}</span></a>
                            </li>

                        @endif  

                        @if(checkPermissions(['list_cities', 'create_city', 'edit_city', 'delete_city', 'show_city']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.cities.index', 'admin.cities.create', 'admin.cities.edit', 'admin.cities.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.cities.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.cities')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_job_cats', 'create_job_cats', 'edit_job_cats', 'delete_job_cats', 'show_job_cats']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.job-categories.index', 'admin.job-categories.create', 'admin.job-categories.edit', 'admin.job-categories.show'])}}">
                                <a class="d-flex alig   n-items-center" href="{{route('admin.job-categories.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.job-categories')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_jobs', 'create_job', 'edit_job', 'delete_job', 'show_job']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.jobs.index', 'admin.jobs.create', 'admin.jobs.edit', 'admin.jobs.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.jobs.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.jobs')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_work_fields', 'create_work_fields', 'edit_work_fields', 'delete_work_fields', 'show_work_fields']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.work-fields.index', 'admin.work-fields.create', 'admin.work-fields.edit', 'admin.work-fields.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.work-fields.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.work-fields')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_charity_cats', 'create_charity_cats', 'edit_charity_cats', 'delete_charity_cats', 'show_charity_cats']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.charity-categories.index', 'admin.charity-categories.create', 'admin.charity-categories.edit', 'admin.charity-categories.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.charity-categories.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.charity-categories')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_parteners', 'create_partener', 'edit_partener', 'delete_partener', 'show_partener']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.parteners.index', 'admin.parteners.create', 'admin.parteners.edit', 'admin.parteners.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.parteners.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.parteners')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_marketings', 'create_marketings', 'edit_marketings', 'delete_marketings']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.marketings.index', 'admin.marketings.create', 'admin.marketings.edit', 'admin.marketings.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.marketings.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.marketings')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_campaign_goal', 'create_campaign_goal', 'edit_campaign_goal', 'show_campaign_goal', 'delete_campaign_goal']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.campaigns.index', 'admin.campaigns.create', 'admin.campaigns.edit', 'admin.campaigns.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.campaigns.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.campaigns_goals')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_ambassadors', 'show_ambassadors']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.ambassadors.index'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.ambassadors.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.ambassadors')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_contacts', 'delete_contacts', 'show_contacts']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.contacts.index', 'admin.contacts.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.contacts.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.contacts')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['edit_static_pages']))
                            <li class="nav-item {{areActiveRoutes(['admin.static-pages.edit', 'admin.static-pages.update', 'admin.static-pages.index'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.static_pages')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item @if(areActiveMainRoutes(['admin.static-pages.index', 'admin.static-pages.edit'])) active @endif"><a class="d-flex align-items-center" href="{{route('admin.static-pages.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.static_pages')}}</span></a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['create_sliders', 'edit_sliders', 'delete_sliders']))
                            <li class="nav-item {{areActiveRoutes(['admin.sliders.create', 'admin.sliders.edit', 'admin.sliders.index'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.sliders')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="{{areActiveRoutes(['admin.sliders.create', 'admin.sliders.edit', 'admin.sliders.index'])}}" ><a class="d-flex align-items-center" href="{{route('admin.sliders.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.sliders')}}</span></a>
                                    </li>
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['edit_agreement']))

                            <li class="nav-item {{areActiveMainRoutes(['admin.agreements.create'])}}">
                                <a class="d-flex align-items-center" href="{{ route('admin.agreements.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.update_agreement') }}</span></a>
                            </li> 

                        @endif

                        @if(checkPermissions(['edit_contract' , 'delete_contract' , 'add_contract']))

                            <li class="nav-item {{areActiveMainRoutes(['admin.contracts.index', 'admin.contracts.create' , 'admin.contracts.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.contracts')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{ route('admin.contracts.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.contracts')}}</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="{{ route('admin.contracts.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.create_contract')}}</span></a>
                                    </li>
                                </ul>
                            </li>                            

                        @endif



                        @if(checkPermissions(['edit_service' , 'delete_service' , 'add_service']))

                            <li class="nav-item {{areActiveMainRoutes(['admin.services.index', 'admin.services.create' , 'admin.services.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.services')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{ route('admin.services.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.services')}}</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="{{ route('admin.services.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.create_service')}}</span></a>
                                    </li>
                                </ul>
                            </li>                            

                        @endif



                        <li class="nav-item {{areActiveRoutes(['admin.question.create' , 'admin.questions.index' , 'admin.questions.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.questions')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{route('admin.questions.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.questions')}}</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="{{route('admin.questions.create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.add_question')}}</span></a>
                                    </li>
                                </ul>
                            </li>                        


                        @if(checkPermissions([ 'create_currency' , 'edit_currency' , 'delete_currency']))

                            <li class="nav-item {{areActiveRoutes(['admin.currencies.create' , 'admin.currencies.index' , 'admin.currencies.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.currencies')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{route('admin.currencies.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.currencies')}}</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="{{route('admin.currencies.create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.add_currency')}}</span></a>
                                    </li>
                                </ul>
                            </li>

                        @endif 


                        @if(checkPermissions(['show_ratingcriteria']))

                            <li class="nav-item {{areActiveRoutes(['admin.rating-criterias.edit' , 'admin.rating-criterias.index'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.rating-criterias.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.rating_criterias')}}</span></a>
                            </li>

                        @endif                          


                        @if(checkPermissions([ 'create_emp' , 'edit_emp' , 'delete_emp' , 'show_emp']))

                            <li class="nav-item {{areActiveRoutes(['admin.employees.create' , 'admin.employees.index' , 'admin.employees.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.employees')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{route('admin.employees.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.employees')}}</span></a>
                                    </li>
                                    <li><a class="d-flex align-items-center" href="{{route('admin.employees.create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.add_employee')}}</span></a>
                                    </li>
                                </ul>
                            </li>

                        @endif 

                        @if(checkPermissions(['list_degrees', 'create_degrees', 'edit_degrees', 'delete_degrees']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.degrees.index', 'admin.degrees.create', 'admin.degrees.edit', 'admin.degrees.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.degrees.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.degrees')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_banks', 'create_banks', 'edit_banks', 'delete_banks']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.banks.index', 'admin.banks.create', 'admin.banks.edit', 'admin.banks.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.banks.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.banks')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_nationalities', 'create_nationalities', 'edit_nationalities', 'delete_nationalities']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.nationalities.index', 'admin.nationalities.create', 'admin.nationalities.edit', 'admin.nationalities.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.nationalities.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.nationalities')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['edit_setting']))

                            <li class="nav-item {{areActiveMainRoutes(['admin.settings.create'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.settings')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{route('admin.setting.edit')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.settings')}}</span></a>
                                    </li>
                                </ul>
                            </li>

                        @endif

                    </ul>
                </div>
            </div>

        @endif
        

    {{-- ADMIN SIDEBAR EBD --}}