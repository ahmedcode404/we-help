
    <!-- BEGIN: Main Menu-->
    @if(auth()->check() && (auth()->user()->hasRole('charity') || auth()->user()->hasRole('charity_employee')))

        <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
            <div class="navbar-header">
                <ul class="nav navbar-nav flex-row">
                    <li class="nav-item mr-auto"><a class="navbar-brand" href="/" target="_blank"><span class="brand-logo">
                        <div> 
                           
                          <img src="{{ url('dashboard/app-assets/images/new/02') }}"> 
                           
                           </div>
                    </li>
                    <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
                </ul>
            </div>
            <div class="shadow-bottom"></div>
            <div class="main-menu-content">
                <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">

                    <li class="nav-item {{areActiveMainRoutes(['charity.dashboard'])}}"><a class="d-flex align-items-center" href="{{ route('charity.dashboard') }}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Dashboards">{{ __('admin.statistics') }}</span></a>
                    </li>
                    <!--<li class=" navigation-header"><span data-i18n="Apps &amp; Pages"></span><i data-feather="more-horizontal"></i>-->
                    <!--</li>-->

                    {{-- CHARITY SIDEBAR START --}}


                    @if(auth()->check() &&
                        (auth()->user()->hasRole('charity') &&
                        auth()->user()->status == 'approved' && 
                        (auth()->user()->contract_after_sign != null || auth()->user()->signature != null)) || 

                        (auth()->user()->hasRole('charity_employee') &&
                        auth()->user()->charity->status == 'approved' &&
                        (auth()->user()->charity->contract_after_sign != null || auth()->user()->charity->signature != null))
                        )

                            @php
                                if(auth()->user()->hasRole('charity_employee')){
                                    $user = auth()->user()->charity;
                                }
                                else{

                                    $user = auth()->user();
                                }
                            @endphp

                            @if(checkPermissions(['list_projects_'.$user->id]) || checkPermissions(['create_projects_'.$user->id]) || 
                                checkPermissions(['edit_projects_'.$user->id]) || checkPermissions(['show_projects_'.$user->id]) || 
                                checkPermissions(['delete_projects_'.$user->id]) ||
                                // صلاحيات المراحل مرتبطه بعرض المشاريع
                                checkPermissions(['list_phases_'.$user->id]) || checkPermissions(['create_phases_'.$user->id]) || 
                                checkPermissions(['edit_phases_'.$user->id]) || checkPermissions(['show_phases_'.$user->id]) || 
                                checkPermissions(['delete_phases_'.$user->id]) || 
                                // صلاحيات الجهات الخارجية مرتبطه بعرض المشاريع
                                // checkPermissions(['list_sponsers_'.$user->id]) || checkPermissions(['create_sponsers_'.$user->id]) || 
                                // checkPermissions(['edit_sponsers_'.$user->id]) || checkPermissions(['show_sponsers_'.$user->id]) || 
                                // checkPermissions(['delete_sponsers_'.$user->id]) ||
                                // صلاحيات التقارير مرتبطة بعرض المشاريع
                                checkPermissions(['list_reports_'.$user->id]) || checkPermissions(['create_reports_'.$user->id]) || 
                                checkPermissions(['edit_reports_'.$user->id]) || checkPermissions(['show_reports_'.$user->id]) || 
                                checkPermissions(['delete_reports_'.$user->id])
                                )

                                <li class="nav-item {{areActiveMainRoutes(['charity.projects.index' , 'charity.projects.create' , 'charity.projects.edit'])}}">
                                    <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">{{ __('admin.projects') }}</span></a>
                                    <ul class="menu-content">
                                        <li><a class="d-flex align-items-center" href="{{ route('charity.projects.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{ __('admin.projects') }}</span></a>
                                        </li>
                                        @if(checkPermissions(['create_projects_'.$user->id]))
                                            <li><a class="d-flex align-items-center" href="{{ route('charity.projects.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.add_projects') }}</span></a>
                                            </li>
                                        @endif
                                    </ul>
                                </li> 

                            @endif

                            @if(checkPermissions(['list_phases_'.$user->id]) || checkPermissions(['create_phases_'.$user->id]) || 
                                checkPermissions(['edit_phases_'.$user->id]) || checkPermissions(['show_phases_'.$user->id]) || 
                                checkPermissions(['delete_phases_'.$user->id]))

                                <li class="nav-item {{areActiveMainRoutes(['charity.phases.index' , 'charity.phases.create' , 'charity.phases.edit'])}}">
                                    <a class="d-flex align-items-center" href="{{ route('charity.phases.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.add_phases') }}</span></a>
                                </li> 
                            @endif

                            @if(checkPermissions(['list_employees_'.$user->id, 'create_employees_'.$user->id , 
                                                'edit_employees_'.$user->id , 'show_employees_'.$user->id , 
                                                'delete_employees_'.$user->id, 'list_job_categories_'.$user->id,
                                                'create_job_categories_'.$user->id, 'edit_job_categories_'.$user->id,
                                                'show_job_categories_'.$user->id, 'delete_job_categories_'.$user->id,
                                                'list_jobs_'.$user->id, 'create_jobs_'.$user->id, 'edit_jobs_'.$user->id,
                                                'show_jobs_'.$user->id, 'delete_jobs_'.$user->id, 'list_degrees_'.$user->id,
                                                'create_degrees_'.$user->id, 'edit_degrees_'.$user->id, 'show_degrees_'.$user->id,
                                                'delete_degrees_'.$user->id, 'list_nationalities_'.$user->id, 
                                                'create_nationalities_'.$user->id, 'edit_nationalities_'.$user->id,
                                                'show_nationalities_'.$user->id, 'delete_nationalities_'.$user->id]))

                                <li class="nav-item {{areActiveMainRoutes(['charity.employees.index' , 'charity.employees.create' , 'charity.employees.edit', 'charity.employees.show',
                                                    'charity.job-categories.index' , 'charity.job-categories.create' , 'charity.job-categories.edit', 'charity.job-categories.show',
                                                    'charity.jobs.index' , 'charity.jobs.create' , 'charity.jobs.edit', 'charity.jobs.show',
                                                    'charity.degrees.index' , 'charity.degrees.create' , 'charity.degrees.edit', 'charity.degrees.show',
                                                    'charity.nationalities.index' , 'charity.nationalities.create' , 'charity.nationalities.edit', 'charity.nationalities.show'])}}">

                                    <a class="d-flex align-items-center" href="#">
                                        <i data-feather="circle"></i>
                                        <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.employees')}}</span>
                                    </a>
                                    
                                    <ul class="menu-content">
        
                                        @if(checkPermissions(['create_employees_'.$user->id]))
                                            <li class="nav-item {{areActiveMainRoutes(['charity.employees.create'])}}"><a class="d-flex align-items-center" href="{{route('charity.employees.create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.add_employee')}}</span></a>
                                            </li>
                                        @endif
        
                                        @if(checkPermissions(['list_employees_'.$user->id, 'create_employees_'.$user->id , 'edit_employees_'.$user->id , 'show_employees_'.$user->id , 'delete_employees_'.$user->id]))
                                            <li class="nav-item {{areActiveMainRoutes(['charity.employees.index', 'charity.employees.edit', 'charity.employees.show'])}}"><a class="d-flex align-items-center" href="{{route('charity.employees.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.employees_data')}}</span></a>
                                            </li>
                                        @endif

                                        @if(checkPermissions(['list_job_categories_'.$user->id, 'create_job_categories_'.$user->id, 'edit_job_categories_'.$user->id, 'delete_job_categories_'.$user->id, 'show_job_categories_'.$user->id]))
                                            <li class="nav-item {{areActiveMainRoutes(['charity.job-categories.index', 'charity.job-categories.create', 'charity.job-categories.edit', 'charity.job-categories.show'])}}">
                                                <a class="d-flex alig   n-items-center" href="{{route('charity.job-categories.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.job-categories')}}</span></a>
                                            </li>
                                        @endif

                                        @if(checkPermissions(['list_jobs_'.$user->id, 'create_jobs_'.$user->id, 'edit_jobs_'.$user->id, 'delete_jobs_'.$user->id, 'show_jobs_'.$user->id]))
                                            <li class="nav-item {{areActiveMainRoutes(['charity.jobs.index', 'charity.jobs.create', 'charity.jobs.edit', 'charity.jobs.show'])}}">
                                                <a class="d-flex align-items-center" href="{{route('charity.jobs.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.assign_job_to_emp')}}</span></a>
                                            </li>
                                        @endif

                                        @if(checkPermissions(['list_degrees_'.$user->id, 'create_degrees_'.$user->id, 'edit_degrees_'.$user->id, 'delete_degrees_'.$user->id]))
                                            <li class="nav-item {{areActiveMainRoutes(['charity.degrees.index', 'charity.degrees.create', 'charity.degrees.edit', 'charity.degrees.show'])}}">
                                                <a class="d-flex align-items-center" href="{{route('charity.degrees.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.emp_degrees')}}</span></a>
                                            </li>
                                        @endif

                                        @if(checkPermissions(['list_nationalities_'.$user->id, 'create_nationalities_'.$user->id, 'edit_nationalities_'.$user->id, 'delete_nationalities_'.$user->id]))
                                            <li class="nav-item {{areActiveMainRoutes(['charity.nationalities.index', 'charity.nationalities.create', 'charity.nationalities.edit', 'charity.nationalities.show'])}}">
                                                <a class="d-flex align-items-center" href="{{route('charity.nationalities.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.nationalities')}}</span></a>
                                            </li>
                                        @endif
        
                                    </ul>
                                </li>
                            @endif
                            
                            {{-- @if(checkPermissions(['create_sponsers_'.$user->id]))

                                <li class="nav-item {{areActiveMainRoutes(['charity.sponsers.index' , 'charity.sponsers.create' , 'charity.sponsers.edit'])}}">
                                    <a class="d-flex align-items-center" href="{{ route('charity.sponsers.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.add_sponser') }}</span></a>
                                </li> 
                            @endif --}}
                            
                            @if(checkPermissions(['list_exchange_bonds_'.$user->id]) || checkPermissions(['create_exchange_bonds_'.$user->id]))
                                <li class="nav-item {{areActiveMainRoutes(['charity.bond.exchange'])}}">
                                    <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">{{ __('admin.bond_exchange') }}</span></a>
                                    <ul class="menu-content">
                                        <li><a class="d-flex align-items-center" href="{{ route('charity.bond.exchange') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{ __('admin.bond_exchange') }}</span></a>
                                        </li>
                                    </ul>
                                </li>  
                            @endif
                            
                            {{-- <li class="nav-item {{areActiveMainRoutes(['charity.order.exchange'])}}">
                                <a class="d-flex align-items-center" href="#"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Invoice">{{ __('admin.order_exchange') }}</span></a>
                                <ul class="menu-content">
                                    <li><a class="d-flex align-items-center" href="{{ route('charity.order.exchange') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{ __('admin.order_exchange') }}</span></a>
                                    </li>
                                </ul>
                            </li> --}}

                        @endif 


                </ul>
            </div>
        </div>

    @endif  
    <!-- END: Main Menu-->

    {{-- ADMIN SIDEBAR START --}}

        @if(auth()->check() && (auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee')))

            <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
                <div class="navbar-header">
                    <ul class="nav navbar-nav flex-row">
                            <li class="nav-item mr-auto"><a class="navbar-brand" href="/" target="_blank"><span class="brand-logo">
                           <div> 
                           
                          <img src="{{ url('dashboard/app-assets/images/new/02') }}"> 
                           
                           </div>
                         </li>
                        <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i class="d-none d-xl-block collapse-toggle-icon font-medium-4  text-primary" data-feather="disc" data-ticon="disc"></i></a></li>
                    </ul>
                </div>
                <div class="shadow-bottom"></div>
                <div class="main-menu-content">
                    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        

                        <li class=" navigation-header"><span data-i18n="Apps &amp; Pages"></span><i data-feather="more-horizontal"></i>
                        </li>

                        <li class="nav-item {{areActiveMainRoutes(['admin.home'])}}">
                            <a class="d-flex align-items-center" href="{{route('admin.home')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.statistics')}}</span></a>
                        </li>

                        @if(checkPermissions(['list_charities', 'show_charity', 'accept_refuse_charities', 'edit_charity', 'create_charity', 'list_charity_edit_requests', 'accept_refuse_charity_edit_requests']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.charities.index', 'admin.charities.show', 'admin.charities.edit', 'admin.charities.create'])}}">

                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.charities')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item {{areActiveMainRoutes(['admin.charities.index', 'admin.charities.show', 'admin.charities.edit'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'waiting') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'waiting')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.waiting_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'approved') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'approved')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.approved_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'rejected') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'rejected')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.rejected_charities')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'charities-status' && request()->segment(count(request()->segments())) == 'hold') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.charities.get-with-status', 'hold')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.hold_charities')}}</span></a>
                                    </li>
                                    
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['list_projects', 'show_project', 'accept_refuse_projects', 'list_project_edit_requests', 'create_project',
                                            'accept_refuse_project_edit_requests', 'list_edit_suspended_supports', 'create_report   ', 'create_phase']))

                            <li class="nav-item {{areActiveMainRoutes(['admin.projects.index', 'admin.projects.show', 'admin.projects.edit', 'admin.projects.create',
                                                'admin.supports.get-suspended-supports', 'admin.phases.create', 'admin.reports.create-report'])}}">

                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.projects')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item {{areActiveMainRoutes(['admin.reports.create-report', 'admin.phases.create', 'admin.projects.index', 'admin.projects.show', 'admin.projects.edit', 'admin.projects.trashed'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'waiting') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'waiting')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.waiting_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'approved') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'approved')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.approved_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'rejected') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'rejected')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.rejected_projects')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'hold') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'hold')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.hold_projects')}}</span></a>
                                    </li> 
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'projects-status' && request()->segment(count(request()->segments())) == 'trashed') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.projects.get-project-with-status', 'trashed')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.trashed_projects')}}</span></a>
                                    </li> 
                                    
                                    @if(checkPermissions(['list_edit_suspended_supports']))

                                        <li class="nav-item {{areActiveRoutes(['admin.supports.get-suspended-supports'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.supports.get-suspended-supports')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.suspended-supports')}}</span></a>
                                        </li>

                                    @endif 
                                    
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['list_project_edit_requests', 'accept_refuse_project_edit_requests']))
                        
                            <li class="nav-item">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.edit_request')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item @if(request()->segment(count(request()->segments())) == 'charity') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.requests.index', 'charity')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.edit_request_charity')}}</span></a>
                                    </li>  

                                    <li class="nav-item @if(request()->segment(count(request()->segments())) == 'project') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.requests.index', 'project')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.edit_request_project')}}</span></a>
                                    </li>                              
                                </ul>
                            </li>
                          
                        @endif

                        @if(checkPermissions(['list_reports', 'show_report', 'accept_refuse_report', 'edit_reports', 'delete_reports']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.reports.index', 'admin.reports.show', 'admin.reports.edit'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.reports')}}</span>
                                </a>
                                <ul class="menu-content">
                                    <li class="nav-item {{areActiveMainRoutes(['admin.reports.index', 'admin.reports.show', 'admin.reports.edit'])}}">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'waiting') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'waiting')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.waiting_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'approved') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'approved')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.approved_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'rejected') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'rejected')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.rejected_reports')}}</span></a>
                                    </li>
                                    <li class="nav-item @if(request()->segment(count(request()->segments()) - 1) == 'report-status' && request()->segment(count(request()->segments())) == 'hold') active @endif">
                                        <a class="d-flex align-items-center" href="{{route('admin.reports.get-report-with-status', 'hold')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.hold_reports')}}</span></a>
                                    </li>
                                    
                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['list_exchange_requests', 'list_catch_requests', 'list_exchange_vouchers', 'list_catch_vouchers',
                                    'create_finance_voucher', 'edit_finance_voucher', 'show_finance_voucher', 'delete_finance_voucher']))
                        
                            <li class="nav-item {{areActiveMainRoutes(['admin.financial-requests.index', 'admin.financial-requests.show', 
                                        'admin.financial-requests.edit', 'admin.financial-requests.create', 'admin.financial-requests.delete', 'admin.financial-requests-status',
                                        'admin.financial-bills.index', 'admin.financial-bills.show', 'admin.financial-bills.edit', 'admin.financial-bills.create',
                                        'admin.financial-bills.delete', 'admin.financial-bills-exchange', 'admin.financial-bills-catch',
                                        'admin.financial-bills-status', 'admin.supports.create-vip-support', 'admin.supports.edit-vip-support', 'admin.supports.vip-support'])}}">

                                    <a class="d-flex align-items-center" href="#">
                                        <i data-feather="circle"></i>
                                        <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.financial')}}</span>
                                    </a>
                                <ul class="menu-content">
                                    
                                    @if(checkPermissions(['list_exchange_requests', 'list_catch_requests']))
                                        <li class="nav-item  {{areActiveMainRoutes(['admin.financial-requests.index', 'admin.financial-requests.edit', 'admin.financial-requests.create', 'admin.financial-requests-status'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.financial-requests.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.exchange_requests')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['list_exchange_vouchers', 'list_catch_vouchers']))
                                        <li class="nav-item  @if(request()->segment(count(request()->segments())) == 'exchange' || areActiveMainRoutes(['admin.financial-bills-exchange', 'admin.financial-bills.create', 'admin.financial-bills,edit'])) active @endif">
                                            <a class="d-flex align-items-center" href="{{route('admin.financial-bills', 'exchange')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.exchange_bills')}}</span></a>
                                        </li>
                                        {{-- <li class="nav-item  {{areActiveMainRoutes(['admin.financial-bills.index', 'admin.financial-bills.create', 'admin.financial-bills.edit', 'admin.financial-bills-status'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.financial-bills.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.all_bills')}}</span></a>
                                        </li> --}}
                                        {{-- <li class="nav-item @if(request()->segment(count(request()->segments())) == 'catch' || areActiveMainRoutes(['admin.financial-bills-catch'])) active @endif">
                                            <a class="d-flex align-items-center" href="{{route('admin.financial-bills', 'catch')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.catch_bills')}}</span></a>
                                        </li>  --}}
                                    @endif

                                    @if(checkPermissions(['list_exchange_vouchers', 'create_finance_voucher', 'edit_finance_voucher', 'show_finance_voucher', 'delete_finance_voucher']))
                                        <li class="nav-item {{areActiveRoutes(['admin.supports.create-vip-support', 'admin.supports.edit-vip-support', 'admin.supports.vip-support'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.supports.vip-support')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.vip_supports')}}</span></a>
                                        </li>
                                    @endif
                                   
                                </ul>
                            </li>
                          
                        @endif

                        @if(checkPermissions(['list_supports', 'list_supporters', 'show_supporter', 'block_unblock_supporter']))

                            <li class="nav-item {{areActiveMainRoutes(['admin.supports.index', 'admin.supporters.index', 'admin.supporters.show'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.supporters')}}</span>
                                </a>
                                <ul class="menu-content">

                                    @if(checkPermissions(['list_supporters', 'show_supporter', 'block_unblock_supporter']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.supporters.index', 'admin.supporters.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.supporters.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.supporters_data')}}</span></a>
                                        </li>
                                    @endif
                                    
                                    @if(checkPermissions(['list_supports']))
                                        <li class="nav-item @if(request()->segment(count(request()->segments())) == 'supports') active @endif"><a class="d-flex align-items-center" href="{{route('admin.supports.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.supports')}}</span></a>
                                        </li>
                                    @endif

                                </ul>
                            </li>

                        @endif

                        @if(checkPermissions(['list_emp', 'create_emp' , 'edit_emp' , 'delete_emp' , 'show_emp', 
                                            'list_job_cats', 'create_job_cats', 'edit_job_cats', 'delete_job_cats', 'show_job_cats',
                                            'list_jobs', 'create_job', 'edit_job', 'delete_job', 'show_job']))

                            <li class="nav-item {{areActiveRoutes(['admin.employees.create' , 'admin.employees.index' , 'admin.employees.edit',
                                                                    'admin.job-categories.index', 'admin.job-categories.create', 
                                                                    'admin.job-categories.edit', 'admin.job-categories.show', 'admin.jobs.index', 
                                                                    'admin.jobs.create', 'admin.jobs.edit', 'admin.jobs.show'])}}">

                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.employees')}}</span>
                                </a>
                                <ul class="menu-content">

                                    @if(checkPermissions(['create_emp']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.employees.create'])}}"><a class="d-flex align-items-center" href="{{route('admin.employees.create')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{trans('admin.add_employee')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['list_emp', 'create_emp' , 'edit_emp' , 'delete_emp' , 'show_emp']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.employees.index', 'admin.employees.edit', 'admin.employees.show'])}}"><a class="d-flex align-items-center" href="{{route('admin.employees.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.employees_data')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['list_job_cats', 'create_job_cats', 'edit_job_cats', 'delete_job_cats', 'show_job_cats']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.job-categories.index', 'admin.job-categories.create', 'admin.job-categories.edit', 'admin.job-categories.show'])}}">
                                            <a class="d-flex alig   n-items-center" href="{{route('admin.job-categories.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.job-categories')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['list_jobs', 'create_job', 'edit_job', 'delete_job', 'show_job']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.jobs.index', 'admin.jobs.create', 'admin.jobs.edit', 'admin.jobs.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.jobs.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.assign_job_to_emp')}}</span></a>
                                        </li>
                                    @endif
                                    
                                </ul>
                            </li>

                        @endif 

                        @if(checkPermissions(['list_contacts', 'delete_contacts', 'show_contacts']))
                            <li class="nav-item {{areActiveMainRoutes(['admin.contacts.index', 'admin.contacts.show'])}}">
                                <a class="d-flex align-items-center" href="{{route('admin.contacts.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.contacts')}}</span></a>
                            </li>
                        @endif

                        @if(checkPermissions(['list_marketings', 'create_marketings', 'edit_marketings', 'delete_marketings',
                                                'list_campaign_goal', 'create_campaign_goal', 'edit_campaign_goal', 'show_campaign_goal', 
                                                'delete_campaign_goal', 'list_ambassadors', 'show_ambassadors']))

                            <li class="nav-item {{areActiveRoutes(['admin.marketings.index', 'admin.marketings.create', 'admin.marketings.edit', 'admin.marketings.show',
                                                                'admin.campaigns.index', 'admin.campaigns.create', 'admin.campaigns.edit', 'admin.campaigns.show',
                                                                'admin.ambassadors.index'])}}">
                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.marketing')}}</span>
                                </a>

                                <ul class="menu-content">

                                    @if(checkPermissions(['list_marketings', 'create_marketings', 'edit_marketings', 'delete_marketings']))
                                        <li class="nav-item @if(isset(request()->segments()[1]) && request()->segments()[1] == 'marketings' || request()->segments() == 'get-marketings-by-type') active @endif">
                                            <a class="d-flex align-items-center" href="{{route('admin.marketings.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.marketings')}}</span></a>
                                        </li>
                                    @endif
                                    {{-- 
                                    @if(checkPermissions(['list_marketings', 'create_marketings', 'edit_marketings', 'delete_marketings']))

                                        <li class="nav-item {{areActiveMainRoutes(['admin.marketings.index', 'admin.marketings.create', 'admin.marketings.edit', 'admin.marketings.show'])}}">
                                            <a class="d-flex align-items-center" href="#">
                                                <i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.marketings')}}</span>
                                            </a>

                                            <ul class="menu-content">
                                                <li class="nav-item {{areActiveMainRoutes(['admin.marketings.index'])}}">
                                                    <a class="d-flex align-items-center" href="{{route('admin.marketings.index')}}">
                                                        <span class="menu-item text-truncate" data-i18n="LoginV1">{{trans('admin.all')}}</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item @if(request()->segment(count(request()->segments())) == 'sms') active @endif">
                                                    <a class="d-flex align-items-center" href="{{route('admin.marketings.get-by-type', 'sms')}}">
                                                        <span class="menu-item text-truncate" data-i18n="LoginV1">{{trans('admin.sms')}}</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item @if(request()->segment(count(request()->segments())) == 'email') active @endif">
                                                    <a class="d-flex align-items-center" href="{{route('admin.marketings.get-by-type', 'email')}}">
                                                        <span class="menu-item text-truncate" data-i18n="LoginV1">{{trans('admin.email')}}</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item @if(request()->segment(count(request()->segments())) == 'mobile') active @endif">
                                                    <a class="d-flex align-items-center" href="{{route('admin.marketings.get-by-type', 'mobile')}}">
                                                        <span class="menu-item text-truncate" data-i18n="LoginV1">{{trans('admin.mobile')}}</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    @endif --}}

                                    @if(checkPermissions(['list_campaign_goal', 'create_campaign_goal', 'edit_campaign_goal', 'show_campaign_goal', 'delete_campaign_goal']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.campaigns.index', 'admin.campaigns.create', 'admin.campaigns.edit', 'admin.campaigns.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.campaigns.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.campaigns_goals')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['list_ambassadors', 'show_ambassadors']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.ambassadors.index'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.ambassadors.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.ambassadors')}}</span></a>
                                        </li>
                                    @endif

                                </ul>
                            </li>
                        @endif

                        @if(checkPermissions(['list_charity_cats', 'create_charity_cats', 'edit_charity_cats', 'delete_charity_cats', 
                                            'show_charity_cats', 'edit_service' , 'delete_service' , 'add_service', ]))
                        
                            <li class="nav-item {{ areActiveMainRoutes(['admin.charity-categories.index', 'admin.charity-categories.create', 'admin.charity-categories.edit',
                                            'admin.charity-categories.show', 'admin.services.index', 'admin.services.create' , 'admin.services.edit',
                                            'admin.service-features.index', 'admin.service-features.create' , 'admin.service-features.edit']) }}">

                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.charity-categories')}}</span>
                                </a>
                                <ul class="menu-content">

                                    @if(checkPermissions(['list_charity_cats', 'create_charity_cats', 'edit_charity_cats', 'delete_charity_cats', 'show_charity_cats']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.charity-categories.index', 'admin.charity-categories.create', 'admin.charity-categories.edit', 'admin.charity-categories.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.charity-categories.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.charity-categories')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['edit_service' , 'delete_service' , 'add_service']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.services.index', 'admin.services.create' , 'admin.services.edit'])}}">
                                            <a class="d-flex align-items-center" href="{{ route('admin.services.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.services')}}</span></a>
                                        </li> 
                                        
                                        <li class="nav-item {{areActiveMainRoutes(['admin.service-features.index', 'admin.service-features.create' , 'admin.service-features.edit'])}}">
                                            <a class="d-flex align-items-center" href="{{ route('admin.service-features.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.service-features')}}</span></a>
                                        </li>
                                    @endif

                                </ul>
                            </li>

                        @endif

                        @if(checkPermissions(['edit_setting', 
                                            // 'list_cities', 'create_city', 'edit_city', 'delete_city', 'show_city',
                                            // 'list_work_fields', 'create_work_fields', 'edit_work_fields', 
                                            // 'delete_work_fields', 'show_work_fields', 
                                            'list_parteners', 'create_partener', 'edit_partener', 
                                            'delete_partener', 'show_partener', 'edit_static_pages', 'create_sliders', 'edit_sliders', 
                                            'delete_sliders', 'edit_agreement', 'edit_contract' , 'delete_contract' , 'add_contract',
                                            'create_question', 'edit_question', 
                                            'delete_question', 'show_question', 'create_currency' , 'edit_currency' , 'delete_currency',
                                            'show_ratingcriteria', 'edit_ratingcriteria', 'list_ratings', 'list_degrees', 'create_degrees', 
                                            'edit_degrees', 'delete_degrees', 'list_nationalities', 'create_nationalities', 'edit_nationalities', 
                                            'delete_nationalities', 'list_banks', 'create_banks', 'edit_banks', 'delete_banks']))


                            <li class="nav-item {{areActiveMainRoutes(['admin.setting.edit',
                                                                    // 'admin.cities.index', 'admin.cities.create', 'admin.cities.edit', 'admin.cities.show',
                                                                    // 'admin.work-fields.index', 'admin.work-fields.create', 'admin.work-fields.edit',
                                                                    // 'admin.work-fields.show', 
                                                                    'admin.parteners.index', 'admin.parteners.create', 'admin.parteners.edit', 
                                                                    'admin.parteners.show', 'admin.static-pages.index', 'admin.static-pages.edit',
                                                                    'admin.sliders.create', 'admin.sliders.edit', 'admin.sliders.index',
                                                                    'admin.agreements.create', 'admin.contracts.index', 'admin.contracts.create' , 
                                                                    'admin.contracts.edit', 
                                                                    'admin.questions.create' , 'admin.questions.index' , 'admin.questions.edit',
                                                                    'admin.currencies.create' , 'admin.currencies.index' , 'admin.currencies.edit',
                                                                    'admin.rating-criterias.edit' , 'admin.rating-criterias.index',
                                                                    'admin.degrees.index', 'admin.degrees.create', 'admin.degrees.edit', 'admin.degrees.show',
                                                                    'admin.ratings.index' , 'admin.ratings.delete-comment',
                                                                    'admin.nationalities.index', 'admin.nationalities.create', 'admin.nationalities.edit', 'admin.nationalities.show',
                                                                    'admin.banks.index', 'admin.banks.create', 'admin.banks.edit', 'admin.banks.show'])}}">

                                <a class="d-flex align-items-center" href="#">
                                    <i data-feather="circle"></i>
                                    <span class="menu-title text-truncate" data-i18n="Invoice">{{trans('admin.web_settings')}}</span>
                                </a>
                                <ul class="menu-content">

                                    @if(checkPermissions(['edit_setting']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.setting.edit'])}}"><a class="d-flex align-items-center" href="{{route('admin.setting.edit')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.advanced_settings')}}</span></a>
                                        </li>
                                    @endif

                                    {{-- @if(checkPermissions(['list_cities', 'create_city', 'edit_city', 'delete_city', 'show_city']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.cities.index', 'admin.cities.create', 'admin.cities.edit', 'admin.cities.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.cities.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.cities')}}</span></a>
                                        </li>
                                    @endif --}}

                                    @if(checkPermissions(['list_work_fields', 'create_work_fields', 'edit_work_fields', 'delete_work_fields', 'show_work_fields']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.work-fields.index', 'admin.work-fields.create', 'admin.work-fields.edit', 'admin.work-fields.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.work-fields.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.work-fields')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['list_parteners', 'create_partener', 'edit_partener', 'delete_partener', 'show_partener']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.parteners.index', 'admin.parteners.create', 'admin.parteners.edit', 'admin.parteners.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.parteners.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.parteners')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['edit_static_pages']))
                                        <li class="nav-item @if(areActiveMainRoutes(['admin.static-pages.index', 'admin.static-pages.edit'])) active @endif">
                                            <a class="d-flex align-items-center" href="{{route('admin.static-pages.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.static_pages')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['create_sliders', 'edit_sliders', 'delete_sliders']))
                                        <li class="{{areActiveRoutes(['admin.sliders.create', 'admin.sliders.edit', 'admin.sliders.index'])}}" >
                                            <a class="d-flex align-items-center" href="{{route('admin.sliders.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.sliders')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['edit_agreement']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.agreements.create'])}}">
                                            <a class="d-flex align-items-center" href="{{ route('admin.agreements.create') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="Preview">{{ __('admin.update_agreement') }}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['edit_contract' , 'delete_contract' , 'add_contract']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.contracts.index', 'admin.contracts.create' , 'admin.contracts.edit'])}}">
                                            <a class="d-flex align-items-center" href="{{ route('admin.contracts.index') }}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.contracts')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['create_question', 'edit_question', 'delete_question', 'show_question']))
                                        <li class="nav-item {{areActiveRoutes(['admin.questions.create' , 'admin.questions.index' , 'admin.questions.edit'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.questions.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.questions')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['create_currency' , 'edit_currency' , 'delete_currency']))
                                        <li class="nav-item {{areActiveRoutes(['admin.currencies.create' , 'admin.currencies.index' , 'admin.currencies.edit'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.currencies.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.currencies')}}</span></a>
                                        </li>
                                    @endif 

                                    @if(checkPermissions(['show_ratingcriteria', 'edit_ratingcriteria']))
                                        <li class="nav-item {{areActiveRoutes(['admin.rating-criterias.edit' , 'admin.rating-criterias.index'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.rating-criterias.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.rating_criterias')}}</span></a>
                                        </li>
                                    @endif  

                                    @if(checkPermissions(['list_ratings']))
                                        <li class="nav-item {{areActiveRoutes(['admin.ratings.index' , 'admin.ratings.delete-comment'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.ratings.index')}}"><i data-feather="circle"></i><span class="menu-item text-truncate" data-i18n="List">{{trans('admin.ratings')}}</span></a>
                                        </li>
                                    @endif 

                                    @if(checkPermissions(['list_degrees', 'create_degrees', 'edit_degrees', 'delete_degrees']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.degrees.index', 'admin.degrees.create', 'admin.degrees.edit', 'admin.degrees.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.degrees.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.emp_degrees')}}</span></a>
                                        </li>
                                    @endif

                                    @if(checkPermissions(['list_nationalities', 'create_nationalities', 'edit_nationalities', 'delete_nationalities']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.nationalities.index', 'admin.nationalities.create', 'admin.nationalities.edit', 'admin.nationalities.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.nationalities.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.nationalities')}}</span></a>
                                        </li>
                                    @endif

                                    {{-- @if(checkPermissions(['list_banks', 'create_banks', 'edit_banks', 'delete_banks']))
                                        <li class="nav-item {{areActiveMainRoutes(['admin.banks.index', 'admin.banks.create', 'admin.banks.edit', 'admin.banks.show'])}}">
                                            <a class="d-flex align-items-center" href="{{route('admin.banks.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.banks')}}</span></a>
                                        </li>
                                    @endif --}}

                                </ul>
                            </li>
                        @endif

                        <li class="nav-item {{areActiveMainRoutes(['admin.logs.index'])}}">
                            <a class="d-flex align-items-center" href="{{route('admin.logs.index')}}"><i data-feather="circle"></i><span class="menu-title text-truncate" data-i18n="Email">{{trans('admin.logs')}}</span></a>
                        </li>

                    </ul>
                </div>
            </div>

        @endif
        

    {{-- ADMIN SIDEBAR EBD --}}