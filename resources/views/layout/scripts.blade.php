<!-- BEGIN: Vendor JS-->
<script src="{{ url('dashboard/app-assets/vendors/js/vendors.min.js') }}"></script>
<!-- BEGIN Vendor JS-->


<!-- BEGIN: firebase -->
<!-- <script src="https://www.gstatic.com/firebasejs/7.23.0/firebase.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/8.2.1/firebase-analytics.js"></script> -->
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.16.1/firebase-messaging.js"></script>
<script src="{{ url('dashboard/notifications/take-token.js') }}"></script>
<!-- <script src="{{ url('dashboard/notifications/notification.js') }}"></script> -->
<!-- <script src="{{ url('firebase-messaging-sw.js') }}"></script> -->
<!-- END: firebase -->




<!-- BEGIN: Theme JS-->
<script src="{{ url('dashboard/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
<script src="{{ url('dashboard/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
<script src="{{ url('dashboard/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ url('dashboard/app-assets/js/core/app.js') }}"></script>
<script src="{{ asset('dashboard/app-assets/js/scripts/ui/ui-feather.js') }}"></script>
<!-- END: Theme JS-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<!-- BEGIN: JQuery Validation -->
<script src="{{ asset('dashboard/app-assets/vendors/js/forms/validation/jquery.validate.min.js') }}"></script>
<!-- END: JQuery Validation -->


<!-- BEGIN: Page Vendor JS-->

<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/datatables.checkboxes.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>-->
<!--<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/dataTables.rowGroup.min.js') }}"></script>-->
<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
<script src="{{ url('dashboard/app-assets/vendors/js/tables/datatable/responsive.bootstrap4.js') }}"></script>
{{-- <script src="{{ url('dashboard/app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script> --}}

<script src="{{ url('dashboard/app-assets/js/scripts/tables/table-datatables-advanced.js') }}"></script>
<script type="text/javascript"
src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.all.min.js"></script>
<script src="{{asset('dashboard/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
<!-- END: Page Vendor JS-->

{{-- Arabic Scripts start --}}
@if (\App::getLocale() == 'ar')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/localization/messages_ar.min.js"></script>
@endif
{{-- Arabic Scripts End --}}

@if (\App::getLocale() == 'ar')
    <script>
        $('table:not(.table-striped)').DataTable({
            "language": {
                "url": "{{ url('dashboard/app-assets/js/arabic.json') }}"
            },
            "paging": false,
            "autoWidth": false

        });
    </script>
@else
    <script>
        $('table:not(.table-striped)').DataTable({
            "paging": false,
            "autoWidth": false

        });
    </script>

@endif

<script>

    var select = $('.select2');

    select.each(function () {
        var $this = $(this);
        $this.wrap('<div class="position-relative"></div>');
        $this.select2({
        // the following code is used to disable x-scrollbar when click in select input and
        // take 100% width in responsive also
        dropdownAutoWidth: true,
        width: '100%',
        dropdownParent: $this.parent()
        });
    });

    $(window).on('load', function() {
        if (feather) {
            feather.replace({
                width: 14,
                height: 14
            });
        }
    });
</script>

@stack('js')

{{-- Sweet Alerts Start --}}
@if (session()->has('success'))
    <script>
        Swal.fire({
            title: "{{ session()->get('success') }}",
            html: "{{ session()->get('html') }}",
            type: 'success',
            timer: 1500,
            showCancelButton: false,
            showConfirmButton: false,
        });
        '{{ session()->forget('success') }}';
    </script>
@endif

@if (session()->has('error'))
    <script>
        Swal.fire({
            title: "{{ session()->get('error') }}",
            html: "{!! session()->get('html') !!}",
            type: 'error',
            timer: 1500,
            showCancelButton: false,
            showConfirmButton: false,
        });
        '{{ session()->forget('error') }}';
    </script>
@endif

@if (session()->has('warning'))
    <script>
        Swal.fire({
            title: "{{ session()->get('warning') }}",
            html: "{{ session()->get('html') }}",
            type: 'warning',
            timer: 1500,
            showCancelButton: false,
            showConfirmButton: false,
        });
        '{{ session()->forget('warning') }}';
    </script>
@endif


@if (\Request::route()->getName() != 'admin.employees.create' && \Request::route()->getName() != 'admin.employees.edit' && \Request::route()->getName() != 'admin.currencies.create' && \Request::route()->getName() != 'admin.currencies.edit' && \Request::route()->getName() != 'admin.campaigns.create' && \Request::route()->getName() != 'admin.campaigns.edit' && \Request::route()->getName() != 'admin.setting.sa' && \Request::route()->getName() != 'admin.questions.create' && \Request::route()->getName() != 'admin.questions.edit')

    {{-- @foreach ($errors->all() as $error)
        <script>
            Swal.fire({
                title: "{{ $error }}",
                icon: 'error',
                timer: 2500,
                showCancelButton: false,
                showConfirmButton: false,
            });
        </script>
    @endforeach --}}
@endif

{{-- Sweet Alerts End --}}

{{-- Remove Alert Start --}}
<script>
    $(document).on('click', '.collapsed .remove-table', function() {
        $(this).closest('tr.child').prev(".parent").remove();
        $(this).closest('tr').remove();
    });


    $(document).on('click', '.remove-table', function(e) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        });

        swalWithBootstrapButtons.fire({
            title: '{{ trans('admin.are_you_sure') }}',
            type: 'warning',
            html: "{{ trans('admin.are_you_sure_text') }}",
            showCancelButton: true,
            confirmButtonText: '{{ trans('admin.yes') }}',
            cancelButtonText: '{{ trans('admin.no') }}',
            showLoaderOnConfirm: true,

        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: $(this).data('url'),
                    type: "POST",
                    dataType: 'html',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        _method: "delete"
                    },
                    success: function(data) {
                        data = JSON.parse(data);

                        if (data.data == 1) {
                            swalWithBootstrapButtons.fire({
                                type: 'success',
                                title: '{{ trans('admin.deleted_success') }}',
                                showConfirmButton: false,
                                timer: 1500
                            });
                        } 
                        else if (data.data == 2) {
                            swalWithBootstrapButtons.fire({
                                type: 'error',
                                title: '{{ trans('admin.do_not_have_permission') }}',
                                showConfirmButton: false,
                                timer: 1000
                            });
                        } 
                        else if (data.data == 3) {
                            swalWithBootstrapButtons.fire({
                                type: 'error',
                                title: '{{ trans('admin.delettion_can_not_be_completed') }}',
                                html: '{{ trans('admin.delettion_can_not_be_completed') }}',
                                showConfirmButton: false,
                                timer: 1000
                            });
                        } 
                        else if (data.data == 0) {
                            swalWithBootstrapButtons.fire({
                                type: 'error',
                                title: '{{ trans('admin.add_error') }}',
                                showConfirmButton: false,
                                timer: 1000
                            });
                        } 
                        else {

                            if (data.data.charity_deleted == 1) {

                                if (data.data.support_status == 'transfered') {

                                    swalWithBootstrapButtons.fire({
                                        type: 'success',
                                        title: '{{ trans('admin.supports_transfered') }}',
                                        html: data.data.notify_supporters == 1 ?
                                            '{{ trans('admin.suppoters_notification_sent') }}' :
                                            '{{ trans('admin.suppoters_notification_not_sent') }}',
                                        showConfirmButton: false,
                                        timer: 2000
                                    });

                                } else if (data.data.support_status == 'frozen') {
                                    swalWithBootstrapButtons.fire({
                                        type: 'success',
                                        title: '{{ trans('admin.supports_frozen') }}',
                                        html: data.data.notify_supporters == 1 ?
                                            '{{ trans('admin.suppoters_notification_sent') }}' :
                                            '{{ trans('admin.suppoters_notification_not_sent') }}',
                                        showConfirmButton: false,
                                        timer: 2000
                                    });
                                }
                                else{
                                    swalWithBootstrapButtons.fire({
                                        type: 'success',
                                        title: '{{ trans('admin.deleted_success') }}',
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                }
                            } else {

                                swalWithBootstrapButtons.fire({
                                    type: 'error',
                                    title: '{{ trans('admin.add_error') }}',
                                    showConfirmButton: false,
                                    timer: 2000
                                });
                            }
                        }

                        if ($(this).data('redirect_to_main') == 'charity') {

                            window.location.href = '{{ route('admin.charities.index') }}';
                        } else if ($(this).data('redirect_to_main') == 'project') {

                            window.location.href = '{{ route('admin.projects.index') }}';
                        }
                    }
                });

                $(this).closest('tr').remove();
            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });

    });


    var lang = $('#lang').val();

    if(lang == 'ar'){

        var error_msg = 'من فضلك أدخل سبب التعديل';
    }
    else{
        var error_msg = 'Please enter the reson to update';
    }

    $(document).on('click', '.send-request', function(e) {

        if(!$('#notes').val()){

            $('.error-notes').html(error_msg);
        }
        else{

            $('.error-notes').html('');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger'
                },
                buttonsStyling: true
            });

            swalWithBootstrapButtons.fire({
                title: $(this).attr('sure'),
                type: 'question',
                html: $(this).attr('text-sure'),
                showCancelButton: true,
                confirmButtonText: '{{ trans('admin.yes') }}',
                cancelButtonText: '{{ trans('admin.no') }}',
            }).then((result) => {

                var item = $(this).attr('id');
                var url = $(this).data('url')

                if (result.value) {

                    $.ajax({
                        url: url,
                        type: "POST",
                        dataType: 'html',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            _method: "post",
                            'item': item,
                            'notes': $('#notes').val()
                        },
                        success: function(data) {

                            $('#edit_notes').modal('hide');
                            
                            data = JSON.parse(data);

                            if(data.success == 1){
                                swalWithBootstrapButtons.fire({
                                    type: 'success',
                                    title: data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });
                            }
                            else{

                                $('.error-notes').html(data.message);

                            }
                            

                        },
                        error: function (errors) {
                            for (var k in errors.responseJSON.errors) {
                                $(document).find('.error-'+k).show();
                                $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                            }
                        }
                    });

                    $('.text-send').text("{{ trans('admin.send_success') }}");
                    $(this).toggleClass('btn-outline-info btn-outline-primary disabled');


                }

            });
        }


    });

    // add-repoort
    $(document).on('click', '.add-repoort', function(e) {

        Swal.fire({
            type: 'info',
            title: "{{ __('admin.oops_report_date') }}",
            text: "{{ __('admin.content_oops_report_date') }}",
        })

    });

    $('.change-langouage').click(function(e) {
        e.preventDefault();
        var url = $(this).attr('url');

        $.ajax({
            url: url,
            type: "GET",
            data: {},
            dataType: 'json',
            success: function() {

                location.reload();

            }, // end of success

        }); // end of ajax
    })
</script>

<script>
    $("input[type='file']").change(function() {
        var self = $(this).next("img");
        var self2 = $(this).parents(".media").find(".user-avatar");
        self.fadeIn().css("display", "block");

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    self.attr('src', e.target.result);
                    self2.attr('src', e.target.result);

                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        readURL(this)
    });
    function toEnglishNumber(strNum) {
    var ar = '٠١٢٣٤٥٦٧٨٩'.split('');
    var en = '0123456789'.split('');
    strNum = strNum.replace(/[٠١٢٣٤٥٦٧٨٩]/g, x => en[ar.indexOf(x)]);
    return strNum;
}

$(document).on('keyup', 'input', function (e) {
    var val = toEnglishNumber($(this).val())
    $(this).val(val)
});

</script>

{{-- Remove Alert End --}}
