<!DOCTYPE html>
<html class="loading" lang="{{ App::getLocale() }}" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>

    @yield('meta')

    <title>{{trans('admin.we_help')}} | @yield('title')</title>
    <meta name="description" content="@yield('description')" />
    <meta name="keywords" content="@yield('keywords')" />
    <meta name="image" content="@yield('image')" />
    <meta name="author" content="{{trans('admin.we_help')}}" />
    <meta name="csrf-token" id="token" content="{{ csrf_token() }}" />

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300&display=swap" rel="stylesheet">
    <link rel="apple-touch-icon" href="{{ url('dashboard/app-assets/images/ico/apple-icon-120.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('dashboard/app-assets/images/ico/favicon.ico') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.gstatic.com">


    @include('layout.styles')

</head>
<!-- END: Head-->

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern  navbar-floating footer-static  " data-open="click" data-menu="vertical-menu-modern" data-col="">
    <input type="hidden" id="lang" value="{{\App::getLocale()}}">
    <input type="hidden" id="url-save-token" value="{{  route('save-token') }}">
    <input type="hidden" id="tel_url" value="{{asset('dashboard/js/utils.js')}}">
    <!-- BEGIN: header -->
        @include('layout.header')
    <!-- END: header -->


    <!-- BEGIN: sidebar -->
        @include('layout.sidebar')
    <!-- END: sidebar -->


    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            
            </div>
            <div class="content-body">
                @yield('content')               
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>




    <!-- BEGIN: footer -->
        @include('layout.footer')
    <!-- END: footer -->

