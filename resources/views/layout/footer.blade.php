

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light">
        <div class="row"></div>
        <p class="clearfix mb-0"><span class="float-left d-inline-block">copyrights &copy; 2021</span>
            <a href="https://jaadara.com/" class="float-right d-inline-block" target="_blank">Made with <i data-feather="heart"></i> by jadara </a>
        </p>
    </footer>
    <button class="btn btn-primary btn-icon scroll-top" type="button"><i data-feather="arrow-up"></i></button>
    <!-- END: Footer-->


    @include('layout.scripts')

    @yield('scripts')


</body>
<!-- END: Body-->

</html>