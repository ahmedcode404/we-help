    <!-- BEGIN: Header-->
    <nav class="header-navbar navbar navbar-expand-lg align-items-center floating-nav navbar-light navbar-shadow">
        <div class="navbar-container d-flex content">

            <div class="bookmark-wrapper d-flex align-items-center">
                <ul class="nav navbar-nav d-xl-none">
                    <li class="nav-item"><a class="nav-link menu-toggle" href="javascript:void(0);"><i class="ficon" data-feather="menu"></i></a></li>
                </ul>
            </div>
            <ul class="nav navbar-nav align-items-center ml-auto">
                <li class="nav-item dropdown dropdown-language">
                    @if(\App::getLocale() == 'ar')
                        <a class="nav-link dropdown-toggle change-langouage"  url="{{ url('lang/en') }}" data-language="en" aria-haspopup="true" aria-expanded="false"> 
                            <span class="selected-language">EN</span>
                        </a>
                    @else
                        <a class="nav-link dropdown-toggle change-langouage"  url="{{ url('lang/ar') }}" data-language="ar" aria-haspopup="true" aria-expanded="false"> 
                            <span class="selected-language">ع</span>
                        </a>
                    @endif
                </li>

                <li class="nav-item dropdown dropdown-language">
                    @php
                        $session_currency = currencySymbol(session('currency'));
                    @endphp
                    @if(auth()->check())
                        <a class="nav-link dropdown-toggle"  href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                            {{-- <span>{{auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee') ? auth()->user()->currency->symbol : Auth::user()->currency->symbol}}</span> --}}
                            <span>{{$session_currency}}</span>

                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-flag">
                            @foreach($currencies as $currency)
                                <a class="dropdown-item" href="{{auth()->check() && auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee') ? route('admin.main.exchange_currency', $currency->id) : route('charity.main.exchange_currency', $currency->id)}}">{{$currency->symbol}}</a>
                            @endforeach
                        </div>
                    @endif
                </li>

                <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-style"><i class="ficon" data-feather="moon"></i></a></li>




                @if(auth()->check() || auth()->guard('charity')->check())
                    @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('employee'))

                        <li class="nav-item dropdown dropdown-notification mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up count-notify-charity">{{ isset($admin_notifications) ? $admin_notifications->count() : 0 }}</span></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header d-flex">
                                        <h4 class="notification-title mb-0 mr-auto">{{ __('admin.notifications') }}</h4>
                                        <div class="badge badge-pill badge-light-primary">{{ isset($admin_notifications) ? $admin_notifications->count() : '' }} {{ __('admin.new') }}</div>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list">
                                    @if(auth()->user()->hasRole('admin') && isset($admin_notifications)  && $admin_notifications->count())
                                        @foreach($admin_notifications as $admin_notification)

                                            <a class="d-flex" 
                                                href="@if($admin_notification->title == 'new_project' || 
                                                            $admin_notification->title == 'update_project')  
                                                            {{ route('admin.projects.get-project-with-status' , 'waiting' ) }} 
                                                        @elseif($admin_notification->title == 'edit_project') 
                                                            {{ route('admin.requests.index', 'project' ) }} 
                                                        @elseif($admin_notification->title == 'new_charity') 
                                                            {{ route('admin.charities.get-with-status' , 'waiting' ) }} 
                                                        @elseif($admin_notification->title == 'edit_charity') 
                                                            {{ route('admin.requests.index', 'charity') }}  
                                                        @else javascript:void(0) @endif">
                                                <div class="media d-flex align-items-start">
                                                    <div class="media-left">
                                                        <div class="avatar bg-light-success">
                                                            <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading"><span class="font-weight-bolder">{{ __('admin.charities') }}</span></p><small class="notification-text">{{ $admin_notification->message }}</small><b class="ml-1">{{ $admin_notification->charity ? $admin_notification->charity->name : '-' }}</b>
                                                    </div>
                                                </div>
                                            </a>

                                        @endforeach
                                    @elseif(auth()->user()->hasRole('employee') && isset($emp_notifications)  && $emp_notifications->count())
                                        @foreach($emp_notifications as $emp_notification)

                                            <a class="d-flex" 
                                                href="@if($emp_notification->title == 'new_project' || 
                                                            $emp_notification->title == 'update_project')  
                                                            {{ route('admin.projects.get-project-with-status' , 'waiting' ) }} 
                                                        @elseif($emp_notification->title == 'edit_project') 
                                                            {{ route('admin.requests.index', 'project' ) }} 
                                                        @elseif($emp_notification->title == 'new_charity') 
                                                            {{ route('admin.charities.get-with-status' , 'waiting' ) }} 
                                                        @elseif($emp_notification->title == 'edit_charity') {{ route('admin.requests.index', 'charity') }}  
                                                        @else javascript:void(0) @endif">
                                                <div class="media d-flex align-items-start">
                                                    <div class="media-left">
                                                        <div class="avatar bg-light-success">
                                                            <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading"><span class="font-weight-bolder">{{ __('admin.charities') }}</span></p><small class="notification-text">{{ $emp_notification->message }}</small><b class="ml-1">{{ $emp_notification->charity->name }}</b>
                                                    </div>
                                                </div>
                                            </a>

                                        @endforeach
                                    @endif
                                </li>
                            </ul>
                        </li> 

                    @else
                        
                        <li class="nav-item dropdown dropdown-notification mr-25"><a class="nav-link" href="javascript:void(0);" data-toggle="dropdown"><i class="ficon" data-feather="bell"></i><span class="badge badge-pill badge-danger badge-up count-notify-charity">{{ isset($charity_notifications) ? $charity_notifications->count() : 0 }}</span></a>
                            <ul class="dropdown-menu dropdown-menu-media dropdown-menu-right">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header d-flex">
                                        <h4 class="notification-title mb-0 mr-auto">{{ __('admin.notifications') }}</h4>
                                        <div class="badge badge-pill badge-light-primary">{{ isset($charity_notifications) ? $charity_notifications->count() : '' }} {{ __('admin.new') }}</div>
                                    </div>
                                </li>
                                <li class="scrollable-container media-list">
                                    @if(isset($charity_notifications) && $charity_notifications->count())
                                        @foreach($charity_notifications as $charity_notification)
                                            @php
                                                if($charity_notification->title == 'project' || 
                                                    $charity_notification->title == 'edit_request_project' || 
                                                    $charity_notification->title == 'report' ||
                                                    $charity_notification->title == 'charity_new_project' ||
                                                    $charity_notification->title == 'charity_project_updated'){

                                                    $route = route('charity.projects.show' , $charity_notification->project->slug );
                                                }
                                                else if($charity_notification->title == 'charity' || 
                                                            $charity_notification->title == 'edit_request_charity' ||
                                                            $charity_notification->title == 'charity_data_updated'){

                                                    $route = route('charity.dashboard.profile');
                                                }
                                                else if($charity_notification->title == 'acceptbondexchange'){
                                                    $route = route('charity.bond.exchange');
                                                }
                                                else{
                                                    $route = 'javascript:void(0)';
                                                }
                                            @endphp
                                            <a class="d-flex" href="{{ $route }}">
                                                <div class="media d-flex align-items-start">
                                                    <div class="media-left">
                                                        <div class="avatar bg-light-success">
                                                            <div class="avatar-content"><i class="avatar-icon" data-feather="check"></i></div>
                                                        </div>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="media-heading"><span class="font-weight-bolder">{{ __('admin.admin') }}</span></p>
                                                        <small class="notification-text">{{ $charity_notification->message }}</small>
                                                        @if($charity_notification->title == 'project' || $charity_notification->title == 'edit_request_project' ||
                                                            $charity_notification->title == 'report' || $charity_notification->title == 'charity_new_project' ||
                                                            $charity_notification->title == 'charity_project_updated')
                                                            
                                                            <b class="ml-1">{{ $charity_notification->project->name }}</b>
                                                            
                                                        @endif
                                                    </div>
                                                </div>
                                            </a>

                                        @endforeach
                                    @endif
                                </li>
                            </ul>
                        </li>                

                    @endif
                @endif

                <li class="nav-item dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-nav d-sm-flex d-none"><span class="user-name font-weight-bolder">
                            @if(auth()->check())
                                {{auth()->user()->name}}
                            @elseif(auth()->guard('charity')->check())
                                {{auth()->guard('charity')->user()->name}}
                            @endif
                            </span><span class="user-status">
                                @if(auth()->check())
                                    {{trans('admin.'.auth()->user()->roles[0]->name)}}
                                @elseif(auth()->guard('charity')->check())
                                    {{trans('admin.'.auth()->guard('charity')->user()->roles[0]->name)}}
                                @endif
                                </span></div>
                                <span class="avatar">
                                    @if(auth()->check())
                                        @if(auth()->user()->image)
                                            <img class="round" src="{{asset('storage/'.auth()->user()->image)}}" alt="avatar" height="40" width="40"><span class="avatar-status-online">
                                        @else
                                            <img class="round" src="{{asset('dashboard/app-assets/images/portrait/small/avatar-s-11.jpg')}}" alt="avatar" height="40" width="40"><span class="avatar-status-online">
                                        @endif
                                    @elseif(auth()->guard('charity')->check())
                                        @if(auth()->guard('charity')->user()->logo)
                                            <img class="round" src="{{asset('storage/'.auth()->guard('charity')->user()->logo)}}" alt="avatar" height="40" width="40"><span class="avatar-status-online">
                                        @else
                                            <img class="round" src="{{asset('dashboard/app-assets/images/portrait/small/avatar-s-11.jpg')}}" alt="avatar" height="40" width="40"><span class="avatar-status-online">
                                        @endif
                                    @endif
                                    </span></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-user">
                        @if(auth()->check() && (auth()->user()->hasRole('charity') || auth()->user()->hasRole('charity_employee')))
                            <a class="dropdown-item" href="{{ route('charity.dashboard.profile') }}"><i class="mr-50" data-feather="user"></i>{{ __('admin.profile') }}</a>
                        @elseif(auth()->user() && (auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee')))
                            <a class="dropdown-item" href="{{ route('admin.profile.edit') }}"><i class="mr-50" data-feather="user"></i>{{ __('admin.profile') }}</a>
                        @endif
                        <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="mr-50" data-feather="power"></i> {{ __('admin.logout') }}</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

       
    <ul class="main-search-list-defaultlist d-none">
        <li class="d-flex align-items-center">
            <a href="javascript:void(0);">
                <h6 class="section-label mt-75 mb-0">Files</h6>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/icons/xls.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;17kb</small>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/icons/jpg.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;11kb</small>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/icons/pdf.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;150kb</small>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between w-100" href="app-file-manager.html">
                <div class="d-flex">
                    <div class="mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/icons/doc.png" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;256kb</small>
            </a>
        </li>
        <li class="d-flex align-items-center">
            <a href="javascript:void(0);">
                <h6 class="section-label mt-75 mb-0">Members</h6>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/portrait/small/avatar-s-8.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/portrait/small/avatar-s-1.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/portrait/small/avatar-s-14.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div>
            </a>
        </li>
        <li class="auto-suggestion">
            <a class="d-flex align-items-center justify-content-between py-50 w-100" href="app-user-view.html">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-75"><img src="{{ url('dashboard/') }}/app-assets/images/portrait/small/avatar-s-6.jpg" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                    </div>
                </div>
            </a>
        </li>
    </ul>

    <ul class="main-search-list-defaultlist-other-list d-none">
        <li class="auto-suggestion justify-content-between">
            <a class="d-flex align-items-center justify-content-between w-100 py-50">
                <div class="d-flex justify-content-start"><span class="mr-75" data-feather="alert-circle"></span><span>No results found.</span></div>
            </a>
        </li>
    </ul>
    <!-- END: Header-->


    