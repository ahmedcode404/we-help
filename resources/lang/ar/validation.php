<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */
    'today' => 'اليوم',
    'phone' => 'رقم الجوال / الهاتف غير صحيح',

    'accepted'             => 'يجب قبول :attribute.',
    'active_url'           => ':attribute لا يُمثّل رابطًا صحيحًا.',
    'after'                => 'يجب على :attribute أن يكون تاريخًا لاحقًا للتاريخ :date.',
    'after_or_equal'       => ':attribute يجب أن يكون تاريخاً لاحقاً أو مطابقاً للتاريخ :date.',
    'alpha'                => 'يجب أن لا يحتوي :attribute سوى على حروف.',
    'alpha_dash'           => 'يجب أن لا يحتوي :attribute سوى على حروف، أرقام ومطّات.',
    'alpha_num'            => 'يجب أن يحتوي :attribute على حروفٍ وأرقامٍ فقط.',
    'array'                => 'يجب أن يكون :attribute ًمصفوفة.',
    'before'               => 'يجب على :attribute أن يكون تاريخًا سابقًا للتاريخ :date.',
    'before_or_equal'      => ':attribute يجب أن يكون تاريخا سابقا أو مطابقا للتاريخ :date.',
    'between'              => [
        'numeric' => 'يجب أن تكون قيمة :attribute بين :min و :max.',
        'file'    => 'يجب أن يكون حجم الملف :attribute بين :min و :max كيلوبايت.',
        'string'  => 'يجب أن يكون عدد حروف النّص :attribute بين :min و :max.',
        'array'   => 'يجب أن يحتوي :attribute على عدد من العناصر بين :min و :max.',
    ],
    'boolean'              => 'يجب أن تكون قيمة :attribute إما true أو false .',
    'confirmed'            => 'حقل التأكيد غير مُطابق للحقل :attribute.',
    'date'                 => ':attribute ليس تاريخًا صحيحًا.',
    'date_equals'          => 'يجب أن يكون :attribute مطابقاً للتاريخ :date.',
    'date_format'          => 'لا يتوافق :attribute مع الشكل :format.',
    'different'            => 'يجب أن يكون الحقلان :attribute و :other مُختلفين.',
    'digits'               => 'يجب أن يحتوي :attribute على :digits رقمًا/أرقام.',
    'digits_between'       => 'يجب أن يحتوي :attribute بين :min و :max رقمًا/أرقام .',
    'dimensions'           => 'الـ :attribute يحتوي على أبعاد صورة غير صالحة.',
    'distinct'             => 'للحقل :attribute قيمة مُكرّرة.',
    'email'                => 'يجب أن يكون :attribute عنوان بريد إلكتروني صحيح البُنية.',
    'ends_with'            => 'يجب أن ينتهي :attribute بأحد القيم التالية: :values',
    'exists'               => 'القيمة المحددة :attribute غير موجودة.',
    'file'                 => 'الـ :attribute يجب أن يكون ملفا.',
    'filled'               => ':attribute إجباري.',
    'gt'                   => [
        'numeric' => 'يجب أن تكون قيمة :attribute أكبر من :value.',
        'file'    => 'يجب أن يكون حجم الملف :attribute أكبر من :value كيلوبايت.',
        'string'  => 'يجب أن يكون طول النّص :attribute أكثر من :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على أكثر من :value عناصر/عنصر.',
    ],
    'gte'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أكبر من :value.',
        'file'    => 'يجب أن يكون حجم الملف :attribute على الأقل :value كيلوبايت.',
        'string'  => 'يجب أن يكون طول النص :attribute على الأقل :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على الأقل على :value عُنصرًا/عناصر.',
    ],
    'image'                => 'يجب أن يكون :attribute صورةً.',
    'in'                   => ':attribute غير موجود.',
    'in_array'             => ':attribute غير موجود في :other.',
    'integer'              => 'يجب أن يكون :attribute عددًا صحيحًا.',
    'ip'                   => 'يجب أن يكون :attribute عنوان IP صحيحًا.',
    'ipv4'                 => 'يجب أن يكون :attribute عنوان IPv4 صحيحًا.',
    'ipv6'                 => 'يجب أن يكون :attribute عنوان IPv6 صحيحًا.',
    'json'                 => 'يجب أن يكون :attribute نصًا من نوع JSON.',
    'lt'                   => [
        'numeric' => 'يجب أن تكون قيمة :attribute أصغر من :value.',
        'file'    => 'يجب أن يكون حجم الملف :attribute أصغر من :value كيلوبايت.',
        'string'  => 'يجب أن يكون طول النّص :attribute أقل من :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على أقل من :value عناصر/عنصر.',
    ],
    'lte'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أصغر من :value.',
        'file'    => 'يجب أن لا يتجاوز حجم الملف :attribute :value كيلوبايت.',
        'string'  => 'يجب أن لا يتجاوز طول النّص :attribute :value حروفٍ/حرفًا.',
        'array'   => 'يجب أن لا يحتوي :attribute على أكثر من :value عناصر/عنصر.',
    ],
    'max'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أصغر من :max.',
        'file'    => 'يجب أن لا يتجاوز حجم الملف :attribute :max كيلوبايت.',
        'string'  => 'يجب أن لا يتجاوز طول النّص :attribute :max حروفٍ/حرفًا.',
        'array'   => 'يجب أن لا يحتوي :attribute على أكثر من :max عناصر/عنصر.',
    ],
    'mimes'                => 'يجب أن يكون ملفًا من نوع : :values.',
    'mimetypes'            => 'يجب أن يكون ملفًا من نوع : :values.',
    'min'                  => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية أو أكبر من :min.',
        'file'    => 'يجب أن يكون حجم الملف :attribute على الأقل :min كيلوبايت.',
        'string'  => 'يجب أن يكون طول النص :attribute على الأقل :min حروفٍ/حرفًا.',
        'array'   => 'يجب أن يحتوي :attribute على الأقل على :min عُنصرًا/عناصر.',
    ],
    'not_in'               => 'العنصر :attribute غير صحيح.',
    'not_regex'            => 'صيغة :attribute غير صحيحة.',
    'numeric'              => 'يجب على :attribute أن يكون رقمًا.',
    'password'             => 'كلمة المرور غير صحيحة.',
    'present'              => 'يجب تقديم :attribute.',
    'regex'                => 'صيغة :attribute .غير صحيحة.',
    'required'             => ':attribute مطلوب.',
    'required_if'          => ':attribute مطلوب في حال ما إذا كان :other يساوي :value.',
    'required_unless'      => ':attribute مطلوب في حال ما لم يكن :other يساوي :values.',
    'required_with'        => ':attribute مطلوب إذا توفّر :values.',
    'required_with_all'    => ':attribute مطلوب إذا توفّر :values.',
    'required_without'     => ':attribute مطلوب إذا لم يتوفّر :values.',
    'required_without_all' => ':attribute مطلوب إذا لم يتوفّر :values.',
    'same'                 => 'يجب أن يتطابق :attribute مع :other.',
    'size'                 => [
        'numeric' => 'يجب أن تكون قيمة :attribute مساوية لـ :size.',
        'file'    => 'يجب أن يكون حجم الملف :attribute :size كيلوبايت.',
        'string'  => 'يجب أن يحتوي النص :attribute على :size حروفٍ/حرفًا بالضبط.',
        'array'   => 'يجب أن يحتوي :attribute على :size عنصرٍ/عناصر بالضبط.',
    ],
    'starts_with'          => 'يجب أن يبدأ :attribute بأحد القيم التالية: :values',
    'string'               => 'يجب أن يكون :attribute نصًا.',
    'timezone'             => 'يجب أن يكون :attribute نطاقًا زمنيًا صحيحًا.',
    'unique'               => 'قيمة :attribute مُستخدمة من قبل.',
    'uploaded'             => 'فشل في تحميل الـ :attribute.',
    'url'                  => 'صيغة الرابط :attribute غير صحيحة.',
    'uuid'                 => ':attribute يجب أن يكون بصيغة UUID سليمة.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

        'role' => 'الوظيفة',
        'phone_num' => 'رقم الجوال',
        'emp_data' => 'البيانات',
        'job_id' => 'الوظيفة',
        'true' => 'بيانات موظف',
        'other' => 'أخرى',
        'password' => 'كلمة المرور',
        'name' => 'الاسم ',
        'charity_category_id' => 'القسم',
        'duration'   => 'المده باليوم',
        'start_date' => 'تاريخ البداية',
        'country_id' => 'الدوله',
        'benef_num'  => 'عدد المستفيدين',
        'goals_ar'   => 'الاهداف  بالعربي',
        'goals_en'   => 'الاهداف  بالانجليزي',
        'desc_en'    => 'الاهداف  بالانجليزي',
        'desc_ar'    => 'الاهداف  بالعربي',
        'long_desc_ar' => 'التفاصيل بالعربي',
        'long_desc_en' => 'التفاصيل بالانجليزي',
        'pdf'          => 'بي دي اف',
        'currency_id'  => 'العملة',
        'cost'         => 'التكلفة',
        'edu_service_id' => 'الخدمات التعليمية',
        'emerg_service_id' => 'خدمات الطوارئ',
        'name_other' => 'اسم اخر',
        'edu_service_other' => 'اسم خدمه تعليمية اخري',
        'emerg_service_other' => 'اسم خدمه طارئه اخري',
        'output_ar' => 'مخرجات المرحله بالعربي',
        'output_en' => 'مخرجات المرحله بالانجليزي',
        'cost_details_ar' => 'التكلفة التفصيليه بالعربي',
        'cost_details_en' => 'التكلفة التفصيليه بالانجليزي',
        'name_ar' => 'اسم المرحلة بالعربي',
        'name_en' => 'اسم المرحلة بالانجليزي',
        'project_id' => 'اسم المشروع',
        'end_date' => 'تاريخ النهاية',
        'order' => 'ترتيب المرحله مطلوب',
        'report_en' => 'التقرير بالانجليزي',
        'report_ar' => 'التقرير بالعربي',
        'vedio' => 'الفيديو',
        'images' => 'صور المشروع',
        'email' => 'البريد  الالكتروني',
        'address_ar' => 'العنوان بالعربي',
        'address_en' => 'العنوان بالانجليزي',
        'strategy_ar' => 'الاستراتيجية بالعربي',
        'strategy_en' => 'الاستراتيجية بالانجليزي',
        'license_end_date' => 'تاريخ إنتهاء الترخيص',
        'license_start_date' => 'تاريخ بداية الترخيص',
        'establish_date' => 'تاريخ التأسيس',
        'branches_num' => 'عدد الفروع',
        'members_num' => 'عدد الأعضاء',
        'contract_date' => 'تاريخ التعاقد',
        'international_name_ar' => 'أسماء المنظمات الدولية بالعربي',
        'international_name_en' => 'أسماء المنظمات الدولية بالانجليزي',
        'representer_name_ar' => 'اسم ممثل الجمعية بالعربي',
        'representer_name_en' => 'اسم ممثل الحمعية بالانجليزي',
        'swift_code' => 'سويفت كود',
        'bank_adress' => 'عنوان البنك',
        'bank_city' => 'مدينة البنك',
        'bank_name' => 'أسم البنك',
        'iban' => 'رقم الأيبان',
        'ambassador_name' => 'إسم السفير',
        'campaign_name' => 'إسم الحملة',
        'campaign_goal_id' => 'هدف الحملة',
        '' => '',

        // agreement
        'charity_register_rules_sa_ar'  => 'قواعد تسجيل الجمعيات الخيرية السعودية بالعربي',
        'charity_register_rules_sa_en'  => 'قواعد تسجيل الجمعيات الخيرية السعودية بالانجليزي',
        'charity_register_rules_uk_ar'  => 'قواعد تسجيل الجمعيات الخيرية الايطاليه بالعربي',
        'charity_register_rules_uk_en'  => 'قواعد تسجيل الجمعيات الخيرية الايطاليه بالانجليزي',    
        'supporter_payment_rules_sa_ar' => 'قواعد الدفع السعودية بالعربية',
        'supporter_payment_rules_sa_en' => 'قواعد الدفع السعودية بالانجليزي',
        'supporter_payment_rules_uk_ar' => 'قواعد الدفع الايطالية بالعربي',
        'supporter_payment_rules_uk_en' => 'قواعد الدفع الايطالية بالانجليزي', 
        
        // contracts
        'title_ar' => 'العنوان بالعربي',
        'title_en' => 'العنوان بالانجليزي',
        'content_ar' => 'المحتوي بالعربي ',
        'content_en' => 'المحتوي بالانجليزي',
        'nation_id' => 'الاصدار',
        'type' => 'نوع المشروع',

        // sponser
        'work_field_id' => 'مجال العمل',
        'name_sponser_ar' => 'اسم الجهه بالعربي',
        'name_sponser_en' => 'اسم الجهه بالانجليزي', 
        
        // employees
        'name_employee_ar' => 'اسم الموظف بالعربي',
        'name_employee_en' => 'اسم الموظف بالانجليزي',
        'street' => 'الشارع',
        'unit_number' => 'رقم الوحده',
        'postal_code' => 'الرمز البريدي',
        'mail_box' => 'الصندوق البريدي',
        'contract_start_date' => 'تاريخ بداية التعاقد',
        'contract_end_date' => 'تاريخ نهاية التعاقد',
        'nation_number' => 'الهوية الوطنية',
        'phone' => 'رقم الجوال',
        'permission' => 'الصلاحيات',
        'degree_id' => 'الدرجه العلمية',

        // CURRENCY
        'name_currency_ar' => 'اسم العمله بالعربي',
        'name_currency_en' => 'اسم العملة بالانجليزي',
        'symbol' => 'الرمز',
        'rate' => 'معدل', 
        
        // donator
        'price ambsaador' => 'مبلغ التبرع',
        'number_input' => 'مبلغ التبرع',


        'question_ar' => 'السؤال بالعربي',
        'question_en' => 'السؤال بالانجليزي',
        'answer_ar' => 'الاجابه بالعربي',
        'answer_en' => 'الاجابه بالانجليزي',       
        'ratings'  => 'التقييم',
        'comment' => 'التعليق',
        'images_1' => 'عدد الصور',
        'stop_monthly_deduction' => 'إيقاف التبرع الشهرى',
        'user_id' => 'رقم المستخدم',
        'device_id' => 'رقم الجهاز',
        'firebase_token' => 'رمز ال Firebase',
        'platform_type' => 'نوع المنصة',
        'user_type' => 'نوع المستخدم',
        'service_option_id' => 'الخدمة',
        'service_feature_id' => 'الخدمة الفرعية',
        'price_ambsaador' => 'التبرع',
        'other_service_option' => 'خدمة أخرى',
        'other_service_feature' => 'خدمة فرعية أخرى',
        'other' => 'أخرى',
        'date' => 'التاريخ',
        'today' => 'اليوم',
        'yesterday' => 'الأمس',
        'time' => 'الوقت',
        'password_confirmation' => 'تأكيد كلمة المرور',
        'country' => 'الدولة',

    ],
];
