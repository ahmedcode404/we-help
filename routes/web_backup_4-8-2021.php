<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\StaticPageController;

// Begin: route namespace charity
// route login charity and admin

// AUTH ROUTES
Route::namespace('Auth')->middleware(['guest'])->group(function(){
    // login
    Route::get('login/{role?}' , 'LoginController@login')->name('admin.login');
    Route::post('login/auth' , 'LoginController@loginAuth')->name('login.auth');
    
    Route::get('authorized'      , 'AuthorizedConrtoller@authorized')->name('authorized');
    Route::get('return-authorized'      , 'AuthorizedConrtoller@returnAuthorized')->name('return.authorized');

    // Forgot password routes
    Route::get('forgot-password' , 'PasswordController@getPasswordForm')->name('forgot-password');
    Route::post('send-link', 'PasswordController@sendResetPasswordLink')->name('send-reset-password-link');
    Route::get('reset-password/{email}/{role}', 'PasswordController@resetPassword')->name('reset-password');
    Route::post('store-new-password', 'PasswordController@storeNewPassword')->name('store-new-password');
    
});

Route::namespace('Auth')->group(function(){
    // route logout
    Route::get('logout' , 'LoginController@logout')->name('admin.logout');
    // route take token firebase
    Route::post('save-token' , 'UserController@saveToken')->name('save-token');

});

// Change language
Route::get('lang/{lang}', 'Web\MainController@lang')->name('lang');



//------------------------------------------------------------------------------------------------------------------------------------------------




// BEGIN: CHARITY ROUTES
Route::middleware(['role:charity', 'auth:charity' , 'lang'])->prefix('charity')->name('charity.')->namespace('Charity')->group(function() {

    // route index dashbaord
    Route::get('/dashboard' , 'DashboardController@index')->name('dashboard');
    // route profile
    Route::get('/dashboard/profile' , 'ProfileController@index')->name('dashboard.profile');
    // route charities
    Route::resource('charities' , 'CharityController');      
    Route::post('send/request/edit/charity' , 'ProfileController@sendRequestEdit')->name('send.request.edit.charity'); 
    // change currency             
    Route::get('exchange/{currency_id}', [App\Http\Controllers\Admin\MainController::class, 'exchangeCurrency'])->name('main.exchange_currency');
    // route contact charity
    Route::get('/contract' , 'ProfileController@contract')->name('contract');
    Route::post('/signature' , 'ProfileController@signature')->name('signature');
    
    Route::middleware(['contract' , 'status'])->group(function() {

        // route projects
        Route::resource('projects' , 'ProjectController'); 
        Route::post('send/request/edit' , 'ProjectController@sendRequestEdit')->name('send.request.edit');              
        Route::get('delete/image/project' , 'ProjectController@deleteImage')->name('delete.image.project');              
        // route phases
        Route::resource('phases' , 'PhasesController');    
        Route::get('get/start/date/project' , 'PhasesController@getStartDateProject')->name('get.start.date.project');              
        // route report
        Route::resource('reports' , 'ReportController');              
        Route::get('delete/image' , 'ReportController@deleteImage')->name('delete.image');              
        Route::get('phase/{phase}' , 'ReportController@create')->name('phase');  
        // sponsers
        Route::resource('sponsers' , 'SponserController');              
        // route cities of country
        Route::get('cities' , 'CityController@cityOfCountry')->name('cities');
        // route service emerg 
        Route::get('services/emerg' , 'ServiceController@emerg')->name('services.emerg');  
        // route service emerg 
        Route::get('services/edu' , 'ServiceController@edu')->name('services.edu');  
        // exchange
        Route::get('order/exchange' , 'ExchangeController@orderExchange')->name('order.exchange');              
        Route::get('bond/exchange' , 'ExchangeController@BondExchange')->name('bond.exchange');              
        Route::get('accept/bond/{id}' , 'ExchangeController@acceptBond')->name('accept.bond');              
        
     });


      
}); // END: CHARITY ROUTES



//--------------------------------------------------------------------------------------------------------------------------------------------------




// BEGIN: ADMIN ROUTES
Route::middleware(['role:admin|employee' , 'auth:web', 'lang'])->prefix('admin')->name('admin.')->namespace('Admin')->group(function(){

    // Admin home
    Route::get('/' , 'HomeController@index')->name('home');

    //Main Controller
    Route::post('get-diff-days', [App\Http\Controllers\Admin\MainController::class, 'diffDays'])->name('get-diff-days');
    Route::post('change-status', [App\Http\Controllers\Admin\MainController::class, 'changeStatus'])->name('main.change-status');
    Route::post('delete-image', [App\Http\Controllers\Admin\MainController::class, 'deleteImage'])->name('main.delete-image');
    Route::get('exchange/{currency_id}', [App\Http\Controllers\Admin\MainController::class, 'exchangeCurrency'])->name('main.exchange_currency');

    // Profile (Admin + Employee)
    Route::get('profile', [App\Http\Controllers\Admin\MainController::class, 'editProfile'])->name('profile.edit');
    Route::put('profile/{id}', [App\Http\Controllers\Admin\MainController::class, 'updateProfile'])->name('profile.update');


    // Static Pages
    Route::get('static-pages', [StaticPageController::class, 'getStaticPagesByNation'])->name('static-pages.index');
    Route::get('static-pages/{id}/edit', [StaticPageController::class, 'edit'])->name('static-pages.edit');
    Route::patch('static-pages/{id}', [StaticPageController::class, 'update'])->name('static-pages.update');

    // Slider
    Route::resource('sliders', SliderController::class)->except(['index', 'show']);
    Route::get('sliders', [App\Http\Controllers\Admin\SliderController::class, 'index'])->name('sliders.index');

    // Charities
    Route::resource('charities', CharityController::class)->except(['create', 'store']);
    Route::get('charities-status/{status}', [App\Http\Controllers\Admin\CharityController::class, 'getCharityWithStatus'])->name('charities.get-with-status');
    
    // Projects Sponsers
    Route::get('sponsers/{project_id}', [App\Http\Controllers\Admin\SponserController::class, 'create'])->name('sponsers.create');
    Route::post('sponsers', [App\Http\Controllers\Admin\SponserController::class, 'store'])->name('sponsers.store');
    Route::put('sponsers/{id}', [App\Http\Controllers\Admin\SponserController::class, 'update'])->name('sponsers.update');
    Route::delete('sponsers/{id}', [App\Http\Controllers\Admin\SponserController::class, 'destroy'])->name('sponsers.destroy');

    // Project Phases
    Route::get('phases/{project_id}', [App\Http\Controllers\Admin\PhaseController::class, 'create'])->name('phases.create');
    Route::get('phases/{id}/show', [App\Http\Controllers\Admin\PhaseController::class, 'show'])->name('phases.show');
    Route::post('phases', [App\Http\Controllers\Admin\PhaseController::class, 'store'])->name('phases.store');
    Route::get('phases/{id}/edit', [App\Http\Controllers\Admin\PhaseController::class, 'edit'])->name('phases.edit');
    Route::put('phases/{id}/update', [App\Http\Controllers\Admin\PhaseController::class, 'update'])->name('phases.update');
    Route::delete('phases/{id}/delete', [App\Http\Controllers\Admin\PhaseController::class, 'destroy'])->name('phases.destroy');

    // Resources contrllers
    // contacts
    Route::resource('contacts', ContactController::class)->only(['index', 'destroy']);

    // projects
    Route::resource('projects', ProjectController::class)->except(['create', 'store']);
    Route::get('projects-status/{status}', [App\Http\Controllers\Admin\ProjectController::class, 'getProjectWithStatus'])->name('projects.get-project-with-status');
    Route::post('projects-edit-request', [App\Http\Controllers\Admin\MainController::class, 'changeStatus'])->name('projects-edit-request');
    Route::post('projects/delete-image', [App\Http\Controllers\Admin\ProjectController::class, 'deleteImage'])->name('projects.delete-image');
    Route::get('projects/restore/{id}', [App\Http\Controllers\Admin\ProjectController::class, 'restore'])->name('projects.restore');
    
    // edit request
    Route::get('requests/{type}', 'EditRequestController@index')->name('requests.index');
    // Route::get('requests/project', 'EditRequestController@editRequestProject')->name('requests.project');
    // Route::get('requests/charity', 'EditRequestController@editRequestCharity')->name('requests.charity');
    Route::delete('requests/destroy/{edit_request}', 'EditRequestController@destroy')->name('requests.destroy');
    
    // reports
    Route::resource('reports', ReportController::class)->except(['create', 'store']);
    Route::get('report-status/{status}', [App\Http\Controllers\Admin\ReportController::class, 'getReportWithStatus'])->name('reports.get-report-with-status');

    // Parteners
    Route::resource('parteners', PartenerController::class)->except(['show']);

    // Marketing
    Route::resource('marketings', MarketingController::class)->except(['show']);

    // Supporter
    Route::get('supporters', [App\Http\Controllers\Admin\SupporterController::class, 'index'])->name('supporters.index');
    Route::get('supporters/{id}', [App\Http\Controllers\Admin\SupporterController::class, 'show'])->name('supporters.show');
    Route::post('supporters/block/{id}', [App\Http\Controllers\Admin\SupporterController::class, 'blockUser'])->name('supporters.block');

    // Supports
    Route::get('supports', [App\Http\Controllers\Admin\SupportController::class, 'index'])->name('supports.index');
    Route::get('supports-suspended', [App\Http\Controllers\Admin\SupportController::class, 'getSuspendedSupports'])->name('supports.get-suspended-supports');
    Route::post('supports/update/{id}', [App\Http\Controllers\Admin\SupportController::class, 'store'])->name('supports.store');

    Route::get('ratings', [App\Http\Controllers\Admin\RatingController::class, 'index'])->name('ratings.index');
    Route::post('delete-comment/{user_id}/{project_id}', [App\Http\Controllers\Admin\RatingController::class, 'deleteComment'])->name('ratings.delete-comment');
    Route::post('show-for-web/{user_id}/{project_id}', [App\Http\Controllers\Admin\RatingController::class, 'showForWeb'])->name('ratings.show-for-web');

    // Financial Requests routes
    Route::resource('financial-requests', FinancialRequestController::class)->except(['show']);
    Route::get('fin-req/{type}', [App\Http\Controllers\Admin\FinancialRequestController::class, 'getRequestByType'])->name('fin-req');
    Route::get('fin-req/{status}/{type}', [App\Http\Controllers\Admin\FinancialRequestController::class, 'getRequestByStatus'])->name('fin-req.get-req-with-status');
    Route::post('get-charity-projects', [App\Http\Controllers\Admin\FinancialRequestController::class, 'getCharityProejcts'])->name('get-charity-projects');

    // Bills of exchange - سندات الصرف
    Route::resource('financial-bills', FinancialBillController::class)->except(['show']);
    Route::get('bill-req/{type}', [App\Http\Controllers\Admin\FinancialBillController::class, 'getBillByType'])->name('bill-req');
    Route::get('bill-req/{status}/{type}', [App\Http\Controllers\Admin\FinancialBillController::class, 'getBillByStatus'])->name('bill-req.get-req-with-status');

    // Ambassadors
    Route::get('ambassadors', [App\Http\Controllers\Admin\AmbassadorController::class, 'index'])->name('ambassadors.index');


    Route::resources([

        'cities' => CityController::class,
        'job-categories' => JobCategoryController::class,
        'work-fields' => WorkFieldController::class,
        'charity-categories' => CharityCategoryController::class,
        'services' => ServiceController::class,
        'jobs' => JobController::class,
        'degrees' => DegreeController::class,
        'banks' => BankController::class,
        'nationalities' => NationalityController::class,

    ]);

    // ahmed

    // Agreements
    Route::resource('agreements' , AgreementController::class)->only(['create', 'store']);
    // contracts
    Route::resource('contracts' , ContractController::class);
    // settings
    Route::resource('settings' , SettingController::class);
    Route::get('setting' , 'SettingController@edit')->name('setting.edit');
    Route::post('setting/{nation_id}/store' , 'SettingController@update')->name('setting.update'); 
    
    // project servcies
    Route::resource('services' , ServiceController::class);
    // route city
    Route::get('city/country' , 'CityController@cityOfCountry')->name('city.country');
    // jop of jop category
    Route::get('jop/jopCategory' , 'JobCategoryController@jopOfJopCategory')->name('jop.jopCategory');
    // route employee
    Route::resource('employees' , EmployeeController::class);
    // route question
    Route::resource('questions' , QuestionController::class);    
    // route campaigns
    Route::resource('campaigns' , CampaignGoalController::class);
    // route currence
    Route::resource('currencies' , CurrencyController::class);
    // route rating criterias
    Route::resource('rating-criterias' , RatingCriteriaController::class)->only(['edit', 'index' , 'update']);;

}); // END: ADMIN ROUTES

// Employee poltics agreements route
Route::post('agree-politics', [App\Http\Controllers\Admin\EmpAgreementController::class, 'saveAgreement'])->name('emp.agree');

// get exchage - catch voucher
Route::get('voucher/{slug}', [App\Http\Controllers\Admin\FinancialRequestController::class, 'getVoucher'])->name('get-voucher')->middleware('lang');


//-------------------------------------------------------------------------------------------------------------------------------------------------



// BEGIN: WEBSITE ROUTES
Route::get('/aff/{aff}', function($aff){

    $marketing = App\Models\Marketing::where('aff', $aff)->first();

    $marketing->update(['seen' => $marketing->seen + 1]);

    // return redirect()->route('web.home');
    return redirect('/');
    
})->name('aff');

Route::name('web.')->namespace('Web')->middleware(['lang'])->group(function(){

    // comming soon
    // Route::get('/', function(){

    //     return view('logo');
    // });

    // Begin: route namespace web
    Route::get('/', 'HomeController@index')->name('home');
    // store contact ud
    Route::post('contact-us', 'HomeController@storeContactUs')->name('store-contact-us');
    // change currency 
    Route::get('/currency', 'MainController@currency')->name('currency');
    // project index
    Route::get('/projects', 'ProjectController@index')->name('projects');

    // filter projects by country
    Route::get('filter-projects/{id}', 'MainController@filterProjectsByCountry')->name('projects.filter-projects-country');

    // project details
    Route::get('projects/{slug}', 'ProjectController@show')->name('projects.show');

    // rate project
    Route::post('rate-project/{slug}', 'ProjectController@rateProject')->name('projects.rate');
    
    // validate cost donate
    Route::post('/price-donate', 'DonateController@checkPrice')->name('price.donate');
    // sotre donate
    Route::post('/project/store', 'DonateController@store')->name('project.store');
    // store campaign
    Route::post('/campaign/store', 'AmbassadorController@store')->name('campaign.store');
    // get campaign
    Route::get('/Campaign/{affiliate}', 'AmbassadorController@index')->name('campaign');
    
    // donate now
    Route::get('/donate', 'DonateController@index')->name('donate');
    Route::post('/get/project', 'DonateController@getProjectOfCategory')->name('get.project');
    Route::get('/check/project', 'DonateController@checkProject')->name('check.project');

    // statistics
    Route::get('/statistics', 'StatisticController@index')->name('statistics');

    // about
    Route::get('/about', 'StaticPageController@about')->name('about');

    // faq
    Route::get('/faq', 'StaticPageController@faq')->name('faq'); 

    // profile
    Route::get('/profile', 'ProfileController@index')->name('profile');      
    Route::get('/edit-profile', 'ProfileController@edit')->name('edit.profile');      
    Route::post('/update-profile', 'ProfileController@update')->name('update.profile');      
    Route::post('/upload/image', 'ProfileController@uploadImage')->name('upload.image');      
    Route::get('/profile/money', 'ProfileController@profileMoney')->name('profile.money');      
    Route::get('/profile/projects', 'ProfileController@profileProject')->name('profile.projects');      
    Route::post('/donate/month/update', 'ProfileController@updateDonateMonth')->name('donate.month.update');
    Route::post('/donate/month/update', 'ProfileController@updateDonateMonth')->name('donate.month.update');      
    Route::get('/donate/again', 'ProfileController@donateAgain')->name('donate.again');      
    Route::get('/delete/project', 'ProfileController@deleteProject')->name('delete.project');      
    Route::get('/profile/report', 'ProfileController@profileReport')->name('profile.report');      
    Route::get('/report/{id}', 'ProfileController@report')->name('report');      
    Route::get('/setting', 'ProfileController@setting')->name('setting');      
    Route::get('/active/notification', 'ProfileController@activeNotification')->name('active.notification');      
    Route::get('/delete/account', 'ProfileController@deleteAccount')->name('delete.account');      
    Route::get('profile/cart', 'ProfileController@getCart')->name('profile.cart');
    Route::post('profile/add-to-cart', 'ProfileController@addToCart')->name('profile.add-to-cart');
    
    // subscribe
    Route::post('subscribe', 'MainController@subscribe')->name('subscribe');

    
    // contact
    Route::get('/contact', 'ContactController@index')->name('contact');      
    Route::post('/contact/store', 'ContactController@store')->name('contact.store');      

    // Cart Routes
    Route::get('cart', 'CartController@index')->name('carts.index');
    Route::get('get-cart-items', 'CartController@getCartItems');
    Route::post('cart/add', 'CartController@store')->name('add-to-cart');
    Route::post('cart/{id}', 'CartController@removeItem')->name('cart.remove-item');

    // Notifications Routes
    Route::get('notifications', 'NotificationController@index')->name('notifications.index')->middleware('auth');
    Route::post('notifications/{id}', 'NotificationController@removeItem')->name('notifications.remove-item')->middleware('auth');

    // Register
    Route::get('register/{role}', 'RegisterController@getRegisterForm')->name('get-register-form');
    Route::post('register-donor', 'RegisterController@registerDonor')->name('register-donor');
    Route::post('register-charity', 'RegisterController@registerCharity')->name('register-charity');

    // Password
    Route::get('forgot-password/{role}', 'PasswordController@getEmailForm')->name('get-email-form');
    Route::post('send-verification-code/{role}', 'PasswordController@sendVerificationCode')->name('send-verification-code');
    Route::get('resend-code/{email}/{name}/{role}', 'PasswordController@sendCode')->name('resend-code');
    Route::post('verify', 'PasswordController@verify')->name('verify');
    Route::post('change-password', 'PasswordController@changePassword')->name('change-password');

    // Static pages
    Route::get('pages/{key}', 'StaticPageController@index')->name('pages');

    // Search
    Route::post('search', 'MainController@search')->name('search');
    

    Route::get('sendSMS', 'MainController@sendSms')->name('sendSMS');


});

Route::get('create-payment-session/{support_id}', [App\Http\Controllers\Web\PaymentController::class, 'createPaymentSession'])->name('create-payment-session');
Route::get('payment-success/{id}', [App\Http\Controllers\Web\PaymentController::class, 'success'])->name('payment.success');
Route::get('payment-cancle/{id}', [App\Http\Controllers\Web\PaymentController::class, 'cancle'])->name('payment.cancle');
Route::post('get-cities', [App\Http\Controllers\Web\RegisterController::class, 'getCities'])->name('web.get-cities');

// END: WEBSITE ROUTES

// Route::get('test', function(){

//     // initiat connection
//     $stripe = new \Stripe\StripeClient(
//         'sk_test_51JCQI8IoY6AGxvlCOndhF0syV5KFm3XvmxaFzj42VeCE3qFRXnbNauCRwmDeIrmqw1UoekBkUG6OaTigG85kerQo00YPszLvBc'
//       );

//     // create product
//     $product = $stripe->products->create([
//         'name' => 'project',
//       ]);

//     // create plan
//     $plan = $stripe->plans->create([
//         'amount' => 1000,
//         'currency' => 'gbp',
//         'interval' => 'week',
//         'product' => $product->id,
//       ]);

//     // create customer  
//     $customer = $stripe->customers->create([
//         'description' => 'My First Test Customer (created for API docs)',
//         'name' => 'Mohamed',
//         'email' => 'mohamed@example.com'
//       ]);

//       $stripe->customers->createSource(
//         $customer->id,
//         ['source' => 'tok_visa']
//       );

//     // create payment method
//     // $payment_method = $stripe->paymentMethods->create([
//     //     'type' => 'card',
//     //     'card' => [
//     //       'number' => '4242424242424242',
//     //       'exp_month' => 7,
//     //       'exp_year' => 2022,
//     //       'cvc' => '314',
//     //     ],
//     //   ]);

//     // $stripe->paymentMethods->attach(
//     //     $payment_method->id,
//     //     ['customer' => $customer->id]
//     //   );

//     // create subscription  
//     $subscription = $stripe->subscriptions->create([
//         'customer' => $customer->id,
//         'items' => [
//           ['price' => $plan->id],
//         ],
//       ]);

    

     

//     $session = $stripe->checkout->sessions->create([
//         'success_url' => 'https://example.com/success',
//         'cancel_url' => 'https://example.com/cancel',
//         'payment_method_types' => ['card'],
//         'line_items' => [
//           [
//             'price' => $plan->id,
//             'quantity' => 1,
//           ],
//         ],
//         'mode' => 'subscription',
//       ]);
    

// });


// Route::get('test', function(){

//         // create session
//         require '../vendor/autoload.php';
//         \Stripe\Stripe::setApiKey('sk_test_51JCQI8IoY6AGxvlCOndhF0syV5KFm3XvmxaFzj42VeCE3qFRXnbNauCRwmDeIrmqw1UoekBkUG6OaTigG85kerQo00YPszLvBc');

//         header('Content-Type: application/json');

//         $YOUR_DOMAIN = 'http://localhost:4242';

//         $checkout_session = \Stripe\Checkout\Session::create([
//         'payment_method_types' => ['card'],
//         'line_items' => [[
//             'price_data' => [
//             'currency' => 'usd',
//             'unit_amount' => 2000,
//             'product_data' => [
//                 'name' => 'Stubborn Attachments',
//                 'images' => ["https://i.imgur.com/EHyR2nP.png"],
//             ],
//             ],
//             'quantity' => 1,
//         ]],
//         'mode' => 'payment',
//         'success_url' => route('payment.success'),
//         'cancel_url' => route('payment.cancle'),
//         ]);

//         header("HTTP/1.1 303 See Other");
//         header("Location: " . $checkout_session->url);

//         return redirect($checkout_session->url);


//     // \Stripe\Stripe::setApiKey('sk_test_51JCQI8IoY6AGxvlCOndhF0syV5KFm3XvmxaFzj42VeCE3qFRXnbNauCRwmDeIrmqw1UoekBkUG6OaTigG85kerQo00YPszLvBc');

//     // dd(\Stripe\PaymentIntent::create([
//     // 'amount' => 1000,
//     // 'currency' => 'usd',
//     // 'payment_method_types' => ['card'],
//     // 'receipt_email' => 'jenny.rosen@example.com',
//     // ]));
// });





