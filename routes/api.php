<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\MainController;
use App\Http\Controllers\Api\RegisterController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\ForgetPasswordController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\ProjectController;
use App\Http\Controllers\Api\StaticPageController;
use App\Http\Controllers\Api\CartController;
use App\Http\Controllers\Api\DonateController;
use App\Http\Controllers\Api\CampaignController;
use App\Http\Controllers\Api\ProfileController;
use App\Http\Controllers\Api\SettingController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('Api')->middleware(['api_lang'])->group(function(){

    /**
     * Splash
     */
    Route::post('select-currency', [MainController::class, 'selectCurrency']);

    /**
     * General requests
     */
    Route::get('get-currencies', [MainController::class, 'getCurrencies']);
    Route::get('get-countries', [MainController::class, 'getCountries']);
    Route::get('get-campaign-goals', [MainController::class, 'getCampaignGoals']);

    /**
     * Register
     */
    Route::post('register', [RegisterController::class, 'register']);

    /**
     * Login
     */
    Route::post('login', [LoginController::class, 'login']);
    Route::get('logout', [LoginController::class, 'logout']);

    /**
     * Forget password
     */
    Route::post('forget-password', [ForgetPasswordController::class, 'forgetPassword']);
    Route::post('verify-code', [ForgetPasswordController::class, 'verifyCode']);
    Route::post('set-new-password', [ForgetPasswordController::class, 'setNewPassword']);

    /**
     * Home
     */
    Route::get('home', [HomeController::class, 'home']);
    Route::get('get-all-categories', [HomeController::class, 'getAllCategories']);
    Route::get('get-category-projects/{id}', [ProjectController::class, 'getCategoryProjects']);
    Route::post('search', [HomeController::class, 'search']);

    /**
     * Project details
    */
    Route::get('get-project-details/{id}', [ProjectController::class, 'getProjectDetails'])->name('get-project-details');
    Route::post('add-to-cart', [CartController::class, 'addToCart']);
    Route::get('get-all-carts', [CartController::class, 'getAllCarts']);
    Route::post('delete-cart-item', [CartController::class, 'deleteCartItem']);

    /**
     * Static pages
     */
    Route::get('static-pages/{key}', [StaticPageController::class, 'getStaticPages']);

    /**
     * Drawer pages
     */
    Route::get('get-user-data', [StaticPageController::class, 'getUserData']);
    Route::get('get-faq', [StaticPageController::class, 'getFAQ']);
    Route::get('get-contact-us-data', [StaticPageController::class, 'getContactUsData']);
    Route::post('contact-us', [StaticPageController::class, 'contactUs']);

    /**
     * Donate routes
     */
    Route::post('donate-single-project', [DonateController::class, 'Donate']);
    Route::post('create-campaing', [CampaignController::class, 'createCampaing']);
    Route::get('check-user-block', [DonateController::class, 'checkUserBlock']);
    

    /**
     * Profile routes
     */
    // 1- Campaings
    Route::get('get-all-campaings', [CampaignController::class, 'getAllCampaings']);
    Route::get('get-campaing-details/{id}', [CampaignController::class, 'getCampaingDetails']);

    // 2- Edit personal info
    Route::get('get-profile-data', [ProfileController::class, 'getProfileData']);
    Route::post('edit-profile-data', [ProfileController::class, 'editProfileData']);

    // 3- Det donated money
    Route::get('get-donated-money', [ProfileController::class, 'getDonatedMoney']);

    // 4- get categorized donated projects
    Route::get('get-donated-projects', [ProfileController::class, 'getDonatedProjects']);
    Route::post('update-monthly-deduction', [ProfileController::class, 'updateMonthlyDeduction']);

    // 5- Get reports
    Route::get('get-reports', [ProfileController::class, 'getReports']);
    Route::get('get-report-details/{id}', [ProfileController::class, 'getReportDetails']);

    // 6- Change password
    Route::post('change-password', [ProfileController::class, 'ChangePassword']);

    /**
     * Rate project
     */
    Route::get('get-rating-criteria', [ProjectController::class, 'getRatingCriteria']);
    Route::post('rate-project', [ProjectController::class, 'rateProject']);

    /**
     * Settings
     */
    Route::get('delete-account', [SettingController::class, 'deleteAccount']);

    /**
     * Notifications
     */

    // 1- Create device token
    Route::post('create-device-token', [MainController::class, 'createDeviceToken']);

    // 2- Get all notifications
    Route::get('get-all-notifications', [MainController::class, 'getAllNotifications']);

    // 3- Delete notification
    Route::get('delete-notification/{id}', [MainController::class, 'deleteNotification']);

    // 4- Enable/Disable Notifications
    Route::post('enable-disable-notifications', [MainController::class, 'enableDisableNotifications']);

    // 5- Clear notifications
    Route::get('clear-notifications', [MainController::class, 'clearNotifications']);

    // 6- Get unread notification count
    Route::get('get-notifications-count', [MainController::class, 'getNotificationsCount']);

    // test notification
    Route::get('test-notifications/{title}', function($title){

        // Notify Supporters via mobile app

        $users = \App\Models\User::whereHas('roles', function($q){
            $q->where('name', 'supporter');
        })->get();

        $project = \App\Models\Project::find(3);
        
        $notificationRepository = \App::make('App\Repositories\NotificationRepositoryInterface');

        $supprters_arr = [];

        $ar = '';
        $en = '';

        if($title == 'new_voucher'){
            $ar = 'فاتورة تبرع الخاص بك';
            $en = 'Your donation voucher';
            notifyMobile(90, $supprters_arr, 'new_voucher', 'supporter',  $ar, $en, asset('web/images/main/user_avatar.png'));
        }
        else if($title == 'new_project'){
            $ar = 'تم إضافة مشروع جديد';
            $en = 'A New project is added';
            notifyMobile(3, $supprters_arr, 'new_project', 'supporter', $ar,  $en, asset('dashboard/app-assets/images/avatars/avatar.png'));
        }
        else if($title == 'account_blocked'){
            $ar = 'تم حظر الحساب الخاص بك';
            $en = 'Your account has been blocked';
            notifyMobile(null, [13], 'account_blocked', 'supporter',  $ar, $en, asset('web/images/main/user_avatar.png'));
        }
        else if($title == 'account_unblocked'){
            $ar = 'تم إالغاء حظر الحساب الخاص بك';
            $en = 'Your account has been unblocked';
            notifyMobile(null, [13], 'account_unblocked', 'supporter',  $ar, $en, asset('web/images/main/user_avatar.png'));
        }
        else if($title == 'montly_deduction'){
            $ar = 'تم خصم التبرع الشهرى لمشروع'.$project->name;
            $en = 'The monthly donation for '.$project->name.' project has been deducted';
            notifyMobile(null, [13], 'montly_deduction', 'supporter', $ar, $en, asset('web/images/main/user_avatar.png'));
        }
        else if($title == 'support_canceled'){
            $ar = 'تم إلغاء تبرعك لعدم إتمام الدفع';
            $en = 'Your donation has been canceled due to non-payment';
            notifyMobile(null, [13], 'support_canceled', 'supporter', $ar, $en, asset('web/images/main/user_avatar.png'));
        }
        else if($title == 'transfered_donates'){
            $ar = 'تم تحويل تبرعك الخاص بالمشروع إلى مشروع أخر';
            $en = 'Your project donations has been transfered to another project';
            notifyMobile($project->id, $supprters_arr, 'transfered_donates', 'supporter',  $ar, $en, asset('storage/'.$project->image));
        }
        else if($title == 'stop_montly_deduction'){
            $ar = 'تم إيقاف التبرع الشهرى لمشروع '.$project->name.'نظراً لإكتمال التبرعات';
            $en = 'The monthly donation to '.$project->name.' project has been stopped due to the completion of donations';
            notifyMobile($project->id, $supprters_arr, 'stop_montly_deduction', 'supporter',  $ar, $en,  asset('storage/'.$project->image));
        }

        foreach($users as $user){

            array_push($supprters_arr, $user->id);

            $data['message_en'] = $en;
            $data['message_ar'] = $ar;
            $data['send_user_id'] = null;
            $data['model_id'] = $project->id;
            $data['title'] = $title;
            $data['type'] = 'supporter';

            $notificationRepository->create($data);
        }        

        return 'done';
    });
});