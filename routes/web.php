<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\StaticPageController;

// Begin: route namespace charity
// route login charity and admin

// AUTH ROUTES
Route::namespace('Auth')->middleware(['guest', 'lang'])->group(function(){
    // login
    Route::get('login/{role?}' , 'LoginController@login')->name('admin.login');
    Route::post('login/auth' , 'LoginController@loginAuth')->name('login.auth');
    
    Route::get('authorized'      , 'AuthorizedConrtoller@authorized')->name('authorized');
    Route::get('return-authorized'      , 'AuthorizedConrtoller@returnAuthorized')->name('return.authorized');

    // Forgot password routes
    Route::get('forgot-password' , 'PasswordController@getPasswordForm')->name('forgot-password');
    Route::post('send-link', 'PasswordController@sendResetPasswordLink')->name('send-reset-password-link');
    Route::get('reset-password/{email}/{role}', 'PasswordController@resetPassword')->name('reset-password');
    Route::post('store-new-password', 'PasswordController@storeNewPassword')->name('store-new-password');
    
});

Route::namespace('Auth')->group(function(){
    // route logout
    Route::get('logout' , 'LoginController@logout')->name('admin.logout')->middleware('lang');
    // route take token firebase
    Route::post('save-token' , 'UserController@saveToken')->name('save-token');

});

// Change language
Route::get('lang/{lang}', 'Web\MainController@lang')->name('lang');

// Get charity category services
Route::post('get-category-services', 'Web\MainController@getCategoryServices')->name('get-category-services')->middleware('lang');
Route::post('get-service-features', 'Web\MainController@getServiceFeatures')->name('get-service-features')->middleware('lang');


//------------------------------------------------------------------------------------------------------------------------------------------------




// BEGIN: CHARITY ROUTES
Route::middleware(['role:charity|charity_employee', 'auth:charity' , 'lang'])->prefix('charity')->name('charity.')->namespace('Charity')->group(function() {

    // route index dashbaord
    Route::get('/dashboard' , 'DashboardController@index')->name('dashboard');
    // route profile
    Route::get('/dashboard/profile' , 'ProfileController@index')->name('dashboard.profile');
    Route::get('/dashboard/profile/edit' , 'ProfileController@edit')->name('dashboard.profile.edit');
    Route::put('/dashboard/profile/update' , 'ProfileController@update')->name('dashboard.profile.update');
    // route charities
    Route::resource('charities' , 'CharityController');      
    Route::post('send/request/edit/charity' , 'ProfileController@sendRequestEdit')->name('send.request.edit.charity');
    
    // Employee edit profile request
    Route::get('profile-edit', 'ProfileController@editEmployeeProfile')->name('emp-profile.edit');
    Route::put('profile-update', 'ProfileController@UpdateEmployeeProfile')->name('emp-profile.update');

    // change currency             
    Route::get('exchange/{currency_id}', [App\Http\Controllers\Admin\MainController::class, 'exchangeCurrency'])->name('main.exchange_currency');
    // route contact charity
    Route::get('/contract' , 'ProfileController@contract')->name('contract');
    Route::post('/signature' , 'ProfileController@signature')->name('signature');
    
    Route::middleware(['contract' , 'status'])->group(function() {

        // route projects
        Route::resource('projects' , 'ProjectController'); 
        Route::post('send/request/edit' , 'ProjectController@sendRequestEdit')->name('send.request.edit');              
        Route::get('delete/image/project' , 'ProjectController@deleteImage')->name('delete.image.project');              
        // route phases
        Route::resource('phases' , 'PhasesController');    
        Route::get('get/start/date/project' , 'PhasesController@getStartDateProject')->name('get.start.date.project');              
        // route report
        Route::resource('reports' , 'ReportController');              
        Route::get('delete/image' , 'ReportController@deleteImage')->name('delete.image');              
        Route::get('phase/{id}/{type}' , 'ReportController@create')->name('phase');  
        // sponsers
        Route::resource('sponsers' , 'SponserController');              
        // route cities of country
        Route::get('cities' , 'CityController@cityOfCountry')->name('cities');
          
        // exchange
        Route::get('order/exchange' , 'ExchangeController@orderExchange')->name('order.exchange');              
        Route::get('bond/exchange' , 'ExchangeController@BondExchange')->name('bond.exchange');              
        Route::post('accept/bond' , 'ExchangeController@acceptBond')->name('accept.bond');  
        
        // route employee
        Route::resource('employees' , 'EmployeeController');

        // route Job categories
        Route::resource('job-categories' , 'JobCategoryController');
        // jop of jop category
        Route::get('jop/jopCategory' , 'JobCategoryController@jopOfJopCategory')->name('jop.jopCategory');

        // route Jobs
        Route::resource('jobs' , 'JobController');

        // Degrees route
        Route::resource('degrees' , 'DegreeController');

        // Nationalities route
        Route::resource('nationalities' , 'NationalityController');
        
     });


      
}); // END: CHARITY ROUTES



//--------------------------------------------------------------------------------------------------------------------------------------------------




// BEGIN: ADMIN ROUTES
Route::middleware(['role:admin|employee' , 'auth:web', 'lang'])->prefix('admin')->name('admin.')->namespace('Admin')->group(function(){

    // Admin home
    Route::get('/' , 'HomeController@index')->name('home');

    //Main Controller
    Route::post('get-diff-days', [App\Http\Controllers\Admin\MainController::class, 'diffDays'])->name('get-diff-days');
    Route::post('change-status', [App\Http\Controllers\Admin\MainController::class, 'changeStatus'])->name('main.change-status');
    Route::post('delete-image', [App\Http\Controllers\Admin\MainController::class, 'deleteImage'])->name('main.delete-image');
    Route::get('exchange/{currency_id}', [App\Http\Controllers\Admin\MainController::class, 'exchangeCurrency'])->name('main.exchange_currency');

    // Profile (Admin + Employee)
    Route::get('profile', [App\Http\Controllers\Admin\MainController::class, 'editProfile'])->name('profile.edit');
    Route::put('profile/{id}', [App\Http\Controllers\Admin\MainController::class, 'updateProfile'])->name('profile.update');


    // Static Pages
    Route::get('static-pages', [StaticPageController::class, 'getStaticPagesByNation'])->name('static-pages.index');
    Route::get('static-pages/{id}/edit', [StaticPageController::class, 'edit'])->name('static-pages.edit');
    Route::patch('static-pages/{id}', [StaticPageController::class, 'update'])->name('static-pages.update');

    // Slider
    Route::resource('sliders', SliderController::class)->except(['index', 'show']);
    Route::get('sliders', [App\Http\Controllers\Admin\SliderController::class, 'index'])->name('sliders.index');

    // Charities
    Route::get('charities-status/{status}', [App\Http\Controllers\Admin\CharityController::class, 'getCharityWithStatus'])->name('charities.get-with-status');
    Route::get('export-all-charities/{status}', [App\Http\Controllers\Admin\CharityController::class, 'exportAll'])->name('export-all-charities');
    Route::get('export-charity/{id}', [App\Http\Controllers\Admin\CharityController::class, 'exportCharity'])->name('export-charity');

    
    // Projects Sponsers
    Route::get('sponsers/{project_id}', [App\Http\Controllers\Admin\SponserController::class, 'create'])->name('sponsers.create');
    Route::post('sponsers', [App\Http\Controllers\Admin\SponserController::class, 'store'])->name('sponsers.store');
    Route::put('sponsers/{id}', [App\Http\Controllers\Admin\SponserController::class, 'update'])->name('sponsers.update');
    Route::delete('sponsers/{id}', [App\Http\Controllers\Admin\SponserController::class, 'destroy'])->name('sponsers.destroy');

    // Project Phases
    Route::get('phases/{project_id}', [App\Http\Controllers\Admin\PhaseController::class, 'create'])->name('phases.create');
    Route::get('phases/{id}/show', [App\Http\Controllers\Admin\PhaseController::class, 'show'])->name('phases.show');
    Route::post('phases', [App\Http\Controllers\Admin\PhaseController::class, 'store'])->name('phases.store');
    Route::get('phases/{id}/edit', [App\Http\Controllers\Admin\PhaseController::class, 'edit'])->name('phases.edit');
    Route::put('phases/{id}/update', [App\Http\Controllers\Admin\PhaseController::class, 'update'])->name('phases.update');
    Route::delete('phases/{id}/delete', [App\Http\Controllers\Admin\PhaseController::class, 'destroy'])->name('phases.destroy');

    // Resources contrllers
    // contacts
    Route::resource('contacts', ContactController::class)->only(['index', 'destroy']);

    // projects
    Route::get('projects-status/{status}', [App\Http\Controllers\Admin\ProjectController::class, 'getProjectWithStatus'])->name('projects.get-project-with-status');
    Route::post('projects-edit-request', [App\Http\Controllers\Admin\MainController::class, 'changeStatus'])->name('projects-edit-request');
    Route::post('projects/delete-image', [App\Http\Controllers\Admin\ProjectController::class, 'deleteImage'])->name('projects.delete-image');
    Route::get('projects/restore/{id}', [App\Http\Controllers\Admin\ProjectController::class, 'restore'])->name('projects.restore');
    Route::get('export-all-projects/{status}', [App\Http\Controllers\Admin\ProjectController::class, 'exportAll'])->name('export-all-projects');
    Route::get('export-project/{id}', [App\Http\Controllers\Admin\ProjectController::class, 'exportProject'])->name('export-project');
    Route::post('get-charity-categories', [App\Http\Controllers\Admin\ProjectController::class, 'getCharityCategories'])->name('get-charity-categories');
    
    // edit request
    Route::get('requests/{type}', 'EditRequestController@index')->name('requests.index');
    Route::delete('requests/destroy/{edit_request}', 'EditRequestController@destroy')->name('requests.destroy');
    
    // reports
    Route::resource('reports', ReportController::class)->except(['create', 'store']);
    Route::get('create-report/{id}', [App\Http\Controllers\Admin\ReportController::class, 'createReport'])->name('reports.create-report');
    Route::post('create-report/{id}', [App\Http\Controllers\Admin\ReportController::class, 'storeReport'])->name('reports.store-report');
    Route::get('report-status/{status}', [App\Http\Controllers\Admin\ReportController::class, 'getReportWithStatus'])->name('reports.get-report-with-status');

    // Parteners
    Route::resource('parteners', PartenerController::class)->except(['show']);

    // Marketing
    Route::resource('marketings', MarketingController::class)->except(['show']);
    Route::get('marketings/get-by-type/{type}', [App\Http\Controllers\Admin\MarketingController::class, 'getByType'])->name('get-marketings-by-type');

    // Supporter
    Route::get('supporters', [App\Http\Controllers\Admin\SupporterController::class, 'index'])->name('supporters.index');
    Route::get('supporters/{id}', [App\Http\Controllers\Admin\SupporterController::class, 'show'])->name('supporters.show');
    Route::post('supporters/block/{id}', [App\Http\Controllers\Admin\SupporterController::class, 'blockUser'])->name('supporters.block');

    // Supports
    Route::get('supports', [App\Http\Controllers\Admin\SupportController::class, 'index'])->name('supports.index');
    Route::get('supports-suspended', [App\Http\Controllers\Admin\SupportController::class, 'getSuspendedSupports'])->name('supports.get-suspended-supports');
    Route::post('supports/update/{id}', [App\Http\Controllers\Admin\SupportController::class, 'store'])->name('supports.store');
    Route::get('supports-suspended/{slug}', [App\Http\Controllers\Admin\SupportController::class, 'showSuspendedSupports'])->name('supports.show-suspended-supports');
    Route::get('suspended-supports-supporters', [App\Http\Controllers\Admin\SupportController::class, 'exportSuspendedSupportsSupporters'])->name('export-suspended-supports-supporters');

    Route::get('ratings', [App\Http\Controllers\Admin\RatingController::class, 'index'])->name('ratings.index');
    Route::post('delete-comment/{user_id}/{project_id}', [App\Http\Controllers\Admin\RatingController::class, 'deleteComment'])->name('ratings.delete-comment');
    Route::post('show-for-web/{user_id}/{project_id}', [App\Http\Controllers\Admin\RatingController::class, 'showForWeb'])->name('ratings.show-for-web');

    // VIP Supports
    Route::get('vip-support', [App\Http\Controllers\Admin\SupportController::class, 'getVIPSupport'])->name('supports.vip-support');
    Route::get('add-support', [App\Http\Controllers\Admin\SupportController::class, 'createVIPSupport'])->name('supports.create-vip-support');
    Route::post('add-support', [App\Http\Controllers\Admin\SupportController::class, 'storeVIPSupport'])->name('supports.store-vip-support');
    Route::get('edit-support/{id}', [App\Http\Controllers\Admin\SupportController::class, 'editVIPSupport'])->name('supports.edit-vip-support');
    Route::put('edit-support/{id}', [App\Http\Controllers\Admin\SupportController::class, 'UpdateVIPSupport'])->name('supports.update-vip-support');

    // Financial Requests routes
    /**
     * financial-requests -> عرض جميع طلبات الصرف - نوعها صرف فقط
     * financial-requests-status/{status} -> عرض طلبات الصرف حسب الحاله: الكل - قيد الانتظار - معلق -معتمد - مرفوض
    */
    Route::resource('financial-requests', FinancialRequestController::class)->except(['show']);
    Route::get('financial-requests-status/{status}', [App\Http\Controllers\Admin\FinancialRequestController::class, 'getRequestByStatus'])->name('financial-requests-status');
    Route::post('get-charity-projects', [App\Http\Controllers\Admin\FinancialRequestController::class, 'getCharityProejcts'])->name('get-charity-projects');

    // Bills of exchange - سندات الصرف
    /**
     * financial-bills -> عرض جميع السندات سواء صرف او قبض
     * financial-bills-status/{status} -> عرض  جميع السندات الصرف و القبض حسب الحاله: قيد الانتظار - مرفوض - معتمد - معلق
     * financial-bills/{type} -> عرض السندات حسب النوع: صرف - قبص
     * financial-bills-exchange/{status} -> عرض سندات الصرف حسب الحاله: قيد الانتظار - معلق - معتمد - مرفوض
     * financial-bills-catch/{status} -> عرض سندات القبض حسب الحالة: قيد الانتظار - معلق - مرفوض - معتمد
    */
    Route::resource('financial-bills', FinancialBillController::class)->except(['show']);
    Route::get('financial-bills-status/{status}', [App\Http\Controllers\Admin\FinancialBillController::class, 'getAllBillsByStatus'])->name('financial-bills-status');
    Route::get('financial-bills/{type}', [App\Http\Controllers\Admin\FinancialBillController::class, 'getBillByType'])->name('financial-bills');
    Route::get('financial-bills-exchange/{status}', [App\Http\Controllers\Admin\FinancialBillController::class, 'getBillByStatus'])->name('financial-bills-exchange');
    Route::get('financial-bills-catch/{status}', [App\Http\Controllers\Admin\FinancialBillController::class, 'getBillByStatus'])->name('financial-bills-catch');
    Route::post('get-charity-iban', [App\Http\Controllers\Admin\FinancialBillController::class, 'getCharityIban'])->name('get-charity-iban');
    Route::post('change-revceive-status', [App\Http\Controllers\Admin\FinancialBillController::class, 'changeRevceiveStatus'])->name('financial-bills.change-revceive-status');

    // Ambassadors
    Route::get('ambassadors', [App\Http\Controllers\Admin\AmbassadorController::class, 'index'])->name('ambassadors.index');

    // Edit logs
    Route::get('logs', [App\Http\Controllers\Admin\LogController::class, 'index'])->name('logs.index');


    Route::resources([

        'cities' => CityController::class,
        'job-categories' => JobCategoryController::class,
        'work-fields' => WorkFieldController::class,
        'charity-categories' => CharityCategoryController::class,
        'services' => ServiceController::class,
        'service-features' => ServiceFeatureController::class,
        'jobs' => JobController::class,
        'degrees' => DegreeController::class,
        'banks' => BankController::class,
        'charities' => CharityController::class,
        'nationalities' => NationalityController::class,
        'projects' => ProjectController::class,

    ]);

    // ahmed

    // Agreements
    Route::resource('agreements' , AgreementController::class)->only(['create', 'store']);
    // contracts
    Route::resource('contracts' , ContractController::class);
    // settings
    Route::resource('settings' , SettingController::class);
    Route::get('setting' , 'SettingController@edit')->name('setting.edit');
    Route::post('setting/{nation_id}/store' , 'SettingController@update')->name('setting.update'); 
    
    // project servcies
    Route::resource('services' , ServiceController::class);
    // route city
    Route::get('city/country' , 'CityController@cityOfCountry')->name('city.country');
    // jop of jop category
    Route::get('jop/jopCategory' , 'JobCategoryController@jopOfJopCategory')->name('jop.jopCategory');
    // route employee
    Route::resource('employees' , EmployeeController::class);
    // route question
    Route::resource('questions' , QuestionController::class);    
    // route campaigns
    Route::resource('campaigns' , CampaignGoalController::class);
    // route currence
    Route::resource('currencies' , CurrencyController::class);
    // route rating criterias
    Route::resource('rating-criterias' , RatingCriteriaController::class)->only(['edit', 'index' , 'update']);;

}); // END: ADMIN ROUTES

// Employee poltics agreements route
Route::post('agree-politics', [App\Http\Controllers\Admin\EmpAgreementController::class, 'saveAgreement'])->name('emp.agree');

// get exchage - catch voucher
Route::get('voucher/{slug}', [App\Http\Controllers\Admin\FinancialRequestController::class, 'getVoucher'])->name('get-voucher')->middleware('lang');


//-------------------------------------------------------------------------------------------------------------------------------------------------



// BEGIN: WEBSITE ROUTES
Route::get('/aff/{aff}', [App\Http\Controllers\Admin\MarketingController::class, 'aff'])->name('aff');

Route::name('web.')->namespace('Web')->middleware(['lang'])->group(function(){

    // comming soon
    // Route::get('/', function(){

    //     return view('logo');
    // });

    // Begin: route namespace web
    Route::get('/', 'HomeController@index')->name('home');
    // store contact ud
    Route::post('contact-us', 'HomeController@storeContactUs')->name('store-contact-us');
    // change currency 
    Route::get('/currency', 'MainController@currency')->name('currency');
    // project index
    Route::get('/projects', 'ProjectController@index')->name('projects');

    // category-projects
    Route::get('category/{slug}/projects', 'ProjectController@getCategoryProjects')->name('get-category-projects');

    // filter projects by country
    Route::get('filter-projects/{id}', 'MainController@filterProjectsByCountry')->name('projects.filter-projects-country');

    // project details
    Route::get('projects/{slug}', 'ProjectController@show')->name('projects.show');

    // rate project
    Route::post('rate-project/{slug}', 'ProjectController@rateProject')->name('projects.rate');
    
    // validate cost donate
    Route::post('/price-donate', 'DonateController@checkPrice')->name('price.donate');
    // sotre donate
    Route::post('/project/store', 'DonateController@store')->name('project.store');
    // store campaign
    Route::post('/campaign/store', 'AmbassadorController@store')->name('campaign.store');
    // get campaign
    Route::get('/campaign/{affiliate}', 'AmbassadorController@index')->name('campaign');
    
    // donate now
    Route::get('/donate', 'DonateController@index')->name('donate');
    Route::post('/get/project', 'DonateController@getProjectOfCategory')->name('get.project');
    Route::get('/check/project', 'DonateController@checkProject')->name('check.project');

    // statistics
    Route::get('/statistics', 'StatisticController@index')->name('statistics');

    // about
    Route::get('/about', 'StaticPageController@about')->name('about');

    // faq
    Route::get('/faq', 'StaticPageController@faq')->name('faq'); 

    // profile
    Route::get('/profile', 'ProfileController@index')->name('profile');      
    Route::get('/edit-profile', 'ProfileController@edit')->name('edit.profile');      
    Route::post('/update-profile', 'ProfileController@update')->name('update.profile');      
    Route::post('/upload/image', 'ProfileController@uploadImage')->name('upload.image');      
    Route::get('/profile/money', 'ProfileController@profileMoney')->name('profile.money');      
    Route::get('/profile/projects', 'ProfileController@profileProject')->name('profile.projects');      
    Route::post('/donate/month/update', 'ProfileController@updateDonateMonth')->name('donate.month.update');      
    Route::get('/donate/again', 'ProfileController@donateAgain')->name('donate.again');      
    Route::get('/delete/project', 'ProfileController@deleteProject')->name('delete.project');      
    Route::get('/profile/report', 'ProfileController@profileReport')->name('profile.report');      
    Route::get('/report/{slug}', 'ProfileController@report')->name('report');      
    Route::get('/report-details/{slug}', 'ProfileController@getReportDetails')->name('profile.get-report-details');      
    Route::get('/setting', 'ProfileController@setting')->name('setting');      
    Route::get('/active/notification', 'ProfileController@activeNotification')->name('active.notification');      
    Route::get('/delete/account', 'ProfileController@deleteAccount')->name('delete.account');      
    Route::get('profile/cart', 'ProfileController@getCart')->name('profile.cart');
    Route::post('profile/add-to-cart', 'ProfileController@addToCart')->name('profile.add-to-cart');
    
    // subscribe
    Route::post('subscribe', 'MainController@subscribe')->name('subscribe');

    
    // contact
    Route::get('/contact', 'ContactController@index')->name('contact');      
    Route::post('/contact/store', 'ContactController@store')->name('contact.store');      

    // Cart Routes
    Route::get('cart', 'CartController@index')->name('carts.index');
    Route::get('get-cart-items', 'CartController@getCartItems');
    Route::post('cart/add', 'CartController@store')->name('add-to-cart');
    Route::post('cart/{id}', 'CartController@removeItem')->name('cart.remove-item');

    // Notifications Routes
    Route::get('notifications', 'NotificationController@index')->name('notifications.index')->middleware('auth');
    Route::post('notifications/{id}', 'NotificationController@removeItem')->name('notifications.remove-item')->middleware('auth');

    // Register
    Route::get('register/{role}', 'RegisterController@getRegisterForm')->name('get-register-form')->middleware(['guest:charity', 'guest']);
    Route::post('register-donor', 'RegisterController@registerDonor')->name('register-donor')->middleware(['guest:charity', 'guest']);
    Route::post('register-charity', 'RegisterController@registerCharity')->name('register-charity')->middleware(['guest:charity', 'guest']);

    // Password
    Route::get('forgot-password/{role}', 'PasswordController@getEmailForm')->name('get-email-form');
    Route::post('send-verification-code/{role}', 'PasswordController@sendVerificationCode')->name('send-verification-code');
    Route::get('resend-code/{email}/{name}/{role}', 'PasswordController@sendCode')->name('resend-code');
    Route::post('verify', 'PasswordController@verify')->name('verify');
    Route::post('change-password', 'PasswordController@changePassword')->name('change-password');

    // Static pages
    Route::get('pages/{key}', 'StaticPageController@index')->name('pages');

    // Search
    Route::post('search', 'MainController@search')->name('search');
    

    Route::get('sendSMS', 'MainController@sendSms')->name('sendSMS');


});

Route::get('create-payment-session/{support_id}', [App\Http\Controllers\Web\PaymentController::class, 'createPaymentSession'])->name('create-payment-session');
Route::get('payment-success', [App\Http\Controllers\Web\PaymentController::class, 'success'])->name('payment.success')->middleware('lang');
Route::get('payment-cancle', [App\Http\Controllers\Web\PaymentController::class, 'cancle'])->name('payment.cancle')->middleware('lang');
Route::post('get-cities', [App\Http\Controllers\Web\RegisterController::class, 'getCities'])->name('web.get-cities');

// END: WEBSITE ROUTES


// +447875272017 





