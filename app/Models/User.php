<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_ar',
        'name_en',
        'password',
        'email',
        'phone',
        'nation_number',
        // 'city_id',
        // 'country_id',
        'city',
        'country',
        'region',
        'street',
        'unit_number',
        'mail_box',
        'postal_code',
        'contract_start_date',
        'contract_end_date',
        'first_login',
        'agreement_approve',
        'blocked',
        'job_id',
        'nation_id',
        'nationality_id',
        'device_token',
        'degree_id',
        'currency_id',
        'is_notify',
        'image',
        'verification_code',
        'api_token'
    ];

    protected $appends = [
        'donor_total_supports'
    ];
    

    public function getDonorTotalSupportsAttribute(){

        $donated_projects = $this->donates()->whereYear('created_at', '=', date('Y'))->get();
    
        $sum = 0;

        $currencyRepository = \App::make('App\Repositories\CurrencyRepositoryInterface');

        $currency = $currencyRepository->getWhere([['symbol', 'GBP']])->first();

        foreach($donated_projects as $donation){

            $sum += exchange($donation->cost, $donation->currency_id, $currency->id);
        }

        return $sum;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute(){

        // if(App::getLocale() == 'ar'){
        //     return $this->name_ar;
        // }
        // else{
        //     return $this->name_en;
        // }
        return $this->{'name_'.App::getLocale()};
    }

    // get auth supporter notifications count
    public function notificationsCount(){

        return $this->donor_notifications()->where('read', 0)->count();

    }

    // get auth supporter unread notifications
    public function getUnreadNotifications(){

        return $this->donor_notifications()->where('read', 0)->get()->take(3);
    
    }

    // User - Project many to many relation
    public function projects(){
        
        return $this->belongsToMany(Project::class)->withPivot(['cost', 'type', 'nation_id', 'currency_id', 'cost_gbp']);
    }

    // User - FinancialRequest one to many relation
    public function financial_requests(){

        return $this->hasMany(FinancialRequest::class)->where('type', 'catch');
    }

    // Country - Project belongs to relation
    // public function country(){

    //     return $this->belongsTo(City::class , 'country_id');

    // } 

    // City - Project belongs to relation
    // public function city(){

    //     return $this->belongsTo(City::class , 'city_id');

    // }    
     
    // User - Job one to many relation
    public function job(){
        return $this->belongsTo(M_Job::class);
    }

    // Nation(Coutnry) - Employee one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }  
    
    // Degree - User one to many relation
    public function degree(){

        return $this->belongsTo(Degree::class);
    }

    function notifications()
    {
        return $this->hasMany(Notification::class , 'send_user_id' , 'id')->where('type' , 'admin')->orderBy('created_at' , 'desc');
    }

    function donor_notifications()
    {
        return $this->hasMany(Notification::class , 'send_user_id' , 'id')->where('type' , 'supporter')->orderBy('created_at' , 'desc');
    }

    // User - Gift one to many relation
    public function gifts(){

        return $this->hasMany(Gift::class);
    }
    
    // User - Rating one to many relation
    public function ratings(){
        return $this->hasMany(Rating::class);
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    public function carts()
    {
        return $this->hasMany(ProjectUser::class)->where('status' , 'cart');
    }

    public function donates()
    {
        return $this->hasMany(ProjectUser::class)->where([['status' , 'support']]);
    } 
    
    public function ambassador()
    {
        return $this->hasMany(Ambassador::class);
    }  
    
    // user - nationality one to many relation
    public function nationality(){
        
        return $this->belongsTo(Nationality::class);
    }
      
    // User - FinancialRequest morph many relation
    // سندات القبض
    public function catch_vouchers(){
        return $this->morphMany(FinancialRequest::class, 'requestable' )->where('type', 'catch');
    }


    // campaing - user many to many relation
    public function campaing(){

        return $this->belongsToMany(Marketing::class);
    }


    // user - transactions one to may relation
    public function transactions(){ 

        return $this->hasMany(PaymentTransaction::class);
    }

    // user - dwvice tokens relation
    public function device_tokens(){

        return $this->hasMany(DeviceToken::class);
    }

    public function edit_logs(){

        return $this->hasMany(EditLog::class, 'user_id')->where('user_type', 'admin');
    }

}
