<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;
class Notification extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'user_id',
        'send_user_id',
        'model_id',
        'type',
        'message_ar',
        'message_en',
        'image',
        'read',
    ];

    // Charity - Notification one to many relation
    public function charity(){

        return $this->belongsTo(Charity::class , 'send_user_id' , 'id');
    } 
    
    // Project - Notification one to many relation
    public function project(){

        return $this->belongsTo(Project::class , 'model_id' , 'id');
    } 
    
    public function voucher(){

        return $this->belongsTo(ProjectUser::class , 'model_id' , 'id');
    } 

    public function getMessageAttribute()
    {

        return $this->{'message_'.App::getLocale()};

    } // end of get name attribute       

}
