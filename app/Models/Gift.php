<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
    use HasFactory;

    protected $fillable = [
        'sender_nick_name',
        'user_id',
        'gift_owner_name',
        'email',
        'phone',
        'project_id',
    ];

    // User - Gift one to many relation
    public function user(){

        return $this->belongsTo(User::class);
    }

}
