<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'user_type',
        'model_id',
        'model_type',
        'edit_type',
    ];

    
    public function admin_emp(){

        return $this->belongsTo(User::class, 'user_id');
    }

    public function charity_emp(){

        return $this->belongsTo(Charity::class, 'user_id');
    }

    public function project(){

        return $this->belongsTo(Project::class, 'model_id');
    }

    public function charity(){

        return $this->belongsTo(Charity::class, 'model_id');
    }

    public function finance(){

        return $this->belongsTo(FinancialRequest::class, 'model_id');
    }


}
