<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Bank extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_ar',
        'name_en',
        'city_id',
        'address',
        'phone',
        'nation_id',
    ];

    
    public function getNameAttribute(){

        return $this->{'name_'.App::getLocale()};
    }

    // Nation(Coutnry) - Bank one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    } // end of nation

    // Bank - charity one to many relation
    public function charities(){
        return $this->hasMany(Charity::class);
    }

    // Bank - city one to many relation - revsrese
    public function city(){

        return $this->belongsTo(City::class);
    }


}
