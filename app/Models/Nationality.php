<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Nationality extends Model
{
    use HasFactory;

    protected $fillable = ['slug', 'name_ar', 'name_en', 'nation_id', 'guard', 'guard_id'];

    public function getNameAttribute(){

        return $this->{'name_'.App::getLocale()};
    }

    // User - Nationality one to many relation
    public function users(){
        return $this->hasMany(User::class);
    }

    public function charity_emp(){

        return $this->hasMany(Charity::class);
    }
}
