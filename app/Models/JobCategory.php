<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class JobCategory extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'slug',
        'name_ar',
        'name_en',
        'nation_id',
        'guard',
        'guard_id',
    ];

    public function getNameAttribute(){

        return $this->{'name_'.App::getLocale()};
    }

    // JobCategory - Job one to many relation
    public function jobs(){
        return $this->hasMany(M_Job::class);
    }

    // JobCategory - Nation(Country) one to many relation
    public function nation(){
        return $this->belongsTo(City::class, 'nation_id');
    }
}
