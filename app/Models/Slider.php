<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Slider extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'path',
        'title_ar',
        'title_en',
        'text_ar',
        'text_en',
        'appearance',
        'nation_id',
    ];

    // Nation(Coutnry) - Slider one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }

    public function getTextAttribute(){

        return $this->{'text_'.App::getLocale()};
    }

    public function getTitleAttribute(){

        return $this->{'title_'.App::getLocale()};
    }
}
