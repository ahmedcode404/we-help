<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class StaticPage extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'key',
        'type',
        'title_ar',
        'title_en',
        'content_ar',
        'content_en',
        'image',
        'link',
        'nation_id',
    ];

    // Nation(Coutnry) - StaticPage one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }

    // Get name according to language
    public function getTitleAttribute()
    {

        return $this->{'title_'.App::getLocale()};

    }

    public function getContentAttribute(){

        return $this->{'content_'.App::getLocale()};

    }

    public function children(){

        return $this->hasMany(StaticPage::class, 'parent_id');
    }

    // scope get data of all page
    public function scopeAboutUs()
    {
        return $this->where([['key' , 'about_us'] , ['nation_id' , getNationId()]])->first();
    }

    public function scopeAboutCorporation()
    {
        return $this->where([['key' , 'about_corporation'] , ['nation_id' , getNationId()]])->first();
    }  
    
    public function scopeVisionMission()
    {
        return $this->where([['key' , 'vision_and_mission'] , ['nation_id' , getNationId()] , ['parent_id' , NULL]])->first();
    }
    
    public function parent()
    {
        return $this->hasMany(self::class , 'parent_id' , 'id')->where('nation_id' , getNationId());
    }

    public function scopeOurgoal()
    {
        return $this->where([['key' , 'our_goal'] , ['nation_id' , getNationId()]])->first();
    } 
    
    public function scopeOurvalue()
    {
        return $this->where([['key' , 'our_value'] , ['nation_id' , getNationId()] , ['parent_id' , null]])->first();
    } 
    
    public function scopeOurservice()
    {
        return $this->where([['key' , 'our_services'] , ['nation_id' , getNationId()] , ['parent_id' , null]])->first();
    } 
    
    public function scopeFeature()
    {
        return $this->where([['key' , 'we_help_features'] , ['nation_id' , getNationId()] , ['parent_id' , null]])->first();
    } 
    
    public function scopeSupport()
    {
        return $this->where([['key' , 'supprot_contract'] , ['nation_id' , getNationId()] , ['parent_id' , null]])->first();
    }     
    
}
