<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeviceToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'device_id',
        'firebase_token',
        'platform_type',
        'user_type',
    ];

    // user - device tokens relation
    public function user(){

        return $this->belongsTo(User::class);
    }
}
