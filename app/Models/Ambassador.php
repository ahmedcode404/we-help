<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ambassador extends Model
{
    use HasFactory;

    protected $fillable = [

        'ambassador_name' ,
        'campaign_name' ,
        'affiliate' ,
        'seen' ,
        'user_id' ,
        'project_id' ,
        'campaign_goal_id'
        
    ];  

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function campaign()
    {
        return $this->belongsTo(CampaignGoal::class , 'campaign_goal_id' , 'id');
    } 
    
    public function user(){

        return $this->belongsTo(User::class);
    }

}

           
