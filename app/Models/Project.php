<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Jobs\SendTransfereSupportEmailJob;
use App;

class Project extends Model
{
    use HasFactory, Sluggable;
    
    protected $fillable = [
        'project_num',
        'charity_category_id',
        'name',
        'type',
        'duration',
        'start_date',
        'image',
        'charity_id',
        'location',
        'lat',
        'lng',
        // 'city_id',
        // 'country_id',
        'city',
        'country',
        'benef_num',
        'goals_ar',
        'goals_en',
        'desc_ar',
        'desc_en',
        'long_desc_ar',
        'long_desc_en',
        'attach',
        'eng_maps',
        'cost',
        'status',
        'currency_id',
        'image',
        'nation_id',
        'profile',
        'active',
        'service_option_id',
        'service_feature_id',
        'other_service_option',
        'other_service_feature',
        'slug',
        'donation_complete'
    ];

    protected $appends = [
        'total_supports_for_project',
        // 'currency_name',
        'get_total'
    ];

    protected $casts = [
        'donation_complete' => 'boolean'
    ];

    // public function getCurrencyNameAttribute(){

    //     return currencySymbol($this->currency_id);
        
    // }

    public function getTotalSupportsForProjectAttribute(){

        $sub_sum = 0;

        $currency = currencySymbol(session('currency'));

        $donates = $this->donates()->where('status', 'support')->get();

        foreach($donates as $support){
            
            $sub_sum += generalExchange($support->pivot->cost_gbp , 'GBP', $currency);

        }

        return $sub_sum;

    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    // Get desc for langouage 
    public function getDescAttribute()
    {
        if(App::getLocale() == 'en'){

            return $this->desc_en;
        }
        else{
            if($this->desc_ar != null){

                return $this->desc_ar;
            }
            else{
                return $this->desc_en;
            }
        }

        // return $this->{'desc_'.\App::getLocale()};

    } // end of get name attribute 
    
    // Get goals for langouage 
    public function getGoalAttribute(){

        if(App::getLocale() == 'en'){

            return $this->goals_en;
        }
        else{
            if($this->goals_ar != null){

                return $this->goals_ar;
            }
            else{
                return $this->goals_en;
            }
        }

        // return $this->{'goals_'.\App::getLocale()};
    } // end of get name attribute 
    
    // Get goals for langouage 
    public function getLongDescAttribute()
    {
        if(App::getLocale() == 'en'){

            return $this->long_desc_en;
        }
        else{
            if($this->long_desc_ar != null){

                return $this->long_desc_ar;
            }
            else{
                return $this->long_desc_en;
            }
        }

    } // end of get name attribute     

    // Charity - Project one to many relation
    public function charity(){

        return $this->belongsTo(Charity::class);
    }

    // Category - Project belongs to relation
    public function category(){

        return $this->belongsTo(CharityCategory::class , 'charity_category_id');

    } // end of category

    // Country - Project belongs to relation
    // public function country(){

    //     return $this->belongsTo(City::class , 'country_id');

    // }
    
    // City - Project belongs to relation
    // public function city(){

    //     return $this->belongsTo(City::class , 'city_id');

    // }

    // Project - Phase one to many relation
    public function phases(){
        return $this->hasMany(Phase::class)->orderBy('order');
    }

    // Project - Report one to many relation
    public function reports(){
        return $this->hasMany(Report::class);
    }

    // Project - Image one to many relation
    // public function images(){
    //     return $this->hasMany(Image::class);
    // }

    public function images(){

        return $this->morphMany(Image::class, 'imageable' );
    }

    // User - Project many to many relation
    public function supports(){
        return $this->belongsToMany(User::class)->withPivot(['cost', 'type', 'nation_id', 'currency_id', 'cost_gbp']);
    }
    
    // User - Project many to many relation
    public function donates(){
        
        return $this->belongsToMany(User::class)->where('status' , 'support')->withPivot(['cost', 'type', 'nation_id', 'currency_id', 'cost_gbp']);
        
    }

    // Project - Sponser one to many relation
    public function sponsers(){
        return $this->hasMany(Sponser::class);
    }

    public function currency(){

        return $this->belongsTo(Currency::class);
        
    }

    // Project - EditRequest one to many relation
    public function edit_requests(){

        return $this->hasMany(EditRequest::class, 'model_id')->where('type', 'project');
    }

    // Nation(Coutnry) - Project one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }

    // Project - Gift one to many relation
    public function gifts(){

        return $this->hasMany(Gift::class);
    }

    // Project - Rating one to many relation
    public function ratings(){
        return $this->hasMany(Rating::class);
    }

    // Project - FinancialRequest one to many relation
    // طلبات الصرف
    public function exchange_requests(){
        return $this->hasMany(FinancialRequest::class)->where('type', 'exchange');
    }

    public function getRatings(){

        $ratings = $this->ratings()->select('user_id', 'project_id')
                    ->selectRaw("round(AVG(grade),0) AS avg_grade")
                    ->groupBy('project_id', 'user_id')->get();
        $sum = 0;

        foreach($ratings as $rate){

            $sum += $rate->avg_grade;
        }

        if (count($ratings) > 0) {

            return $sum / count($ratings);
            
        } else  
        {
            return 0;
        }
    
    }

    public function getFinishedPhases(){

        return count($this->phases()->where('approved', 1)->get());
    }

    public function getProjectRatings(){

        return $this->ratings()->where('show_for_web', 1)->select('user_id', 'project_id')
                ->selectRaw("round(AVG(grade),0) AS avg_grade")
                ->groupBy('project_id', 'user_id')->get();
    }

    // اجمالى سندات الصرف المصروفه للمشروع 
    public function getTotalProjectExchangeVouchers(){

        $requests = $this->exchange_requests()->where([['bank_account_num', '!=', null], ['voucher_num', '!=', null]])->get();

        $sum = 0;

        foreach($requests as $request){

            $sum += exchange($request->out_to_charity, $request->currency_id, session('currency'));

        }

        return $sum;
    }

    // // Get name county or city for langouage 
    // public function getNameAttribute()
    // {
    //     if (App::getLocale() == 'ar') {

    //         return $this->name_ar;

    //     }else 
    //     {

    //         return $this->name_en;

    //     }

    // } // end of get name attribute    

    public function getGetTotalAttribute()
    {

        if($this->category){

            return $this->cost * $this->category->superadmin_ratio / 100 + $this->cost;
        }
        else{

            return $this->cost;
        }

    } // end of get total  

    public function projectUser()
    {
        return $this->belongsTo(ProjectUser::class , 'id' , 'id');
    }
    
    public function scopeProjectOfcountry()
    {
        
        return $this->where([['nation_id' , getNationId()] , ['charity_id' , '!=' , null]])->get();

    } // end of function project of category


    // اشعار المتبرعين بتوع المشروع دا ان التبرع بتاهم هيروح لمشروع تانى
    public function notifySupporters($charity_name){

        // get supporters
        $supporters_ids = ProjectUser::where('project_id', $this->id)->distinct('user_id')->pluck('user_id');

        $supporters_emails = User::whereIn('id', $supporters_ids)->get();

        $data = [
            'users'       => $supporters_emails,
            'name_project' => $this->name,
            'charity_name' => $charity_name,
        ];

        try {
    
            $job = (new SendTransfereSupportEmailJob($data))->delay(now()->addSeconds(2));
    
            dispatch($job); 

            $response = 1;

        } catch (Throwable $e) {

            $response = 0;

        } 

        return $response;
    
    }

    // Project - transactions one to may relation
    public function transactions(){

        return $this->hasMany(PaymentTransaction::class);
    }


    public function edit_logs(){

        return $this->hasMany(EditLog::class, 'model_id')->where('type', 'project');
    }


    public function service_option(){

        return $this->hasOne(ServiceOption::class, 'id', 'service_option_id');
    }

    public function service_feature(){

        return $this->hasOne(ServiceOption::class, 'id', 'service_feature_id');
    }

    public function notifications(){
        return $this->hasMany(Notification::class, 'model_id');
    }



}
