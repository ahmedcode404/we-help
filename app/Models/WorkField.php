<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class WorkField extends Model
{
    use HasFactory;

    // protected $with = ['nation'];
    
    protected $fillable = [
        'name_ar',
        'name_en',
        'nation_id',
    ];

    // WorkField - Charity many to many relation
    public function charities(){
        return $this->belongsToMany(Charity::class);
    }

    // WorkField - Nation(Country) one to many relation
    public function nation(){
        return $this->belongsTo(City::class, 'nation_id');
    }

    public function getNameAttribute()
    {
        return $this['name_' . App::getLocale()];
    }
}
