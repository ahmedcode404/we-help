<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Marketing extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'type',
        'title',
        'content',
        'date',
        'time',
        'aff',
        'intended_users',
        'nation_id',
        'project_link'
    ];

    protected $dates = ['date', 'time'];

    // Nation - Marketing one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }

    // campaing - user many to many relation
    public function users(){

        return $this->belongsToMany(User::class);
    }


    // get donation amount for this campaing - اجمالى التبرعات اللى جت من المستخدمين اللى  فى  الحملة دى
    public function getDonationAmounts(){

        $users = $this->users;

        $total_donations = 0;

        foreach($users as $user){

            $user_donations = $user->donates()->where('project_user.created_at', '>', $this->date)->orWhere('project_user.created_at', $this->date)->get();
            
            foreach($user_donations as $donation){

                $total_donations += $donation->cost;
            }

        }

        return $total_donations;

    }


    // get doners ratio - نسبة المترعين اللى اتبرعوا بسبب الحمله دى بالنسبه لاجمالى المتبرعين
    public function usersRatio(){

        $all_donors_count = count(User::whereHas('roles', function($q){
                                $q->where('name', 'supporter');
                            })->get());

        $campaing_users = count($this->users()->has('donates', '>', 0)->get());

        if($all_donors_count > 0){

            $ratio = ($campaing_users / $all_donors_count) * 100;
        }
        else{
            $ratio = 0;
        }


        return $ratio;
    }
}
