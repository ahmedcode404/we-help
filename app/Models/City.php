<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use App;

class City extends Model
{
    use HasFactory, Sluggable;

    protected $fillable = [
        'name_ar',
        'name_en',
        'parent_id',
        'nation_id',
        'slug',
        'code',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name_en'
            ]
        ];
    }

    // Get name county or city for langouage 
    public function getNameAttribute()
    {

        return $this->{'name_'.App::getLocale()};


    } // end of get name attribute

    // Country - City one to many relation
    public function cities(){

        return $this->hasMany(Self::class, 'parent_id')->where('parent_id', '!=', null);
    }

    // Country - City one to many relation - reverse
    public function country(){

        return $this->belongsTo(Self::class, 'parent_id');
    }

    // Nation(Coutnry) - City one to many relation - reverse
    public function nation(){

        return $this->belongsTo(Self::class, 'nation_id');
    }

    // Charity - City one to many relation
    public function charities(){

        return $this->hasMany(Charity::class);
    }

    // Bank - city one to many relation
    public function banks(){

        return $this->hasMany(Bank::class);
    }
}
