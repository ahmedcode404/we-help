<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;
class Sponser extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_ar',
        'name_en',
        'work_field_id',
        'project_id',
    ];

    // Get goals for langouage 
    public function getNameAttribute()
    {
        if (App::getLocale() == 'ar') {

            return $this->name_ar;

        }else 
        {

            return $this->name_en;

        }

    } // end of get name attribute     

    // Project - Sponser one to many relation
    public function project(){
        return $this->belongsTo(Project::class);
    }

    // WorkField - Sponser one to many relation
    public function work_field(){
        return $this->belongsTo(WorkField::class);
    }
}
