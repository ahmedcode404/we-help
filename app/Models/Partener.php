<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Partener extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name_ar',
        'name_en',
        'logo',
        'link',
        'nation_id',
    ];


    public function getNameAttribute(){

        return $this->{'name_'.App::getLocale()};
    }

    // Nation - Partener one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }
}
