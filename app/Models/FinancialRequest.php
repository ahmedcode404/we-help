<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FinancialRequest extends Model
{
    use HasFactory;

    protected $fillable = [
        // 'cost',
        // 'total_supports',
        // 'remaining_cost',
        'slug',
        'out_to_charity',
        'recieve_status',
        // 'charity_id',
        'requestable_id',
        'requestable_type',
        'project_id',
        'phase_id',
        'status',
        'notes',
        'type',
        'bank_account_num',
        'voucher_num',
        'currency_id',
        // 'nation_id',
    ];

    // Morph many relation with charity table and users table
    // الجمعيه ليها طلبات صرف
    // المشروع ليه طلبات صرف - التابعه للجمعيه برده
    // المتبرع ليع طلبات قبض
    public function requestable(){
        return $this->morphTo();
    }

    // Project - FinancialRequest one to many relation
    public function project(){
        return $this->belongsTo(Project::class);
    }

    public function edit_logs(){

        return $this->hasMany(EditLog::class, 'model_id')->where('type', 'finance');
    }

    public function currency(){

        return $this->belongsTo(Currency::class);
        
    }

    public function phase(){

        return $this->belongsTo(Phase::class);
    }
}
