<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'key',
        'country',
        'neckname',
        'type',
        'value',
        'nation_id',
    ];

    public function scopePhaseNumber()
    {
        if(getNationId() == 2)
        {
            return $this->where('key' , 'phases_number_uk')->first();
        } else
        {
            return $this->where('key' , 'phases_number_sa')->first();

        }
    }

    public function scopeDataLogin()
    {
        if(getNationId() == 2)
        {

            return $this->where('key' , 'msg_data_login_uk_ar')->first();

        } else
        {

            return $this->where('key' , 'msg_data_login_sa_ar')->first();

        }
    }
    
    public function scopeSlogan()
    {
        if(getNationId() == 2)
        {

            return $this->where('key' , 'we_help_slogan_uk_ar')->first();

        } else
        {

            return $this->where('key' , 'we_help_slogan_uk_ar')->first();

        }
    }    
}
