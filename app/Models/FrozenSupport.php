<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FrozenSupport extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'from_project_id',
        'prev_donations',
        'out_to_charity',
        'to_project_id',
        'to_project_cost',
        'cost',
        'currency_id',
        'supporters_ids',
    ];

    public function fromProject(){

        return $this->belongsTo(Project::class, 'from_project_id');
    }

    public function toProject(){

        return $this->belongsTo(Project::class, 'to_project_id');
    }

    public function currency(){

        return $this->belongsTo(Currency::class);
    }

    public function getSupporters(){

        $supporters_ids = json_decode($this->supporters_ids);

        $supporters = User::whereIn('id', $supporters_ids)->get();

        return $supporters;

    }
}
