<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CharityCategory extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'slug',
        'name_ar',
        'name_en',                                                                                       
        'superadmin_ratio',
        'main_color',
        'sub_color',
        'icon',
        'nation_id',
        'type',
    ];


    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name_en'
            ]
        ];
    }

    // Get name category for langouage 
    public function getNameAttribute()
    {

        if (\App::getLocale() == 'ar') {

            return $this->name_ar;

        }else 
        {

            return $this->name_en;

        }

    } // end of get name attribute    

    public function charities(){

        return $this->belongsToMany(Charity::class, 'charity_charity_category');
    }

    // projects of charity category
    public function projects(){

        return $this->hasMany(Project::class)->where([['nation_id' , getNationId()] , ['charity_id' , '!=' , null] , ['active' , 1]])
                ->orderBy('donation_complete', 'asc');
    }    

    // Nation(Coutnry) - CharityCategory one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');

    }

    public function services(){

        return $this->hasMany(ServiceOption::class);
    }


}
