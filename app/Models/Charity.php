<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class Charity extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    protected $guard_name = 'web';

    protected $table = 'charities';

    protected $fillable = [
        'slug',
        'type',
        'name',
        'license_number',
        'license_start_date',
        'license_end_date',
        'license_file',
        'establish_date',
        'branches_num',
        'members_num',
        'contract_date',
        'strategy_ar',
        'strategy_en',
        'attach',
        'internal_image',
        'year_report',
        'years',
        'password',
        'logo',
        'address_ar',
        'address_en',
        'country',
        'city',
        // 'city_id',
        // 'country_id',
        'phone',
        'mobile',
        'email',
        'representer_name_ar',
        'representer_name_en',
        'representer_passport_image',
        'representer_nation_image',

        // new start
        'representer_title_ar',
        'representer_title_en',
        'representer_title_file',
        'representer_email',
        // new end
        
        'international_member',
        'international_name_ar',
        'international_name_en',
        'website',
        'twitter_link',
        'facebook_link',

        'iban',
        'bank_name',
        'bank_address',
        'bank_city',
        'bank_phone',
        'swift_code',
        
        'contract_after_sign',
        'signature',
        'status',
        'notes',
        'nation_id',
        'device_token',
        'currency_id',
        'verification_code',

        // Employee data
        'name_ar',
        'name_en',
        'nation_number',
        'region',
        'street',
        'unit_number',
        'mail_box',
        'postal_code',
        'contract_start_date',
        'contract_end_date',
        'parent_id',
        'degree_id',
        'nationality_id',
        'job_id',
        'parent_id',
        'work_fields',
    ];


    function notifications()
    {
        return $this->hasMany(Notification::class , 'send_user_id' , 'id')->where('type' , 'charity')->orderBy('created_at' , 'desc');
    } 
    
    // get auth supporter notifications count
    public function notificationsCount(){

        return auth()->guard('charity')->user()->notifications()->where('read', 0)->where('title', 'supporter')->count();

    }

    // get auth supporter unread notifications
    public function getUnreadNotifications(){

        return auth()->guard('charity')->user()->notifications()->where('read', 0)->where('title', 'supporter')->get()->take(3);
    
    }

    public function getAddressAttribute(){

        if(App::getLocale() == 'en'){

            return $this->address_en;
        }
        else{
            if($this->address_ar != null){

                return $this->address_ar;
            }
            else{
                return $this->address_en;
            }
        }
        // return $this->{'address_'.App::getLocale()};
    }

    public function getRepresenterNameAttribute(){

        if(App::getLocale() == 'en'){

            return $this->representer_name_en;
        }
        else{
            if($this->representer_name_ar != null){

                return $this->representer_name_ar;
            }
            else{
                return $this->representer_name_en;
            }
        }

        // return $this->{'representer_name_'.App::getLocale()};
    }

    public function getRepresenterTitleAttribute(){

        if(App::getLocale() == 'en'){

            return $this->representer_title_en;
        }
        else{
            if($this->representer_title_ar != null){

                return $this->representer_title_ar;
            }
            else{
                return $this->representer_title_en;
            }
        }
        // return $this->{'representer_title_'.App::getLocale()};
    }

    public function getStrategyAttribute(){

        if(App::getLocale() == 'en'){

            return $this->strategy_en;
        }
        else{
            if($this->strategy_ar != null){

                return $this->strategy_ar;
            }
            else{
                return $this->strategy_en;
            }
        }
        // return $this->{'strategy_'.App::getLocale()};
    }

    public function getInternationalNameAttribute(){

        if(App::getLocale() == 'en'){

            return $this->international_name_en;
        }
        else{
            if($this->international_name_ar != null){

                return $this->international_name_ar;
            }
            else{
                return $this->international_name_en;
            }
        }

        // return $this->{'international_name_'.App::getLocale()};
    }

    public function getEmpNameAttribute(){

        if(App::getLocale() == 'en'){

            return $this->hasRole('charity_employee') ? $this->name_en : $this->name;
        }
        else{
            if($this->name_ar != null){

                return $this->hasRole('charity_employee') ? $this->name_ar : $this->name;
            }
            else{
                return $this->hasRole('charity_employee') ? $this->name_en : $this->name;
            }
        }
    }

    // CharityCategory - Charity many to many relation
    public function categories(){

        return $this->belongsToMany(CharityCategory::class, 'charity_charity_category');
    }

    // Charity - Project one to many relation
    public function projects(){

        return $this->hasMany(Project::class);
    }

    // Country - Charity belongs to relation
    // public function country(){

    //     return $this->belongsTo(City::class , 'country_id');

    // }  

    // Charity - City one to many relation
    // public function city(){ 

    //     return $this->belongsTo(City::class);
    // }

    // Charity - EditRequest one to many relation
    public function edit_requests(){

        return $this->hasMany(EditRequest::class, 'model_id')->where('type', 'charity');
        
    }

    // Charity - FinancialRequest morph many relation
    // طلبات الصرف
    public function exchange_requests(){
        return $this->morphMany(FinancialRequest::class, 'requestable' )->where('type', 'exchange');
    }

    // Charity - Report one to many relation
    public function reports(){

        return $this->hasMany(Report::class);
    }

    // WorkField - Charity many to many relation
    public function work_fields(){
        return $this->belongsToMany(WorkField::class);
    }

    // Nation(Coutnry) - Charity one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }

    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    // Bank - charity one to many relation - reverse
    public function bank(){
        return $this->belongsTo(Bank::class);
    }

    // اجمالى طلبات الصرف المصروفه للجمعيه 
    public function getTotalOutToCharity()
    {

        $requests = $this->exchange_requests()->where([['bank_account_num', null], ['voucher_num', null]])->get();

        $sum = 0;

        foreach($requests as $request){

            $sum += exchange($request->out_to_charity, $request->currency_id, session('currency'));

        }

        return $sum;

    }
    
    // اجمالى سندات الصرف المصروفه للجمعيه 
    public function getTotalCharityExchangeVouchers(){  

        $requests = $this->exchange_requests()->where([['bank_account_num', '!=', null], ['voucher_num', '!=', null]])->get();

        $sum = 0;

        foreach($requests as $request){

            $sum += exchange($request->out_to_charity, $request->currency_id, session('currency'));

        }

        return $sum;

    }


    // employee - charity one to many relation
    public function employees(){

        return $this->hasMany(Self::class, 'parent_id');
    }

    // employee - charity one to many relation
    public function charity(){

        return $this->belongsTo(Self::class, 'parent_id');
    }

    // Degree - charity employee one to many relation
    public function degree(){

        return $this->belongsTo(Degree::class);
    }

    // charity employee - Job one to many relation
    public function job(){

        return $this->belongsTo(M_Job::class);
    }

    // charity employee - nationality one to many relation
    public function nationality(){
        
        return $this->belongsTo(Nationality::class);
    }

    public function edit_logs(){

        return $this->hasMany(EditLog::class, 'user_id')->where('user_type', 'charity');
    }


    public function createCharityPermissions(){

        // projects permissions
        $charity_projects_management = Role::create(['name' => 'charity_projects_management_'.$this->id, 'type' => 'charity']);
        $charity_projects_management_per = [
                ['name' => 'list_projects_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_projects_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_projects_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_projects_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_projects_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_projects_management_per);
        $charity_projects_management->givePermissionTo([
            'list_projects_'.$this->id,
            'create_projects_'.$this->id,
            'edit_projects_'.$this->id,
            'show_projects_'.$this->id,
            'delete_projects_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_projects_'.$this->id,
        //     'create_projects_'.$this->id,
        //     'edit_projects_'.$this->id,
        //     'show_projects_'.$this->id,
        //     'delete_projects_'.$this->id,
        // ]);

        // phases permissions
        $charity_phases_management = Role::create(['name' => 'charity_phases_management_'.$this->id, 'type' => 'charity']);
        $charity_phases_management_per = [
                ['name' => 'list_phases_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_phases_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_phases_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_phases_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_phases_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_phases_management_per);
        $charity_phases_management->givePermissionTo([
            'list_phases_'.$this->id,
            'create_phases_'.$this->id,
            'edit_phases_'.$this->id,
            'show_phases_'.$this->id,
            'delete_phases_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_phases_'.$this->id,
        //     'create_phases_'.$this->id,
        //     'edit_phases_'.$this->id,
        //     'show_phases_'.$this->id,
        //     'delete_phases_'.$this->id,
        // ]);

        // employees permissions
        $charity_employees_management = Role::create(['name' => 'charity_employees_management_'.$this->id, 'type' => 'charity']);
        $charity_employees_management_per = [
                ['name' => 'list_employees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_employees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_employees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_employees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_employees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_employees_management_per);
        $charity_employees_management->givePermissionTo([
            'list_employees_'.$this->id,
            'create_employees_'.$this->id,
            'edit_employees_'.$this->id,
            'show_employees_'.$this->id,
            'delete_employees_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_employees_'.$this->id,
        //     'create_employees_'.$this->id,
        //     'edit_employees_'.$this->id,
        //     'show_employees_'.$this->id,
        //     'delete_employees_'.$this->id,
        // ]);

        // job categories management
        $charity_job_categories_management = Role::create(['name' => 'charity_job_categories_management_'.$this->id, 'type' => 'charity']);
        $charity_job_categories_management_per = [
                ['name' => 'list_job_categories_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_job_categories_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_job_categories_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_job_categories_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_job_categories_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_job_categories_management_per);
        $charity_job_categories_management->givePermissionTo([
            'list_job_categories_'.$this->id,
            'create_job_categories_'.$this->id,
            'edit_job_categories_'.$this->id,
            'show_job_categories_'.$this->id,
            'delete_job_categories_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_job_categories_'.$this->id,
        //     'create_job_categories_'.$this->id,
        //     'edit_job_categories_'.$this->id,
        //     'show_job_categories_'.$this->id,
        //     'delete_job_categories_'.$this->id,
        // ]);

        // jobs management
        $charity_jobs_management = Role::create(['name' => 'charity_jobs_management_'.$this->id, 'type' => 'charity']);
        $charity_jobs_management_per = [
                ['name' => 'list_jobs_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_jobs_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_jobs_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_jobs_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_jobs_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_jobs_management_per);
        $charity_jobs_management->givePermissionTo([
            'list_jobs_'.$this->id,
            'create_jobs_'.$this->id,
            'edit_jobs_'.$this->id,
            'show_jobs_'.$this->id,
            'delete_jobs_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_jobs_'.$this->id,
        //     'create_jobs_'.$this->id,
        //     'edit_jobs_'.$this->id,
        //     'show_jobs_'.$this->id,
        //     'delete_jobs_'.$this->id,
        // ]);

        // sponsers permissions
        $charity_sponsers_management = Role::create(['name' => 'charity_sponsers_management_'.$this->id, 'type' => 'charity']);
        $charity_sponsers_management_per = [
                ['name' => 'list_sponsers_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_sponsers_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_sponsers_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_sponsers_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_sponsers_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_sponsers_management_per);
        $charity_sponsers_management->givePermissionTo([
            'list_sponsers_'.$this->id,
            'create_sponsers_'.$this->id,
            'edit_sponsers_'.$this->id,
            'show_sponsers_'.$this->id,
            'delete_sponsers_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_sponsers_'.$this->id,
        //     'create_sponsers_'.$this->id,
        //     'edit_sponsers_'.$this->id,
        //     'show_sponsers_'.$this->id,
        //     'delete_sponsers_'.$this->id,
        // ]);

        // exchange_bonds permissions
        $charity_exchange_bonds_management = Role::create(['name' => 'charity_exchange_bonds_management_'.$this->id, 'type' => 'charity']);
        $charity_exchange_bonds_management_per = [
                ['name' => 'list_exchange_bonds_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_exchange_bonds_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_exchange_bonds_management_per);
        $charity_exchange_bonds_management->givePermissionTo([
            'list_exchange_bonds_'.$this->id,
            'create_exchange_bonds_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_exchange_bonds_'.$this->id,
        //     'create_exchange_bonds_'.$this->id, 
        // ]);

        // Educational degrees permissions
        $charity_degrees_management = Role::create(['name' => 'charity_degrees_management_'.$this->id, 'type' => 'charity']);
        $charity_degrees_management_per = [
                ['name' => 'list_degrees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_degrees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_degrees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_degrees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_degrees_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_degrees_management_per);
        $charity_degrees_management->givePermissionTo([
            'list_degrees_'.$this->id,
            'create_degrees_'.$this->id,
            'edit_degrees_'.$this->id,
            'show_degrees_'.$this->id,
            'delete_degrees_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_degrees_'.$this->id,
        //     'create_degrees_'.$this->id,
        //     'edit_degrees_'.$this->id,
        //     'show_degrees_'.$this->id,
        //     'delete_degrees_'.$this->id,
        // ]);

        // Nationalities permissions
        $charity_nationalities_management = Role::create(['name' => 'charity_nationalities_management_'.$this->id, 'type' => 'charity']);
        $charity_nationalities_management_per = [
                ['name' => 'list_nationalities_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_nationalities_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_nationalities_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_nationalities_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_nationalities_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_nationalities_management_per);
        $charity_nationalities_management->givePermissionTo([
            'list_nationalities_'.$this->id,
            'create_nationalities_'.$this->id,
            'edit_nationalities_'.$this->id,
            'show_nationalities_'.$this->id,
            'delete_nationalities_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_nationalities_'.$this->id,
        //     'create_nationalities_'.$this->id,
        //     'edit_nationalities_'.$this->id,
        //     'show_nationalities_'.$this->id,
        //     'delete_nationalities_'.$this->id,
        // ]);

        // Reports permissions
        $charity_reports_management = Role::create(['name' => 'charity_reports_management_'.$this->id, 'type' => 'charity']);
        $charity_reports_management_per = [
                ['name' => 'list_reports_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'create_reports_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'edit_reports_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'show_reports_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
                ['name' => 'delete_reports_'.$this->id, 'guard_name' => 'web', 'model_id' => $this->id, 'model_type' => 'charity'], 
        ];
        Permission::insert($charity_reports_management_per);
        $charity_reports_management->givePermissionTo([
            'list_reports_'.$this->id,
            'create_reports_'.$this->id,
            'edit_reports_'.$this->id,
            'show_reports_'.$this->id,
            'delete_reports_'.$this->id,
        ]);
        // $this->givePermissionTo([
        //     'list_reports_'.$this->id,
        //     'create_reports_'.$this->id,
        //     'edit_reports_'.$this->id,
        //     'show_reports_'.$this->id,
        //     'delete_reports_'.$this->id,
        // ]);

    }
    
} 
