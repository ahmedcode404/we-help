<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    use HasFactory;

    protected $fillable = [

        'user_id',
        'project_id',
        'product_id',
        'setup_fee_id',
        'setup_fee_price_id',
        'customer_id',
        'invoice_item_id',
        'invoice_id',
        'subscription_id',
        'session_id',
        'canceled',
        'type'

    ];

    // user - transactions one to may relation
    public function user(){

        return $this->belongsTo(User::class);
    }


    // Project - transactions one to may relation
    public function project(){

        return $this->belongsTo(Project::class);
    }
}
