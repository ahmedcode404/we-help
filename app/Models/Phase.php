<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Phase extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'name_ar',
        'name_en',
        'desc_ar',
        'desc_en',
        'start_date',
        'end_date',
        'duration',
        'cost',
        'remaining_cost',
        'cost_details_ar',
        'cost_details_en',
        'output_ar',
        'output_en',
        'order',
        'approved',
        'notes',
        'project_id',
    ];


    // Get name for langouage 
    public function getNameAttribute()
    {
        if(App::getLocale() == 'en'){

            return $this->name_en;
        }
        else{
            if($this->name_ar != null){

                return $this->name_ar;
            }
            else{
                return $this->name_en;
            }
        }

    } // end of get name attribute    

    public function getDescAttribute(){

        if(App::getLocale() == 'en'){

            return $this->desc_en;
        }
        else{
            if($this->desc_ar != null){

                return $this->desc_ar;
            }
            else{
                return $this->desc_en;
            }
        }

        // return $this->{'name_'.App::getLocale()};
    }

    public function getOutputAttribute(){

        if(App::getLocale() == 'en'){

            return $this->output_en;
        }
        else{
            if($this->output_ar != null){

                return $this->output_ar;
            }
            else{
                return $this->output_en;
            }
        }

        // return $this->{'output_'.App::getLocale()};
    }

    public function getCostDetailsAttribute(){

        if(App::getLocale() == 'en'){

            return $this->cost_details_en;
        }
        else{
            if($this->cost_details_ar != null){

                return $this->cost_details_ar;
            }
            else{
                return $this->cost_details_en;
            }
        }

        // return $this->{'cost_details_'.App::getLocale()};
    }

    // Project - Phase one to mnay relation
    public function project(){
        return $this->belongsTo(Project::class);
    }
    
    // Phase - Report one to many relation
    public function reports(){
        return $this->hasMany(Report::class);
    }

    // Phase - Image morphMany relation
    public function images(){

        return $this->morphMany(Image::class, 'imageable' );
    }
}
