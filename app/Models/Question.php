<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;
class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'question_ar',
        'question_en',
        'answer_ar',
        'answer_en',
        'nation_id',
    ];

    // Nation(Coutnry) - StaticPage one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }

    // Get name according to language
    public function getQuestionAttribute()
    {
        return $this->{'question_'.App::getLocale()};
    }

    public function getAnswerAttribute(){

        return $this->{'answer_'.App::getLocale()};
    }
}
