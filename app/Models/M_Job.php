<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use App;

class M_Job extends Model
{
    use HasFactory, HasRoles;
    
    protected $table = 'm_jobs';

    protected $fillable = [
        'slug',
        'name_ar',
        'name_en',
        'job_category_id',
        'guard',
        'guard_id',
    ];
    
    public function getNameAttribute(){

        return $this->{'name_'.App::getLocale()};
    }

    // JobCategory - Job one to many relation
    public function category(){
        return $this->belongsTo(JobCategory::class, 'job_category_id');
    }

    // User - Job one to many relation
    public function emps(){
        return $this->hasMany(User::class, 'job_id');
    }

    public function charity_emp(){

        return $this->hasMany(Charity::class, 'job_id');
    }
}
