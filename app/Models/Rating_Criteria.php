<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Rating_Criteria extends Model
{
    use HasFactory;

    protected $fillable = [

        'name_ar',
        'name_en',
        'nation_id',

    ];
    
    // Nation(Coutnry) - Rating_Criteria one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    } 
    
    public function getNameAttribute(){

        return $this->{'name_'.App::getLocale()};
    }

}
