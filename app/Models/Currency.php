<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;
class Currency extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_ar',
        'name_en',
        'symbol',
        'icon',
        'nation_id',
    ];

    // Nation(Coutnry) - Currency one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }    

    // Get name for langouage 
    public function getNameAttribute()
    {
        if (App::getLocale() == 'ar') {

            return $this->name_ar;

        }else 
        {

            return $this->name_en;

        }

    } // end of get name attribute    

}
