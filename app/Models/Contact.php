<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use HasFactory;

    protected $fillable = [
        'email',
        'name',
        'phone',
        'content',
        'nation_id',
    ];

    // Nation(Coutnry) - Contact one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    }
}
