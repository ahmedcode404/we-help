<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class ServiceOption extends Model
{
    use HasFactory;

    protected $fillable = ['name_ar', 'name_en', 'charity_category_id', 'parent_id', 'nation_id'];

    public function getNameAttribute(){

        return $this->{'name_'.App::getLocale() };
    }

    public function category(){

        return $this->belongsTo(CharityCategory::class);
    }

    public function project(){

        return $this->belongsTo(Project::class);
    }

    public function features(){

        return $this->hasMany(Self::class, 'parent_id');
    }

    public function service(){

        return $this->belongsTo(Self::class, 'parent_id');
    }
}
