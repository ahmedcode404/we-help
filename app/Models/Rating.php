<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

class Rating extends Model
{
    use HasFactory;

    protected $fillable = [

        'user_id', 'project_id', 'rating_criteria_id', 'grade', 'comment', 'show_for_web'
    ];

    // Project - Rating one to many relation
    public function project(){
        return $this->belongsTo(Project::class);
    }

    // Rating_criterias - Rating one to many relation
    public function criteria(){
        return $this->belongsTo(Rating_Criteria::class, 'rating_criteria_id');
    }

    // Rating - User one to many relation
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function projectRating($user_id){

        return $this->groupBy('project_id')->avg('grade');
    }

    public function details($user_id, $project_id){

        return $this->with('criteria')->where([['user_id', $user_id], ['project_id', $project_id]])->get();
    }

    public function getComment($user_id, $project_id){

        return $this->with('criteria')->where([['user_id', $user_id], ['project_id', $project_id], ['comment', '!=', null], ['show_for_web', 1]])->first();
    }

    public function getCommentForAdmin($user_id, $project_id){

        return $this->with('criteria')->where([['user_id', $user_id], ['project_id', $project_id], ['comment', '!=', null]])->first();
    }

    public function date($user_id, $project_id){

        $rate = $this->where([['user_id', $user_id], ['project_id', $project_id]])->first();

        return Carbon::createFromTimeStamp(strtotime($rate->created_at))->diffForHumans();
    }
}
