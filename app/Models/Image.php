<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    protected $fillable = [
        'imageable_id',
        'imageable_type',
        'path',
    ];

    // Project - Image one to many relation
    public function imageable(){

        return $this->morphTo();
    }    

    // public function project(){

    //     return $this->belongsTo(Project::class);

    // }
}
