<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class ContractTerm extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'title_ar',
        'title_en',
        'content_ar',
        'content_en',
        'nation_id',
    ];

    // Nation(Coutnry) - City one to many relation - reverse
    public function nation(){

        return $this->belongsTo(City::class, 'nation_id');
    } 
    
    public function getTitleAttribute()
    {

        return $this->{'title_'.App::getLocale()};


    }

    public function getContentAttribute()
    {

        return $this->{'content_'.App::getLocale()};


    }

}
