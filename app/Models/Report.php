<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App;

class Report extends Model
{
    use HasFactory;

    protected $fillable = [
        'slug',
        'charity_id',
        'project_id',
        'phase_id',
        'report_ar',
        'report_en',
        'vedio',
        'status',
        'nation_id',
        'notes',
    ];

    // Get name for langouage 
    public function getReportAttribute()
    {
        if (App::getLocale() == 'ar') {

            return $this->report_ar;

        }else 
        {

            return $this->report_en;

        }

    } // end of get name attribute        

    // Charity - Report one to many relation
    public function charity(){
        return $this->belongsTo(Charity::class);
    }

    public function images(){

        return $this->morphMany(Image::class, 'imageable' );
    }    

    // Project - Report one to many relation
    public function project(){
        return $this->belongsTo(Project::class);
    }

    // Phase - Report one to many relation
    public function phase(){
        return $this->belongsTo(Phase::class);
    }
}
