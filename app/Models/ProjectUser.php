<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectUser extends Model
{
    use HasFactory;

    protected $table = 'project_user';

    protected $fillable = [
        'user_id', 'project_id', 
        'nation_id', 'status', 
        'type', 'cost', 'cost_gbp' , 'currency_id' , 
        'name_anonymouse', 'cart_no', 
        'project_covering', 'receipt', 'vip',
    ];

    // protected $appends = [
    //     'currency_name'
    // ];

    // public function getCurrencyNameAttribute(){

    //     return currencySymbol($this->currency_id);
    // }

    // Support - User relation
    public function user(){
        return $this->belongsTo(User::class);
    }

    // Support - Project relation
    public function project(){
        return $this->belongsTo(Project::class);
    }

    // Support - Currency relation
    public function currency(){
        return $this->belongsTo(Currency::class);
    }

    // Support - coutnry relation
    public function nation(){
        return $this->belongsTo(City::class);
    }

    public function scopeOnces()
    {
        return $this->where([ ['status' , 'support'] , ['type' , 'once'] , ['user_id' , auth()->user()->id]])->groupBy('project_id')->get();
    } // end of scope once

    public function scopeMonths()
    {
        return $this->where([ ['status' , 'support'] , ['type' , 'monthly'] , ['user_id' , auth()->user()->id]])->groupBy('project_id')->get();
    } // end of scope month  

    public function scopeGifts()
    {
        return $this->where([ ['status' , 'support'] , ['type' , 'gift'] , ['user_id' , auth()->user()->id]])->groupBy('project_id')->get();
    } // end of scope gift  
    
    public function scopeAmbassadors()
    {
        return $this->where([ ['status' , 'support'] , ['type' , 'Ambassador'] , ['user_id' , auth()->user()->id]])->groupBy('project_id')->get();
    } // end of scope Ambassador      
    


}
