<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EditRequest extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'model_id',
        'type',
        'approved',
        'notes',
        'emp_id',
        'nation_id',
    ];
    
    // Charity - EditRequest one to many relation
    public function charity(){
        return $this->belongsTo(Charity::class , 'model_id' , 'id');
    }

    public function project(){
        return $this->belongsTo(Project::class , 'model_id' , 'id');
    }    
    
}
