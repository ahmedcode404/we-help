<?php

namespace App\Servicies;

use App\Models\Charity;
use App\Models\DeviceToken;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App;

class Notify{


    // GET WEB TOKEN OF SEND TO NOTIFICATION 
    public static function getTokens($send_to, $id = null)
    {

        if($send_to == "user_premission"){

            return User::where('is_notify' , 1)->whereIn('id' , $id)->pluck('device_token');
        }
        // Charity
        elseif($send_to == "charity") {

            return Charity::where([ ['is_notify' , 1] , ['id' , $id]])->pluck('device_token');

        } 
        // Supporter
        elseif($send_to == "user" || $send_to == "supporter"){

            return User::where('is_notify' , 1)->pluck('device_token');
        }
        // Admin + employee
        elseif($send_to == "adminOrEmployee") {

            return User::where('is_notify' , 1)->pluck('device_token');

        } // end of if send to users        

    } // end of function get tokens


    // GET MOBILE DEVICE TOKENS
    public static function getMobileDeviceTokens($send_user_id, $type){


        $deviceTokenRepository = \App::make('App\Repositories\DeviceTokenRepositoryInterface');

        $tokens = [];
        
        if(gettype($send_user_id) == 'object'){
            $send_user_id = $send_user_id->toArray();
        }

        if(count($send_user_id) == 0){

            $tokens = $deviceTokenRepository->whereHas('user', [['is_notify', 1]], [['firebase_token', '!=', null], 
                                        ['user_type', $type]])->pluck('firebase_token');
        }
        else{

            $tokens = $deviceTokenRepository->whereHasIn('user', [['is_notify', 1]], 'user_id', $send_user_id, [['firebase_token', '!=', null], 
                                            ['user_type', $type]])->pluck('firebase_token');
        }
        
        return $tokens;

    }


    // SEND NOTIFICATION TO USER IN MOBILE
    // public static function NotifyMob($send_to, $message, $title, $type = 'admin-message', $store_id = null, $order_id = null, $delegate_id = null, $user_id = null){
    // $title: type of notification
    // $msg_ar, $msg_en: body of notification
    // $send_user_id: reciever of notification id
    // $type: user type (supporter, charity, admin, ..)
    // $model_id: id of created element, updated element, ..... to route to it
    // $not_type: notificfation type (created_reply_not, rent_expire_not, ...)
    // $image: notification image
    public static function NotifyMob($title, $msg_ar, $msg_en, $send_user_id, $type, $model_id, $image){

        // dd($title, $msg_ar, $msg_en, $send_user_id, $type, $model_id, $image);

        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';

        $tokenList = SELF::getMobileDeviceTokens($send_user_id, $type);

        $data = [];

        if($title == 'new_project'){

            $item = [
                'type' => 'screen',
                'id' => $model_id
            ];
            
            array_push($data, $item);
        }
        else if($title == 'new_voucher'){

            $financialRequestRepository = \App::make('App\Repositories\FinancialRequestRepositoryInterface');
            $voucher = $financialRequestRepository->findOne($model_id);

            $item = [
                'type' => 'web_view',
                'link' => route('get-voucher', $voucher->slug)
            ];

            array_push($data, $item);
        }
        else{

            $item = [
                'type' => 'no_action',
            ];

            array_push($data, $item);
        }

        $notification = [
            'title' => $title,
            'body' => \App::getLocale() == 'ar' ? $msg_ar : $msg_en,
            'data' => $data,
            'user_id' => $send_user_id,
            'image' => $image,
            'sound' => true,
        ];

        $extraNotificationData = [
            'data' => $data,
            'user_id' => $send_user_id,
            "click_action"=>"FLUTTER_NOTIFICATION_CLICK",
            "sound"=> "default",
            "badge"=> "8",
            "color"=> "#ffffff",
            "priority" => "high",
            'type' => $title
        ];

        $fcmNotification = [
            'registration_ids' => $tokenList, //multple token array
            'notification' => $notification,
            'data' => $extraNotificationData,
        ];

        // dump($fcmNotification);

        $headers = [
            'Authorization: key=AAAAYW7fb94:APA91bHa5APu15natsvy9BZ5LN_dgDtdei81e2Vnc_mq9vPUMY2LakqwBu_b9gD86FDHLB2Vi-Ve7DbKfEDAsOBPSDGxCMVNxO9GOIbXL6sLJNgzzWnnN3vHrE-eND1yKtVECadbfzva',
            'Content-Type: application/json'
        ];

        // dump($fcmNotification);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);

        // dd(json_decode($result));
        curl_close($ch);
    }


    // SEND NOTIFICATION TO  USER WEB
    public static function NotifyWeb($message , $title , $send_to , $send_user_id)
    {

        $tokenList;

        if($send_to == 'user_premission'){

            $tokenList = SELF::getTokens($send_to, $send_user_id);

        }
        // Charity
        elseif ($send_to == 'charity'){

            $tokenList = SELF::getTokens($send_to , $send_user_id);

        } 
        // Supporter
        elseif($send_to == "user" || $send_to == "supporter"){
            
            $tokenList = SELF::getTokens($send_to , $send_user_id);
        }
        // Admin + employee
        elseif ($send_to == 'adminOrEmployee'){

            $tokenList = SELF::getTokens($send_to , $send_user_id);

        }         

        $SERVER_API_KEY = 'AAAAYW7fb94:APA91bHa5APu15natsvy9BZ5LN_dgDtdei81e2Vnc_mq9vPUMY2LakqwBu_b9gD86FDHLB2Vi-Ve7DbKfEDAsOBPSDGxCMVNxO9GOIbXL6sLJNgzzWnnN3vHrE-eND1yKtVECadbfzva';
                
        $data = [
            "registration_ids" => $tokenList,
            "notification" => [
                "title"    => __('admin.we_help'),
                "user_id"  => $send_user_id,
                'body'     => App::getLocale() == 'ar'  ? $message->message_ar : $message->message_en ,
                "icon"     => url('dashboard/images/avatar.png'),

            ]
        ];
        
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

    } // end of notify web


} // end of class

