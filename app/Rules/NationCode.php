<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App;

class NationCode implements Rule
{
    private $nation_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($nation_id)
    {
        $this->nation_id = $nation_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // SA
        if($this->nation_id == 1){

            return (preg_match('/^\+966/', $value));

        }
        // UK
        else if($this->nation_id == 2){

            return (preg_match('/^\+44/', $value));
        }
        // Both SA n or UK
        else{

            return (preg_match('/^\+966/', $value) || preg_match('/^\+44/', $value));
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return App::getLocale() == 'ar' ? 'صيغة الهاتف أو الجوال غير صحيحة' : 'In valid Phone or Mobile Number';
    }
}
