<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ApiPhone implements Rule
{
    private $nation_id;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($nation_id)
    {
        $this->nation_id = $nation_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        // SA
        if($this->nation_id == 1){

            return (preg_match('/^\+9665/', $value) || preg_match('/^\+96605/', $value));
        }
        // UK
        else if($this->nation_id == 2){

            return (preg_match('/^\+4479/', $value));

        }
        else{

            return (preg_match('/^\+9665/', $value) || preg_match('/^\+96605/', $value) || preg_match('/^\+44/', $value));
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('api.invalid_phone_number');
    }
}
