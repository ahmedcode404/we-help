<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IBAN implements Rule
{

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^[A-Z]{2}[0-9]{2}/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return  \App::getLocale() == 'ar' ? 'رقم الحساب غير صحيح' : 'Invalide IBAN';
    }
}
