<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class TextLength implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(count(explode(' ', $value)) > 500){

            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return \App::getLocale() == 'ar' ? 'عدد الكلمات لا يجب أن يتجاوز 500 كلمة' : 'Number of words must not exceed 500 word';
    }
}
