<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SWIFT implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        if((count(str_split($value)) == 8 && preg_match('/^[A-Z]{6}/', $value)) || 
            (count(str_split($value)) == 11 && preg_match('/^[A-Z]{6}/', $value))){

            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return \App::getLocale() == 'ar' ? 'صيغة رمز السرعة غير صحيحة' : 'Invalide SWIFT code';
    }
}
