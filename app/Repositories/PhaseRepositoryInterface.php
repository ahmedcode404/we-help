<?php

namespace App\Repositories;

interface PhaseRepositoryInterface
{

    public function unApproved($project_id);

    public function adminUpdatePhase($request, $id);
    
    public function getSum($project_id);    

    public function updatePhase(array $data, $id);  
    
    public function getFirstOne($project_id);    

    public function last($project_id);    

    public function previou($id);

    public function CheckDateBigThisDate();    

    public function adminDeletePhase($id);
    public function next($id);

    public function count($ids);
    
    public function getFirst($project_id);
    
    public function CheckDateBigThisDateEdit($id);    
    
    public function getStartDate($id);
}
