<?php

namespace App\Repositories;

interface EditRequestRepositoryInterface
{

    public function getRequestEditProject($id);   

    public function getRequestEditCharity($id);    
    
}
