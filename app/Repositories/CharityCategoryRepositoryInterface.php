<?php

namespace App\Repositories;

interface CharityCategoryRepositoryInterface
{
    public function getCategories($nation_id);
    
}
