<?php

namespace App\Repositories;

interface CurrencyRepositoryInterface
{
   
    public function nation($nation);
    
}
