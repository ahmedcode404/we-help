<?php

namespace App\Repositories;

interface AgreementRepositoryInterface
{
   
    public function updateAgreement(array $attr);    
    
}
