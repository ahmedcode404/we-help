<?php

namespace App\Repositories;

interface CityRepositoryInterface
{

    public function country();
    public function city();
    public function cityOfCountry($country);
    public function countriesCount();
    public function adminCountriesCount($nation_id);
    
}
