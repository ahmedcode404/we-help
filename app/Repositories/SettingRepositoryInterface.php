<?php

namespace App\Repositories;

interface SettingRepositoryInterface
{
   
    public function updateSetting(array $attr);
    public function getSa();
    public function where($data);
    public function getUk();
    
}
