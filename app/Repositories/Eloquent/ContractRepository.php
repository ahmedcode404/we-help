<?php

namespace App\Repositories\Eloquent;

use App\Models\ContractTerm;
use App\Repositories\ContractRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ContractRepository extends BaseRepository implements ContractRepositoryInterface
{

    public function __construct()
    {
        $this->model = new ContractTerm();
        
    } // end of constuct


}
