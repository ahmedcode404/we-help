<?php

namespace App\Repositories\Eloquent;

use Spatie\Permission\Models\Permission;
use App\Repositories\PermissionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Spatie\Permission\Models\Role;

class PermissionRepository extends BaseRepository implements PermissionRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Permission();
        $this->modelRole = new Role();
        
    }

    public function allRole(): Collection
    {

        return $this->modelRole->where('type' , null)->get();

    } // end of all Role

    public function getCharityRoles(): Collection
    {

        return $this->modelRole->where([['name', '!=', 'charity'], ['name', '!=', 'charity_employee'], ['type', 'charity']])->get();

    } // end of all Role

    
    
}
