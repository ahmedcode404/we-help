<?php

namespace App\Repositories\Eloquent;

use App\Models\Currency;
use App\Repositories\CurrencyRepositoryInterface;

class CurrencyRepository extends BaseRepository implements CurrencyRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new Currency();
        
    }

    public function nation($nation)
    {
        return $this->model->where('nation_id' , $nation)->get();
    }
 
}
