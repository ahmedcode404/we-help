<?php

namespace App\Repositories\Eloquent;

use App\Models\ProjectUser;
use App\Repositories\ProjectUserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ProjectUserRepository extends BaseRepository implements ProjectUserRepositoryInterface
{

    public function __construct()
    {
        $this->model = new ProjectUser();
        
    } // end of constuct

    // get cost project 
    public function cost($id)
    {

        return $this->model->where([['status' , 'support'] , ['project_id' , $id]])->sum('cost');

    } // end of get cost project

    // get cost donate projects 
    public function costAllProject($ids)
    {

        return $this->model->where('status' , 'support')->whereIn('project_id' , $ids)->sum('cost');

    } // end of cost donate projects 

}
