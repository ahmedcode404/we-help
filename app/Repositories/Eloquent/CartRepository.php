<?php

namespace App\Repositories\Eloquent;

use App\Models\ProjectUser;
use App\Repositories\CartRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CartRepository extends BaseRepository implements CartRepositoryInterface
{

    public function __construct()
    {
        $this->model = new ProjectUser();
        
    } // end of constuct
  
}
