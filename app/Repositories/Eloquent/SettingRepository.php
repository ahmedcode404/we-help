<?php

namespace App\Repositories\Eloquent;

use App\Models\Setting;
use App\Repositories\SettingRepositoryInterface;

class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{

    public function __construct()
    {
        
        return $this->model = new Setting();
        
    }

    public function getSa()
    {
        
       return $this->model->where('nation_id' , 1)->get();
        
    }
    
    public function getUk()
    {
        
        return $this->model->where('nation_id' , 2)->get();
        
    }   
    
    public function where($data)
    {
        
        return $this->model->where($data)->first();
        
    }     

    public function updateSetting(array $attr)
    {

		foreach ($attr as $key => $att) {

            $this->model->where('key' , $key)->update(['value' => $att]);

        } // end of foreach
        	
        return response()->json();
        
    } // end of update     
 
}
