<?php

namespace App\Repositories\Eloquent;

use App\Models\Phase;
use App\Models\Project;
use App\Repositories\PhaseRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use Storage;

class PhaseRepository extends BaseRepository implements PhaseRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Phase();
        
    } // end of constuct
    
    public function unApproved($project_id)
    {

        return $this->model->where([['project_id' , $project_id] , ['approved' , 0]])->orderBy('order' , 'desc')->get();

        
    } // end of unApproved 

    public function getSum($project_id)
    {

        return $this->model->where('project_id' , $project_id)->sum('duration');
   
    } // end of getSum     
    
    public function updatePhase(array $data, $id)
    {
        $this->model->where('id', $id)->update($data);
        return $this->model->where('id' , $id)->first();
    }  

    public function getFirstOne($project_id)
    {

        return $this->model->where('project_id' , $project_id)->first();

    } // end of get first 

    public function last($project_id)
    {

        return $this->model->where('project_id' , $project_id)->latest()->first();

    } // end of get first    
    
    public function previou($id)
    {

        return $this->model->where('id'  , '<' ,  $id)->max('end_date');

    } // end of get first
    
    public function next($id)
    {

        return $this->model->where('id'  , '>' ,  $id)->min('start_date');

    } // end of get first    
    
    public function getFirst($project_id)
    {

        return $this->model->where('project_id' , $project_id)->get();

    } // end of get first 


    public function CheckDateBigThisDate()
    {

        return $this->model->pluck('end_date');

    } // end of get first  
    
    public function CheckDateBigThisDateEdit($id)
    {

        return $this->model->where('id' , '!=' , $id)->pluck('end_date');

    } // end of get first 
    
    
    public function getStartDate($id)
    {

        return $this->model->where('id' , '>' , $id)->get();

    }
    
    public function count($ids)
    {

        return $this->model->whereIn('project_id' , $ids)->count();

    } // end of count    

        

    // edit phase - ADMIN
    public function adminUpdatePhase($request, $id){

        $data = $request->except(['_token', '_method', 'cost']);

        $phase = $this->model->with('project')->find($id);

        $old_cost = $phase->cost;

        // set the phase cost to 0 - علشان لما اجيب اجمالى تكلفة المراحل مع التكلفة الجديده تبقى الحسابات مظبوطه
        $phase->update(['cost' => 0, 'remaining_cost' => 0]);

        $project = Project::with(['phases', 'currency'])->find($phase->project_id);

        $phases_cost = generalExchange($project->phases->sum('cost'), $project->currency->symbol, currencySymbol(session('currency'))) + 
            generalExchange((float)$request->cost, $project->currency->symbol, currencySymbol(session('currency')));

        // get project actual cost without admin ratio
        $project_cost = generalExchange($project->cost, $project->currency->symbol, currencySymbol(session('currency')));

        if($phases_cost > $project_cost){

            $phase->update(['cost' => $old_cost, 'remaining_cost' => $old_cost]);

            return 'cost_exceeds';

        }

        $data['remaining_cost'] = $request->cost;
        $data['cost'] = $request->cost;

        // update start_date
        $new_start_date = Carbon::createFromDate($request->start_date);
        $old_start_date = Carbon::createFromDate($phase->start_date);

        
        $new_end_date = Carbon::createFromDate($request->end_date);
        $old_end_date = Carbon::createFromDate($phase->end_date);

        if($new_start_date->format('Y-m-d') < $old_start_date->format('Y-m-d')){

            if($request->order != 1){

                return 'date_out_range';
            }
        }

        if($new_end_date->format('Y-m-d') > $old_end_date->format('Y-m-d')){
        
            return 'date_out_range';
        }

        // if it is the first phase, then change the project start_date
        if($phase->order == 1){

            $phase->project()->update([
                'start_date' => $request->start_date
            ]);
        }

        $old_start_date = Carbon::createFromDate($phase->start_date);
        $new_start_date = Carbon::createFromDate($data['start_date']);

        $difference_days = $old_start_date->diffInDays($new_start_date);

        if($request->has('approved')){

            $data['approved'] = 1;
        }
        else{
            $data['approved'] = 0;
        }

        // Update Phase
        $updated = $phase->update($data);

        $phase = $this->model->with('project')->find($id);

        // $project = $phase->project;
        
        // update project duration and cost
        $project_data['duration'] = $project->phases->sum('duration');
        // $project_data['cost'] = $project->phases->sum('cost');

        $project->update($project_data);

        if($updated){

            return 'updated';
        }
        else{
            return 'not-updated';
        }
    }

    // Delete phase - ADMIN
    public function adminDeletePhase($id){

        $phase = $this->model->with('reports', 'project')->find($id);

        foreach($phase->reports as $report){

            Storage::delete($report->video);

            foreach($report->images as $image){

                Storage::delete($image->path);
            }

            // delete report images records
            $report->images()->delete();

            // delete report
            $report->delete();
        }

        $other_phases = $this->model->where([['project_id', $phase->project_id], ['order', '>', $phase->order]])->orderBy('order')->get();

        if($other_phases){
            
            foreach($other_phases as $other){

                $other->update([
                    'order' => $other->order - 1
                ]);
            }

        }

        $project_id = $phase->project_id;

        $deleted = $phase->delete();

        $project = Project::with('phases')->find($project_id);

        // update project duration and cost
        $project_data['duration'] = $project->phases->sum('duration');
        // $project_data['cost'] = $project->phases->sum('cost');

        $project->update($project_data);


        // delete phase
        return $deleted;

    }

}
