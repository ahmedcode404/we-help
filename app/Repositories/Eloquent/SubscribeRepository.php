<?php

namespace App\Repositories\Eloquent;

use App\Models\Subscribe;
use App\Repositories\SubscribeRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class SubscribeRepository extends BaseRepository implements SubscribeRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Subscribe();
        
    } // end of constuct

}
