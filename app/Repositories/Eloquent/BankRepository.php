<?php

namespace App\Repositories\Eloquent;

use App\Models\Bank;
use App\Repositories\BankRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class BankRepository extends BaseRepository implements BankRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Bank();
        
    } // end of constuct
  
}
