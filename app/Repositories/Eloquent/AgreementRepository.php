<?php

namespace App\Repositories\Eloquent;

use App\Models\Agreement;
use App\Repositories\AgreementRepositoryInterface;

class AgreementRepository extends BaseRepository implements AgreementRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new Agreement();
        
    }

    public function updateAgreement(array $attr)
    {

		foreach ($attr as $key => $att) {

            $this->model->where('key' , $key)->update(['content' => $att]);

        } // end of foreach
        	
        return response()->json();
        
    } // end of update    
 
}
