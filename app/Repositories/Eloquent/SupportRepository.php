<?php

namespace App\Repositories\Eloquent;

use App\Models\ProjectUser;
use App\Models\Setting;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class SupportRepository extends BaseRepository implements SupportRepositoryInterface
{

    private $currencyRepository;

    public function __construct(CurrencyRepositoryInterface $currencyRepository)
    {
        $this->model = new ProjectUser();

        $this->currencyRepository = $currencyRepository;
        
    } // end of constuct

    // Get Total supports for specific version
    public function totalAmmount(array $data, $currency = null){

        $supports = $this->model->with('currency')->where([['cost', '!=', 0], ['status', 'support'], ['nation_id', $data[0][1]]])->sum('cost_gbp');

        // $sum = 0;

        // foreach($supports as $key => $support){

        //     $sum += generalExchange($support->cost, $support->currency->symbol, $currency);
        // }

        // return $sum;

        if($currency == null){

            $currency = currencySymbol(session('currency'));
        }
        
        return generalExchange($supports, 'GBP', $currency);

    }


    /**
     * retrieve all the rows matching the where condition group by columns
     * @params OPTIONAL $orderBy with the column name && dir
     */
    public function getWhereGroupByAll($data)
    {
        return $this->model->select('user_id', 'cart_no', 'project_id', 'status', 'cost', 'type', 'name_anonymouse', 'nation_id', 'currency_id')
            ->where($data)->groupBy('user_id', 'cart_no', 'project_id', 'status', 'cost', 'type', 'name_anonymouse', 'nation_id', 'currency_id')->get();
        // return $this->model->where($data)->groupBy('user_id', 'cart_no', 'project_id', 'status', 'cost', 'type', 'name_anonymouse', 'nation_id', 'currency_id')->orderBy($orderBy['column'], $orderBy['dir'])->get();
    }


}
