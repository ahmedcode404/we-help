<?php

namespace App\Repositories\Eloquent;

use App\Models\Rating_Criteria;
use App\Repositories\RatingCriteriaRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class RatingCriteriaRepository extends BaseRepository implements RatingCriteriaRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Rating_Criteria();
        
    } // end of constuct


}
