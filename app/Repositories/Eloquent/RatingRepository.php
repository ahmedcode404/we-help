<?php

namespace App\Repositories\Eloquent;

use App\Models\Rating;
use App\Repositories\RatingRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use DB;

class RatingRepository extends BaseRepository implements RatingRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Rating();
        
    } // end of constuct

    /**
     * $nation_id if it is not null, then return the ratings of the project of the admin's authed nation_id
     * and if it is null, then return the ratings of the project of the ip address nation_id
    */
    public function getRatings($nation_id = null){

        if($nation_id == null){

            $nation_id = getNationId();
        }

        return $this->model->with('user', 'project', 'project.charity')
                    ->select('show_for_web', 'user_id', 'project_id')
                    // ->selectRaw("DATE_FORMAT(timestamp, '%Y%m%d') as period")
                    // ->selectRaw("MIN(value) AS min")
                    // ->selectRaw("MAX(value) AS max")
                    ->selectRaw("round(AVG(grade),0) AS avg_grade")
                    ->whereHas('project', function($q) use ($nation_id){

                        $q->where('nation_id', $nation_id);
                    })
                    ->groupBy('show_for_web', 'project_id', 'user_id')->paginate(10);
    }

    public function updateComment($id , $attr)
    {
        // dd($id);
        // dd($attr);
        return $this->model->where('id' , $id)->update($attr);

    } // end of update comment    

}
