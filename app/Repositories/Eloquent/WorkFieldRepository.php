<?php

namespace App\Repositories\Eloquent;

use App\Models\WorkField;
use App\Repositories\WorkFieldRepositoryInterface;

class WorkFieldRepository extends BaseRepository implements WorkFieldRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new WorkField();
        
    }
 
}
