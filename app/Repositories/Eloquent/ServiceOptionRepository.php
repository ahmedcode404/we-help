<?php

namespace App\Repositories\Eloquent;

use App\Models\ServiceOption;
use App\Repositories\ServiceOptionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ServiceOptionRepository extends BaseRepository implements ServiceOptionRepositoryInterface
{

    public function __construct()
    {
        $this->model = new ServiceOption();
        
    } // end of constuct 

  
}
