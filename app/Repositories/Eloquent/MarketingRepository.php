<?php

namespace App\Repositories\Eloquent;

use App\Models\Marketing;
use App\Repositories\MarketingRepositoryInterface;

class MarketingRepository extends BaseRepository implements MarketingRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new Marketing();
        
    }
 
}
