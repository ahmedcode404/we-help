<?php

namespace App\Repositories\Eloquent;

use App\Models\Partener;
use App\Repositories\PartenerRepositoryInterface;

class PartenerRepository extends BaseRepository implements PartenerRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new Partener();
        
    }
 
}
