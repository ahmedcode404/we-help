<?php

namespace App\Repositories\Eloquent;

use App\Models\ProjectUser;
use App\Repositories\DonateRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class DonateRepository extends BaseRepository implements DonateRepositoryInterface
{

    public function __construct()
    {
        $this->model = new ProjectUser();
        
    } // end of constuct


}
