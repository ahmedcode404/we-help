<?php

namespace App\Repositories\Eloquent;

use App\Models\FinancialRequest;
use App\Repositories\FinancialRequestRepositoryInterface;

class FinancialRequestRepository extends BaseRepository implements FinancialRequestRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new FinancialRequest();
        
    }
 
}
