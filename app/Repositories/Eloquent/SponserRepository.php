<?php

namespace App\Repositories\Eloquent;

use App\Models\Sponser;
use App\Repositories\SponserRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class SponserRepository extends BaseRepository implements SponserRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Sponser();
        
    } // end of constuct

    public function count($ids)
    {

        return $this->model->whereIn('project_id' , $ids)->count();

    } // end of count
 
}
