<?php

namespace App\Repositories\Eloquent;

use App\Models\Project;
use App\Models\FrozenSupport;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\PaymentTransactionRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;

use Storage;
use Carbon\Carbon;
use App\Models\ProjectUser;

class ProjectRepository extends BaseRepository implements ProjectRepositoryInterface
{

    private $paymentTransactionRepository;
    private $supportRepository;
    private $settingRepository;
    private $logRepository;
    private $notificationRepository;
    private $userepository;
    private $charityCategoryRepository;

    public function __construct(PaymentTransactionRepositoryInterface $paymentTransactionRepository,
                                SupportRepositoryInterface $supportRepository,
                                LogRepositoryInterface $logRepository,
                                SettingRepositoryInterface $settingRepository,
                                NotificationRepositoryInterface $notificationRepository,
                                UserRepositoryInterface $userRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository)
    {
        
        $this->model = new Project();

        $this->paymentTransactionRepository = $paymentTransactionRepository;
        $this->supportRepository = $supportRepository;
        $this->settingRepository = $settingRepository;
        $this->logRepository = $logRepository;
        $this->notificationRepository = $notificationRepository;
        $this->userRepository = $userRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        
    }

    public function updateProject(array $data, $id)
    {
        $this->model->where('id', $id)->update($data);
        return $this->model->where('id' , $id)->first();
    }

    public function deleteProject($id)
    {

        return $this->model->where('id', $id)->delete();
        
    }

    // Update Project data - ADMIN
    public function adminUpdateProject($request, $id){

        $project = $this->model->with('phases')->find($id);

        $data = $request->except(['_token', '_method', 'eng_maps', 'images', 'attach', 'image', 'prev_project']);

        if(!$request->has('prev_project')){

            if($request->service_option_id == 'other'){

                $data['service_option_id'] = null;
            }
    
            if($request->service_feature_id == 'other'){
                
                $data['service_feature_id'] = null;
            }
    
            if($request->has('start_date') && $data['start_date'] != $project->start_date){
    
                $old_date = Carbon::createFromDate($project->start_date);
                $new_date = Carbon::createFromDate($data['start_date']);
    
                $difference_days = $old_date->diffInDays($new_date);
    
                if($new_date > $old_date){
    
                    // subtract days
                    foreach($project->phases as $phase){
    
                        $new_phase_start = new Carbon($phase->start_date);
                        $new_phase_end = new Carbon($phase->end_date);
    
                        $new_phase_start = $new_phase_start->addDays($difference_days)->format('Y-m-d');
                        $new_phase_end = $new_phase_end->addDays($difference_days)->format('Y-m-d');
    
                        $phase->update([
                            'start_date' => $new_phase_start,
                            'end_date' => $new_phase_end
                        ]);
                    }
    
                }
                else{
                    //add days
    
                    foreach($project->phases as $phase){
    
                        $new_phase_start = new Carbon($phase->start_date);
                        $new_phase_end = new Carbon($phase->end_date);
    
                        $new_phase_start = $new_phase_start->subDays($difference_days)->format('Y-m-d');
                        $new_phase_end = $new_phase_end->subDays($difference_days)->format('Y-m-d');
    
                        $phase->update([
                            'start_date' => $new_phase_start,
                            'end_date' => $new_phase_end
                        ]);
                    }
                }
    
    
            }
    
            $category = $this->charityCategoryRepository->findOne($request->charity_category_id);
    
            if($category && $category->type == null){
    
                $data['service_option_id'] = null;
                $data['other_service_option'] = null;
                $data['service_feature_id'] = null;
                $data['other_service_feature'] = null;
            }
    
            if($request->has('eng_maps')){
    
                // Delete old eng_maps
                Storage::delete($project->eng_maps);
    
                // Upload new eng_maps
                $data['eng_maps'] = $request->file('eng_maps')->store('uploads/project/images');
    
                $data['service_option_id'] = null;
                $data['other_service_option'] = null;
                $data['service_feature_id'] = null;
                $data['other_service_feature'] = null;
            }
            else{
    
                // Delete old eng_maps
                Storage::delete($project->eng_maps);
    
                // Upload new eng_maps
                $data['eng_maps'] = null;
            }
    
            if($request->has('image')){
    
                // Delete old image
                Storage::delete($project->image);
    
                // Upload new image
                $data['image'] = $request->file('image')->store('uploads/project/images');
    
            }
    
            if($request->has('attach')){
    
                // Delete old attach
                Storage::delete($project->attach);
    
                // Upload new attach
                $data['attach'] = $request->file('attach')->store('uploads/project/pdf');
            }
    
            if(isset($data['status']) && $data['status'] == 'approved'){
    
                // Notify Supporters via mobile app
    
                $supprters_arr = [];
    
                $supporters = $this->userRepository->whereHas('roles', [['name', 'suporter']]);
    
                foreach($supporters as $supporter){
    
                    array_push($supprters_arr, $supporter->id);
    
                    $data['message_en'] = 'A New project is added';
                    $data['message_ar'] = 'تم إضافة مشروع جديد';
                    $data['send_user_id'] = [];
                    $data['model_id'] = $project->id;
                    $data['title'] = 'new_project';
                    $data['type'] = 'supporter';
    
                    $this->notificationRepository->create($data);
                }
    
                
                notifyMobile($project->id, $supprters_arr, 'new_project', 'supporter', 'تم إضافة مشروع جديد',  'A New project is added', asset('storage/'.$project->image));
    
            }
        }

        if($request->has('images')){

            foreach($request->images as $image){

                $project->images()->create([
                    'imageable_id' => $project->id,
                    'imageable_type' => 'App\Models\Project',
                    'path' => $image->store('uploads/project/images')
                ]);
            }
        }

        return $this->model->where('id', $id)->update($data);
    }

    // Delete project image - ADMIN
    public function adminDeleteProjectImage($image_id, $project_id){

        $project = $this->model->with('images')->find($project_id);

        $image = $project->images()->where('id', $image_id)->first();

        Storage::delete($image->path);

        return $image->delete();
    }

    // get total beneficiaries for specific version
    public function getBenefCount(array $data){

        return $this->model->where($data)->sum('benef_num');
    }

    // get cost project 
    public function cost($id)
    {

        return $this->model->find($id)->get_total;
        // return $this->model->where('id' , $id)->sum('cost');

    } // end of get cost project
    
    // get waiting project 
    public function waiting()
    {

        return $this->model->where('status' , 'waiting')->count();

    } // end of get waiting project   
    
    // get approved project 
    public function approved()
    {

        return $this->model->where('status' , 'approved')->count();

    } // end of get approved project   
    
    // get rejected project 
    public function rejected()
    {

        return $this->model->where('status' , 'rejected')->count();

    } // end of get rejected project 
    
    // get hold project 
    public function hold()
    {

        return $this->model->where('status' , 'hold')->count();

    } // end of get hold project 
    
    public function checkCommentOrNo($project_id , $user_id)
    {
        
        return $this->model->whereHas('ratings' , function ($q) use ($project_id , $user_id)
        {

            $q->where([['project_id' , $project_id] , ['user_id' , $user_id]]);

        })->get();

    } // end of check Comment Or No


    // delete project - ADMIN
    public function adminDeleteProject($id, $charity_id = null){

        $project = $this->model->with(['supports', 'transactions'])->find($id);

        $charity_name = $project->charity ? $project->charity->name : '';

        $response = new \stdClass;

        foreach($project->transactions as $transaction){

            if($transaction->type == 'monthly' && $transaction->canceled == 0){

                // Set your secret key. Remember to switch to your live secret key in production.
                // See your keys here: https://dashboard.stripe.com/apikeys
                \Stripe\Stripe::setApiKey(config('services.stripe.apiKey'));

                $subscription = \Stripe\Subscription::retrieve($transaction->subscription_id);
                
                $subscription->cancel();

                $this->paymentTransactionRepository->update(['canceled' => 1], $transaction->id);
            }
        }

        $message = new \stdClass();
        $message->message_ar = 'تم إيقاف التبرع الشهرى لمشروع '.$project->name.'نظراً لإكتمال التبرعات';
        $message->message_en = 'The monthly donation to '.$project->name.' project has been stopped due to the completion of donations';

        // set project to be inactive
        $deleted = $project->update(['active' => 0]);
        // $deleted = 1;

        $response->charity_deleted = $deleted;

        $session_currency = currencySymbol(session('currency'));

        if($deleted){

            // get project of other charities except for the deleted charity
            // اجمالى سندات الصرف
            $total_vouchers = $project->getTotalProjectExchangeVouchers();

            // إجمالى التبرعات
            $total_supports = getTotalSupportsForProject($project);

            // المتبقى
            $remaining_cost = $total_supports - $total_vouchers;

            // delete the supports of the projects - علشان يفضل اجمالى التبرعات مظبوط
            $project->supports()->update(['cost' => 0, 'cost_gbp' => 0]);

            // get all the projects ofd the same category of this project
            if($charity_id != null){

                $other_projects = Project::with('currency', 'supports')->where([['charity_category_id', $project->charity_category_id], ['id', '!=', $project->id], ['charity_id', '!=', $charity_id],  ['active', 1]])->get();
            }
            else{

                $other_projects = Project::with('currency', 'supports')->where([['charity_category_id', $project->charity_category_id], ['id', '!=', $project->id], ['active', 1]])->get();
            }

            if(count($other_projects) == 0 || $remaining_cost > 0){

                // اضافة المبلغ فى جدول التبرعات التجمده
                // اشعار الادمن بعدم وجود مشاريع من نفس التصنيف و يصبح له حرية اضافة المبلغ لاى مشروع
                $old_frozen = FrozenSupport::where('from_project_id', $project->id)->first();
                if(!$old_frozen){

                    $main_fornzen_support = FrozenSupport::create([
                        'slug' => \Str::random(12),
                        'from_project_id' => $project->id,
                        'prev_donations' => $total_supports,
                        'out_to_charity' => $total_vouchers,
                        'cost' => $remaining_cost < 0 ? 0 : $remaining_cost,
                        'currency_id' => session('currency')
                    ]);
                    
                    $response->support_status = 'frozen';
    
                }
                
            }

            // create supports for the other projects
            $supportsArr = [];

            if(count($other_projects) > 0){

                foreach($other_projects as $pro){

                    if($remaining_cost <= 0){

                        break;
                    }

                    // إجمالى التبرعات للمشروع الاخر
                    $other_pro_total_supports = $pro->total_supports_for_project;

                    //تكلفة المشروع الاخر
                    $other_pro_cost = generalExchange($pro->get_total, $pro->currency->symbol, $session_currency);

                    //  المبلغ المتبقى من تبرعات المشروع الاخر
                    $other_pro_remaining_cost =  $other_pro_cost - $other_pro_total_supports;

                    // فى حالة المبلغ المتبقى اقل من او يساوى المبلغ المتبقى من تبرعات المشروع الاخر - اضف قيمة التبرع كامله
                    if($remaining_cost <= $other_pro_remaining_cost){

                        $to_project_cost = $remaining_cost;

                        $arr = [
                            'user_id' => auth()->user()->id,
                            'project_id' => $pro->id,
                            'status' => 'support',
                            'cost' => $to_project_cost < 0 ? 0 : $to_project_cost,
                            'cost_gbp' => generalExchange($to_project_cost, currencySymbol(session('currency')), 'GBP'),
                            'type' => 'once',
                            'name_anonymouse' => 'Amount Transferred by Adminstration',
                            'nation_id' => $project->nation_id,
                            'currency_id' => session('currency'),
                        ];
    
                        array_push($supportsArr, $arr);

                        $remaining_cost = 0;

                        break;
                    }
                    // فى حالة ان المبلغ المتبقى اكبر من تكلفة المشروع الاخر يبقى هضيف القيمه اللى محتاجه المشروع الاخر فقط و الباقى هحوله على مشروع تانى
                    else{

                        $to_project_cost = $other_pro_remaining_cost;

                        $arr = [
                            'user_id' => auth()->user()->id,
                            'project_id' => $pro->id,
                            'status' => 'support',
                            'cost' => $to_project_cost < 0 ? 0 : $to_project_cost,
                            'cost_gbp' => generalExchange($to_project_cost, currencySymbol(session('currency')), 'GBP'),
                            'type' => 'once',
                            'name_anonymouse' => 'Amount Transferred by Adminstration',
                            'nation_id' => $project->nation_id,
                            'currency_id' => session('currency'),
                        ];
    
                        array_push($supportsArr, $arr);

                        $remaining_cost = $remaining_cost - $other_pro_remaining_cost;

                        $main_fornzen_support->update(['cost' => $remaining_cost]);

                        $pro->update(['donation_complete' => 1]);

                    }


                    $fornzen_support = FrozenSupport::create([
                        'slug' => \Str::random(12),
                        'from_project_id' => $project->id,
                        'prev_donations' => $total_supports,
                        'out_to_charity' => $total_vouchers,
                        'cost' => $remaining_cost < 0 ? 0 : $remaining_cost,
                        'to_project_id' => $pro->id,
                        'to_project_cost' => $to_project_cost,
                        'currency_id' => session('currency')
                    ]);
                }

                if (count($supportsArr) > 0) {
                    $new_supports = $this->supportRepository->CreateMulti($supportsArr);

                    $response->support_status = 'transfered';
                }
            }

            // اشعار المتبرعين بتوع المشروع دا ان التبرع بتاهم هيروح لمشروع تانى
            $response->notify_supporters = $project->notifySupporters($charity_name);

            // get supporters
            $supporters_ids = ProjectUser::where('project_id', $project->id)->distinct('user_id')->pluck('user_id');

            // Store supporters ids with frozen cost
            if(isset($fornzen_support )){

                $main_fornzen_support->update([
                    'supporters_ids' => json_encode($supporters_ids)
                ]);
            }
            

            // Notify Supporters via mobile app that deduction for this project transfered
            notifyMobile($project->id, $supporters_ids, 'transfered_donates', 'supporter',  'تم تحويل تبرعك الخاص بالمشروع إلى مشروع أخر', 'Your project donations has been transfered to another project', asset('storage/'.$project->image));

            // Notify Supporters via mobile app that monthly deduction for this project stopped
            notifyMobile($project->id, $supporters_ids, 'stop_montly_deduction', 'supporter',  $message->message_ar,   $message->message_en, null);
        }

        if($charity_id == null){

            return $response;
        }
        else{

            return $response->support_status;
        }

    }

    // get only the projects that have reports
    public function getProjectsHaveReports($user_id){

        // dd($this->model->with('reports', 'reports.phase', 'donates')->whereHas('donates', function($q) use ($user_id){

        //     $q->where('user_id', $user_id);

        // })->get());
        return $this->model->with('reports', 'reports.phase', 'donates')->whereHas('donates', function($q) use ($user_id){

            $q->where('user_id', $user_id);

        })->withCount('reports')->having('reports_count', '>', 0)->get();
    }


    public function whereHasWithLimit(array $with, $relation, array $data, array $data2 = [], $limit, $orderBy = ['column' => 'id', 'dir' => 'Asc']){

        return $this->model->with($with)->whereHas($relation, function($q) use($data){
            $q->where($data);
        })->where($data2)->orderBy($orderBy['column'], $orderBy['dir'])->limit($limit)->get();
    }


    public function updateProjectNumbers($project_num){

        $projects = $this->model->where('project_num', '>', $project_num)->get();

        foreach($projects as $project){

            $project->update(['project_num', $project->project_num - 1]);
        }
    }


    public function getCategoryProjects($slug){

        $nation_id = getNationId();

        if($slug == 'all'){
            $projects = $this->model->with(['charity', 'phases', 'currency', 'category'])->whereHas('charity', function($q){
                        $q->where('status', 'approved');
                    })->where([['profile', 0], ['nation_id', $nation_id] , 
                        ['status', 'approved'], ['active', 1]])->orderBy('donation_complete', 'ASC')->paginate(6);
        }
        else{

            $projects = $this->model->with(['charity', 'phases', 'currency', 'category'])->whereHas('category', function($q) use ($slug){
                    $q->where('slug', $slug);
                })->whereHas('charity', function($q){
                    $q->where('status', 'approved');
                })->where([['profile', 0], ['nation_id', $nation_id], ['status', 'approved'], ['active', 1]])->orderBy('donation_complete', 'ASC')->paginate(6);
        }

        return $projects;

    }

    public function searchProjects($token){

        $projects = $this->model->where(function($q){

                        $q->where([['profile', 0], ['active', 1], ['status', 'approved']]);

                    })->where('name', $token)->orWhere('project_num', $token)->orWhere('location', $token)
                        ->orWhere('goals_ar', $token)->orWhere('goals_en', $token)->orWhere('desc_ar', $token)
                        ->orWhere('desc_en', $token)->orWhere('long_desc_ar', $token)->orWhere('long_desc_en', $token)->paginate(6);
        return $projects;
    }
 
 
} // end of class
