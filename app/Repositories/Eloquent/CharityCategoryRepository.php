<?php

namespace App\Repositories\Eloquent;

use App\Models\CharityCategory;
use App\Repositories\CharityCategoryRepositoryInterface;

class CharityCategoryRepository extends BaseRepository implements CharityCategoryRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new CharityCategory();
        
    }

    public function getCategories($nation_id){


        $categories = $this->model->with(['projects', 'projects.currency', 'projects.phases', 'projects.ratings', 'projects.donates', 
                        'projects.donates.currency', 'projects.charity'])

                        ->whereHas('projects.charity', function($q) use ($nation_id){
                            $q->where([['status', 'approved'], ['nation_id', $nation_id]]);
                        })

                        ->whereHas('projects', function($q) use ($nation_id){
                            $q->where([['nation_id', $nation_id], ['status', 'approved'], ['active', 1], ['profile', 0]]);

                        })->get()->map(function($category){
                            $category->setRelation('projects', $category->projects->take(6));
    
                            return $category;
                        });

        return $categories;

    }
 
}
