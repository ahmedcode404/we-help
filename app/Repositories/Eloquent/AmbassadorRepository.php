<?php

namespace App\Repositories\Eloquent;

use App\Models\Ambassador;
use App\Repositories\AmbassadorRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class AmbassadorRepository extends BaseRepository implements AmbassadorRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Ambassador();
        
    } // end of constuct


    public function affiliate($affiliate)
    {
        return  $this->model->where('affiliate' , $affiliate)->first();
    }

}
