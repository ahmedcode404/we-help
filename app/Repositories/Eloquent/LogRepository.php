<?php

namespace App\Repositories\Eloquent;

use App\Models\EditLog;
use App\Repositories\LogRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class LogRepository extends BaseRepository implements LogRepositoryInterface
{

    public function __construct()
    {
        $this->model = new EditLog();
        
    }
  
}
