<?php

namespace App\Repositories\Eloquent;

use App\Models\CampaignGoal;
use App\Repositories\CampaignGoalRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CampaignGoalRepository extends BaseRepository implements CampaignGoalRepositoryInterface
{

    public function __construct()
    {
        $this->model = new CampaignGoal();
        
    } // end of constuct
}
