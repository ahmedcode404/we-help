<?php

namespace App\Repositories\Eloquent;

use App\Models\Nationality;
use App\Repositories\NationalityRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class NationalityRepository extends BaseRepository implements NationalityRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Nationality();
        
    } // end of constuct


}
