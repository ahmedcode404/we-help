<?php

namespace App\Repositories\Eloquent;

use App\Models\FrozenSupport;
use App\Repositories\FrozenSupportRepositoryInterface;

class FrozenSupportRepository extends BaseRepository implements FrozenSupportRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new FrozenSupport();
        
    }
 
}
