<?php

namespace App\Repositories\Eloquent;

use App\Models\Gift;
use App\Repositories\GiftRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class GiftRepository extends BaseRepository implements GiftRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Gift();
        
    } // end of constuct

}
