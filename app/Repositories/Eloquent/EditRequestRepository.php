<?php

namespace App\Repositories\Eloquent;

use App\Models\EditRequest;
use App\Repositories\EditRequestRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class EditRequestRepository extends BaseRepository implements EditRequestRepositoryInterface
{

    public function __construct()
    {
        $this->model = new EditRequest();
        
    } // end of constuct

    public function getRequestEditProject($id)
    {

        return $this->model->where([['model_id' , $id] , ['type' , 'project'] , ['status' , 'waiting']])->latest()->first();
        
    } // end of class

    public function getRequestEditCharity($id)
    {

        return $this->model->where([['model_id' , $id] , ['type' , 'charity'] , ['status' , 'waiting']])->latest()->first();
        
    } // end of class    
  
} // 
