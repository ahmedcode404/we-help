<?php

namespace App\Repositories\Eloquent;

use App\Models\M_Job;
use App\Repositories\JobRepositoryInterface;

class JobRepository extends BaseRepository implements JobRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new M_Job();
        
    }
 
}
