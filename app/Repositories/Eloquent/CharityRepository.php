<?php

namespace App\Repositories\Eloquent;

use App\Models\Charity;
use App\Models\Project;
use App\Models\User;
use App\Models\ProjectUser;
use App\Models\FrozenSupport;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\PaymentTransactionRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use Illuminate\Support\Facades\Validator;
use Storage;

class CharityRepository extends BaseRepository implements CharityRepositoryInterface
{

    private $supportRepository;
    private $paymentTransactionRepository;
    private $projectRepository;

    public function __construct(SupportRepositoryInterface $supportRepository,
                                PaymentTransactionRepositoryInterface $paymentTransactionRepository,
                                ProjectRepositoryInterface $projectRepository)
    {
        
        $this->model = new Charity();

        $this->supportRepository = $supportRepository;
        $this->paymentTransactionRepository = $paymentTransactionRepository;
        $this->projectRepository = $projectRepository;
        
    }

    public function loginWhere(array $data)
    {
        
        return $this->model->where($data)->first();

    } // end of loginWhere

    // Update Charity data - ADMIN
    public function adminUpdateCharity($request, $id){

        $charity = $this->model->with('categories')->find($id);

        $data = $request->except(['_token', '_method', 'phone_num', 'mobile_num', 'charity_category_ids', 'work_fields',
        'internal_image', 'year_report', 'logo', 'representer_passport_image', 'representer_nation_image', 'representer_title', 
        'attach', 'contact_number']);

        if(!$request->has('international_member')){

            $data['international_member'] = 0;
            $data['international_name_ar'] = null;
            $data['international_name_en'] = null;
        }

        if($request->has('attach')){

            // Delete old attach
            Storage::delete($charity->attach);

            // Upload new attach
            $data['attach'] = $request->file('attach')->store('uploads/charity');
        }

        if($request->has('license_file')){

            // Delete old license_file
            Storage::delete($charity->license_file);

            // Upload new license_file
            $data['license_file'] = $request->file('license_file')->store('uploads/charity');
        }

        if($request->has('internal_image')){

            // Delete old internal_image
            Storage::delete($charity->internal_image);

            // Upload new internal_image
            $data['internal_image'] = $request->file('internal_image')->store('uploads/charity');
        }

        if($request->has('year_report')){
            
            // Delete old year_report
            Storage::delete($charity->year_report);

            // Upload new year_report
            $data['year_report'] = $request->file('year_report')->store('uploads/charity');
        }

        if($request->has('logo')){
            
            // Delete old logo
            Storage::delete($charity->logo);

            // Upload new logo
            $data['logo'] = $request->file('logo')->store('uploads/charity/images');
        }

        if($request->has('representer_passport_image')){
            
            // Delete old representer_passport_image
            Storage::delete($charity->representer_passport_image);

            // Upload new representer_passport_image
            $data['representer_passport_image'] = $request->file('representer_passport_image')->store('uploads/charity');
        }

        if($request->has('representer_nation_image')){
            
            // Delete old representer_nation_image
            Storage::delete($charity->representer_nation_image);

            // Upload new representer_nation_image
            $data['representer_nation_image'] = $request->file('representer_nation_image')->store('uploads/charity');
        }

        if($request->has('representer_title_file')){
            
            // Delete old representer_title_file
            Storage::delete($charity->representer_title_file);

            // Upload new representer_title_file
            $data['representer_title_file'] = $request->file('representer_title_file')->store('uploads/charity');
        }
        
        if($request->has('charity_category_ids')){

            $charity->categories()->sync($request->charity_category_ids);
        }

        return $this->model->where('id', $id)->update($data);
    }

    // Register charity
    public function createCharity($request){

        // remove the last null item

        if(count($request->projects_names) > 0 && $request->projects_names[0] != null){

            if(!$request->has('admin_add_charity') && $request->has('projects_names') && $request->has('projects_types')){

                $validation = $this->validateImages($request);
    
                if($validation != true){
    
                    return $validation;
                }
            }

        }
        
        $data = $request->except(['_token', '_method', 'mobile_num', 'international_member', 'password', 'password_confirmation',
                        'project_type', 'project_name', 'images', 'admin_add_charity',
                        'license_file', 'representer_passport_image', 'representer_nation_image', 
                        'representer_title_file', 'attach', 'internal_image', 'year_report', 'logo', 'contact_number']);

        // remove special characters from text inputs and textareas
        $data['name'] = clearText($request->name);
        $data['strategy_ar'] = clearText($request->strategy_ar);
        $data['strategy_en'] = clearText($request->strategy_en);
        $data['address_ar'] = clearText($request->address_ar);
        $data['address_en'] = clearText($request->address_en);
        $data['representer_name_ar'] = clearText($request->representer_name_ar);
        $data['representer_name_en'] = clearText($request->representer_name_en);
        $data['representer_title_ar'] = clearText($request->representer_title_ar);
        $data['representer_title_en'] = clearText($request->representer_title_en);
        $data['international_name_ar'] = clearText($request->international_name_ar);
        $data['international_name_en'] = clearText($request->international_name_en);
        $data['account_number'] = clearText($request->account_number);
        $data['swift_code'] = clearText($request->swift_code);
        // $data['projects_types'] = clearText($request->projects_types);

        // handle the rest of inputs
        $data['international_member'] = $request->has('international_member') ? 1 : 0;
        $data['license_start_date'] = date('Y-m-d', strtotime($request->license_start_date));
        $data['establish_date'] = date('Y-m-d', strtotime($request->establish_date));
        $data['license_end_date'] = date('Y-m-d', strtotime($request->license_end_date));
        $data['contract_date'] = date('Y-m-d', strtotime($request->contract_date));


        $data['password'] = bcrypt($request->password);

        $data['nation_id'] = getNationId();
        $data['currency_id'] = session('currency');
        $data['slug'] = \Str::random(12);

        if($request->has('license_file')){

            $data['license_file'] = $request->license_file->store('uploads/charity');
        }

        if($request->has('representer_passport_image')){

            $data['representer_passport_image'] = $request->representer_passport_image->store('uploads/charity');
        }

        if($request->has('representer_nation_image')){

            $data['representer_nation_image'] = $request->representer_nation_image->store('uploads/charity');
        }

        if($request->has('representer_title_file')){

            $data['representer_title_file'] = $request->representer_title_file->store('uploads/charity');
        }

        if($request->has('attach')){

            $data['attach'] = $request->attach->store('uploads/charity');
        }

        if($request->has('internal_image')){

            $data['internal_image'] = $request->internal_image->store('uploads/charity');
        }

        if($request->has('year_report')){

            $data['year_report'] = $request->year_report->store('uploads/charity');
        }

        if($request->has('logo')){

            $data['logo'] = $request->logo->store('uploads/charity/images');
        }


        $charity = $this->model->create($data);

        if($request->has('charity_category_ids')){

            $charity->categories()->sync($request->charity_category_ids);
        }

        // if($charity){

        //     $charity->work_fields()->attach($request->work_fields);
        // }

        // $charity = $this->model->find(1);

        // create previous projects
        if($request->has('projects_names') && $request->has('projects_types')){

            $project_names = $request->projects_names;
            $projects_types = $request->projects_types;
    
            for($i = 0; $i < count($project_names); $i++){
    
                if($project_names[$i] != null){
                    $project_name = htmlspecialchars($project_names[$i]);
                    $project_name = strip_tags($project_names[$i]);
        
                    $project_type = htmlspecialchars($projects_types[$i]);
                    $project_type = strip_tags($projects_types[$i]);
        
                    $project_data['slug'] = \Str::random(12);
                    $project_data['project_num'] = rand();
                    $project_data['name'] = $project_name;
                    $project_data['type'] = $project_type;
                    // $project_data['image'] = asset('web/images/projects/1.png');
                    $project_data['currency_id'] = session('currency');
                    $project_data['cost'] = 0;
                    $project_data['nation_id'] = $charity->nation_id;
                    $project_data['country'] = $charity->country;
                    $project_data['city'] = $charity->city;
                    $project_data['profile'] = 1;
            
            
                    $project = Project::create($project_data);
            
                    $charity->projects()->save($project);
        
                    if($request->has('admin_add_charity')){

                        if($request->has('images')){
        
                            foreach($request->images as $image){
                
                                $image_path = $image->store('uploads/project/images');
            
                                $project->images()->create([
                                    'imageable_id' => $project->id,
                                    'imageable_type' => 'App\Models\Project',
                                    'path' => $image_path
                                ]);
                                
                                $project->update(['image', $image_path]);
                            }
                        }

                    }
                    else{

                        if($request->has('images_'.($i+1))){
        
                            foreach($request->{'images_'.($i+1)} as $image){
                
                                $image_path = $image->store('uploads/project/images');
            
                                $project->images()->create([
                                    'imageable_id' => $project->id,
                                    'imageable_type' => 'App\Models\Project',
                                    'path' => $image_path
                                ]);
            
                                // $project_data['image'] = $image_path;
                                
                                $project->update(['image', $image_path]);
                            }
                        }
                    }
                }
            }
        }
        

        return $charity;
    }

    // Delete charity - ADMIN
    public function deleteCharity($id){

        $response = new \stdClass;

        $charity = $this->model->with(['projects', 'projects.supports', 'projects.transactions', 'exchange_requests', 'categories', 
            'edit_requests', 'notifications', 'work_fields'])->find($id);

        $charity_id = $charity->id;
        $charity_name = $charity->name;
        $charity_projects = $charity->projects;

        // update charity projects to have charity_id = null
        $charity->projects()->update(['charity_id' => null]);

        $projects = $charity->projects;

        // update charity exchange requests to have charity_id = null - طلبات الصرف و سندات الصرف
        $charity->exchange_requests()->update(['requestable_id' => null, 'requestable_type' => null]);

        // break the many to many relation between charity and cgarity categories - امسح تصنيفات الجمعيه
        $categories = $charity->categories;
        foreach($categories as $cat){

            $cat->charities()->detach($charity->id);
        }

        // update charity edit requests to have charity_id = null
        $charity->edit_requests()->delete();

        // delete logo
        Storage::delete($charity->logo);

        // delete notifications
        $charity->notifications()->delete();

        // update charity reports to have charity_id = null
        $charity->reports()->update(['charity_id' => null]);

        // break the many to many relation between charity and work fields - امسح مجالات اعمال الجمعيه
        $work_fields = $charity->work_fields;
        foreach($work_fields as $field){

            $field->charities()->detach($charity->id);
        }

        $deleted = $charity->delete();
        // $deleted = 1;

        $response->charity_deleted = $deleted;

        if($deleted){

            /**
             *  هجيب كل مشاريع الجمعيه
             * هحسب اجماليات سند الصرف لكل مشروع لوحده
             * هحسب اجماليات التبرعات لكل مشروع لوحده
             * هطرح اجماليات سند الصرف للمشروع من اجماليات تبرعات المشروع
             * هيتم اضافة تبرع جديد بالمبلغ الناتج لاى مشروع اخر من نفس نوع المشروع الحالى
             * فى حالة عدم وجود مشاريع من نفس النوع .. هيتم ابلاغ السوبر ادمن ان المبلغ دا مجمد و يبقى ليه صلاحيه انه يدخل يضيفه لاى مشروع من اختياره كعملية تبرع برده
            */

            foreach($charity_projects as $project){


                $response->support_status = $this->projectRepository->adminDeleteProject($project->id, $charity_id);

            }
        }

        return $response;

    }

    public function validateImages($request){

        // dd($request->all());
        if(count($request->projects_names) > 0){

            for($i = 0; $i < count($request->projects_names); $i++){

                $imageRules = array(
                    'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
                );

                // dump($i+1);
                // if($i+1 != count($request->projects_names)){

                    $images = $request->{'images_'.($i+1)};

                    foreach($images as $image){

                        //Validate image
                        $image_to_validate = array('image' => $image);
                        $imageValidator = Validator::make($image_to_validate, $imageRules);
                        if ($imageValidator->fails()) {
                            return $imageValidator->messages();
                        }
                    }

                // }

            }
            // dd('aaa');

        }

        return true;
    }

    
}
