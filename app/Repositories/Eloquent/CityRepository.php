<?php

namespace App\Repositories\Eloquent;

use App\Models\City;
use App\Repositories\CityRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Project;
use App\Models\Charity;

class CityRepository extends BaseRepository implements CityRepositoryInterface
{

    public function __construct()
    {
        $this->model = new City();
        
    } // end of constuct

    public function country()
    {

        return $this->model->where('parent_id' , null)->get();

    } // end of country

    public function city()
    {

       return $this->model->where('parent_id' , '!=' , null)->get();

    } // end of country  
    
    public function cityOfCountry($country)
    {

        return $this->model->where('parent_id' , $country)->get();
        
    }

    public function countriesCount(){

        $projects = Project::distinct('country')->count();

        $charities = Charity::with('roles')->whereHas('roles', function($q){
            
            $q->where('name', 'charity');

        })->distinct('country')->count();

        return $projects + $charities;
    }

    public function adminCountriesCount($nation_id){

        $projects = Project::where('nation_id', $nation_id)->distinct('country')->count();

        $charities = Charity::with('roles')->whereHas('roles', function($q){
            
            $q->where('name', 'charity');

        })->where('nation_id', $nation_id)->distinct('country')->count();

        return $projects + $charities;
    }

  
}
