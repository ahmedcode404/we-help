<?php

namespace App\Repositories\Eloquent;

use App\Models\Degree;
use App\Repositories\DegreeRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class DegreeRepository extends BaseRepository implements DegreeRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Degree();
        
    } // end of constuct

}
