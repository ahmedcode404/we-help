<?php

namespace App\Repositories\Eloquent;

use App\Models\Image;
use App\Repositories\ImageRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class ImageRepository extends BaseRepository implements ImageRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Image();
        
    } // end of constuct
  

}
