<?php

namespace App\Repositories\Eloquent;

use App\Models\Question;
use App\Repositories\FaqRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class FaqRepository extends BaseRepository implements FaqRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Question();
        
    } // end of constuct

}
