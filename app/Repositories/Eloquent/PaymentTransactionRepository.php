<?php

namespace App\Repositories\Eloquent;

use App\Models\PaymentTransaction;
use App\Repositories\PaymentTransactionRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class PaymentTransactionRepository extends BaseRepository implements PaymentTransactionRepositoryInterface
{

    public function __construct()
    {
        $this->model = new PaymentTransaction();
        
    } // end of constuct
  
}
