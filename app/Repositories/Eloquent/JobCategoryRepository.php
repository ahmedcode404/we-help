<?php

namespace App\Repositories\Eloquent;

use App\Models\JobCategory;
use App\Repositories\JobCategoryRepositoryInterface;

class JobCategoryRepository extends BaseRepository implements JobCategoryRepositoryInterface
{

    public function __construct()
    {
        
        $this->model = new JobCategory();
        
    }
 
}
