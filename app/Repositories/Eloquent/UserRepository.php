<?php

namespace App\Repositories\Eloquent;

use App\Models\User;
use App\Repositories\UserRepositoryInterface;
use Auth;
use Spatie\Permission\Models\Permission;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    public function __construct()
    {
        $this->model = new User();
        
    }

    public function loginWhere(array $data)
    {
        
        return $this->model->where($data)->first();

    }  // end of login where 
    

    public function employee($nation_id)
    {

		return $this->model->where('id', '!=' , Auth::id())->whereHas('permissions' ,function($q){
            
            $q->whereIn('name', Permission::pluck('name'));
            
        })->with(['permissions'])->where('nation_id', $nation_id)->get();
        
    }  // end of employee   


    // get supporters with count projects
    public function donationCount($nation_id, $type){

        if ($type == 'max'){

            $users = $this->model->where('nation_id' , $nation_id)->withCount('projects')->orderBy('projects_count', 'DESC')->limit(100)->get(); 

            return $users->where('projects_count', '>', 0);

        } 
        else if($type == 'min'){

            $users = $this->model->where('nation_id' , $nation_id)->withCount('projects')->orderBy('projects_count', 'ASC')->limit(100)->get(); 

            return $users->where('projects_count', '>', 0);

        }
        else if($type == 'never'){

            return $this->model->where('nation_id' , $nation_id)->has('projects', '=', 0)->get(); 
        }


    } // end of donation count

}
