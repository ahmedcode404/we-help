<?php

namespace App\Repositories\Eloquent;

use App\Models\Report;
use App\Repositories\ReportRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use Storage;

class ReportRepository extends BaseRepository implements ReportRepositoryInterface
{

    public function __construct()
    {
        $this->model = new Report();
        
    } // end of constuct  

    public function updateReport(array $data, $id)
    {
        $this->model->where('id', $id)->update($data);
        return $this->model->where('id' , $id)->first();
    } 

    // update report - ADMIN
    public function adminUpdateReport($request, $id){

        $data = $request->except(['_token', '_method', 'video', 'images']);

        $report = $this->model->find($id);

        if($request->has('vedio')){

            try{

                // Delete old vedio
                Storage::delete($report->vedio);

                // Upload new vedio
                $data['vedio'] = $request->file('vedio')->store('uploads/reports/vedios');
            }
            catch(Throwable $e){
                dd('Error uploading video');
            }
            
            
        }

        if($request->has('images')){

            foreach($request->images as $image){

                $report->images()->create([
                    'imageable_id' => $report->id,
                    'imageable_type' => 'App\Models\Report',
                    'path' => $image->store('uploads/report/images')
                ]);
            }
        }

        return $this->model->where('id', $id)->update($data);
        
    }

    // Delete project image - ADMIN
    public function adminDeleteReportImage($image_id, $id){

        $report = $this->model->with('images')->find($id);

        $image = $report->images()->where('id', $image_id)->first();

        Storage::delete($image->path);

        return $image->delete();
    }

}
