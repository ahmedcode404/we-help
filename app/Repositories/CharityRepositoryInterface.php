<?php

namespace App\Repositories;

interface CharityRepositoryInterface
{
   
    public function loginWhere(array $data);

    public function adminUpdateCharity($request, $id);

    public function deleteCharity($id);

    public function validateImages($request);
}
