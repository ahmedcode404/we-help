<?php

namespace App\Repositories;

interface UserRepositoryInterface
{
    
    public function loginWhere(array $data);
    
    public function employee($nation_id);
    
    public function donationCount($nation_id, $type);

}
