<?php

namespace App\Repositories;

interface ProjectUserRepositoryInterface
{
    
    public function cost($id);

    public function costAllProject($ids);

}
