<?php

namespace App\Repositories;

interface RatingRepositoryInterface
{

    public function updateComment($id , $attr);
    
}
