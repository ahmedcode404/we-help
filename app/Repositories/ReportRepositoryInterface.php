<?php

namespace App\Repositories;

interface ReportRepositoryInterface
{

    public function updateReport(array $data, $id);
    
}
