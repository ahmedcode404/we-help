<?php

namespace App\Repositories;

interface ProjectRepositoryInterface
{
   
    public function updateProject(array $data, $id);

    public function deleteProject($id);
    
    public function adminUpdateProject($request, $id);

    public function adminDeleteProjectImage($image_id, $project_id);

    public function getBenefCount(array $data);

    public function cost($id);   
    
    public function waiting(); 

    public function approved();  

    public function rejected(); 

    public function hold();   

    public function checkCommentOrNo($project_id , $user_id);

    public function whereHasWithLimit(array $with, $relation, array $data, array $data2, $limit, $orderBy);

    public function updateProjectNumbers($project_num);

    public function getCategoryProjects($slug);

    public function searchProjects($token);
    


}
