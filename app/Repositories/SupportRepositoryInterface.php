<?php

namespace App\Repositories;

interface SupportRepositoryInterface
{

    public function totalAmmount(array $data);
    
}
