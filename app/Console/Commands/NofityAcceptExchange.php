<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class NofityAcceptExchange extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notifiy:acceptexchange {financial}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'notifiy when charity dont accept bond exchange';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $financialrepository = \App::make('App\Repositories\FinancialRequestRepositoryInterface');

        $financial_id = $this->argument('financial');

        $financial = $financialrepository->findOne($financial_id);

        // send notify accept exchange to charity
        sendNotifiyAcceptBondExchange($financial);

        $this->info('send notify accept exchange to charity successfully'); 

    }
}
