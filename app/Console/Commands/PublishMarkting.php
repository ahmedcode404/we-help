<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PublishMarkting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish:markting {markting}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Message To Supporter Sms Or Email Or Notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $marktingRepository = \App::make('App\Repositories\MarketingRepositoryInterface');

        $markting_id = $this->argument('markting');

        $markting = $marktingRepository->findWith($markting_id, ['users']);

        // send  email or sms to supporter
        \Log::info(sendNotificationMarkting($markting));

        $this->info('Publish Markting Send All User');        

    }
}
