<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;
use App\Servicies\Notify;

class Redonate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:redonate {support}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Repeat project donate monthly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $supportRepository = \App::make('App\Repositories\SupportRepositoryInterface');
        $notificationRepository = \App::make('App\Repositories\NotificationRepositoryInterface');
        $projectRepository = \App::make('App\Repositories\ProjectRepositoryInterface');
        
        $support = json_decode($this->argument('support'));

        $data['user_id'] = $support->user_id;
        $data['project_id'] = $support->project_id;
        $data['status'] = $support->status;
        $data['cost'] = $support->cost;
        $data['type'] = $support->type;
        $data['nation_id'] = $support->nation_id;
        $data['currency_name'] = $support->currency_name;

        $new_support = $supportRepository->create($data);

        $support = $supportRepository->findWith($new_support->id, ['project', 'user']);

        $payment = recurringPayment($support);

        if($payment == true){

            // Notify user
            // notification 
            $project = $projectRepository->findOne($support->project_id);
            $message = new \stdClass();
            $message->message_ar = 'تم خصم التبرع الشهرى لمشروع'.$project->name;
            $message->message_en = 'The monthly donation for '.$project->name.' project has been deducted';

            $notification_data['message_en'] = $message->message_en;
            $notification_data['message_ar'] = $message->message_ar;
            $notification_data['send_user_id'] = $support->user_id;
            $notification_data['title'] = 'montly_deduction';
            $notification_data['type'] = 'user';

            $notificationRepository->create($notification_data);            

            Notify::NotifyWeb($message , 'montly_deduction' , 'supporter' , $support->user_id); 
            // Notify::NotifyMobile($message , 'montly_deduction' , 'supporter' , $support->user_id); 
            notifyMobile(null, [$support->user_id], 'montly_deduction', 'supporter', 'تم خصم التبرع الشهرى لمشروع'.$project->name, 'The monthly donation for '.$project->name.' project has been deducted', asset('web/images/main/user_avatar.png'));
            
        }


        if($new_support){

            \Log::info('support created');
        }
        else{
            \Log::info('error with support create');
        }
    }
}
