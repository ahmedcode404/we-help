<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Carbon;
use App\Servicies\Notify;

class Kernel extends ConsoleKernel
{

    protected $commands = [

        Commands\PublishMarkting::class,
        Commands\Redonate::class,
    
    ];

    // FUNCTION SEND NOTIFICATION EMAIL OF SMS OR MOBILE TO SUPPORTER IN ANY TIME
    protected function schedule(Schedule $schedule)
    {

        /******************************************** schedulign marketing compaigns *************************************************/
        $marketinfRepository = \App::make('App\Repositories\MarketingRepositoryInterface');

        $marketings = $marketinfRepository->getAll();

        foreach($marketings as $mar){

            // day
            $day = $mar->date->format('Y-m-d');

            // time
            $time = $mar->time->format('H:i');

            $job_time = Carbon::parse($time);
            $job_time = $job_time->addMinutes(1)->format('H:i');

            // concatinate day with time
            $timestamp = Carbon::createFromTimestamp(strtotime($day . $time));
            // time now
            $now = Carbon::now();
 
            // if today's date == campaign date, then send the messages
            if($timestamp->format('Y-m-d') == $now->format('Y-m-d'))
            {

                $schedule->command('publish:markting', [$mar->id])->dailyAt($time)->timezone('Asia/Riyadh')->withoutOverlapping();
                // $schedule->command('publish:markting', [$mar->id])->dailyAt('14:25')->timezone('Africa/Cairo')->withoutOverlapping();

                // run queue:work --once to send marketing emails
                $schedule->command('queue:work --once')->dailyAt($job_time)->timezone('Asia/Riyadh')->withoutOverlapping();
                // $schedule->command('queue:work --once')->dailyAt('13:37')->timezone('Africa/Cairo')->withoutOverlapping();

            } // end of if timestam >= time now

        } // end of foreach

        /******************************************** scheduling donation monthly ***************************************************/
        $supportRepository = \App::make('App\Repositories\SupportRepositoryInterface');
        $paymentTransactionRepository = \App::make('App\Repositories\PaymentTransactionRepositoryInterface');
        $notificationRepository = \App::make('App\Repositories\NotificationRepositoryInterface');

        $supports = $supportRepository->getWhereGroupByAll([['status', 'support'], ['type', 'monthly']]);

        $complete_projects_arr = [];
        $supporters_arr = [];
        $supporters_ids = [];

        foreach($supports as $support){

            if($support->cost != 0){

                // stop support when the project supports are complete
                // get the remainig cost for project - الحسابات بتتم على اساس عملة التبرع
                $remaining_cost = exchange($support->project->get_total, $support->project->currency_id, $support->currency_id) - getTotalSupportsForProject($support->project, $support->currency_id);

                // compare the values
                if($support->cost > $remaining_cost){

                    array_push($complete_projects_arr, $support->project);
                    array_push($supporters_arr, $support->user);
                    array_push($supporters_ids, $support->user->id);

                }
                // الدفع الفعلى بيتم تلقائيا من بوابة الدفع و بيتم تسجيلة فى قاعدة البيانات
                else{ 
                    $schedule->command('project:redonate', [$support])->monthlyOn($support->created_at->format('d'), $support->created_at->format('H:i'))->timezone('Asia/Riyadh')->withoutOverlapping();
                }
            }
        }

        if(count($complete_projects_arr) > 0){

            // Convert all user supports for this project to once (stop monthy deduction)
            for($i = 0; $i < count($complete_projects_arr); $i++){

                $supportRepository->updateWhere(['type' => 'once'], [['project_id', $complete_projects_arr[$i]->id], 
                                                                    ['user_id', $supporters_arr[$i]->id],
                                                                    ['type', 'monthly']]);

                // Cancle strip subscription -  ايقاف الدفع الشهرى للمشروع دا من بوابة الدفع
                $transactions = $paymentTransactionRepository->getWhere([['project_id', $complete_projects_arr[$i]->id], ['canceled', 0]]);

                foreach($transactions as $transaction){

                    if($transaction->subscription_id != null){

                        // Set your secret key. Remember to switch to your live secret key in production.
                        // See your keys here: https://dashboard.stripe.com/apikeys
                        \Stripe\Stripe::setApiKey(config('services.stripe.apiKey'));

                        $subscription = \Stripe\Subscription::retrieve($transaction->subscription_id);
                        
                        $subscription->cancel();

                        $paymentTransactionRepository->update(['canceled' => 1], $transaction->id);

                    }

                }


                // Notify user
                // notification 
                $message = new \stdClass();
                $message->message_ar = 'تم إيقاف التبرع الشهرى لمشروع '.$complete_projects_arr[$i]->name.'نظراً لإكتمال التبرعات';
                $message->message_en = 'The monthly donation to '.$complete_projects_arr[$i]->name.' project has been stopped due to the completion of donations';

                // $notification_data['message_en'] = $message->message_en;
                // $notification_data['message_ar'] = $message->message_ar;
                // $notification_data['send_user_id'] = $supporters_arr[$i]->id;
                // $notification_data['title'] = 'stop_montly_deduction';
                // $notification_data['type'] = 'user';
        
                // $notificationRepository->create($notification_data);

                // Notify Supporters via website
                Notify::NotifyWeb($message , 'stop_montly_deduction' , 'supporter' , $supporters_arr[$i]->id); 

                // Notify Supporters via mobile app
                notifyMobile($complete_projects_arr[$i]->id, $supporters_ids,  'stop_montly_deduction', 'supporter',  $message->message_ar,   $message->message_en, null);

            }
        }


        ///////////////////////////////// BEGIN: notify accept bond exchange ///////////////////////////////

        $financialrepository = \App::make('App\Repositories\FinancialRequestRepositoryInterface');

        $financials = $financialrepository->getWhere([['recieve_status' , 'paid'] , ['status' , 'approved']]);

        foreach($financials as $financial)
        {

            $schedule->command('notifiy:acceptexchange', [$financial->id])->quarterly($financial->updated_at->format('d'), $financial->updated_at->format('H:i'))->timezone('Asia/Riyadh')->withoutOverlapping();

        } // end of foreach

        ///////////////////////////////// END: notify accept bond exchange ///////////////////////////////

        // Run job queue
        // $schedule->command('job:queue')->dailyAt('12:00')->timezone('Asia/Riyadh')->withoutOverlapping();
        // $schedule->command('queue:work')->dailyAt('16:59')->timezone('Africa/Cairo')->withoutOverlapping();

        
    } // end of function scedule

    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
