<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WeHelp extends Mailable
{
    use Queueable, SerializesModels;

    protected $topic;
    protected $message;
    protected $notes;
    protected $extra_data;
    protected $phone;
    protected $email;
    protected $nation_id;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($topic, $message, $nation_id, $notes, $extra_data)
    {
        $this->topic = $topic;
        $this->message = $message;
        $this->nation_id = $nation_id;
        $this->notes = $notes;
        $this->extra_data = $extra_data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['topic'] = $this->topic;
        $data['message'] = $this->message;
        $data['notes'] = $this->notes;
        $data['nation_id'] = $this->nation_id;
        $data['extra_data'] = $this->extra_data;

        return $this->markdown('admin.emails.email')->with([
            'data' => $data,
        ]);

    }
}
