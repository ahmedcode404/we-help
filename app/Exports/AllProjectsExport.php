<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Project;

class AllProjectsExport implements FromView
{
    private $status;

    public function __construct($status){

        $this->status = $status;
    }

    public function view(): View
    {

        $nation_id = getAuthNationId();

        if($this->status == 'projects'){

            $data['projects'] = Project::With('charity', 'category', 'phases', 'reports', 'sponsers', 'edit_requests', 'exchange_requests',
                                'service_option', 'service_feature')->
                                where([['active', 1], ['profile', 0], ['nation_id', $nation_id]])->get();

        }
        else{

            $data['projects'] = Project::With('charity', 'category', 'phases', 'reports', 'sponsers', 'edit_requests', 'exchange_requests',
                                'service_option', 'service_feature')->
                                where([['status', $this->status], ['active', 1], ['profile', 0], ['nation_id', $nation_id]])->get();
        }
        
        return view('admin.projects.excel.export-all')->with([
            'data' => $data
        ]);
    }
}
