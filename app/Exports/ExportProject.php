<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Project;

class ExportProject implements FromView
{
    private $id;

    public function __construct($id){

        $this->id = $id;
    }

    public function view(): View
    {
        $nation_id = getAuthNationId();

        $data['projects'] = Project::With('charity', 'category', 'phases', 'reports', 'sponsers', 'edit_requests', 'exchange_requests',
                                'service_option', 'service_feature')->
                                where([['id', $this->id], ['active', 1], ['profile', 0], ['nation_id', $nation_id]])->get();
        
        return view('admin.projects.excel.export-all')->with([
            'data' => $data
        ]);
    }
}
