<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Charity;

class ExportCharity implements FromView
{
    private $id;

    public function __construct($id){

        $this->id = $id;
    }

    public function view(): View
    {

        $data['charities'] = Charity::whereHas('roles', function($q){
            
            $q->where('name', 'charity');

        })->where('id', $this->id)->With('projects', 'employees', 'edit_requests', 'exchange_requests', 'categories')->get();
        
        return view('admin.charities.excel.export-all')->with([
            'data' => $data
        ]);
    }
}
