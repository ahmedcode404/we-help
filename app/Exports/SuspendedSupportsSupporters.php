<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SuspendedSupportsSupporters implements FromView
{
    private $frozenSupportRepository;

    public function __construct($frozenSupportRepository){

        $this->frozenSupportRepository = $frozenSupportRepository;
    }

    public function view(): View
    {
        $nation_id = getAuthNationId();

        $data['supports'] = $this->frozenSupportRepository->paginateWhereHasWith(['fromProject', 'fromProject.charity'], 'fromProject', 
                            [['nation_id', $nation_id]], 10, [['to_project_id', null]]);

        return view('admin.supports.excel.supporters')->with([
            'data' => $data
        ]);
    }
}
