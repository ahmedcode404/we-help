<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\Models\Charity;

class AllCharitiesExport implements FromView
{
    private $status;

    public function __construct($status){

        $this->status = $status;
    }

    public function view(): View
    {

        if($this->status == 'charities'){

            $data['charities'] = Charity::whereHas('roles', function($q){
            
                $q->where('name', 'charity');
    
            })->With('projects', 'employees', 'edit_requests', 'exchange_requests', 'categories')->get();

        }
        else{

            $data['charities'] = Charity::whereHas('roles', function($q){
            
                $q->where('name', 'charity');
    
            })->where('status', $this->status)->With('projects', 'employees', 'edit_requests', 'exchange_requests', 'categories')->get();
        }
        
        return view('admin.charities.excel.export-all')->with([
            'data' => $data
        ]);
    }
}
