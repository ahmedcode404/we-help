<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\TransfereProjectSupports;

class SendTransfereSupportEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data  = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // get all emails subscribe
        $users = $this->data['users'];

        $emails = [];

        foreach($users as $user)
        {

            $email = ['email' => $user->email];
            array_push($emails, $email);
        }

        // send data to send multi email 
        $email = new TransfereProjectSupports($this->data);
        // send to  all email
        Mail::to($emails)->send($email); 
    }
}
