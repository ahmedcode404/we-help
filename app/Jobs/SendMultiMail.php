<?php

namespace App\Jobs;

use App\Mail\SendMailMarkting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMultiMail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {

        $this->data = $data;

    }

    public function handle()
    {

        // get all emails subscribe
        $users = $this->data['users'];
        $emails = [];

        foreach($users as $user)
        {

            $emails[] = $user->email;

        }

        // send data to send multi email 
        $email = new SendMailMarkting($this->data);
        // send to  all email
        Mail::to($emails)->send($email); 

    }

}
