<?php

namespace App\Jobs;

use App\Mail\SendEmailGifts as MailSendEmailGifts;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmailGifts implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {

        $this->data = $data;

    }

    public function handle()
    {

  
        $users = $this->data['emails'];

        $emails = [];

        foreach($users as $user)
        {

            $emails[] = $user;

        }        

        // send data to send multi email 
        $email = new MailSendEmailGifts($this->data);
        // send to  all email
        Mail::to($emails)->send($email); 

    }
}
