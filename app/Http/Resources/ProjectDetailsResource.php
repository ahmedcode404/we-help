<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\CommentResource;
use App\Http\Resources\ImageResource;

class ProjectDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $session_currency = currencySymbol(session('currency'));

        $remaining = generalExchange($this->get_total, $this->currency->symbol, $session_currency) - $this->total_supports_for_project;

        if($remaining < 0){
            $remaining = 0;
        }

        return [
            'id' => $this->id,
            'images' => ImageResource::collection($this->images),
            'target' => number_format(generalExchange($this->get_total, $this->currency->symbol, $session_currency), 2),
            'beneficiaries' => $this->benef_num,
            'donated_amount' => number_format($this->total_supports_for_project, 2),
            'remaining_amount' => number_format($remaining, 2),
            'project_name' => $this->name,
            'charity_name' => $this->charity ? $this->charity->name : '',
            'charity_logo' => $this->charity && $this->charity->logo ? asset('storage/'.$this->charity->logo) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'rating' => $this->getRatings(),
            'description' => $this->long_desc,
            'comments' => CommentResource::collection($this->getProjectRatings()),
            // 'short_description' => $this->desc,
            'link' => route('web.projects.show', $this->slug),
            'donation_complete' => $this->donation_complete == 1 ? 1 : 0,
            'currency' => $session_currency
        ];
    }
}
