<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->phase ? $this->phase->name : trans('admin.project_report'),
            'content' => $this->report,
            'video' => $this->vedio ? asset('storage/'.$this->vedio) : null,
        ];
    }
}
