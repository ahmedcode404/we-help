<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];

        if($this->title == 'new_project'){

            $item = [
                'type' => 'screen',
                'id' => $this->model_id
            ];
            
            array_push($data, $item);
        }
        else if($this->title == 'new_voucher'){

            $financialRequestRepository = \App::make('App\Repositories\FinancialRequestRepositoryInterface');
            $voucher = $financialRequestRepository->findOne($this->model_id);

            $item = [
                'type' => 'web_view',
                'link' => route('get-voucher', $voucher->slug)
            ];

            array_push($data, $item);
        }
        else{

            $item = [
                'type' => 'no_action',
            ];

            array_push($data, $item);
        }

        return [
            'id' => $this->id,
            'title' => trans('api.'.$this->title),
            'message' => $this->message,
            'image' => $this->image ? $this->image : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'time' => \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans(),
            'type' => $this->type,
            'data' => $data
        ];
    }
}
