<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CharityCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $main_color = ltrim($this->main_color, '#');
        $main_color = '0xFF'.$main_color;

        $sub_color = ltrim($this->sub_color, '#');
        $sub_color = '0xFF'.$sub_color;

        return [
            'id' => $this->id,
            'name' => ucfirst($this->name),
            'icon' => $this->icon ? asset('storage/'.$this->icon) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'main_color' => $main_color,
            'sub_color' => $sub_color
        ];
    }
}
