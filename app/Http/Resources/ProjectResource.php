<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use Illuminate\Http\Resources\Json\ResourceCollection;


class ProjectResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {   
        return [
            'data' => $this->collection->transform(function($project){

                $session_currency = currencySymbol(session('currency'));

                $remaining = generalExchange($project->get_total, $project->currency->symbol, $session_currency) - $project->total_supports_for_project;

                if($remaining < 0){

                    $remaining = 0;
                }

                return [
                    'id' => $project->id,
                    'charity_name' => $project->charity ? $project->charity->name : null,
                    'charity_logo' => $project->charity && $project->charity->logo ? asset('storage/'.$project->charity->logo) : asset('dashboard/app-assets/images/avatars/avatar.png'),
                    'rating' => $project->getRatings(),
                    'project_name' => $project->name,
                    'project_image' => $project->image ? asset('storage/'.$project->image) : asset('dashboard/app-assets/images/avatars/avatar.png'),
                    'description' => $project->desc,
                    'total_donations' => number_format($project->total_supports_for_project, 2),
                    'remaining_donations' => number_format($remaining, 2),
                    'donation_complete' => $project->donation_complete == 1 ? 1 : 0,
                    'currency' => $session_currency
                ];
            }),
            "links" => [
                "prev" => $this->previousPageUrl(),
                "next" => $this->nextPageUrl(),
            ],

            "meta" => [
                "current_page" => $this->currentPage(),
                "next_page" => $this->nextPageUrl(),
                "from" => $this->firstItem(),
                "to" => $this->lastItem(),
                "last_page" => $this->lastPage(), // not For Simple
                "per_page" => $this->perPage(),
                'count' => $this->count(), //count of items at current page
                "total" => $this->total() // not For Simple
            ],
        ];
    }
}
