<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [

            'user_image' => $this->user && $this->user->image ? asset('storage/'.$this->user->image) : asset('web/images/main/user_avatar.png'),
            'user_name' => $this->user ? $this->user->name : null,
            'rating' => $this->avg_grade,
            'date' => $this->date($this->user_id, $this->project_id), 
            'comment' => $this->getComment($this->user_id, $this->project_id) ? $this->getComment($this->user_id, $this->project_id)->comment : null,
        ];
    }
}
