<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Monarobase\CountryList\CountryListFacade;

class PersonalInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->image ? asset('storage/'.$this->image) : asset('web/images/main/user_avatar.png'),
            'name' => $this->name,
            'email' => $this->email,
            'phone' => $this->phone,
            'country_id' => $this->country, // country key
            'country_name'  => \Countries::getOne($this->country, \App::getLocale()), // country name according to key
            'blocked' => $this->blocked,
            'is_notify' => $this->is_notify,
            'currency_id' => $this->currency_id,
            'currency_name' => currencySymbol(session('currency'))
        ];
    }
}
