<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\ReportResource;

class CategorizedDonationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'project_image' => $this->image ? asset('storage/'.$this->image) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'project_name' => $this->name,
            'rating' => $this->getRatings(),
            'reports_no' => count($this->reports),
            'reports' => ReportResource::collection($this->reports),
            'donation_complete' => $this->donation_complete == 1 ? 1 : 0,
        ];
    }
}
