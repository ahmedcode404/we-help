<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\VoucherResource;

class DonatedProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if($this->project){
            
            $name = $this->project->name;
        }
        else{
            $name = '';
        }

        return [
            'project_id' => $this->project ? $this->project->id : '',
            'project_image' => $this->project && $this->project->image ? asset('storage/'.$this->project->image) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'project_name' => $name,
            'rating' => $this->project ? $this->project->getRatings() : 0,
            'description' => $this->project ? $this->project->long_desc : '',
            'project_goal' => $this->project ? number_format(exchange($this->project->get_total, $this->project->currency_id, session('currency')), 2).' '.currencySymbol(session('currency')) : '0.0 '.currencySymbol(session('currency')),
            'donated_money' => number_format(exchange($this->cost, $this->currency_id, session('currency')), 2).' '.currencySymbol(session('currency')),
            'voucher_link' => VoucherResource::collection(auth()->user()->catch_vouchers()->where('project_id', $this->project_id)->get()),
            // 'voucher_link' => new VoucherResource(auth()->user()->catch_vouchers()->where('project_id', $this->project_id)->first()),
            'donation_complete' => $this->project && $this->project->donation_complete == 1 ? 1 : 0,
        ];
    }
}
