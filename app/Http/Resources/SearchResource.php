<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $session_currency = currencySymbol(session('currency'));

        $remaining = generalExchange($this->get_total, $this->currency->symbol, $session_currency) - $this->total_supports_for_project;

        if($remaining < 0){
            $remaining = 0;
        }
        return [
            'id' => $this->id,
            'charity_name' => $this->charity ? $this->charity->name : null,
            'charity_logo' => $this->charity && $this->charity->logo ? asset('storage/'.$this->charity->logo) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'rating' => $this->getRatings(),
            'project_name' => $this->name,
            'project_image' => $this->image ? asset('storage/'.$this->image) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'description' => $this->desc,
            'long_description' => $this->long_desc,
            'goals' => $this->goals,
            'total_donations' => number_format($this->total_supports_for_project, 2),
            'remaining_donations' => number_format($remaining, 2),
            'donation_complete' => $this->donation_complete == 1 ? 1 : 0,
            'currency' => $session_currency
        ];
    }
}
