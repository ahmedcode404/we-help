<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $session_currency = currencySymbol(session('currency'));

        if($this->project){
            
            $name = $this->project->name;

            $remaining = ( generalExchange($this->project->get_total, $this->project->currency->symbol, $session_currency) - $this->project->total_supports_for_project);
        }
        else{
            $name = '';
            
            $remaining = 0;
        }

        if($remaining < 0){

            $remaining = 0;
        }

        return [
            'id' => $this->id,
            'campaing_name' => $this->name,
            'target' => $this->project ?  generalExchange($this->project->get_total, $this->project->currency->symbol, $session_currency) : 0,
            'images' => $this->project && $this->project->images ? ImageResource::collection($this->project->images) : null,
            'beneficiaries' => $this->project ? $this->project->benef_num : 0,
            'donated_amount' => $this->project ? $this->project->total_supports_for_project : 0,
            'remaining_amount' => $this->project ? $remaining : 0,
            'project_name' => $name,
            'charity_name' => $this->project && $this->charity ? $this->charity->name : null,
            'charity_logo' => $this->project && $this->charity && $this->charity->logo ? asset('storage/'.$this->charity->logo) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'rating' => $this->project ? $this->project->getRatings() : 0,
            'description' => $this->project ? $this->project->long_desc : '',
            'comments' => $this->project ? CommentResource::collection($this->project->getProjectRatings()) : null,
            'donation_complete' => $this->project && $this->project->donation_complete == 1 ? 1 : 0,
            'currency' => $session_currency
        ];
    }
}
