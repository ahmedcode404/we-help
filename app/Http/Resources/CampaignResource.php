<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CampaignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'project_id' => $this->project_id,
            'campaing_name' => $this->campaign_name,
            'project_image' => $this->project && $this->project->image ? asset('storage/'.$this->project->image) : asset('dashboard/app-assets/images/avatars/avatar.png'),
            'project_name' => $this->project ? $this->project->name : '',
            'rating' => $this->project ? $this->project->getRatings() : 0,
            'desc' => $this->project ? $this->project->desc : null,
            'donation_complete' => $this->project && $this->project->donation_complete == 1 ? 1 : 0,
        ];
    }
}
