<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class UserController extends Controller
{
    public function saveToken(Request $request)
    {
        $updated = 0;
        
        if(auth()->guard('charity')->check())
        {
        
            $updated = auth()->guard('charity')->user()->update(['device_token' => $request->token]);

        } else if(auth()->check())
        {
            // dd('admin');
            $updated = auth()->user()->update(['device_token' => $request->token]);
            
        }

        if($updated){

            return response()->json(['token saved successfully.']);
        }
        else{

            return response()->json(['User not logged in']);
        }


    } // end of saveToken

}
