<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthorizedConrtoller extends Controller
{

    public function authorized()
    {

        return redirect('charity/dashboard');

    }

    public function returnAuthorized()
    {

        return view('auth.not-authorized');

    }   
    
    public function returnNotFound()
    {

        return view('auth.not-found');

    }       

} // end of class
