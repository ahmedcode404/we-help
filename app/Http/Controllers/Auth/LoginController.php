<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use App\Http\Requests\login\LoginRequest;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CartRepositoryInterface;
use Illuminate\Http\Request;
use App;
use Hash;

class LoginController extends Controller
{

    private $charityrepository;
    private $userrepository;
    private $cartrepository;

    public function __construct(CharityRepositoryInterface $charityrepository, 
                                UserRepositoryInterface $userrepository,
                                CartRepositoryInterface $cartrepository)
    {

        $this->charityrepository = $charityrepository;
        $this->userrepository = $userrepository;
        $this->cartrepository = $cartrepository;
        
    } // end of construct

    public function login($role = null)
    {
        if($role == null){

            return view('auth.login');
        }
        else{

            return view('web.auth.login', compact('role'));
        }

    } // end of login

    public function loginAuth(LoginRequest $request)
    {
        
        // Admin + Employee login
        if($request->role ==  'admin'){

            // check in table user
            $user = $this->userrepository->getWhere([['email', $request->email]])->first();
            
            if(isset($user) && Hash::check($request->password, $user->password))
            {
                // Employee login
                if($user->hasRole('employee')){

                    if($user->first_login == 1){

                        $data['agreement'] = getAgreement('emp_politics_rules', $user->nation_id);

                        $data['user_email'] = $user->email;
    
                        return view('admin.employees.agreement')->with([
    
                            'data' => $data
                        ]);
                    }
                    else{

                        auth()->login($user);

                        if(session()->has('cart_no')){
            
                            $this->cartrepository->updateWhere(['user_id' => $user->id, 'cart_no' => null], [['cart_no', session('cart_no')]]);
                            
                            session()->forget('cart_no');
                        }
            
                        return redirect()->route('admin.home');
                        // return redirect()->route('admin.home')->with('success' , __('admin.successLogin'));
                    }

                }
                // Admin login
                else if($user->hasRole('admin')){

                    // update admin nation_id: الادمن هيشوف البيانات الخاصه بنسخته فقط و لذلك يجب تحديث ال  nation_id
                    $nation_id = getNationId();

                    $user->update(['nation_id' => $nation_id]);

                    auth()->login($user);

                    if(session()->has('cart_no')){

                        $this->cartrepository->updateWhere(['user_id' => $user->id, 'cart_no' => null], [['cart_no', session('cart_no')]]);

                        session()->forget('cart_no');
                    }

                    // session()->put('currency' , auth()->user()->currency_id);

                    return redirect()->route('admin.home');
                }
    
            } else 
            {
    
                return redirect()->back()->with('error' , __('admin.error_login'));
    
            }  // end of else user          
        

        } 
        // Charity login
        else if($request->role ==  'association') {

            $charity = $this->charityrepository->getWhere([['email', $request->email]])->first();

            // check in table charity
            if(isset($charity) && Hash::check($request->password, $charity->password))
            {

                // Employee login
                if($charity->hasRole('charity_employee')){

                    if($charity->first_login == 1){

                        $data['agreement'] = getAgreement('emp_politics_rules', $charity->nation_id);

                        $data['user_email'] = $charity->email;
    
                        return view('charities.employees.agreement')->with([
    
                            'data' => $data
                        ]);
                    }
                    
                    auth()->guard('charity')->login($charity);
                        
                    return redirect()->route('charity.dashboard');

                }

                auth()->guard('charity')->login($charity);
    
                return redirect()->route('charity.dashboard');
    
            // check in table user    
            } 
            else
            {

                return redirect()->back()->with('error' , __('admin.error_login'));
    
            } // end of else charity       
            
            return redirect()->back()->with('error' , __('admin.error_login'));

        }
        // Donor login
        else if($request->role == 'donor'){

            $user = $this->userrepository->getWhere([['email', $request->email]])->first();
            
            if(isset($user) && Hash::check($request->password, $user->password))
            {
                if($user->hasRole('supporter')){

                    auth()->login($user);

                    if(session()->has('cart_no')){

                        $this->cartrepository->updateWhere(['user_id' => $user->id, 'cart_no' => null], [['cart_no', session('cart_no')]]);
                        session()->forget('cart_no');
                    }

                    // session()->put('currency' , auth()->user()->currency_id);

                    return redirect()->route('web.home');
                }
                else{

                    return redirect()->back()->with('error' , __('admin.error_login'));

                }
    
            } else 
            {
                return redirect()->back()->with('error' , __('admin.error_login'));
    
            }  // end of else user          
        }
        else{
            
            return redirect()->back()->with('error' , __('admin.error_login'));

        }
        // end of else check role
        

    } // end of class

    public function logout()
    {
  
        if(auth()->guard('charity')->check()){

            auth()->guard('charity')->logout();
        }
        else{

            auth()->logout();
        }

        // return redirect()->route('web.home')->with('success' , __('admin.successLogout'));
        return redirect()->route('web.home');
    }

} // end of class
