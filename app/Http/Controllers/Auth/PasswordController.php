<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\UserRepositoryInterface;

class PasswordController extends Controller
{

    public $charityrepository;
    public $userrepository;


    public function __construct(CharityRepositoryInterface $charityrepository, 
                                UserRepositoryInterface $userrepository
                                ){

        $this->charityrepository = $charityrepository;
        $this->userrepository = $userrepository;
        
    }


    public function getPasswordForm(){

        return view('auth.forgot-password');
    }

    public function sendResetPasswordLink(Request $request){

        $this->validate($request, [
            'role' => 'required|in:admin,charity',
            'email' => 'required|email',

        ]);

        if($request->role == 'admin'){

            $this->validate($request, [
                'email' => 'exists:users'
            ]);
        }
        else if($request->role == 'charity'){

            $this->validate($request, [
                'email' => 'exists:charities'
            ]);
        }
        

        // Send email to reset password
        $message = '<p>'.trans('admin.press_link_to_reset_password').'</p><a href="'.route("reset-password", ['email' => $request->email, 'role' => $request->role]).'">'.trans('admin.reset_password').'</a>';

        $topic = trans('admin.reset_password');
        $to = [
            [
                'email' => $request->email,
                'name' => $request->email
            ]
        ];
        
        sendMail($to, $topic, $message, getNationId());

        session()->flash('success', __('admin.reset_password_link_sent'));

        return redirect()->back();

    }

    public function resetPassword($email, $role){
        
        return view('auth.set_new_password')->with(['email' => $email, 'role' => $role]);
    }

    public function storeNewPassword(Request $request){

        $this->validate($request, [

            'password' => 'required|min:6|confirmed'

        ]);
        

        $data['password'] = bcrypt($request->password);

        if($request->role == 'admin'){

            $user = $this->userrepository->getWhere([['email', $request->email]])->first();

            $updated = $this->userrepository->update($data, $user->id);
        }
        else if($request->role == 'charity'){

            $user = $this->charityrepository->getWhere([['email', $request->email]])->first();

            $updated = $this->charityrepository->update($data, $user->id);
        }

        if($updated){

            session()->flash('success', 'تم تعيين كلمة المرور بنجاح');
        }
        else{

            session()->flash('error', 'حدث خطأ أثناء التعديل، برجاء المحاولة مرة أخرى');
        }

        // Redirect to login
        return redirect('/');
    }
}
