<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Repositories\FinancialRequestRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use Illuminate\Http\Request;

class ExchangeController extends Controller
{

    public $financialrepository;
    public $notificationRepository;

    public function __construct(FinancialRequestRepositoryInterface $financialrepository,
                                NotificationRepositoryInterface $notificationRepository)
    {

        $this->financialrepository = $financialrepository;
        $this->notificationRepository = $notificationRepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
        
    } // end of construct

    public function BondExchange()
    {

        if(!checkPermissions(['list_exchange_bonds_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // delete notifications
        $this->notificationRepository->deleteWhere([['title', 'acceptbondexchange']]);

        $financial_requests = $this->financialrepository->getWhereWith(['requestable', 'project', 'currency'], 
                [['requestable_id' , $this->user->id] , ['bank_account_num' , '!=' , null] , ['voucher_num' ,'!=' , null] , [
                    'status' , 'approved'] , ['type' , 'exchange'], ['recieve_status', 'paid']]);

        return view('charities.orders.bond-exchange.index' , compact('financial_requests'));

    }

    public function orderExchange()
    {

        $financial_requests = $this->financialrepository->getWhere([['requestable_id' , $this->user->user()->id] , ['bank_account_num' , null] , ['status' , 'approved'] , ['voucher_num' , null] , ['type' , 'exchange']]);

        return view('charities.orders.order-exchange.index' , compact('financial_requests'));

    }    

    public function acceptBond(Request $request)
    {
        
        if(!checkPermissions(['list_exchange_bonds_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $updated = $this->financialrepository->update(['recieve_status' => $request->recieve_status] , $request->id);

        if($updated){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }

    }


} // end of class
