<?php

namespace App\Http\Controllers\charity;

use App\Http\Controllers\Controller;
use App\Http\Requests\Charity\ProjectRequest;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Setting;
use App\Models\User;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ServiceOptionRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Servicies\Notify;
use App\Servicies\UploadFilesServices;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;
use Storage;
use Str;

use Monarobase\CountryList\CountryListFacade;

class ProjectController extends Controller
{

    private $projectrepository;
    private $cityrepository;
    private $fileservice;
    private $servicerepository;
    private $currencyrepository;
    private $phaserepository;
    private $imagerepository;
    private $editrequestrepository;
    private $logRepository;
    private $role;
    private $charityCategoryRepository;
    private $nation_id;
    private $userrepository;
    private $notificationRepository;
    
    
    public function __construct(PhaseRepositoryInterface $phaserepository , 
                                ServiceOptionRepositoryInterface $servicerepository , 
                                UploadFilesServices $fileservice , 
                                ProjectRepositoryInterface $projectrepository , 
                                CityRepositoryInterface $cityrepository , 
                                CurrencyRepositoryInterface $currencyrepository , 
                                ImageRepositoryInterface $imagerepository , 
                                EditRequestRepositoryInterface $editrequestrepository,
                                LogRepositoryInterface $logRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                UserRepositoryInterface $userrepository,
                                NotificationRepositoryInterface $notificationRepository)
    {

        $this->projectrepository     = $projectrepository;
        $this->cityrepository        = $cityrepository;
        $this->fileservice           = $fileservice;
        $this->servicerepository     = $servicerepository;
        $this->currencyrepository    = $currencyrepository;
        $this->phaserepository       = $phaserepository;
        $this->imagerepository       = $imagerepository;
        $this->editrequestrepository = $editrequestrepository;
        $this->userrepository        = $userrepository;
        $this->logRepository         = $logRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->notificationRepository = $notificationRepository;

        $this->nation_id = getAuthNationId();

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });

    } // end of construct
    
    public function index()
    {
        // عرض جميع المراحل بيبقى من داخل عرض المشروع علشان كدا لازم اعرض المشاريع مع صلاحية عرض المراحل
        if(!checkPermissions(['list_projects_'.$this->user->id]) && 
            !checkPermissions(['list_phases_'.$this->user->id]) && !checkPermissions(['edit_phases_'.$this->user->id]) &&
            !checkPermissions(['show_phases_'.$this->user->id]) && !checkPermissions(['delete_phases_'.$this->user->id]) &&

            // !checkPermissions(['list_sponsers_'.$this->user->id]) && !checkPermissions(['edit_sponsers_'.$this->user->id]) &&
            // !checkPermissions(['show_sponsers_'.$this->user->id]) && !checkPermissions(['delete_sponsers_'.$this->user->id]) &&

            !checkPermissions(['list_reports_'.$this->user->id]) && !checkPermissions(['create_reports_'.$this->user->id]) && 
            !checkPermissions(['edit_reports_'.$this->user->id]) && !checkPermissions(['show_reports_'.$this->user->id]) && 
            !checkPermissions(['delete_reports_'.$this->user->id])
            
            ){

            $this->notificationRepository->deleteWhere([['title', 'charity_new_project']]);
            $this->notificationRepository->deleteWhere([['title', 'project']]);
            $this->notificationRepository->deleteWhere([['title', 'charity_project_updated']]);
            $this->notificationRepository->deleteWhere([['title', 'edit_request_project']]);

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $projects = $this->projectrepository->paginateWhereWith([['charity_id', $this->user->id], ['profile', 0]], ['sponsers']);

        return view('charities.projects.index' , compact('projects'));

    } // end of index

    public function create()
    {
        if(!checkPermissions(['create_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $currencies = $this->currencyrepository->nation($this->user->nation_id);

        $categories = $this->user->categories;

        $countries = CountryListFacade::getList(\App::getLocale());

        $user = $this->user;

        return view('charities.projects.create' , compact('categories' , 'countries' , 'currencies', 'user'));

    } // end of create 
    
    public function store(ProjectRequest $request)
    {
        if(!checkPermissions(['create_projects_'.$this->user->id])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        // except some request
        $attribute = $request->except(['_token', '_method', 'eng_maps', 'images', 'attach', 'image']);

        if($request->service_option_id == 'other'){

            $attribute['service_option_id'] = null;
        }

        if($request->service_feature_id == 'other'){

            $attribute['service_feature_id'] = null;
        }
      
        // uploade file attach
        if ($request->hasFile('attach')) {

            $attribute['attach'] = $request->file('attach')->store('uploads/projects');

        } // end of if image

        // uploade file eng maps
        if ($request->hasFile('eng_maps')) {     

            $attribute['eng_maps'] = $request->file('eng_maps')->store('uploads/projects');

        } // end of if image 

        // generate project number
        // $attribute['project_num']  = rand();
        $last_project = $this->projectrepository->getWhere([['profile', 0]])->first();

        if($last_project){

            $attribute['project_num'] = $last_project->project_num +1;
        }
        else{
            $attribute['project_num'] = 1;
        }

        // nation id
        $attribute['nation_id']  = $this->user->nation_id;  
        // charity id
        $attribute['charity_id']  = $this->user->id; 

        // status
        $attribute['status']  = 'waiting'; 
        
        $attribute['slug'] = Str::random(12);
        
        if ($request->hasFile('image')) {

            $attribute['image'] = $request->file('image')->store('uploads/projects');

        } // end of if image

        $project = $this->projectrepository->create($attribute);

        Permission::create(['name' => 'edit_project_' . $project->id , 'guard_name' => 'web']);

        // uploade mulitple image 
        if ($request->hasFile('images')) {

            $files = $request->file('images');
            foreach ($request->images as $file) {

                $img = $file;
                $image = $this->fileservice->uploadfile($img, 'project');
                $project->images()->create(

                    ['imageable_id' => $project->id , 'path' => $image]

                ); // end of create

            } // end of foreach

        } // end of if


        // Create log data
        $log_data['user_id'] = auth()->user()->id;
        $log_data['user_type'] = 'charity';
        $log_data['model_id'] = $project->id;
        $log_data['model_type'] = 'project';
        $log_data['edit_type'] = 'Add project data';

        $this->logRepository->create($log_data);


        // notify admnis that have permissions to accept and refuse projects
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'تم اضافة مشروع جديد من قبل جمعية ';
        $message->message_en = 'Add New Project From As Charity';

        // GET ALL ADMINS THAT HAVE PERMISSION TO ACCEPT AND REFUSE PROJECTS
        $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_projects']]);

        // FOREACH ADMINS
        foreach($admins as $admin)    
        {

            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $admin->id,
                'model_id'       => $project->id,
                'title'          => 'new_project',
                'type'           => 'admin'
            ]);             
    
            
        } // end of foreach admins  
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);             

        if($project){

            $response['status'] = 1;
            $response['redirect'] = route('charity.projects.create');
            $response['reload'] = 0;
            return $response;
        }
        else{

            $response['status'] = 0;
            $response['reload'] = 0;
            return $response;
        }
        

    } // end of store
    
    public function show($project)
    {

        if(!checkPermissions(['show_projects_'.$this->user->id]) && 

            !checkPermissions(['list_phases_'.$this->user->id]) && !checkPermissions(['edit_phases_'.$this->user->id]) &&
            !checkPermissions(['show_phases_'.$this->user->id]) && !checkPermissions(['delete_phases_'.$this->user->id]) &&

            !checkPermissions(['list_sponsers_'.$this->user->id]) && !checkPermissions(['edit_sponsers_'.$this->user->id]) &&
            !checkPermissions(['show_sponsers_'.$this->user->id]) && !checkPermissions(['delete_sponsers_'.$this->user->id]) &&

            !checkPermissions(['list_reports_'.$this->user->id]) && !checkPermissions(['create_reports_'.$this->user->id]) && 
            !checkPermissions(['edit_reports_'.$this->user->id]) && !checkPermissions(['show_reports_'.$this->user->id]) && 
            !checkPermissions(['delete_reports_'.$this->user->id])

            ){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // delete notification
        $this->notificationRepository->deleteWhere([['title', 'project']]);
        $this->notificationRepository->deleteWhere([['title', 'edit_request_project']]);
        $this->notificationRepository->deleteWhere([['title', 'report']]);
        $this->notificationRepository->deleteWhere([['title', 'charity_new_project']]);
        $this->notificationRepository->deleteWhere([['title', 'charity_project_updated']]);

        $project = $this->projectrepository->getWhereWith(['service_option', 'service_feature', 'reports'], [['slug', $project], ['charity_id', $this->user->id]])->first();

        if(! isset($project)){
            
            return view('auth.not-found');
            
        }          

        $phase_unapproved = $this->phaserepository->unApproved($project->id);

        $edit_request = $this->editrequestrepository->getRequestEditProject($project->id);
      
        $dateNow = date('Y-m-d');  

        $session_currency = currencySymbol(session('currency'));

        return view('charities.projects.show' , compact('project' , 'phase_unapproved' , 'edit_request' , 'dateNow', 'session_currency'));

    } // end of show  
    
    public function edit($projec)
    {

        if(!checkPermissions(['edit_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $project = $this->projectrepository->getWhereWith(['images'], [['slug', $projec], ['charity_id', $this->user->id]])->first();

        if(! isset($project)){
            
            return view('auth.not-found');
            
        }  

        if ($project->status == 'waiting' || $this->user->can('edit_project_' . $project->id)) {

            $currencies = $this->currencyrepository->nation($this->user->nation_id);

            $categories = $this->user->categories;

            $countries = CountryListFacade::getList(\App::getLocale());

            $services = $this->servicerepository->getWhere([['parent_id', null], ['charity_category_id', $project->charity_category_id]]);
            
            if($project->service_feature_id != null){

                $feature = $this->servicerepository->findOne($project->service_feature_id);

                $features = $this->servicerepository->getWhere([['parent_id', $feature->parent_id]]);
            }
            else{

                $features = [];
            }

            $user = $this->user;

            return view('charities.projects.edit' , compact('user', 'currencies' , 'categories' , 'countries', 'project', 'services', 'features'));

        } else {

            return view('auth.not-authorized');            

        } // END OF ELSE IF

    } // end of edit 
    
    public function update(ProjectRequest $request, $id)
    {
         
        if(!checkPermissions(['edit_projects_'.$this->user->id])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $attribute = $request->except(['_token', '_method', 'eng_maps', 'images', 'attach', 'image']);

        $pro = $this->projectrepository->getWhere([['id', $id], ['charity_id', $this->user->id]])->first();

        if(! isset($pro)){
            
            return view('auth.not-found');
            
        }

        if ($pro->status != 'approved' || $this->user->can('edit_project_' . $id)) 
        {        

            if($request->service_option_id == 'other'){

                $attribute['service_option_id'] = null;
            }

            if($request->service_feature_id == 'other'){

                $attribute['service_feature_id'] = null;
            }
            

            // uploade file attach
            if ($request->hasFile('attach')) {

                Storage::delete($pro->attach);

                $attribute['attach'] = $request->file('attach')->store('uploads/projects');
 
            } 

            // uploade file eng maps
            if ($request->hasFile('eng_maps')) {

                Storage::delete($pro->eng_maps);

                $attribute['eng_maps'] = $request->file('eng_maps')->store('uploads/projects');
                
            }       
            
            // status
            $attribute['status']  = 'waiting';   
            
            if ($request->hasFile('image')) {

                Storage::delete($pro->image);

                $attribute['image'] = $request->file('image')->store('uploads/projects');
    
                
            } 

            $updated = $this->projectrepository->update($attribute , $id);

            $project = $this->projectrepository->findOne($id);

            // uploade mulitple image 
            if ($request->hasFile('images')) {              

                // Delete old images
                foreach($project->images as $image){
                    
                    $old_image = $project->images()->where([['imageable_id', $project->id], ['imageable_type', 'App\Models\Project']])->first();

                    if($old_image){

                        $old_image->delete();
                    }

                    Storage::delete($image->path);
                }
                
                // Upload new images
                $files = $request->file('images');
                foreach ($files as $file) {

                    $img = $file;
                    $image = $this->fileservice->uploadfile($img, 'project');
                    $project->images()->create(

                        ['imageable_id' => $project->id , 'path' => $image]

                    ); // end of create

                } // end of foreach

            } // end of if   
            
            $this->user->revokePermissionTo(['edit_project_' . $project->id]); // for revoke permission to user

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'charity';
            $log_data['model_id'] = $project->id;
            $log_data['model_type'] = 'project';
            $log_data['edit_type'] = 'Edit project data';

            $this->logRepository->create($log_data);
            
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تم تعديل المشروع من قبل جمعية ';
            $message->message_en = 'Update Project From As Charity';


            // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
            $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_projects']]);

            // FOREACH ADMINS
            foreach($admins as $admin)    
            {

                Notification::create([ 

                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $admin->id,
                    'model_id'       => $project->id,
                    'title'          => 'new_project',
                    'type'           => 'admin'
                ]);             
                
            } // end of foreach admins             
            
            // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
            Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);  

            if($updated){

                $response['status'] = 1;
                $response['redirect'] = route('charity.projects.edit', $project->slug);
                $response['reload'] = 0;
                return $response;
            }
            else{
    
                $response['status'] = 0;
                $response['reload'] = 0;
                return $response;
            }

        } else {

            return view('auth.not-authorized');            
    
        } // END OF ELSE IF            

    } // end of update

    public function destroy($project)
    {

        if(!checkPermissions(['delete_projects_'.$this->user->id])){

            return response()->json(['data' => 2], 200);
        }

        $project = $this->projectrepository->getWhereWith(['notifications'], [['id', $project], ['charity_id', $this->user->id]])->first();

        if(!$project){

            return response()->json(['data' => 2], 200);
        }

        try{

            $project->notifications()->delete();

            $deleted = $this->projectrepository->delete($project->id);
        }
        catch(Throwable $e){

            return response()->json(['data' => 3], 200);
        }


        if($deleted){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'charity';
            $log_data['model_id'] = $project->id;
            $log_data['model_type'] = 'project';
            $log_data['edit_type'] = 'Delete project data';

            $this->logRepository->create($log_data);

            // update project_num for all other projects
            // $this->projectrepository->updateProjectNumbers($project_num);

        }
        else{

            return response()->json(['data' => 3], 200);

        }

        return response()->json(['data' => 1], 200);

    }

    public function deleteImage(Request $request)
    {

        $this->imagerepository->delete($request->id);

        return response()->json(['data' => 1], 200);

    } // end of delete image 
    
    public function sendRequestEdit(Request $request)
    {
       
        if(!checkPermissions(['edit_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }
        
        $attribute = $request->except('_token' , 'nation_id' , 'status');

        // append nation id
        $attribute['nation_id'] = $this->user->nation_id;
        // append status
        $attribute['status'] = 'waiting';

        $attribute['model_id'] = $request->item;
        $attribute['type'] = 'project';
        $attribute['emp_id'] = auth()->user()->id; // اللى باعت الطلب ايا كان جمعبه او موظف جمعيه

        $project = $this->projectrepository->findOne($request->item);

        $this->editrequestrepository->create($attribute);

        
        // Create log data
        $log_data['user_id'] = auth()->user()->id;
        $log_data['user_type'] = 'charity';
        $log_data['model_id'] = $request->item;
        $log_data['model_type'] = 'project';
        $log_data['edit_type'] = 'Send project edit request';

        $this->logRepository->create($log_data);

        // notify admins that have permission to accept and refuse project edit requests
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'طلب تعديل مشروع';
        $message->message_en = 'Edit Request Project';


        // GET ALL ADMINS THAT HAVE PERMISSION TO ACCEPT AND REFUSE PROJECT EDIT REQUEST
        $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_project_edit_requests']]);

        // FOREACH ADMINS
        foreach($admins as $admin)    
        {

            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $admin->id,
                'model_id'       => $project->id,
                'title'          => 'edit_project',
                'type'           => 'admin'
            ]);             
    
            
        } // end of foreach admins             
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);  

        return response()->json([
            'success' => 1,
            'message' => trans('admin.send_request_success')
        ], 200);

    } // end of sendRequestEdit    

} // end of class
