<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Repositories\CityRepositoryInterface;
use Illuminate\Http\Request;

class CityController extends Controller
{
    
    public $cityrepository;
   
    public function __construct(CityRepositoryInterface $cityrepository)
    {

        $this->cityrepository    = $cityrepository;
        
    } // end of construct
    
    public function cityOfCountry(Request $request)
    {

        $nation_id = getNationId();

        $cities = $this->cityrepository->getWhere([['nation_id', $nation_id], ['parent_id', $request->id]]);

        return view('charities.projects.cities' , compact('cities'))->render();

    } // end of index    

}
