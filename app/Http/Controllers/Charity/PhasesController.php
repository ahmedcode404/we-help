<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Http\Requests\Charity\PhasesRequest;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\PhasesRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;
use Auth;
class PhasesController extends Controller
{
    public $projectrepository;
    public $phasesrepository;
    
    public function __construct(ProjectRepositoryInterface $projectrepository , PhaseRepositoryInterface $phasesrepository)
    {

        $this->projectrepository = $projectrepository;
        $this->phasesrepository = $phasesrepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
        
    } // end of construct
    
    public function create()
    {

        if(!checkPermissions(['create_phases_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // $projects = $this->projectrepository->getWhere([['charity_id', $this->user->id], ['profile', 0], ['status', '!=', 'approved']]);
        $projects = $this->projectrepository->getWhere([['charity_id', $this->user->id], ['profile', 0], ['active', 1]]);

        $orders = [__('admin.first') ,__('admin.second') ,__('admin.third') ,__('admin.fourth') ,__('admin.fifth') ,__('admin.sexth') ,__('admin.seventh') ,__('admin.eigth') ,__('admin.ninth') , __('admin.tenth') ];
        $order_en = [ 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 ];

        return view('charities.phases.create' , compact('projects' , 'orders' , 'order_en'));

    } // end of create 
    
    public function store(PhasesRequest $request)
    {

        if(!checkPermissions(['create_phases_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // except some request
        $attribute = $request->except(

            '_token' ,

        ); // end of except 

        $project = $this->projectrepository->findWith($request->project_id, ['phases', 'currency']);

        $phases_cost = generalExchange($project->phases->sum('cost'), $project->currency->symbol, currencySymbol(session('currency'))) + 
            generalExchange((float)$request->cost, $project->currency->symbol, currencySymbol(session('currency')));

        // get project actual cost without admin ratio            
        $project_cost = generalExchange($project->cost, $project->currency->symbol, currencySymbol(session('currency')));

        if($phases_cost > $project_cost){

            return response()->json([
                'status' => 0,
                'message' => trans('admin.phases_cost_exceeds_proejct_cost')
            ]);
        }

        $attribute['remaining_cost'] = $request->cost;
        $attribute['slug'] = \Str::random(12);

        $phase = $this->phasesrepository->create($attribute);

        Permission::create(['name' => 'edit_phase_' . $phase->id , 'guard_name' => 'web']);

        $this->projectrepository->update(['duration' => sumDayProject($phase->project_id)] , $phase->project_id);

        return response()->json([
            'status' => 1,
            'message' => '',
            'redirect' => route('charity.projects.show', $project->slug)
        ]);

    } // end of store

    public function show($phase)
    {

        if(!checkPermissions(['show_phases_'.$this->user->id]) &&
        
            !checkPermissions(['list_reports_'.$this->user->id]) && !checkPermissions(['create_reports_'.$this->user->id]) && 
            !checkPermissions(['edit_reports_'.$this->user->id]) && !checkPermissions(['show_reports_'.$this->user->id]) && 
            !checkPermissions(['delete_reports_'.$this->user->id])
            ){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $phase_one = $this->phasesrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id]], [['slug', $phase]])->first();

        if(! isset($phase_one)){
            
            return view('auth.not-found');
            
        }          

        return view('charities.phases.show' , compact('phase_one'));

    }

    public function edit($phas)
    {

        if(!checkPermissions(['edit_phases_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $phase = $this->phasesrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id], ['active', 1]], [['slug', $phas]])->first();

        if(!$phase){
            
            return view('auth.not-found');
            
        }          

        $phaseFirstProject = $this->phasesrepository->getFirstOne($phase->project_id);

        // dd($phaseFirstProject->id);

        if ($phase->approved == 0 || Auth::user()->can('edit_phases_' . $this->user->id)) 
        {        

            $projects = $this->projectrepository->getWhere([['charity_id', $this->user->id], ['profile', 0]]);

            $orders = [__('admin.first') ,__('admin.second') ,__('admin.third') ,__('admin.fourth') ,__('admin.fifth') ,__('admin.sexth') ,__('admin.seventh') ,__('admin.eigth') ,__('admin.ninth') , __('admin.tenth') ];
            $order_en = [ 1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9 , 10 ];


            return view('charities.phases.edit' , compact('phase' , 'projects' , 'order_en' , 'orders' , 'phaseFirstProject'));

        } else {

            return view('auth.not-authorized');            

        } // END OF ELSE IF

    } // end of edit      
    
    public function update(PhasesRequest $request)
    {

        if(!checkPermissions(['edit_phases_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $phase = $this->phasesrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id]], [['id', $request->item]])->first();

        if(!$phase){
            
            return view('auth.not-found');
            
        }

        $old_cost = $phase->cost;

        $this->phasesrepository->update(['cost' => 0, 'remaining_cost' => 0], $request->item);

        $project = $this->projectrepository->findWith($request->project_id, ['phases', 'currency']);

        $phases_cost = generalExchange($project->phases->sum('cost'), $project->currency->symbol, currencySymbol(session('currency'))) + 
            generalExchange((float)$request->cost, $project->currency->symbol, currencySymbol(session('currency')));
        
            // get project actual cost without admin ratio
        $project_cost = generalExchange($project->cost, $project->currency->symbol, currencySymbol(session('currency')));

        if($phases_cost > $project_cost){

            $this->phasesrepository->update(['cost' => $old_cost, 'remaining_cost' => $old_cost], $request->item);

            return response()->json([
                'status' => 0,
                'message' => trans('admin.phases_cost_exceeds_proejct_cost')
            ]);
        }

        if ($phase->approved == 0 || Auth::user()->can('edit_phases_' . $this->user->id)) 
        {           

            // except some request
            $attribute = $request->except(

                '_token'  ,
                '_method' ,
                'item' ,

            ); // end of except

            $attribute['remaining_cost'] = $request->cost;

            $phaseOne = $this->phasesrepository->updatePhase($attribute , $request->item);

            $this->projectrepository->update(['duration' => sumDayProject($phaseOne->project_id)] , $phaseOne->project_id);

            return response()->json([
                'status' => 1,
                'redirect' => route('charity.projects.show', $project->slug),
                'message' => ''
            ]);

        } else {

            return view('auth.not-authorized');            

        } // end of else if

    } // end of update 
    
    public function destroy($phase)
    {
        
        if(!checkPermissions(['delete_phases_'.$this->user->id])){

            return response()->json(['data' => 2], 200);
        }

        $phase = $this->phasesrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id]], [['id', $phase]])->first();

        if(!$phase){

            return response()->json(['data' => 2], 200);
        }

        $this->phasesrepository->delete($phase->id);

        return response()->json(['data' => 1], 200);

    } // destroy

    public function getStartDateProject(Request $request)
    {

        $phaseFirstProject = $this->phasesrepository->getFirst($request->id);

        $project = $this->projectrepository->findOne($request->id);

        return view('charities.phases.start-date-project' , compact('phaseFirstProject' , 'project'))->render();

    } // end of get create project

} // end of class



// if ($request->end_date > $phase->end_date) {
    
//     $old_s_date = Carbon::createFromFormat('Y-m-d', $phase->start_date);
//     $new_s_date = Carbon::createFromFormat('Y-m-d', $phase->end_date);

//     $diffr = $old_s_date->diffInDays($new_s_date);

//     $end_dates = $this->phasesrepository->getStartDate($request->item);

//     foreach ($end_dates as $end_date) {

//         $S_date = Carbon::createFromFormat('Y-m-d', $end_date->start_date);
//         $e_date = Carbon::createFromFormat('Y-m-d', $end_date->end_date);

//         $newStartDate = $S_date->addDays($diffr);
//         $newEndDate   = $e_date->addDays($diffr);

//         $formatStartDate =  date('Y-m-d' , strtotime($newStartDate));
//         $formatEndDate   =  date('Y-m-d' , strtotime($newEndDate));

//         $new_s_date_format = Carbon::createFromFormat('Y-m-d', $phase->end_date);
//         $new_e_date_format = Carbon::createFromFormat('Y-m-d', $request->end_date);                    

//         $diffr_duration = $new_s_date_format->diffInDays($new_e_date_format);

//         $end_date->update(['start_date' => $formatStartDate , 'end_date' => $formatEndDate , 'duration' => $diffr_duration]);

//     } // end of foreach

// }