<?php

namespace App\Http\Controllers\charity;

use App\Http\Controllers\Controller;
use App\Http\Requests\Charity\CharityRequest;
use App\Models\Notification;
use App\Models\User;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use App\Servicies\Notify;
use App\Servicies\UploadFilesServices;
use Illuminate\Http\Request;
use Auth;
use Spatie\Permission\Models\Permission;

use Monarobase\CountryList\CountryListFacade;

class CharityController extends Controller
{

    private $charityrepository;
    private $cityrepository;
    private $fileservice;
    private $userrepository;
    private $logRepository;
    
    public function __construct(CharityRepositoryInterface $charityrepository ,
                                CityRepositoryInterface $cityrepository , 
                                UploadFilesServices $fileservice,
                                UserRepositoryInterface $userrepository,
                                LogRepositoryInterface $logRepository)
    {

        $this->charityrepository     = $charityrepository;
        $this->cityrepository        = $cityrepository ;
        $this->fileservice           = $fileservice ;
        $this->userrepository        = $userrepository;
        $this->logRepository = $logRepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
        
    } // end of construct

    public function edit($charity)
    {

        $charity_one = $this->charityrepository->findOne($charity);

        if(! isset($charity_one)){
            
            return view('auth.not-found');
            
        }          

        if($charity_one->status == 'approved' && ! $this->user->can('edit_charity_' . $charity_one->id))
        {
            
            return view('auth.not-authorized');            
            
        }

        // $countries = $this->cityrepository->country();

        // $cities = $this->cityrepository->cityOfCountry($charity_one->country_id);

        $countries = CountryListFacade::getList(\App::getLocale());

        return view('charities.profile.edit' , compact('countries' , 'charity_one'));



    } // end of edit

    public function update(CharityRequest $request)
    {

        ////////////////////////////////////////////// basic info ////////////////////////////////////////////
        if ($request->type_form == 'basic-info') {

            // except some request
            $attribute = $request->except(

                '_token' ,
                '_method',
                'logo',
                'type_form',
                'phone_num',
                'mobile_num',

            ); // end of except   

            // uploade file attach
            if ($request->hasFile('logo')) {

                $img = $request->file('logo');

                $logo = $this->fileservice->uploadfile($img, 'charity');

                $attribute['logo'] = $logo;
                
            } // end of if image

            $attribute['status'] = 'hold';

            $this->charityrepository->update($attribute , $this->user->id);

             // Create log data
             $log_data['user_id'] = auth()->user()->id;
             $log_data['user_type'] = 'charity';
             $log_data['model_id'] = $this->user->id;
             $log_data['model_type'] = 'charity';
             $log_data['edit_type'] = 'Edit charity data';
 
             $this->logRepository->create($log_data);

            $this->user->revokePermissionTo(['edit_charity_' . $this->user->id]); // for revoke permission to user

        } 

        ////////////////////////////////////////////// official info ////////////////////////////////////////////

        else if($request->type_form == 'official-info')  // condition for form basic-info
        {
           
            // except some request
            $attribute = $request->except(

                '_token' ,
                '_method',
                'attach',
                'license_file',
                'year_report',
                'internal_image',
                'type_form'

            ); // end of except   

            // uploade file attach
            if ($request->hasFile('attach')) {

                $img = $request->file('attach');

                $attach = $this->fileservice->uploadfile($img, 'charity');

                $attribute['attach'] = $attach;
                
            } // end of if image 
            
            
            // uploade file license_file
            if ($request->hasFile('license_file')) {

                $img = $request->file('license_file');

                $license_file = $this->fileservice->uploadfile($img, 'charity');

                $attribute['license_file'] = $license_file;
                
            } // end of if image 
            

            // uploade file year_report
            if ($request->hasFile('year_report')) {

                $img = $request->file('year_report');

                $year_report = $this->fileservice->uploadfile($img, 'charity');

                $attribute['year_report'] = $year_report;
                
            } // end of if image
            

            // uploade file internal_image
            if ($request->hasFile('internal_image')) {

                $img = $request->file('internal_image');

                $internal_image = $this->fileservice->uploadfile($img, 'charity');

                $attribute['internal_image'] = $internal_image;
                
            } // end of if image 
            
            $attribute['status'] = 'hold';            

            $this->charityrepository->update($attribute , $this->user->id);  
            
            $this->user->revokePermissionTo(['edit_charity_' . $this->user->id]); // for revoke permission to user
            
            
        } 

        ////////////////////////////////////////////// representer form ////////////////////////////////////////////        
        else if($request->type_form == 'representer-form')
        {

            // except some request
            $attribute = $request->except(

                '_token' ,
                '_method',
                'type_form',
                'representer_passport_image',
                'representer_nation_image',
                'representer_title',

            ); // end of except   

            // uploade file representer_passport_image
            if ($request->hasFile('representer_passport_image')) {

                $img = $request->file('representer_passport_image');

                $representer_passport_image = $this->fileservice->uploadfile($img, 'charity');

                $attribute['representer_passport_image'] = $representer_passport_image;
                
            } // end of if image 
            
            
            // uploade file representer_nation_image
            if ($request->hasFile('representer_nation_image')) {

                $img = $request->file('representer_nation_image');

                $representer_nation_image = $this->fileservice->uploadfile($img, 'charity');

                $attribute['representer_nation_image'] = $representer_nation_image;
                
            } // end of if image 
            

            // uploade file representer_title
            if ($request->hasFile('representer_title')) {

                $img = $request->file('representer_title');

                $representer_title = $this->fileservice->uploadfile($img, 'charity');

                $attribute['representer_title'] = $representer_title;
                
            } // end of if image   
            
            $attribute['status'] = 'hold';
            

            $this->charityrepository->update($attribute , $this->user->id); 

            $this->user->revokePermissionTo(['edit_charity_' . $this->user->id]); // for revoke permission to user

        }  
               
        ////////////////////////////////////////////// bank data form ////////////////////////////////////////////        
        else if($request->type_form == 'bank-data-form')
        {

            // except some request
            $attribute = $request->except(

                '_token' ,
                '_method',
                'type_form',

            ); // end of except   

            $attribute['status'] = 'hold';
        
            $this->charityrepository->update($attribute , $this->user->id); 

            $this->user->revokePermissionTo(['edit_charity_' . $this->user->id]); // for revoke permission to user

        }  

        ////////////////////////////////////////////// bank data form ////////////////////////////////////////////        
        else if($request->type_form == 'socials-form')
        {

            // except some request
            $attribute = $request->except(

                '_token' ,
                '_method',
                'type_form',

            ); // end of except 
            
            $attribute['status'] = 'hold';            
        
            $this->charityrepository->update($attribute , $this->user->id); 

            $this->user->revokePermissionTo(['edit_charity_' . $this->user->id]); // for revoke permission to user


        } // END request tpye form // END request tpye form
        
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'تم التعديل من قبل الجمعية وبإنتظار الموافقة';
        $message->message_en = 'It has been modified by the charity and is awaiting approval';

        // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
        $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_charities']]);

        // FOREACH ADMINS
        foreach($admins as $admin)    
        {

            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $admin->id,
                'model_id'       => $this->user->id,
                'title'          => 'edit_charity',
                'type'           => 'admin'
            ]);             
        
        } // end of foreach admins             
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);  


    } // end of update

} // end of class
