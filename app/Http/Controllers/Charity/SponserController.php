<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Http\Requests\Charity\SponserRequest;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\SponserRepositoryInterface;
use App\Repositories\WorkFieldRepositoryInterface;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Auth;
class SponserController extends Controller
{
    public $projectrepository;
    public $sponserrepository;
    public $workfieldrepository;
    
    public function __construct(ProjectRepositoryInterface $projectrepository , SponserRepositoryInterface $sponserrepository , WorkFieldRepositoryInterface $workfieldrepository)
    {

        $this->projectrepository   = $projectrepository;
        $this->sponserrepository   = $sponserrepository;
        $this->workfieldrepository = $workfieldrepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
        
    } // end of construct
    
    public function create()
    {
        if(!checkPermissions(['create_sponsers_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $projects = $this->projectrepository->getWhere([['charity_id', $this->user->id], ['profile', 0], ['status', '!=', 'approved']]);

        $works    = $this->workfieldrepository->getAll();

        return view('charities.sponsers.create' , compact('projects' , 'works'));

    } // end of create 
    
    public function store(SponserRequest $request)
    {

        if(!checkPermissions(['create_sponsers_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // except some request
        $attribute = $request->except(

            '_token' ,
            'name_sponser_ar' ,
            'name_sponser_en' ,

        ); // end of except
        
        $attribute['name_ar'] = $request->name_sponser_ar;
        $attribute['name_en'] = $request->name_sponser_en;

        $sponser = $this->sponserrepository->create($attribute);

        return response()->json();

    } // end of store

    public function show($sponser)
    {

        if(!checkPermissions(['show_sponsers_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $sponser_one = $this->sponserrepository->findOne($sponser);

        if(! isset($sponser_one)){
            
            return view('auth.not-found');
            
        } 

        return view('charities.sponsers.show' , compact('sponser_one'));

    }

    public function edit($sponser)
    {

        if(!checkPermissions(['edit_sponsers_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $sponserOne = $this->sponserrepository->findOne($sponser);

        if(! isset($sponserOne)){
            
            return view('auth.not-found');
            
        } 

        $pro = $this->projectrepository->findOne($sponserOne->project_id);

        if ($pro->status == 'waiting' || Auth::user()->can('edit_project_' . $sponserOne->project_id)) 
        {        

            $projects = $this->projectrepository->getWhere([['charity_id', $this->user->id], ['profile', 0], ['status', '!=', 'approved']]);

            $works    = $this->workfieldrepository->getAll();

            return view('charities.sponsers.edit' , compact('sponserOne' , 'projects' , 'works'));

        } else {

            return view('auth.not-authorized');            

        } // END OF ELSE IF

    } // end of edit      
    
    public function update(SponserRequest $request)
    {
        if(!checkPermissions(['edit_sponsers_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $pro = $this->projectrepository->findOne($request->project_id);

        if ($pro->status == 'waiting' || Auth::user()->can('edit_project_' . $request->project_id)) 
        {           

            // except some request
            $attribute = $request->except(

                '_token'  ,
                '_method' ,
                'item' ,
                'name_sponser_ar' ,
                'name_sponser_en' ,                

            ); // end of except 

            $attribute['name_ar'] = $request->name_sponser_ar;
            $attribute['name_en'] = $request->name_sponser_en;
    
            $this->sponserrepository->update($attribute , $request->item);

            return response()->json();

        } else {

            return view('auth.not-authorized');            

        } // end of else if

    } // end of update 
    
    public function destroy($spons)
    {

        if(!checkPermissions(['delete_sponsers_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $this->sponserrepository->delete($spons);

        return response()->json(['data' => 1], 200);

    } // destroy

} // end of class
