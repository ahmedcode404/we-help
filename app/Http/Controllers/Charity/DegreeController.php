<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\DegreeRepositoryInterface;
use App\Repositories\UserRepositoryInterface;

class DegreeController extends Controller
{

    private $degreeRepository;
    private $userRepository;

    public function __construct(DegreeRepositoryInterface $degreeRepository,
                                UserRepositoryInterface $userRepository){

        $this->degreeRepository = $degreeRepository;
        $this->userRepository = $userRepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_degrees_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['degrees'] = $this->degreeRepository->paginateWhere([['nation_id', $nation_id], ['guard', 'charity'], ['guard_id', $this->user->id]]);

        return view('charities.degrees.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_degrees_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('charities.degrees.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!checkPermissions(['create_degrees_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $data['nation_id'] = $this->user->nation_id;
        $data['guard'] = 'charity';
        $data['guard_id'] = $this->user->id;
        $data['slug'] = \Str::random(12);
        
        $degree = $this->degreeRepository->create($data);

        if($degree){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_degrees_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['degree'] = $this->degreeRepository->getWhereWith(['charity_emp'], [['slug', $id], ['nation_id', $nation_id], 
                        ['guard', 'charity'], ['guard_id', $this->user->id]])->first();

        if(!$data['degree']){

            return view('auth.not-found');
            
        }

        return view('charities.degrees.show')->with([
            'data' => $data
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_degrees_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['degree'] = $this->degreeRepository->getWhere([['slug', $id], ['nation_id', $nation_id], ['guard', 'charity'], ['guard_id', $this->user->id]])->first();

        if(!$data['degree']){

            return view('auth.not-found');
            
        }

        return view('charities.degrees.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!checkPermissions(['edit_degrees_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $updated = $this->degreeRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_degrees_'.$this->user->id])){

            return response()->json(['data' => 2], 200);
        }

        $data['degree'] = $this->degreeRepository->getWhere([['id', $id], ['nation_id', $nation_id], 
                        ['guard', 'charity'], ['guard_id', $this->user->id]])->first();

        if(!$data['degree']){

            return response()->json(['data' => 2], 200);
            
        }

        // update related users to have degree_id = null
        $this->userRepository->updateWhere(['degree_id' => null], [['degree_id', $id]]);

        // delete degree
        $deleted = $this->degreeRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
