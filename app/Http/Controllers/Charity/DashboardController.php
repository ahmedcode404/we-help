<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ProjectUserRepositoryInterface;
use App\Repositories\SponserRepositoryInterface;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public $projectrepository;
    public $sponserrepository;
    public $phasesrepository;
    public $projectuserrepository;

    public function __construct(ProjectRepositoryInterface $projectrepository ,
                                SponserRepositoryInterface $sponserrepository,
                                PhaseRepositoryInterface $phasesrepository,
                                ProjectUserRepositoryInterface $projectuserrepository
                                )
    {
     
        $this->projectrepository     = $projectrepository;
        $this->sponserrepository     = $sponserrepository;
        $this->phasesrepository      = $phasesrepository;
        $this->projectuserrepository = $projectuserrepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
        
    }

    public function index()
    {
        // if(auth()->user()->hasRole('charity_employee')){
        //     $user = auth()->user()->charity;
        // }
        // else{

        //     $user = auth()->user();
        // }

        // dd(checkPermissions(['list_projects_'.$user->id]) || checkPermissions(['create_projects_'.$user->id]) || 
        // checkPermissions(['edit_projects_'.$user->id]) || checkPermissions(['show_projects_'.$user->id]) || 
        // checkPermissions(['delete_projects_'.$user->id]) ||
        // checkPermissions(['list_phases_'.$user->id]) || checkPermissions(['create_phases_'.$user->id]) || 
        // checkPermissions(['edit_phases_'.$user->id]) || checkPermissions(['show_phases_'.$user->id]) || 
        // checkPermissions(['delete_phases_'.$user->id]));

        $ids      = $this->user->projects()->pluck('id');
        $sponsers = $this->sponserrepository->count($ids);
        $phases   = $this->phasesrepository->count($ids);
        $cost_donate = $this->projectuserrepository->costAllProject($ids);

        // get status projects
        // $project_waiting  = $this->projectrepository->waiting();
        // $project_approved = $this->projectrepository->approved();
        // $project_rejected = $this->projectrepository->rejected();
        // $project_hold     = $this->projectrepository->hold();

        $project_waiting  = $this->projectrepository->getWhere([['status', 'waiting'], ['charity_id', $this->user->id]]);
        $project_approved = $this->projectrepository->getWhere([['status', 'approved'], ['charity_id', $this->user->id]]);
        $project_rejected = $this->projectrepository->getWhere([['status', 'rejected'], ['charity_id', $this->user->id]]);
        $project_hold     = $this->projectrepository->getWhere([['status', 'hold'], ['charity_id', $this->user->id]]);
        $user = $this->user;
        
        $projects = $user->projects()->where('profile', 0)->get();

        return view('charities.dashboard' , compact('sponsers' , 'cost_donate' , 'phases' , 'project_waiting' , 
            'project_approved' , 'project_hold' , 'project_rejected', 'user', 'projects'));

    } // end of class

} // end of class
