<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Http\Requests\Charity\ReportRequest;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use App\Servicies\UploadFilesServices;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;

class ReportController extends Controller
{

    public $reportrepository;
    public $phasesrepository;
    public $projectrepository;
    public $fileservice;
    public $imagerepository;
    
    public function __construct(ImageRepositoryInterface $imagerepository , UploadFilesServices $fileservice , ProjectRepositoryInterface $projectrepository , ReportRepositoryInterface $reportrepository , PhaseRepositoryInterface $phasesrepository)
    {

        $this->reportrepository  = $reportrepository;
        $this->phasesrepository  = $phasesrepository;
        $this->projectrepository = $projectrepository;
        $this->fileservice       = $fileservice;
        $this->imagerepository   = $imagerepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
        
    } // end of construct

    public function create($id, $type)
    {

        if(!checkPermissions(['create_reports_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }


        if($type == 'phase'){

            $phaseOne = $this->phasesrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id]], [['slug', $id]])->first();

            if(! isset($phaseOne)){
            
                return view('auth.not-found');
                
            }   
    
            $dateNow = date('Y-m-d');  
            
            if ($phaseOne->end_date <= $dateNow) 
            {
    
                            
                $project = $this->projectrepository->findOne($phaseOne->project_id);
        
                return view('charities.reports.create' , compact('project' , 'phaseOne', 'type'));
     
            } else 
            {
    
                return view('auth.not-authorized'); 
    
            }
        }

        else if($type == 'project'){

            $project = $this->projectrepository->getWhere([['charity_id', $this->user->id], ['slug', $id]])->first();

            if(! isset($project)){
            
                return view('auth.not-found');
                
            }   
            
            return view('charities.reports.create_project_report' , compact('project', 'type'));
        }

        

    } // end of create 

    public function store(ReportRequest $request)
    {
        if(!checkPermissions(['create_reports_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // except some request
        $attribute = $request->except(

            '_token',
            'project_id',
            'images',
            'vedio',

        ); // end of except 

        $project = $this->projectrepository->findOne($request->project_id);

        if($request->type == 'phase'){

            $phase = $this->phasesrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id]], [['id', $request->phase_id]])->first();

            if(! isset($phase)){
                
                return view('auth.not-found');
                
            } 
        }

        $attribute['project_id'] = $project->id;
        $attribute['phase_id']   = $request->type == 'phase' ? $phase->id : null;
        $attribute['charity_id'] = $project->charity_id;
        $attribute['status']     = 'waiting';
        $attribute['slug'] = \Str::random(12);

        // uploade file vedio
        if ($request->hasFile('vedio')) {     

            $img = $request->file('vedio');

            $vedio = $this->fileservice->uploadfile($img, 'report');

            $attribute['vedio'] = $vedio;

        } // end of if vedio         

        $report = $this->reportrepository->create($attribute);
        
        Permission::create(['name' => 'edit_report_' . $report->id , 'guard_name' => 'web']);

        // uploade mulitple image 
        if ($request->hasFile('images')) {

            $files = $request->file('images');
            foreach ($files as $file) {

                $img = $file;
                $image = $this->fileservice->uploadfile($img, 'project');
                $report->images()->create(

                    ['imageable_id' => $report->id , 'path' => $image]

                ); // end of create

            } // end of foreach

        } // end of if         
    
        return response()->json();

    } // end of store 

    public function show($report)
    {

        if(!checkPermissions(['show_reports_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $report_one = $this->reportrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id]], [['slug', $report]])->first();

        if(! isset($report_one)){
            
            return view('auth.not-found');
            
        }         

        return view('charities.reports.show' , compact('report_one'));
        
    } // end of show
    
    public function edit($report)
    {

        if(!checkPermissions(['edit_reports_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $report_one = $this->reportrepository->whereHasWith(['project'], 'project', [['charity_id', $this->user->id]], [['slug', $report]])->first();

        if(! isset($report_one)){
            
            return view('auth.not-found');
            
        }  

        if ($report_one->status == 0 || Auth::user()->can('edit_project_' . $report_one->project_id)) 
        {        

            $phaseOne = $this->phasesrepository->findOne($report_one->phase_id);

            $project = $this->projectrepository->findOne($report_one->project_id);


            return view('charities.reports.edit' , compact('report_one' , 'project' , 'phaseOne'));

        } else  
        {

            return view('auth.not-authorized');            

        } // en of else if

    } // end of edit
    
    public function update(ReportRequest $request)
    {

        if(!checkPermissions(['edit_reports_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // $report_one = $this->reportrepository->findOne($request->report_id);

        $report_one = $this->reportrepository->whereHasWith(['phase.project'], 'phase.project', [['charity_id', $this->user->id]], [['id', $request->report_id]])->first();

        if(! isset($report_one)){
            
            return view('auth.not-found');
            
        }


        if ($report_one->status == 0 || Auth::user()->can('edit_project_' . $report_one->project_id)) 
        {        


            // except some request
            $attribute = $request->except(

                '_token' ,
                '_method',
                'project_id' ,
                'images' ,
                'report_id',
                'vedio'

            ); // end of except 

            $project = $this->projectrepository->findOne($request->project_id);
            $phase = $this->phasesrepository->findOne($request->phase_id);

            $attribute['project_id'] = $project->id;
            $attribute['phase_id']   = $phase->id;
            $attribute['charity_id'] = $project->charity_id;
            $attribute['status']     = 'waiting';

            // uploade file vedio
            if ($request->hasFile('vedio')) {     

                $img = $request->file('vedio');

                $vedio = $this->fileservice->uploadfile($img, 'report');

                $attribute['vedio'] = $vedio;

            } // end of if vedio                   
            // dd($attribute);
            $report = $this->reportrepository->updateReport($attribute , $request->report_id);

            // uploade mulitple image 
            if ($request->hasFile('images')) {

        

                $files = $request->file('images');
                foreach ($files as $file) {

                    $img = $file;
                    $image = $this->fileservice->uploadfile($img, 'project');
                    $report->images()->create(

                        ['imageable_id' => $report->id , 'path' => $image]

                    ); // end of create

                } // end of foreach

            } // end of if         
        
            return response()->json();

        } else 
        {

            return view('auth.not-authorized');            
            
        } // end of else if

    } // end of update     
    
    public function destroy($report)
    {

        if(!checkPermissions(['delete_reports_'.$this->user->id])){

            return response()->json(['data' => 2], 200);
        }

        $report_one = $this->reportrepository->whereHasWith(['phase.project'], 'phase.project', [['charity_id', $this->user->id]], [['id', $report]])->first();

        if(! isset($report_one)){
            
            return response()->json(['data' => 2], 200);
            
        }

        $this->reportrepository->delete($report);

        return response()->json(['data' => 1], 200);

    } // destroy

    public function deleteImage(Request $request)
    {

        $this->imagerepository->delete($request->id);

        return response()->json(['data' => 1], 200);

    } // end of delete image
    
} // end of class
