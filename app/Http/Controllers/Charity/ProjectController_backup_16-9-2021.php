<?php

namespace App\Http\Controllers\charity;

use App\Http\Controllers\Controller;
use App\Http\Requests\Charity\ProjectRequest;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Setting;
use App\Models\User;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ServiceRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use App\Servicies\Notify;
use App\Servicies\UploadFilesServices;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;
use Carbon\Carbon;

use Monarobase\CountryList\CountryListFacade;

class ProjectController extends Controller
{

    private $projectrepository;
    private $cityrepository;
    private $fileservice;
    private $servicerepository;
    private $currencyrepository;
    private $phaserepository;
    private $imagerepository;
    private $editrequestrepository;
    private $logRepository;
    private $role;
    
    
    public function __construct(PhaseRepositoryInterface $phaserepository , 
                                ServiceRepositoryInterface $servicerepository , 
                                UploadFilesServices $fileservice , 
                                ProjectRepositoryInterface $projectrepository , 
                                CityRepositoryInterface $cityrepository , 
                                CurrencyRepositoryInterface $currencyrepository , 
                                ImageRepositoryInterface $imagerepository , 
                                EditRequestRepositoryInterface $editrequestrepository,
                                LogRepositoryInterface $logRepository,
                                UserRepositoryInterface $userrepository)
    {

        $this->projectrepository     = $projectrepository;
        $this->cityrepository        = $cityrepository;
        $this->fileservice           = $fileservice;
        $this->servicerepository     = $servicerepository;
        $this->currencyrepository    = $currencyrepository;
        $this->phaserepository       = $phaserepository;
        $this->imagerepository       = $imagerepository;
        $this->editrequestrepository = $editrequestrepository;
        $this->userrepository        = $userrepository;
        $this->logRepository         = $logRepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });

    } // end of construct
    
    public function index()
    {
        
        if(!checkPermissions(['list_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $projects = $this->projectrepository->getWhereWith(['sponsers'], [['charity_id', $this->user->id], ['profile', 0]]);

        return view('charities.projects.index' , compact('projects'));

    } // end of index

    public function create()
    {
        if(!checkPermissions(['create_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $currencies = $this->currencyrepository->nation($this->user->nation_id);

        $names = ['health' , 'medical' , 'drilling' , 'sponsoring'];

        $countries = CountryListFacade::getList(\App::getLocale());

        $user = $this->user;

        return view('charities.projects.create' , compact('names' , 'countries' , 'currencies', 'user'));

    } // end of create 
    
    public function store(ProjectRequest $request)
    {
        if(!checkPermissions(['create_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // except some request
        $attribute = $request->except(

            '_token' ,
            '_method' ,
            'project_num' ,
            'attach' ,
            'eng_maps' ,
            'images' ,
            'nation_id' ,
            'edu_service_id' ,
            'edu_service_other' ,
            'emerg_service_id' ,
            'emerg_service_other' ,
            'care_type' ,
            'status',
            'image',
            'charity_id',
            'type',

        ); // end of except

        if($request->edu_service_id > 0 && $request->edu_service_id != 'undefined')
        {

            // edu service id
            $attribute['edu_service_id']  = $request->edu_service_id;

        } 
        if($request->edu_service_other != NULL && $request->edu_service_other != 'undefined')
        {

            // edu service other
            $attribute['edu_service_other']  = $request->edu_service_other;

        } 
        if($request->emerg_service_id > 0 && $request->emerg_service_id != 'undefined')
        {

            // emerg service id
            $attribute['emerg_service_id']  = $request->emerg_service_id;

        } 
        if($request->emerg_service_other != NULL && $request->emerg_service_other != 'undefined')
        {

            // emerg service other
            $attribute['emerg_service_other']  = $request->emerg_service_other;  

        } 
        if($request->care_type != "null")
        {

            // care_type
            $attribute['care_type']  = $request->care_type; 

        } 

      
        // uploade file attach
        if ($request->hasFile('attach')) {
        
			$img = $request->file('attach');

			$attach = $this->fileservice->uploadfile($img, 'project');

            $attribute['attach'] = $attach;

        } // end of if image

        // uploade file eng maps
        if ($request->hasFile('eng_maps')) {     

			$img = $request->file('eng_maps');

			$eng_maps = $this->fileservice->uploadfile($img, 'project');

            $attribute['eng_maps'] = $eng_maps;

        } // end of if image 

        // generate project number
        $attribute['project_num']  = rand();

        // nation id
        $attribute['nation_id']  = $this->user->nation_id;  
        // charity id
        $attribute['charity_id']  = $this->user->id; 

        // status
        $attribute['status']  = 'waiting';  
        
        
        if ($request->hasFile('image')) {

			$img = $request->file('image');
			$image = $this->fileservice->uploadfile($img, 'project');

            $attribute['image'] = $image;

        } // end of if image

        $project = $this->projectrepository->create($attribute);

        Permission::create(['name' => 'edit_project_' . $project->id , 'guard_name' => 'web']);


        // uploade mulitple image 
        if ($request->hasFile('images')) {

            $files = $request->file('images');
            foreach ($files as $file) {

                $img = $file;
                $image = $this->fileservice->uploadfile($img, 'project');
                $project->images()->create(

                    ['imageable_id' => $project->id , 'path' => $image]

                ); // end of create

            } // end of foreach

        } // end of if


        // Create log data
        $log_data['user_id'] = auth()->user()->id;
        $log_data['user_type'] = 'charity';
        $log_data['model_id'] = $project->id;
        $log_data['model_type'] = 'project';
        $log_data['edit_type'] = 'Add project data';

        $this->logRepository->create($log_data);


        // notify admnis that have permissions to accept and refuse projects
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'تم اضافة مشروع جديد من قبل جمعية ';
        $message->message_en = 'Add New Project From As Charity';

        // GET ALL ADMINS THAT HAVE PERMISSION TO ACCEPT AND REFUSE PROJECTS
        $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_projects']]);

        // FOREACH ADMINS
        foreach($admins as $admin)    
        {

            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $admin->id,
                'model_id'       => $project->id,
                'title'          => 'new_project',
                'type'           => 'admin'
            ]);             
    
            
        } // end of foreach admins  
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);             

        return response()->json();

    } // end of store
    
    public function show($project)
    {

        if(!checkPermissions(['show_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $project = $this->projectrepository->findOne($project);

        if(! isset($project)){
            
            return view('auth.not-found');
            
        }          

        $phase_unapproved = $this->phaserepository->unApproved($project->id);

        $edit_request = $this->editrequestrepository->getRequestEditProject($project->id);
      
        $dateNow = date('Y-m-d');  

        return view('charities.projects.show' , compact('project' , 'phase_unapproved' , 'edit_request' , 'dateNow'));

    } // end of show  
    
    public function edit($projec)
    {

        if(!checkPermissions(['edit_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $project = $this->projectrepository->findOne($projec);

        if(! isset($project)){
            
            return view('auth.not-found');
            
        }          

        if ($project->status == 'waiting' || $this->user->can('edit_project_' . $projec)) {
            // خدمات المراحل التعليمية
            $edu_services = $this->servicerepository->edu();
            $emerg_services = $this->servicerepository->emerg();

            // خدمات ده التكافل
            $care_durations = [__('admin.annual'), __('admin.cut_off')];
            $care_durations_en = ['annual','cut_off'];

            // المراحل التعليمية
            $stages = [__('admin.primary'), __('admin.secondary'), __('admin.intermediate')];
            $stages_en = ['primary', 'secondary', 'intermediate'];

            // انواع رعايه اليتيم
            $care_types = [__('admin.clothing'), __('admin.warranty') , __('admin.feast')];
            $care_types_en = ['clothing', 'warranty' , 'feast'];


            // اسماء المشاريع
            $names = ['health' , 'medical' , 'drilling' , 'sponsoring'];

            // اقسام المشاريع المستدامة
            $stable_projects = [__('admin.debt'), __('admin.loan') , __('admin.donation')];
            $stable_project_en = ['debt', 'loan' ,'donation'];

            $currencies = $this->currencyrepository->nation(getNationId());
            
            $countries = CountryListFacade::getList(\App::getLocale());

            $user = $this->user;

            return view('charities.projects.edit' , compact('user', 'currencies' , 'stable_projects' , 'stable_project_en' , 'care_durations_en' , 'stages_en', 'care_types_en', 'names' , 'countries' , 'project' , 'care_types' , 'stages' , 'edu_services' , 'care_durations' , 'emerg_services'));

        } else {

            return view('auth.not-authorized');            

        } // END OF ELSE IF

    } // end of edit 
    
    public function update(ProjectRequest $request)
    {
         
        if(!checkPermissions(['edit_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $pro = $this->projectrepository->findOne($request->item);

        if ($pro->status != 'approved' || $this->user->can('edit_project_' . $request->item)) 
        {        

            // except some request
            $attribute = $request->except(

                '_token' ,
                '_method' ,
                'item' ,
                'project_num' ,
                'type',
                'attach' ,
                'eng_maps' ,
                'images' ,
                'nation_id' ,
                'edu_service_id' ,
                'edu_service_other',
                'emerg_service_id',
                'emerg_service_other',
                'care_type',
                'status',
                'charity_category_id',
                'care_duration',
                'stable_project',
                'stage',
                'image',
                'charity_id',

            ); // end of except
            
            
            if($request->care_type != "null")
            {

                // care_type
                $attribute['care_type']  = $request->care_type; 

            } 
            
            $attribute['stage']  = $request->stage;

            // uploade file attach
            if ($request->hasFile('attach')) {

                $img = $request->file('attach');

                $attach = $this->fileservice->uploadfile($img, 'project');

                
            } // end of if image
            
            $attribute['attach'] = isset($attach) ? $attach : $pro->attach;

            // uploade file eng maps
            if ($request->hasFile('eng_maps')) {

                $img = $request->file('eng_maps');

                $eng_maps = $this->fileservice->uploadfile($img, 'project');

                
            } // end of if image 


            $attribute['care_duration']  = $request->care_duration;
    
            if($request->edu_service_id != 'undefined' && $request->edu_service_id != 'other')
            {

                $attribute['edu_service_id']   = $request->edu_service_id;

            } else  {

                $attribute['edu_service_id']   = NULL;
                $attribute['edu_service_other']   = $request->edu_service_other;

            }
            
            if ($request->type == 'emergency') 
            {
            
                $attribute['eng_maps']            = NULL;
                $attribute['edu_service_id']      = NULL;

                if($request->emerg_service_id != 'other')
                {

                    $attribute['emerg_service_id']    = $request->emerg_service_id;
                    $attribute['emerg_service_other'] = NULL;

                } else {

                    $attribute['emerg_service_other'] = $request->emerg_service_other;
                    $attribute['emerg_service_id']    = NULL;

                }

                $attribute['stable_project']      = NULL;
                $attribute['edu_service_other']   = NULL;
                $attribute['stage']               = NULL;
                $attribute['care_duration']       = NULL;
                $attribute['care_type']           = NULL;

            }
            
            if ($request->charity_category_id == 1 || $request->charity_category_id == 2) 
            {
            
                $attribute['eng_maps']            = NULL;
                $attribute['care_type']           = NULL;
                $attribute['care_duration']       = NULL;
                $attribute['edu_service_id']      = NULL;
                $attribute['emerg_service_id']    = NULL;
                $attribute['stable_project']      = NULL;
                $attribute['emerg_service_other'] = NULL;
                $attribute['edu_service_other']   = NULL;
                $attribute['stage']               = NULL;
        
            }

            if ($request->type == 'sustainable') 
            {

                $attribute['stable_project']      = $request->stable_project;
                $attribute['eng_maps']            = NULL;
                $attribute['care_type']           = NULL;
                $attribute['care_duration']       = NULL;
                $attribute['edu_service_id']      = NULL;
                $attribute['emerg_service_id']    = NULL;
                $attribute['emerg_service_other'] = NULL;
                $attribute['edu_service_other']   = NULL;
                $attribute['stage']               = NULL;
        
            }        

            if ($request->type == 'sponsorship') 
            {
            
                $attribute['eng_maps']            = NULL;
                $attribute['edu_service_id']      = NULL;
                $attribute['emerg_service_id']    = NULL;
                $attribute['stable_project']      = NULL;
                $attribute['emerg_service_other'] = NULL;
                $attribute['edu_service_other']   = NULL;
                $attribute['stage']               = NULL;
        
            }
            
            if ($request->type == 'educational') 
            {
            
                $attribute['eng_maps']            = NULL;
                $attribute['emerg_service_id']    = NULL;

                if($request->edu_service_id != 'other')
                {

                    $attribute['edu_service_id']    = $request->edu_service_id;
                    $attribute['edu_service_other'] = NULL;

                } else {

                    $attribute['edu_service_other'] = $request->edu_service_other;
                    $attribute['edu_service_id']    = NULL;

                }  

                $attribute['stage']               = $request->stage;
                $attribute['stable_project']      = NULL;
                $attribute['emerg_service_other'] = NULL;
                $attribute['care_duration']       = NULL;
                $attribute['care_type']           = NULL;

            }   
            
            if ($request->type == 'engineering') {

                $attribute['edu_service_id']      = NULL;
                $attribute['edu_service_other']   = NULL;
                $attribute['eng_maps'] = isset($eng_maps) ? $eng_maps : $pro->eng_maps;
                $attribute['emerg_service_id']    = NULL;
                $attribute['stable_project']      = NULL;
                $attribute['emerg_service_other'] = NULL;
                $attribute['care_duration']       = NULL;
                $attribute['stage']               = NULL;
                $attribute['care_type']           = NULL;

            } else {

                $attribute['eng_maps'] = NULL;

            }        


            // generate project number
            $attribute['project_num']  = $pro->project_num;

            // nation id
            $attribute['nation_id']  = $this->user->nation_id; 

            $attribute['charity_id']  = $this->user->id; 

            $attribute['charity_category_id']  = $request->charity_category_id;
            
            // status
            $attribute['status']  = 'waiting';   
            
            if ($request->hasFile('image')) {

                $img = $request->file('image');
                $image = $this->fileservice->uploadfile($img, 'project');
    
                
            } // end of if image            
            
            $attribute['image'] = isset($image) ? $image : $pro->image;

            $project = $this->projectrepository->updateProject($attribute , $request->item);

            // uploade mulitple image 
            if ($request->hasFile('images')) {              

                $files = $request->file('images');
                foreach ($files as $file) {

                    $img = $file;
                    $image = $this->fileservice->uploadfile($img, 'project');
                    $project->images()->create(

                        ['imageable_id' => $project->id , 'path' => $image]

                    ); // end of create

                } // end of foreach

            } // end of if   
            
            $this->user->revokePermissionTo(['edit_project_' . $project->id]); // for revoke permission to user

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'charity';
            $log_data['model_id'] = $project->id;
            $log_data['model_type'] = 'project';
            $log_data['edit_type'] = 'Edit project data';

            $this->logRepository->create($log_data);
            
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تم تعديل المشروع من قبل جمعية ';
            $message->message_en = 'Update Project From As Charity';


            // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
            $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_projects']]);

            // FOREACH ADMINS
            foreach($admins as $admin)    
            {

                Notification::create([ 

                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $admin->id,
                    'model_id'       => $project->id,
                    'title'          => 'new_project',
                    'type'           => 'admin'
                ]);             
                
            } // end of foreach admins             
            
            // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
            Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);  

            return response()->json();

        } else {

            return view('auth.not-authorized');            
    
        } // END OF ELSE IF            

    } // end of update

    public function destroy($project)
    {

        if(!checkPermissions(['delete_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $project = $this->projectrepository->findOne($project);

        // Create log data
        $log_data['user_id'] = auth()->user()->id;
        $log_data['user_type'] = 'charity';
        $log_data['model_id'] = $project->id;
        $log_data['model_type'] = 'project';
        $log_data['edit_type'] = 'Delete project data';

        $this->logRepository->create($log_data);

        $this->projectrepository->deleteProject($project);

        return response()->json(['data' => 1], 200);

    }

    public function deleteImage(Request $request)
    {

        $this->imagerepository->delete($request->id);

        return response()->json(['data' => 1], 200);

    } // end of delete image 
    
    public function sendRequestEdit(Request $request)
    {
       
        if(!checkPermissions(['edit_projects_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }
        
        $attribute = $request->except('_token' , 'nation_id' , 'status');

        // append nation id
        $attribute['nation_id'] = $this->user->nation_id;
        // append status
        $attribute['status'] = 'waiting';

        $attribute['model_id'] = $request->item;
        $attribute['type'] = 'project';
        $attribute['emp_id'] = auth()->user()->id; // اللى باعت الطلب ايا كان جمعبه او موظف جمعيه

        $project = $this->projectrepository->findOne($request->item);


        $this->editrequestrepository->create($attribute);

        // notify admins that have permission to accept and refuse project edit requests
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'طلب تعديل مشروع';
        $message->message_en = 'Edit Request Project';


        // GET ALL ADMINS THAT HAVE PERMISSION TO ACCEPT AND REFUSE PROJECT EDIT REQUEST
        $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_project_edit_requests']]);

        // FOREACH ADMINS
        foreach($admins as $admin)    
        {

            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $admin->id,
                'model_id'       => $project->id,
                'title'          => 'edit_project',
                'type'           => 'admin'
            ]);             
    
            
        } // end of foreach admins             
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);  

        return response()->json();

    } // end of sendRequestEdit    

} // end of class
