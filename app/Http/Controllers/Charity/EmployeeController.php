<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EmployeeRequest;
use App\Models\Setting;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\DegreeRepositoryInterface;
use App\Repositories\JobCategoryRepositoryInterface;
use App\Repositories\JobRepositoryInterface;
use App\Repositories\PermissionRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\NationalityRepositoryInterface;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

use Monarobase\CountryList\CountryListFacade;

class EmployeeController extends Controller
{

    private $charityrepository;
    private $cityrepository;
    private $permissionrepository;
    private $degreerepository;
    private $jopcategoyrepository;
    private $jobrepository;
    private $currencyrepository;
    private $nationalityRepository;
    
    public function __construct(CharityRepositoryInterface $charityrepository ,
                                CityRepositoryInterface $cityrepository ,
                                PermissionRepositoryInterface $permissionrepository ,
                                JobCategoryRepositoryInterface $jopcategoyrepository,
                                JobRepositoryInterface $jobrepository,
                                DegreeRepositoryInterface $degreerepository,
                                CurrencyRepositoryInterface $currencyrepository,
                                NationalityRepositoryInterface $nationalityRepository
                                )
    {
        
        $this->charityrepository    = $charityrepository;
        $this->cityrepository       = $cityrepository;
        $this->permissionrepository = $permissionrepository;
        $this->degreerepository     = $degreerepository;
        $this->jopcategoyrepository = $jopcategoyrepository;
        $this->jobrepository        = $jobrepository;
        $this->currencyrepository   = $currencyrepository;
        $this->nationalityRepository = $nationalityRepository;   
        
        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });

    } // end of construct

    public function index()
    {
        if(!checkPermissions(['list_employees_'. $this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $employees = $this->charityrepository->paginateWhereHas('roles', [['name', 'charity_employee']], 10,  
                    [['parent_id',  $this->user->id]], ['column' => 'id', 'dir' => 'DESC']);

        return view('charities.employees.index' , compact('employees'));

    } // end of index

    public function create()
    {
        if(!checkPermissions(['create_employees_'. $this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        // $roles = $this->permissionrepository->getCharityRoles();

        $roles = $this->user->roles()->where([['name', '!=', 'charity'], ['name', '!=', 'charity_employee']])->get();

        $jop_categories = $this->jopcategoyrepository->getWhere([['nation_id', $nation_id], ['guard', 'charity'], ['guard_id', $this->user->id]]);

        $countries = CountryListFacade::getList(\App::getLocale());

        $degrees = $this->degreerepository->getWhere([['nation_id', $nation_id], ['guard',  'charity'], ['guard_id', $this->user->id]]);
        
        $nationalities = $this->nationalityRepository->getWhere([['nation_id', $nation_id], ['guard', 'charity'], ['guard_id', $this->user->id]]);

        return view('charities.employees.create' , compact('countries' , 'roles' , 'degrees' , 'jop_categories', 'nationalities'));

    } // end of create

    public function store(EmployeeRequest $request)
    {
        if(!checkPermissions(['create_employees_'. $this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $attribute   = $request->except(
            '_token' ,
             'permission' ,
            'password' ,
            'confirm_password',
            'blocked',
            'phone_num',
            'jop_category_id',
        );

        $nation_id = getAuthNationId();

        $permissions = Permission::find($request->permission);


        $attribute['name_ar'] = $request->name_employee_ar;
        $attribute['name_en'] = $request->name_employee_en;
        $attribute['phone'] = $request->phone;
        $attribute['nation_id'] = $nation_id;
        $attribute['parent_id'] = $this->user->id;
        $attribute['slug'] = \Str::random(12);

        $currency = $this->currencyrepository->getWhere([['symbol', 'GBP'], ['nation_id', $nation_id]])->first();

        // $attribute['currency_id'] = $currency ? $currency->id : auth()->guard('charity')->user()->currency_id;

        $attribute['currency_id'] = $currency ? $currency->id : session('currency');

        $pass_rand = Str::random(12);

        $attribute['password'] = bcrypt($pass_rand);

        $employees = $this->charityrepository->create($attribute);

        session()->forget('emp_phone');

        $employees->syncPermissions($permissions);
        $employees->assignRole('charity_employee');

        // send data Login to this email

        $setting = Setting::DataLogin();
        $slogo = Setting::Slogan();

        $msg = '';
        $email = $request->email;
        $topic = $slogo->value;
        $nation_id = $nation_id;
        $dataLogin = $setting->value . '<br><b>' .
         __('admin.email') . ' </b> ' . ' : ' . $request->email . '<br>' .
          '<b>' . __('admin.password') . ' </b> ' . ' : ' . $pass_rand;
        
        $msg = getSettingMsg('msg_data_login', $nation_id)->value;
        
        try {
            
            sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $dataLogin);
        }
        catch(Throwable $e){

            session()->flash('error', trans('admin.unable_to_send_email'));

            return redirect()->back();
        }

        // return redirect()->route('charity.employees.index')->with('success' , __('admin.add_employee_succcess'));

        $response['status'] = 1;
        $response['redirect'] = route('charity.employees.create');
        $response['reload'] = 0;

        return $response;

    } // end of store

    public function edit($employee)
    {
        if(!checkPermissions(['edit_employees_'. $this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $employee  = $this->charityrepository->getWhereWith(['job', 'nationality'], 
                    [['slug', $employee], ['nation_id', $nation_id], ['parent_id',  $this->user->id]])->first();

        if(!$employee){

            return view('auth.not-found');
            
        } 

        $countries = CountryListFacade::getList(\App::getLocale());

        $degrees = $this->degreerepository->getWhere([['nation_id', $nation_id]]);
        // $roles = $this->permissionrepository->getCharityRoles();
        $roles = $this->user->roles()->where([['name', '!=', 'charity'], ['name', '!=', 'charity_employee']])->get();
        $jop_categories = $this->jopcategoyrepository->getWhere([['nation_id', $nation_id], ['guard', 'charity']]);
        
        if($employee->job){

            $jops = $this->jobrepository->getWhere([['job_category_id', $employee->job->job_category_id]]);
        }
        else{
            $jops = $this->jobrepository->getAll();
        }

        $nationalities = $this->nationalityRepository->getWhere([['nation_id', $nation_id], ['guard', 'charity']]);

        return view('charities.employees.edit' , compact('employee' , 'jops' , 'jop_categories' , 'countries' , 'roles', 'degrees', 'nationalities'));

        
    } // end of edit 

    public function show($employee)
    {

        if(!checkPermissions(['show_employees_'. $this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $employee = $this->charityrepository->getWhereWith(['job', 'nationality'], 
                    [['slug', $employee], ['nation_id', $nation_id], ['parent_id', $this->user->id]])->first();

        if(!$employee){

            return view('auth.not-found');
            
        } 

        return view('charities.employees.show' , compact('employee'));
        
    } // end of show     
     
    public function update(EmployeeRequest $request , $emp)
    {
        if(!checkPermissions(['edit_employees_'. $this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();
        
        $employee  = $this->charityrepository->getWhere([['id', $emp], ['nation_id', $nation_id], 
                    ['parent_id', $this->user->id]])->first();

        if(!$employee){

            return view('auth.not-found');
            
        } 

        $attribute  = $request->except(
            '_token' ,
            '_method',
            'permission' ,
            'password' ,
            'confirm_password',
            'blocked',
            'email',
            'item',
            'name_employee_ar',
            'name_employee_en',
            'phone_num',
            'jop_id',
            'jop_category_id',

        );

        $permissions = Permission::find($request->permission);


        $attribute['name_ar'] = $request->name_employee_ar;
        $attribute['name_en'] = $request->name_employee_en;
        $attribute['email'] = $employee->email;
        $attribute['phone'] = $request->phone ;

        $this->charityrepository->update($attribute , $emp); 

        $employee->syncPermissions($permissions);
            
        // return redirect()->route('charity.employees.index')->with('success' , __('admin.update_employee_succcess'));

        $response['status'] = 1;
        $response['redirect'] = route('charity.employees.edit', $employee->slug);
        $response['reload'] = 0;

        return $response;

    } // end of update 
    
    public function destroy($emp)
    {

        if(!checkPermissions(['delete_employees_'. $this->user->id])){

            return response()->json(['data' => 2], 200);
        }

        $employee  = $this->charityrepository->getWhere([['id', $employee], ['nation_id', $nation_id], 
                    ['parent_id', $this->user->id]])->first();

        if(!$employee){

            return response()->json(['data' => 2], 200);
            
        } 

        $employee->permissions()->detach();
        
        $this->charityrepository->delete($emp);


        return response()->json(['data' => 1], 200);
        
    } // end of destroy 

}
