<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\User;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\EditRequestRepositoryInterface;
use App\Servicies\Notify;
use Illuminate\Http\Request;
use Auth;
use Spatie\Permission\Models\Permission;
use Alkoumi\LaravelHijriDate\Hijri;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\ContractRepositoryInterface;
use App\Servicies\UploadFilesServices;
use App\Http\Requests\Charity\CharityEmployeeProfileRequest;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Http\Requests\Charity\ProfileRequest;
use Storage;

use Monarobase\CountryList\CountryListFacade;

class ProfileController extends Controller
{
    private $editrequestrepository;
    private $charityrepository;
    private $contractrepository;
    private $charitycategoryrepository;
    private $fileservice;
    private $userrepository;
    private $logRepository;
    private $notificationRepository;
    
    public function __construct(EditRequestRepositoryInterface $editrequestrepository ,
                                CharityRepositoryInterface $charityrepository ,
                                ContractRepositoryInterface $contractrepository,
                                CharityCategoryRepositoryInterface $charitycategoryrepository,
                                UserRepositoryInterface $userrepository,
                                UploadFilesServices $fileservice,
                                LogRepositoryInterface $logRepository,
                                NotificationRepositoryInterface $notificationRepository
    )
    {
        
        $this->editrequestrepository     = $editrequestrepository;
        $this->charityrepository         = $charityrepository;
        $this->contractrepository        = $contractrepository;
        $this->charitycategoryrepository = $charitycategoryrepository;
        $this->fileservice               = $fileservice;
        $this->userrepository            = $userrepository;
        $this->logRepository             = $logRepository;
        $this->notificationRepository             = $notificationRepository;

    } // end of construct

    public function index()
    {

        // delete notifications
        $this->notificationRepository->deleteWhere([['title', 'charity_data_updated']]);
        $this->notificationRepository->deleteWhere([['title', 'charity']]);
        $this->notificationRepository->deleteWhere([['title', 'edit_request_charity']]);

        $edit_request = $this->editrequestrepository->getRequestEditCharity(Auth::user()->id);

        return view('charities.profile.profile' , compact('edit_request'));

    } // end of class

    public function sendRequestEdit(Request $request)
    { 

        $attribute = $request->except('_token' , 'nation_id' , 'status');

        // append nation id
        $attribute['nation_id'] = getNationId();
        // append status
        $attribute['status'] = 'waiting';

        $attribute['model_id'] = $request->item;
        $attribute['type'] = 'charity';
        $attribute['emp_id'] = auth()->user()->id; // ال id بتاع اللى باعت طلب التعديل

        $charity = $this->charityrepository->findOne($request->item);

        $this->editrequestrepository->create($attribute);


        // Create log data
        $log_data['user_id'] = auth()->user()->id;
        $log_data['user_type'] = 'charity';
        $log_data['model_id'] = $request->item;
        $log_data['model_type'] = 'charity';
        $log_data['edit_type'] = 'Send charity edit request';

        $this->logRepository->create($log_data);

        // notify all admins that have permission to accept and refuse charity edit requests
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'طلب تعديل جمعية';
        $message->message_en = 'Edit Request Charity';

        // GET ALL ADMINS THAT HAVE PERMISSION TO ACCEPT AND REFUSE CHARITY EDIT REQUEST
        $admins = $this->userrepository->whereHas('permissions', [['name', 'accept_refuse_charity_edit_requests']]);

        // FOREACH ADMINS
        foreach($admins as $admin)    
        {

            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $admin->id,
                'model_id'       => $charity->id,
                'title'          => 'edit_charity',
                'type'           => 'admin'
            ]);             
        
        } // end of foreach admins             
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);  

        return response()->json([
            'success' => 1,
            'message' => trans('admin.send_request_success')
        ], 200);

    } // end of sendRequestEdit   
    
    public function contract()
    {
        $nation_id = getNationId();

        // date higri
        $date_higri = \App::getLocale() == 'ar' ? Hijri::DateIndicDigits('Y') : Hijri::Date('Y');
        // date milady
        $date = Date('Y');

        $contracts = $this->contractrepository->getWhere([['nation_id', $nation_id]]);
        $categories = $this->charitycategoryrepository->getWhere([['nation_id', $nation_id]]);

        return view('charities.profile.contract' , compact('date' , 'date_higri' , 'contracts' , 'categories'));
    } // end fo contract

    public function signature(Request $request)
    {

        if ($request->e3tmad_input == 'pdf') {

            $attribute = $request->except('_token' , 'file_input' , 'e3tmad_input' , 'canvas_convert');

            // uploade file contract after sign
            if ($request->hasFile('file_input')) {     

                $img = $request->file('file_input');

                $file_input = $this->fileservice->uploadfile($img, 'charity');

                $attribute = $file_input;

            } // end of if image 
            
            auth()->guard('charity')->user()->update([
                'contract_after_sign' => $attribute
            ]);

        } else 
        {

            auth()->guard('charity')->user()->update([
                'signature' => $request->canvas_convert
            ]);

        } // end of else if

        return redirect()->route('charity.dashboard');
    }

    public function editEmployeeProfile(){

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        return view('charities.profile.edit-emp-profile')->with([
            'data' => $data
        ]);
    }

    public function updateEmployeeProfile(CharityEmployeeProfileRequest $request){

        $data = $request->except(['_token', '_method', 'phone_num']);

        $updated = $this->charityrepository->update($data, auth()->user()->id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }

    }

    public function edit(){

        return view('charities.profile.edit-profile');
    }

    public function update(ProfileRequest $request){
        
        $data = $request->except(['_token', '_method']);

        if($request->has('logo')){

            Storage::delete(auth()->user()->logo);

            $data['logo'] = $request->file('logo')->store('uploads');
        }

        $updated = auth()->user()->update($data);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

} // end of class

