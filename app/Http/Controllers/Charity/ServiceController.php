<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use App\Repositories\ServiceOptionRepositoryInterface;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    
    public $servicerepository;

    public function __construct(ServiceOptionRepositoryInterface $servicerepository)
    {

        $this->servicerepository = $servicerepository;
        
    } // end of construct

    public function emerg(Request $request)
    {
       
        $emergs = $this->servicerepository->emerg();

        return view('charities.projects.services-emerg' , compact('emergs'))->render();

    } // end of emerg

    public function edu(Request $request)
    {
       
        $edus = $this->servicerepository->edu();

        return view('charities.projects.services-edu' , compact('edus'))->render();

    } // end of emerg    

} // end of class
