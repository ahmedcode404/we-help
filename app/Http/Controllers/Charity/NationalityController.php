<?php

namespace App\Http\Controllers\Charity;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NationalityRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Http\Requests\Admin\NationalityRequest;

class NationalityController extends Controller
{

    private $nationalityRepository;
    public $userRepository;

    public function __construct(NationalityRepositoryInterface $nationalityRepository,
                                UserRepositoryInterface $userRepository
                                ){

        $this->nationalityRepository = $nationalityRepository;
        $this->userRepository = $userRepository;

        $this->middleware(function ($request, $next) {

            if(auth()->user()->hasRole('charity_employee')){

                $this->user = auth()->user()->charity;
            }
            else{
                
                $this->user = auth()->user();
            }

            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_nationalities_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['nationalities'] = $this->nationalityRepository->paginateWhere([['guard', 'charity'], ['guard_id', $this->user->id]]);

        return view('charities.nationalities.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_nationalities_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('charities.nationalities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NationalityRequest $request)
    {
        if(!checkPermissions(['create_nationalities_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $data['nation_id'] = $this->user->nation_id;
        $data['guard'] = 'charity';
        $data['guard_id'] = $this->user->id;
        $data['slug'] = \Str::random(12);
        
        $nationality = $this->nationalityRepository->create($data);

        if($nationality){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_nationalities_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['nationality'] = $this->nationalityRepository->getWhereWith(['charity_emp'], [['slug', $id], ['guard', 'charity'], ['guard_id', $this->user->id]])->first();

        if(!$data['nationality']){

            return view('auth.not-found');
            
        }


        return view('charities.nationalities.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_nationalities_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['nationality'] = $this->nationalityRepository->getWhere([['slug', $id], ['guard', 'charity'], ['guard_id', $this->user->id]])->first();

        if(!$data['nationality']){

            return view('auth.not-found');
            
        }

        return view('charities.nationalities.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NationalityRequest $request, $id)
    {
        if(!checkPermissions(['edit_nationalities_'.$this->user->id])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $updated = $this->nationalityRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_nationalities_'.$this->user->id])){

            return response()->json(['data' => 2], 200);
        }

        $data['nationality'] = $this->nationalityRepository->getWhere([['id', $id], ['guard', 'charity'], ['guard_id', $this->user->id]])->first();

        if(!$data['nationality']){

            return response()->json(['data' => 2], 200);
            
        }

        // update related users to have nationality_id = null
        $this->userRepository->updateWhere(['nationality_id' => null], [['nationality_id', $id]]);

        // delete nationality
        $deleted = $this->nationalityRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
