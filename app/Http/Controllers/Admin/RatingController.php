<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Rating;
use App\Models\Project;
use DB;
use App\Repositories\RatingRepositoryInterface;


class RatingController extends Controller
{
    private $ratingRepository;

    public function __construct(RatingRepositoryInterface $ratingRepository){

        $this->ratingRepository = $ratingRepository;
    }

    
    public function index(){

        if(!checkPermissions(['list_ratings'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['ratings'] = $this->ratingRepository->getRatings($nation_id);

        return view('admin.ratings.index')->with([
            'data' => $data
        ]);
    }

    public function deleteComment($user_id, $project_id){

        if(!checkPermissions(['delete_comment'])){

            return response()->json(['data' => 2], 200);
        }


        $data['comment'] = null;

        $updated = $this->ratingRepository->updateWhere($data, [['user_id', $user_id], ['project_id', $project_id]]);

        if($updated){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 'error'], 200);
        }

    }

    public function showForWeb(Request $request, $user_id, $project_id){

        if(!checkPermissions(['list_ratings'])){

            return response()->json(['data' => 2], 200);
        }

        $rates = $this->ratingRepository->getWhere([['user_id', $user_id], ['project_id', $project_id]]);

        if(count($rates) > 0){

            $updated = $this->ratingRepository->updateWhere(['show_for_web' => $request->value], [['user_id', $user_id], ['project_id', $project_id]]);
        }

        // if(count($rates) > 0 && $rates[0]->show_for_web == 1){

        //     $updated = $this->ratingRepository->updateWhere(['show_for_web' => 0], [['user_id', $user_id], ['project_id', $project_id]]);
        // }
        // else if(count($rates) > 0 && $rates[0]->show_for_web == 0){

        //     $updated = $this->ratingRepository->updateWhere(['show_for_web' => 1], [['user_id', $user_id], ['project_id', $project_id]]);
        // }

        if($updated){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 'error'], 200);
        }

    }
}
