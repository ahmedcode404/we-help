<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\ChangeStatusRequest;
use App\Models\Notification;
use App\Models\Setting;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use App\Servicies\Notify;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\DegreeRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\FinancialRequestRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use Auth;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\Admin\ProfileRequest;
use Storage;

use Monarobase\CountryList\CountryListFacade;

class MainController extends Controller
{
    private $charityRepository;
    private $projectRepository;
    private $editrequestrepository;
    private $cityrepository;
    private $degreeRepository;
    private $userRepository;
    private $financialRequestRepository;
    private $notificationRepository;
    private $logRepository;

    public function __construct(CharityRepositoryInterface $charityRepository,
                                ProjectRepositoryInterface $projectRepository,
                                ReportRepositoryInterface $reportRepository,
                                EditRequestRepositoryInterface $editrequestrepository,
                                CityRepositoryInterface $cityrepository,
                                DegreeRepositoryInterface $degreeRepository,
                                UserRepositoryInterface $userRepository,
                                FinancialRequestRepositoryInterface $financialRequestRepository,
                                NotificationRepositoryInterface $notificationRepository,
                                LogRepositoryInterface $logRepository
                                ){

        $this->charityRepository = $charityRepository;
        $this->projectRepository = $projectRepository;
        $this->reportRepository = $reportRepository;
        $this->editrequestrepository = $editrequestrepository;
        $this->cityrepository = $cityrepository;
        $this->degreeRepository = $degreeRepository;
        $this->userRepository = $userRepository;
        $this->financialRequestRepository = $financialRequestRepository;
        $this->notificationRepository = $notificationRepository;
        $this->logRepository = $logRepository;
    }

    public function diffDays(Request $request){

        $duration =  getDuration($request->start_date, $request->end_date);

        return response()->json([

            'data' => 'success',
            'duration' => $duration

        ], 200);
    }

    // Change status
    public function changeStatus(ChangeStatusRequest $request){

        if(!checkPermissions(['accept_refuse_charities'])){

            session('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $voucher_link = '';

        $data = $request->except(['_token', '_method', 'id', 'model']);

        $updated = false;

        if($request->model == 'charity'){

            if(!checkPermissions(['accept_refuse_charities'])){

                return response()->json(['data' => 2], 200);
            }

            $updated = $this->charityRepository->update($data, $request->id);
    
            // Send Email to charity
            $charity = $this->charityRepository->findOne($request->id);

            $msg = '';
            $email = [['email' => $charity->email]];
            $topic = trans('admin.change_status_topic');
            $nation_id = $charity->nation_id;
            $notes = $charity->notes;

            if($request->status == 'approved'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Approve charity';

                $this->logRepository->create($log_data);


                $msg = getSettingMsg('charity_approve_msg', $charity->nation_id)->value;
                $notify_ar = Setting::where('key' , 'charity_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'charity_approve_msg_sa_en')->first();

            }
            else if($request->status == 'rejected'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Reject charity';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('charity_reject_msg', $charity->nation_id)->value;
                $notify_ar = Setting::where('key' , 'charity_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'charity_reject_msg_sa_en')->first();

            }
            else if($request->status == 'waiting'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Set charity status to Waiting';

                $this->logRepository->create($log_data);
            }
            else if($request->status == 'hold'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Set charity status to Hold';

                $this->logRepository->create($log_data);
            }

            if ($request->status == 'approved' || $request->status == 'rejected') 
            {
                
                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $charity->id,
                    'title'          => $request->model,
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , $request->model , 'charity' , $charity->id); 


            } /// end of if rejected or approved

        }
        else if($request->model == 'project'){

            if(!checkPermissions(['accept_refuse_projects'])){

                return response()->json(['data' => 2], 200);
            }

            $updated = $this->projectRepository->update($data, $request->id);

            // Send Email to charity
            $project = $this->projectRepository->findWith($request->id, ['charity']);

            $msg = '';
            $email = [['email' => $project->charity->email]];
            $topic = trans('admin.change_status_topic');
            $nation_id = $project->nation_id;
            $notes = $project->notes;

            if($request->status == 'approved'){

                // Creat log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Approve project';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('project_approve_msg', $project->nation_id)->value;
                $notify_ar = Setting::where('key' , 'project_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'project_approve_msg_sa_en')->first();                

            }
            else if($request->status == 'rejected'){

                // Creat log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Reject project';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('project_reject_msg', $project->nation_id)->value;
                $notify_ar = Setting::where('key' , 'project_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'project_reject_msg_sa_en')->first();

            }
            else if($request->status == 'waiting'){

                // Creat log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Set project status to Waiting';

                $this->logRepository->create($log_data);
            }
            else if($request->status == 'hold'){

                // Creat log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Set project status to Hold';

                $this->logRepository->create($log_data);
            }

            if ($request->status == 'approved' || $request->status == 'rejected') 
            {
                
                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $project->charity->id,
                    'model_id'       => $project->id,
                    'title'          => $request->model,
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , $request->model , 'charity' , $project->charity->id); 

                $messagesubscribe = 'تم اضافه مشروع جديد للمشاهد اضغط علي الرابط الموجود بالاسفل';

                sendMailSubscribe($messagesubscribe , $project->slug, 1);



            } /// end of if rejected or approved            

        }
        else if($request->model == 'edit_request_project'){
            
            $updated = $this->editrequestrepository->update($data, $request->id);
            
            // Send Email to charity
            $edit_note = $this->editrequestrepository->findWhereWith(['id' => $request->id , 'type' => 'project' ], ['project.charity']);
            $charity_emp = $this->charityRepository->findOne($edit_note->emp_id);

            $msg = '';
            $email = [['email' => $charity_emp->email]];
            $topic = trans('admin.change_status_topic');
            $nation_id = $charity_emp->nation_id;
            $notes = $edit_note->notes;

            if($request->status == 'approved'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $edit_note->model_id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Approve edit request to project';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('edit_approve_msg', $edit_note->nation_id)->value;
                $notify_ar = Setting::where('key' , 'edit_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'edit_approve_msg_sa_en')->first();

                $permission = Permission::where('name' , 'edit_project_' . $edit_note->model_id)->first();

                $edit_note->project->charity->syncPermissions($permission);

                $this->projectRepository->update(['status' => 'hold'], $edit_note->project->id);

            }
            else if($request->status == 'rejected'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $edit_note->model_id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Reject edit request to project';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('edit_reject_msg', $edit_note->nation_id)->value;
                $notify_ar = Setting::where('key' , 'edit_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'edit_reject_msg_sa_en')->first();

            }
            else if($request->status == 'waiting'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $edit_note->model_id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Set project edit request status to Wainting';

                $this->logRepository->create($log_data);
            }
            else if($request->status == 'hold'){

                 // Create log data
                 $log_data['user_id'] = auth()->user()->id;
                 $log_data['user_type'] = 'admin';
                 $log_data['model_id'] = $edit_note->model_id;
                 $log_data['model_type'] = 'project';
                 $log_data['edit_type'] = 'Set project edit request status to Hold';
 
                 $this->logRepository->create($log_data);
            }

            if ($request->status == 'approved' || $request->status == 'rejected') 
            {
                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;

                $notification_data['title'] = $request->model;
                $notification_data['send_user_id'] = $edit_note->emp_id;
                $notification_data['model_id'] = $edit_note->project->id;
                $notification_data['type'] = 'charity';
                $notification_data['message_ar'] = $message->message_ar;
                $notification_data['message_en'] = $message->message_en;
        
                $this->notificationRepository->create($notification_data);

                Notify::NotifyWeb($message , $request->model , 'charity' , $charity_emp->id); 


            } /// end of if rejected or approved             

        } 
        else if($request->model == 'edit_request_charity'){

            $updated = $this->editrequestrepository->update($data, $request->id);

            $edit_note = $this->editrequestrepository->findWhereWith(['id' => $request->id , 'type' => 'charity' ], ['charity']);
            $charity_emp = $this->charityRepository->findOne($edit_note->emp_id);

            $permission = Permission::where('name' , 'edit_charity_' . $edit_note->model_id)->first();
            
            // Send Email to charity

            $msg = '';
            $email = [['email' => $charity_emp->email]];
            $topic = trans('admin.change_status_topic');
            $nation_id = $charity_emp->nation_id;
            $notes = $edit_note->notes;

            if($request->status == 'approved'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $edit_note->model_id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Approve edit request to charity';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('edit_approve_msg', $edit_note->nation_id)->value;
                $notify_ar = Setting::where('key' , 'edit_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'edit_approve_msg_sa_en')->first();

                $edit_note->charity->syncPermissions($permission);

                $this->charityRepository->update(['status' => 'hold'], $edit_note->charity->id);

            }
            else if($request->status == 'rejected'){

                $edit_note->charity->revokePermissionTo($permission->name);

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $edit_note->model_id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Reject edit request to charity';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('edit_reject_msg', $edit_note->nation_id)->value;
                $notify_ar = Setting::where('key' , 'edit_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'edit_reject_msg_sa_en')->first();

            }
            else if($request->status == 'waiting'){

                $edit_note->charity->revokePermissionTo($permission->name);

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $edit_note->model_id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Set charity edit request to Waiting';

                $this->logRepository->create($log_data);
            }
            else if($request->status == 'hold'){

                $edit_note->charity->revokePermissionTo($permission->name);

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $edit_note->model_id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Set charity edit request to Hold';

                $this->logRepository->create($log_data);
            }

            if ($request->status == 'approved' || $request->status == 'rejected') 
            {
                
                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $edit_note->emp_id,
                    'title'           => $request->model,
                    'type'           => 'charity'
                ]); 
                

                Notify::NotifyWeb($message , $request->model , 'charity' , $charity_emp->id); 


            } /// end of if rejected or approved              

        }                
        else if($request->model == 'report'){

            if(!checkPermissions(['accept_refuse_reports'])){

                return response()->json(['data' => 2], 200);
            }

            $updated = $this->reportRepository->update($data, $request->id);

            // Send Email to charity and to project supporters
            $report = $this->reportRepository->findWith($request->id, ['project', 'project.supports']);

            $email = [];

            if($request->status == 'approved'){

                foreach($report->project->supports as $support){

                    array_push($email, ['email' => $support->email]);
                }
            }

            $msg = '';
            array_push($email, ['email' => $report->project->charity->email]);
            $topic = trans('admin.change_status_topic');
            $nation_id = $report->project->nation_id;
            $notes = $report->notes;

            if($request->status == 'approved'){
                
                $msg = getSettingMsg('report_approve_msg', $nation_id)->value;
                $notify_ar = Setting::where('key' , 'report_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'report_approve_msg_sa_en')->first();

            }
            else if($request->status == 'rejected'){

                $msg = getSettingMsg('report_reject_msg', $nation_id)->value;
                $notify_ar = Setting::where('key' , 'report_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'report_reject_msg_sa_en')->first();

            }
            
            if ($request->status == 'approved' || $request->status == 'rejected') 
            {
                
                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $report->project->charity->id,
                    'model_id'       => $report->project->id,
                    'title'          => $request->model,
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , $request->model , 'charity' , $report->charity->id); 


            } /// end of if rejected or approved             

        }
        else if($request->model == 'financial_request'){

            if(!checkPermissions(['accept_refuse_finance_requests'])){

                return response()->json(['data' => 2], 200);
            }

            $updated = $this->financialRequestRepository->update($data, $request->id);

            $fin_request = $this->financialRequestRepository->findWith($request->id, ['requestable']);
    
            // Send Email to charity

            $msg = '';
            $email = [['email' => $fin_request->requestable->email]];
            $topic = trans('admin.change_status_topic');
            $nation_id = $fin_request->requestable->nation_id;
            $notes = $fin_request->notes;

            if($request->status == 'approved'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'finance';
                $log_data['edit_type'] = 'Approve financial request';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('fin_req_approve_msg', $fin_request->requestable->nation_id)->value;
                $notify_ar = Setting::where('key' , 'fin_req_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'fin_req_approve_msg_sa_en')->first();

                // create exchange voucher link - ارسال طلب الصرف - فاتوره بالمبلغ اللى هيتم تحويله للجمعيه
                $voucher_link = '<a href="'.route('get-voucher', $fin_request->slug).'">'.trans('admin.show').'</a>';

            }
            else if($request->status == 'rejected'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'finance';
                $log_data['edit_type'] = 'Reject financial request';

                $this->logRepository->create($log_data);
                
                $msg = getSettingMsg('fin_req_reject_msg', $fin_request->requestable->nation_id)->value;
                $notify_ar = Setting::where('key' , 'fin_req_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'fin_req_reject_msg_sa_en')->first();

            }
            else if($request->status == 'waiting'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'finance';
                $log_data['edit_type'] = 'Set financial request status to Waiting';

                $this->logRepository->create($log_data);
            }
            else if($request->status == 'hold'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $request->id;
                $log_data['model_type'] = 'finance';
                $log_data['edit_type'] = 'Set financial request status to Hold';

                $this->logRepository->create($log_data);
            }

            if ($request->status == 'approved' || $request->status == 'rejected') 
            {
                
                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $fin_request->requestable->id,
                    'title'          => $request->model,
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , $request->model , 'charity' , $fin_request->requestable->id); 


            } /// end of if rejected or approved

        }

        if($updated){

            if($request->status == 'approved' || $request->status == 'rejected'){

                try {
    
                    sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

                } catch (Throwable $e) {
    
                    return response()->json(['data' => 'exception'], 200);
    
                }  
    
            }

            return response()->json(['data' => 'success'], 200);
            
        }
        else{

            return response()->json(['data' => 'error'], 200);
        }

       
    }

    // delete image
    public function deleteImage(Request $request){

        $deleted = false;

        if($request->model == 'project'){

            if(!checkPermissions(['edit_project'])){

                return response()->json(['data' => 2], 200);
            }

            $deleted = $this->projectRepository->adminDeleteProjectImage($request->image_id, $request->id);
        }
        else if($request->model == 'report'){

            if(!checkPermissions(['edit_report'])){

                return response()->json(['data' => 2], 200);
            }

            $deleted = $this->reportRepository->adminDeleteReportImage($request->image_id, $request->id);

        }
        

        if($deleted){

            return response()->json(['data' => 'success'], 200);
        }
        else{

            return response()->json(['data' => 'error'], 200);
        }
    }

    // Edit profile for admin and employee
    public function editProfile(){

        $nation_id = getAuthNationId();

        $data['user'] = auth()->user();

        // $data['countries'] = $this->cityrepository->getWhere([['parent_id', null], ['nation_id', $nation_id]]);

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        if($data['user']->hasRole('employee')){

            $data['degrees'] = $this->degreeRepository->getWhere([['nation_id', $data['user']->nation_id]]);

            $data['cities'] = $this->cityrepository->getWhere([['parent_id', '!=', null], ['nation_id', $nation_id]]);
        }

        return view('admin.profile.edit')->with([
            'data' => $data
        ]);
    }

    public function updateProfile(ProfileRequest $request, $id){

        // $exist_user = $this->userRepository->getWhere([['phone', $request->phone]])->first();

        // if($exist_user){

        //     session()->flash('error', trans('admin.phone_exists'));

        //     return redirect()->back();
        // }

        $data = $request->except(['_token', '_method', 'phone_num', 'emp_data', 'password_confirmation', 'image']);

        if($request->has('password')){

            $data['password'] = bcrypt($request->password);

        }

        if($request->has('image')){

            // delete old image
            Storage::delete(auth()->user()->image);

            //ipload new image
            $data['image'] = $request->file('image')->store('uploads/');
        }

        $updated = $this->userRepository->update($data, $id);

        if($updated){

            // session()->flash('success', trans('admin.update_success'));

            // return redirect()->back();

            $response['status'] = 1;
            $response['redirect'] = '';
            $response['reload'] = 0;

            return $response;
        }
        else{

            // session()->flash('error', trans('admin.add_error'));

            // return redirect()->back();

            $response['status'] = 0;
            $response['redirect'] = '';
            $response['reload'] = 0;

            return $response;
        }

    }

    // exchange currency rate for auth user
    public function exchangeCurrency($currency_id){

        if(auth()->check() && auth()->user()->hasRole('admin') || auth()->user()->hasRole('employee'))
        {

            auth()->user()->update(['currency_id' => $currency_id]);

            session()->put('currency' , $currency_id);

        } else 
        {
            
            auth()->guard('charity')->user()->update(['currency_id' => $currency_id]);

            session()->put('currency' , $currency_id);

        }

        return redirect()->back();
    }

}
