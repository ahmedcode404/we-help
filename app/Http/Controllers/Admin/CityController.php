<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\CityRequest;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;

class CityController extends Controller
{

    private $cityRepository;
    private $charityRepository;
    private $projectRepository;

    function __construct(CityRepositoryInterface $cityRepository,
                        CharityRepositoryInterface $charityRepository,
                        ProjectRepositoryInterface $projectRepository){

        $this->cityRepository = $cityRepository;
        $this->charityRepository = $charityRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_cities'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cities'] = $this->cityRepository->paginateWhere([['nation_id', $nation_id]]);

        return view('admin.cities.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_city'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['countries'] = $this->cityRepository->getWhere([['parent_id', null], ['nation_id', $nation_id]]);

        return view('admin.cities.create')->with([
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CityRequest $request)
    {
        if(!checkPermissions(['create_city'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }
        
        $data = $request->except(['_token', '_method', 'category']);

        $data['nation_id'] = auth()->user()->nation_id;

        $city = $this->cityRepository->create($data);

        if($city){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_city'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['city'] = $this->cityRepository->getWhereWith(['charities', 'country'], [['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['city']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.cities.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_city'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['countries'] = $this->cityRepository->getWhere([['parent_id', null], ['nation_id', $nation_id]]);

        $data['city'] = $this->cityRepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['city']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.cities.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CityRequest $request, $id)
    {
        if(!checkPermissions(['edit_city'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method', 'category']);

        if($request->category == 'country'){

            $data['parent_id'] = null;
        }

        $updated = $this->cityRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_city'])){

            return response()->json(['data' => 2], 200);
        }
        
        // make city_id = null for charity - projects
        $this->charityRepository->updateWhere(['city_id' => null], [['city_id', $id]]);
        $this->projectRepository->updateWhere(['city_id' => null], [['city_id', $id]]);
        $this->cityRepository->updateWhere(['parent_id' => null], [['parent_id', $id]]);

        // delete city
        $deleted = $this->cityRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }

    }

    public function cityOfCountry(Request $request)
    {
        $nation_id = getAuthNationId();

        $cities = $this->cityRepository->getWhere([['nation_id', $nation_id], ['parent_id', $request->id]]);

        return view('charities.projects.cities' , compact('cities'))->render();

    } // end of city       
}
