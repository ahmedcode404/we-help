<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\AmbassadorRepositoryInterface;

class AmbassadorController extends Controller
{

    private $ambassadorRepository;

    public function __construct(AmbassadorRepositoryInterface $ambassadorRepository){

        $this->ambassadorRepository = $ambassadorRepository;

    }

    public function index(){

        if(!checkPermissions(['list_ambassadors'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['ambassadors'] = $this->ambassadorRepository->paginateWhereHasWith(['user', 'project', 'campaign'], 'user', [['nation_id', $nation_id]]);

        return view('admin.ambassadors.index')->with([
            'data' => $data
        ]);

    }
}
