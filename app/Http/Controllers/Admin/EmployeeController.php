<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\EmployeeRequest;
use App\Models\Setting;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\DegreeRepositoryInterface;
use App\Repositories\JobCategoryRepositoryInterface;
use App\Repositories\JobRepositoryInterface;
use App\Repositories\PermissionRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\NationalityRepositoryInterface;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Str;

use Monarobase\CountryList\CountryListFacade;

class EmployeeController extends Controller
{

    private $userrepository;
    private $cityrepository;
    private $permissionrepository;
    private $degreerepository;
    private $jopcategoyrepository;
    private $jobrepository;
    private $currencyrepository;
    private $nationalityRepository;
    
    public function __construct(UserRepositoryInterface $userrepository ,
                                CityRepositoryInterface $cityrepository ,
                                PermissionRepositoryInterface $permissionrepository ,
                                JobCategoryRepositoryInterface $jopcategoyrepository,
                                JobRepositoryInterface $jobrepository,
                                DegreeRepositoryInterface $degreerepository,
                                CurrencyRepositoryInterface $currencyrepository,
                                NationalityRepositoryInterface $nationalityRepository
                                )
    {
        
        $this->userrepository       = $userrepository;
        $this->cityrepository       = $cityrepository;
        $this->permissionrepository = $permissionrepository;
        $this->degreerepository     = $degreerepository;
        $this->jopcategoyrepository = $jopcategoyrepository;
        $this->jobrepository        = $jobrepository;
        $this->currencyrepository = $currencyrepository;
        $this->nationalityRepository = $nationalityRepository;

        $this->middleware('can:create_emp', ['only' => ['create','store']]);
        $this->middleware('can:edit_emp', ['only' => ['edit','update']]);
        $this->middleware('can:delete_emp', ['only' => ['destroy']]);
        $this->middleware('can:show_emp', ['only' => ['show']]);          
        $this->middleware('can:list_emp', ['only' => ['index']]);          

    } // end of construct

    public function index()
    {
        $nation_id = getAuthNationId();

        $employees = $this->userrepository->paginateWhereHas('roles', [['name', 'employee']], 10,  [], ['column' => 'id', 'dir' => 'DESC']);

        return view('admin.employees.index' , compact('employees'));

    } // end of index

    public function create()
    {
        $nation_id = getAuthNationId();

        $roles = $this->permissionrepository->allRole();
        $jop_categories = $this->jopcategoyrepository->getWhere([['nation_id', $nation_id]]);
        $countries = CountryListFacade::getList(\App::getLocale());
        $degrees = $this->degreerepository->getWhere([['nation_id', $nation_id]]);
        $nationalities = $this->nationalityRepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.employees.create' , compact('countries' , 'roles' , 'degrees' , 'jop_categories', 'nationalities'));

    } // end of create

    public function store(EmployeeRequest $request)
    {

        $attribute   = $request->except(
            '_token' ,
             'permission' ,
            'password' ,
            'confirm_password',
            'blocked',
            'phone_num',
            'jop_category_id',
        );

        $nation_id = getAuthNationId();

        $permissions = Permission::find($request->permission);

        $attribute['name_ar'] = $request->name_employee_ar;
        $attribute['name_en'] = $request->name_employee_en;
        $attribute['blocked'] = 0;
        $attribute['phone'] = $request->phone;
        $attribute['nation_id'] = $nation_id;

        $currency = $this->currencyrepository->getWhere([['symbol', 'GBP'], ['nation_id', $nation_id]])->first();

        // $attribute['currency_id'] = $currency ? $currency->id : auth()->user()->currency_id;

        $attribute['currency_id'] = $currency ? $currency->id : session('currency');

        $pass_rand = Str::random(12);

        $attribute['password'] = bcrypt($pass_rand);

        $employees = $this->userrepository->create($attribute);

        session()->forget('emp_phone');

        $employees->syncPermissions($permissions);
        $employees->assignRole('employee');

        // send data Login to this email

        $setting = Setting::DataLogin();
        $slogo = Setting::Slogan();

        $msg = '';
        $email = $request->email;
        $topic = $slogo->value;
        $nation_id = $nation_id;
        $dataLogin = $setting->value . '<br><b>' .
         __('admin.email') . ' </b> ' . ' : ' . $request->email . '<br>' .
          '<b>' . __('admin.password') . ' </b> ' . ' : ' . $pass_rand;
        
        $msg = getSettingMsg('msg_data_login', $nation_id)->value;
        
        try {
            
            sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $dataLogin);
        }
        catch(Throwable $e){

            session()->flash('error', trans('admin.unable_to_send_email'));

            return redirect()->back();
        }

        // return redirect()->route('admin.employees.index')->with('success' , __('admin.add_employee_succcess'));

        $response['status'] = 1;
        $response['redirect'] = route('admin.employees.create');
        $response['reload'] = 0;
        
        return $response;

    } // end of store

    public function edit($employee)
    {
        $nation_id = getAuthNationId();

        $employee  = $this->userrepository->getWhereWith(['job', 'nationality'], [['id', $employee], ['nation_id', $nation_id]])->first();

        if(!$employee){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        } 

        $countries = CountryListFacade::getList(\App::getLocale());
        $degrees = $this->degreerepository->getWhere([['nation_id', $nation_id]]);
        $roles     = $this->permissionrepository->allRole();
        $jop_categories = $this->jopcategoyrepository->getWhere([['nation_id', $nation_id]]);
        
        if($employee->job){

            $jops = $this->jobrepository->getWhere([['job_category_id', $employee->job->job_category_id]]);
        }
        else{
            $jops = $this->jobrepository->getAll();
        }
        $nationalities = $this->nationalityRepository->getAll();

        return view('admin.employees.edit' , compact('employee' , 'jops' , 'jop_categories' , 'countries' , 'roles', 'degrees', 'nationalities'));

        
    } // end of edit 

    public function show($employee)
    {

        $nation_id = getAuthNationId();

        $employee  = $this->userrepository->getWhereWith(['job', 'nationality'], [['id', $employee], ['nation_id', $nation_id]])->first();

        if(!$employee){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        } 

        return view('admin.employees.show' , compact('employee'));
        
    } // end of show     
     
    public function update(EmployeeRequest $request , $emp)
    {
        $attribute  = $request->except(
            '_token' ,
            '_method',
            'permission' ,
            'password' ,
            'confirm_password',
            'blocked',
            'email',
            'item',
            'name_employee_ar',
            'name_employee_en',
            'phone_num',
            'jop_id',
            'jop_category_id',

        );

        $permissions = Permission::find($request->permission);

        $employee = $this->userrepository->findOne($emp);

        $attribute['name_ar'] = $request->name_employee_ar;
        $attribute['name_en'] = $request->name_employee_en;
        $attribute['blocked'] = 0;
        $attribute['email'] = $employee->email;
        $attribute['phone'] = $request->phone ;

        $this->userrepository->update($attribute , $emp);

        $employee->syncPermissions($permissions);
            
        // return redirect()->route('admin.employees.index')->with('success' , __('admin.update_employee_succcess'));

        $response['status'] = 1;
        $response['redirect'] = route('admin.employees.edit', $employee->id);
        $response['reload'] = 0;

        return $response;

    } // end of update 
    
    public function destroy($emp)
    {
        
        $employee = $this->userrepository->findOne($emp);

        $employee->permissions()->detach();
        
        $this->userrepository->delete($emp);


        return response()->json(['data' => 1], 200);
        
    } // end of destroy 

}
