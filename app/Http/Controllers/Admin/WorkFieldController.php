<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\WorkFieldRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Http\Requests\Admin\JobCategoryRequest;

class WorkFieldController extends Controller
{
    private $workFieldRepository;
    private $cityRepository;
    private $charityRepository;

    public function __construct(WorkFieldRepositoryInterface $workFieldRepository,
                                CityRepositoryInterface $cityRepository,
                                CharityRepositoryInterface $charityRepository){

        $this->workFieldRepository = $workFieldRepository;
        $this->cityRepository = $cityRepository;
        $this->charityRepository = $charityRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_work_fields'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['work-fields'] = $this->workFieldRepository->paginateWhere([['nation_id', $nation_id]]);

        return view('admin.work-fields.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_work_fields'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.work-fields.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobCategoryRequest $request)
    {
        if(!checkPermissions(['create_work_fields'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $data['nation_id'] = auth()->user()->nation_id;
        
        $field = $this->workFieldRepository->create($data);

        if($field){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_work_fields'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['field'] = $this->workFieldRepository->getWhereWith(['charities'], [['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['field']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.work-fields.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_work_fields'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['field'] = $this->workFieldRepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['field']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.work-fields.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobCategoryRequest $request, $id)
    {
        if(!checkPermissions(['edit_work_fields'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $updated = $this->workFieldRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(!checkPermissions(['delete_work_fields'])){

            return response()->json(['data' => 2], 200);
        }

        // detach charities
        $field = $this->workFieldRepository->findWith($id, ['charities']);

        foreach($field->charities as $charity){

            $charity->work_fields()->attach($id);
        }

        // delete wotk field
        $deleted = $this->workFieldRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
