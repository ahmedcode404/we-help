<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\LogRepositoryInterface;

class LogController extends Controller
{

    private $logRepository;

    public function __construct(LogRepositoryInterface $logRepository){

        $this->logRepository = $logRepository;
    }

    public function index(){

        $data['logs'] = $this->logRepository->paginateWith(['admin_emp', 'charity_emp', 'project', 'finance']);

        return view('admin.logs.index')->with([

            'data' => $data
        ]);
    }
}
