<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\MarketingRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Http\Requests\Admin\MarketingRequest;

class MarketingController extends Controller
{
    private $marketingRepository;
    private $cityRepository;
    private $projectRepository;

    public function __construct(MarketingRepositoryInterface $marketingRepository,
                                CityRepositoryInterface $cityRepository,
                                ProjectRepositoryInterface $projectRepository){

        $this->marketingRepository = $marketingRepository;
        $this->cityRepository = $cityRepository;
        $this->projectRepository = $projectRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(!checkPermissions(['list_marketings'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }
        
        $nation_id = getAuthNationId();

        $data['marketings'] = $this->marketingRepository->paginateWhere([['nation_id', $nation_id]]);

        return view('admin.marketings.index')->with([
            'data' => $data
        ]);

    }

    public function getByType($type){

        if(!checkPermissions(['list_marketings'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['marketings'] = $this->marketingRepository->paginateWhere([['nation_id', $nation_id], ['type', $type]]);

        return view('admin.marketings.index')->with([
            'data' => $data
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        if(!checkPermissions(['create_marketings'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['aff'] = substr(uniqid(), 0, 8);

        $data['projects'] = $this->projectRepository->whereHasWith(['charity'], 'charity', [['status', 'approved']], 
                        [['profile', 0], ['active', 1], ['status', 'approved'], ['donation_complete', 0]]);

        return view('admin.marketings.create')->with([
            'data' => $data
        ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MarketingRequest $request)
    {

        // dd($request->all());
        if(!checkPermissions(['create_marketings'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data = $request->except(['_token', '_method', 'files', 'link']);

        if($request->project_link != null){

            $data['aff'] = null;
            $data['project_link'] = $request->aff;
        }

        $data['nation_id'] = $nation_id;

        $marketing = $this->marketingRepository->create($data);

        if($marketing){

            // session()->flash('success', trans('admin.add_success'));

            // return redirect()->back();

            $response['status'] = 1;
            $response['redirect'] = route('admin.marketings.create');
            $response['reload'] = 0;
            return $response;
        }
        else{

            $response['status'] = 0;
            $response['reload'] = 0;
            return $response;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_marketings'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['marketing'] = $this->marketingRepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        $data['projects'] = $this->projectRepository->whereHasWith(['charity'], 'charity', [['status', 'approved']], 
                        [['profile', 0], ['active', 1], ['status', 'approved'], ['donation_complete', 0]]);

        $data['aff'] = substr(uniqid(), 0, 8);

        if(!$data['marketing']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.marketings.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MarketingRequest $request, $id)
    {
        // dd($request->all());
        if(!checkPermissions(['edit_marketings'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method', 'files', 'link']);

        if($request->project_link != null){

            $data['aff'] = null;
            $data['project_link'] = $request->aff;
        }

        $updated = $marketing = $this->marketingRepository->update($data, $id);

        if($updated){

            // session()->flash('success', trans('admin.update_success'));

            // return redirect()->back();

            $response['status'] = 1;
            $response['redirect'] = route('admin.marketings.edit', $id);
            $response['reload'] = 0;
            return $response;
        }
        else{

            // session()->flash('error', trans('admin.add_error'));

            // return redirect()->back();

            $response['status'] = 0;
            $response['reload'] = 0;
            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_marketings'])){

            return response()->json(['data' => 2], 200);
        }

        // delete marketing
        $deleted = $this->marketingRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }

    public function aff($aff){

        $marketing = $this->marketingRepository->getWhere([['aff', $aff]])->first();

        if($marketing){

            $this->marketingRepository->update(['seen' => $marketing->seen + 1], $marketing->id);
        }

        // return redirect()->route('web.home');
        return redirect('/');
    }
}
