<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepositoryInterface;

class SupporterController extends Controller
{

    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository){

        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_supporters'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['supporters'] = $this->userRepository->paginateWhereHas('roles', [['name', 'supporter']], 10, [['nation_id', $nation_id]]);

        return view('admin.supporters.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_supporter'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        // $data['supporter'] = $this->userRepository->whereHasWith(['projects', 'country'], 'projects', [['projects.nation_id', $nation_id]], [['id', $id], ['nation_id', $nation_id]])->first();
        $data['supporter'] = $this->userRepository->getWhereWith(['projects'], [['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['supporter']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.supporters.show')->with([
            'data' => $data
        ]);

    }

    public function blockUser($id){

        if(!checkPermissions(['block_unblock_supporter'])){

            return response()->json(['data' => 2], 200);
        }  

        $supporter = $this->userRepository->findOne($id);

        $updated = $this->userRepository->update(['blocked' => !$supporter->blocked], $id);

        $supporter = $this->userRepository->findOne($id);

        if($updated){

            $ids = [];
            array_push($ids, $id);

            if($supporter->blocked == 1){

                // Notify Supporters via mobile app that his account is blocked
                notifyMobile(null, $ids, 'account_blocked', 'supporter',  'تم حظر الحساب الخاص بك', 'Your account has been blocked', asset('web/images/main/user_avatar.png'));
                
            }
            else{

                // Notify Supporters via mobile app that his account is blocked
                notifyMobile(null, $ids, 'account_unblocked', 'supporter',  'تم إالغاء حظر الحساب الخاص بك', 'Your account has been unblocked', asset('web/images/main/user_avatar.png'));
            }
            

            return response()->json(['data' => 'success'], 200);
            
        }
        else{

            return response()->json(['data' => 'error'], 200);
        }
        
    }

    
}
