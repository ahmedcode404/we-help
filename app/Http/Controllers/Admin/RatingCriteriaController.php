<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RatingCriteriaRequest;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\RatingCriteriaRepositoryInterface;
use Illuminate\Http\Request;

class RatingCriteriaController extends Controller
{

    private $ratingcriteriarepository;
    private $cityrepository;
   
    public function __construct(RatingCriteriaRepositoryInterface $ratingcriteriarepository , CityRepositoryInterface $cityrepository)
    {
        
        $this->ratingcriteriarepository = $ratingcriteriarepository;
        $this->cityrepository           = $cityrepository;

        $this->middleware('can:edit_ratingcriteria',   ['only' => ['edit','update']]);
        $this->middleware('can:show_ratingcriteria',   ['only' => ['index']]);               

    } // end of construct    

    public function index()
    {

        $nation_id = getAuthNationId();

        $ratingcriterias = $this->ratingcriteriarepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.rating_criterias.index' , compact('ratingcriterias')); 

    } // end of create     

    public function edit($ratingc)
    {
        $nation_id = getAuthNationId();

        $ratingcriteria = $this->ratingcriteriarepository->getWhere([['id', $ratingc], ['nation_id', $nation_id]])->first();

        if(!$ratingcriteria){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.rating_criterias.edit' , compact('ratingcriteria')); 

    } // end of create    

    public function update(RatingCriteriaRequest $request , $currency)
    {
       
        $attribute = $request->except('_token' , '_method' , 'name_rating_criteria_ar' , 'name_rating_criteria_en');

        $attribute['name_ar'] = $request->name_rating_criteria_ar;
        $attribute['name_en'] = $request->name_rating_criteria_en;        

        $this->ratingcriteriarepository->update($attribute , $currency);

        return redirect()->route('admin.rating-criterias.index')->with('success' , __('admin.update_rating_criteria_succcess'));
        
    } // end of update  
}
