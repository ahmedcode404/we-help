<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\StaticPageRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Http\Requests\Admin\StaticPageRequest;
use Storage;

class StaticPageController extends Controller
{

    private $staticPageRepository;
    private $cityRepository;

    public function __construct(StaticPageRepositoryInterface $staticPageRepository, 
                                CityRepositoryInterface $cityRepository){

        $this->staticPageRepository = $staticPageRepository;
        $this->cityRepository = $cityRepository;
    }
    
    
    // Get static page by version
    public function getStaticPagesByNation(){

        $nation_id = getAuthNationId();

        $data['pages'] = $this->staticPageRepository->paginateWhereWith([['nation_id', $nation_id], ['parent_id', null], ['key', '!=', 'supprot_contract']], 
                                                    ['nation'], ['column' => 'id', 'dir' => 'ASC']);

        return view('admin.static_pages.index')->with([
            'data' => $data
        ]);
    }

    // Update static page
    public function edit($id){

        if(!checkPermissions(['edit_static_pages'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }
        
        $nation_id = getAuthNationId();

        $data['page'] = $this->staticPageRepository->getWhereWith(['children'], [['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['page']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.static_pages.edit')->with([
            'data' => $data
        ]);

    }

    // Update static page
    public function update(StaticPageRequest $request, $id){

        if($request->has('child_ids')){
            
            foreach($request->child_ids as $child_id){

                if($request->has('image-'.$child_id)){

                    $this->validate($request, [
                        'image-'.$child_id => 'nullable|image|mimes:jpeg,png,jpg|max:2048'
                    ]);
                }
            }
        }

        if(!checkPermissions(['edit_static_pages'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->only(['title_ar', 'title_en', 'content_ar', 'content_en', 'image']);

        $page = $this->staticPageRepository->findWith($id, ['children']);

        if($request->has('image')){

            Storage::delete($page->image);

            $data['image'] = $request->file('image')->store('uploads/pages/images');
        }

        $updated = $this->staticPageRepository->update($data, $id);

        $child_updated = 1;

        foreach($page->children as $key => $child){
            
            $child_data['title_ar'] = $request->{'title_ar_'.$child->id};
            $child_data['title_en'] = $request->{'title_en_'.$child->id};

            $child_data['content_ar'] = $request->{'content_ar_'.$child->id};
            $child_data['content_en'] = $request->{'content_en_'.$child->id};

            if($request->has('image-'.$child->id)){

                Storage::delete($child->image);

                $child_data['image'] = $request->{'image-'.$child->id}->store('uploads/pages/images');
            }

            if($request->has('link-'.$child->id)){

                $child_data['link'] = $request->{'link-'.$child->id};
            }

            $child_updated = $this->staticPageRepository->update($child_data, $child->id);

            $child_data = [];
        }

        if($updated && $child_updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }

    }


}
