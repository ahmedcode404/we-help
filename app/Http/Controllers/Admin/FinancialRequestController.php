<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\FinancialRequestRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Http\Requests\Admin\FinanceRequest;
use Illuminate\Support\Str;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\PhaseRepositoryInterface;
use App\Models\Notification;
use App\Models\Setting;
use App\Servicies\Notify;

class FinancialRequestController extends Controller
{

    private $financialRequestRepository;
    private $charityRepository;
    private $projectRepository;
    private $logRepository;
    private $phaseRepository;

    public function __construct(FinancialRequestRepositoryInterface $financialRequestRepository,
                                CharityRepositoryInterface $charityRepository,
                                ProjectRepositoryInterface $projectRepository,
                                LogRepositoryInterface $logRepository,
                                PhaseRepositoryInterface $phaseRepository
                                ){

        $this->financialRequestRepository = $financialRequestRepository;
        $this->charityRepository = $charityRepository;
        $this->projectRepository = $projectRepository;
        $this->logRepository = $logRepository;
        $this->phaseRepository = $phaseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(!checkPermissions(['list_exchange_requests']) && !checkPermissions(['list_catch_requests'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['financial_requests'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project', 'currency'], 'requestable', 
                                    [['nation_id', $nation_id]], 10, [['bank_account_num', null], ['voucher_num', null], ['type', 'exchange']]);

        return view('admin.financial-requests.index')->with([
            'data' => $data
        ]);
    }


    // get requests by type (exchange - catch)
    public function getRequestByStatus($status){

        if(!checkPermissions(['list_exchange_requests']) && !checkPermissions(['list_catch_requests'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['financial_requests'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', 
                                    [['nation_id', $nation_id]], 10, [['bank_account_num', null], ['voucher_num', null], ['type', 'exchange'], ['status', $status]]);
        
        return view('admin.financial-requests.index')->with([
            'data' => $data
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_finance_request'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }
        
        $nation_id = getAuthNationId();

        $data['charities'] = $this->charityRepository->getWhere([['status', 'approved'], ['nation_id', $nation_id]]);

        return view('admin.financial-requests.create')->with([
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FinanceRequest $request)
    {

        if(!checkPermissions(['create_finance_request'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $project = $this->projectRepository->findWith($request->project_id, ['exchange_requests', 'currency']);

        $project_cost = generalExchange($project->get_total, $project->currency->symbol, currencySymbol(session('currency')));

        // اجمالى التبرعات كلها
        $total_supports = $project->total_supports_for_project;

        //  اجمالى ما تم صرفه فعليا للمشروع
        $total_vouchers = $project->getTotalProjectExchangeVouchers();

        //  - المتبقى بعد الصرف - المتبقى من التبرعات و لم يتم صرف فعليا للمشروع
        $remaining_supports = $total_supports - $total_vouchers;

        // المتبقى من تكلفة المشروع - لم يتم جمع تبرعات ليه
        $remaining_cost = $project_cost - $total_supports;

        // dd($project_cost, $total_supports, $total_vouchers, $remaining_supports, $remaining_cost);

        // check if there is a registered financial request for this project before or not
        $old_request = $this->financialRequestRepository->getWhere([['requestable_type', 'App\Models\Charity'], ['project_id', $project->id],
                                                                ['bank_account_num', null], ['voucher_num', null]])->first();
        
        if($old_request != null){

            $response['status'] = 0;
            $response['message'] = trans('admin.old_request_exists');
            $response['reload'] = 0;

            return $response;

        }

        // if project supports are complete, then create the request - يتم انشاء طلب الصرف فقط فى حالة اكتمال تبرعات المشروع
        // if(count($project->phases) == 0 && $remaining_cost > 0){

        //     $response['status'] = 0;
        //     $response['message'] = trans('admin.support_not_complete');
        //     $response['reload'] = 0;

        //     return $response;

        // }

        // check if the entered cost is greater than project total supports
        if($request->out_to_charity > $remaining_supports){

            $response['status'] = 0;
            $response['message'] = trans('admin.invalid_cost');
            $response['reload'] = 0;

            return $response;
        }

        $transfered = false;
        $out_to_charity = $request->out_to_charity;
        $session_currency = currencySymbol(session('currency'));

        // يتم انشاء طلب الصرف فى حالة اكتمال مبلغ مرحله من مراحل المشروع
        if(count($project->phases) > 0){

            foreach($project->phases as $phase){

                if($phase->remaining_cost != 0){
    
                    $phase_cost = generalExchange($phase->remaining_cost, $project->currency->symbol, $session_currency);
    
                    // فى حالة تحق الشرط دا معناه ان فيه مرحلة فلوسها مكتمله و نقدر نعملها طلب صرف
                    if($out_to_charity >= $phase_cost){
                        
                        $data = $request->except(['_token', '_method']);
    
                        $data['requestable_type'] = 'App\Models\Charity';
                        $data['status'] = 'waiting';
                        $data['currency_id'] = session('currency');
                        $data['type'] = 'exchange';
                        $data['recieve_status'] = 'deserved';   
                        $data['slug'] = Str::random(12);
                        $data['out_to_charity'] = $phase_cost;
                        $data['phase_id'] = $phase->id;
    
                        $request = $this->financialRequestRepository->create($data);
    
                        $remaining = $phase_cost - $request->out_to_charity;
                        $this->phaseRepository->update(['remaining_cost' => $remaining < 0 ? 0 : $remaining], $phase->id);

                        $transfered = true;

                        $out_to_charity = $out_to_charity - $phase_cost;
                    }
                }
                
            }

            if(!$transfered){

                $response['status'] = 0;
                $response['message'] = trans('admin.phases_cost_not_complete');
                $response['reload'] = 0;

                return $response;

            }
        }
        // فى حالة عدم وجود مراحل للمشروع يتم الصرف للمشروع مباشرة
        else{
    
            $data = $request->except(['_token', '_method']);
    
            $data['requestable_type'] = 'App\Models\Charity';
            $data['status'] = 'waiting';
            $data['currency_id'] = session('currency');
            $data['type'] = 'exchange';
            $data['recieve_status'] = 'deserved';
            $data['slug'] = Str::random(12);
    
            $request = $this->financialRequestRepository->create($data);
    
        }

        if($request){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $request->id;
            $log_data['model_type'] = 'finance';
            $log_data['edit_type'] = 'Add financial request';

            $this->logRepository->create($log_data);

            // session()->flash('success', trans('admin.add_success'));

            // return redirect()->back();

            $response['status'] = 1;
            $response['redirect'] = route('admin.financial-requests.create');
            $response['reload'] = 0;

            return $response;
        }
        else{

            // session()->flash('error', trans('admin.add_error'));

            // return redirect()->back();

            $response['status'] = 0;
            $response['redirect'] = route('admin.financial-requests.create');
            $response['reload'] = 0;

            return $response;
        }

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_finance_request'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['request'] = $this->financialRequestRepository->whereHasWith(['project', 'requestable'], 'requestable', [['nation_id', $nation_id]], [['id', $id]])->first();

        if(!$data['request']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        $data['charities'] = $this->charityRepository->getWhere([['nation_id', $nation_id]]);

        $data['projects'] = $this->projectRepository->getWhere([['charity_id', $data['request']->requestable_id]]);

        return view('admin.financial-requests.edit')->with([
            'data' => $data
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FinanceRequest $request, $id)
    {
        if(!checkPermissions(['edit_finance_request'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
            
        }

        $fin_request = $this->financialRequestRepository->findWith($id, ['phase']);

        $project = $this->projectRepository->findWith($request->project_id, ['exchange_requests', 'currency', 'phases']);

        $project_cost = generalExchange($project->get_total, $project->currency->symbol, currencySymbol(session('currency')));

        // اجمالى التبرعات كلها
        $total_supports = $project->total_supports_for_project;

        //  اجمالى ما تم صرفه فعليا للمشروع
        $total_vouchers = $project->getTotalProjectExchangeVouchers();

        //  - المتبقى بعد الصرف - المتبقى من التبرعات و لم يتم صرف فعليا للمشروع
        $remaining_supports = $total_supports - $total_vouchers;

        // المتبقى من تكلفة المشروع - لم يتم جمع تبرعات ليه
        $remaining_cost = $project_cost - $total_supports;

        // dd($project_cost, $total_supports, $total_vouchers, $remaining_supports, $remaining_cost);

        // if project supports are complete, then create the request - يتم انشاء طلب الصرف فقط فى حالة اكتمال تبرعات المشروع
        // if(count($project->phases) == 0 && $remaining_cost > 0){

        //     $response['status'] = 0;
        //     $response['message'] = trans('admin.support_not_complete');
        //     $response['reload'] = 0;

        //     return $response;

        // }

        // check if the entered cost is greater than project total supports
        if($request->out_to_charity > $remaining_supports){

            $response['status'] = 0;
            $response['message'] = trans('admin.invalid_cost');
            $response['reload'] = 0;

            return $response;
        }

        $transfered = false;
        $out_to_charity = $request->out_to_charity;
        $session_currency = currencySymbol(session('currency'));

        // يتم انشاء طلب الصرف فى حالة اكتمال مبلغ مرحله من مراحل المشروع
        if(count($project->phases) > 0){

            // فى حالة تعديل طلب الصرف يتم ارجاع المبلغ المصروف للمرحلة علشان اعيد الحسابات على المبلغ المدخل جديد
            $old_phase = $this->phaseRepository->getWhere([['id', $fin_request->phase_id]])->first();
            $old_cost = generalExchange($fin_request->out_to_charity, $session_currency, $project->currency->symbol);
            $this->phaseRepository->update(['remaining_cost' => $old_phase->cost], $fin_request->phase_id);

            $project = $this->projectRepository->findWith($request->project_id, ['exchange_requests', 'currency', 'phases']);

            foreach($project->phases as $phase){

                if($phase->remaining_cost != 0){
    
                    $phase_cost = generalExchange($phase->remaining_cost, $project->currency->symbol, $session_currency);
    
                    // فى حالة تحق الشرط دا معناه ان فيه مرحلة فلوسها مكتمله و نقدر نعملها طلب صرف
                    if($out_to_charity >= $phase_cost){
    
                        $data['status'] = $request->status;
                        $data['currency_id'] = session('currency');
                        $data['out_to_charity'] = $phase_cost;
                        $data['notes'] = $request->notes;
                        $data['phase_id'] = $phase->id;
    
                        $updated = $this->financialRequestRepository->update($data, $id);
    
                        $remaining = $phase_cost - $request->out_to_charity;
                        $this->phaseRepository->update(['remaining_cost' => $remaining < 0 ? 0 : $remaining], $phase->id);

                        $transfered = true;

                        $out_to_charity = $out_to_charity - $phase_cost;
                    }
                }
                
            }

            if(!$transfered){

                $response['status'] = 0;
                $response['message'] = trans('admin.phases_cost_not_complete');
                $response['reload'] = 0;

                return $response;

            }
        }
        // فى حالة عدم وجود مراحل للمشروع يتم الصرف للمشروع مباشرة
        else{
    
            $data['status'] = $request->status;
            $data['currency_id'] = session('currency');
            $data['out_to_charity'] = $request->out_to_charity;
            $data['notes'] = $request->notes;
    
            $updated = $this->financialRequestRepository->update($data, $id);
    
        }

        if($updated){

            if($fin_request->status != 'approved' && $request->status == 'approved'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $fin_request->id;
                $log_data['model_type'] = 'finance';
                $log_data['edit_type'] = 'Approve financial request';

                $this->logRepository->create($log_data);

                $msg = getSettingMsg('fin_req_approve_msg', $fin_request->requestable->nation_id)->value;
                $email = [['email' => $fin_request->requestable->email]];
                $topic = trans('admin.change_status_topic');
                $nation_id = $fin_request->requestable->nation_id;
                $notes = $fin_request->notes;

                $notify_ar = Setting::where('key' , 'fin_req_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'fin_req_approve_msg_sa_en')->first();

                // create exchange voucher link - ارسال طلب الصرف - فاتوره بالمبلغ اللى هيتم تحويله للجمعيه
                $voucher_link = '<a href="'.route('get-voucher', $fin_request->slug).'">'.trans('admin.show').'</a>';

                try {
    
                    sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

                } catch (Throwable $e) {
    
                    return response()->json(['data' => 'exception'], 200);
    
                }  

                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $fin_request->requestable->id,
                    'title'          => 'financial_request',
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , 'financial_request' , 'charity' , $fin_request->requestable->id); 

            }
            else if($fin_request->status != 'rejected' && $request->status == 'rejected'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $fin_request->id;
                $log_data['model_type'] = 'finance';
                $log_data['edit_type'] = 'Reject financial request';

                $this->logRepository->create($log_data);

                $voucher_link = '';
                $msg = getSettingMsg('fin_req_reject_msg', $fin_request->requestable->nation_id)->value;
                $email = [['email' => $fin_request->requestable->email]];
                $topic = trans('admin.change_status_topic');
                $nation_id = $fin_request->requestable->nation_id;
                $notes = $fin_request->notes;

                $notify_ar = Setting::where('key' , 'fin_req_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'fin_req_reject_msg_sa_en')->first();

                try {
    
                    sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

                } catch (Throwable $e) {
    
                    return response()->json(['data' => 'exception'], 200);
    
                }  

                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $fin_request->requestable->id,
                    'title'          => 'financial_request',
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , 'financial_request' , 'charity' , $fin_request->requestable->id);

            }
            else{

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $id;
                $log_data['model_type'] = 'finance';
                $log_data['edit_type'] = 'Edit financial request';

                $this->logRepository->create($log_data);
            }

            $response['status'] = 1;
            $response['rdirect'] = route('admin.financial-requests.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['rdirect'] = route('admin.financial-requests.edit', $id);
            $response['reload'] = 0;

            return $response;
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_finance_request'])){

            return response()->json(['data' => 2], 200);
        }

        $session_currency = currencySymbol(session('currency'));

        $fin_request = $this->financialRequestRepository->findWith($id, ['project', 'project.currency', 'phase']);

        if($fin_request->project && count($fin_request->project->phases) > 0){

            // فى حالة تعديل طلب الصرف يتم ارجاع المبلغ المصروف للمرحلة علشان اعيد الحسابات على المبلغ المدخل جديد
            $old_phase = $this->phaseRepository->getWhere([['id', $fin_request->phase_id]])->first();
            $old_cost = generalExchange($fin_request->out_to_charity, $session_currency, $fin_request->project->currency->symbol);
            $this->phaseRepository->update(['remaining_cost' => $old_phase->cost], $fin_request->phase_id);
            
        }

        // delete request
        $deleted = $this->financialRequestRepository->delete($id);

        if($deleted){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'finance';
            $log_data['edit_type'] = 'Delete financial request';

            $this->logRepository->create($log_data);

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }

    // get charity projects
    public function getCharityProejcts(Request $request){

        $data['projects'] = $this->projectRepository->getWhere([['charity_id', $request->charity_id], ['profile', 0], ['active', 1], 
                        ['status', 'approved']]);

        return view('admin.financial-requests.ajax.projects')->with([
            'data' => $data
        ]);

    }

    public function getVoucher($slug){

        $voucher = $this->financialRequestRepository->getWhereWith(['requestable', 'project', 'project.phases'], [['slug', $slug]])->first();

        return view('admin.financial-requests.voucher', compact('voucher'));
    }
}
