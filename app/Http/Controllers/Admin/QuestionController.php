<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\QuestionRequest;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\FaqRepositoryInterface;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    
    public $faqrepository;
    public $cityrepository;

    public function __construct(FaqRepositoryInterface $faqrepository , CityRepositoryInterface $cityrepository)
    {

        $this->faqrepository  = $faqrepository;
        $this->cityrepository = $cityrepository;

    } // end of construct

    public function index()
    {

        if(!checkPermissions(['show_question'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $questions = $this->faqrepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.questions.index' , compact('questions'));
    } // end of index

    public function create()
    {

        if(!checkPermissions(['create_question'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.questions.create');

    } // end of create

    public function store(QuestionRequest $request)
    {

        if(!checkPermissions(['create_question'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $attribute = $request->except('_token', '_method');

        $attribute['nation_id'] = auth()->user()->nation_id;

        $this->faqrepository->create($attribute);

        return redirect()->back()->with('success' , __('admin.add_question_succcess'));

    } // end of store
    
    public function edit($id)
    {

        if(!checkPermissions(['edit_question'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $question = $this->faqrepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        if(!$question){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.questions.edit' , compact('question'));

    } // end of edit

    public function update(QuestionRequest $request , $id)
    {

        if(!checkPermissions(['edit_question'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $attribute = $request->except('_token' , '_method');

        $this->faqrepository->update($attribute , $id);

        return redirect()->route('admin.questions.index')->with('success' , __('admin.edit_question_succcess'));

    } // end of update 

    public function destroy($id)
    {

        if(!checkPermissions(['delete_question'])){

            return response()->json(['data' => 2], 200);
        }

        $this->faqrepository->delete($id);

        return response()->json(['data' => 1], 200);

    } // end of destroy

} // end of class
