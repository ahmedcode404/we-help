<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CartRepositoryInterface;

class EmpAgreementController extends Controller
{
    private $userrepository;
    private $charityrepository;
    private $cartrepository;

    public function __construct(UserRepositoryInterface $userrepository,
                                CharityRepositoryInterface $charityrepository, 
                                CartRepositoryInterface $cartrepository)
    {

        $this->userrepository = $userrepository;
        $this->charityrepository = $charityrepository;
        $this->cartrepository = $cartrepository;
        
    }

    public function saveAgreement(Request $request){

        $this->validate($request, ['agreement_approve' => 'required']);

        $user = $this->userrepository->getWhere([['email', $request->user_email]])->first();
        
        if(!$user){
            
            $user = $this->charityrepository->getWhere([['email', $request->user_email]])->first();
        }
        
        $data['agreement_approve'] = 1;
        $data['first_login'] = 0;

        if($user->hasRole('charity_employee')){

            $updated = $this->charityrepository->update($data, $user->id);
        }
        else{

            $updated = $this->userrepository->update($data, $user->id);
        }


        if($updated){

            if($user->hasRole('charity_employee')){

                auth()->guard('charity')->login($user);
            }
            else{

                auth()->login($user);
            }

            if(session()->has('cart_no')){

                $this->cartrepository->updateWhere(['user_id' => $user->id, 'cart_no' => null], [['cart_no', session('cart_no')]]);
                
                session()->forget('cart_no');
            }

            if($user->hasRole('charity_employee')){

                return redirect()->route('charity.dashboard')->with('success' , __('admin.successLogin'));
            }
            else{

                return redirect()->route('admin.home')->with('success' , __('admin.successLogin'));
            }

        }
        else{


            return redirect()->back();

        }
    }
}
