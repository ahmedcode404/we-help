<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ServiceOptionRequest;
use App\Repositories\ServiceOptionRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
   
    private $servicerepository;
    private $categoryrepository;

    private $nation_id;
   
    public function __construct(ServiceOptionRepositoryInterface $servicerepository , CharityCategoryRepositoryInterface $categoryrepository)
    {
        
        $this->servicerepository = $servicerepository;
        $this->categoryrepository = $categoryrepository;

        $this->nation_id = getAuthNationId();

    } // end of construct

    public function index()
    {
        
        if(!checkPermissions(['show_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $services = $this->servicerepository->paginateWhere([['nation_id', $this->nation_id], ['parent_id', null]]);

        return view('admin.services.index' , compact('services')); 

    } // end of create    

    public function create()
    {

        if(!checkPermissions(['add_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }  

        $categories = $this->categoryrepository->getWhere([['nation_id', $this->nation_id]]);

        return view('admin.services.create' , compact('categories')); 

    } // end of create


    public function store(ServiceOptionRequest $request)
    {
        if(!checkPermissions(['add_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        } 

        $attribute = $request->except('_token', '_method');

        $attribute['nation_id'] = getAuthNationId();

        $service = $this->servicerepository->create($attribute);

        if($service){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
        
    } // end of store 
    
    public function edit($service)
    {

        if(!checkPermissions(['edit_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }          

        $service_one = $this->servicerepository->getWhere([['id', $service], ['nation_id', $this->nation_id]])->first();

        if(!$service_one){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        $categories = $this->categoryrepository->getWhere([['nation_id', $this->nation_id]]);


        return view('admin.services.edit' , compact('service_one' , 'categories')); 

    } // end of create
    
    public function update(ServiceOptionRequest $request, $id)
    {

        if(!checkPermissions(['edit_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }           

        $attribute = $request->except('_token' , '_method');

        $updated = $this->servicerepository->update($attribute , $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
        
    } // end of update  
    
    public function destroy($service)
    {
        if(!checkPermissions(['delete_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }            
        
        // $this->servicerepository->updateWhere(['parent_id' => null], [['parent_id', $service]]);

        $deleted = $this->servicerepository->delete($service);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
        
    } // end of destroy   
}
