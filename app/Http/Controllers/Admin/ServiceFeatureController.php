<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\ServiceOptionRepositoryInterface;

use App\Http\Requests\Admin\ServiceFeatureRequest;

class ServiceFeatureController extends Controller
{

    private $serviceRepository;
    private $nation_id;
   
    public function __construct(ServiceOptionRepositoryInterface $serviceRepository)
    {
        
        $this->serviceRepository = $serviceRepository;

        $this->nation_id = getAuthNationId();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['show_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['features'] = $this->serviceRepository->paginateWhere([['parent_id', '!=', null]]);

        return view('admin.service-features.index')->with([
            'data' => $data
        ]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['services'] = $this->serviceRepository->getWhere([['nation_id', $this->nation_id], ['parent_id', null]]);

        return view('admin.service-features.create')->with([
            
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceFeatureRequest $request)
    {
        if(!checkPermissions(['add_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        } 

        $attribute = $request->except('_token', '_method');

        $attribute['nation_id'] = $this->nation_id;

        $feature = $this->serviceRepository->create($attribute);

        if($feature){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['feature'] = $this->serviceRepository->findOne($id);

        $data['services'] = $this->serviceRepository->getWhere([['nation_id', $this->nation_id], ['parent_id', null]]);

        return view('admin.service-features.edit')->with([
            
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except(['_token', '_method']);

        $updated = $this->serviceRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_service'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }            

        $deleted = $this->serviceRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
