<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\SponserRepositoryInterface;
use App\Repositories\WorkFieldRepositoryInterface;
use App\Http\Requests\Admin\SponserRequest;
use Illuminate\Http\Request;

class SponserController extends Controller
{
    private $workFieldRepository;
    private $sponserRepository;

    public function __construct(WorkFieldRepositoryInterface $workFieldRepository,
                                SponserRepositoryInterface $sponserRepository){

        $this->workFieldRepository = $workFieldRepository;
        $this->sponserRepository = $sponserRepository;
    }

    public function create($project_id){

        if(!checkPermissions(['edit_project_data'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['work_fields'] = $this->workFieldRepository->getWhere([['nation_id', $nation_id]]);
        $data['project_id'] = $project_id;

        return view('admin.sponsers.create')->with([
            'data' => $data
        ]);
    }

    public function store(SponserRequest $request){

        if(!checkPermissions(['edit_project_data'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['project_id'] = $request->project_id;
        $data['name_ar'] = $request->sponser_name_ar;
        $data['name_en'] = $request->sponser_name_en;
        $data['work_field_id'] = $request->sponser_work_field_id;

        $sponser = $this->sponserRepository->create($data);

        if($sponser){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->route('admin.projects.edit', $request->project_id);
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->route('admin.projects.edit', $request->project_id);
        }

    }

    public function update(SponserRequest $request, $id){

        if(!checkPermissions(['edit_project_data'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['project_id'] = $request->project_id;
        $data['name_ar'] = $request->sponser_name_ar;
        $data['name_en'] = $request->sponser_name_en;
        $data['work_field_id'] = $request->sponser_work_field_id;

        $updated = $this->sponserRepository->update($data, $id);

        $sponser = $this->sponserRepository->findOne($id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->route('admin.projects.edit', $sponser->project_id);
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->route('admin.projects.edit', $request->project_id);
        }
    }

    public function destroy($id){

        if(!checkPermissions(['edit_project_data'])){

            return response()->json(['data' => 2], 200);
        }

        // delete sponser
        $deleted = $this->sponserRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }


    }
}
