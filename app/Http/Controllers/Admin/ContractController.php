<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ContractRequest;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\ContractRepositoryInterface;
use Illuminate\Http\Request;

class ContractController extends Controller
{
    
    public $contractrepository;
    public $cityrepository;
   
    public function __construct(ContractRepositoryInterface $contractrepository , CityRepositoryInterface $cityrepository)
    {
        
        $this->contractrepository = $contractrepository;
        $this->cityrepository     = $cityrepository;

        $this->middleware('can:add_contract',    ['only' => ['create','store']]);
        $this->middleware('can:edit_contract',   ['only' => ['edit','update']]);
        $this->middleware('can:delete_contract', ['only' => ['destroy']]);
        $this->middleware('can:show_contract',   ['only' => ['index']]);               

    } // end of construct

    public function index()
    {

        $nation_id = getAuthNationId();

        $contracts = $this->contractrepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.contracts.index' , compact('contracts')); 

    } // end of create    

    public function create()
    {

        return view('admin.contracts.create'); 

    } // end of create


    public function store(ContractRequest $request)
    {

        $attribute = $request->except('_token', '_method');

        $attribute['nation_id'] = auth()->user()->nation_id;

        $this->contractrepository->create($attribute);

        return response()->json(['data' => 1], 200);
        
    } // end of store 
    
    public function edit($contract)
    {
        $nation_id = getAuthNationId();

        $contract_one = $this->contractrepository->getWhere([['id', $contract], ['nation_id', $nation_id]])->first();

        if(!$contract_one){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.contracts.edit' , compact('contract_one')); 

    } // end of create
    
    public function update(ContractRequest $request)
    {
       
        $attribute = $request->except('_token' , '_method' , 'item');

        $this->contractrepository->update($attribute , $request->item);

        return response()->json(['data' => 1], 200);
        
    } // end of update  
    
    public function destroy($contract)
    {

        $this->contractrepository->delete($contract);

        return response()->json(['data' => 1], 200);
        
    } // end of destroy      

} // end of class
