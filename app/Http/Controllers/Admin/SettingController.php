<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\SettingRequest;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Servicies\UploadFilesServices;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    

    public $settingrepository;
    public $fileservice;
    public $currencyrepository;
   
    public function __construct(SettingRepositoryInterface $settingrepository , UploadFilesServices $fileservice,
                                CurrencyRepositoryInterface $currencyrepository)
    {
        
        $this->settingrepository = $settingrepository;
        $this->fileservice       = $fileservice;
        $this->currencyrepository = $currencyrepository;

    } // end of construct

    public function edit()
    {
        
        if(!checkPermissions(['edit_setting'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $settings = $this->settingrepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'asc']);
        $currencies = $this->currencyrepository->getWhere([['nation_id', $nation_id]]);

        if($nation_id == 1){

            return view('admin.settings.update-sa' , compact('settings', 'currencies', 'nation_id'));   
        } 
        else{

            return view('admin.settings.update-uk   ' , compact('settings', 'currencies', 'nation_id'));
        }
        
    } // end of getSASetting

    public function update(SettingRequest $request)
    {

        if(!checkPermissions(['edit_setting'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }          

        $nation_id = getAuthNationId();

        if($nation_id == 1){

            $attribute = $request->except('_token' , 'we_help_logo_sa_ar' , 'we_help_logo_sa_en' , 'stamp_sa' , 'logo_sa_ar' ,'logo_sa_en');


            // uploade file we_help_logo_sa_ar
            if ($request->hasFile('we_help_logo_sa_ar')) {

                $img = $request->file('we_help_logo_sa_ar');

                $we_help_logo_sa_ar = $this->fileservice->uploadfile($img, 'setting');

                $attribute['we_help_logo_sa_ar'] = $we_help_logo_sa_ar;

            } // end of if image 
            
            // uploade file we_help_logo_sa_en
            if ($request->hasFile('we_help_logo_sa_en')) {

                $img = $request->file('we_help_logo_sa_en');

                $we_help_logo_sa_en = $this->fileservice->uploadfile($img, 'setting');

                $attribute['we_help_logo_sa_en'] = $we_help_logo_sa_en;

            } // end of if image

            // uploade file logo_sa_ar
            if ($request->hasFile('logo_sa_ar')) {

                $img = $request->file('logo_sa_ar');

                $logo_sa_ar = $this->fileservice->uploadfile($img, 'setting');

                $attribute['logo_sa_ar'] = $logo_sa_ar;

            } // end of if image 
            
            // uploade file logo_sa_en
            if ($request->hasFile('logo_sa_en')) {

                $img = $request->file('logo_sa_en');

                $logo_sa_en = $this->fileservice->uploadfile($img, 'setting');

                $attribute['logo_sa_en'] = $logo_sa_en;

            } // end of if image            

            // uploade file stamp_sa
            if ($request->hasFile('stamp_sa')) {

                $img = $request->file('stamp_sa');

                $stamp_sa = $this->fileservice->uploadfile($img, 'setting');

                $attribute['stamp_sa'] = $stamp_sa;

            } // end of if image 

            // uploade file pages_header_sa
            if ($request->hasFile('pages_header_sa')) {

                $img = $request->file('pages_header_sa');

                $pages_header_sa = $this->fileservice->uploadfile($img, 'setting');

                $attribute['pages_header_sa'] = $pages_header_sa;

            } // end of if image 
            
            $attribute['phone_sa'] = $request->phone_sa_key . $request->phone_sa;

            $attribute['enable_svg_logo_sa'] = $request->has('enable_svg_logo_sa') ? 1 : 0;
        }
        else{

            $attribute = $request->except('_token' , 'we_help_logo_uk_ar' , 'we_help_logo_uk_en' , 'stamp_uk' , 'logo_uk_en' , 'logo_uk_ar');

            // uploade file we_help_logo_uk_ar
            if ($request->hasFile('we_help_logo_uk_ar')) {
    
                $img = $request->file('we_help_logo_uk_ar');
    
                $we_help_logo_uk_ar = $this->fileservice->uploadfile($img, 'project');
    
                $attribute['we_help_logo_uk_ar'] = $we_help_logo_uk_ar;
    
            } // end of if image 
            
            // uploade file we_help_logo_uk_en
            if ($request->hasFile('we_help_logo_uk_en')) {
    
                $img = $request->file('we_help_logo_uk_en');
    
                $we_help_logo_uk_en = $this->fileservice->uploadfile($img, 'project');
    
                $attribute['we_help_logo_uk_en'] = $we_help_logo_uk_en;
    
            } // end of if image
    
            // uploade file logo_sa_ar
            if ($request->hasFile('logo_uk_ar')) {
    
                $img = $request->file('logo_uk_ar');
    
                $logo_uk_ar = $this->fileservice->uploadfile($img, 'setting');
    
                $attribute['logo_uk_ar'] = $logo_uk_ar;
    
            } // end of if image 
            
            // uploade file logo_uk_en
            if ($request->hasFile('logo_uk_en')) {
    
                $img = $request->file('logo_uk_en');
    
                $logo_uk_en = $this->fileservice->uploadfile($img, 'setting');
    
                $attribute['logo_uk_en'] = $logo_uk_en;
    
            } // end of if image             
    
            // uploade file stamp_uk
            if ($request->hasFile('stamp_uk')) {
    
                $img = $request->file('stamp_uk');
    
                $stamp_uk = $this->fileservice->uploadfile($img, 'project');
    
                $attribute['stamp_uk'] = $stamp_uk;
    
            } // end of if image    
            
            // uploade file pages_header_uk
            if ($request->hasFile('pages_header_uk')) {

                $img = $request->file('pages_header_uk');

                $pages_header_uk = $this->fileservice->uploadfile($img, 'setting');

                $attribute['pages_header_uk'] = $pages_header_uk;

            } // end of if image 
            
            $attribute['phone_uk'] = $request->phone_uk_key . $request->phone_uk;   

            $attribute['enable_svg_logo_uk'] = $request->has('enable_svg_logo_uk') ? 1 : 0;
        }
        
        $this->settingrepository->updateSetting($attribute);

        return redirect()->back()->with('success' , __('admin.update_setting_success'));
        
    } // end of storeSASetting  


}
