<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CompaignRequest;
use App\Repositories\CampaignGoalRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use Illuminate\Http\Request;

class CampaignGoalController extends Controller
{

    public $campaigngoalrepository;
    public $cityrepository;

    public function __construct(CampaignGoalRepositoryInterface $campaigngoalrepository , CityRepositoryInterface $cityrepository)
    {
        
        $this->campaigngoalrepository = $campaigngoalrepository;
        $this->cityrepository         = $cityrepository;

        $this->middleware('can:create_campaign_goal', ['only' => ['create','store']]);
        $this->middleware('can:edit_campaign_goal', ['only' => ['edit','update']]);
        $this->middleware('can:delete_campaign_goal', ['only' => ['destroy']]);
        $this->middleware('can:show_campaign_goal', ['only' => ['index']]);          

    } // end of construct

    public function index()
    {

        $nation_id = getAuthNationId();

        $campaigns = $this->campaigngoalrepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.campaigns.index' , compact('campaigns'));

    } // end of index

    public function create()
    {
        $nation_id = getAuthNationId();
        
        return view('admin.campaigns.create');

    } // end of create

    public function store(CompaignRequest $request)
    {

        $attribute   = $request->except('_token', '_method');

        $attribute['nation_id'] = auth()->user()->nation_id;

        $this->campaigngoalrepository->create($attribute);

        return redirect()->route('admin.campaigns.index')->with('success' , __('admin.add_compaign_succcess'));

    } // end of store

    public function edit($compaign)
    {
        $nation_id = getAuthNationId();

        $campaign_one  = $this->campaigngoalrepository->getWhere([['id', $compaign], ['nation_id', $nation_id]])->first();

        if(!$campaign_one){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        } 

        return view('admin.campaigns.edit' , compact('campaign_one'));

    } // end of edit   
     
    public function update(CompaignRequest $request , $comp)
    {

        $attribute  = $request->except('_token' ,'_method');

        $this->campaigngoalrepository->update($attribute , $comp);
            
        return redirect()->route('admin.campaigns.index')->with('success' , __('admin.update_compaign_succcess'));

    } // end of update 
    
    public function destroy($comp)
    {
         
        $this->campaigngoalrepository->delete($comp);

        return response()->json(['data' => 1], 200);
        
    } // end of destroy 
}
