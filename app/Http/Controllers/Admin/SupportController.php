<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\FrozenSupportRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\FinancialRequestRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use Str;

use Excel;
use App\Exports\SuspendedSupportsSupporters;

use App\Http\Requests\Admin\VIPSupportRequest;

use Monarobase\CountryList\CountryListFacade;

class SupportController extends Controller
{

    private $supportRepository;
    private $cityRepository;
    private $frozenSupportRepository;
    private $projectRepository;
    private $currencyRepository;
    private $userRepository;
    private $financialRequestRepository;
    private $notificationRepository;

    private $nation_id;

    public function __construct(SupportRepositoryInterface $supportRepository,
                                CityRepositoryInterface $cityRepository,
                                FrozenSupportRepositoryInterface $frozenSupportRepository,
                                ProjectRepositoryInterface $projectRepository,
                                CurrencyRepositoryInterface $currencyRepository,
                                UserRepositoryInterface $userRepository,
                                FinancialRequestRepositoryInterface $financialRequestRepository,
                                NotificationRepositoryInterface $notificationRepository){

        $this->supportRepository = $supportRepository;
        $this->cityRepository = $cityRepository;
        $this->frozenSupportRepository = $frozenSupportRepository;
        $this->projectRepository = $projectRepository;
        $this->currencyRepository = $currencyRepository;
        $this->userRepository = $userRepository;
        $this->financialRequestRepository = $financialRequestRepository;
        $this->notificationRepository = $notificationRepository;

        $this->nation_id = getAuthNationId();
    }

    public function index(){

        if(!checkPermissions(['list_supports'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['supports'] = $this->supportRepository->paginateWhereWith([['status', 'support'], ['nation_id', $this->nation_id]], ['user', 'project', 'project.charity', 'currency', 'nation']);

        return view('admin.supports.index')->with([
            'data' => $data
        ]);

    }

    public function getSuspendedSupports(){

        if(!checkPermissions(['list_edit_suspended_supports'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['supports'] = $this->frozenSupportRepository->paginateWhereHasWith(['fromProject', 'fromProject.charity'], 'fromProject', 
                            [['nation_id', $this->nation_id]], 10, [['to_project_id', null]], ['column' => 'id', 'dir' => 'DESC']);

        $data['projects'] = $this->projectRepository->getWhere([['profile', 0], ['active', 1], ['donation_complete', 0], ['nation_id', $this->nation_id]]);


        return view('admin.supports.frozen-supports')->with([
            'data' => $data
        ]);
        
    }

    public function store(Request $request, $id){

        $this->validate($request, ['to_project_id' => 'required', 'cost' => 'required|numeric']);

        $frozen = $this->frozenSupportRepository->findOne($id);

        $frozen_cost = exchange($frozen->cost, $frozen->currency_id, session('currency'));

        if($request->cost > $frozen_cost){

            session()->flash('error', trans('admin.invalid_cost'));
            
            return redirect()->back();
        }

        $request_currency = currencySymbol(session('currency'));

        $project = $this->projectRepository->findWith($request->to_project_id, ['currency']);

        $project_total_donates = $project->total_supports_for_project;

        $remaning_donates = generalExchange($project->get_total, $project->currency->symbol, $request_currency) - $project_total_donates;

        if($remaning_donates <= 0){

            session()->flash('error', trans('admin.project_not_deserve'));
            
            return redirect()->back();
        }

        if($request->cost <= $remaning_donates){


            $data['user_id'] = auth()->user()->id;
            $data['project_id'] = $request->to_project_id;
            $data['status'] = 'support';
            $data['cost'] = $request->cost;
            $data['cost_gbp'] = generalExchange($request->cost, $request_currency, 'GBP');
            $data['type'] = 'once';
            $data['name_anonymouse'] = 'Amount Transferred by Adminstration';
            $data['nation_id'] = getNationId();
            $data['currency_id'] = session('currency');

            $support = $this->supportRepository->create($data);

            if($support){

                if($request->cost <= $frozen_cost){

                    $remainig_frozen = $frozen_cost - $request->cost;

                    $this->frozenSupportRepository->update(['cost' => $remainig_frozen], $frozen->id);

                    $frozen_data['slug'] = \Str::random(12);
                    $frozen_data['from_project_id'] = $frozen->from_project_id;
                    $frozen_data['prev_donations'] = $frozen->prev_donations;
                    $frozen_data['out_to_charity'] = $frozen->out_to_charity;
                    $frozen_data['to_project_id'] = $request->to_project_id;
                    $frozen_data['cost'] = $remainig_frozen;
                    $frozen_data['to_project_cost'] = $request->cost;
                    $frozen_data['currency_id'] = $frozen->currency_id;
                    $frozen_data['supporters_ids'] = $frozen->supporters_ids;

                    $this->frozenSupportRepository->create($frozen_data);

                }
                else{
                    
                    // $this->frozenSupportRepository->delete($id);

                    $remaning_donates = 0;

                    $this->frozenSupportRepository->update(['cost' => $remainig_frozen], $frozen->id);
                }
    
                session()->flash('success', trans('admin.add_success'));
    
                return redirect()->back();
            }
            else{
    
                session()->flash('error', trans('admin.add_error'));
    
                return redirect()->back();
            }

        }
        else{

            session()->flash('error', trans('admin.extra_donate'));
    
            return redirect()->back();

            // $data['user_id'] = auth()->user()->id;
            // $data['project_id'] = $request->project_id;
            // $data['status'] = 'support';
            // $data['cost'] = $remaning_donates;
            // $data['type'] = 'once';
            // $data['name_anonymouse'] = 'Amount Transferred by Adminstration';
            // $data['nation_id'] = getNationId();
            // $data['currency_id'] = $frozen->currency_id;

            // $support = $this->supportRepository->create($data);

            // if($support){

            //     $remaning_donates = $frozen->cost - $remaning_donates;
                
            //     $this->frozenSupportRepository->update(['cost' => $remaning_donates], $id);
    
            //     session()->flash('success', trans('admin.add_success'));
    
            //     return redirect()->back();
            // }
            // else{
    
            //     session()->flash('error', trans('admin.add_error'));
    
            //     return redirect()->back();
            // }

        }
    }

    public function showSuspendedSupports($slug){


        $data['support'] = $this->frozenSupportRepository->getWhereWith(['fromProject', 'fromProject.charity'], 
                            [['slug', $slug]])->first();
        
                            
        $data['support_projects'] = $this->frozenSupportRepository->getWhereWith(['toProject', 'toProject.charity'], 
                            [['from_project_id', $data['support']->from_project_id], ['to_project_id', '!=', null]]);
                            
        $data['session_currency'] = currencySymbol(session('currency'));
                            

        return view('admin.supports.show-frozen-supports')->with([
            'data' => $data
        ]);
    }


    public function getVIPSupport(){

        if(!checkPermissions(['list_exchange_vouchers'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['supports'] = $this->supportRepository->paginateWhereWith([['vip', 1], ['nation_id', $this->nation_id], ['cost', '!=', 0]], ['user', 'project', 'project.charity', 'currency', 'nation']);

        return view('admin.supports.vip-supports')->with([
            'data' => $data
        ]);
    }

    public function createVIPSupport(){

        if(!checkPermissions(['create_finance_voucher'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        $data['currencies'] = $this->currencyRepository->getWhere([['nation_id', $this->nation_id]]);

        $data['projects'] = $this->projectRepository->whereHas('charity', [['status', 'approved']], [['profile', 0], ['nation_id', $this->nation_id] , 
                                ['status', 'approved'], ['active', 1], ['donation_complete', 0]]);

        return view('admin.supports.create-vip-support')->with([

            'data' => $data
        ]);

    }

    public function storeVIPSupport(VIPSupportRequest $request){

        if(!checkPermissions(['create_finance_voucher'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        // check for donation cost
        $project = $this->projectRepository->findWith($request->project_id, ['currency']);

        $project_total_donates = $project->total_supports_for_project;

        $remaning_donates = generalExchange($project->get_total, $project->currency->symbol, currencySymbol(session('currency'))) - $project_total_donates;

        $request_currency = currencySymbol($request->currency_id);

        if(exchange($request->cost, $request->currency_id, session('currency')) > $remaning_donates){

            if($remaning_donates <= 0){

                $response['status'] = 0;
                $response['message'] = trans('admin.donation_complete');
                $response['reload'] = 0;

                return $response;
            }
            else{

                $response['status'] = 0;
                $response['message'] = trans('admin.extra_donate').' '.trans('admin.remainig_amount').' '. $remaning_donates.' '.currencySymbol(session('currency'));
                $response['reload'] = 0;
    
                return $response;
            }
           

            // session()->flash('error', trans('admin.extra_donate'));
            // session()->flash('html', trans('admin.remainig_amount').' '. $remaning_donates.' '.currencySymbol(session('currency')));

            // return redirect()->back();

        }

        $user_data = $request->only(['email', 'phone', 'country', 'currency_id']);

        $user_data['name_ar'] = $request->name;
        $user_data['name_en'] = $request->name;

        $password = Str::random(12);

        $user_data['password'] = bcrypt($password);

        $user_data['blocked'] = 0;
        $user_data['nation_id'] = $this->nation_id;

        $user = $this->userRepository->create($user_data);

        if($user){

            $user->assignRole('supporter');

            $support_data = $request->only(['project_id', 'cost', 'currency_id', 'project_covering']);
            $support_data['user_id'] = $user->id;
            $support_data['status'] = 'support';
            $support_data['vip'] = 1;
            $support_data['type'] = 'once';
            $support_data['name_anonymouse'] = $user->name;
            $support_data['receipt'] = $request->file('receipt')->store('uploads');
            $support_data['nation_id'] = $this->nation_id;
            $support_data['cost_gbp']  = generalExchange($request->cost, $request_currency, 'GBP');

            $support = $this->supportRepository->create($support_data);

            $project = $this->projectRepository->findWith($request->project_id, ['currency']);
            
            if(generalExchange($project->get_total, $project->currency->symbol, currencySymbol(session('currency'))) > getTotalSupportsForProject($project)){

                $project_data['donation_complete'] =  0;
            }
            else{

                $project_data['donation_complete'] =  1;
            }

            $this->projectRepository->update($project_data, $request->project_id);

            if($support){

                // Send voucher to user
                // Create catch voucher - انشاء سند القبض
                $voucher_data['slug'] = Str::random(12);
                $voucher_data['out_to_charity'] = $support->cost;
                $voucher_data['requestable_id'] = $support->user_id;
                $voucher_data['requestable_type'] = 'App\Models\User';
                $voucher_data['project_id'] = $support->project_id;
                $voucher_data['status'] = 'approved';
                $voucher_data['type'] = 'catch';
                $voucher_data['currency_id'] = $support->currency_id;
                $voucher_data['bank_account_num'] = null;
                $voucher_data['voucher_num'] = null;

                $voucher = $this->financialRequestRepository->create($voucher_data);

                // Email Voucher to user
                $this->emailVoucher($voucher, $password);

                // session()->flash('success', trans('admin.add_success'));
    
                // return redirect()->back();

                $response['status'] = 1;
                $response['redirect'] = route('admin.supports.create-vip-support');
                $response['reload'] = 0;

                return $response;
            }
            else{
    
                // session()->flash('error', trans('admin.add_error'));
    
                // return redirect()->back();

                $response['status'] = 0;
                $response['redirect'] = route('admin.supports.create-vip-support');
                $response['reload'] = 0;

                return $response;
            }
        }
        else{

            $response['status'] = 0;
            $response['reload'] = 0;

            return $response;
        }
        
    }

    public function editVIPSupport($id){

        if(!checkPermissions(['edit_finance_voucher'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['support'] = $this->supportRepository->findWith($id, ['user']);

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        $data['currencies'] = $this->currencyRepository->getWhere([['nation_id', $this->nation_id]]);

        $data['projects'] = $this->projectRepository->whereHas('charity', [['status', 'approved']], [['profile', 0], ['nation_id', $this->nation_id] , 
                                ['status', 'approved'], ['active', 1]]);

        return view('admin.supports.edit-vip-support')->with([
            'data' => $data
        ]);

    }

    public function updateVIPSupport(VIPSupportRequest $request, $id){

        if(!checkPermissions(['edit_finance_voucher'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $request_currency = currencySymbol($request->currency_id);

        // delete old cost - علشان احسب اجمالى التبرعات من غير التبرع اللى هيتعدل
        $old_support = $this->supportRepository->findWith($id, ['user']);
        $cost = $old_support->cost;
        $cost_gbp = $old_support->cost_gbp;
        $currency_id = $old_support->currency_id;
        $this->supportRepository->update(['cost' => 0, 'cost_gbp' => 0], $id);

        // check for donation cost
        $project = $this->projectRepository->findOne($request->project_id);

        $project_total_donates = getTotalSupportsForProject($project);

        $remaning_donates = exchange($project->get_total, $project->currency_id, session('currency')) - $project_total_donates;

        if(exchange($request->cost, $request->currency_id, session('currency')) > $remaning_donates){

            // هرجع التبرع زى ماكان
            $this->supportRepository->update(['cost' => $cost, 'currency_id' => $currency_id, 'cost_gbp' => $cost_gbp], $id);

            // session()->flash('error', trans('admin.extra_donate'));
            // session()->flash('html', trans('admin.remainig_amount').' '. $remaning_donates.' '.currencySymbol(session('currency')));

            // return redirect()->back();

            if($remaning_donates <= 0){

                $response['status'] = 0;
                $response['message'] = trans('admin.donation_complete');
                $response['reload'] = 0;

                return $response;
            }
            else{

                $response['status'] = 0;
                $response['message'] = trans('admin.extra_donate').' '.trans('admin.remainig_amount').' '. $remaning_donates.' '.currencySymbol(session('currency'));
                $response['reload'] = 0;
    
                return $response;
            }

        }

        $user_data = $request->only(['email', 'phone', 'country', 'currency_id']);

        $user_data['name_ar'] = $request->name;
        $user_data['name_en'] = $request->name;

        $user_data['blocked'] = 0;
        $user_data['nation_id'] = $this->nation_id;

        $password = Str::random(12);

        if($old_support->user){

            $user = $this->userRepository->update($user_data, $old_support->user->id);
            $user = $this->userRepository->findOne($old_support->user->id);
        }
        else{

            $user_data['password'] = bcrypt($password); 

            $user = $this->userRepository->create($user_data);
        }

        if($user){

            $user->assignRole('supporter');
            
            $support_data = $request->only(['project_id', 'cost', 'currency_id', 'project_covering']);
            $support_data['user_id'] = $user->id;
            $support_data['status'] = 'support';
            $support_data['vip'] = 1;
            $support_data['type'] = 'once';
            $support_data['name_anonymouse'] = $user->name;
            $support_data['cost_gbp']  = generalExchange($request->cost, $request_currency , 'GBP');

            if($request->has('receipt')){

                $support_data['receipt'] = $request->file('receipt')->store('uploads');
            }
            
            $support_data['nation_id'] = $this->nation_id;

            $support = $this->supportRepository->update($support_data, $id);
            $support = $this->supportRepository->findWith($id, ['user']);

            if($support){

                // Send voucher to user
                // Create catch voucher - انشاء سند القبض
                $voucher_data['slug'] = Str::random(12);
                $voucher_data['out_to_charity'] = $support->cost;
                $voucher_data['requestable_id'] = $support->user_id;
                $voucher_data['requestable_type'] = 'App\Models\User';
                $voucher_data['project_id'] = $support->project_id;
                $voucher_data['status'] = 'approved';
                $voucher_data['type'] = 'catch';
                $voucher_data['currency_id'] = $support->currency_id;
                $voucher_data['bank_account_num'] = null;
                $voucher_data['voucher_num'] = null;

                $voucher = $this->financialRequestRepository->create($voucher_data);

                // Email Voucher to user
                $this->emailVoucher($voucher, $password);

                // session()->flash('success', trans('admin.update_success'));
    
                // return redirect()->back();

                $response['status'] = 1;
                $response['redirect'] = route('admin.supports.edit-vip-support', $id);
                $response['reload'] = 0;
    
                return $response;
            }
            else{
    
                $response['status'] = 0;
                $response['redirect'] = route('admin.supports.edit-vip-support', $id);
                $response['reload'] = 0;
    
                return $response;
            }
        }
        else{

            $response['status'] = 0;
            $response['reload'] = 0;

            return $response;
        }
    }

    public function emailVoucher($voucher, $password){

        $email = [['email' => $voucher->requestable->email]];
        $topic = trans('admin.support_voucher');
        $nation_id = $voucher->requestable->nation_id;
        $notes = $voucher->notes;
        $msg = '<pr>'.trans('admin.vip_client_message').'<br>'.
                    '<strong>'.trans('admin.email').'</strong>: '.$voucher->requestable->email.'<br>'.
                    '<strong>'.trans('admin.password').'</strong>: '.$password.'<br>'.
                    trans('admin.attach_text').
               '</p>';

        $notify_ar = 'فاتورة تبرع';
        $notify_en = 'Support Voucher';

        // create exchange voucher link - ارسال طلب الصرف - فاتوره بالمبلغ اللى هيتم تحويله للجمعيه
        $voucher_link = '<a href="'.route('get-voucher', $voucher->slug).'">'.trans('admin.show').'</a>';

        // notification 
        $message = new \stdClass();
        $message->message_ar = $notify_ar;
        $message->message_en = $notify_en;

        $notification_data['message_en'] = $message->message_en;
        $notification_data['message_ar'] = $message->message_ar;
        $notification_data['send_user_id'] = $voucher->requestable->id;
        $notification_data['title'] = 'financial_request';
        $notification_data['type'] = 'user';

        $this->notificationRepository->create($notification_data); 
        
        $users = [];
        array_push($users, $voucher->requestable->id);

        // Notify Supporters via mobile app that monthly deduction for this project transfered
        notifyMobile($voucher->id, $users, 'new_voucher', 'supporter',  'فاتورة تبرع الخاص بك', 'Your donation voucher', asset('web/images/main/user_avatar.png'));

        // send email
        try {
    
            sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

        } catch (Throwable $e) {

            return response()->json(['data' => 'exception'], 200);

        }  
    }

    public function exportSuspendedSupportsSupporters(){

        return Excel::download(new SuspendedSupportsSupporters($this->frozenSupportRepository), 'supporters.xlsx');
    }
}
