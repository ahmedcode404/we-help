<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\ServiceOptionRepositoryInterface;
use App\Repositories\WorkFieldRepositoryInterface;
use App\Http\Requests\Admin\ProjectRequest;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use Storage;

use Monarobase\CountryList\CountryListFacade;

class ProjectController extends Controller
{
    private $cityRepository;
    private $projectRepository;
    private $phaseRepository;
    private $reportRepository;
    private $currencyRepository;
    private $projectServiceRepository;
    private $workFieldRepository;
    private $logRepository;

    public function __construct(CityRepositoryInterface $cityRepository,
                                ProjectRepositoryInterface $projectRepository,
                                PhaseRepositoryInterface $phaseRepository,
                                ReportRepositoryInterface $reportRepository,
                                CurrencyRepositoryInterface $currencyRepository,
                                ServiceOptionRepositoryInterface $projectServiceRepository,
                                LogRepositoryInterface $logRepository,
                                WorkFieldRepositoryInterface $workFieldRepository
                                ){

        $this->cityRepository           = $cityRepository;
        $this->projectRepository        = $projectRepository;
        $this->phaseRepository          = $phaseRepository;
        $this->reportRepository         = $reportRepository;
        $this->currencyRepository       = $currencyRepository;
        $this->projectServiceRepository = $projectServiceRepository;
        $this->workFieldRepository      = $workFieldRepository;
        $this->logRepository            = $logRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_projects'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();


        $data['projects'] = $this->projectRepository->paginateWhereWith([['active', 1], ['profile', 0], ['nation_id', $nation_id]], ['nation', 'currency', 'category']);
        // $data['projects'] = $this->projectRepository->paginateWhereHasWith(['nation', 'currency', 'category'], 'charity', [['status', 'approved']], 10, [['profile', 0], ['nation_id', $nation_id]]);

        return view('admin.projects.index')->with([
            'data' => $data
        ]);
    }
 
    // Get project of a specific status
    public function getProjectWithStatus($status){

        if(!checkPermissions(['list_projects'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        if($status == 'trashed'){

            $data['projects'] = $this->projectRepository->paginateWhereWith([['active', 0], ['profile', 0], ['nation_id', $nation_id]], ['nation', 'currency', 'category']);
            // $data['projects'] = $this->projectRepository->paginateWhereHasWith(['nation', 'currency', 'category'], 'charity', [['status', 'approved']], 10, [['profile', 0], ['nation_id', $nation_id]]);
        }
        else{

            $data['projects'] = $this->projectRepository->paginateWhereWith([['active', 1], ['profile', 0], ['status', $status], ['nation_id', $nation_id]], ['nation', 'currency', 'category']);
        }


        return view('admin.projects.index')->with([
            'data' => $data
        ]);
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['project'] = $this->projectRepository->getWhereWith(['category', 'reports', 'supports', 'nation', 'currency', 'phases', 
                            'charity', 'country', 'country.cities', 'city', 'images', 'edu_services', 'emerg_services', 'sponsers', 'sponsers.work_field', 'edit_requests'],
                            [['id', $id], ['nation_id', $nation_id]])->first();
        
        if(!$data['project']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.projects.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_project_data'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['project'] = $this->projectRepository->getWhereWith(['category', 'reports', 'supports', 'nation', 'currency', 'phases', 
                            'charity', 'country', 'city', 'images', 'edu_services', 'emerg_services', 'sponsers', 'sponsers.work_field', 'edit_requests'],
                            [['id', $id], ['nation_id', $nation_id]])->first();
                                                
        if(!$data['project']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }
        
        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        $data['currencies'] = $this->currencyRepository->getwhere([['nation_id', $nation_id]]);

        $data['services'] = $this->projectServiceRepository->getWhere([['nation_id', $nation_id]]);

        $data['work_fields'] = $this->workFieldRepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.projects.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        if(!checkPermissions(['edit_project_data'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }      

        $updated = $this->projectRepository->adminUpdateProject($request, $id);

        if($updated){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'project';
            $log_data['edit_type'] = 'Edit project data';

            $this->logRepository->create($log_data);

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }
        
        $response = $this->projectRepository->adminDeleteProject($id);

        return response()->json(['data' => $response], 200);

    }

    // restore project - change active to 1
    public function restore($id){

        $updated = $this->projectRepository->update(['active' => 1], $id);

        if($updated){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'project';
            $log_data['edit_type'] = 'Restore project';

            $this->logRepository->create($log_data);

            session()->flash('success', trans('admin.activated'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.error'));

            return redirect()->back();
        }

    }

}
