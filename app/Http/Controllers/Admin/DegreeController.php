<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\DegreeRepositoryInterface;
use App\Repositories\UserRepositoryInterface;

class DegreeController extends Controller
{

    private $degreeRepository;
    private $userRepository;

    public function __construct(DegreeRepositoryInterface $degreeRepository,
                                UserRepositoryInterface $userRepository){

        $this->degreeRepository = $degreeRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_degrees'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['degrees'] = $this->degreeRepository->paginateWhere([['nation_id', $nation_id], ['guard', 'admin']]);

        return view('admin.degrees.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_degrees'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.degrees.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!checkPermissions(['create_degrees'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $data['nation_id'] = auth()->user()->nation_id;
        $data['guard'] = 'admin';
        $data['slug'] = \Str::random(12);
        
        $degree = $this->degreeRepository->create($data);

        if($degree){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_degrees'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['degree'] = $this->degreeRepository->getWhereWith(['users'], [['id', $id], ['nation_id', $nation_id], ['guard', 'admin']])->first();

        if(!$data['degree']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.degrees.show')->with([
            'data' => $data
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_degrees'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['degree'] = $this->degreeRepository->getWhere([['id', $id], ['nation_id', $nation_id], ['guard', 'admin']])->first();

        if(!$data['degree']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.degrees.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!checkPermissions(['edit_degrees'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $updated = $this->degreeRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_degrees'])){

            return response()->json(['data' => 2], 200);
        }

        // update related users to have degree_id = null
        $this->userRepository->updateWhere(['degree_id' => null], [['degree_id', $id]]);

        // delete degree
        $deleted = $this->degreeRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
