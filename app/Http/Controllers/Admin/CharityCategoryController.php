<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Http\Requests\Admin\CharityCategoryRequest;

use Storage;
use Str;

class CharityCategoryController extends Controller
{
    private $charityCategoryRepository;
    private $cityRepository;
    private $charityRepository;
    private $projectRepository;

    public function __construct(CharityCategoryRepositoryInterface $charityCategoryRepository,
                                CityRepositoryInterface $cityRepository,
                                CharityRepositoryInterface $charityRepository,
                                ProjectRepositoryInterface $projectRepository){

        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->cityRepository = $cityRepository;
        $this->charityRepository = $charityRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(!checkPermissions(['list_charity_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['charity-categories'] = $this->charityCategoryRepository->paginateWhere([['nation_id', $nation_id]]);

        return view('admin.charity-categories.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_charity_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.charity-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CharityCategoryRequest $request)
    {
        if(!checkPermissions(['create_charity_cats'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $data = $request->except(['_token', '_method']);

        $data['icon'] = $request->file('icon')->store('uploads');
        $data['nation_id'] = auth()->user()->nation_id;
        $data['slug'] = Str::random(12);
        
        $cat = $this->charityCategoryRepository->create($data);

        if($cat){

            $response['status'] = 1;
            $response['redirect'] = route('admin.charity-categories.create');
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.charity-categories.create');
            $response['reload'] = 0;

            return $response;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_charity_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cat'] = $this->charityCategoryRepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['cat']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
        }

        $data['charities'] = $data['cat']->charities()->where('nation_id', $nation_id)->paginate(10);

        return view('admin.charity-categories.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_charity_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cat'] = $this->charityCategoryRepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['cat']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
        }

        return view('admin.charity-categories.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CharityCategoryRequest $request, $id)
    {
        if(!checkPermissions(['edit_charity_cats'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $cat = $this->charityCategoryRepository->findOne($id);

        $data = $request->except(['_token', '_method']);

        // Delete old icon
        Storage::delete($cat->icon);

        if($request->has('icon')){

            $data['icon'] = $request->file('icon')->store('uploads'); 
        }

        $updated = $this->charityCategoryRepository->update($data, $id);

        if($updated){

            $response['status'] = 1;
            $response['redirect'] = route('admin.charity-categories.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.charity-categories.edit', $id);
            $response['reload'] = 0;

            return $response;
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_charity_cats'])){

            return response()->json(['data' => 2], 200);
        }

        // detach charities
        $cat = $this->charityCategoryRepository->findWith($id, ['charities']);

        foreach($cat->charities as $charity){

            $charity->categories()->detach($id);
        }

        // update related projects to have charity_category_id = null
        $this->projectRepository->updateWhere(['charity_category_id' => null], [['charity_category_id', $id]]);

        // delete work field
        $deleted = $this->charityCategoryRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
