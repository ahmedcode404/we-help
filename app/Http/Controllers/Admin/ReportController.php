<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ReportRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Http\Requests\Admin\ReportRequest;
use App\Models\Notification;
use App\Servicies\Notify;
use App\Models\Setting;

use Storage;

class ReportController extends Controller
{
    private $reportRepository;
    private $projectRepository;

    public function __construct(ReportRepositoryInterface $reportRepository,
                                ProjectRepositoryInterface $projectRepository){

        $this->reportRepository = $reportRepository;
        $this->projectRepository = $projectRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_reports'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['reports'] = $this->reportRepository->paginateWhereHasWith(['images', 'phase', 'project', 'charity'], 'project', [['nation_id', $nation_id]], 
                            10, [], ['column' => 'id', 'dir' => 'DESC']);

        return view('admin.reports.index')->with([
            'data' => $data
        ]);

    }

    // Get report of a specific status
    public function getReportWithStatus($status){

        if(!checkPermissions(['list_reports'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['reports'] = $this->reportRepository->paginateWhereHasWith(['images', 'phase', 'project', 'charity'], 'project', [['nation_id', $nation_id]], 10, [['status', $status]]);

        return view('admin.reports.index')->with([
            'data' => $data
        ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_report'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }        
        
        $nation_id = getAuthNationId();

        $data['report'] = $this->reportRepository->paginateWhereHasWith(['charity', 'project', 'phase', 'images'], 'project', [['nation_id', $nation_id]], 10, [['id', $id]])->first();

        if(!$data['report']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.reports.show')->with([
            'data' => $data
        ]);
    }

    public function createReport($project_id){

        if(!checkPermissions(['create_report'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.reports.create', compact('project_id'));

    }
    
    public function storeReport(ReportRequest $request, $project_id){

        if(!checkPermissions(['create_report'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $data = $request->except(['_token', '_method', 'video', 'images']);

        if($request->has('vedio')){

            try{

                // Upload new vedio
                $data['vedio'] = $request->file('vedio')->store('uploads/reports/vedios');
            }
            catch(Throwable $e){
                dd('Error uploading video');
            }
            
            
        }

        $project = $this->projectRepository->findOne($project_id);

        $data['project_id'] = $project_id;
        $data['charity_id'] = $project->charity_id;
        $data['slug'] = \Str::random(12);

        $report = $this->reportRepository->create($data);

        if($request->has('images')){

            foreach($request->images as $image){

                $report->images()->create([
                    'imageable_id' => $report->id,
                    'imageable_type' => 'App\Models\Report',
                    'path' => $image->store('uploads/report/images')
                ]);
            }
        }

        if($report){

            $response['status'] = 1;
            $response['redirect'] = route('admin.reports.create-report', $project_id);
            $response['reload'] = 0;

        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.reports.create-report', $project_id);
            $response['reload'] = 0;

        }

        return $response;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_report'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }  

        $nation_id = getAuthNationId();

        $data['report'] = $this->reportRepository->paginateWhereHasWith(['images'], 'project', [['nation_id', $nation_id]], 10, [['id', $id]])->first();

        if(!$data['report']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.reports.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ReportRequest $request, $id)
    {
        if(!checkPermissions(['edit_report'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }  

        $report = $this->reportRepository->findWith($id, ['project', 'project.supports', 'project.charity']);

        $report_status = $report->status;

        if($report_status !='approved' && $request->status == 'approved'){

            // Send Email to charity and to project supporters
            $email = [];

            foreach($report->project->supports as $support){

                array_push($email, ['email' => $support->email]);
            }

            $voucher_link = '';
            array_push($email, ['email' => $report->project->charity->email]);
            $topic = trans('admin.change_status_topic');
            $nation_id = $report->project->nation_id;
            $msg = getSettingMsg('report_approve_msg', $nation_id)->value;
            $notes = $report->notes;

            $notify_ar = Setting::where('key' , 'report_approve_msg_sa_ar')->first();
            $notify_en = Setting::where('key' , 'report_approve_msg_sa_en')->first();

            try {
    
                sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

            } catch (Throwable $e) {

                return response()->json(['data' => 'exception'], 200);

            }  

            // notification 
            $message = new \stdClass();
            $message->message_ar = $notify_ar->value;
            $message->message_en = $notify_en->value;
    
            Notification::create([ 
    
                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $report->project->charity->id,
                'model_id'       => $report->project->id,
                'title'          => 'report',
                'type'           => 'charity'
            ]);             

            Notify::NotifyWeb($message , 'report' , 'charity' , $report->charity->id); 

        }
        else if($report_status !='rejected' && $request->status == 'rejected'){

            $email = [];
            $voucher_link = '';
            array_push($email, ['email' => $report->project->charity->email]);
            $topic = trans('admin.change_status_topic');
            $nation_id = $report->project->nation_id;
            $msg = getSettingMsg('report_reject_msg', $nation_id)->value;
            $notes = $report->notes;

            $notify_ar = Setting::where('key' , 'report_reject_msg_sa_ar')->first();
            $notify_en = Setting::where('key' , 'report_reject_msg_sa_en')->first();

            try {
    
                sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

            } catch (Throwable $e) {

                return response()->json(['data' => 'exception'], 200);

            }  

            // notification 
            $message = new \stdClass();
            $message->message_ar = $notify_ar->value;
            $message->message_en = $notify_en->value;
    
            Notification::create([ 
    
                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $report->project->charity->id,
                'model_id'       => $report->project->id,
                'title'          => 'report',
                'type'           => 'charity'
            ]);             

            Notify::NotifyWeb($message , 'report' , 'charity' , $report->charity->id);
        }

        $updated = $this->reportRepository->adminUpdateReport($request, $id);

        if($updated){

            $response['status'] = 1;
            $response['redirect'] = route('admin.reports.edit', $id);
            $response['reload'] = 0;

        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.reports.edit', $id);
            $response['reload'] = 0;

        }

        return $response;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_report'])){

            return response()->json(['data' => 2], 200);
        }  

        $report = $this->reportRepository->findWith($id, ['images']);

        // delete video
        Storage::delete($report->video);

        // delete images
        foreach($report->images as $image){
            storage::delete($image->path);
        }
        $report->images()->delete();

        // delete report
        $deleted = $this->reportRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
