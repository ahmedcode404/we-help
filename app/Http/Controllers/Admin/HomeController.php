<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\PartenerRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\MarketingRepositoryInterface;

class HomeController extends Controller
{

    private $projectRepository;
    private $partenerRepository;
    private $supportRepository;
    private $cityRepository;
    private $charityRepository;
    private $charityCategoryRepository;
    private $editRequestRepository;
    private $userRepository;
    private $marketingRepository;


    public function __construct(ProjectRepositoryInterface $projectRepository,
                                PartenerRepositoryInterface $partenerRepository,
                                SupportRepositoryInterface $supportRepository,
                                CityRepositoryInterface $cityRepository,
                                CharityRepositoryInterface $charityRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                EditRequestRepositoryInterface $editRequestRepository,
                                UserRepositoryInterface $userRepository,
                                MarketingRepositoryInterface $marketingRepository
                            ){

        $this->projectRepository = $projectRepository;
        $this->partenerRepository = $partenerRepository;
        $this->supportRepository = $supportRepository;
        $this->cityRepository = $cityRepository;
        $this->charityRepository = $charityRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->editRequestRepository = $editRequestRepository;
        $this->userRepository = $userRepository;
        $this->marketingRepository = $marketingRepository;
    }


    public function index(){
        
        $nation_id = getAuthNationId();

        $data['all_projects'] = $this->projectRepository->getWhere([['nation_id', $nation_id], ['profile', 0]])->count();
        $data['active_projects_count'] = $this->projectRepository->getWhere([['nation_id', $nation_id], ['profile', 0], ['active', 1]])->count();
        $data['deleted_projects_count'] = $this->projectRepository->getWhere([['nation_id', $nation_id], ['profile', 0], ['active', 0]])->count();

        $data['parteners_count'] = $this->partenerRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['donation_ammounts'] = $this->supportRepository->totalAmmount([['nation_id', $nation_id]]);
        
        $supports = $this->supportRepository->getWhereWith(['currency'], [['status', 'support'], ['nation_id', $nation_id]]);

        $data['benef_count'] = $this->projectRepository->getBenefCount([['nation_id', $nation_id]]);

        $data['session_currency'] = currencySymbol(session('currency'));

        $data['countries'] = $this->cityRepository->adminCountriesCount($nation_id);

        $data['charities'] = $this->charityRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['edit_requests'] = $this->editRequestRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['supporters'] = $this->userRepository->whereHas('roles', [['name', 'supporter']], [['nation_id', $nation_id]])->count();
        $data['compaigns'] = $this->marketingRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['emps'] = $this->userRepository->whereHas('roles', [['name', 'employee']], [['nation_id', $nation_id]])->count();

        return view('admin.home')->with([
            'data' => $data
        ]);
        
    }
}
