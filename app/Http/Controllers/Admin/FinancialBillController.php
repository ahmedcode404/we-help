<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\FinancialRequestRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\FinancialBillRequest;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;

class FinancialBillController extends Controller
{

    private $financialRequestRepository;
    private $logRepository;
    private $charityepository;

    public function __construct(FinancialRequestRepositoryInterface $financialRequestRepository,
                                LogRepositoryInterface $logRepository,
                                CharityRepositoryInterface $charityRepository
                                ){

        $this->financialRequestRepository = $financialRequestRepository;
        $this->logRepository = $logRepository;
        $this->charityRepository = $charityRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_exchange_vouchers']) && !checkPermissions(['list_catch_vouchers'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null]]);

        return view('admin.financial-bills.index')->with([
            'data' => $data
        ]);
    }

    // get requests by type (exchange - catch)
    public function getBillByType($type){

        if(!checkPermissions(['list_exchange_vouchers']) && !checkPermissions(['list_catch_vouchers'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project', 'currency'], 'requestable', 
                [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['type', $type]],
                ['column' => 'id', 'dir' => 'DESC']);
        
        if($type == 'exchange'){
            return view('admin.financial-bills.exchange-bills')->with([
                'data' => $data
            ]);
        }
        else{
            return view('admin.financial-bills.catch-bills')->with([
                'data' => $data
            ]);
        }
        
    }

    // get all requests by status (all - waiting - approved - rejected - hold)
    public function getAllBillsByStatus($status){

        if(!checkPermissions(['list_exchange_vouchers']) && !checkPermissions(['list_catch_vouchers'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['status', $status]]);
        
        return view('admin.financial-bills.index')->with([
            'data' => $data
        ]);
    }


    // get request by status (all - waiting - approved - rejected - hold)
    public function getBillByStatus($status){

        $nation_id = getAuthNationId();

        if(request()->segment(count(request()->segments()) - 1) == 'financial-bills-exchange'){

            $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', 
                [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['type', 'exchange'], ['status', $status]]);

            return view('admin.financial-bills.exchange-bills')->with([
                'data' => $data
            ]);
        }
        else if(request()->segment(count(request()->segments()) - 1) == 'financial-bills-catch'){
            $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', 
                [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['type', 'catch'], ['status', $status]]);

            return view('admin.financial-bills.catch-bills')->with([
                'data' => $data
            ]);
        }
        

        // if($status == 'all' && $type == 'all'){

        //     // $data['bills'] = $this->financialRequestRepository->paginateWhereWith([['bank_account_num', '!=', null], ['voucher_num', '!=', null]], ['requestable', 'project']);

        //     $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', 
        //                         [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null]]);

        // }
        // else if($status == 'all'){

        //     // $data['bills'] = $this->financialRequestRepository->paginateWhereWith([['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['type', $type]], ['requestable', 'project']);

        //     $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', 
        //                         [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['type', $type]]);
        // }
        // else if($type == 'all'){

        //     // $data['bills'] = $this->financialRequestRepository->paginateWhereWith([['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['status', $status]], ['requestable', 'project']);

        //     $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', 
        //                         [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['status', $status]]);
        // }
        // else{

        //     // $data['bills'] = $this->financialRequestRepository->paginateWhereWith([['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['status', $status], ['type', $type]], ['requestable', 'project']);

        //     $data['bills'] = $this->financialRequestRepository->paginateWhereHasWith(['requestable', 'project'], 'requestable', 
        //                         [['nation_id', $nation_id]], 10, [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['type', $type], ['status', $status]]);
        // }

        return view('admin.financial-bills.index')->with([
            'data' => $data
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_finance_voucher'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['requests'] = $this->financialRequestRepository->whereHasWith(['project'], 'project', [['nation_id', $nation_id]], 
                            [['bank_account_num', null], ['voucher_num', null], ['status', 'approved'], ['type', 'exchange']]);

        return view('admin.financial-bills.create')->with([
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FinancialBillRequest $request)
    {

        if(!checkPermissions(['create_finance_voucher'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $data = $request->except(['_token', '_method', 'request_id']);

        $data['status'] = 'waiting';
        // $data['recieve_status'] = 'paid';

        $updated = $this->financialRequestRepository->update($data, $request->request_id);

        if($updated){

            $bill = $this->financialRequestRepository->findOne($request->request_id);

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $bill->id;
            $log_data['model_type'] = 'finance';
            $log_data['edit_type'] = 'Add financial bill';

            $this->logRepository->create($log_data);

            $response['status'] = 1;
            $response['redirect'] = route('admin.financial-bills.create');
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.financial-bills.create');
            $response['reload'] = 0;

            return $response;
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_finance_voucher'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['bill'] = $this->financialRequestRepository->whereHasWith(['requestable'], 'requestable', [['nation_id', $nation_id]], [['id', $id]])->first();

        if(!$data['bill']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        // $data['requests'] = $this->financialRequestRepository->whereHas('requestable', [['nation_id', $nation_id]], [['bank_account_num', '!=', null], ['voucher_num', '!=', null], ['status', 'approved']]);


        return view('admin.financial-bills.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FinancialBillRequest $request, $id)
    {
        if(!checkPermissions(['edit_finance_voucher'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $data = $request->except(['_token', '_method', 'request_id']);

        $bill = $this->financialRequestRepository->update($data, $request->request_id);

        if($bill){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'finance';
            $log_data['edit_type'] = 'Edit financial bill';

            $this->logRepository->create($log_data);

            $response['status'] = 1;
            $response['redirect'] = route('admin.financial-bills.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.financial-bills.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_finance_voucher'])){

            return response()->json(['data' => 2], 200);
        }

        // الحذف هنا يعنى حذف بيانات البنك و الحواله و اعادة سند الصرف لطلب صرف مره اخرى
        $updated = $this->financialRequestRepository->update(['bank_account_num' => null, 'voucher_num' => null, 'status' => 'approved'], $id);

        if($updated){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'finance';
            $log_data['edit_type'] = 'Delete financial bill';

            $this->logRepository->create($log_data);

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }

    }


    public function getCharityIban(Request $request){

        $charity = $this->charityRepository->findOne($request->charity_id);

        return $charity->iban;

    }

    public function changeRevceiveStatus(Request $request){

        $updated = $this->financialRequestRepository->update(['recieve_status' => $request->recieve_status], $request->id);

        if($updated){

            return response()->json(['data' => 'success'], 200);
        }
        else{

            return response()->json(['data' => 'error'], 200);
        }
    }
}
