<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\PhaseRequest;
use Storage;

class PhaseController extends Controller
{
    private $phaseRepository;
    private $projectRepository;
    private $reportRepository;

    public function __construct(PhaseRepositoryInterface $phaseRepository,
                                ProjectRepositoryInterface $projectRepository,
                                ReportRepositoryInterface $reportRepository){

        $this->phaseRepository = $phaseRepository;
        $this->projectRepository = $projectRepository;
        $this->reportRepository = $reportRepository;
    }

    public function show($id){

        $data['phase'] = $this->phaseRepository->findWith($id, ['project', 'images', 'reports', 'reports']);

        return view('admin.phases.show')->with([
            'data' => $data
        ]);
    }

    public function create($project_id){

        if(!checkPermissions(['edit_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['project'] = $this->projectRepository->findWith($project_id, ['phases']);

        return view('admin.phases.create')->with([
            'data' => $data
        ]);
    }

    public function store(PhaseRequest $request){

        if(!checkPermissions(['edit_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $project = $this->projectRepository->findWith($request->project_id, ['phases', 'currency']);

        $phases_cost = generalExchange($project->phases->sum('cost'), $project->currency->symbol, currencySymbol(session('currency'))) + 
            generalExchange((float)$request->cost, $project->currency->symbol, currencySymbol(session('currency')));

        // get project actual cost without admin ratio
        $project_cost = generalExchange($project->cost, $project->currency->symbol, currencySymbol(session('currency')));

        if($phases_cost > $project_cost){

            session()->flash('error', trans('admin.phases_cost_exceeds_proejct_cost'));

            return redirect()->back();

        }

        if(count($project->phases) == 0){

            $data['start_date'] = $project->start_date;
        }

        if($request->has('approved')){

            $data['approved'] = 1;
        }

        $data['remaining_cost'] = $request->cost;
        $data['slug'] = \Str::random(12);

        $phase = $this->phaseRepository->create($data);

        // update project duration and cost
        $project_data['duration'] = $phase->project->phases->sum('duration');
        // $project_data['cost'] = $phase->project->phases->sum('cost');

        $this->projectRepository->update($project_data, $request->project_id);

        if($phase){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->route('admin.projects.edit', $phase->project_id);
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->route('admin.projects.edit', $phase->project_id);
        }
    }

    public function edit($id){
        
        if(!checkPermissions(['edit_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['phase']  =$this->phaseRepository->findWith($id, ['project']);

        return view('admin.phases.edit')->with([
            'data' => $data
        ]);

    }

    public function update(PhaseRequest $request, $id){

        if(!checkPermissions(['edit_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $updated = $this->phaseRepository->adminUpdatePhase($request, $id);

        if($updated == 'cost_exceeds'){

            session()->flash('error', trans('admin.phases_cost_exceeds_proejct_cost'));

            return redirect()->back();

        }

        if($updated == 'updated'){

            session()->flash('success', trans('admin.update_success'));

        }
        else if($updated == 'date_out_range'){

            session()->flash('error', trans('admin.date_out_range'));
            session()->flash('html', trans('admin.date_out_range_text'));

        }
        else if($updated == 'not-updated'){

            session()->flash('error', trans('admin.add_error'));

        }
        return redirect()->route('admin.projects.edit', $request->project_id);

    }

    public function destroy($id){

        if(!checkPermissions(['edit_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // delete phase
        $deleted = $this->phaseRepository->adminDeletePhase($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 'error'], 200);
        }

    }
}
