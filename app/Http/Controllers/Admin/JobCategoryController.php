<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JobCategoryRepositoryInterface;
use App\Repositories\JobRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Http\Requests\Admin\JobCategoryRequest;

class JobCategoryController extends Controller
{

    private $jobCategoryRepository;
    private $cityRepository;
    private $jobRepository;

    public function __construct(JobCategoryRepositoryInterface $jobCategoryRepository,
                                CityRepositoryInterface $cityRepository,
                                JobRepositoryInterface $jobRepository){

        $this->jobCategoryRepository = $jobCategoryRepository;
        $this->cityRepository = $cityRepository;
        $this->jobRepository = $jobRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(!checkPermissions(['list_job_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['job-categories'] = $this->jobCategoryRepository->paginateWhere([['nation_id', $nation_id], ['guard', 'admin']]);

        return view('admin.job-categories.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_job_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.job-categories.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobCategoryRequest $request)
    {
        if(!checkPermissions(['create_job_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $data['nation_id'] = auth()->user()->nation_id;
        $data['guard'] = 'admin';
        $data['slug'] = \Str::random(12);
        
        $cat = $this->jobCategoryRepository->create($data);

        if($cat){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_job_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cat'] = $this->jobCategoryRepository->getWhereWith(['jobs'], [['id', $id], ['nation_id', $nation_id], ['guard', 'admin']])->first();
        
        if(!$data['cat']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.job-categories.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_job_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cat'] = $this->jobCategoryRepository->getWhere([['id', $id], ['nation_id', $nation_id], ['guard', 'admin']])->first();

        if(!$data['cat']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.job-categories.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobCategoryRequest $request, $id)
    {
        if(!checkPermissions(['edit_job_cats'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $updated = $this->jobCategoryRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_job_cats'])){

            return response()->json(['data' => 2], 200);
        }

        //update related jobs to have job_category_id = null
        $this->jobRepository->updateWhere(['job_category_id' => null], [['job_category_id', $id]]);

        // delete category
        $deleted = $this->jobCategoryRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }

    public function jopOfJopCategory(Request $request)
    {

        $jops = $this->jobRepository->getWhere([['job_category_id', $request->id], ['guard', 'admin']], $orderBy = ['column' => 'id', 'dir' => 'DESC']);
    
        return view('admin.employees.jopOfJopCategory' , compact('jops'))->render();

    }
}
