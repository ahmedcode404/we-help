<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AgreementRequest;
use App\Repositories\AgreementRepositoryInterface;
use Illuminate\Http\Request;

class AgreementController extends Controller
{

    public $agreementrepository;
   
    public function __construct(AgreementRepositoryInterface $agreementrepository)
    {
        
        $this->agreementrepository = $agreementrepository;

        $this->middleware('can:edit_agreement', ['only' => ['create','store']]);
        
    } // end of construct

    public function create()
    {
        $nation_id = getAuthNationId();
         
        $agreements = $this->agreementrepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'ASC']);

        return view('admin.agreements.create' , compact('agreements'));
        
    } // end of create


    public function store(AgreementRequest $request)
    {

        $attribute = $request->except('_token');

        $this->agreementrepository->updateAgreement($attribute);

        return response()->json(['data' => 1], 200);
        
    } // end of store    


} // end of class
