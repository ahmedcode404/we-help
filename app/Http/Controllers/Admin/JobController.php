<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\JobRepositoryInterface;
use App\Repositories\JobCategoryRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Http\Requests\Admin\JobRequest;

class JobController extends Controller
{
    private $jobRepository;
    private $jobCategoryRepository;
    private $userRepository;

    public function __construct(JobRepositoryInterface $jobRepository,
                                JobCategoryRepositoryInterface $jobCategoryRepository,
                                UserRepositoryInterface $userRepository){

        $this->jobRepository = $jobRepository;
        $this->jobCategoryRepository = $jobCategoryRepository;
        $this->userRepository = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_jobs'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['jobs'] = $this->jobRepository->paginateWhereHasWith(['category'], 'category', [['nation_id', $nation_id]], 10, [['guard', 'admin']]);

        return view('admin.jobs.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_job'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cats'] = $this->jobCategoryRepository->getWhere([['nation_id', $nation_id], ['guard', 'admin']]);

        return view('admin.jobs.create')->with([
            'data' => $data
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        if(!checkPermissions(['create_job'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $data['guard'] = 'admin';
        $data['slug'] = \Str::random(12);
        
        $job = $this->jobRepository->create($data);

        if($job){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_job'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['job'] = $this->jobRepository->WhereHasWith(['emps', 'category'], 'category', [['nation_id', $nation_id]], [['id', $id], ['guard', 'admin']])->first();

        if(!$data['job']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.jobs.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_job'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cats'] = $this->jobCategoryRepository->getWhere([['nation_id', $nation_id], ['guard', 'admin']]);

        $data['job'] = $this->jobRepository->whereHas('category', [['nation_id', $nation_id]], [['id', $id], ['guard', 'admin']])->first();

        if(!$data['job']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.jobs.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, $id)
    {
        if(!checkPermissions(['edit_job'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method']);

        $updated = $this->jobRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_job'])){

            return response()->json(['data' => 2], 200);
        }

        //update related users to have job_category_id = null
        $this->userRepository->updateWhere(['job_id' => null], [['job_id', $id]]);

        // delete category
        $deleted = $this->jobRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
