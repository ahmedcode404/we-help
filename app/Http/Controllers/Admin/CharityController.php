<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\BankRepositoryInterface;
use App\Http\Requests\Admin\CharityRequest;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\WorkFieldRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Models\Setting;
use Spatie\Permission\Models\Permission;
use App\Models\Notification;
use App\Servicies\Notify;

use Excel;
use App\Exports\AllCharitiesExport;
use App\Exports\ExportCharity;

use App\Http\Requests\Web\RegisterCharityRequest;


use Monarobase\CountryList\CountryListFacade;

class CharityController extends Controller
{
    private $charityRepository;
    private $cityRepository;
    private $bankRepository;
    private $projectRepository;
    private $charityCategoryRepository;
    private $logRepository;
    private $userRepository;
    private $notificationRepository;

    public function __construct(CharityRepositoryInterface $charityRepository,
                                CityRepositoryInterface $cityRepository,
                                BankRepositoryInterface $bankRepository,
                                ProjectRepositoryInterface $projectRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                WorkFieldRepositoryInterface $workFieldRepository,
                                LogRepositoryInterface $logRepository,
                                UserRepositoryInterface $userRepository,
                                NotificationRepositoryInterface $notificationRepository){

        $this->charityRepository = $charityRepository;
        $this->cityRepository = $cityRepository;
        $this->bankRepository = $bankRepository;
        $this->projectRepository = $projectRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->workFieldRepository = $workFieldRepository;
        $this->logRepository = $logRepository;
        $this->userRepository = $userRepository;
        $this->notificationRepository = $notificationRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_charities'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // delete notifications
        $this->notificationRepository->deleteWhere([['title', 'new_charity']]);
        $this->notificationRepository->deleteWhere([['title', 'edit_charity']]);

        $nation_id = getAuthNationId();
        
        // $data['charities'] = $this->charityRepository->paginateWhereWith([['nation_id', $nation_id]], ['nation']);
        $data['charities'] = $this->charityRepository->paginateWhereHas('roles', [['name', 'charity']], 10, [['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'DESC']);

        return view('admin.charities.index')->with([
            'data' => $data
        ]);
    }

    // Get charity of a specific status
    public function getCharityWithStatus($status){

        if(!checkPermissions(['list_charities'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // delete notifications
        $this->notificationRepository->deleteWhere([['title', 'new_charity']]);
        $this->notificationRepository->deleteWhere([['title', 'edit_charity']]);

        $nation_id = getAuthNationId();

        // $data['charities'] = $this->charityRepository->paginateWhereWith([['status', $status], ['nation_id', $nation_id]], ['nation']);
        $data['charities'] = $this->charityRepository->paginateWhereHas('roles', [['name', 'charity']], 10, [['nation_id', $nation_id], ['status', $status]]);

        return view('admin.charities.index')->with([
            'data' => $data
        ]);
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_charity'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['charity'] = $this->charityRepository->getWhereWith(['projects', 'nation', 'projects.supports', 'categories', 
             'edit_requests', 'exchange_requests', 'reports', 'work_fields', 'projects.category'], [['id', $id], ['nation_id', $nation_id]])->first();

    
        if(!$data['charity']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.charities.show')->with([
            'data' => $data
        ]);
    }


    public function create(){

        if(!checkPermissions(['create_charity'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        $data['categories'] = $this->charityCategoryRepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.charities.create')->with([
            'data' => $data
        ]);
    }

    public function store(RegisterCharityRequest $request){

        if(!checkPermissions(['create_charity'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        if($request->has('projects_names') && $request->has('projects_types') && $request->has('images')){
            
            $validator;
            $this->validate($request, [
    
                'images.*' => 'required|mimes:png,jpg,jpeg|max:2048',
            ]);
        }
        
        $nation_id = getNationId();

        $request->password = \Str::random(12);

        $charity = $this->charityRepository->createCharity($request);

        if($charity){

            session()->forget('charity_phone');
            session()->forget('charity_mobile');
            session()->forget('bank_phone');

            // create special edit permission
            Permission::create(['name' => 'edit_charity_' . $charity->id]);

            // Create charity permissions
            $charity->createCharityPermissions();

            // Assign charity roles
            $charity->assignRole([
                'charity', 
                'charity_projects_management_'.$charity->id, 
                'charity_phases_management_'.$charity->id,
                'charity_employees_management_'.$charity->id,
                'charity_sponsers_management_'.$charity->id,
                'charity_exchange_bonds_management_'.$charity->id,
                'charity_job_categories_management_'.$charity->id,
                'charity_jobs_management_'.$charity->id,
                'charity_degrees_management_'.$charity->id,
                'charity_nationalities_management_'.$charity->id,
                'charity_reports_management_'.$charity->id,
            ]);

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $charity->id;
            $log_data['model_type'] = 'charity';
            $log_data['edit_type'] = 'Add new charity';

            $this->logRepository->create($log_data);

            // Send login data to charity
            $setting = Setting::DataLogin();
            $slogo = Setting::Slogan();

            $msg = '';
            $email = $charity->email;
            $topic = $slogo->value;
            $nation_id = $nation_id;
            $dataLogin = $setting->value . '<br><b>' .
            __('admin.email') . ' </b> ' . ' : ' . $charity->email . '<br>' .
            '<b>' . __('admin.password') . ' </b> ' . ' : ' . $request->password;
            
            $msg = getSettingMsg('msg_data_login', $nation_id)->value;
            
            try {
                
                sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $dataLogin);
            }
            catch(Throwable $e){

                $response['status'] = 0;
                $response['message'] = trans('admin.unable_to_send_email');
                $response['reload'] = 0;

                return $response;
            }

            // Notify admins that have permissions to accept and refuse charities
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تسجيل جمعية جديدة';
            $message->message_en = 'A New Charity is Registered';


            // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
            $admins = $this->userRepository->whereHas('permissions', [['name', 'accept_refuse_charities']]);

            // FOREACH ADMINS
            foreach($admins as $admin)    
            {

                Notification::create([ 

                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $admin->id,
                    'model_id'       => $charity->id,
                    'title'          => 'new_charity',
                    'type'           => 'admin'
                ]);             
                
            } // end of foreach admins             
            
            // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
            Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);

            // return redirect()->route('web.home')->with('success', __('web.register_success'));

            $response['status'] = 1;
            $response['redirect'] = route('admin.charities.create');
            $response['reload'] = 0;

            return $response;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_charity'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        // $data['charity'] = $this->charityRepository->getWhereWith(['nation', 'work_fields'], [['id', $id], ['nation_id', $nation_id]])->first();

        $data['charity'] = $this->charityRepository->getWhereWith(['nation'], [['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['charity']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        $data['projects'] = $this->projectRepository->getWhereWith(['images'], [['charity_id', $id], ['profile', 1], ['nation_id', $nation_id]]);

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        // $data['countries'] = $this->cityRepository->getWhere([['parent_id', null], ['nation_id', $nation_id]]);

        // $data['cities'] = $this->cityRepository->getWhere([['parent_id', $data['charity']->country_id]]);

        $data['categories'] = $this->charityCategoryRepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.charities.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CharityRequest $request, $id)
    {

        if(!checkPermissions(['edit_charity'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $charity = $this->charityRepository->findOne($id);

        $charity_status = $charity->status;

        $updated = $this->charityRepository->adminUpdateCharity($request, $id);

        if($updated){

            //Send mail to charity in case off approval or rejection
            if($charity_status != 'approved' && $request->status == 'approved'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Approve charity';

                $this->logRepository->create($log_data);

                $voucher_link = '';
                $msg = getSettingMsg('charity_approve_msg', $charity->nation_id)->value;
                $email = [['email' => $charity->email]];
                $topic = trans('admin.change_status_topic');
                $nation_id = $charity->nation_id;
                $notes = $charity->notes;

                $notify_ar = Setting::where('key' , 'charity_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'charity_approve_msg_sa_en')->first();

                try {
    
                    sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

                } catch (Throwable $e) {
    
                    return response()->json(['data' => 'exception'], 200);
    
                } 

                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $charity->id,
                    'title'          => 'charity',
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , 'charity' , 'charity' , $charity->id); 
                
            }
            else if($charity_status != 'rejected' && $request->status == 'rejected'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Reject charity';

                $this->logRepository->create($log_data);

                $voucher_link = '';
                $msg = getSettingMsg('charity_reject_msg', $charity->nation_id)->value;
                $email = [['email' => $charity->email]];
                $topic = trans('admin.change_status_topic');
                $nation_id = $charity->nation_id;
                $notes = $charity->notes;

                $notify_ar = Setting::where('key' , 'charity_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'charity_reject_msg_sa_en')->first();

                try {
    
                    sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

                } catch (Throwable $e) {
    
                    return response()->json(['data' => 'exception'], 200);
    
                } 

                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $charity->id,
                    'title'          => 'charity',
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , 'charity' , 'charity' , $charity->id); 

            }
            else{

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $id;
                $log_data['model_type'] = 'charity';
                $log_data['edit_type'] = 'Edit charity data';

                $this->logRepository->create($log_data);
            }
            

            // Notify charity
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تم تعديل البيانات الخاصة بكم من قبل إدارة We Help';
            $message->message_en = 'Your data has been updated by We Help adminstration';


            // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $charity->id,
                'model_id'       => $charity->id,
                'title'          => 'charity_data_updated',
                'type'           => 'charity'
            ]);             
            
            // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
            Notify::NotifyWeb($message , null, 'charity' , [$charity->id]);

            // session()->flash('success', trans('admin.update_success'));

            // return redirect()->back();

            $response['status'] = 1;
            $response['redirect'] = route('admin.charities.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
        else{

            // session()->flash('error', trans('admin.add_error'));

            // return redirect()->back();

            $response['status'] = 0;
            $response['redirect'] = route('admin.charities.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_charity'])){

            return response()->json(['data' => 2], 200);
        }

        // delete charity
        $response = $this->charityRepository->deleteCharity($id);

        if($response->charity_deleted){
            
            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'charity';
            $log_data['edit_type'] = 'Delete charity data';

            $this->logRepository->create($log_data);
        }

        return response()->json(['data' => $response], 200);

    }

    public function exportAll($status){

        return Excel::download(new AllCharitiesExport($status), 'charities.xlsx');

    }

    public function exportCharity($id){

        return Excel::download(new ExportCharity($id), 'charities.xlsx');

    }
}
