<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\SliderRequest;
use App\Repositories\SliderRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use Storage;

class SliderController extends Controller
{
    private $sliderRepository;
    private $cityRepository;

    public function __construct(SliderRepositoryInterface $sliderRepository,
                                CityRepositoryInterface $cityRepository){

        $this->sliderRepository = $sliderRepository;
        $this->cityRepository = $cityRepository;
    }
    
    // Get SA Sliders
    public function index(){

        $nation_id = getAuthNationId();

        $data['sliders'] = $this->sliderRepository->paginateWhere([['nation_id', $nation_id]]);

        return view('admin.sliders.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_sliders'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SliderRequest $request)
    {
        if(!checkPermissions(['create_sliders'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $nation_id = getAuthNationId();

        $data = $request->except(['_token', '_method', 'path']);

        $data['nation_id'] = $nation_id;

        $data['path'] = $request->file('path')->store('uploads');
        
        $slider = $this->sliderRepository->create($data);

        if($slider){

            $response['status'] = 1;
            $response['rediredct'] = route('admin.sliders.create');
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['rediredct'] = route('admin.sliders.create');
            $response['reload'] = 0;

            return $response;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_sliders'])){
            
            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['slider'] = $this->sliderRepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['slider']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }
        
        return view('admin.sliders.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SliderRequest $request, $id)
    {
        if(!checkPermissions(['edit_sliders'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $data = $request->except(['_token', '_method']);

        if($request->has('path')){

            $slider = $this->sliderRepository->findOne($id);
            
            //delete image
            Storage::delete($slider->path);

            $data['path'] = $request->file('path')->store('uploads');
        }


        $updated = $this->sliderRepository->update($data, $id);

        if($updated){

            $response['status'] = 1;
            $response['rediredct'] = route('admin.sliders.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['rediredct'] = route('admin.sliders.edit', $id);
            $response['reload'] = 0;

            return $response;
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_sliders'])){

            return response()->json(['data' => 2], 200);
        }

        $slider = $this->sliderRepository->findOne($id);

        //delete image
        Storage::delete($slider->path);

        // delete slider
        $deleted = $this->sliderRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
