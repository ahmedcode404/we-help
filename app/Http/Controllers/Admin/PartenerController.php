<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\PartenerRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use  App\Http\Requests\Admin\PartenerRequest;
use Storage;

class PartenerController extends Controller
{
    private $partenerRepository;
    private $cityRepository;

    public function __construct(PartenerRepositoryInterface $partenerRepository,
                                CityRepositoryInterface $cityRepository){

        $this->partenerRepository = $partenerRepository;
        $this->cityRepository = $cityRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_parteners'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['parteners'] = $this->partenerRepository->paginateWhere([['nation_id', $nation_id]]);

        return view('admin.parteners.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_partener'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        return view('admin.parteners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PartenerRequest $request)
    {
        if(!checkPermissions(['create_partener'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $data = $request->except(['_token', '_method', 'logo']);

        $data['nation_id'] = auth()->user()->nation_id;

        $data['logo'] = $request->file('logo')->store('uploads/parteners/images');
        
        $partener = $this->partenerRepository->create($data);

        if($partener){

            $response['status'] = 1;
            $response['redirect'] = route('admin.parteners.create');
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.parteners.create');
            $response['reload'] = 0;

            return $response;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_partener'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['part'] = $this->partenerRepository->getWhere([['nation_id', $nation_id]])->first();

        if(!$data['part']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }
        
        return view('admin.parteners.edit')->with([
            'data' => $data
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PartenerRequest $request, $id)
    {
        if(!checkPermissions(['edit_partener'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $data = $request->except(['_token', '_method', 'logo']);

        $partener = $this->partenerRepository->findOne($id);

        if($request->has('logo')){

            // delete old logo
            Storage::delete($partener->logo);

            // upload new logo
            $data['logo'] = $request->file('logo')->store('uploads/parteners/images');
        }

        $updated = $this->partenerRepository->update($data, $id);

        if($updated){

            $response['status'] = 1;
            $response['redirect'] = route('admin.parteners.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
        else{

            $response['status'] = 0;
            $response['redirect'] = route('admin.parteners.edit', $id);
            $response['reload'] = 0;

            return $response;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_partener'])){

            return response()->json(['data' => 2], 200);
        }

        $partener = $this->partenerRepository->findOne($id);

        // delete logo
        Storage::delete($partener->logo);

        // delete partener
        $deleted = $this->partenerRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
