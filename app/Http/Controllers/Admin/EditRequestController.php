<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use Illuminate\Http\Request;

class EditRequestController extends Controller
{

    private $editrequestrepository;
    private $notificationRepository;

    public function __construct(EditRequestRepositoryInterface $editrequestrepository,
                                NotificationRepositoryInterface $notificationRepository)
    {

        $this->editrequestrepository    = $editrequestrepository;
        $this->notificationRepository = $notificationRepository;

    }    

    public function index($type){

        if(!checkPermissions(['list_project_edit_requests']) && !checkPermissions(['list_charity_edit_requests'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $edit_requests = $this->editrequestrepository->getWhereWith(['charity', 'project'], [['type', $type], ['nation_id', $nation_id]]);

        if($type == 'charity'){

            // delete notification
            $this->notificationRepository->deleteWhere([['title', 'edit_charty']]);

            return view('admin.requests.charities' , compact('edit_requests'));
        }
        else{

            // delete notification
            $this->notificationRepository->deleteWhere([['title', 'edit_project']]);

            return view('admin.requests.projects' , compact('edit_requests'));
        }

    }   
    
    public function destroy($edit_request)
    {

        $this->editrequestrepository->delete($edit_request);

        return response()->json(['data' => 1], 200);

    } // end of destroy    

} // end of class
