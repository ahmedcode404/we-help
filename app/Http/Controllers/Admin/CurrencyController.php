<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CurrencyRequest;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use Illuminate\Http\Request;
use Storage;

class CurrencyController extends Controller
{
    private $currencyrepository;
    private $cityrepository;
   
    public function __construct(CurrencyRepositoryInterface $currencyrepository,
                                 CityRepositoryInterface $cityrepository)
    {
        
        $this->currencyrepository = $currencyrepository;
        $this->cityrepository     = $cityrepository;

        $this->middleware('can:create_currency',    ['only' => ['create','store']]);
        $this->middleware('can:edit_currency',   ['only' => ['edit','update']]);
        $this->middleware('can:delete_currency', ['only' => ['destroy']]);
        $this->middleware('can:show_currency',   ['only' => ['index']]);               

    } // end of construct

    public function index()
    {

        $nation_id = getAuthNationId();

        $currencies = $this->currencyrepository->getWhere([['nation_id', $nation_id]]);

        return view('admin.currencies.index' , compact('currencies')); 

    } // end of create    

    public function create()
    {

        return view('admin.currencies.create'); 

    } // end of create


    public function store(CurrencyRequest $request)
    {

        $attribute = $request->except('_token', '_method', 'icon');

        $attribute['name_ar'] = $request->name_currency_ar;
        $attribute['name_en'] = $request->name_currency_en;
        $attribute['icon'] = $request->file('icon')->store('uploads');

        $attribute['nation_id'] = auth()->user()->nation_id;

        $this->currencyrepository->create($attribute);

        $response['status'] = 1;
        $response['redirect'] = route('admin.currencies.create');
        $response['reload'] = 0;

        return $response;
        
    } // end of store 
    
    public function edit($curren)
    {
        
        $nation_id = getAuthNationId();

        $currency = $this->currencyrepository->getWhere([['id', $curren], ['nation_id', $nation_id]])->first();

        if(!$currency){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.currencies.edit' , compact('currency')); 

    } // end of create
    
    public function update(CurrencyRequest $request , $currency)
    {
    
        $attribute = $request->except('_token' , '_method' , 'name_currency_ar' , 'name_currency_en', 'icon');

        $currency = $this->currencyrepository->findOne($currency);

        $attribute['name_ar'] = $request->name_currency_ar;
        $attribute['name_en'] = $request->name_currency_en;    
        
        if($request->has('icon')){

            // Delete old icon
            Storage::delete($currency->icon);

            $attribute['icon'] = $request->file('icon')->store('uploads');  
        }
        

        $this->currencyrepository->update($attribute , $currency->id);

        $response['status'] = 1;
        $response['redirect'] = route('admin.currencies.edit', $currency->id);
        $response['reload'] = 0;

        return $response;
        
    } // end of update  
    
    public function destroy($currency)
    {

        $currency = $this->currencyrepository->findOne($currency);

        // Delete old icon
        Storage::delete($currency->icon);

        $this->currencyrepository->delete($currency->id);

        return response()->json(['data' => 1], 200);
        
    } // end of destroy      

} // end of class
