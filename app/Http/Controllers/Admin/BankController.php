<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\BankRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Http\Requests\Admin\BankRequest;

class BankController extends Controller
{

    private $bankRepository;
    private $cityrepository;
    private $charityRepository;

    public function __construct(CityRepositoryInterface $cityrepository,
                                BankRepositoryInterface $bankRepository,
                                CharityRepositoryInterface $charityRepository){

        $this->cityrepository = $cityrepository;
        $this->bankRepository = $bankRepository;
        $this->charityRepository = $charityRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_banks'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data['banks'] = $this->bankRepository->paginateWith(['nation']);

        return view('admin.banks.index')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!checkPermissions(['create_banks'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['cities'] = $this->cityrepository->getWhere([['parent_id', '!=', null], ['nation_id', $nation_id]]);

        return view('admin.banks.create')->with([
            'data' => $data
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BankRequest $request)
    {
        if(!checkPermissions(['create_banks'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method', 'phone_num']);

        $data['nation_id'] = auth()->user()->nation_id;
        
        $bank = $this->bankRepository->create($data);

        if($bank){

            session()->flash('success', trans('admin.add_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_banks'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['bank'] = $this->bankRepository->getWhere([['id', $id], ['nation_id', $nation_id]])->first();

        if(!$data['bank']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        $data['cities'] = $this->cityrepository->getWhere([['parent_id', '!=', null], ['nation_id', $nation_id]]);

        return view('admin.banks.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BankRequest $request, $id)
    {
        if(!checkPermissions(['edit_banks'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $data = $request->except(['_token', '_method', 'phone_num']);

        $updated = $this->bankRepository->update($data, $id);

        if($updated){

            session()->flash('success', trans('admin.update_success'));

            return redirect()->back();
        }
        else{

            session()->flash('error', trans('admin.add_error'));

            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_banks'])){

            return response()->json(['data' => 2], 200);
        }

        // update related charities to have bank_id = null
        $this->charityRepository->updateWhere(['bank_id' => null], [['bank_id', $id]]);

        // delete bank
        $deleted = $this->banksRepository->delete($id);

        if($deleted){

            return response()->json(['data' => 1], 200);
        }
        else{

            return response()->json(['data' => 0], 200);
        }
    }
}
