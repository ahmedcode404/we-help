<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\ServiceOptionRepositoryInterface;
use App\Repositories\WorkFieldRepositoryInterface;
use App\Http\Requests\Admin\ProjectRequest;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\LogRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use Spatie\Permission\Models\Permission;
use App\Servicies\UploadFilesServices;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Models\Notification;
use App\Servicies\Notify;
use App\Models\Setting;

use Excel;
use App\Exports\AllProjectsExport;
use App\Exports\ExportProject;

use Storage;

use Monarobase\CountryList\CountryListFacade;

class ProjectController extends Controller
{
    private $cityRepository;
    private $projectRepository;
    private $phaseRepository;
    private $reportRepository;
    private $currencyRepository;
    private $projectServiceRepository;
    private $workFieldRepository;
    private $logRepository;
    private $charityCategoryRepository;
    private $charityRepository;
    private $fileservice;
    private $userRepository;
    private $notificationRepository;

    public function __construct(CityRepositoryInterface $cityRepository,
                                ProjectRepositoryInterface $projectRepository,
                                PhaseRepositoryInterface $phaseRepository,
                                ReportRepositoryInterface $reportRepository,
                                CurrencyRepositoryInterface $currencyRepository,
                                ServiceOptionRepositoryInterface $projectServiceRepository,
                                LogRepositoryInterface $logRepository,
                                WorkFieldRepositoryInterface $workFieldRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                CharityRepositoryInterface $charityRepository,
                                UploadFilesServices $fileservice,
                                UserRepositoryInterface $userRepository,
                                NotificationRepositoryInterface $notificationRepository
                                ){

        $this->cityRepository           = $cityRepository;
        $this->projectRepository        = $projectRepository;
        $this->phaseRepository          = $phaseRepository;
        $this->reportRepository         = $reportRepository;
        $this->currencyRepository       = $currencyRepository;
        $this->projectServiceRepository = $projectServiceRepository;
        $this->workFieldRepository      = $workFieldRepository;
        $this->logRepository            = $logRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->charityRepository = $charityRepository;
        $this->fileservice           = $fileservice;
        $this->userRepository = $userRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!checkPermissions(['list_projects'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // delete notifications
        $this->notificationRepository->deleteWhere([['title', 'new_project']]);
        $this->notificationRepository->deleteWhere([['title', 'update_project']]);

        $nation_id = getAuthNationId();

        $data['projects'] = $this->projectRepository->paginateWhereWith([['active', 1], ['profile', 0], ['nation_id', $nation_id]], ['nation', 'currency', 'category']);
        $data['session_currency'] = currencySymbol(session('currency'));
        // $data['projects'] = $this->projectRepository->paginateWhereHasWith(['nation', 'currency', 'category'], 'charity', [['status', 'approved']], 10, [['profile', 0], ['nation_id', $nation_id]]);

        return view('admin.projects.index')->with([
            'data' => $data
        ]);
    }
 
    // Get project of a specific status
    public function getProjectWithStatus($status){

        if(!checkPermissions(['list_projects'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        // delete notifications
        $this->notificationRepository->deleteWhere([['title', 'new_project']]);
        $this->notificationRepository->deleteWhere([['title', 'update_project']]);


        $nation_id = getAuthNationId();

        if($status == 'trashed'){

            $data['projects'] = $this->projectRepository->paginateWhereWith([['active', 0], ['profile', 0], ['nation_id', $nation_id]], ['nation', 'currency', 'category']);
            // $data['projects'] = $this->projectRepository->paginateWhereHasWith(['nation', 'currency', 'category'], 'charity', [['status', 'approved']], 10, [['profile', 0], ['nation_id', $nation_id]]);
        }
        else{

            $data['projects'] = $this->projectRepository->paginateWhereWith([['active', 1], ['profile', 0], ['status', $status], ['nation_id', $nation_id]], ['nation', 'currency', 'category']);
        }

        $data['session_currency'] = currencySymbol(session('currency'));


        return view('admin.projects.index')->with([
            'data' => $data
        ]);
        
    }

    public function create()
    {
        if(!checkPermissions(['create_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['currencies'] = $this->currencyRepository->nation($nation_id);

        $data['charities'] = $this->charityRepository->getWhere([['nation_id', $nation_id], ['status', 'approved']]);

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        return view('admin.projects.create')->with([
            'data' => $data
        ]);

    }


    public function store(ProjectRequest $request)
    {

        if(!checkPermissions(['create_project'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }

        $nation_id = getAuthNationId();

        // except some request
        $attribute = $request->except(['_token', '_method', 'eng_maps', 'images', 'attach', 'image']);

        if($request->service_option_id == 'other'){

            $attribute['service_option_id'] = null;
        }

        if($request->service_feature_id == 'other'){

            $attribute['service_feature_id'] = null;
        }
      
        // uploade file attach
        if ($request->hasFile('attach')) {

            $attribute['attach'] = $request->file('attach')->store('uploads/projects');

        } // end of if image

        // uploade file eng maps
        if ($request->hasFile('eng_maps')) {     

            $attribute['eng_maps'] = $request->file('eng_maps')->store('uploads/projects');

        } // end of if image 

        // generate project number
        // $attribute['project_num']  = rand();
        $last_project = $this->projectRepository->getWhere([['profile', 0]])->first();

        if($last_project){

            $attribute['project_num'] = $last_project->project_num +1;
        }
        else{
            $attribute['project_num'] = 1;
        }

        // nation id
        $attribute['nation_id']  = $nation_id;  

        // status
        $attribute['status']  = 'waiting'; 
        
        $attribute['slug'] = \Str::random(12);
        
        if ($request->hasFile('image')) {

            $attribute['image'] = $request->file('image')->store('uploads/projects');

        } // end of if image

        $project = $this->projectRepository->create($attribute);

        Permission::create(['name' => 'edit_project_' . $project->id , 'guard_name' => 'web']);

        // uploade mulitple image 
        if ($request->hasFile('images')) {

            $files = $request->file('images');
            foreach ($request->images as $file) {

                $img = $file;
                $image = $this->fileservice->uploadfile($img, 'project');
                $project->images()->create(

                    ['imageable_id' => $project->id , 'path' => $image]

                ); // end of create

            } // end of foreach

        } // end of if


        // Create log data
        $log_data['user_id'] = auth()->user()->id;
        $log_data['user_type'] = 'admin';
        $log_data['model_id'] = $project->id;
        $log_data['model_type'] = 'project';
        $log_data['edit_type'] = 'Add project data';

        $this->logRepository->create($log_data);


        // notify admnis that have permissions to accept and refuse projects
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'تم اضافة مشروع جديد من قبل الإدارة ';
        $message->message_en = 'Add New Project From by Admin';

        // GET ALL ADMINS THAT HAVE PERMISSION TO ACCEPT AND REFUSE PROJECTS
        $admins = $this->userRepository->whereHas('permissions', [['name', 'accept_refuse_projects']]);

        // FOREACH ADMINS
        foreach($admins as $admin)    
        {

            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $admin->id,
                'model_id'       => $project->id,
                'title'          => 'new_project',
                'type'           => 'admin'
            ]);             
    
            
        } // end of foreach admins  
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);   
        
        
        // Notify charity
        // notification 
        $message = new \stdClass();
        $message->message_ar = 'تم اضافة مشروع جديد من قبل الإدارة ';
        $message->message_en = 'Add New Project From by Admin';


        // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
        Notification::create([ 

            'message_en'     => $message->message_en,
            'message_ar'     => $message->message_ar,
            'send_user_id'   => $project->charity_id,
            'model_id'       => $project->id,
            'title'          => 'charity_new_project',
            'type'           => 'charity'
        ]);             
        
        // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
        Notify::NotifyWeb($message , null, 'charity' , [$project->charity_id]);

        if($project){

            $response['status'] = 1;
            $response['redirect'] = route('admin.projects.create');
            $response['reload'] = 0;
            return $response;
        }
        else{

            $response['status'] = 0;
            $response['reload'] = 0;
            return $response;
        }
        

    } 


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!checkPermissions(['show_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['project'] = $this->projectRepository->getWhereWith(['category', 'reports', 'supports', 'nation', 'currency', 'phases', 
                            'charity', 'images', 'sponsers', 'sponsers.work_field', 'edit_requests'],
                            [['id', $id], ['nation_id', $nation_id], ['profile', 0]])->first();
        
        if(!$data['project']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }

        return view('admin.projects.show')->with([
            'data' => $data
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!checkPermissions(['edit_project'])){

            session()->flash('error', trans('admin.do_not_have_permission'));

            return redirect()->back();
        }

        $nation_id = getAuthNationId();

        $data['project'] = $this->projectRepository->getWhereWith(['category', 'reports', 'supports', 'nation', 'currency', 'phases', 
                            'charity', 'images', 'sponsers', 'sponsers.work_field', 'edit_requests'],
                            [['id', $id], ['nation_id', $nation_id], ['profile', 0]])->first();
                                                
        if(!$data['project']){

            session()->flash('error', trans('admin.no_data'));

            return redirect()->back();
            
        }
        
        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        $data['currencies'] = $this->currencyRepository->getwhere([['nation_id', $nation_id]]);

        $data['work_fields'] = $this->workFieldRepository->getWhere([['nation_id', $nation_id]]);

        $data['services'] = $this->projectServiceRepository->getWhere([['parent_id', null], ['charity_category_id', $data['project']->charity_category_id]]);
            
        if($data['project']->service_feature_id != null){

            $feature = $this->projectServiceRepository->findOne($data['project']->service_feature_id);

            $data['features'] = $this->projectServiceRepository->getWhere([['parent_id', $feature->parent_id]]);
        }
        else{

            $data['features'] = [];
        }

        return view('admin.projects.edit')->with([
            'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        if(!checkPermissions(['edit_project'])){

            $response['status'] = 0;
            $response['message'] = trans('admin.do_not_have_permission');
            $response['reload'] = 0;

            return $response;
        }      

        $project = $this->projectRepository->findOne($id);

        $project_status = $project->status;

        $updated = $this->projectRepository->adminUpdateProject($request, $id);


        if($updated){

            if($project_status !='approved' && $request->status == 'approved'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Approve project';

                $this->logRepository->create($log_data);

                $voucher_link = '';
                $msg = getSettingMsg('project_approve_msg', $project->nation_id)->value;
                $email = [['email' => $project->charity->email]];
                $topic = trans('admin.change_status_topic');
                $nation_id = $project->nation_id;
                $notes = $project->notes;

                $notify_ar = Setting::where('key' , 'project_approve_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'project_approve_msg_sa_en')->first(); 

                try {
    
                    sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

                } catch (Throwable $e) {
    
                    return response()->json(['data' => 'exception'], 200);
    
                } 

                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $project->charity_id,
                    'model_id'       => $project->id,
                    'title'          => 'project',
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , 'project' , 'charity' , $project->charity_id); 

                $messagesubscribe = 'تم اضافه مشروع جديد للمشاهد اضغط علي الرابط الموجود بالاسفل';

                sendMailSubscribe($messagesubscribe , $project->slug, 1);
            }
            else if($project_status !='rejected' && $request->status == 'rejected'){

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Reject project';

                $this->logRepository->create($log_data);

                $voucher_link = '';
                $msg = getSettingMsg('project_reject_msg', $project->nation_id)->value;
                $email = [['email' => $project->charity->email]];
                $topic = trans('admin.change_status_topic');
                $nation_id = $project->nation_id;
                $notes = $project->notes;

                $notify_ar = Setting::where('key' , 'project_reject_msg_sa_ar')->first();
                $notify_en = Setting::where('key' , 'project_reject_msg_sa_en')->first();

                try {
    
                    sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

                } catch (Throwable $e) {
    
                    return response()->json(['data' => 'exception'], 200);
    
                } 

                // notification 
                $message = new \stdClass();
                $message->message_ar = $notify_ar->value;
                $message->message_en = $notify_en->value;
        
                Notification::create([ 
        
                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $project->charity_id,
                    'model_id'       => $project->id,
                    'title'          => 'project',
                    'type'           => 'charity'
                ]);             

                Notify::NotifyWeb($message , 'project' , 'charity' , $project->charity_id); 

            }
            else{

                // Create log data
                $log_data['user_id'] = auth()->user()->id;
                $log_data['user_type'] = 'admin';
                $log_data['model_id'] = $id;
                $log_data['model_type'] = 'project';
                $log_data['edit_type'] = 'Edit project data';

                $this->logRepository->create($log_data);
                
            }

            // Notify charity
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تم تعديل مشروع من قبل الإدارة';
            $message->message_en = 'A project data has been updated by adminstration';


            // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
            Notification::create([ 

                'message_en'     => $message->message_en,
                'message_ar'     => $message->message_ar,
                'send_user_id'   => $project->charity_id,
                'model_id'       => $id,
                'title'          => 'charity_project_updated',
                'type'           => 'charity'
            ]);             
            
            // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
            Notify::NotifyWeb($message , null, 'charity' , [$project->charity_id]);

            // session()->flash('success', trans('admin.update_success'));

            // return redirect()->back();

            if($request->has('prev_project')){

                $response['status'] = 1;
                $response['redirect'] = route('admin.charities.edit', $id);
                $response['reload'] = 0;
            }
            else{

                $response['status'] = 1;
                $response['redirect'] = route('admin.projects.edit', $id);
                $response['reload'] = 0;

            }
            

            return $response;
        }
        else{

            // session()->flash('error', trans('admin.add_error'));

            // return redirect()->back();

            if($request->has('prev_project')){

                $response['status'] = 0;
                $response['redirect'] = route('admin.charities.edit', $id);
                $response['reload'] = 0;
            }
            else{

                $response['status'] = 0;
                $response['redirect'] = route('admin.projects.edit', $id);
                $response['reload'] = 0;

            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!checkPermissions(['delete_project'])){

            return response()->json(['data' => 2], 200);
        }

        // $project = $this->projectRepository->findOne($id);

        // $project_num = $project->project_num;
        
        $response = $this->projectRepository->adminDeleteProject($id);

        if($response->charity_deleted){

            // Creat log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'project';
            $log_data['edit_type'] = 'Delete project data';

            $this->logRepository->create($log_data);

            // update project_num for all other projects
            // $this->projectRepository->updateProjectNumbers($project_num);

        }

        return response()->json(['data' => $response], 200);

    }

    // restore project - change active to 1
    public function restore($id){

        $updated = $this->projectRepository->update(['active' => 1], $id);

        if($updated){

            // Create log data
            $log_data['user_id'] = auth()->user()->id;
            $log_data['user_type'] = 'admin';
            $log_data['model_id'] = $id;
            $log_data['model_type'] = 'project';
            $log_data['edit_type'] = 'Restore project';

            $this->logRepository->create($log_data);

            return response()->json(['data' => 1], 200);
            
        }
        else{

            return response()->json(['data' => 0], 200);
        }

    }

    public function exportAll($status){

        return Excel::download(new AllProjectsExport($status), 'projects.xlsx');

    }

    public function exportProject($id){

        return Excel::download(new ExportProject($id), 'projects.xlsx');

    }

    public function getCharityCategories(Request $request){

        $data['categories'] = $this->charityCategoryRepository->whereHas('charities', [['charity_id', $request->id]]);

        return view('admin.projects.ajax.categories')->with([
            'data' => $data
        ]);
    }

}
