<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CartRepositoryInterface;

use App\Http\Resources\PersonalInfoResource;

use Validator;
use Hash;

class LoginController extends Controller
{
    private $charityRepository;
    private $userRepository;
    private $cartRepository;

    public function __construct(CharityRepositoryInterface $charityRepository,
                                userRepositoryInterface $userRepository,
                                CartRepositoryInterface $cartRepository){

        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            
            $this->middleware('auth:sanctum', ['only' => ['logout']]);
        }

        $this->middleware('sessions');

        $this->charityRepository = $charityRepository;
        $this->userRepository = $userRepository;
        $this->cartRepository = $cartRepository;

    }

    public function login(Request $request){

        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        $user = $this->userRepository->whereHas('roles', [['name', 'supporter']], [['email', $request->email]])->first();

        if($user && Hash::check($request->password, $user->password)){

            // update cart
            if(session()->has('cart_no')){
            
                $this->cartRepository->updateWhere(['user_id' => $user->id, 'cart_no' => null], [['cart_no', session('cart_no')]]);
                
                session()->forget('cart_no');
            }

            $data['data']['user'] = new PersonalInfoResource($user);
            $data['data']['token'] = 'Bearer '.$user->createToken('login')->plainTextToken;
            $data['message'] = trans('api.login_success');
            $data['status'] = true;
            $data['http_response'] = 200;

            return $data;
            
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.login_failed');
            $data['status'] = false;
            $data['http_response'] = 401;

            return $data;
        }
    }

    public function logout(){

        // delete the token that was used to authenticate the current request
        auth()->user()->currentAccessToken()->delete();

        // delete prev tokens
        auth()->user()->tokens()->delete();

        // delete device tokens
        auth()->user()->device_tokens()->delete();

        $data['data'] = null;
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }
}
