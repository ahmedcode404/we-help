<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\CityRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\ProjectUserRepositoryInterface;

use App\Http\Resources\PersonalInfoResource;
use App\Http\Resources\DonatedProjectResource;
use App\Http\Resources\CategorizedDonationResource;
use App\Http\Resources\ReportDetailsResource;

use Monarobase\CountryList\CountryListFacade;

use Validator;
use Storage;
use Hash;

class ProfileController extends Controller
{

    private $cityRepository;
    private $projectRepository;
    private $reportRepository;
    private $currencyRepository;
    private $projectUserRepository;
    private $nation_id;

    public function __construct(CityRepositoryInterface $cityRepository,
                                ProjectRepositoryInterface $projectRepository,
                                ReportRepositoryInterface $reportRepository,
                                CurrencyRepositoryInterface $currencyRepository,
                                ProjectUserRepositoryInterface $projectUserRepository
    ){

        $this->cityRepository = $cityRepository;
        $this->projectRepository = $projectRepository;
        $this->reportRepository = $reportRepository;
        $this->currencyRepository = $currencyRepository;
        $this->projectUserRepository = $projectUserRepository;

        $this->nation_id = getNationId();

        $this->middleware(['auth:sanctum', 'sessions']);
    }
    
    public function getProfileData(){

        $data['data']['user'] = new PersonalInfoResource(auth()->user());
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;

    }

    public function editProfileData(Request $request){

        $validator = Validator::make($request->all(), [

            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.auth()->user()->id,
            'phone' => 'required|min:8|max:14|unique:users,phone,'.auth()->user()->id,
            'country' => 'required',
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        // $country = $this->cityRepository->getWhere([['id', $request->country_id], ['nation_id', $this->nation_id], ['parent_id', null]])->first();

        $country = CountryListFacade::getList(\App::getLocale());

        if(!$country){

            $data['data'] = null;
            $data['message'] = trans('api.country_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }

        $user_data = $request->except(['image', 'password']);

        $user_data['name_ar'] = $request->name;
        $user_data['name_en'] = $request->name;


        if($request->has('image')){

            // Delete old image
            Storage::delete(auth()->user()->image);

            $user_data['image'] = $request->file('image')->store('uploads'); 
        }

        $updated = auth()->user()->update($user_data);

        if($updated){

            $data['data']['user'] = new PersonalInfoResource(auth()->user());
            $data['message'] = trans('api.update_success');
            $data['status'] = true;
            $data['http_response'] = 200;

            return $data;
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }
    }

    public function ChangePassword(Request $request){

        $validator = Validator::make($request->all(), [

            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        if(Hash::check($request->old_password, auth()->user()->password)){

            $user_data['password'] = bcrypt($request->password); 

            $updated = auth()->user()->update($user_data);

            if($updated){

                $data['data'] = null;
                $data['message'] = trans('api.update_success');
                $data['status'] = true;
                $data['http_response'] = 200;
    
                return $data;
            }
            else{
    
                $data['data'] = null;
                $data['message'] = trans('api.failed');
                $data['status'] = false;
                $data['http_response'] = 400;
    
                return $data;
            }
        }
        else{

            $data['data'] = null;
                $data['message'] = trans('api.wrong_password');
                $data['status'] = false;
                $data['http_response'] = 400;
    
                return $data;
        }  

    }

    public function getDonatedMoney(){

        $data['data']['projects'] = DonatedProjectResource::collection(auth()->user()->donates);
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;

    }

    public function getDonatedProjects(){

        $data['data']['once_donations'] = DonatedProjectResource::collection(auth()->user()->donates->where('type', 'once'));
        $data['data']['monthly_donations'] = DonatedProjectResource::collection(auth()->user()->donates->where('type', 'monthly'));
        $data['data']['gift_donations'] = DonatedProjectResource::collection(auth()->user()->donates->where('type', 'gift'));
        $data['data']['ambassador_donations'] = DonatedProjectResource::collection(auth()->user()->donates->where('type', 'ambassador'));
        $data['donated_projects_count'] = auth()->user()->donates()->count();
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    public function getReports(){

        $data['data']['projects'] = CategorizedDonationResource::collection($this->projectRepository->getProjectsHaveReports(auth()->user()->id));
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;

    }

    public function getReportDetails($id){

        $data['data']['report'] = new ReportDetailsResource($this->reportRepository->findOne($id));
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    public function updateMonthlyDeduction(Request $request){

        $validator = Validator::make($request->all(), [

            'project_id' => 'required|exists:projects,id',
            'cost' => 'required_if:stop_monthly_deduction,0|numeric',
            'currency_id' => 'required_if:stop_monthly_deduction,0|exists:currencies,id',
            'stop_monthly_deduction' => 'required|in:1,0',
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        // check for project
        $project = $this->projectRepository->getWhere([['id', $request->project_id], ['nation_id', $this->nation_id]])->first();

        if(!$project){

            $data['data'] = null;
            $data['message'] = trans('api.project_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }

        // check for currency
        if($request->has('currency_id')){

            $currency = $this->currencyRepository->getWhere([['id', $request->currency_id], ['nation_id', $this->nation_id]])->first();

            if(!$currency){

                $data['data'] = null;
                $data['message'] = trans('api.currency_not_found');
                $data['status'] = false;
                $data['http_response'] = 404;

                return $data;
            }
            
        }
        

        if($request->stop_monthly_deduction == 0){

            $donate_data['cost'] = $request->cost;
            $donate_data['cost_gbp'] = generalExchange($request->cost, currencySymbol($request->currency_id), 'GBP');
            $donate_data['currency_id'] = $request->currency_id;    
        }
        else{
            $donate_data['type'] = 'once';
        }

        $updated = $this->projectUserRepository->updateWhere($donate_data, [['project_id', $request->project_id], ['user_id', auth()->user()->id]]);

        if($updated){

            $data['data'] = null;
            $data['message'] = trans('api.update_success');
            $data['status'] = true;
            $data['http_response'] = 200;

            return $data;
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }

    }
}
