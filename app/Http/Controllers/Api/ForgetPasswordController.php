<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use Validator;

class ForgetPasswordController extends Controller
{
    private $userRepository;
    private $charityRepository;

    public function __construct(UserRepositoryInterface $userRepository,
                                CharityRepositoryInterface $charityRepository){

        $this->userRepository = $userRepository;
        $this->charityRepository = $charityRepository;
        
    }

    public function forgetPassword(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        $user = $this->userRepository->whereHas('roles', [['name', 'supporter']], [['email', $request->email]])->first();

        if($user){

            return $this->sendCode($user);
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.email_not_exist');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }
    }


    // Send/resend code in email
    public function sendCode($user){

        // create code
        $code = mt_rand(1000,9999);

        // update user with new verificatio_code
        $this->userRepository->update(['verification_code' => $code], $user->id);

        $user = $this->userRepository->findOne($user->id);
        
        // Send Verification code - Email
        $users = [
            [
                'name' => $user->name,
                'email' => $user->email,
            ]
        ];

        $topic = trans('web.verification_code_topic');
        $message = trans('web.verification_code_msg');
        $nation_id = getNationId();
        $notes = $code;

        try {

            sendMail($users, $topic, $message, $nation_id, $notes);

        } catch (Throwable $e) {

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;

        } 

        $data['data']['code'] = $user->verification_code;
        $data['data']['email'] = $user->email;
        $data['message'] = trans('api.code_sent');
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    // Verify code
    public function verifyCode(Request $request){

        $validator = Validator::make($request->all(), [
            'code' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        $user = $this->userRepository->whereHas('roles', [['name', 'supporter']], [['email', $request->email]])->first();
        
        if($user){

            if($request->code == $user->verification_code){

                $data['data']['email'] = $user->email;
                $data['message'] = trans('api.valid_code');
                $data['status'] = true;
                $data['http_response'] = 200;

                return $data;
            }
            else{

                $data['data'] = null;
                $data['message'] = trans('api.invalid_code');
                $data['status'] = false;
                $data['http_response'] = 406;

                return $data;
            }
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.email_not_exist');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }


    }

    public function setNewPassword(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'same:password'
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        $user = $this->userRepository->whereHas('roles', [['name', 'supporter']], [['email', $request->email]])->first();

        if($user){

            $updated = $this->userRepository->updateWhere(['password' => bcrypt($request->password)], [['email', $request->email]]);
            
            if($updated){

                $data['data'] = null;
                $data['message'] = trans('api.password_changed');
                $data['status'] = true;
                $data['http_response'] = 200;
    
                return $data;
            }
            else{
    
                $data['data'] = null;
                $data['message'] = trans('api.failed');
                $data['status'] = false;
                $data['http_response'] = 406;

                return $data;
            }
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.email_not_exist');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }


    }
}
