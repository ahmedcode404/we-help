<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\AmbassadorRepositoryInterface;

use App\Http\Resources\CampaignResource;
use App\Http\Resources\CampaignDetailsResource;

use Validator;

class CampaignController extends Controller
{
    private $projectRepository;
    private $ambassadorRepository;

    public function __construct(ProjectRepositoryInterface $projectRepository,
                                AmbassadorRepositoryInterface $ambassadorRepository){

        $this->projectRepository = $projectRepository;
        $this->ambassadorRepository = $ambassadorRepository;
        
        $this->nation_id = getNationId();

        $this->middleware(['auth:sanctum', 'sessions']);

    }


    // Get all users campiangs
    public function getAllCampaings(){

        $campaings = $this->ambassadorRepository->getWhereWith(['project'], [['user_id', auth()->user()->id]]);
        
        $data['data']['campaings'] = CampaignResource::collection($campaings);
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    // Get campaing details
    public function getCampaingDetails($id){

        $campaing = $this->ambassadorRepository->getWhereWith(['project'], [['id', $id], ['user_id', auth()->user()->id]])->first();

        if($campaing){

            $data['data']['campaing'] = new CampaignDetailsResource($campaing);
            $data['message'] = '';
            $data['status'] = true;
            $data['http_response'] = 200;

            return $data;
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.campaign_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }
        
        
    }

    // Create campaing for ambassadors
    public function createCampaing(Request $request){

        $validator = Validator::make($request->all(), [
            'project_id' => 'required|exists:projects,id',
            'ambassador_name' => 'required',
            'campaign_name' => 'required',
            'campaign_goal_id' => 'required|exists:campaign_goals,id',
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        // check for project existance
        $project = $this->projectRepository->getWhere([['id', $request->project_id], ['nation_id', $this->nation_id]])->first();

        if(!$project){

            $data['data'] = null;
            $data['message'] = trans('api.project_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }

        $ambassador_data['ambassador_name'] = $request->ambassador_name;
        $ambassador_data['campaign_name'] = $request->campaign_name;
        $ambassador_data['affiliate'] = substr(uniqid(), 0, 8);
        $ambassador_data['user_id'] = auth()->user()->id;
        $ambassador_data['project_id'] = $request->project_id;
        $ambassador_data['campaign_goal_id'] = $request->campaign_goal_id;

        $campaing = $this->ambassadorRepository->create($ambassador_data);

        if($campaing){

            $data['data']['campaing_route'] = route('web.campaign', $campaing->affiliate);
            $data['message'] = trans('api.campaing_success');
            $data['status'] = true;
            $data['http_response'] = 201;

            return $data;
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }
    }
}
