<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rules\ApiPhone;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\CityRepositoryInterface;

use App\Http\Resources\PersonalInfoResource;

use Validator;

class RegisterController extends Controller
{
    private $userRepository;
    private $currencyRepository;
    private $cityRepository;

    public function __construct(userRepositoryInterface $userRepository,
                                CurrencyRepositoryInterface $currencyRepository,
                                CityRepositoryInterface $cityRepository){

        $this->userRepository = $userRepository;
        $this->currencyRepository = $currencyRepository;
        $this->cityRepository = $cityRepository;

        $this->middleware('sessions');

    }

    public function register(Request $request){

        $nation_id = getNationId();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            // 'phone' => ['required', 'unique:users,phone', new ApiPhone($nation_id)],
            'phone' => 'required|unique:users,phone|min:8|max:16|phone:'.$request->country,
            // 'country_id' => 'required|exists:cities,id',
            'country' => 'required|string',
            'password' => 'required|min:6|confirmed'
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        // $country = $this->cityRepository->getWhere([['id', $request->country_id], ['parent_id', null]])->first();

        // if(!$country){

        //     $data['data'] = null;
        //     $data['message'] = trans('api.country_not_found');
        //     $data['status'] = false;
        //     $data['http_response'] = 404;

        //     return $data;
        // }

        $create_data = $request->except(['_method', '_token', 'name', 'password_confirmation']);

        $create_data['name_ar'] = $request->name;
        $create_data['name_en'] = $request->name;
        $create_data['blocked'] = 0;
        $create_data['nation_id'] = $nation_id;
        $create_data['password'] = bcrypt($request->password);

        $user = $this->userRepository->create($create_data);

        // \session()->forget('currency');

        if(!session()->has('currency') || $user->currency_id == null){

            $currency = $this->currencyRepository->getWhere([['nation_id', $nation_id]])->first();

            $this->userRepository->update(['currency_id' => $currency->id], $user->id);

            $user = $this->userRepository->findOne($user->id);

            session('currency', $currency->id);

        }

        if($user){

            $user->assignRole('supporter');

            $data['data']['user'] = new PersonalInfoResource($user);
            $data['data']['token'] = 'Bearer '. $user->createToken('login')->plainTextToken;
            $data['message'] = trans('api.register_success');
            $data['status'] = true;
            $data['http_response'] = 200;

            return $data;
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;

        }


    }

}
