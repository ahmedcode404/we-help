<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct(){

        $this->middleware(['auth:sanctum', 'sessions']);
    }

    public function deleteAccount(){

        if(count(auth()->user()->donates) == 0){

            $updated = auth()->user()->delete();

            if($updated){

                $data['data'] = null;
                $data['message'] = trans('api.delete_success');
                $data['status'] = true;
                $data['http_response'] = 204;

                return $data;
            }
            else{
                $data['data'] = null;
                $data['message'] = trans('api.failed');
                $data['status'] = false;
                $data['http_response'] = 400;

                return $data;
            }
        }
        
        $data['data'] = null;
        $data['message'] = trans('api.account_can_no_be_deleted');
        $data['status'] = false;
        $data['http_response'] = 400;

        return $data;
    }
}
