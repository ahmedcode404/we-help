<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\StaticPageRepositoryInterface;
use App\Repositories\FaqRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\ContactRepositoryInterface;

use  App\Http\Resources\StaticPageResource;
use  App\Http\Resources\FAQResource;
use Validator;

class StaticPageController extends Controller
{
    private $staticPageRepository;
    private $faqRepository;
    private $settingRepository;
    public $contactRepository;

    public function __construct(StaticPageRepositoryInterface $staticPageRepository,
                                FaqRepositoryInterface $faqRepository,
                                SettingRepositoryInterface $settingRepository,
                                ContactRepositoryInterface $contactRepository){

        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:sanctum', ['only' => ['getUserData']]);
        }

        $this->staticPageRepository = $staticPageRepository;
        $this->faqRepository = $faqRepository;
        $this->settingRepository = $settingRepository;
        $this->contactRepository = $contactRepository;

        $this->middleware('sessions');

    }
    
    public function getStaticPages($key){

        $nation_id = getNationId();

        $page = $this->staticPageRepository->getWhere([['key', $key], ['nation_id', $nation_id]])->first();

        $data['data']['page'] = new StaticPageResource($page);

        if($key == 'about_us'){
            $data['data']['statistics'] = route('web.statistics');
            $data['data']['more'] = route('web.about');
        }

        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;

    }

    public function getFAQ(){
        
        $nation_id = getNationId();

        $faq = $this->faqRepository->getWhere([['nation_id', $nation_id]]);

        $data['data']['questions'] = FAQResource::collection($faq);
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    public function getContactUsData(){

        $nation_id = getNationId();

        $phone = getSettingValue('phone', $nation_id);
        $email = getSettingValue('email', $nation_id);
        $address = getSettingValue('address', $nation_id);
        
        $facebook = getSettingValue('facebook', $nation_id);
        $twitter = getSettingValue('twitter', $nation_id);
        $snapchat = getSettingValue('snapchat', $nation_id);
        $instagram = getSettingValue('insta', $nation_id);

        $data['data']['phone'] = $phone ? $phone->value : null;
        $data['data']['email'] = $email ? $email->value : null;
        $data['data']['address'] = $address ? $address->value : null;

        $data['data']['facebook'] = $facebook ? $facebook->value : null;
        $data['data']['twitter'] = $twitter ? $twitter->value : null;
        $data['data']['snapchat'] = $snapchat ? $snapchat->value : null;
        $data['data']['instagram'] = $instagram ? $instagram->value : null;

        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;

    }

    public function contactUs(Request $request){

        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required|min:9|max:14',
            'message' => 'required'
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        $name = clearText($request->name);
        $contact_data['name'] = $name;

        $phone = clearText($request->phone);
        $contact_data['phone'] = $phone;

        $email = clearText($request->email);
        $contact_data['email'] = $email;

        $content = clearText($request->content);
        $contact_data['content'] = $content;

        $contact_data['name'] = $name;
        $contact_data['phone'] = $phone;
        $contact_data['email'] = $email;
        $contact_data['message'] = $content;
        $contact_data['nation_id'] = getNationId();

        $contact = $this->contactRepository->create($contact_data);
        
        $data['data'] = null;

        if($contact){

            $data['message'] = trans('api.success');
            $data['status'] = true;
            $data['http_response'] = 201;
        }
        else{

            $data['message'] = trans('api.failed');
            $data['status'] = true;
            $data['http_response'] = 400;
        }

        return $data;

    }

    public function getUserData(){

        $data['data']['user_name'] = auth()->check() ? auth()->user()->name : null;
        $data['data']['user_email'] = auth()->check() ? auth()->user()->email : null;
        $data['data']['user_image'] = auth()->check() && auth()->user()->image ? asset('storage/'.auth()->user()->image) : asset('web/images/main/user_avatar.png');

        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }
}
