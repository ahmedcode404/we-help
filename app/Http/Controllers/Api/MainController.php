<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\CampaignGoalRepositoryInterface;
use App\Repositories\DeviceTokenRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;

use App\Http\Resources\CurrencyResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\NotificationResource;

use Monarobase\CountryList\CountryListFacade;

use Validator;


class MainController extends Controller
{

    private $currencyRepository;
    private $cityRepository;
    private $campaignGoalRepository;
    private $deviceTokenRepository;
    private $notificationRepository;

    private $nation_id;

    public function __construct(CurrencyRepositoryInterface $currencyRepository,
                                CityRepositoryInterface $cityRepository,
                                CampaignGoalRepositoryInterface $campaignGoalRepository,
                                DeviceTokenRepositoryInterface $deviceTokenRepository,
                                NotificationRepositoryInterface $notificationRepository){

        $this->currencyRepository = $currencyRepository;
        $this->cityRepository = $cityRepository;
        $this->campaignGoalRepository = $campaignGoalRepository;
        $this->deviceTokenRepository = $deviceTokenRepository;
        $this->notificationRepository = $notificationRepository;

        $this->nation_id = getNationId();

        $this->middleware('sessions');

        $this->middleware('auth:sanctum', ['only' => ['getAllNotifications', 'deleteNotification', 
                                            'enableDisableNotifications', 'clearNotifications',
                                            'getNotificationsCount']]);

    }

    public function getCurrencies(){

        $currencies = $this->currencyRepository->getWhere([['nation_id', $this->nation_id]]);

        $data['data']['currencies'] = CurrencyResource::collection($currencies);
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    public function selectCurrency(Request $request){

        $validator = Validator::make($request->all(), [
            'currency_id' => 'required'
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        // check for currency
        $currency = $this->currencyRepository->getWhere([['id', $request->currency_id], ['nation_id', $this->nation_id]])->first();

        if(!$currency){

            $data['data'] = null;
            $data['message'] = trans('api.currency_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }

        session('currency', $request->currenc_id);

        $data['data'] = null;
        $data['message'] = trans('api.success');
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    public function getCountries(){

        // $countries = $this->cityRepository->getWhere([['nation_id', $this->nation_id], ['parent_id', null]]);

        $countries = CountryListFacade::getList(\App::getLocale());

        $data['data']['countries'] = CountryResource::collection($countries);
        $data['data']['countries'] = $countries;
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;

    }

    public function getCampaignGoals(){

        $goals = $this->campaignGoalRepository->getWhere([['nation_id', $this->nation_id]]);

        $data['data']['countries'] = CountryResource::collection($goals);
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;

    }


    public function createDeviceToken(Request $request){
        

        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'device_id' => 'required',
            'firebase_token' => 'required|max:255',
            'platform_type' => 'required',
            'user_type' => 'required|in:supporter,charity,employee',
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        $user_token = $this->deviceTokenRepository->getWhere([
            ['user_id' , $request->user_id], 
            ['device_id', $request->device_id], 
            ['firebase_token', $request->firebase_token],
            ['user_type', $request->user_type]
        ])->first();

        // If user already has a token, delete old token and create a new one, else create a new token
        if($user_token){

            $this->deviceTokenRepository->delete($user_token->id);
            $token = $this->deviceTokenRepository->create($request->all());

        }
        else{

            $token = $this->deviceTokenRepository->create($request->all());

        }

        if($token){

            $data['data']['token'] = $token;
            $data['message'] = '';
            $data['status'] = true;
            $data['http_response'] = 201;

            return $data;

        } 
        else{

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;

        }
    }

    public function getAllNotifications(){

        $data['data']['notifications'] = NotificationResource::collection(auth()->user()->donor_notifications);
        $data['data']['count'] = 0;
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        auth()->user()->donor_notifications()->update(['read' => 1]);

        return $data;

    }

    public function deleteNotification($id){

        $deleted = $this->notificationRepository->delete($id);

        if($deleted){

            $data['data'] = null;
            $data['message'] = trans('api.delete_success');
            $data['status'] = true;
            $data['http_response'] = 204;

            return $data;
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;

        }

    }

    public function enableDisableNotifications(Request $request){

        $validator = Validator::make($request->all(),[
            'enable' => 'required|in:0,1'
        ]);

        if ($validator->fails()) {

            return apiValidation($validator);
        }

        $updated = auth()->user()->update(['is_notify' => $request->enable]);

        if($updated){

            $data['data'] = null;
            $data['message'] = $request->enable == 1 ? trans('api.notifications_enabled') : trans('api.notifications_disabled');
            $data['status'] = true;
            $data['http_response'] = 400;

            return $data;
        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }

    }

    public function clearNotifications(){

        auth()->user()->donor_notifications()->delete();

        $data['data'] = null;
        $data['message'] = trans('api.delete_success');
        $data['status'] = true;
        $data['http_response'] = 204;

        return $data;

    }

    public function getNotificationsCount(){

        $data['data'] = count(auth()->user()->donor_notifications()->where('read', 0)->get());
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }
}
