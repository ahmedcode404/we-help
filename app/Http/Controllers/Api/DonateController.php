<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\ProjectUserRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\GiftRepositoryInterface;
use App\Repositories\AmbassadorRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;

use Validator;

class DonateController extends Controller
{
    private $projectUserRepository;
    private $projectRepository;
    private $giftRepository;
    private $ambassadorRepository;
    private $currencyRepository;
    private $nation_id;

    public function __construct(ProjectUserRepositoryInterface $projectUserRepository,
                                ProjectRepositoryInterface $projectRepository,
                                GiftRepositoryInterface $giftRepository,
                                AmbassadorRepositoryInterface $ambassadorRepository,
                                CurrencyRepositoryInterface $currencyRepository){


        $this->projectUserRepository = $projectUserRepository;
        $this->projectRepository = $projectRepository;
        $this->giftRepository = $giftRepository;
        $this->ambassadorRepository = $ambassadorRepository;
        $this->currencyRepository = $currencyRepository;

        $this->nation_id = getNationId();

        $this->middleware(['auth:sanctum', 'sessions']);
    }
    
    // Store actual donate
    public function Donate(Request $request){

        if(auth()->check()){

            $total_donor_supports = auth()->user()->donor_total_supports;
        }
        else{

            $total_donor_supports = 0;
        }

        if($total_donor_supports >= 10000){

            $data['data'] = null;
            $data['message'] = trans('api.user_donation_full');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;

        }

        if(!$request->has('cost')){

            $data['data'] = null;
            $data['message'] = trans('api.cost_required');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }

        if(!$request->has('currency_id')){

            $data['data'] = null;
            $data['message'] = trans('api.currency_required');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }

        if(generalExchange($request->cost, currencySymbol($request->currency_id), 'GBP') > 10000){

            $data['data'] = null;
            $data['message'] = trans('api.user_donation_full_2');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;

        }

        $validator = Validator::make($request->all(), [

            'project_id' => 'required|exists:projects,id',
            'type' => 'required|in:once,monthly,gift,ambassador',
            'cost' => 'required|numeric|gt:0',
            'currency_id' => 'required|exists:currencies,id',
            'donate_anonymouse' => 'required|in:1,0',

            // validate type gift
            'gift_recievers' => 'required_if:type,gift|array',
            'gift_recievers.*.name' => 'required_if:type,gift',
            'gift_recievers.*.phone' => 'required_if:type,gift|min:9|max:14',
            'gift_recievers.*.email' => 'required_if:type,gift|email',
            
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        // check for cost (check if the project deserve the donation cost or not)

        // 1- get project and currency
        $project = $this->projectRepository->getWhereWith(['currency'], [['id', $request->project_id], ['nation_id', $this->nation_id]])->first();

        if(!$project){

            $data['data'] = null;
            $data['message'] = trans('api.project_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }

        // 2- check for cost
        $cost = $this->checkCost($request->cost, $request->currency_id, $project);

        if(!$cost){

            $data['data'] = null;
            $data['message'] = trans('api.large_donation_cost');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }

        // collect and create donate data
        $donate_data['user_id'] = auth()->user()->id;
        $donate_data['project_id'] = $request->project_id;
        $donate_data['status'] = 'cart';
        $donate_data['cost'] = $request->cost;
        $donate_data['cost_gbp'] = generalExchange($request->cost, currencySymbol($request->currency_id), 'GBP');
        $donate_data['type'] = $request->type;
        $user_name = \App::getLocale() == 'ar' ? auth()->user()->name_ar : auth()->user()->name_en;
        $donate_data['name_anonymouse'] = $request->donate_anonymouse == 1 ? trans('api.name_anonymouse') : $user_name;
        $donate_data['nation_id'] = $this->nation_id;
        $donate_data['currency_id'] = $request->currency_id;

        if($request->type == 'once' || $request->type == 'monthly' || $request->type == 'ambassador'){
            
            // Create donation
            $donation = $this->projectUserRepository->create($donate_data);

            if($donation){

                // return esponse with payment rouet
                $payment_route = route('create-payment-session', $donation->id);

                $data['data']['payment_route'] = $payment_route;
                $data['message'] = trans('api.donate_success');
                $data['status'] = true;
                $data['http_response'] = 201;

                return $data;
            }
            else{

                $data['data'] = null;
                $data['message'] = trans('api.failed');
                $data['status'] = false;
                $data['http_response'] = 400;

                return $data;
            }
        }
        else if($request->type == 'gift'){

            // Create donation
            $donation = $this->projectUserRepository->create($donate_data);

            if($donation){

                $emails = [];
            
                // create gifts list
                foreach($request->gift_recievers as $reciever){

                    $gift_data['sender_nick_name'] = $request->has('donate_anonymouse') ? $request->donate_anonymouse : trans('api.name_anonymouse');
                    $gift_data['user_id'] = auth()->user()->id;
                    $gift_data['gift_owner_name'] = $reciever['name'];
                    $gift_data['email'] = $reciever['email'];
                    $gift_data['phone'] = $reciever['phone'];
                    $gift_data['project_id'] = $request->project_id;

                    $gift = $this->giftRepository->create($gift_data);

                    array_push($emails, $reciever['email']);
                }

                // get remaining cost after donation
                $cost = exchange($project->cost, $project->currency_id, session('currency'));
                $donated = getTotalSupportsForProject($project);
                $remaining = $cost - $donated;

                // Notify gift recievers via email
                try{
                    sendMailGifts(
                        $emails,
                        $donation->project->name ,
                        $donation->project->image ? asset('storage/'.$donation->project->image) : asset('dashboard/app-assets/images/avatars/avatar.png'),
                        $remaining,
                        currencySymbol(session('currency')),
                        $request->has('donate_anonymouse') ? $request->donate_anonymouse : trans('api.name_anonymouse'),
                        route('web.projects.show', $project->slug),
                        $this->nation_id
                    ); 
                }
                catch (Throwable $e) {

                    $data['data'] = null;
                    $data['message'] = trans('api.email_failed');
                    $data['status'] = false;
                    $data['http_response'] = 400;
        
                    return $data;
        
                } 

                // return esponse with payment rouet
                $payment_route = route('create-payment-session', $donation->id);

                $data['data']['payment_route'] = $payment_route;
                $data['message'] = trans('api.donate_success');
                $data['status'] = true;
                $data['http_response'] = 201;

                return $data;
            }
            else{

                $data['data'] = null;
                $data['message'] = trans('api.failed');
                $data['status'] = false;
                $data['http_response'] = 400;

                return $data;
            }
        }

    }

    // check if project deserve donation cost
    public function checkCost($cost, $currency, $project){

        $session_currency = currencySymbol(session('currency'));

        // 1- get total donations for the project
        $total_donations = $project->total_supports_for_project;

        // 2- get remainig cost
        $remainig_cost = generalExchange($project->get_total, $project->currency->symbol, $session_currency) - $total_donations;

        // 3- convert cost currency to default currency
        $current_cost = exchange($cost, $currency, session('currency'));

        // 4- compare costs
        if($current_cost > $remainig_cost){

            return false;
        }

        return true;
    }

    // check if donor is blocked or not
    public function checkUserBlock(){

        $data['data']['blocked'] = auth()->user()->blocked;
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
        
    }
    
}
