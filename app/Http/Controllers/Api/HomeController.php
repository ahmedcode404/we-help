<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;

use App\Http\Resources\CharityCategoryResource;
use App\Http\Resources\ProjectResource;
use App\Http\Resources\SearchResource;

use Validator;

class HomeController extends Controller
{
    private $charityCategoryRepository;
    private $projectRepository;

    public function __construct(CharityCategoryRepositoryInterface $charityCategoryRepository,
                                ProjectRepositoryInterface $projectRepository){

        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            
            $this->middleware('auth:sanctum', ['only' => ['home']]);
        }

        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->projectRepository = $projectRepository;

        $this->middleware('sessions');
    }

    public function home(){
        
        $nation_id = getNationId();

        $categories = $this->charityCategoryRepository->limitWhere([['nation_id', $nation_id]], 4);

        $projects = $this->projectRepository->paginateWhereHasWith(['charity', 'ratings', 'currency'], 'charity', [['status', 'approved']], 4, [['profile', 0], ['nation_id', $nation_id],
                            ['status', 'approved'], ['active', 1]]);

        $data['data']['user_name'] = auth()->check() ? auth()->user()->name : null;
        $data['data']['user_image'] = auth()->check() && auth()->user()->image ? asset('storage/'.auth()->user()->image) : asset('web/images/main/user_avatar.png');
        $data['data']['categories'] = count($categories) > 0 ? CharityCategoryResource::collection($categories) : null;
        $data['data']['projects'] = count($projects) > 0 ? new ProjectResource($projects) : null;
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
        
    }

    public function getAllCategories(){

        $nation_id = getNationId();

        $categories = $this->charityCategoryRepository->getWhere([['nation_id', $nation_id]]);

        $data['data']['categories'] = count($categories) > 0 ? CharityCategoryResource::collection($categories) : null;
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;   

        return $data;

    }

    public function search(Request $request){

        $validator = Validator::make($request->all(), [
            'key' => 'required'
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        $token = htmlspecialchars($request->key);
        $token = strip_tags($token);

        
        $projects = $this->projectRepository->search(['profile', '=', 0],
                ['name', 'project_num', 'location', 'goals_ar', 'goals_en', 'desc_ar', 'desc_en', 'long_desc_ar', 'long_desc_en'], 
                $token ,['column' => 'id', 'dir' => 'DESC']);


        $data['data']['projects'] = count($projects) > 0 ? SearchResource::collection($projects) : null;
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;   

        return $data;

    }

    // public function search(){

    //     $nation_id = getNationId();

    //     $projects = $this->projectRepository->whereHasWith(['charity'], 'charity', [['status', 'approved']], [['profile', 0], ['nation_id', $nation_id],
    //                         ['status', 'approved'], ['active', 1]]);


    //     $data['data']['projects'] = count($projects) > 0 ? SearchResource::collection($projects) : null;
    //     $data['message'] = '';
    //     $data['status'] = true;
    //     $data['http_response'] = 200;   

    //     return $data;

    // }
}
