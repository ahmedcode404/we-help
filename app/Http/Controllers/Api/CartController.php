<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Repositories\CartRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;

use App\Http\Resources\CartResource;

use Validator;

class CartController extends Controller
{

    private $cartRepository;
    private $currencyRepository;
    private $projectRepository;

    public function __construct(CartRepositoryInterface $cartRepository,
                                CurrencyRepositoryInterface $currencyRepository,
                                ProjectRepositoryInterface $projectRepository){

        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {

            $this->middleware('auth:sanctum', ['only' => ['addToCart', 'getAllCarts', 'deleteCartItem']]);
        }

        $this->middleware('sessions');

        $this->projectRepository = $projectRepository;
        $this->cartRepository = $cartRepository;
        $this->currencyRepository = $currencyRepository;

        $this->nation_id = getNationId();
    }


    public function getAllCarts(){


        $carts = $this->getCarts();

        $sum = $this->cartTotal($carts);

        $data['data']['carts'] = CartResource::collection($carts);
        $data['data']['total_amount'] = $sum.' '.currencySymbol(session('currency'));
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    public function addToCart(Request $request){

        $validator = Validator::make($request->all(), [

            'project_id' => 'required|exists:projects,id',
            // 'cost' => 'required|numeric',
            'type' => 'required|in:once,monthly,gift,ambassador',
            'name_anonymouse' => 'nullable',
            'currency_id' => 'required|exists:currencies,id'
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        // check for project nation_id
        $project = $this->projectRepository->findOne($request->project_id);

        if($project->nation_id != $this->nation_id){

            $data['data'] = null;
            $data['message'] = trans('api.project_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;

        }

        // check for currency nation_id
        $currency = $this->currencyRepository->findOne($request->currency_id);

        if($currency->nation_id != $this->nation_id){

            $data['data'] = null;
            $data['message'] = trans('api.currency_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;

        }

        // check if project is already exist before or not
        if(auth()->check()){

            $cart_exists = auth()->user()->carts()->where('project_id', $request->project_id)->first();
        }
        else if(session()->has('cart_no')){

            $cart_exists = $this->cartRepository->getWhere([['cart_no', session('cart_no')], ['project_id', $request->project_id]])->first();
        }
        else{

            $cart_exists =  null;
        }

        if($cart_exists){

            $data['data'] = null;
            $data['message'] = trans('api.cart_exsist');
            $data['status'] = false;
            $data['http_response'] = 302;

            return $data;
        }

        // if project not exsist before, then add it
        if(auth()->check()){
            
            $cart_data['user_id'] = auth()->user()->id;
        }
        else if(session()->has('cart_no')){
            
            $cart_data['cart_no'] = session('cart_no');
            
        }
        else{
            
            session(['cart_no' => rand(1000,9999)]);
            $cart_data['cart_no'] = session('cart_no');
        }

        $cart_data['project_id'] = $request->project_id;
        $cart_data['status'] = 'cart';
        // $cart_data['cost'] = (float)$request->cost;
        $cart_data['cost'] = 0;
        $cart_data['type'] = $request->type;
        $cart_data['nation_id'] = $this->nation_id;
        $cart_data['currency_id'] = $request->currency_id;

        $cart = $this->cartRepository->create($cart_data);
        
        if($cart){

            $data['data'] = null;
            $data['message'] = trans('api.cart_added_success');
            $data['status'] = true;
            $data['http_response'] = 201;

            return $data;
        }
        else{
            
            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = true;
            $data['http_response'] = 400;

            return $data;
        }

    }

    public function deleteCartItem(Request $request){

        $validator = Validator::make($request->all(), [

            'cart_id' => 'required|exists:project_user,id',
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        $cart = $this->cartRepository->findOne($request->cart_id);

        if($cart->status != 'cart'){

            $data['data'] = null;
            $data['message'] = trans('api.cart_not_found');
            $data['status'] = false;
            $data['http_response'] = 204;

            return $data;
        }

        $deleted =  $this->cartRepository->delete($request->cart_id);

        if($deleted){

            $carts = $this->getCarts();

            $sum = $this->cartTotal($carts);

            $data['data']['total_amount'] = $sum.' '.currencySymbol(session('currency'));
            $data['message'] = trans('api.delete_success');
            $data['status'] = true;
            $data['http_response'] = 204;

            return $data;
        }
        else{
            
            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;

        }
    }

    public function cartTotal($carts){

        $sum = 0;

        if(count($carts) > 0){

            foreach($carts as $cart){

                $sum += exchange($cart->cost, $cart->currency_id, $this->nation_id);
            }
        }

        return $sum;
    }

    public function getCarts(){

        if(auth()->check()){

            $carts = $this->cartRepository->getWhereWith(['project', 'project.ratings'], [['user_id', auth()->user()->id] , ['status' , 'cart']]);
        }
        else if(session()->has('cart_no')){

            $carts = $this->cartRepository->getWhereWith(['project', 'project.ratings'], [['cart_no', session('cart_no')] , ['status' , 'cart']]);

        }
        else{

            $carts = [];
        }

        return $carts;
    }
}
