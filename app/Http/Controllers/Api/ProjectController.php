<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\RatingCriteriaRepositoryInterface;
use App\Repositories\RatingRepositoryInterface;

use App\Http\Resources\ProjectDetailsResource;
use App\Http\Resources\CommentResource;
use App\Http\Resources\ProjectResource;
use App\Http\Resources\CountryResource;

use Validator;


class ProjectController extends Controller
{
    private $projectRepository;
    private $charityCategoryRepository;
    private $ratingCriteriaRepository;
    private $ratingRepository;
    private $nation_id;

    public function __construct(ProjectRepositoryInterface $projectRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                RatingCriteriaRepositoryInterface $ratingCriteriaRepository,
                                RatingRepositoryInterface $ratingRepository){

        if (array_key_exists('HTTP_AUTHORIZATION', $_SERVER)) {
            $this->middleware('auth:sanctum', ['only' => ['addToCart', 'rateProject']]);
        }

        $this->middleware('auth:sanctum', ['only' => ['rateProject']]);

        $this->projectRepository = $projectRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->ratingCriteriaRepository = $ratingCriteriaRepository;
        $this->ratingRepository = $ratingRepository;

        $this->middleware('sessions');

        $this->nation_id = getNationId();
    }

    public function getProjectDetails($id){

        $project = $this->projectRepository->findWith($id, ['images', 'charity', 'ratings', 'ratings.user', 'currency']);

        if($project){

            $data['data']['project'] = new ProjectDetailsResource($project);
            $data['message'] = '';
            $data['status'] = true;
            $data['http_response'] = 200;

            return $data;
        }

        $data['data'] = null;
        $data['message'] = trans('api.not_found');
        $data['status'] = false;
        $data['http_response'] = 404;

        return $data;
    }

    public function getCategoryProjects($id){

        $nation_id = getNationId();

        $category = $this->charityCategoryRepository->getWhere([['nation_id', $nation_id], ['id', $id]])->first();

        if($category){

            $projects = $this->projectRepository->paginateWhereHasWith(['charity', 'ratings'], 'charity', [['status', 'approved']], 4, [['profile', 0], ['nation_id', $nation_id],
                            ['status', 'approved'], ['active', 1], ['charity_category_id', $id]]);

            $data['data']['projects'] = count($projects) > 0 ? new ProjectResource($projects) : null;
            $data['message'] = '';
            $data['status'] = true;
            $data['http_response'] = 200;

            return $data;

        }
        else{

            $data['data'] = null;
            $data['message'] = trans('api.not_found');
            $data['status'] = false;
            $data['http_respone'] = 404;

            return $data;
        }

        
    }


    public function getRatingCriteria(){

        $rating_criterias = $this->ratingCriteriaRepository->getWhere([['nation_id', $this->nation_id]], ['column' => 'id', 'dir' => 'ASC']);

        $data['data']['rating_criterias'] = CountryResource::collection($rating_criterias);
        $data['message'] = '';
        $data['status'] = true;
        $data['http_response'] = 200;

        return $data;
    }

    public function rateProject(Request $request){

        $validator = Validator::make($request->all(), [

            'project_id' => 'required|exists:projects,id',
            'ratings' => 'required|array|min:5|max:5',
            'ratings.*.rating_criteria_id' => 'required|exists:rating__criterias,id',
            'ratings.*.grade' => 'required|integer|in:1,2,3,4,5',
            'comment' => 'required',
        ]);

        if($validator->fails()){

            return apiValidation($validator);
        }

        $project = $this->projectRepository->getWhere([['id', $request->project_id], ['nation_id', $this->nation_id]])->first();

        if(!$project){

            $data['data'] = null;
            $data['message'] = trans('api.project_not_found');
            $data['status'] = false;
            $data['http_response'] = 404;

            return $data;
        }
        
        foreach($request->ratings as $rating){
            
            $rating_criteria = $this->ratingCriteriaRepository->getWhere([['id', $rating['rating_criteria_id']], ['nation_id', $this->nation_id]])->first();

            if(!$rating_criteria){

                $data['data'] = null;
                $data['message'] = trans('api.criteria_not_found');
                $data['status'] = false;
                $data['http_response'] = 404;
    
                return $data;
            }

            $rateing_data['user_id'] = auth()->user()->id;
            $rateing_data['project_id'] = $request->project_id;
            $rateing_data['rating_criteria_id'] = $rating['rating_criteria_id'];
            $rateing_data['grade'] = $rating['grade'];

            $rate = $this->ratingRepository->create($rateing_data);
        }

        if($rate){

            $comment = clearText($request->comment);
            $rate_comment = $this->ratingRepository->updateWhere(['comment' => $comment], [['id', $rate->id]]);

            $data['data'] = null;
            $data['message'] = trans('api.rate_success');
            $data['status'] = true;
            $data['http_response'] = 201;

            return $data;
        }
        else{
            
            $data['data'] = null;
            $data['message'] = trans('api.failed');
            $data['status'] = false;
            $data['http_response'] = 400;

            return $data;
        }

    }

}
