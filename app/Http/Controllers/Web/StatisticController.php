<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\PartenerRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use Illuminate\Http\Request;
use App;
class StatisticController extends Controller
{

    public $projectrepository;
    public $supportrepository;
    public $cityrepository;
    public $partenerrepository;
    public $settingrepository;

    public function __construct(ProjectRepositoryInterface $projectrepository ,
                                SupportRepositoryInterface $supportrepository,
                                CityRepositoryInterface $cityrepository,
                                PartenerRepositoryInterface $partenerrepository,
                                SettingRepositoryInterface $settingrepository
    )
    {

        $this->projectrepository  = $projectrepository;
        $this->supportrepository  = $supportrepository;
        $this->cityrepository     = $cityrepository;
        $this->partenerrepository = $partenerrepository;
        $this->settingrepository  = $settingrepository;

    } // end of construct

    public function index()
    {
        $nation_id = getNationId();

        // get data 
        $benef_count    = $this->projectrepository->getBenefCount([['nation_id', $nation_id]]);
        $total_count   = $this->supportrepository->totalAmmount([['nation_id', $nation_id]]);
        $country_count  = $this->cityrepository->getWhere([['nation_id', $nation_id]]);
        $project_count  = count($this->projectrepository->getWhere([['nation_id', $nation_id]]));
        $partener_count = count($this->partenerrepository->getWhere([['nation_id', $nation_id]]));

        // get text
        
        if($nation_id == 1)
        {

            $text_country = $this->settingrepository->where(['key' => 'text_country_sa_' . App::getLocale()]);
            $text_project = $this->settingrepository->where(['key' => 'text_project_sa_' . App::getLocale()]);
            $text_partener = $this->settingrepository->where(['key' => 'text_partener_sa_' . App::getLocale()]);
            $text_amount = $this->settingrepository->where(['key' => 'text_amount_sa_' . App::getLocale()]);
            $text_beneficiary = $this->settingrepository->where(['key' => 'text_beneficiary_sa_' . App::getLocale()]);
        
        } else 
        {

            $text_country = $this->settingrepository->where(['key' => 'text_country_uk_' . App::getLocale()]);
            $text_project = $this->settingrepository->where(['key' => 'text_project_uk_' . App::getLocale()]);
            $text_partener = $this->settingrepository->where(['key' => 'text_partener_uk_' . App::getLocale()]);
            $text_amount = $this->settingrepository->where(['key' => 'text_amount_uk_' . App::getLocale()]);
            $text_beneficiary = $this->settingrepository->where(['key' => 'text_beneficiary_uk_' . App::getLocale()]);
                    
        }

        return view('web.statistics.statistic' , compact(
                'partener_count' ,
                'project_count' ,
                'country_count' ,
                'total_count' ,
                'benef_count',
                'text_country',
                'text_project',
                'text_partener',
                'text_amount',
                'text_beneficiary',
            ));

    }

}
