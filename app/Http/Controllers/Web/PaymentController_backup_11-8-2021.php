<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\FinancialRequestRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use Illuminate\Support\Str;
use App\Servicies\Notify;

class PaymentController extends Controller
{
    private $supportRepository;
    private $financialRequestRepository;
    private $notificationRepository;

    public function __construct(SupportRepositoryInterface $supportRepository,
                                FinancialRequestRepositoryInterface $financialRequestRepository,
                                NotificationRepositoryInterface $notificationRepository){

        $this->supportRepository = $supportRepository;
        $this->financialRequestRepository = $financialRequestRepository;
        $this->notificationRepository = $notificationRepository;

    }

    public function createPaymentSession($support_id){

        $support = $this->supportRepository->findWith($support_id, ['project']);

        // create session
        require '../vendor/autoload.php';
        \Stripe\Stripe::setApiKey('sk_test_51JCQI8IoY6AGxvlCOndhF0syV5KFm3XvmxaFzj42VeCE3qFRXnbNauCRwmDeIrmqw1UoekBkUG6OaTigG85kerQo00YPszLvBc');

        header('Content-Type: application/json');

        $checkout_session = \Stripe\Checkout\Session::create([
        'payment_method_types' => ['card'],
        'line_items' => [[
            'price_data' => [
            'currency' => currencySymbol($support->currency_id),
            'unit_amount' => $support->cost * 100, // المزود بياخد الفلوس بالسنت .. سؤال للدعم الفنى
            'product_data' => [
                'name' => $support->project->name,
                'images' => [$support->project->image],
            ],
            ],
            'quantity' => 1,
        ]],
        'mode' => 'payment',
        'success_url' => route('payment.success', $support->id),
        'cancel_url' => route('payment.cancle', $support->id),
        ]);

        header("HTTP/1.1 303 See Other");
        header("Location: " . $checkout_session->url);

        return redirect($checkout_session->url);

    }

    public function success($id){

        $support = $this->supportRepository->findOne($id);

        // Create catch voucher - انشاء سند القبض
        $voucher_data['slug'] = Str::random(12);
        $voucher_data['out_to_charity'] = $support->cost;
        $voucher_data['requestable_id'] = $support->user_id;
        $voucher_data['requestable_type'] = 'App\Models\User';
        $voucher_data['project_id'] = $support->project_id;
        $voucher_data['status'] = 'approved';
        $voucher_data['type'] = 'catch';
        $voucher_data['currency_id'] = $support->currency_id;
        $voucher_data['bank_account_num'] = null;
        $voucher_data['voucher_num'] = null;

        $voucher = $this->financialRequestRepository->create($voucher_data);

        // Email Voucher to user
        $this->emailVoucher($voucher);

        return view('web.payment.success');

    }

    public function cancle($id){
        
        $support = $this->supportRepository->findOne($id);

        $user_id = $support->user_id;

        // delete support
        $deleted = $this->supportRepository->deleteWhere([['id', $id]]);

        if($deleted){

            // Notify user
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تم إلغاء تبرعك لعدم إتمام الدفع';
            $message->message_en = 'Your donation has been canceled due to non-payment';

            $notification_data['message_en'] = $message->message_en;
            $notification_data['message_ar'] = $message->message_ar;
            $notification_data['send_user_id'] = $user_id;
            $notification_data['title'] = 'support_canceled';
            $notification_data['type'] = 'user';

            $this->notificationRepository->create($notification_data);            

            Notify::NotifyWeb($message , 'support_canceled' , 'user' , $user_id); 
            Notify::NotifyMobile($message , 'support_canceled' , 'user' , $user_id); 

        }

        return redirect()->route('web.home');

    }


    public function emailVoucher($voucher){

        $msg = '';
        $email = [['email' => $voucher->requestable->email]];
        $topic = trans('admin.support_voucher');
        $nation_id = $voucher->requestable->nation_id;
        $notes = $voucher->notes;

        $msg = trans('admin.your_support_voucher');
        $notify_ar = 'فاتورة تبرع';
        $notify_en = 'Support Voucher';

        // create exchange voucher link - ارسال طلب الصرف - فاتوره بالمبلغ اللى هيتم تحويله للجمعيه
        $voucher_link = '<a href="'.route('get-voucher', $voucher->slug).'">'.trans('admin.show').'</a>';

        // notification 
        $message = new \stdClass();
        $message->message_ar = $notify_ar;
        $message->message_en = $notify_en;

        $notification_data['message_en'] = $message->message_en;
        $notification_data['message_ar'] = $message->message_ar;
        $notification_data['send_user_id'] = $voucher->requestable->id;
        $notification_data['title'] = 'financial_request';
        $notification_data['type'] = 'user';

        $this->notificationRepository->create($notification_data);            

        Notify::NotifyWeb($message , 'financial_request' , 'user' , $voucher->requestable->id); 
        Notify::NotifyMobile($message , 'financial_request' , 'user' , $voucher->requestable->id); 

        // send email
        try {
    
            sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

        } catch (Throwable $e) {

            return response()->json(['data' => 'exception'], 200);

        }  
    }
}
