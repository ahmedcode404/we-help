<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use Illuminate\Http\Request;
use Validator;

class PasswordController extends Controller
{

    private $charityRepository;
    private $userRepository;

    public function __construct(CharityRepositoryInterface $charityRepository , UserRepositoryInterface $userRepository)
    {

        $this->charityRepository = $charityRepository;
        $this->userRepository = $userRepository;
        
    }

    // return email form view
    public function getEmailForm($role){

        return view('web.auth.password.email-form', compact('role'));
    }


    // Send verification code to the entered email
    public function sendVerificationCode(Request $request, $role){

        if($role == 'admin' || $role == 'donor'){

            $rules = [
                'email' => 'required|email|exists:users,email'
            ];

        }
        else if($role == 'association'){

            $rules = [
                'email' => 'required|email|exists:charities,email'
            ];
        }

        $customMessages = [
            'required' => trans('web.email_required'),
            'email' => trans('web.email_invalid'),
            'exists' => trans('web.email_exists'),
        ];
    
        $this->validate($request, $rules, $customMessages);
        

        // admin - emp - donor
        if($role == 'admin' || $role == 'donor'){

            $user = $this->userRepository->getWhere([['email', $request->email]])->first();
            $name = $user->name;

        }
        // charity
        else if($role == 'association'){

            $user = $this->charityRepository->getWhere([['email', $request->email]])->first();

            if($user->hasRole('charity')){
                $name = $user->name;
            }
            else{
                $name = $user->emp_name; 
            }
        }

        if($user){

            return $this->sendCode($user->email, $name, $role);

        }
        else{

            session()->flash('error', trans('web.error'));

            return redirect()->back();
        }

    }

    // Actually send/resend code in email
    public function sendCode($email, $name, $role){

        // create code
        $code = mt_rand(1000,9999);

        // update user with new verificatio_code
        if($role == 'admin' || $role == 'donor'){

            $this->userRepository->updateWhere(['verification_code' => $code], [['email', $email]]);
        }
        else if($role == 'association'){

            $this->charityRepository->updateWhere(['verification_code' => $code], [['email', $email]]);
        }

        // Send Verification code - Email
        $users = [
            [
                'name' => $name,
                'email' => $email,
            ]
        ];

        $topic = trans('web.verification_code_topic');
        $message = trans('web.verification_code_msg');
        $nation_id = getNationId();
        $notes = $code;

        try {

            sendMail($users, $topic, $message, $nation_id, $notes);

        } catch (Throwable $e) {

            return redirect()->back()->with('error', trans('web.error'));

        } 

        session()->flash('success', trans('web.code_sent'));
        
        return view('web.auth.password.verification-code', compact('code', 'email', 'name', 'role'))->with('success', trans('web.code_sent_success'));
    }

    // verify entered code
    public function verify(Request $request){
        
        $this->validate($request, [
            'code' => 'required'
        ]);

        $role = $request->role;
        $email = $request->email;

        // Build full code
        $code = '';

        foreach($request->code as $digit){

            $code .= $digit;
        }

        // get user
        if($role == 'admin' || $role == 'donor'){

            $user = $this->userRepository->getWhere([['email', $request->email]])->first();
        }
        else if($role == 'association'){

            $user = $this->charityRepository->getWhere([['email', $request->email]])->first();
        }
        
        $name = $user->name;

        if($user && $user->verification_code == $code){

            session()->flash('success', __('web.correct_code'));

            return view('web.auth.password.change-password', compact('code', 'email', 'name', 'role'));
        }
        else{

            session()->flash('error', __('web.invalide_code'));

            return view('web.auth.password.verification-code', compact('code', 'email', 'name', 'role'));
        }

    }

    // Actually change password
    public function changePassword(Request $request){

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6|confirmed' 
        ]);

        if($validator->fails()){

            $code = $request->code;
            $name = $request->name;
            $email = $request->email;
            $role = $request->role;

            session()->flash('error', trans('web.invalid_password'));
        
            return view('web.auth.password.verification-code', compact('code', 'email', 'name', 'role'));
        }

        $this->validate($request, [
            'password' => 'required|min:6|confirmed'
        ]);

        $updated = 0;
        // admin - emp - donor
        if($request->role == 'admin' || $request->role == 'donor'){

            $updated = $this->userRepository->updateWhere(['password' => bcrypt($request->password)], [['email', $request->email]]);

        }
        // charity
        else if($request->role == 'association'){

            $updated = $this->charityRepository->updateWhere(['password' => bcrypt($request->password)], [['email', $request->email]]);
        }

        if($updated){

            session()->flash('success', __("web.password_changed_success"));

            return redirect()->route('web.home');
        
        }
        else{

            session()->flash('error', __("web.error"));

            return redirect()->route('web.home');
        }
    }
}
