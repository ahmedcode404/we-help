<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\StaticPage;
use App\Repositories\FaqRepositoryInterface;
use Illuminate\Http\Request;
use App\Repositories\StaticPageRepositoryInterface;

class StaticPageController extends Controller
{
    private $staticPageRepository;
    private $faqrepository;

    public function __construct(StaticPageRepositoryInterface $staticPageRepository , FaqRepositoryInterface $faqrepository)
    {

        $this->staticPageRepository = $staticPageRepository;
        $this->faqrepository        = $faqrepository;

    } // end of construct

    public function index($key)
    {

        $data['page'] = $this->staticPageRepository->getWhere([['key', $key], ['nation_id', getNationId()]])->first();

        return view('web.static-pages.index')->with([
            'data' => $data
        ]);
    } // end of index

    public function about()
    {

        $about              = StaticPage::aboutus();
        $about_corporation  = StaticPage::AboutCorporation();
        $vision_and_mission = StaticPage::VisionMission();
        $our_goal           = StaticPage::Ourgoal();
        $our_value          = StaticPage::Ourvalue();
        $our_service        = StaticPage::Ourservice();
        $feature            = StaticPage::Feature();
        
        
        return view('web.static-pages.about' , compact(
                'about' ,
                'about_corporation' ,
                'vision_and_mission' ,
                'our_goal' ,
                'our_value',
                'our_service',
                'feature'
            ));

    } // end of about

    public function faq()
    {

        $questions = $this->faqrepository->getWhere(['nation_id' => getNationId()]);

        return view('web.static-pages.faq' , compact( 'questions' ));

    } // end of faq     

}
