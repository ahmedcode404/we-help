<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SliderRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\PartenerRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\StaticPageRepositoryInterface;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Http\Requests\Web\ContactUsRequest;

class HomeController extends Controller
{


    private $sliderRepository;
    private $projectRepository;
    private $charityCategoryRepository;
    private $currencyrepository;
    private $partenerRepository;
    private $supportRepository;
    private $cityRepository;
    private $staticPageRepository;
    private $contactRepository;
    public $settingrepository;
    public $userrepository;
    
    public function __construct(SliderRepositoryInterface $sliderRepository,
                                ProjectRepositoryInterface $projectRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                CurrencyRepositoryInterface $currencyrepository,
                                PartenerRepositoryInterface $partenerRepository,
                                SupportRepositoryInterface $supportRepository,
                                CityRepositoryInterface $cityRepository,
                                StaticPageRepositoryInterface $staticPageRepository,
                                ContactRepositoryInterface $contactRepository,
                                SettingRepositoryInterface $settingrepository,
                                UserRepositoryInterface $userrepository
                                ){
        
        $this->sliderRepository = $sliderRepository;
        $this->projectRepository = $projectRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->currencyrepository = $currencyrepository;
        $this->partenerRepository = $partenerRepository;
        $this->supportRepository = $supportRepository;
        $this->cityRepository = $cityRepository;
        $this->staticPageRepository = $staticPageRepository;
        $this->contactRepository = $contactRepository;
        $this->settingrepository = $settingrepository;
        $this->userrepository = $userrepository;
    }
    
    public function index(){
        

        session()->forget('support_id');
        
        $nation_id = getNationId();
        
        $data['slider'] = $this->sliderRepository->getWhere([['nation_id', $nation_id], ['appearance', 'web']]);

        // if(session()->has('session-country')){

        //     $data['projects'] = $this->projectRepository->whereHasWith(['charity', 'phases', 'currency'], 'charity', [['status', 'approved']], [['profile', 0], ['nation_id', $nation_id] , 
        //                         ['country', session('session-country')] , ['status', 'approved'], ['active', 1]]);
        // }
        // else{
        //     $data['projects'] = $this->projectRepository->whereHasWithLimit(['charity', 'phases', 'currency', 'ratings', 'donates', 'donates.currency'], 'charity', [['status', 'approved']], [['profile', 0], ['nation_id', $nation_id] , 
        //                         ['status', 'approved'], ['active', 1]], 6);

        // }

        $data['session_currency'] = currencySymbol(session('currency'));

        // $data['categories'] = $this->charityCategoryRepository->getWhereWith(['projects', 'projects.currency', 'projects.phases', 
        //             'projects.ratings', 'projects.donates', 'projects.donates.currency', 'projects.charity'], [['nation_id', $nation_id]], 
        //             ['column' => 'id', 'dir' => 'ASC'])->map(function($category){
        //                 $category->setRelation('projects', $category->projects->take(6));

        //                 return $category;
        //             });

        $data['categories'] = $this->charityCategoryRepository->getCategories($nation_id);
        
        $data['projects_count'] = $this->projectRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['parteners_count'] = $this->partenerRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['donation_ammounts'] = $this->supportRepository->totalAmmount([['nation_id', $nation_id]], $data['session_currency']);

        // dd($data['donation_ammounts']);
        // $supports = $this->supportRepository->getWhereWith(['currency'], [['status', 'support'], ['nation_id', $nation_id]]);

        // $default_currency = currencySymbol(getSettingData('default_currency'));

        // $sum = 0;

        // foreach($supports as $key => $support){

        //     $sum += generalExchange($support->cost, $support->currency->name, $default_currency);
        // }

        // $data['donation_ammounts'] = $sum;


        if(auth()->check()){

            $user = $this->userrepository->getWhereWith(['donates', 'donates.currency'], [['created_at', '=', date('Y')], ['id', auth()->user()->id]])->first();
            // $donated_projects = $this->donates()->whereYear('created_at', '=', date('Y'))->get();
        
            if($user){

                $data['donor_total_supports'] = auth()->user()->donates()->sum('cost_gbp');

                // $data['donor_total_supports'] = 0;
    
                // foreach($user->donates as $donation){
        
                //     $data['donor_total_supports'] += generalExchange($donation->cost, $donation->currency->symbol, 'GBP');
                // }
            }
            else{

                $data['donor_total_supports'] = 0;
            }
            
        }
        else{
            $data['donor_total_supports'] = 0;
        }


        $data['countries'] = $this->cityRepository->countriesCount();
        $data['benef_count'] = $this->projectRepository->getBenefCount([['nation_id', $nation_id]]);
        $data['partners'] = $this->partenerRepository->getWhere([['nation_id', $nation_id]]);
        $data['about_us'] = $this->staticPageRepository->getWhere([['nation_id', $nation_id], ['key', 'about_us']])->first();


        if($nation_id == 1)
        {

            $data['lang'] = $this->settingrepository->getWhere([['key' , 'langtude_sa']])->first();
            $data['lat'] = $this->settingrepository->getWhere([['key' , 'latitude_sa']])->first();
            
        } else 
        {

            $data['lang'] = $this->settingrepository->getWhere([['key' , 'langtude_uk']])->first();
            $data['lat'] = $this->settingrepository->getWhere([['key' , 'latitude_uk']])->first();

        }

        return view('web.home.index')->with([
            'data' => $data
        ]);
    }

    public function storeContactUs(Request $request){

        $data = $request->except(['_token', '_method', 'name', 'phone', 'content']);

        $nation_id = getNationId();

        $name = htmlspecialchars($request->name);
        $name = strip_tags($name);
        $data['name'] = $name;

        $phone = htmlspecialchars($request->phone);
        $phone = strip_tags($phone);
        $data['phone'] = $phone;

        $content = htmlspecialchars($request->content);
        $content = strip_tags($content);
        $data['content'] = $content;

        $data['nation_id'] = $nation_id;

        $contact = $this->contactRepository->create($data);

        if($contact){

            session()->flash('success', __('web.contact_success'));

            return back();
        }
        else{

            session()->flash('error', __('web.contact_failed'));

            return back();

        }
    }
}
