<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\DontateRequest;
use App\Http\Requests\Web\PriceDontateRequest;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\ProjectUserRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\FinancialRequestRepositoryInterface;
use Illuminate\Http\Request;
use Auth;
use App;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\GiftRepositoryInterface;
use Illuminate\Support\Str;
use App\Models\Notification;
use App\Servicies\Notify;


class DonateController extends Controller
{
    private $currencyrepository;
    private $projectuserrepository;
    private $giftrepository;
    private $projectrepository;
    private $charitycategoryrepository;
    private $financialRequestRepository;

    public function __construct(CurrencyRepositoryInterface $currencyrepository ,
                                ProjectUserRepositoryInterface $projectuserrepository,
                                GiftRepositoryInterface $giftrepository,
                                ProjectRepositoryInterface $projectrepository,
                                CharityCategoryRepositoryInterface $charitycategoryrepository,
                                FinancialRequestRepositoryInterface $financialRequestRepository
    )
    {

        $this->currencyrepository        = $currencyrepository;
        $this->projectuserrepository     = $projectuserrepository;
        $this->giftrepository            = $giftrepository;
        $this->projectrepository         = $projectrepository;
        $this->charitycategoryrepository = $charitycategoryrepository;
        $this->financialRequestRepository = $financialRequestRepository;

    } // end of construct

    public function index()
    {

        $nation_id = getNationId();

        $charitycategories = $this->charitycategoryrepository->getWhere([['nation_id', $nation_id]]);

        $currencies = $this->currencyrepository->nation($nation_id);


        return view('web.donates.donate' , compact('charitycategories' , 'currencies'));
    }

    public function getProjectOfCategory(Request $request)
    {

        $projects = $this->projectrepository->getWhere([['charity_category_id', $request->id], ['status', 'approved'], ['active', 1], ['profile', 0],
                    ['donation_complete', 0]]);
        
        return view('web.donates.get_project_ofcategory' , compact('projects'))->render();
    }

    public function checkProject(Request $request)
    {

        // get project of donate 
        $project = $this->projectrepository->findOne($request->id);
        // check cost donate == project cost
        $result = getTotalSupportsForProject($project) == exchange($project->cost, $project->currency_id, session('currency')) ? true : false;

        return response()->json(['data' => $result]);
    }

    public function checkPrice(PriceDontateRequest $request)
    {
        if($request->has('currency_id') && $request->has('number_input')){

            if(auth()->check()){

                $total_donor_supports = auth()->user()->donates()->sum('cost_gbp');
            }
            else{
    
                $total_donor_supports = 0;
            }

            $exchanged_donations = generalExchange((int)$request->number_input, currencySymbol($request->currency_id), 'GBP');

            if( ($total_donor_supports + $exchanged_donations) > 10000){

                $allowed_cost = round(10000 - $total_donor_supports, 2);

                return response()->json([
                
                    'project_id'   => -1,
                    'allowed_cost' => $allowed_cost

                ]); 
            }

            if($request->number_input == 0){

                return response()->json([
                
                    'project_id'   => -2,
                    'allowed_cost' => ''

                ]); 
            }
            
        }

        if($request->number_input)
        {

            $currency = $this->currencyrepository->findOne($request->currency_id);
            
            return response()->json([
            
                'project_id'   => $request->project_id,
                'currency'     => $currency->symbol,
                'currency_id'  => $currency->id,
                'number_input' => $request->number_input,
                'price'        => $request->number_input,

            ]);

        } else 
        {

            return response()->json([
            
                'project_id'   => $request->project_id,
                'allowed_cost' => 0

            ]); 

        } // end of else if

    } // end of check price
 
    public function store(DontateRequest $request)
    {

        // condition of cart
        if ($request->cart) {

            $this->projectuserrepository->delete($request->id);

        } // end of condition of cart

        $request_currency = currencySymbol($request->currency_id);

        //////////////////////////////////////////////// add donate type once //////////////////////////////////
        if ($request->type == 'once') {
            
            $attribute = $request->except('_token' , 'price');

            $attribute['cost']      = $request->price;
            $attribute['cost_gbp']  = generalExchange($request->price, $request_currency, 'GBP');
            $attribute['user_id']   = Auth::user()->id;
            $attribute['status']    = 'cart';
            $attribute['nation_id'] = getNationId();

            // condition if user check name anonymouse
            if ($request->once_check_1) {
                
                $attribute['name_anonymouse'] = App::getLocale() == 'ar' ? 'متبرع مجهول' : 'Name Anonymouse';

            }
            
            $project_user = $this->projectuserrepository->create($attribute);

        }
        //////////////////////////////////////////////// add donate type month //////////////////////////////////
        
        else if($request->type == 'monthly') 
        {
            
            $attribute = $request->except('_token' , 'price');

            $attribute['cost']      = $request->price;
            $attribute['cost_gbp']  = generalExchange($request->price, $request_currency, 'GBP');
            $attribute['user_id']   = Auth::user()->id;
            $attribute['status']    = 'cart';            
            $attribute['nation_id'] = getNationId();

            // condition if user check name anonymouse
            if ($request->month_check_1) {
                
                $attribute['name_anonymouse'] = App::getLocale() == 'ar' ? 'متبرع مجهول' : 'Name Anonymouse';

            }

            $project_user = $this->projectuserrepository->create($attribute); 

        } 
        //////////////////////////////////////////////// add donate type gift //////////////////////////////////
        
        else if($request->type == 'gift')
        {
        
            $attribute = $request->except('_token' , 'price' , 'name2' , 'tel2' , 'email2');

            $attribute['cost']      = $request->price;
            $attribute['cost_gbp']  = generalExchange($request->price, $request_currency, 'GBP');
            $attribute['user_id']   = Auth::user()->id;
            $attribute['status']    = 'cart';            
            $attribute['nation_id'] = getNationId();

            // condition if user check name anonymouse
            if ($request->check_14) {
                
                $attribute['name_anonymouse'] = App::getLocale() == 'ar' ? 'متبرع مجهول' : 'Name Anonymouse';

            }

            // create project user -> support
            $project_user = $this->projectuserrepository->create($attribute);

            // foreach save data to gifts
            foreach ($request->name2 as $key => $name) {
                
                $this->giftrepository->create([

                    'gift_owner_name'  => $name,
                    'project_id'       => $request->project_id,
                    'sender_nick_name' => $request->name,
                    'user_id'          => Auth::user()->id,
                    'email'            => $request->email2[$key],
                    'phone'            => $request->tel2[$key],

                ]); // end of create

            } // end of foreach

            // get cost order by price form session
            $cost = exchange($project_user->project->cost, $project_user->project->currency_id, session('currency'));
            $donated = getTotalSupportsForProject($project_user->project);
            $reamining = $cost - $donated;

            // send mail gifts to emails
            sendMailGifts(
                $request->email2,
                $project_user->project->name ,
                isset($project_user->project->image) ? $project_user->project->image : 'project/default.png',
                $reamining,
                currencySymbol(session('currency')),
                isset($request->name) ? $request->name : 'شخص مجهول' ,
                \URL::to('/').'/projects/'.$project_user->project->slug ,
                getNationId()
            );                  

        }
        //////////////////////////////////////////////// add donate type ambassador //////////////////////////////////
        else 
        {

            $attribute = $request->except('_token' , 'price_ambsaador');

            $attribute['cost']      = $request->price_ambsaador;
            $attribute['cost_gbp']  = generalExchange($request->price_ambsaador, $request_currency, 'GBP');
            $attribute['user_id']   = Auth::user()->id;
            $attribute['status']    = 'cart';            
            $attribute['nation_id'] = getNationId();

            $project_user = $this->projectuserrepository->create($attribute);        

        }

        // $project = $this->projectrepository->findWith($request->project_id, ['currency']);
        
        // if(generalExchange($project->get_total, $project->currency->symbol, currencySymbol(session('currency'))) > getTotalSupportsForProject($project)){

        //     $project_data['donation_complete'] =  0;
        // }
        // else{

        //     $project_data['donation_complete'] =  1;
        // }

        // $this->projectrepository->update($project_data, $request->project_id);

        $route = route('create-payment-session', $project_user->id);

        return response()->json([
            'checkoute_route' => $route
        ], 200);

    } // end of store

} // end of class
