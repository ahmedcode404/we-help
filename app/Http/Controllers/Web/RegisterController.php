<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\BankRepositoryInterface;
use App\Repositories\WorkFieldRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CartRepositoryInterface;
use App\Http\Requests\Web\DonorRequest;
use App\Http\Requests\Web\RegisterCharityRequest;
use App\Servicies\Notify;
use App\Models\Notification;
use Auth;
use Spatie\Permission\Models\Permission;
use Validator;

use App\Rules\PhoneNumber;
use App\Rules\TextLength;
use App\Rules\NationCode;
use App\Rules\IBAN;
use App\Rules\SWIFT;

use Monarobase\CountryList\CountryListFacade;

class RegisterController extends Controller
{
    private $cityRepository;
    private $userRepository;
    private $bankRepository;
    private $workFieldRepository;
    private $charityRepository;
    private $cartrepository;

    public function __construct(CityRepositoryInterface $cityRepository,
                                UserRepositoryInterface $userRepository,
                                BankRepositoryInterface $bankRepository,
                                WorkFieldRepositoryInterface $workFieldRepository,
                                CharityRepositoryInterface $charityRepository,
                                CartRepositoryInterface $cartrepository){

        $this->cityRepository = $cityRepository;
        $this->userRepository = $userRepository;
        $this->bankRepository = $bankRepository;
        $this->workFieldRepository = $workFieldRepository;
        $this->charityRepository = $charityRepository;
        $this->cartrepository = $cartrepository;

    }

    public function getRegisterForm($role){

        $nation_id =getNationId();

        // $data['countries'] = $this->cityRepository->getWhere([['nation_id', $nation_id]]);

        $data['countries'] = CountryListFacade::getList(\App::getLocale());

        if($role == 'donor'){

            return view('web.auth.donor-register')->with([
                'data' => $data
            ]);
        }
        else if($role == 'association'){

            // $data['work_fields'] = $this->workFieldRepository->getWhere([['nation_id', $nation_id]]);
            
            return view('web.auth.charity-register.index')->with([
                'data' => $data
            ]);
        }
    }

    public function registerDonor(DonorRequest $request){

        $exist_user = $this->userRepository->getWhere([['phone', $request->phone]])->first();

        if($exist_user){

            $response['status'] = 2;
            $response['reload'] = 0;
            $response['message'] = trans('admin.phone_exists');
            return $response;

        }
        
        $data = $request->except(['_token', '_method', 'role', 'agree', 'password_confirmation', 'password','name', 'phone_num']);

        $data['name_ar'] = $request->name;
        $data['name_en'] = $request->name;
        $data['blocked'] = 0;
        $data['nation_id'] = getNationId();
        $data['password'] = bcrypt($request->password);
        $data['currency_id'] = session('currency');

        $user = $this->userRepository->create($data);

        if($user){

            session()->forget('donor_phone');
            
            $user->assignRole('supporter');

            Auth::login($user);

            if(session()->has('cart_no')){

                $this->cartrepository->updateWhere(['user_id' => $user->id, 'cart_no' => null], [['cart_no', session('cart_no')]]);
                
                session()->forget('cart_no');
            }

            // return redirect()->route('web.home')->with('success', __('web.register_success'));

            $response['status'] = 1;
            $response['redirect'] = route('web.home');
            $response['reload'] = 0;
            return $response;
        }
        
        session()->flash('error', __("web.error"));

        return redirect()->back();
    }

    public function registerCharity(RegisterCharityRequest $request){
        
        if($request->has('projects_names') && $request->has('projects_types')){

            // remove the last null item

            if(count($request->projects_names) > 0 && $request->projects_names[0] != null){

                $validator;
                for($i = 0; $i < count($request->projects_names); $i++){

                    $this->validate($request, [

                        'images_'.($i+1) => 'required|array|min:1|max:6',
                    ]);
                    
                }
            }
            
        }

        $charity = $this->charityRepository->createCharity($request);

        if($charity){

            session()->forget('charity_phone');
            session()->forget('charity_mobile');
            session()->forget('bank_phone');

            // create special edit permission
            Permission::create(['name' => 'edit_charity_' . $charity->id]);

            // Create charity permissions
            $charity->createCharityPermissions();

            // Assign charity roles
            $charity->assignRole([
                'charity', 
                'charity_projects_management_'.$charity->id, 
                'charity_phases_management_'.$charity->id,
                'charity_employees_management_'.$charity->id,
                'charity_sponsers_management_'.$charity->id,
                'charity_exchange_bonds_management_'.$charity->id,
                'charity_job_categories_management_'.$charity->id,
                'charity_jobs_management_'.$charity->id,
                'charity_degrees_management_'.$charity->id,
                'charity_nationalities_management_'.$charity->id,
                'charity_reports_management_'.$charity->id,
            ]);

            Auth::guard('charity')->login($charity);

            // Notify admins that have permissions to accept and refuse charities
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تسجيل جمعية جديدة';
            $message->message_en = 'A New Charity is Registered';


            // GET ALL ADMINS THAT HAVE PERMISSION TO REVISE , ACCEPT AND REFUSE CHARITIES
            $admins = $this->userRepository->whereHas('permissions', [['name', 'accept_refuse_charities']]);

            // FOREACH ADMINS
            foreach($admins as $admin)    
            {

                Notification::create([ 

                    'message_en'     => $message->message_en,
                    'message_ar'     => $message->message_ar,
                    'send_user_id'   => $admin->id,
                    'model_id'       => $charity->id,
                    'title'          => 'new_charity',
                    'type'           => 'admin'
                ]);             
                
            } // end of foreach admins             
            
            // HEHRE FUNCTION SEND NOTIFICATION ALL ADMINS WHERE HAS ANY PERMISSIONS  
            Notify::NotifyWeb($message , null, 'adminOrEmployee' , null);

            // return redirect()->route('web.home')->with('success', __('web.register_success'));

            $response['status'] = 1;
            $response['redirect'] = route('charity.dashboard');
            $response['reload'] = 0;

            return $response;
        }
        
        // session()->flash('error', __("web.error"));

        // return redirect()->back();

    }

    public function getCities(Request $request){

        $data['cities'] = $this->cityRepository->getWhere([['parent_id', $request->id]]);

        // return response()->json([
        //     'cities' => $data['cities']
        // ], 200);

        return view('web.auth.charity-register.ajax.cities')->with([
            'data' => $data
        ]);

    }
}
