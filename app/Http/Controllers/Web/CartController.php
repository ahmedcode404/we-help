<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;

class CartController extends Controller
{

    public $cartrepository;
    public $currencyrepository;

    public function __construct(CartRepositoryInterface $cartrepository,
                                CurrencyRepositoryInterface $currencyrepository
                                )
    {
        
        $this->cartrepository = $cartrepository;
        $this->currencyrepository = $currencyrepository;


    } // end of construct

    // get all carts
    public function index(){

        if(auth()->check()){

            $data['carts'] = $this->cartrepository->getWhereWith(['project', 'project.charity', 'project.ratings', 'currency'], [['user_id', auth()->user()->id] , ['status' , 'cart']]);
        }
        else if(session()->has('cart_no')){

            $data['carts'] = $this->cartrepository->getWhereWith(['project', 'project.charity', 'project.ratings', 'currency'], [['cart_no', session('cart_no')] , ['status' , 'cart']]);

        }
        else{

            $data['carts'] = [];
        }

        $data['currencies'] = $this->currencyrepository->getWhere([['nation_id', getNationId()]]);

        return view('web.cart.index')->with([

            'data' => $data
        ]);

    }

    // add item to cart
    public function store(Request $request)
    {

        // check if project is already exist before or not
        if(auth()->check()){

            $carts = auth()->user()->carts()->where('project_id', $request->project_id)->first();
        }
        else if(session()->has('cart_no')){

            $carts = $this->cartrepository->getWhere([['cart_no', session('cart_no')], ['project_id', $request->project_id]])->first();
        }
        else{
            $carts =  null;
        }

        if($carts){

            return response()->json([
                'status' => 2,
                'msg' => __('web.cart_exsist'),
                'icon' => 'warning'
            ], 200);
        }

        // if project not exsist before, then add it
        $data = $request->except(['_token', '_method']);

        if(auth()->check()){
            
            $data['user_id'] = auth()->user()->id;
        }
        else if(session()->has('cart_no')){
        
            $data['cart_no'] = session('cart_no');
            
        }
        else{

            session(['cart_no' => rand(1000,9999)]);
            $data['cart_no'] = session('cart_no');
        }
        
        $data['status'] = 'cart';
        $data['nation_id'] = getNationId();

        $cart = $this->cartrepository->create($data);
        
        if($cart){

            return response()->json([
                'status' => 1,
                'msg' => __('web.add_cart_success'),
                'icon' => 'success'
            ], 200);
        }
        else{
            return response()->json([
                'status' => 1,
                'msg' => __('web.error'),
                'icon' => 'error'
            ], 200);
        }


    } // end of store


    // remove item from cart
    public function removeItem($id){

        $deleted = $this->cartrepository->delete($id);

        if($deleted){

            return response()->json([
                'status' => 1,
                'msg' => __('web.item_removed_success'),
                'icon' => 'success'
            ], 200);
        }
        else{

            return response()->json([
                'status' => 0,
                'msg' => __('web.error'),
                'icon' => 'error'
            ], 200);
        }

    }

    // get cart items to be shown in header
    public function getCartItems(){

        $carts = getCart('limit');

        return view('web.layout.ajax.cart', compact('carts'));
    }
}
