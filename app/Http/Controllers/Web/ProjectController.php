<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\CampaignGoalRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\RatingCriteriaRepositoryInterface;
use App\Repositories\RatingRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\MarketingRepositoryInterface;
use Illuminate\Http\Request;

class ProjectController extends Controller
{

    public $projectrepository;
    public $charitycategoryrepository;
    public $currencyrepository;
    public $settingrepository;
    public $ratingCriteriaRepository;
    public $ratingRepository;
    public $campaigngoalrepository;
    private $notificationRepository;
    private $supportRepository;
    private $marketingRepository;
    
    public function __construct(ProjectRepositoryInterface $projectrepository ,
                                CharityCategoryRepositoryInterface $charitycategoryrepository,
                                CurrencyRepositoryInterface $currencyrepository,
                                SettingRepositoryInterface $settingrepository,
                                RatingCriteriaRepositoryInterface $ratingCriteriaRepository,
                                RatingRepositoryInterface $ratingRepository,
                                CampaignGoalRepositoryInterface $campaigngoalrepository,
                                NotificationRepositoryInterface $notificationRepository,
                                SupportRepositoryInterface $supportRepository,
                                MarketingRepositoryInterface $marketingRepository
    )
    {

        $this->projectrepository         = $projectrepository;
        $this->charitycategoryrepository = $charitycategoryrepository;
        $this->currencyrepository        = $currencyrepository;
        $this->settingrepository         = $settingrepository;
        $this->ratingCriteriaRepository  = $ratingCriteriaRepository;
        $this->ratingRepository          = $ratingRepository;
        $this->campaigngoalrepository    = $campaigngoalrepository;
        $this->notificationRepository    = $notificationRepository;
        $this->supportRepository         = $supportRepository;
        $this->marketingRepository = $marketingRepository;
        
    }  // end of construct
    
    public function index()
    {
        $nation_id = getNationId();

        // get all charity category
        // $categories = $this->charitycategoryrepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'ASC']);
        
        // if(session()->has('session-country')){

        //     $projects = $this->projectrepository->getWhereWith(['charity', 'phases', 'currency'], [['profile', 0], ['nation_id', $nation_id] , 
        //             ['country', session('session-country')] , ['status', 'approved'], ['active', 1]]);
        // }
        // else{
        //     $projects = $this->projectrepository->getWhereWith(['charity', 'phases', 'currency'], [['profile', 0], ['nation_id', $nation_id] , 
        //             ['status', 'approved'], ['active', 1]]);
        // }

        // get all charity category
        $categories = $this->charitycategoryrepository->getWhereWith(['projects', 'projects.currency', 'projects.phases', 
                    'projects.ratings', 'projects.donates', 'projects.donates.currency', 'projects.charity'], [['nation_id', $nation_id]], 
                    ['column' => 'id', 'dir' => 'ASC']);

        $projects = $this->projectrepository->paginateWhereWith([['profile', 0], ['nation_id', $nation_id] , 
                    ['status', 'approved'], ['active', 1]], ['charity', 'phases', 'currency'], ['column' => 'id', 'dir' => 'DESC'], 6);

        $session_currency = currencySymbol(session('currency'));

        if(auth()->check()){

            $donor_total_supports = auth()->user()->donates()->sum('cost_gbp');
        }
        else{
            $donor_total_supports = 0;
        }
        
        // get all currency of nation id
        $currencies = $this->currencyrepository->nation($nation_id);

        $slug = 'all';

        return view('web.projects.project' , compact('categories' , 'currencies', 'projects', 'session_currency', 'slug', 'donor_total_supports'));

    } // end of index

    // Show project details
    public function show($slug){

        // increase seen of project campaing
        $marketing = $this->marketingRepository->getWhere([['project_link', $slug]])->first();
        if($marketing){

            $this->marketingRepository->updateWhere(['seen' => $marketing->seen + 1], [['project_link', $slug]]);
        }

        $nation_id = getNationId();

        $data['currency_symbol'] = currencySymbol(session('currency'));

        $data['project'] = $this->projectrepository->getWhereWith(['charity', 'ratings', 'images'], [['slug', $slug]])->first();

        if(auth()->check()){

            $data['comment'] = $this->ratingRepository->getWhere([['user_id', auth()->user()->id], ['project_id', $data['project']->id]], ['column' => 'id', 'dir' => 'ASC']);
        }

        $data['donations_cost'] = 0;

        // mark notification of this project to be read
        if(auth()->check()){

            $this->notificationRepository->updateWhere(['read' => 1], [['model_id', $data['project']->id], ['send_user_id', auth()->user()->id]]);

            $donates = $this->supportRepository->getWhereWith(['currency'], [['user_id', auth()->user()->id], ['project_id', $data['project']->id],
                                ['status', 'support']]);

            foreach($donates as $donate){

                $data['donations_cost'] += generalExchange($donate->cost, $donate->currency->symbol, $data['currency_symbol']);
            }

        }

        $data['criterias'] =  $this->ratingCriteriaRepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'ASC']);

        $data['currencies'] = $this->currencyrepository->getWhere([['nation_id', $nation_id]]);

        if (auth()->check()) {
            
            $data['comments'] = $this->projectrepository->checkCommentOrNo($data['project']->id ,  auth()->user()->id);
        
        } else 
        {
            $data['comments'] = [];
        }

        if(!$data['project']){
            
            return back();
        }

        return view('web.projects.show')->with([
            'data' => $data
        ]);
    } 

    // Rate project
    public function rateProject(Request $request, $slug){

        $project =  $this->projectrepository->getWhere([['slug', $slug]])->first();

        // $comment = $this->projectrepository->checkCommentOrNo($project->id , auth()->user()->id);

        $comment = $this->ratingRepository->getWhere([['user_id', auth()->user()->id], ['project_id', $project->id]], ['column' => 'id', 'dir' => 'ASC']);

        $data = $request->except(['_token', '_method', 'item']);

        if(count($comment) > 0)
        {
            session()->flash('success', trans('web.already_rated'));

            return redirect()->back();

            // $index = 1;
            
            // foreach($data as $key => $value){

            //     $item['user_id'] = auth()->user()->id;
            //     $item['project_id'] = $project->id;
            //     $item['grade'] = $value;
            //     $item['rating_criteria_id'] = $key;

            //     if($index == 5){

            //         $item['comment'] = $request->comment;
            //         $rating = $this->ratingRepository->update($item, $comment[$index - 1]->id);

            //         break;
            //     }
            //     else if($index < 5){

            //         $rating = $this->ratingRepository->update($item, $comment[$index]->id);
            //         $index ++;
            //     }
    
            // }
    
            // if($rating){
    
            //     session()->flash('success', __('web.eval_update_success'));
    
            //     return back();
            // }
            // else{
    
            //     session()->flash('error', __('web.error'));

            //     return back();
            // }              


        } 
        else 
        {

            $index = 1;

            foreach($data as $key => $value){

                $item['user_id'] = auth()->user()->id;
                $item['project_id'] = $project->id;
                $item['grade'] = $value;
                $item['rating_criteria_id'] = $key;

                if($index == 5){

                    $item['comment'] = $request->comment;
                    $rating = $this->ratingRepository->create($item);

                    break;
                }
                else if($index < 5){

                    $index ++;
                }

                // $rating = $this->ratingRepository->create($item);

                // dump($rating);

                // if($key == 'comment'){

                //     $item['comment'] = $request->comment;

                // }
                // else{

                //     $item['user_id'] = auth()->user()->id;
                //     $item['project_id'] = $project->id;
                //     $item['grade'] = $value;
                //     $item['rating_criteria_id'] = $key;
                // }
    
                $rating = $this->ratingRepository->create($item);
            }
    
            if($rating){
    
                session()->flash('success', __('web.eval_success'));
    
                return back();
            }
            else{
    
                session()->flash('error', __('web.error'));
    
                return back();
            }               

        }
  
    } // end of rate project

    public function getCategoryProjects(Request $request, $slug){

        $nation_id = getNationId();

        $categories = $this->charitycategoryrepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'ASC']);

        $session_currency = currencySymbol(session('currency'));

        $projects = $this->projectrepository->getCategoryProjects($slug);

        if(auth()->check()){

            $donor_total_supports = auth()->user()->donates()->sum('cost_gbp');
        }
        else{
            $donor_total_supports = 0;
        }
        
        // get all currency of nation id
        $currencies = $this->currencyrepository->nation($nation_id);

        if($request->ajax()){

            return view('web.projects.ajax.projects' , compact('categories' , 'currencies', 'session_currency', 'slug', 'projects', 'donor_total_supports'));

        }

        return view('web.projects.project' , compact('categories' , 'currencies', 'session_currency', 'slug', 'projects', 'donor_total_supports'));
        
    }

} // end of class
