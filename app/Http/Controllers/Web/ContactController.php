<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use Illuminate\Http\Request;

class ContactController extends Controller
{

    public $settingrepository;
    public $contactrepository;
    
    public function __construct(SettingRepositoryInterface $settingrepository , ContactRepositoryInterface $contactrepository)
    {

        $this->settingrepository = $settingrepository;
        $this->contactrepository = $contactrepository;

    }// end of construct


    public function index()
    {

        if(getNationId() == 1)
        {

            $lang = $this->settingrepository->getWhere([['key' , 'langtude_sa']])->first();
            $lat = $this->settingrepository->getWhere([['key' , 'latitude_sa']])->first();
            $phone = $this->settingrepository->getWhere([['key' , 'phone_sa']])->first();
            $email = $this->settingrepository->getWhere([['key' , 'email_sa']])->first();
            $address = $this->settingrepository->getWhere([['key' , 'address_sa']])->first();
            
        } else 
        {

            $lang = $this->settingrepository->getWhere([['key' , 'langtude_uk']])->first();
            $lat = $this->settingrepository->getWhere([['key' , 'latitude_uk']])->first();
            $phone = $this->settingrepository->getWhere([['key' , 'phone_uk']])->first();
            $email = $this->settingrepository->getWhere([['key' , 'email_uk']])->first();
            $address = $this->settingrepository->getWhere([['key' , 'address_uk']])->first();

        }

        return view('web.contacts.contact' , compact('lang' , 'lat' , 'phone' , 'email' , 'address'));

    } // end of index

} // end of controller 
