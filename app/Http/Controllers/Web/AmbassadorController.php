<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\CampaignGoalRequest;
use App\Repositories\AmbassadorRepositoryInterface;
use App\Repositories\CampaignGoalRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ProjectUserRepositoryInterface;
use App\Repositories\RatingCriteriaRepositoryInterface;
use Illuminate\Support\Facades\Auth;

class AmbassadorController extends Controller
{

    public $projectrepository;
    public $ambassadorrepository;
    public $projectuserrepository;
    public $ratingcriteriarepository;
    public $currencyrepository;
    public $campaigngoalrepository;

    public function __construct(ProjectRepositoryInterface $projectrepository,
                                AmbassadorRepositoryInterface $ambassadorrepository,
                                ProjectUserRepositoryInterface $projectuserrepository,
                                RatingCriteriaRepositoryInterface $ratingcriteriarepository,
                                CurrencyRepositoryInterface $currencyrepository,
                                CampaignGoalRepositoryInterface $campaigngoalrepository


    )
    {

        $this->projectrepository        = $projectrepository;
        $this->ambassadorrepository     = $ambassadorrepository;
        $this->projectuserrepository    = $projectuserrepository;
        $this->ratingcriteriarepository = $ratingcriteriarepository;
        $this->currencyrepository       = $currencyrepository;
        $this->campaigngoalrepository   = $campaigngoalrepository;

    } // end of construct
     
    public function store(CampaignGoalRequest $request)
    {

        $attribute = $request->except('_token');

        $affiliate = substr(uniqid(), 0, 8);

        $attribute['affiliate'] = $affiliate;
        $attribute['user_id']   = Auth::user()->id;

        $this->ambassadorrepository->create($attribute);

        return response()->json([
            'url' => \URL::to('/').'/campaign/'.$affiliate
        ]);
        
    } // end of store

     public function index($affiliate)
     {

        // $amdassador = $this->ambassadorrepository->affiliate($affiliate);
        // $amdassador->update(['seen' => $amdassador->seen + 1]);
        // $cost_project = $this->projectrepository->cost($amdassador->project_id);
        // $cost_donate = $this->projectuserrepository->cost($amdassador->project_id);

        $nation_id = getNationId();

        $session_currency = currencySymbol(session('currency'));

        $amdassador = $this->ambassadorrepository->getWhereWith(['project', 'project.currency'], [['affiliate', $affiliate]])->first();

        $this->ambassadorrepository->update(['seen' => $amdassador->seen + 1], $amdassador->id);
        

        $cost_project = generalExchange($amdassador->project->get_total, $amdassador->project->currency->symbol, $session_currency);

        $cost_donate = $amdassador->project->total_supports_for_project;

        $rating_criteriars = $this->ratingcriteriarepository->getwhere([['nation_id', $nation_id]]);

        // get all currency of nation id
        $currencies = $this->currencyrepository->nation(getNationId());

        // get all campaign goal
        $campaign_goals = $this->campaigngoalrepository->getwhere([['nation_id', $nation_id]]);        

        if (auth()->check()) {
            
            $comments = $this->projectrepository->checkCommentOrNo($amdassador->project_id ,  auth()->user()->id);
        
        } else 
        {
            $comments = 0;
        }
    
         return view('web.campaigns.campaign' , compact('session_currency', 'campaign_goals' , 'currencies' , 'comments' , 'rating_criteriars' , 'amdassador' , 'cost_project' , 'cost_donate'));

     } // end of index

} // end of class
