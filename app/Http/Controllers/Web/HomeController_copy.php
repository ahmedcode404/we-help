<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SliderRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\PartenerRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\StaticPageRepositoryInterface;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Http\Requests\Web\ContactUsRequest;

class HomeController_copy extends Controller
{


    private $sliderRepository;
    private $projectRepository;
    private $charityCategoryRepository;
    private $currencyrepository;
    private $partenerRepository;
    private $supportRepository;
    private $cityRepository;
    private $staticPageRepository;
    private $contactRepository;
    public $settingrepository;
    
    public function __construct(SliderRepositoryInterface $sliderRepository,
                                ProjectRepositoryInterface $projectRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                CurrencyRepositoryInterface $currencyrepository,
                                PartenerRepositoryInterface $partenerRepository,
                                SupportRepositoryInterface $supportRepository,
                                CityRepositoryInterface $cityRepository,
                                StaticPageRepositoryInterface $staticPageRepository,
                                ContactRepositoryInterface $contactRepository,
                                SettingRepositoryInterface $settingrepository
                                ){
        
        $this->sliderRepository = $sliderRepository;
        $this->projectRepository = $projectRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->currencyrepository = $currencyrepository;
        $this->partenerRepository = $partenerRepository;
        $this->supportRepository = $supportRepository;
        $this->cityRepository = $cityRepository;
        $this->staticPageRepository = $staticPageRepository;
        $this->contactRepository = $contactRepository;
        $this->settingrepository = $settingrepository;
    }
    
    public function index(){

        $nation_id = getNationId();
        
        $data['slider'] = $this->sliderRepository->getWhere([['nation_id', $nation_id], ['appearance', 'web']]);

        if(session()->has('session-country')){

            $data['projects'] = $this->projectRepository->whereHasWith(['charity', 'phases'], 'charity', [['status', 'approved']], [['profile', 0], ['nation_id', $nation_id] , 
                                ['country', session('session-country')] , ['status', 'approved'], ['active', 1]]);
        }
        else{
            $data['projects'] = $this->projectRepository->whereHasWithLimit(['charity', 'phases'], 'charity', [['status', 'approved']], [['profile', 0], ['nation_id', $nation_id] , 
                                ['status', 'approved'], ['active', 1]], 6);
        }
        

        $data['categories'] = $this->charityCategoryRepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'ASC']);
        $data['projects_count'] = $this->projectRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['parteners_count'] = $this->partenerRepository->getWhere([['nation_id', $nation_id]])->count();
        $data['donation_ammounts'] = $this->supportRepository->totalAmmount([['nation_id', $nation_id]]);
        $data['countries'] = $this->cityRepository->countriesCount();
        $data['benef_count'] = $this->projectRepository->getBenefCount([['nation_id', $nation_id]]);
        $data['partners'] = $this->partenerRepository->getWhere([['nation_id', $nation_id]]);
        $data['about_us'] = $this->staticPageRepository->getWhere([['nation_id', $nation_id], ['key', 'about_us']])->first();


        if($nation_id == 1)
        {

            $data['lang'] = $this->settingrepository->getWhere([['key' , 'langtude_sa']])->first();
            $data['lat'] = $this->settingrepository->getWhere([['key' , 'latitude_sa']])->first();
            
        } else 
        {

            $data['lang'] = $this->settingrepository->getWhere([['key' , 'langtude_uk']])->first();
            $data['lat'] = $this->settingrepository->getWhere([['key' , 'latitude_uk']])->first();

        }

        return view('web.home.index')->with([
            'data' => $data
        ]);
    }

    public function storeContactUs(Request $request){

        $data = $request->except(['_token', '_method', 'name', 'phone', 'content']);

        $nation_id = getNationId();

        $name = htmlspecialchars($request->name);
        $name = strip_tags($name);
        $data['name'] = $name;

        $phone = htmlspecialchars($request->phone);
        $phone = strip_tags($phone);
        $data['phone'] = $phone;

        $content = htmlspecialchars($request->content);
        $content = strip_tags($content);
        $data['content'] = $content;

        $data['nation_id'] = $nation_id;

        $contact = $this->contactRepository->create($data);

        if($contact){

            session()->flash('success', __('web.contact_success'));

            return back();
        }
        else{

            session()->flash('error', __('web.contact_failed'));

            return back();

        }
    }
}
