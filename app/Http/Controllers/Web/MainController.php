<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\SubscribeRepositoryInterface;
use App\Repositories\ServiceOptionRepositoryInterface;
use Exception;
use Illuminate\Http\Request;
use Twilio\Rest\Client;
use App\Models\CharityCategory;

class MainController extends Controller
{

    public $projectRepository;
    public $charityCategoryRepository;
    public $currencyRepository;
    public $subscriberepository;
    public $servicerepository;

    public function __construct(ProjectRepositoryInterface $projectRepository,
                                CharityCategoryRepositoryInterface $charityCategoryRepository,
                                CurrencyRepositoryInterface $currencyRepository,
                                ServiceOptionRepositoryInterface $servicerepository,
                                SubscribeRepositoryInterface $subscriberepository
    ){

        $this->projectRepository         = $projectRepository;
        $this->charityCategoryRepository = $charityCategoryRepository;
        $this->currencyRepository        = $currencyRepository;
        $this->subscriberepository       = $subscriberepository;
        $this->servicerepository       = $servicerepository;
    }

    public function lang($locale){

        if (! in_array($locale, ['en', 'ar'])) {
            abort(400);
        }

        session()->put('locale', $locale);
        return response()->json();
    }

    public function currency(Request $request)
    {

        session()->put('currency', $request->id);

        return response()->json([
            'data' => 1
        ], 200);   

    }

    public function search(Request $request){

        $token = htmlspecialchars($request->token);
        $token = strip_tags($token);

        $nation_id = getNationId();

        $session_currency = currencySymbol(session('currency'));

        $categories = $this->charityCategoryRepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'ASC']);

        $projects = $this->projectRepository->searchProjects($token);

        // get all currency of nation id
        $currencies = $this->currencyRepository->nation($nation_id);

        $slug = 'search';
        // get all charity category
        // $charity_categories = $this->charityCategoryRepository->searchRelation(['projects'], 'projects', ['id', '>', 0],
        //                     ['name', 'project_num', 'location', 'goals_ar', 'goals_en', 'desc_ar', 'desc_en', 'long_desc_ar', 'long_desc_en'],
        //                     $token, ['column' => 'id', 'dir' => 'DESC']);
            

        // // get all currency of nation id
        // $currencies = $this->currencyRepository->nation(getNationId());

        
        // $projects = $this->projectRepository->search(['id', '>', 0],
        //         ['name', 'project_num', 'location', 'goals_ar', 'goals_en', 'desc_ar', 'desc_en', 'long_desc_ar', 'long_desc_en'], 
        //         $token ,['column' => 'id', 'dir' => 'DESC']);

        // return view('web.projects.project' , compact('charity_categories' , 'currencies', 'projects'));

        return view('web.projects.project' , compact('categories' , 'currencies', 'session_currency', 'slug', 'projects'));



    }

    public function subscribe(Request $request)
    {
        
        $attribute = $request->except('_token');

        $sub = $this->subscriberepository->create($attribute);

        $topic = \App::getLocale() == 'ar' ? 'إشتراك النشرة البريدية' : 'Subscribe to the newsletter';
        $message =  \App::getLocale() == 'ar' ? 'تم إشتراكك فى النشرة البريدية لموقع وى هيلب بنجاح' : 'You have successfully subscribed to the We Help newsletter';
        $nation_id = getNationId();

        // send email
        sendMail($sub['email-subscribe'], $topic, $message, $nation_id, null, null);

        $response['status'] = 1;
        $response['message'] = __('web.success_subscribe');

        return $response;
        // return redirect()->back()->with('success' , __('web.success_subscribe'));
    } // end of funcrion subscribe


    // Just for testing
    public function sendSms()
    {

        $receiverNumber = "+201023966383";
        $message = "اخويا الفته";

        try {
  
            $account_sid = 'AC20d1c1f24835fd535746a07e6b6c4a8d';
            $auth_token = '4e4bc4851f42a94792d108c2be71ff46';
            $twilio_number = '+12566009877';
  
            $client = new Client($account_sid, $auth_token);
            $client->messages->create($receiverNumber, [
                'from' => $twilio_number, 
                'body' => $message]);
  
            dd('SMS Sent Successfully.');
  
        } catch (Exception $e) {
            dd("Error: ". $e->getMessage());
        }

    } // end of send sms

    // filter projects by country
    public function filterProjectsByCountry($id){

        if($id == 'reset')
        {

            session()->forget('session-country');
            
        } else 
        {
            
            session()->put('session-country', $id);

        }

        return redirect()->back();  
         
        // $nation_id = getNationId();

        // // get all charity category
        // $charity_categories = $this->charitycategoryrepository->getWhere([['nation_id', $nation_id]], ['column' => 'id', 'dir' => 'ASC']);
        
        // $projects = $this->projectrepository->whereHasWith(['charity', 'phases'], 'country', [['slug', $slug]],  [['profile', 0]]);
        
        // // get all currency of nation id
        // $currencies = $this->currencyrepository->nation(getNationId());

        // return view('web.projects.project' , compact('charity_categories' , 'currencies', 'projects'));
        
    }


    // get charity category services
    public function getCategoryServices(Request $request){

        $data['services'] = $this->servicerepository->getWhere([['charity_category_id', $request->id], ['parent_id', null]]);

        return view('charities.projects.ajax.services')->with([
            'data' => $data
        ]);

    }

    // get charity category services
    public function getServiceFeatures(Request $request){

        $data['features'] = $this->servicerepository->getWhere([['parent_id', $request->id]]);

        return view('charities.projects.ajax.features')->with([
            'data' => $data
        ]);

    }

}
