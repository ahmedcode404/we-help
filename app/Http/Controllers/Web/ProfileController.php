<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\DontateRequest;
use App\Http\Requests\Web\PriceDontateRequest;
use App\Http\Requests\Web\UpdateDonateMonthRequest;
use App\Http\Requests\Web\UserUpdateRequest;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ProjectUserRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\PaymentTransactionRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use App\Servicies\UploadFilesServices;
use Illuminate\Http\Request;

use Monarobase\CountryList\CountryListFacade;

class ProfileController extends Controller
{

    private $userrepository;
    private $cityrepository;
    private $charityrepository;
    private $fileservice;
    private $projectuserrepository;
    private $projectrepository;
    private $currencyrepository;
    private $cartrepository;
    private $paymentTransactionRepository;
    private $reportRepository;
    
    public function __construct(UserRepositoryInterface $userrepository ,
                                CityRepositoryInterface $cityrepository,
                                CharityRepositoryInterface $charityrepository,
                                UploadFilesServices $fileservice ,
                                ProjectUserRepositoryInterface $projectuserrepository ,
                                ProjectRepositoryInterface $projectrepository,
                                CurrencyRepositoryInterface $currencyrepository,
                                CartRepositoryInterface $cartrepository,
                                PaymentTransactionRepositoryInterface $paymentTransactionRepository,
                                ReportRepositoryInterface $reportRepository 
    )
    {

        $this->userrepository        = $userrepository;
        $this->cityrepository        = $cityrepository;
        $this->charityrepository     = $charityrepository;
        $this->fileservice           = $fileservice;
        $this->projectuserrepository = $projectuserrepository;
        $this->projectrepository     = $projectrepository;
        $this->currencyrepository    = $currencyrepository;
        $this->cartrepository        = $cartrepository;
        $this->paymentTransactionRepository = $paymentTransactionRepository;
        $this->reportRepository = $reportRepository;

    }// end of construct

    public function index()
    {

        return view('web.profile.profile');

    } // /end of index

    public function edit()
    {

        // $countries = $this->cityrepository->country();
        $countries = CountryListFacade::getList(\App::getLocale());

        return view('web.profile.profile' , compact('countries'));

    } // end of edit

    public function update(UserUpdateRequest $request)
    {

        $attribute = $request->except('_token' , 'password' , 'name', 'password_confirmation', 'phone_num');

        if($request->password)
        {
            
            $attribute['password'] = bcrypt($request->password);
            
        } // end of if request password
        
        $attribute['name_ar'] = $request->name;
        $attribute['name_en'] = $request->name;

        $this->userrepository->update($attribute , auth()->user()->id);            

        session()->flash('success', __('web.profile_updated'));

        return redirect()->back();

    } // end of update

    public function uploadImage(Request $request)
    {

        // uploade file image
        if ($request->hasFile('image')) {
        
            $img = $request->file('image');

            $image = $this->fileservice->uploadfile($img, 'user');

            $attribute['image'] = $image;

        } // end of if image

        $this->userrepository->update($attribute , auth()->user()->id);            

        return response()->json();

    } // end of upload image

    public function profileMoney()
    {

        return view('web.profile.profile');

    } // end of profile money

    public function profileProject()
    {

        return view('web.profile.profile');

    } // end of profile project

    public function updateDonateMonth(UpdateDonateMonthRequest $request)
    {
        if ($request->switch_1) {

            $this->projectuserrepository->update([

                'cost'        => $request->number_input,
                'cost_gbp'    => generalExchange($request->number_input, currencySymbol($request->currency_id), 'GBP'),
                'currency_id' => $request->currency_id
                
            ] , $request->month_id);


            $support = $this->projectuserrepository->findOne($request->month_id);

            // create a new support
            $data['user_id'] = $support->user_id;
            $data['project_id'] = $support->project_id;
            $data['status'] = 'cart';
            $data['cost'] = $support->cost;
            $data['cost_gbp'] = generalExchange($support->cost, currencySymbol($support->currency_id), 'GBP');
            $data['type'] = $support->type;
            $data['nation_id'] = $support->nation_id;
            $data['currency_id'] = $support->currency_id;

            $new_support = $this->projectuserrepository->create($data);

        } else 
        {

            $this->projectuserrepository->update([

                'type' => 'once',
            
            ] , $request->month_id);

        } // end of else if

        $route = route('create-payment-session', $request->month_id);

        return response()->json([
            'checkoute_route' => $route
        ]);
        
    } // end of update Donate Month


    public function getCart(){

        $data['currencies'] = $this->currencyrepository->getWhere([['nation_id', getNationId()]]);

        $data['carts'] = $this->cartrepository->getWhereWith(['project', 'project.charity', 'project.ratings', 'currency'], [['user_id', auth()->user()->id] , ['status' , 'cart']]);

        return view('web.profile.profile')->with([
            'data' => $data
        ]);
    }

    public function addToCart(Request $request){

        // $old_support = auth()->user()->donates()->orderBy('id', 'desc')->first();

        $old_cart = auth()->user()->carts()->where([['project_id', $request->project_id]])->first();

        if($old_cart){

            session()->flash('error', __('web.cart_exsist'));

            return redirect()->back();

        }
        
        $old_support = auth()->user()->donates()->where([['project_id', $request->project_id]])->orderBy('id', 'desc')->first();

        // create the new support
        $data['user_id'] = $old_support->user_id;
        $data['project_id'] = $old_support->project_id;
        $data['status'] = 'cart';
        $data['cost'] = $old_support->cost;
        $data['type'] = $old_support->type;
        $data['name_anonymouse'] = $old_support->name_anonymouse;
        $data['nation_id'] = $old_support->nation_id;
        $data['currency_id'] = $old_support->currency_id;

        $cart = $this->cartrepository->create($data);

        if($cart){
            session()->flash('success', __('web.add_cart_success'));

            return redirect()->back();
        }
        else{
            session()->flash('error', __('web.error'));

            return redirect()->back();
        }
    }

    
    public function donateAgain(Request $request)
    {
       
        $project_user = $this->projectuserrepository->findOne($request->id);
        
        $project = $this->projectrepository->findWith($project_user->project_id , ['supports']);

        $cost_donate  = getTotalSupportsProject($project_user->project , $project_user->currency_id);

        $donate_cost  = exchange($project->get_total, $project->currency_id, $project_user->currency_id) - $cost_donate;            

        if ($donate_cost < $project_user->cost) {

            return response()->json([

                'data' => $donate_cost,
                'status' => 0

            ]); // end of response

        } else  
        {

            $attribute = [

                'type'            => $project_user->type,
                'user_id'         => $project_user->user_id,
                'project_id'      => $project_user->project_id,
                'nation_id'       => $project_user->nation_id,
                'status'          => 'cart',
                'cost'            => $project_user->cost,
                'cost_gbp'        => generalExchange($project_user->cost, currencySymbol($project_user->currency_id), 'GBP'),
                'currency_id'     => $project_user->currency_id,
                'name_anonymouse' => $project_user->name_anonymouse

            ]; // end of attribute

            $donation = $this->projectuserrepository->create($attribute);

            $route = route('create-payment-session', $donation->id);

            return response()->json([

                'data' => $donate_cost,
                'checkoute_route' => $route,
                'status' => 1

            ]); // end of response

        }  // end of else if donate coat < project user cost

    }  // end of donate again

    public function profileReport()
    {

        return view('web.profile.profile');

    } // end of profile report

    public function report($slug)
    {

        $project = $this->projectrepository->getWhereWith(['reports'], [['slug', $slug], ['status', 'approved']])->first();

        if($project){

            return view('web.profile.profile' , compact('project'));
        }
        else{

            session()->flash('error', trans('admin.add_error'));
            return redirect()->back();
        }

    } // end of report

    public function getReportDetails($slug){

        $report = $this->reportRepository->getWhereWith(['phase', 'project'], [['slug', $slug], ['status', 'approved']])->first();

        // $report = $this->reportRepository->findWith($id, ['phase']);

        return view('web.profile.profile', compact('report'));
    }

    public function deleteProject(Request $request)
    {

        $support = $this->projectuserrepository->findWith($request->id, ['project']);

        $project = $this->projectrepository->WhereHasWith(['transactions'], 'transactions', [['canceled', 0]], [['id',  $support->project->id]])->first();

        foreach($project->transactions as $transaction){

            if($transaction->canceled == 0){

                // Set your secret key. Remember to switch to your live secret key in production.
                // See your keys here: https://dashboard.stripe.com/apikeys
                \Stripe\Stripe::setApiKey(config('services.stripe.apiKey'));

                $subscription = \Stripe\Subscription::retrieve($transaction->subscription_id);
                
                $subscription->cancel();

                $this->paymentTransactionRepository->update(['canceled' => 1], $transaction->id);
            }
        }

        $this->projectuserrepository->update(['type' => 'once'] , $request->id);
    
        return response()->json();

    }  // end of delete project    
    
    public function setting()
    {

        return view('web.profile.profile');

    } // end of setting

    public function activeNotification()
    {

        if (auth()->user()->is_notify == 0) {

            auth()->user()->update(['is_notify' => 1]);

            return response()->json(['data' => 1]);
        } else 
        {

            auth()->user()->update(['is_notify' => 0]);

            return response()->json(['data' => 0]);

        }// end of else if condition 

    } // end of active notification

    public function deleteAccount()
    {
        auth()->user()->delete();

        return response()->json();

    } // end of delete account

} // end of class

