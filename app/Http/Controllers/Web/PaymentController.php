<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\FinancialRequestRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Repositories\PaymentTransactionRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use Illuminate\Support\Str;
use App\Servicies\Notify;

class PaymentController extends Controller
{
    private $supportRepository;
    private $financialRequestRepository;
    private $notificationRepository;
    private $paymentTransctionRepository;
    private $projectrepository;

    public function __construct(SupportRepositoryInterface $supportRepository,
                                FinancialRequestRepositoryInterface $financialRequestRepository,
                                NotificationRepositoryInterface $notificationRepository,
                                PaymentTransactionRepositoryInterface $paymentTransctionRepository,
                                ProjectRepositoryInterface $projectrepository){

        $this->supportRepository = $supportRepository;
        $this->financialRequestRepository = $financialRequestRepository;
        $this->notificationRepository = $notificationRepository;
        $this->paymentTransctionRepository = $paymentTransctionRepository;
        $this->projectrepository = $projectrepository;

    }

    public function createPaymentSession($support_id){

        session(['support_id' => $support_id]);

        $support = $this->supportRepository->findWith($support_id, ['project']);

        if($support->type == 'monthly'){

            return $this->createSubscription($support);
        }
        else{

            return $this->CreateOncePayment($support);
        }

    }


    public function CreateOncePayment($support){

        session(['support_id' => $support->id]);

        if(generalExchange($support->cost, $support->currency->symbol, 'GBP') < 0.30){

            session()->flash('warning', trans('web.invalid_support_cost'));

            return redirect()->back();
        }

        // create session
        require '../vendor/autoload.php';
        \Stripe\Stripe::setApiKey(config('services.stripe.apiKey'));

        header('Content-Type: application/json');

        $checkout_session = \Stripe\Checkout\Session::create([
            'payment_method_types' => ['card'],
            'line_items' => [[
                'price_data' => [
                'currency' => currencySymbol($support->currency_id),
                'unit_amount' => $support->cost * 100, // المزود بياخد الفلوس بالسنت .. سؤال للدعم الفنى
                'product_data' => [
                    'name' => $support->project->name,
                    'images' => [$support->project->image],
                ],
                ],
                'quantity' => 1,
            ]],
            'mode' => 'payment',
            'success_url' => route('payment.success'),
            'cancel_url' => route('payment.cancle'),
        ]);

        
        header("HTTP/1.1 303 See Other");

        // Register the transaction
        $data['session_id'] = $checkout_session->id;
        $data['project_id'] = $support->project->id;
        $data['user_id'] = $support->user->id;
        $data['type'] = 'once';
        
        $this->paymentTransctionRepository->create($data);

        header("Location: " . $checkout_session->url);

        return redirect($checkout_session->url);

    }

    public function createSubscription($support){

        session(['support_id' => $support->id]);

        // Set your secret key. Remember to switch to your live secret key in production.
        // See your keys here: https://dashboard.stripe.com/apikeys
        \Stripe\Stripe::setApiKey(config('services.stripe.apiKey'));

        $product = \Stripe\Product::create([
            'name' => $support->project->name,
        ]);

        $setup_fee = \Stripe\Product::create([
            'name' => 'Starter Setup',
        ]);
    
        $setup_fee_price = \Stripe\Price::create([
            'product' => $setup_fee->id,
            'unit_amount' => generalExchange(20, 'USD', $support->currency->symbol) * 100,
            'currency' => currencySymbol($support->currency_id),
        ]);

        
        $customer = \Stripe\Customer::create([
            'email' => $support->user->email,
            'payment_method' => 'pm_card_visa',
            'invoice_settings' => [
                'default_payment_method' => 'pm_card_visa',
            ],
        ]);

        
        $invoice_item = \Stripe\InvoiceItem::create([
            'customer' => $customer->id,
            'price' => $setup_fee_price->id,
        ]);
        
        $invoice = \Stripe\Invoice::create([
            'customer' => $customer->id,
        ]);
        
        $subscription = \Stripe\Subscription::create([
            'customer' => $customer->id,
            'items' => [[
                'price_data' => [
                    'unit_amount' => $support->cost * 100,
                    'currency' => currencySymbol($support->currency_id),
                    'product' => $product->id,
                    'recurring' => [
                        'interval' => 'month',
                    ],
                ],
                ]],
        ]);
            
        $checkout_session = \Stripe\Checkout\Session::create([
            'customer' => $customer->id,
            'payment_method_types' => ['card'],
            'line_items' => [
                [
                    'price_data' => [
                        'unit_amount' => $support->cost * 100,
                        'currency' => currencySymbol($support->currency_id),
                        'product' => $product->id,
                        'recurring' => [
                        'interval' => 'month',
                        ],
                    ],
                    'quantity' => 1,
                ],
            ],
            'mode' => 'subscription',
            'success_url' => route('payment.success'),
            'cancel_url' => route('payment.cancle'),
        ]);

        // Register the transaction
        $data['project_id'] = $support->project->id;
        $data['user_id'] = $support->user->id;
        $data['product_id'] = $product->id;
        $data['setup_fee_id'] = $setup_fee->id;
        $data['setup_fee_price_id'] = $setup_fee_price->id;
        $data['customer_id'] = $customer->id;
        $data['invoice_item_id'] = $invoice_item->id;
        $data['invoice_id'] = $invoice->id;
        $data['subscription_id'] = $subscription->id;
        $data['session_id'] = $checkout_session->id;
        $data['type'] = 'monthly';

        $this->paymentTransctionRepository->create($data);

        return redirect($checkout_session->url);
    }

    public function emailVoucher($voucher){

        $msg = '';
        $email = [['email' => $voucher->requestable->email]];
        $topic = trans('admin.support_voucher');
        $nation_id = $voucher->requestable->nation_id;
        $notes = $voucher->notes;

        $msg = trans('admin.your_support_voucher');
        $notify_ar = 'فاتورة تبرع';
        $notify_en = 'Support Voucher';

        // create exchange voucher link - ارسال طلب الصرف - فاتوره بالمبلغ اللى هيتم تحويله للجمعيه
        $voucher_link = '<a href="'.route('get-voucher', $voucher->slug).'">'.trans('admin.show').'</a>';

        // notification 
        $message = new \stdClass();
        $message->message_ar = $notify_ar;
        $message->message_en = $notify_en;

        $notification_data['message_en'] = $message->message_en;
        $notification_data['message_ar'] = $message->message_ar;
        $notification_data['send_user_id'] = $voucher->requestable->id;
        $notification_data['title'] = 'financial_request';
        $notification_data['type'] = 'user';

        $this->notificationRepository->create($notification_data); 
        
        $users = [];
        array_push($users, $voucher->requestable->id);

        Notify::NotifyWeb($message , 'financial_request' , 'user' , $voucher->requestable->id); 
        // Notify::NotifyMob($message , 'financial_request' , 'user' , $voucher->requestable->id); 
        // Notify Supporters via mobile app that monthly deduction for this project transfered
        notifyMobile($voucher->id, $users, 'new_voucher', 'supporter',  'فاتورة تبرع الخاص بك', 'Your donation voucher', asset('web/images/main/user_avatar.png'));

        // send email
        try {
    
            sendMail($email, $topic, '<p>'.$msg.'</p>', $nation_id, $notes, $voucher_link);

        } catch (Throwable $e) {

            return response()->json(['data' => 'exception'], 200);

        }  
    }

    public function success(){

        if(!session()->has('support_id')){

            session()->flash('error', trans('web.no_support'));

            return redirect()->back();
        }

        $id = session('support_id');

        $this->supportRepository->update(['status' => 'support'], $id);

        $support = $this->supportRepository->findWith($id, ['project', 'project.currency', 'project.charity', 'currency']);

        // Create catch voucher - انشاء سند القبض
        $voucher_data['slug'] = Str::random(12);
        $voucher_data['out_to_charity'] = $support->cost;
        $voucher_data['requestable_id'] = $support->user_id;
        $voucher_data['requestable_type'] = 'App\Models\User';
        $voucher_data['project_id'] = $support->project_id;
        $voucher_data['status'] = 'approved';
        $voucher_data['type'] = 'catch';
        $voucher_data['currency_id'] = $support->currency_id;
        $voucher_data['bank_account_num'] = null;
        $voucher_data['voucher_num'] = null;

        $voucher = $this->financialRequestRepository->create($voucher_data);


        $project = $support->project;
        $project_cost = generalExchange($project->get_total, $project->currency->symbol, currencySymbol(session('currency')));
        $project_donates = $project->total_supports_for_project;
        $remaining_donates = $project_cost - $project_donates;
        
        if($remaining_donates <= 0 ){

            $project_data['donation_complete'] =  1;
            $this->projectrepository->update($project_data, $project->id);
        }

        // Email Voucher to user
        $this->emailVoucher($voucher);

        return view('web.payment.success', compact('support', 'voucher'));

    }

    public function cancle(){
        
        if(!session()->has('support_id')){

            session()->flash('error', trans('web.no_support'));

            return redirect()->back();
        }

        $id = session('support_id');

        $support = $this->supportRepository->findOne($id);

        $user_id = $support->user_id;

        // delete support
        $deleted = $this->supportRepository->deleteWhere([['id', $id]]);

        if($deleted){

            // Notify user
            // notification 
            $message = new \stdClass();
            $message->message_ar = 'تم إلغاء تبرعك لعدم إتمام الدفع';
            $message->message_en = 'Your donation has been canceled due to non-payment';

            $notification_data['message_en'] = $message->message_en;
            $notification_data['message_ar'] = $message->message_ar;
            $notification_data['send_user_id'] = $user_id;
            $notification_data['title'] = 'support_canceled';
            $notification_data['type'] = 'user';

            $this->notificationRepository->create($notification_data);            

            Notify::NotifyWeb($message , 'support_canceled' , 'user' , $user_id); 
            // Notify::NotifyMobile($message , 'support_canceled' , 'user' , $user_id); 
            notifyMobile(null, [$user_id], 'support_canceled', 'supporter', 'تم إلغاء تبرعك لعدم إتمام الدفع', 'Your donation has been canceled due to non-payment', asset('web/images/main/user_avatar.png'));

        }

        return redirect()->route('web.home');

    }
}
