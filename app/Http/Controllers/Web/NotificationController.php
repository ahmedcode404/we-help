<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\NotificationRepositoryInterface;

class NotificationController extends Controller
{

    private $notificationRepository;

    public function __construct(NotificationRepositoryInterface $notificationRepository){

        $this->notificationRepository = $notificationRepository;

    }

    // get all notifications
    public function index(){

        $this->notificationRepository->updateWhere(['read' => 1], [['send_user_id', auth()->user()->id]]);

        $data['notifications'] = $this->notificationRepository->getWhereWith(['project'], [['send_user_id', auth()->user()->id], ['type', 'supporter']]);

        return view('web.notifications.index')->with([
            'data' => $data
        ]);

    }

    // remove item from notification
    public function removeItem($id){

        $deleted = $this->notificationRepository->delete($id);

        if($deleted){

            return response()->json([
                'status' => 1,
                'msg' => __('web.item_removed_success'),
                'icon' => 'success'
            ], 200);
        }
        else{

            return response()->json([
                'status' => 0,
                'msg' => __('web.error'),
                'icon' => 'error'
            ], 200);
        }

    }
}
