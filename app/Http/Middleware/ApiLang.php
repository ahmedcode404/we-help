<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApiLang
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = app('request')->header('Accept-Language');
        \App::setLocale($lang);
        return $next($request);
    }
}
