<?php

namespace App\Http\Middleware\charity;

use Closure;
use Illuminate\Http\Request;
use Auth;
class Charity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check() && auth()->user()->hasRole('charity')){

            $user = auth()->user();
        }
        else if(auth()->check() && auth()->user()->hasRole('charity_employee')){

            $user = auth()->user()->charity;
        }

        $permissionRepository = \App::make('App\Repositories\PermissionRepositoryInterface');
        $permissions = $permissionRepository->getWhere([['model_type', 'charity'], ['model_id', $user->id]]);

        // if auth user is charity, then check for his status
        // if auth user is charity employee, then check for his charity status
        if(auth()->check() && ((auth()->user()->hasRole('charity') && auth()->user()->hasAnyPermission($permissions) && auth()->user()->status == 'approved') 
            || (auth()->user()->hasRole('charity_employee') && auth()->user()->hasAnyPermission($permissions) && auth()->user()->charity->status == 'approved') )){

            return $next($request);
            
        } 
        
        return redirect('return-authorized');            
        
    }
}
