<?php

namespace App\Http\Middleware\Charity;

use Closure;
use Illuminate\Http\Request;
use Auth;
class Contract
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        // if auth user is chrity, then check for contract signature
        // if auth user ic charity employee, then check for its charity contract signature
        if(auth()->check() && (auth()->user()->hasRole('charity') && (auth()->user()->contract_after_sign != NULL || auth()->user()->signature != NULL))
        || (auth()->user()->hasRole('charity_employee') && (auth()->user()->charity->contract_after_sign != NULL || auth()->user()->charity->signature != NULL))){
            
            return $next($request);
            
        } 
        
        return redirect('return-authorized'); 

    }
}
