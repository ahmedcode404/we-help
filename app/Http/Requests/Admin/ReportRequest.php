<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    
                    return [
                        'report_ar'  => 'required',
                        'report_en'  => 'required',
                        'vedio'      => 'required|mimes:mp4,mov,ogg,qt,avi|max:51200',
                        'status'     => 'required',
                        'images'     => 'required|array',
                        'images.*'   => 'required|mimes:png,jpeg,jpg|max:2048',
                    ];

                }
                case 'PUT':
                case 'PATCH':
                {

                    return [
                        'report_ar' => 'nullable',
                        'report_en' => 'nullable',
                        'images' => 'nullable',
                        'images.*' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
                        'status' => 'nullable',
                        'vedio' => 'nullable|mimes:mp4,mov,ogg,qt,avi|max:51200',
                    ];

                }
                default:break;

        }
    }
}
