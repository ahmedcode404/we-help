<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Rules\PhoneNumber;
use App\Rules\NationCode;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = auth()->user()->id;
        $nation_id = getNationId();

        return [
            'name_ar' => 'nullable',
            'name_en' => 'nullable',
            'email' => 'nullable|email|unique:users,email,'.$id.'|unique:charities,email,'.$id,
            // 'phone' => new NationCode($request->has('nation_id') ? $request->nation_id : $nation_id),
            'phone' => $request->has('emp_data') ? 'nullable|unique:users,phone,'.$id.'|unique:charities,phone,'.$id.'|unique:charities,mobile,'.$id.'|phone:'.$request->country : 'nullable|unique:users,phone,'.$id.'|unique:charities,phone,'.$id.'|unique:charities,mobile,'.$id,
            // 'phone_num' => ['nullable','unique:users,phone,'.$id,'unique:charities,phone,'.$id,'min:8', 'max:10', new PhoneNumber($request->has('nation_id') ? $request->nation_id : $nation_id) ],
            'image' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'nation_number' => 'required_if:emp_data,true',
            'region' => 'required_if:emp_data,true',
            'street' => 'required_if:emp_data,true',
            'unit_number' => 'required_if:emp_data,true',
            'mail_box' => 'required_if:emp_data,true',
            'postal_code' => 'required_if:emp_data,true',
            'contract_start_date' => 'required_if:emp_data,true|date',
            'contract_end_date' => 'required_if:emp_data,true|date|after:contract_start_date',
            'country' => 'required_if:emp_data,true',
            'city' => 'required_if:emp_data,true',
            // 'nation_id' => 'required_if:emp_data,true',
            'degree_id' => 'required_if:emp_data,true',
            'job_id' => 'required_if:emp_data,true',
            'password' => 'nullable|min:6|confirmed'
        ];
    }
}
