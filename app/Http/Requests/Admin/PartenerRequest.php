<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PartenerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name_ar' => 'required',
                    'name_en' => 'required',
                    'logo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    'link' => 'required'
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'name_ar' => 'required',
                    'name_en' => 'required',
                    'logo' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
                    'link' => 'required'
                ];
        }
    }

    public function messages(){

        return [
            'name_ar.required' => trans('admin.required'),
            'name_en.required' => trans('admin.required'),
            'logo.required' => trans('admin.required'),
            'logo.image' => trans('admin.valid_image'),
            'logo.mimes' => trans('admin.images_mimes'),
            'logo.max' => trans('admin.image_max'),
            'link.required' => trans('admin.required')
        ];
    }
}
