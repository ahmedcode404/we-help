<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class AgreementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $nation_id = getNationId();
        return [

            // validation version saudia
            'supporter_payment_rules_sa_ar' => $nation_id == 1 ? 'required' : 'nullable',
            'supporter_payment_rules_sa_en' => $nation_id == 1 ? 'required' : 'nullable',
            'charity_register_rules_sa_en'  => $nation_id == 1 ? 'required' : 'nullable',
            'charity_register_rules_sa_ar'  => $nation_id == 1 ? 'required' : 'nullable',

            // validation version italian
            'supporter_payment_rules_uk_ar' => $nation_id == 2 ? 'required' : 'nullable',
            'supporter_payment_rules_uk_en' => $nation_id == 2 ? 'required' : 'nullable',
            'charity_register_rules_uk_ar'  => $nation_id == 2 ? 'required' : 'nullable',
            'charity_register_rules_uk_en'  => $nation_id == 2 ? 'required' : 'nullable',

        ];

    }

    public function messages(){

        return [
            'supporter_payment_rules_sa_ar.required' => trans('admin.required'),
            'supporter_payment_rules_sa_en.required' => trans('admin.required'),
            'charity_register_rules_sa_en.required' => trans('admin.required'),
            'charity_register_rules_sa_ar.required' => trans('admin.required'),

            'supporter_payment_rules_uk_ar.required' => trans('admin.required'),
            'supporter_payment_rules_uk_en.required' => trans('admin.required'),
            'charity_register_rules_uk_ar.required' => trans('admin.required'),
            'charity_register_rules_uk_en.required' => trans('admin.required'),
        ];
    }
}
