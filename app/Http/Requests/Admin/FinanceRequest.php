<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class FinanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'out_to_charity' => 'required',
                    'requestable_id' => 'required',
                    'project_id' => 'required',
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'status' => 'required',
                ];
            break;
        }

        
    }

    public function messages(){

        return [
            'out_to_charity.required' => trans('admin.required'),
            'requestable_id.required' => trans('admin.required'),
            'project_id.required' => trans('admin.required'),
        ];
    }
}
