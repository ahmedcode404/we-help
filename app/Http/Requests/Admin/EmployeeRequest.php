<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Rules\NationCode;
use App\Rules\PhoneNumber;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        session(['emp_phone' => $request->phone]);

        $id = $this->route('employee');

        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                        'name_employee_ar'    => 'required|string',
                        'name_employee_en'    => 'required|string',
                        // 'country_id'          => 'required|string',
                        // 'city_id'             => 'required|string',
                        'nation_number'       => 'required|unique:users,nation_number',
                        'country'             => 'required|string',
                        'city'                => 'nullable|string',
                        'region'              => 'required|string',
                        'street'              => 'required|string',
                        'unit_number'         => 'required|numeric',
                        'mail_box'            => 'required|string',
                        'postal_code'         => 'required|numeric',
                        'phone'               => 'required|unique:users,phone|unique:charities,phone|phone:'.$request->country,
                        'email'               => 'required|email|unique:users,email|unique:charities,email',
                        'contract_start_date' => 'required',
                        'contract_end_date'   => 'required|after:contract_start_date',
                        'permission'          => 'required',
                        'degree_id'           => 'required',
                        // 'nation_id'           => 'required',
                        'job_id'              => 'required',
                        'nationality_id'      => 'required',
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {

                    return [
                        'name_employee_ar'    => 'required|string',
                        'name_employee_en'    => 'required|string',
                        // 'country_id'          => 'required|string',
                        // 'city_id'             => 'required|string',
                        'nation_number'       => 'required|unique:users,nation_number,'.$id,
                        'country'             => 'required|string',
                        'city'                => 'nullable|string',
                        'region'              => 'required|string',
                        'street'              => 'required|string',
                        'unit_number'         => 'required|numeric',
                        'mail_box'            => 'required|string',
                        'postal_code'         => 'required|numeric',
                        'phone'               => 'required|unique:charities,phone,'.$id.'|unique:users,phone,'.$id.'|phone:'.$request->country,
                        'email'               => 'required|email|unique:users,email,'.$id.'|email|unique:charities,email,'.$id,
                        'contract_start_date' => 'required',
                        'contract_end_date'   => 'required|after:contract_start_date',
                        'permission'          => 'required',
                        'degree_id'           => 'required',
                        // 'nation_id'           => 'required',
                        'job_id'              => 'required',
                        'nationality_id'      => 'required',
                    ];

                }
                default:break;

        } // end of switch           

    }

    public function messages(){

        return [
            'name_employee_ar.required' => trans('admin.required'),
            'name_employee_ar.string' => trans('admin.string'),

            'name_employee_en.required' => trans('admin.required'),
            'name_employee_en.string' => trans('admin.string'),

            'nation_number.required' => trans('admin.required'),
            'nation_number.unique' => trans('admin.unique'),

            'country.required' => trans('admin.required'),
            'city.required' => trans('admin.required'),
            'region.required' => trans('admin.required'),
            'street.required' => trans('admin.required'),

            'country.string' => trans('admin.string'),
            'city.string' => trans('admin.string'),
            'region.string' => trans('admin.string'),
            'street.string' => trans('admin.string'),

            'unit_number.required' => trans('admin.required'),
            'unit_number.numeric' => trans('admin.numeric'),

            'mail_box.required' => trans('admin.required'),
            'mail_box.string' => trans('admin.string'),

            'postal_code.required' => trans('admin.required'),
            'postal_code.numeric' => trans('admin.numeric'),

            'phone.required' => trans('admin.required'),
            'phone.unique' => trans('admin.unique'),

            'email.required' => trans('admin.required'),
            'email.unique' => trans('admin.unique'),
            'email.valid_email' => trans('admin.valid_email'),

            'contract_start_date.required' => trans('admin.required'),
            'contract_end_date.required' => trans('admin.required'),
            'permission.required' => trans('admin.required'),
            'degree_id.required' => trans('admin.required'),
            'job_id.required' => trans('admin.required'),
            'nationality_id.required' => trans('admin.required'),
        ];
    }
}
