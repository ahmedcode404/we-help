<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [

                    'name_currency_ar' => 'required',
                    'name_currency_en' => 'required',
                    'symbol'           => 'required',
                    'icon'             => 'required|image|mimes:jpeg,png,jpg|max:2048',
                    
                ];
            case 'PUT':
            case 'PATCH':
                return [

                    'name_currency_ar' => 'required',
                    'name_currency_en' => 'required',
                    'symbol'           => 'required',
                    'icon'             => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
                    
                ];
        }
    }

    public function messages(){
        return [
            'name_currency_ar.required' => trans('admin.required'),
            'name_currency_en.required' => trans('admin.required'),
            'symbol.required' => trans('admin.required'),
            'icon.required' => trans('admin.required'),
            'icon.image' => trans('admin.valid_image'),
            'icon.mimes' => trans('admin.images_mimes'),
            'icon.max' => trans('admin.image_max'),
        ];
    }
}
