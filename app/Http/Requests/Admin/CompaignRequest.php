<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CompaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name_ar'   => 'required|string',
            'name_en'   => 'required|string',
        ];
    }

    public function messages(){

        return [
            'name_ar.required' => trans('admin.required'),
            'name_en.required' => trans('admin.required'),
            'name_ar.string' => trans('admin.string'),
            'name_en.string' => trans('admin.string'),
        ];
    }
}
