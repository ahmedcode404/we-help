<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PhoneNumber;
use App\Rules\NationCode;
use Illuminate\Http\Request;

class BankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $this->route('bank');

        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                        'name_ar' => 'required',
                        'name_en' => 'required',
                        'address' => 'required',
                        'phone' => [new NationCode(auth()->user()->nation_id)],
                        'phone_num' => ['nullable','unique:charities,phone','unique:users,phone,'.$id,'min:8', 'max:10', new PhoneNumber(auth()->user()->nation_id)],
                        'city_id' => 'required',
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {

                    return [
                        'name_ar' => 'required',
                        'name_en' => 'required',
                        'address' => 'required',
                        'phone' => [new NationCode(auth()->user()->nation_id)],
                        'phone_num' => ['nullable','unique:charities,phone,'.$id,'unique:users,phone,'.$id,'min:8', 'max:10', new PhoneNumber(auth()->user()->nation_id)],
                        'city_id' => 'required',
                    ];

                }
                default:break;

        }
    }
}
