<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Project;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        $id = $this->route('project');

        $project = Project::find($id);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
        
                    'charity_category_id' => 'required',
                    'name'                => 'required|string',
                    // 'name_other'          => $this->name == 'other Name'  ? 'required' : '',
                    // 'duration'            => 'required',
                    'start_date'          => 'required|date',
                    // 'start_date'          => 'required|date|after:today',
                    'location'            => 'required',
                    'city'                => 'nullable|string',
                    'country'             => 'required',
                    'currency_id'         => 'required',
                    'benef_num'           => 'required',
                    'goals_ar'            => 'nullable|string',
                    'goals_en'            => 'required|string',
                    'desc_ar'             => 'nullable|string',
                    'desc_en'             => 'required|string',
                    'long_desc_ar'        => 'nullable|string',
                    'long_desc_en'        => 'required|string',
                    'images'              => 'required',
                    'images.*'            => 'required|mimes:jpg,png,jpeg|max:2048',
                    'attach'              => 'required|mimes:pdf|max:51200',
                    'image'              => 'required|mimes:jpg,png,jpeg|max:2048',
                    'service_option_id'  => 'nullable',
                    'service_feature_id'  => 'nullable',
                    'other_service_option' => 'required_if:service_option_id,other',
                    'other_service_feature' => 'required_if:service_feature_id,other',
                    'eng_maps' => 'nullable|mimes:jpg,png,jpeg|max:2048',
                    'cost'                => 'required', // التكلفه


                    // 'eng_maps'            => $this->charity_category_id == "4" ? 'required|mimes:jpg,png,jpeg,gif' : '', // الخرائط الهندسيه للمشروع - مطلوب فقط فى حالة المشاريع الانشائيه
                    // 'stage'               => $this->charity_category_id == "3" ? 'required' : '', // المرحلة التعليمية - مطلوبه فقط فى حالة المشاريع التعليميه
                    // 'edu_service_other'   => $this->edu_service_id == "other" ? 'required|string' : '', // خدمات تعليميه اخرى فى حالة لم يتم اختيار الخدمه من القائمه المنسدله - مطلوبه فقط فى حالة المشاريع التعليميه
                    // 'care_type'           => $this->charity_category_id == "5" ? 'required' : '', // مطلوبه فى حالة مشاريع كفالة اليتيم: رعاية - كسوة - عيديه
                    // 'care_duration'       => $this->charity_category_id == "5" ? 'required' : '',  // مدة الخدمه: سنويه - مقطوعه - مطلوبه فقط فى مشاريع كفالة اليتيم
                    // 'emerg_service_other' => $this->emerg_service_id == "other" ? 'required|string' : '', // خدمات طارئة اخرى - مطلوبة فقط فى حالة المشاريع الطارئة
                    // 'stable_project'      => $this->charity_category_id == "7" ? 'required' : '', // المشاريع المستدامه: دين - قرض حسن - تبرع و مطلوبه فقط فى حالة المشاريع المستدامه
                    // 'edu_service_id'      => $this->charity_category_id == "3" ? 'required' : '', // الخدمات التعليمية
                    // 'emerg_service_id'    => $this->charity_category_id == "6" ? 'required' : '', // الخدمات الطارئه 
        
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'project_num' => 'nullable|unique:projects,project_num,'.$id,
                    'charity_id' => 'nullable',
                    'name' => 'nullable',
                    'start_date' => 'nullable|date',
                    'duration' => 'nullable|numeric',
                    'cost' => 'nullable|numeric',
                    'currency_id' => 'nullable',
                    'benef_num' => 'nullable|numeric',
                    'charity_category_id' => 'nullable',
        
                    'service_option_id'  => 'nullable',
                    'service_feature_id'  => 'nullable',
                    'other_service_option' => 'required_if:service_option_id,other',
                    'other_service_feature' => 'required_if:service_feature_id,other',
                    'eng_maps' => 'nullable|mimes:jpg,png,jpeg|max:2048',
                    'image' => 'nullable|mimes:jpg,png,jpeg|max:2048',
        
                    'status' => 'nullable',
                    'notes' => 'nullable',
                    'country' => 'nullable',
                    'city' => 'nullable',
                    'location' => 'nullable',
                    'images.*' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
                    'attach' => 'nullable|mimetypes:application/pdf|max:51200',
                ];
            }
            default:break;
        }

        
    }
}
