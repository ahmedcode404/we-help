<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                        'title_ar'   => 'required|string|min:3',
                        'title_en'   => 'required|string|min:3',
                        'content_ar' => 'required|string|min:3',
                        'content_en' => 'required|string|min:3',
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {

                    return [
                        'title_ar'   => 'required|string|min:3',
                        'title_en'   => 'required|string|min:3',
                        'content_ar' => 'required|string|min:3',
                        'content_en' => 'required|string|min:3',
                    ];

                }
                default:break;

        } // end of switch           

    }

    public function message(){

        return [
            'title_ar.required' => trans('admin.required'),
            'title_ar.string' => trans('admin.string'),
            'title_ar.min' => trans('admin.string_min'),

            'title_en.required' => trans('admin.required'),
            'title_en.string' => trans('admin.string'),
            'title_en.min' => trans('admin.string_min'),

            'content_ar.required' => trans('admin.required'),
            'content_ar.string' => trans('admin.string'),
            'content_ar.min' => trans('admin.string_min'),

            'content_en.required' => trans('admin.required'),
            'content_en.string' => trans('admin.string'),
            'content_en.min' => trans('admin.string_min'),
        ];
    }
}
