<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PhaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $phaseRepository = \App::make('App\Repositories\PhaseRepositoryInterface');
        $projectRepository = \App::make('App\Repositories\ProjectRepositoryInterface');

        // Phase id
        $id = $this->route('id');

        $project = $projectRepository->findOne($request->project_id);
        
        if($this->method() == 'POST'){
            
            $prev_phase = $phaseRepository->getWhere([['project_id', $request->project_id], ['order', ($request->order - 1)]])->first();
            
        }
        else if($this->method() == 'PUT' || $this->method() == 'PATCH'){
            
            $phase = $phaseRepository->findOne($id);
            
            $prev_phase = $phaseRepository->getWhere([['project_id', $request->project_id], ['order', ($phase->order - 1)]])->first();
            
        }

        $date = '';

        if($prev_phase){
            $date = 'after:'.$prev_phase->end_date;
        }
        else{
            $date = 'after_or_equal:'.$project->start_date;
        }


        switch($this->method()){
            case 'GET':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'name_ar' => 'required',
                    'name_en' => 'required',
                    'desc_ar' => 'required',
                    'desc_en' => 'required',
                    'start_date' => 'required_unless:order,1|date|'.$date,
                    'end_date' => 'required|date|after:start_date',
                    'cost' => 'required|numeric',
                    'cost_details_ar' => 'required',
                    'cost_details_en' => 'required',
                    'output_ar' => 'required',
                    'output_en' => 'required',
                    'order' => ['required', Rule::unique('phases', 'order')
                                                ->where(function ($query) use ($request){
                                                    $query->where('project_id',$request->project_id);
                                                })],  
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'name_ar' => 'required',
                    'name_en' => 'required',
                    'desc_ar' => 'required',
                    'desc_en' => 'required',
                    // 'start_date' => 'required|date|'.$date,
                    'start_date' => 'required|date',
                    'end_date' => 'required|date|after:start_date',
                    'cost' => 'required|numeric',
                    'cost_details_ar' => 'required',
                    'cost_details_en' => 'required',
                    'output_ar' => 'required',
                    'output_en' => 'required',
                    // 'order' => ['required', Rule::unique('phases', 'order')
                    //                             ->where(function ($query) use ($request){
                    //                                 $query->where('project_id',$request->project_id);
                    //                             })->ignore($id)], 
                ];
        }
        
    }
}
