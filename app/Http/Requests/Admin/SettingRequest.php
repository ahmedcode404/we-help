<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [

            //SA

            "contract_intro_sa_en"      => "required_if:nation_id,1",
            "contract_intro_sa_ar"      => "required_if:nation_id,1",
            "we_help_desc_sa_ar"        => "required_if:nation_id,1",
            "we_help_desc_sa_en"        => "required_if:nation_id,1",
            "org_name_sa_ar"            => "required_if:nation_id,1",
            "org_name_sa_en"            => "required_if:nation_id,1",
            "license_number_sa"         => "required_if:nation_id,1",
            "address_sa"                => "required_if:nation_id,1",
            "representer_name_sa_ar"    => "required_if:nation_id,1",
            "representer_name_sa_en"    => "required_if:nation_id,1",
            "charity_approve_msg_sa_ar" => "required_if:nation_id,1",
            "charity_approve_msg_sa_en" => "required_if:nation_id,1",
            "charity_reject_msg_sa_ar"  => "required_if:nation_id,1",
            "charity_reject_msg_sa_en"  => "required_if:nation_id,1",
            "project_approve_msg_sa_ar" => "required_if:nation_id,1",
            "project_approve_msg_sa_en" => "required_if:nation_id,1",
            "project_reject_msg_sa_ar"  => "required_if:nation_id,1",
            "project_reject_msg_sa_en"  => "required_if:nation_id,1",
            "edit_approve_msg_sa_ar"    => "required_if:nation_id,1",
            "edit_approve_msg_sa_en"    => "required_if:nation_id,1",
            "edit_reject_msg_sa_ar"     => "required_if:nation_id,1",
            "edit_reject_msg_sa_en"     => "required_if:nation_id,1",
            "phases_number_sa"          => "required_if:nation_id,1",
            "min_support_sa"            => "required_if:nation_id,1",
            "default_currency_sa"       => "required_if:nation_id,1",
            "our_projects_text_sa_ar"   => "required_if:nation_id,1",
            "our_projects_text_sa_en"   => "required_if:nation_id,1",
            "statistics_text_sa_ar"     => "required_if:nation_id,1",
            "statistics_text_sa_en"     => "required_if:nation_id,1",
            "parteners_text_sa_ar"      => "required_if:nation_id,1",
            "parteners_text_sa_en"      => "required_if:nation_id,1",
            "about_us_text_sa_ar"       => "required_if:nation_id,1",
            "about_us_text_sa_en"       => "required_if:nation_id,1",
            "contact_us_text_sa_ar"     => "required_if:nation_id,1",
            "contact_us_text_sa_en"     => "required_if:nation_id,1",
            "website_sa"                => "required_if:nation_id,1",
            "email_sa"                  => "required_if:nation_id,1",
            "revieved_duration_sa"      => "required_if:nation_id,1", 
            "we_help_logo_sa_ar"        => "nullable|mimes:png,jpg,jpeg|max:2048", 
            "we_help_logo_sa_en"        => "nullable|mimes:png,jpg,jpeg|max:2048", 
            "logo_sa_ar"                => "nullable|mimes:png,jpg,jpeg|max:2048", 
            "logo_sa_en"                => "nullable|mimes:png,jpg,jpeg|max:2048", 
            "pages_header_sa"           => "nullable|mimes:png,jpg,jpeg|max:2048",
            "text_beneficiary_sa_en"    => "required_if:nation_id,1", 
            "text_beneficiary_sa_ar"    => "required_if:nation_id,1", 
            "text_partener_sa_en"       => "required_if:nation_id,1", 
            "text_partener_sa_ar"       => "required_if:nation_id,1", 
            "text_amount_sa_ar"         => "required_if:nation_id,1", 
            "text_amount_sa_en"         => "required_if:nation_id,1", 
            "text_project_sa_en"        => "required_if:nation_id,1", 
            "text_project_sa_ar"        => "required_if:nation_id,1", 
            "text_country_sa_en"        => "required_if:nation_id,1", 
            "text_country_sa_ar"        => "required_if:nation_id,1", 



            // UK
            "org_name_uk_ar"            => "required_if:nation_id,2",
            "org_name_uk_en"            => "required_if:nation_id,2",
            "license_number_uk"         => "required_if:nation_id,2",
            "address_uk"                => "required_if:nation_id,2",
            "website_uk"                => "required_if:nation_id,2",
            "representer_name_uk_ar"    => "required_if:nation_id,2",
            "representer_name_uk_en"    =>  "required_if:nation_id,2",
            "contract_intro_uk_ar"      => "required_if:nation_id,2",
            "contract_intro_uk_en"      => "required_if:nation_id,2",
            "revieved_duration_uk"      => "required_if:nation_id,2",
            "charity_approve_msg_uk_ar" => "required_if:nation_id,2",
            "charity_approve_msg_uk_en" => "required_if:nation_id,2",
            "charity_reject_msg_uk_ar"  => "required_if:nation_id,2",
            "charity_reject_msg_uk_en"  => "required_if:nation_id,2",
            "report_approve_msg_uk_ar"  => "required_if:nation_id,2",
            "report_approve_msg_uk_en"  => "required_if:nation_id,2",
            "report_reject_msg_uk_ar"   => "required_if:nation_id,2",
            "report_reject_msg_uk_en"   => "required_if:nation_id,2",
            "project_approve_msg_uk_ar" => "required_if:nation_id,2",
            "project_approve_msg_uk_en" => "required_if:nation_id,2",
            "project_reject_msg_uk_ar"  => "required_if:nation_id,2",
            "project_reject_msg_uk_en"  => "required_if:nation_id,2",
            "edit_approve_msg_uk_ar"    => "required_if:nation_id,2",
            "edit_approve_msg_uk_en"    => "required_if:nation_id,2",
            "edit_reject_msg_uk_ar"     => "required_if:nation_id,2",
            "edit_reject_msg_uk_en"     => "required_if:nation_id,2",
            "we_help_desc_uk_ar"        => "required_if:nation_id,2",
            "we_help_desc_uk_en"        => "required_if:nation_id,2",
            "we_help_logo_uk_ar"        => "nullable|mimes:png,jpg,jpeg|max:2048",
            "logo_uk_ar"                => "nullable|mimes:png,jpg,jpeg|max:2048",
            "logo_uk_en"                => "nullable|mimes:png,jpg,jpeg|max:2048",
            "we_help_logo_uk_en"        => "nullable|mimes:png,jpg,jpeg|max:2048",
            "stamp_uk"                  => "nullable|mimes:png,jpg,jpeg|max:2048",
            "we_help_slogan_uk_ar"      => "required_if:nation_id,2",                                                                      
            "we_help_slogan_uk_en"      => "required_if:nation_id,2",
            "msg_data_login_uk_ar"      => "required_if:nation_id,2",
            "msg_data_login_uk_en"      => "required_if:nation_id,2",
            "pages_header_uk"           => "nullable|mimes:png,jpg,jpeg|max:2048",
            "phone_uk"                  => "required_if:nation_id,2",
            "email_uk"                  => "required_if:nation_id,2",
            "phases_number_uk"          => "required_if:nation_id,2",
            "min_support_uk"            => "required_if:nation_id,2",
            "our_projects_text_uk_ar"   => "required_if:nation_id,2",
            "our_projects_text_uk_en"   => "required_if:nation_id,2",
            "statistics_text_uk_ar"     => "required_if:nation_id,2",
            "statistics_text_uk_en"     => "required_if:nation_id,2",
            "parteners_text_uk_ar"      => "required_if:nation_id,2",
            "parteners_text_uk_en"      => "required_if:nation_id,2",
            "about_us_text_uk_ar"       => "required_if:nation_id,2",
            "about_us_text_uk_en"       => "required_if:nation_id,2",
            "contact_us_text_uk_ar"     => "required_if:nation_id,2",
            "contact_us_text_uk_en"     => "required_if:nation_id,2",       
            "text_beneficiary_uk_en"    => "required_if:nation_id,2", 
            "text_beneficiary_uk_ar"    => "required_if:nation_id,2", 
            "text_partener_uk_en"       => "required_if:nation_id,2", 
            "text_partener_uk_ar"       => "required_if:nation_id,2", 
            "text_amount_uk_ar"         => "required_if:nation_id,2", 
            "text_amount_uk_en"         => "required_if:nation_id,2", 
            "text_project_uk_en"        => "required_if:nation_id,2", 
            "text_project_uk_ar"        => "required_if:nation_id,2", 
            "text_country_uk_en"        => "required_if:nation_id,2", 
            "text_country_uk_ar"        => "required_if:nation_id,2",
           
            
            

        ];
    }
}
