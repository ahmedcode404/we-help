<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class VIPSupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = $request->id;

        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                        'name' => 'required',
                        'email' => 'required|email|unique:users,email|unique:charities,email',
                        'phone' => 'required|unique:users,phone|unique:charities,phone|phone:'.$request->country,
                        'country' => 'required|string',
                        'project_id' => 'required|exists:projects,id',
                        'currency_id' => 'required|exists:currencies,id',
                        'project_covering' => 'required|in:total,partial',
                        'receipt' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
                        'cost' => 'required|numeric',
            
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {   

                    return [
                        'name' => 'required',
                        'email' => 'required|email|unique:users,email,'.$id.'|unique:charities,email,'.$id.'|unique:charities,representer_email,'.$id,
                        'phone' => 'required|unique:users,phone,'.$id.'|unique:charities,phone,'.$id.'|phone:'.$request->country.'|unique:charities,mobile,'.$id,
                        'country' => 'required|string',
                        'project_id' => 'required|exists:projects,id',
                        'currency_id' => 'required|exists:currencies,id',
                        'project_covering' => 'required|in:total,partial',
                        'receipt' => 'nullable|mimes:jpeg,png,jpg,pdf|max:51200',
                        'cost' => 'required|numeric',
            
                    ];

                }
                default:break;

        }
    }
}
