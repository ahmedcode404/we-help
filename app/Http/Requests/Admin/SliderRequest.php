<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [
                        'path' => 'required|image|mimes:jpeg,png,jpg|max:2048',
                        'text_ar' => 'required',
                        'text_en' => 'required',
                        'appearance' => 'required',
                    ];
                }
                case 'PUT':
                case 'PATCH':
                {

                    return [
                        'path' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
                        'text_ar' => 'required',
                        'text_en' => 'required',
                        'appearance' => 'required',
                    ];

                }
                default:break;

        }
    }

}
