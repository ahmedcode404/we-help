<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Rules\PhoneNumber;
use App\Rules\TextLength;
use App\Rules\NationCode;
use App\Rules\IBAN;
use App\Rules\SWIFT;

class CharityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        
        $id = $this->route('charity');

        return [
            'type' => 'nullable|in:foundation,organization,charity',
            'name' => 'nullable',
            'license_number' => 'nullable',
            'license_start_date' => 'nullable',
            'license_end_date' => 'nullable',
            'license_file' => 'nullable|mimes:jpeg,png,jpg,pdf|max:51200',
            'establish_date' => 'nullable',
            'branches_num' => 'nullable',
            'members_num' => 'nullable',
            'contract_date' => 'nullable',
            'strategy_ar' => [new TextLength($request->strategy_ar)],
            'strategy_en' => [new TextLength($request->strategy_en)],
            'attach' => 'nullable|mimes:jpeg,png,jpg,pdf|max:51200',
            'internal_image' => 'nullable|mimes:jpeg,png,jpg,pdf|max:51200',
            'year_report' => 'nullable|mimes:jpeg,png,jpg,pdf|max:51200',
            'years' => 'nullable',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
            'address' => 'nullable',
            'city' => 'nullable',
            'country' => 'nullable',
            // 'city_id' => 'nullable',
            // 'country_id' => 'nullable',

            'phone' => 'nullable|unique:charities,phone,'.$id.'|unique:users,phone,'.$id.'|phone:'.$request->country,
            'mobile' => 'nullable|unique:charities,phone,'.$id.'|unique:users,phone,'.$id.'|phone:'.$request->country,
            
            'email' => 'nullable|email|unique:charities,email,'.$id,'unique:users,email,'.$id,
            'representer_name_ar' => 'nullable',
            'representer_name_en' => 'nullable',
            'representer_passport_image' => 'nullable|mimes:jpeg,png,jpg,pdf|max:51200',
            'representer_nation_image' => 'nullable|mimes:jpeg,png,jpg,pdf|max:51200',
            'representer_title_ar' => 'nullable',
            'representer_title_en' => 'nullable',
            'representer_title_file' => 'nullable|mimetypes:application/pdf|max:51200',
            'representer_email' => 'nullable|email|unique:charities,email,'.$id,'unique:users,email,'.$id,
            'international_member' => 'nullable',
            'international_name_ar' => $request->has('international_member') ? 'required' : 'nullable',
            'international_name_en' => $request->has('international_member') ? 'required' : 'nullable',
            'website' => 'nullable',
            'twitter_link' => 'nullable',
            'facebook_link' => 'nullable',
            'swift_code' => 'nullable',
            'status' => 'nullable|in:waiting,approved,rejected,hold',
            'work_fields' => 'nullable',

            'iban' => 'nullable',
            // 'iban' => ['nullable', new IBAN(), 'max:35'],
            'bank_name' => 'nullable|min:3',
            'bank_address' => 'nullable|min:3',
            'bank_city' => 'nullable|min:3',
            'bank_phone' => 'nullable|min:9|max:16',
            'swift_code' => 'nullable',
            // 'swift_code' => ['nullable', new SWIFT()],
        ];
    }

    public function messages(){
        return [
            'type.in' => trans('admin.charity_type_in'),
            'license_file.mimes' => trans('admin.image_mix_type'),
            'license_file.max' => trans('admin.image_max'),
            'attach.mimes' => trans('admin.image_mix_type'),
            'attach.mimes.max' => trans('admin.image_max'),
            'internal_image.mimes' => trans('admin.image_mix_type'),
            'internal_image.max' => trans('admin.image_max'),
            'year_report.mimes' => trans('admin.image_mix_type'),
            'year_report.max' => trans('admin.image_max'),
            'logo.mimes' => trans('admin.images_mimes'),
            'logo.max' => trans('admin.image_max'),
            'phone.unique' => trans('admin.unique'),
            'mobile.unique' => trans('admin.unique'),
            'email.email' => trans('admin.valid_email'),
            'email.unique' => trans('admin.unique'),
            'representer_passport_image.mimes' => trans('admin.image_mix_type'),
            'representer_passport_image.max' => trans('admin.image_max'),
            'representer_nation_image.mimes' => trans('admin.image_mix_type'),
            'representer_nation_image.max' => trans('admin.image_max'),
            'representer_title_file.mimes' => trans('admin.valid_pdf'),
            'representer_title_file.max' => trans('admin.pdf_max'),
            'international_name_ar.required' => trans('admin.required'),
            'international_name_en.required' => trans('admin.required'),
            'status.in' => trans('admin.status_in'),
            'bank_name.min' => trans('admin.string_min'),
            'bank_address.min' => trans('admin.string_min'),
            'bank_city.min' => trans('admin.string_min'),
            'bank_phone.min' => trans('admin.phone_min'),
            'bank_phone.max' => trans('admin.phone_max'),
        ];
    }
}
