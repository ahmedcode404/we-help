<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MarketingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'title' => 'required',
            'content' => 'required',
            'date' => 'required|date|after:yesterday',
            'time' => 'required|date_format:H:i|after:'.now()->format('H:i'),
            'aff' => 'required_if:project_link,null',
            'project_link' => 'required_if:aff,null',
            'intended_users' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => trans('admin.required'),
            'title.required' => trans('admin.required'),
            'content.required' => trans('admin.required'),
            'date.required' =>  trans('admin.required'),
            'date.date' => trans('admin.valid_date'),
            'date.after' => trans('admin.date_after'),

            'time.required' => trans('admin.required'),
            'time.date_format' => trans('admin.time_format'),

            'aff.required_if' => trans('admin.required'),
            'intended_users.required' => trans('admin.required'),
            'project_link.required_if' => trans('admin.required'),
        ];
    }
}
