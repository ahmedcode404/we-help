<?php

namespace App\Http\Requests\Web;

use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UpdateDonateMonthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        // dd($request->all());
        if (getNationId() == 1) {
            
            $cost_min = Setting::where('key' , 'min_support_sa')->first()['value'];

        } else 
        {
            
            $cost_min = Setting::where('key' , 'min_support_uk')->first()['value'];

        }        

        if (! $request->switch_1) {

            return [];
            
        } else 
        {

            $projectuserRepository = \App::make('App\Repositories\ProjectUserRepositoryInterface');
            $cost_old = $projectuserRepository->findOne($request->month_id);
            $projectRepository     = \App::make('App\Repositories\ProjectRepositoryInterface');
            $project = $projectRepository->findWith($request->project_id , ['supports']);
            $cost_donate  = getTotalSupportsProject($project , $request->currency_id);
            
            $donate_cost  = exchange($project->get_total, $project->currency_id, $request->currency_id) - $cost_donate + $cost_old->cost;            
            // dd($donate_cost);
            if ($request->number_input >= $donate_cost) {

                $cost_min = $donate_cost;

            }            

            return [
    
                'number_input' => 'required|numeric|min:' . $cost_min . '|max:' . $donate_cost,
    
            ];

        } // end of else
    }
}
