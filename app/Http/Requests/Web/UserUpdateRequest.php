<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = auth()->user()->id;

        return [
            'name'       => 'required',
            'email'      => 'required|email|unique:users,email,'.$id,
            'country' => 'required',
            'password'   => 'nullable|min:6',
            'password_confirmation' => $request->password != null ? 'min:6|same:password' : 'nullable',
            'phone' => 'required|min:8|max:16|unique:users,phone,'.$id.'|unique:charities,phone,'.$id.'|unique:charities,mobile,'.$id.'|phone:'.$request->country,
        ];
    }
}
