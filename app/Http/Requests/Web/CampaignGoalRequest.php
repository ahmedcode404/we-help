<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;

class CampaignGoalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'ambassador_name'  => 'required',
            'campaign_name'    => 'required',
            'campaign_goal_id' => 'required',
            'amb_check_1'      => 'nullable',

        ];
    }
}
