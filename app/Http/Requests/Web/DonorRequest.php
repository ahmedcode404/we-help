<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PhoneNumber;
use App\Rules\NationCode;
use Illuminate\Http\Request;

class DonorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd($request->all());
        session(['donor_phone' => $request->phone]);
        
        $nation_id = getNationId();

        return [
            'name' => 'required',
            'email' => 'required|email|unique:users,email|unique:charities,email',
            'phone' => 'required|unique:charities,phone|unique:users,phone|phone:'.$request->country,
            'country' => 'required|not_in:SA,AE',
            'password' => 'required|min:6|confirmed',
        ];
    }


    public function messages(){

        return [
            'name.required' => trans('admin.required'),
            'email.required' => trans('admin.required'),
            'email.email' => trans('admin.valid_email'),
            'email.unique' => trans('admin.unique'),
            'phone.required' => trans('admin.required'),
            'phone.unique' => trans('admin.unique'),
            'phone.phone' => trans('admin.valid_phone'),
            'country.required' => trans('admin.required'),
            'country.not_in' => trans('admin.country_not_in'),
            'password.required' => trans('admin.required'),
            'password.confirmed' => trans('admin.confirmed'),
            'password.min' => trans('admin.password_min'),
        ];
    }
}
