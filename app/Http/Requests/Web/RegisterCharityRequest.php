<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\Rules\PhoneNumber;
use App\Rules\TextLength;
use App\Rules\NationCode;
use App\Rules\IBAN;
use App\Rules\SWIFT;

class RegisterCharityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        // dd($request->all());
        session(['charity_phone' => $request->phone]);
        session(['charity_mobile' => $request->mobile]);
        session(['bank_phone' => $request->bank_phone]);
        
        // dd($request->all());
        
        $id = $this->route('charity');

        $nation_id = getNationId();

        return [
            'type' => 'required|in:foundation,organization,charity',
            'name' => 'required',
            'license_number' => 'required|min:5',
            'license_start_date' => 'required|date',
            'license_end_date' => 'required|date',
            'contract_date' => 'required|date',
            'license_file' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
            'establish_date' => 'required|date',
            'branches_num' => 'required|numeric',
            'members_num' => 'required|numeric',
            'strategy_ar' => $request->has('admin_add_charity') ? ['required', new TextLength($request->strategy_ar)] : ['nullable', new TextLength($request->strategy_ar)],
            'strategy_en' => ['required', new TextLength($request->strategy_en)],
            'attach' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
            'internal_image' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
            'year_report' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
            'years' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'address_ar' => $request->has('admin_add_charity') ? 'required' : 'nullable',
            'address_en' => 'required',
            'city' => 'required',
            'country' => 'required|not_in:SA,AE',
            'password' => $request->has('admin_add_charity') ? 'nullable' : 'required|min:6|confirmed',
            'contract_after_sign' => $request->has('admin_add_charity') ? 'required|mimes:pdf,max:51200' : 'nullable',

            'phone' => 'required|unique:charities,phone,'.$id.'|unique:users,phone,'.$id.'|phone:'.$request->country,
            'mobile' => 'required|unique:charities,phone,'.$id.'|unique:users,phone,'.$id.'|phone:'.$request->country,
            
            'email' => 'required|email|unique:charities,email','unique:users,email',
            'representer_name_en' => 'required',
            'representer_name_ar' => $request->has('admin_add_charity') ? 'required' : 'nullable',
            'representer_passport_image' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
            'representer_nation_image' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
            'representer_title_ar' => $request->has('admin_add_charity') ? 'required' : 'nullable',
            'representer_title_en' => 'required',
            'representer_title_file' => 'required|mimes:jpeg,png,jpg,pdf|max:51200',
            'representer_email' => 'required|email|unique:charities,email','unique:users,email',
            'international_name_ar' => $request->has('admin_add_charity') ? 'required_if:international_member,1' : 'nullable',
            'international_name_en' => 'required_if:international_member,1',
            // 'website' => 'required|url',
            // 'twitter_link' => 'required|url',
            // 'facebook_link' => 'required|url',
            'iban' => 'required',
            'swift_code' => 'required',
            'projects_names' => 'nullable|array|max:6',
            'projects_types' => 'nullable|array|max:6',

            'iban' => 'required',
            // 'iban' => ['required', new IBAN(), 'max:35'],
            'bank_name' => 'required',
            'bank_address' => 'required',
            'bank_city' => 'required',
            'bank_phone' => 'required|min:9|max:16',
            'swift_code' => 'required',
            // 'swift_code' => ['required', new SWIFT()],
            'work_fields' => 'required',
        ];
    }
}
