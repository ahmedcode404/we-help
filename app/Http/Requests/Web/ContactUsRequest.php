<?php

namespace App\Http\Requests\Web;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\PhoneNumber;
use App\Rules\NationCode;

class ContactUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $nation_id = getNationId();
        
        return [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => [new NationCode($nation_id)],
            'contact_number' => ['required','min:8', 'max:10', new PhoneNumber($nation_id)],
            'content' => 'required',
        ];
    }
}
