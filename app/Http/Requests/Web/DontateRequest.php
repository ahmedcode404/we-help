<?php

namespace App\Http\Requests\Web;

use App\Models\Setting;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class DontateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        // dd($request->all());
        if (getNationId() == 1) {
            
            $cost_min = Setting::where('key' , 'min_support_sa')->first()['value'];

        } else 
        {
            
            $cost_min = Setting::where('key' , 'min_support_uk')->first()['value'];

        }          

        
        $projectuserRepository = \App::make('App\Repositories\ProjectUserRepositoryInterface');
        $projectRepository     = \App::make('App\Repositories\ProjectRepositoryInterface');
        $project = $projectRepository->findWith($request->project_id , ['supports']);
        
        $cost_donate  = getTotalSupportsProject($project , $request->currency_id);

        $donate_cost  = exchange($project->get_total, $project->currency_id, $request->currency_id) - $cost_donate;            

        
        if ($request->number_input >= $donate_cost) {

            $cost_min = $donate_cost;

        }           

        return [

            // validate type once
            // 'once_check_2'       => 'nullable|required_if:type,once',

            // validate type monthly dedction
            // 'month_check_2'      => 'nullable|required_if:type,monthly',

            // validate type gift 
            // 'name'               => 'nullable|required_if:type,gift',
            'name2.0'            => 'nullable|required_if:type,gift',
            'tel2.0'             => 'nullable|required_if:type,gift',
            'email2.0'           => 'nullable|required_if:type,gift',
            // 'gift_check_1'       => 'nullable|required_if:type,gift',

            // validate type ambassador
            // 'price_ambsaador'    => 'nullable|required_if:type,ambassador|numeric|min:' . $cost_min . '|max:' . $donate_cost,
            'price_ambsaador'    => 'nullable|required_if:type,ambassador|numeric|max:' . $donate_cost,
            
        ];

    } // end of rules
   
}
