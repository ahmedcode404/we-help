<?php

namespace App\Http\Requests\Charity;

use App\Rules\BigDate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PhasesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {

                    $phaseRepository = \App::make('App\Repositories\PhaseRepositoryInterface');
                    $last = null;
                    $last =  $phaseRepository->last($request->project_id);

                    if($last != null)
                    {
                        $end = 'required|after:' . $last->end_date;
                    } else 
                    {
                        $end = 'required';
                    }

                    return [    

                        'project_id'      => 'required',
                        'name_ar'         => 'nullable|string',
                        'name_en'         => 'required|string',
                        'start_date'      => $end,
                        'end_date'        => 'required|date|after:start_date',
                        'duration'        => 'required',
                        'cost'            => 'required',
                        'order'           => 'required',
                        'output_ar'       => 'nullable|string',
                        'output_en'       => 'required|string',
                        'desc_ar'         => 'nullable|string',
                        'desc_en'         => 'required|string',
                        'cost_details_ar' => 'nullable|string',
                        'cost_details_en' => 'required|string',

                    ];
                }
                case 'PUT':
                case 'PATCH':
                {

                    $phaseRepository = \App::make('App\Repositories\PhaseRepositoryInterface');
                 
                    $previou = $phaseRepository->previou($request->item);
                    $next    = $phaseRepository->next($request->item);

                    if($next != null)
                    {
                        $end = 'required|date|after:start_date|before:' . $next;
                    } else 
                    {
                        $end = 'required';
                    }

                    if($previou != null)
                    {
                        $start = 'required|date|after:' . $previou;
                    } else 
                    {
                        $start = 'required';
                    }                    

                    return [
            
                        'project_id'      => 'required',
                        'name_ar'         => 'nullable|string',
                        'name_en'         => 'required|string',
                        'start_date'      => $start,
                        'end_date'        => $end,
                        'duration'        => 'required',
                        'cost'            => 'required',
                        'order'           => 'required',
                        'output_ar'       => 'nullable|string',
                        'output_en'       => 'required|string',
                        'desc_ar'         => 'nullable|string',
                        'desc_en'         => 'required|string',
                        'cost_details_ar' => 'nullable|string',
                        'cost_details_en' => 'required|string',

                    ];
                }
                default:break;

        } // end of switch    

    } // end of rules

    public function messages()
    {
        // use trans instead on Lang 
        return [

            // 'start_date.unique' => __('admin.date_find'),
            
        ];
    } // end of messages    
}
