<?php

namespace App\Http\Requests\Charity;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class SponserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [

            'project_id'      => 'required',
            'work_field_id'   => 'required',
            'name_sponser_ar' => 'required',
            'name_sponser_en' => 'required',

        ];
    }
}
