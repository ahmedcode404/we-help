<?php

namespace App\Http\Requests\Charity;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class ReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        
        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {

                    return [
                        'report_ar'  => 'required',
                        'report_en'  => 'required',
//                        'vedio'      => 'nullable|mimes:mp4,mov,ogg,qt,avi|max:51200',
                        'project_id' => 'required',
                        'images'     => 'required',
                        'images.*'   => 'required|mimes:png,jpeg,gif,jpg|max:2048',
                    ];

                }
                case 'PUT':
                case 'PATCH':
                {

                    return [
                        'report_ar'  => 'required',
                        'report_en'  => 'required',
                        'project_id' => 'required',
                        'vedio'      => 'nullable|mimes:mp4,mov,ogg,qt,avi|max:51200',
                        // 'images'     => 'required',
                        'images.*'   => 'mimes:png,jpeg,gif,jpg|max:2048',
                    ];

                }
                default:break;

        } // end of switch

    }
}
