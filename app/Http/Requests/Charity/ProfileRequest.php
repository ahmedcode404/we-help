<?php

namespace App\Http\Requests\Charity;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|unique:charities,email,'.auth()->user()->id,
            'phone' => 'nullable|unique:charities,phone,'.auth()->user()->id,
            'mobile' => 'nullable|unique:charities,mobile,'.auth()->user()->id,
            'phone_num' => 'nullable','unique:charities,phone,'.auth()->user()->id,'unique:users,phone,'.auth()->user()->id,'min:8', 'max:10',
            'mobile_num' => 'nullable','unique:charities,mobile,'.auth()->user()->id,'unique:users,phone,'.auth()->user()->id, 'min:8', 'max:10',
            'logo' => 'nullable|image|mimes:jpeg,png,jpg|max:2048',
        ];
    }
}
