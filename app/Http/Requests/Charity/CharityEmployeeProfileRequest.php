<?php

namespace App\Http\Requests\Charity;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CharityEmployeeProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $id = auth()->user()->id;

        return [
            'name_ar'             => 'required|string',
            'name_en'             => 'required|string',
            'country'             => 'required|string',
            'city'                => 'nullable|string',
            'region'              => 'required|string',
            'street'              => 'required|string',
            'unit_number'         => 'required|numeric',
            'mail_box'            => 'required|string',
            'postal_code'         => 'required|numeric',
            'phone'               => 'required|unique:charities,phone,'.$id.'|unique:users,phone,'.$id,
            'phone_num'           => 'required|unique:charities,phone,'.$id.'|unique:users,phone,'.$id.'|min:8|max:10',
            'email'               => 'required|email|unique:users,email,'.$id.'|unique:charities,email,'.$id,
        ]; 
    }
}
