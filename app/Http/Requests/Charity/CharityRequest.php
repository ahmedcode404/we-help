<?php

namespace App\Http\Requests\Charity;

use Illuminate\Foundation\Http\FormRequest;
use  Auth;
use Illuminate\Http\Request;
use App\Rules\PhoneNumber;
use App\Rules\TextLength;
use App\Rules\NationCode;
use App\Rules\IBAN;
use App\Rules\SWIFT;

class CharityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {

        if(auth()->user()->hasRole('charity_employee')){

            $user = auth()->user()->charity;
        }
        else{
            
            $user = auth()->user();
        }

        $nation_id = getNationId();

        switch($this->method())
        {
            case 'GET':
                case 'DELETE':
                {
                    return [];
                }
                case 'POST':
                {
                    return [                     
            
                        
                        'project_id'      => 'required',
                        'name_ar'         => 'required|string',
                        'name_en'         => 'required|string',
                        'start_date'      => 'required|date|after:today',
                        'end_date'        => 'required|date|after_or_equal:start_date',
                        'duration'        => 'required',
                        'cost'            => 'required',
                        'order'           => 'required',
                        'output_ar'       => 'required|string',
                        'output_en'       => 'required|string',
                        'desc_ar'         => 'required|string',
                        'desc_en'         => 'required|string',
                        'cost_details_ar' => 'required|string',
                        'cost_details_en' => 'required|string',

                    ];
                }
                case 'PUT':
                case 'PATCH':
                {

                    if ($request->type_form == 'basic-info') {

                        return [
            
                            'name'       => 'required',
                            'type'       => 'required|string',
                            'country'    => 'required',
                            'city'       => 'nullable',
                            // 'country_id' => 'required|string',
                            // 'city_id'    => 'required',
                            'email'      => 'required|email|unique:charities,email,'.$user->id.'|unique:users,email,'.$user->id,
                            // 'phone' => [new NationCode($nation_id)],
                            // 'mobile' => [new NationCode($nation_id)],
                            // 'phone_num' => ['nullable','unique:charities,phone,'.Auth::user()->id,'unique:users,phone,'.Auth::user()->id,'min:8', 'max:10', new PhoneNumber($nation_id)],
                            // 'mobile_num' => ['nullable','unique:charities,mobile,'.Auth::user()->id,'unique:users,phone,'.Auth::user()->id, 'min:8', 'max:10', new PhoneNumber($nation_id)],

                            'phone' => 'required|unique:charities,phone,'.$user->id.'|unique:users,phone,'.$user->id,
                            'mobile' => 'required|unique:charities,mobile,'.$user->id,
                            'phone_num' => 'nullable|unique:charities,phone,'.$user->id.'|unique:users,phone,'.$user->id.'|min:8|max:10',
                            'mobile_num' => 'nullable|unique:charities,mobile,'.$user->id.'|unique:users,phone,'.$user->id.'|min:8|max:10',
                            'address_ar' => 'required|string',
                            'address_en' => 'required|string',
                            'logo'       => 'mimes:png,jpg,gif,jpeg',
    
                        ];

                    } else if($request->type_form == 'official-info')
                    {

                        return [
            
                            'license_start_date' => 'required',
                            'license_end_date'   => 'required',
                            'establish_date'     => 'required',
                            'contract_date'      => 'required',
                            'branches_num'       => 'required',
                            'members_num'        => 'required',
                            'years'              => 'required',
                            'attach'             => 'mimes:jpeg,png,jpg,pdf|max:51200',
                            // 'license_number'     => 'mimes:pdf',
                            'license_file'       => 'mimes:jpeg,png,jpg,pdf|max:51200',
                            'year_report'        => 'mimes:jpeg,png,jpg,pdf|max:51200',
                            'internal_image'     => 'mimes:jpeg,png,jpg,pdf|max:51200',
                            'strategy_ar' => ['nullable', new TextLength($request->strategy_ar)],
                            'strategy_en' => [new TextLength($request->strategy_en)],
                            'work_fields' => 'required',
    
                        ];                        

                    } else if($request->type_form == 'representer-form')
                    {

                        return [
            
                            'representer_name_ar'        => 'nullable',
                            'representer_name_en'        => 'required',
                            'representer_title_ar'       => 'nullable',
                            'representer_title_en'       => 'required',
                            'representer_title_file'     => 'mimes:jpeg,png,jpg,pdf|max:51200',
                            'representer_passport_image' => 'mimes:jpeg,png,jpg,pdf|max:51200',
                            'representer_nation_image'   => 'mimes:jpeg,png,jpg,pdf|max:51200',
                            'international_name_en'      => $request->international_member == 1 ? 'required' : '',
                            // 'international_name_ar'      => $request->international_member == 1 ? 'required' : '',
                            'international_name_ar'      => 'nullable',
    
                        ];                        

                    }
                    else if($request->type_form == 'bank-data-form')
                    {
                        
                        return [
            
                            'iban' => 'required',
                            // 'iban' => ['required', new IBAN(), 'max:35'],
                            'bank_name' => 'required|min:3',
                            'bank_address' => 'required|min:3',
                            'bank_city' => 'required|min:3',
                            'bank_phone' => 'required|min:8|max:14',
                            'swift_code' => 'required',
                            // 'swift_code' => ['required', new SWIFT()],
    
                        ];                        

                    } 
                    else if($request->type_form == 'socials-form')
                    {
                        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
                        return [
            
                            'website'       => 'nullable',
                            'facebook_link' => 'nullable',
                            'twitter_link'  => 'nullable',

                        ];                        

                    }                                          
                    
                }
                default:break;

        } // end of switch  
    }
}
