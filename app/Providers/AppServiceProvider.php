<?php

namespace App\Providers;

use App\Models\CampaignGoal;
use App\Models\Currency;
use App\Models\Notification;
use App\Models\Project;
use App\Models\Setting;
use App\Models\City;
use App\Observers\Charity\ProjectObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Monarobase\CountryList\CountryListFacade;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $nation_id = getNationId();

        if(! session('currency'))
        {
            // get currncey default 
            // session()->put('currency' , getSettingData('default_currency'));
            if($nation_id == 1){

                session()->put('currency' , 2);
            }
            else{

                session()->put('currency' , 4);
            }

        }
        
        // $settings = Setting::where('nation_id' , $nation_id)->get();
        
        
        $campaign_goals = CampaignGoal::all();


        if ($nation_id == 1) {
            
            $cost_min = Setting::where('key' , 'min_support_sa')->first()['value'];
            $pages_header = Setting::where('key', 'pages_header_sa')->first()['value'];

        } else 
        {
            
            $cost_min = Setting::where('key' , 'min_support_uk')->first()['value'];
            $pages_header = Setting::where('key', 'pages_header_uk')->first()['value'];

        }

        view()->share(['campaign_goals' => $campaign_goals]);

        $currencies = Currency::where('nation_id' , $nation_id)->get();
 
        view()->share(['currencies' => $currencies , 'cost_min' => $cost_min, 'pages_header' => $pages_header]);

        $footer_logo = getWebSettingMsg('logo')->value;
        $header_logo = getWebSettingMsg('we_help_logo')->value;
        $footer_about_us = getWebSettingMsg('about_us_text')->value;
        $twitter = getSettingData('twitter');
        $facebook = getSettingData('facebook');
        $insta = getSettingData('insta');
        $snapchat = getSettingData('snapchat');
        $enable_logo_svg = getSettingValue('enable_svg_logo', $nation_id)->value;

        view()->share(['footer_logo' => $footer_logo, 'footer_about_us' => $footer_about_us, 'twitter' => $twitter,
                        'facebook' => $facebook, 'insta' => $insta, 'snapchat' => $snapchat, 'header_logo' => $header_logo,
                    'enable_logo_svg' => $enable_logo_svg]);
 
        if($nation_id == 1){

            // $default_currency = Setting::where('key', 'default_currency_sa')->first()->value;
            $default_currency = 2;
        }
        else{

            // $default_currency = Setting::where('key', 'default_currency_uk')->first()->value;
            $default_currency = 2;
        }

        view()->share(['default_currency' => $default_currency]);

        view()->composer('*', function ($view) 
        {

            $nation_id = getNationId();

            view()->share(['nation_id' => $nation_id]);

            // $currency_providers = Currency::where('nation_id' , $nation_id)->get();

            // view()->share(['currency_providers' => $currency_providers]);

            // $countries = City::where([['parent_id', null], ['nation_id', $nation_id]])->get();
            $countries = CountryListFacade::getList(\App::getLocale());

            view()->share(['countries' => $countries]);

            
            if(! session('currency'))
            {

                // get currncey default 
                // session()->put('currency' , getSettingData('default_currency'));
            if($nation_id == 1){

                session()->put('currency' , 2);
            }
            else{
                
                session()->put('currency' , 4);
            }

            }

            if (auth()->guard('charity')->user()) {

                $charity_notifications = Auth::guard('charity')->user()->notifications()->paginate(6);

                view()->share(['charity_notifications' => $charity_notifications]);

            } else if(Auth::user()) 
            {
    
                $admin_notifications = Auth::user()->notifications()->where('type' , 'admin')->limit(6)->get();
                $emp_notifications = Auth::user()->notifications()->where('type' , 'employee')->limit(6)->get();
                
                view()->share(['admin_notifications' => $admin_notifications], ['emp_notifications' => $emp_notifications]);
    
            }            

        });


        schema::defaultStringLength(191);
        Project::observe(new ProjectObserver);

    }
}
