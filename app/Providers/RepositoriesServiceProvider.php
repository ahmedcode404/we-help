<?php

namespace App\Providers;

use App\Repositories\AgreementRepositoryInterface;
use App\Repositories\AmbassadorRepositoryInterface;
use App\Repositories\BaseRepositoryInterface;
use App\Repositories\CampaignGoalRepositoryInterface;
use App\Repositories\CartRepositoryInterface;
use App\Repositories\CharityRepositoryInterface;
use App\Repositories\CityRepositoryInterface;
use App\Repositories\PermissionRepositoryInterface;
use App\Repositories\UserRepositoryInterface;
use App\Repositories\ProjectRepositoryInterface;
use App\Repositories\ServiceOptionRepositoryInterface;
use App\Repositories\JobCategoryRepositoryInterface;
use App\Repositories\JobRepositoryInterface;
use App\Repositories\WorkFieldRepositoryInterface;
use App\Repositories\CharityCategoryRepositoryInterface;
use App\Repositories\ContactRepositoryInterface;
use App\Repositories\ContractRepositoryInterface;
use App\Repositories\StaticPageRepositoryInterface;
use App\Repositories\SliderRepositoryInterface;
use App\Repositories\CurrencyRepositoryInterface;
use App\Repositories\DegreeRepositoryInterface;
use App\Repositories\DonateRepositoryInterface;
use App\Repositories\EditRequestRepositoryInterface;
use App\Repositories\SettingRepositoryInterface;
use App\Repositories\PhaseRepositoryInterface;
use App\Repositories\ReportRepositoryInterface;
use App\Repositories\ImageRepositoryInterface;
use App\Repositories\PartenerRepositoryInterface;
use App\Repositories\MarketingRepositoryInterface;
use App\Repositories\RatingCriteriaRepositoryInterface;
use App\Repositories\SponserRepositoryInterface;
use App\Repositories\SupportRepositoryInterface;
use App\Repositories\RatingRepositoryInterface;
use App\Repositories\NotificationRepositoryInterface;
use App\Repositories\BankRepositoryInterface;
use App\Repositories\FinancialRequestRepositoryInterface;
use App\Repositories\FrozenSupportRepositoryInterface;
use App\Repositories\PaymentTransactionRepositoryInterface;
use App\Repositories\NationalityRepositoryInterface;
use App\Repositories\DeviceTokenRepositoryInterface;
use App\Repositories\LogRepositoryInterface;



use App\Repositories\Eloquent\AgreementRepository;
use App\Repositories\Eloquent\AmbassadorRepository;
use App\Repositories\Eloquent\BaseRepository;
use App\Repositories\Eloquent\CampaignGoalRepository;
use App\Repositories\Eloquent\CartRepository;
use App\Repositories\Eloquent\CharityRepository;
use App\Repositories\Eloquent\CityRepository;
use App\Repositories\Eloquent\PermissionRepository;
use App\Repositories\Eloquent\ProjectRepository;
use App\Repositories\Eloquent\ServiceOptionRepository;
use App\Repositories\Eloquent\UserRepository;
use App\Repositories\Eloquent\JobCategoryRepository;
use App\Repositories\Eloquent\JobRepository;
use App\Repositories\Eloquent\WorkFieldRepository;
use App\Repositories\Eloquent\CharityCategoryRepository;
use App\Repositories\Eloquent\PhaseRepository;
use App\Repositories\Eloquent\ContactRepository;
use App\Repositories\Eloquent\ContractRepository;
use App\Repositories\Eloquent\StaticPageRepository;
use App\Repositories\Eloquent\SliderRepository;
use App\Repositories\Eloquent\CurrencyRepository;
use App\Repositories\Eloquent\DegreeRepository;
use App\Repositories\Eloquent\DonateRepository;
use App\Repositories\Eloquent\EditRequestRepository;
use App\Repositories\Eloquent\GiftRepository;
use App\Repositories\Eloquent\ImageRepository;
use App\Repositories\Eloquent\ReportRepository;
use App\Repositories\Eloquent\SettingRepository;
use App\Repositories\Eloquent\SponserRepository;
use App\Repositories\Eloquent\PartenerRepository;
use App\Repositories\Eloquent\MarketingRepository;
use App\Repositories\Eloquent\ProjectUserRepository;
use App\Repositories\Eloquent\RatingCriteriaRepository;
use App\Repositories\Eloquent\SupportRepository;
use App\Repositories\Eloquent\RatingRepository;
use App\Repositories\Eloquent\NotificationRepository;
use App\Repositories\Eloquent\BankRepository;
use App\Repositories\Eloquent\FaqRepository;
use App\Repositories\FaqRepositoryInterface;
use App\Repositories\GiftRepositoryInterface;
use App\Repositories\ProjectUserRepositoryInterface;
use App\Repositories\Eloquent\FinancialRequestRepository;
use App\Repositories\Eloquent\SubscribeRepository;
use App\Repositories\Eloquent\FrozenSupportRepository;
use App\Repositories\Eloquent\PaymentTransactionRepository;
use App\Repositories\Eloquent\NationalityRepository;
use App\Repositories\Eloquent\DeviceTokenRepository;
use App\Repositories\Eloquent\LogRepository;

use App\Repositories\SubscribeRepositoryInterface;

use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(){

        $this->app->bind(BaseRepositoryInterface::class, BaseRepository::class);
        $this->app->bind(CityRepositoryInterface::class, CityRepository::class);
        $this->app->bind(PermissionRepositoryInterface::class, PermissionRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(CharityRepositoryInterface::class, CharityRepository::class);
        $this->app->bind(ProjectRepositoryInterface::class, ProjectRepository::class);
        $this->app->bind(ServiceOptionRepositoryInterface::class, ServiceOptionRepository::class);
        $this->app->bind(JobCategoryRepositoryInterface::class, JobCategoryRepository::class);
        $this->app->bind(JobRepositoryInterface::class, JobRepository::class);
        $this->app->bind(WorkFieldRepositoryInterface::class, WorkFieldRepository::class);
        $this->app->bind(CharityCategoryRepositoryInterface::class, CharityCategoryRepository::class);
        $this->app->bind(PhaseRepositoryInterface::class, PhaseRepository::class);
        $this->app->bind(ContactRepositoryInterface::class, ContactRepository::class);
        $this->app->bind(StaticPageRepositoryInterface::class, StaticPageRepository::class);
        $this->app->bind(SliderRepositoryInterface::class, SliderRepository::class);
        $this->app->bind(CurrencyRepositoryInterface::class, CurrencyRepository::class);
        $this->app->bind(ReportRepositoryInterface::class, ReportRepository::class);
        $this->app->bind(SettingRepositoryInterface::class, SettingRepository::class);
        $this->app->bind(ImageRepositoryInterface::class, ImageRepository::class);
        $this->app->bind(AgreementRepositoryInterface::class, AgreementRepository::class);
        $this->app->bind(ContractRepositoryInterface::class, ContractRepository::class);
        $this->app->bind(EditRequestRepositoryInterface::class, EditRequestRepository::class);
        $this->app->bind(SponserRepositoryInterface::class, SponserRepository::class);
        $this->app->bind(PartenerRepositoryInterface::class, PartenerRepository::class);
        $this->app->bind(MarketingRepositoryInterface::class, MarketingRepository::class);
        $this->app->bind(RatingCriteriaRepositoryInterface::class, RatingCriteriaRepository::class);
        $this->app->bind(DegreeRepositoryInterface::class, DegreeRepository::class);
        $this->app->bind(SupportRepositoryInterface::class, SupportRepository::class);
        $this->app->bind(RatingRepositoryInterface::class, RatingRepository::class);
        $this->app->bind(DonateRepositoryInterface::class, DonateRepository::class);
        $this->app->bind(ProjectUserRepositoryInterface::class, ProjectUserRepository::class);
        $this->app->bind(GiftRepositoryInterface::class, GiftRepository::class);
        $this->app->bind(CampaignGoalRepositoryInterface::class, CampaignGoalRepository::class);
        $this->app->bind(AmbassadorRepositoryInterface::class, AmbassadorRepository::class);
        $this->app->bind(CartRepositoryInterface::class, CartRepository::class);
        $this->app->bind(NotificationRepositoryInterface::class, NotificationRepository::class);
        $this->app->bind(BankRepositoryInterface::class, BankRepository::class);
        $this->app->bind(FaqRepositoryInterface::class, FaqRepository::class);
        $this->app->bind(FinancialRequestRepositoryInterface::class, FinancialRequestRepository::class);
        $this->app->bind(SubscribeRepositoryInterface::class, SubscribeRepository::class);
        $this->app->bind(FrozenSupportRepositoryInterface::class, FrozenSupportRepository::class);
        $this->app->bind(PaymentTransactionRepositoryInterface::class, PaymentTransactionRepository::class);
        $this->app->bind(NationalityRepositoryInterface::class, NationalityRepository::class);
        $this->app->bind(DeviceTokenRepositoryInterface::class, DeviceTokenRepository::class);
        $this->app->bind(LogRepositoryInterface::class, LogRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
