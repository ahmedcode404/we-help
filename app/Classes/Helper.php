<?php

use App\Jobs\SendMultiMail;
use App\Mail\SendMailMarkting;
// use Stevebauman\Location\Facades\Location;
use Adrianorosa\GeoLocation\GeoLocation;
use Illuminate\Support\Facades\Mail;
use App\Mail\WeHelp;
use App\Models\Notification;
use App\Servicies\Notify;
use Carbon\Carbon;
use AmrShawky\LaravelCurrency\Facade\Currency;
use App\Jobs\SendEmailGifts;
use App\Jobs\sendMailSubscribe;
use App\Mail\BondMail;
use Twilio\Rest\Client;

/*curr
|--------------------------------------------------------------------------
| Detect Active Routes Function
|--------------------------------------------------------------------------
|
| Compare given routes with current route and return output if they match.
| Very useful for navigation, marking if the link is active.
|
*/

function isActiveRoute($route, $output = "active"){
    if (\Route::currentRouteName() == $route) return $output;
}

function areActiveRoutes(Array $routes, $output = "active show-sub"){

    foreach ($routes as $route){
        if (\Route::currentRouteName() == $route) return $output;
    }
}

function areActiveMainRoutes(Array $routes, $output = "active"){

    foreach ($routes as $route){
        if (\Route::currentRouteName() == $route) return $output;
    }
}

/*
|--------------------------------------------------------------------------
| Detect Nation
|--------------------------------------------------------------------------
|
| Get country from client's ip and returns country id
|
*/


function getNationId(){

    // $details = GeoLocation::lookup(\Request::ip());

    // $position = $details->getCountryCode();

    $countryRepository = \App::make('App\Repositories\CityRepositoryInterface');

    $position = 'UK';
    
    if($position == 'SA'){

        return $countryRepository->getWhere([['code', 'SA']])->first()->id;

    }
    else{

        return $countryRepository->getWhere([['code', 'Uk']])->first()->id;

    }

}

function getAuthNationId(){

    // if(auth()->check()){

    //     return auth()->user()->nation_id;
    // }
    // else{

        return 2;
    // }
}


function getDonorTotalSupports(){

    if(auth()->check()){

        $donated_projects = auth()->user()->donates()->whereYear('created_at', '=', date('Y'))->get();

        $sum = 0;

        $currencyRepository = \App::make('App\Repositories\CurrencyRepositoryInterface');

        $currency = $currencyRepository->getWhere([['symbol', 'GBP']])->first();

        foreach($donated_projects as $donation){

            $sum += exchange($donation->cost, $donation->currency_id, $currency->id);
        }

        return $sum;
    }
    else{

        return 0;
    }
}


function checkPermissions(array $permissions){

    if(auth()->guard('web')->check()){

        // check for web guard
        return auth()->user()->hasAnyPermission($permissions);
    }
    else if(auth()->guard('charity')->check()){
        
        return auth()->guard('charity')->user()->hasAnyPermission($permissions);
    }

}

function getTotalSupports($projects){

    $sum = 0;

    foreach($projects as $project){

        $sub_sum = 0;

        // if(auth()->check()){

        //     $currency = auth()->user()->currency_id;
        // }
        // else{

            $currency = session('currency');

        // }
        
        foreach($project->supports as $support){

            $sub_sum += exchange($support->pivot->cost, $support->pivot->currency_id, $currency);
        }

        $sum += $sub_sum;

    }
    
    return $sum;
}

function getTotalSupportsForProject($project, $currency = null){

    $sub_sum = 0;
        
    if($currency == null){
        
        // if(auth()->check() && auth()->user()->currency_id != null){

        //     $currency = auth()->user()->currency_id;
        // }
        // else{
    
            $currency = session('currency');
    
        // }
    }

    foreach($project->donates as $support){
        
        $sub_sum += exchange($support->pivot->cost , $support->pivot->currency_id, $currency);

    }

    return $sub_sum;

}

function getTotalSupportsProject($project , $currency){

    $sub_sum = 0;
        
    foreach($project->donates as $support){
        
        $sub_sum += exchange($support->pivot->cost, $support->pivot->currency_id , $currency);
        
    }

    return $sub_sum;

}

// function getTotalSupportsProjectNotCart($project , $currency){

//     $sub_sum = 0;
    
//     foreach($project->donates as $support){

//         $sub_sum += exchange($support->cost, $support->currency_id , $currency);
        
//     }

//     return $sub_sum;

// }

function getCartTotalCost($carts){

    $sum = 0;

    $session_currency = currencySymbol(session('currency'));

    foreach($carts as $cart){

        $sum += generalExchange($cart->cost, $cart->currency->symbol, $session_currency);
    }

    return $sum;

}

function generalExchange($cost, $from, $to){

    return round(Currency::convert()->from($from)->to($to)->amount($cost)->get(), 2);
}

// exchange currency
function exchange($cost, $from_crrency_id, $to_currency_id){

    $currencyRepository = \App::make('App\Repositories\CurrencyRepositoryInterface');

    // dd($from_crrency_id  , $to_currency_id);
    $from = currencySymbol($from_crrency_id);
    $to = currencySymbol($to_currency_id);

    if($cost == null){

        $cost = 0;
    }


    return round(Currency::convert()->from($from)->to($to)->amount($cost)->get(), 2);

}

// remove special charachters from text inputs and textareas
function clearText($value){

    $new_value = htmlspecialchars($value);
    $new_value = strip_tags($value);

    return $new_value;
}


function getEndDate($start_date, $duration){

    $date = new Carbon($start_date);

    return $date->addDays($duration)->format('Y-m-d');
}

// get currency symbol
function currencySymbol($crrency_id){

    $currencyRepository = \App::make('App\Repositories\CurrencyRepositoryInterface');

    $currency = $currencyRepository->findOne($crrency_id);

    return $currency->symbol;
}

// Send emails
function sendMail($users, $topic, $message, $nation_id, $notes = null, $extra_data = null){
    
    Mail::to($users)->send(new WeHelp($topic, $message, $nation_id, $notes, $extra_data));
}

function getSettingValue($key, $nation_id){

    $nation_id = 2;

    $settingRepository = \App::make('App\Repositories\SettingRepositoryInterface');

    // SA
    if($nation_id == 1){

        return $settingRepository->getWhere([['key', $key.'_sa'], ['nation_id', $nation_id]])->first();
    }
    // UK
    else{

        return $settingRepository->getWhere([['key', $key.'_uk'], ['nation_id', $nation_id]])->first();
    }
}

function getSettingMsg($key, $nation_id){

    $nation_id = 2;

    $settingRepository = \App::make('App\Repositories\SettingRepositoryInterface');

    // SA
    if($nation_id == 1){
        
        return $settingRepository->getWhere([['key', $key.'_sa_'.App::getLocale()], ['nation_id', $nation_id]])->first();
    }
    // UK
    else {

        return $settingRepository->getWhere([['key', $key.'_uk_'.App::getLocale()], ['nation_id', $nation_id]])->first();
    }
}

function getAgreement($key, $nation_id){

    $nation_id = 2;
    
    $agreementRepository = \App::make('App\Repositories\AgreementRepositoryInterface');

    // SA
    if($nation_id == 1){
        
        return $agreementRepository->getWhere([['key', $key.'_sa_'.App::getLocale()], ['nation_id', $nation_id]])->first();
    }
    // UK
    else{

        return $agreementRepository->getWhere([['key', $key.'_uk_'.App::getLocale()], ['nation_id', $nation_id]])->first();
    }
}

function getOrder($project){

    if(count($project->phases) == 0){

        return 1;
    }

    return $project->phases->last()->order + 1;

}

// GET SUPPORTER FOR SEND TO NOTIFICATION EMAIL OR SMS OR MOBILE
function getSupporters($intended_users , $nation_id){

    $userRepository = \App::make('App\Repositories\UserRepositoryInterface');

    if($intended_users == 'all'){

        $users = $userRepository->whereHas('roles', [['name', 'supporter']]);

    }
    else if($intended_users == 'most_donated'){

        $users = $userRepository->donationCount($nation_id , 'max');

    }
    else if($intended_users == 'least_donated'){

        $users = $userRepository->donationCount($nation_id, 'min');
    }
    else if($intended_users = 'never_donated'){

        $users = $userRepository->donationCount($nation_id, 'never');
    }

    return $users;
} // end of get supporter

// SEND MAIL MULTI USERS
function sendMailMatkting($users, $title, $content, $link , $nation_id){

    $data = [

        'title_ar'   => $title,
        'title_en'   => $title,
        'content_ar' => $content,
        'content_en' => $content,
        'link'       => $link,
        'nation_id'  => $nation_id,
        'users'      => $users,

    ];

    $job = (new SendMultiMail($data))->delay(now()->addSeconds(2));

    dispatch($job); 

    return 'Emails Sent';

} // end of send mail multi users

// // SEND MAIL MULTI USERS
function sendMailGifts($emails, $name_project , $name_image , $cost_project , $currency, $name_donator , $link , $nation_id){

    $data = [

        'emails'       => $emails,
        'name_project' => $name_project,
        'name_image'   => $name_image,
        'cost_project' => $cost_project,
        'currency'     => $currency,
        'name_donater' => $name_donator,
        'link'         => $link,
        'nation_id'    => $nation_id,

    ];

    $job = (new SendEmailGifts($data))->delay(now()->addSeconds(2));

    dispatch($job); 

} // end of send mail multi users

function sendNotifyMarkting($users, $title_ar , $title_en, $content_ar , $content_en, $link , $nation_id)
{


    // notification 
    $message = new \stdClass();
    $message->message_ar = $title_ar;
    $message->message_en = $title_en;
    foreach($users as  $user)
    {
        
        Notification::create([ 
    
            'message_en'     => $message->message_en,
            'message_ar'     => $message->message_ar,
            'content_ar'     => $content_ar,
            'content_en'     => $content_en,
            'send_user_id'   => $user->id,
            'model_id'       => $link,
            'title'          => 'markting',
            'type'           => 'suporter'
        ]);

        Notify::NotifyWeb($message , 'markting' , 'suporter' , $user->id); 

    }


}

// SEND MESSAGE NOTIFICATION MARKTING SMS OE EMAIL OR MOBILE
function sendNotificationMarkting($markting)
{

    // Get intended_users of the campaign
    $users = getSupporters($markting->intended_users , $markting->nation_id);

    // attach users to campaign
    $markting->users()->attach($users);

    // SEND MESSAGE SMS
    if($markting->type == 'sms')
    {

        $route = '#';

        if($markting->aff == null){

            $route = route('web.projects.show', $markting->project_link);
        }
        else{
            $route = route('aff' , $markting->aff);
        }

        $message =  strip_tags( $markting->content ) . ' رابط المشروع ' . $route;

        $account_sid = env("TWILIO_ACCOUNT_SID");
        $auth_token = env("TWILIO_AUTH_TOKEN");
        $twilio_number = env("TWILIO_PHONE_NUMBER");

        $client = new Client($account_sid, $auth_token);

        foreach($users as $user)
        {

            // $client->messages->create('+201009160863', 
            //     [
            //     'from' => $twilio_number, 
            //     'body' => $message
            //     ]
            // );

            try{
                $client->messages->create($user->phone, 
                    [
                    'from' => $twilio_number, 
                    'body' => $message
                    ]
                ); // end of client  
            }
            catch(Throwable $e){
                
                return 'Sender ID is not supporterd for this country';
            }

                     

        } // end of foreach

        return 'SMSs Sent';

    } 
    // SEND MESSAGE EMAIL
    else if($markting->type == 'email')
    {

        // SEND MAIL MULTI
        return sendMailMatkting(

            $users, 
            $markting->title ,
            $markting->content ,
            $markting->aff ,
            $markting->nation_id

        );

    }

    // SEND MESSAGE MOBILE 
    else if($markting->type == 'mobile')
    {

        notifyMobile(null, $users->pluck('id')->toArray(), $markting->title, 'supporter', $markting->content, 
                    $markting->content, asset('dashboard/app-assets/images/avatars/avatar.png'));

        return 'Notifications Sent';


    }    


} // end of send notification markting

function getDuration($start_date, $end_date){

    // $start_date = new Carbon($start_date);
    // $end_date = new Carbon($end_date);

    // $daysForExtraCoding = $start_date->diffInDaysFiltered(function(Carbon $date) {
    //     if($date->isWeekend()){
    //         dump($date->format('Y-m-d'));
    //     }
    //     return !$date->isWeekend();
    // }, $end_date);

    // return $daysForExtraCoding;

}
    
function sumDayProject($project)
{

    $phaseRepository = \App::make('App\Repositories\PhaseRepositoryInterface');

    return $phaseRepository->getSum($project);
    
} // end of sum day project



function getSettingData($key){

    $settingRepository = \App::make('App\Repositories\SettingRepositoryInterface');

    $nation_id = getNationId();

    // SA
    if($nation_id == 1){
        
        return $settingRepository->getWhere([['key', $key.'_sa'], ['nation_id', 1]])->first()['value'];
    
    }
    // UK
    else if($nation_id == 2){

        return $settingRepository->getWhere([['key', $key.'_uk'], ['nation_id', 2]])->first()['value'];
    
    } else
    {

        return $settingRepository->getWhere([['key', $key.'_sa'], ['nation_id', 1]])->first()['value'];

    }
}

function getWebSettingMsg($key){

    $settingRepository = \App::make('App\Repositories\SettingRepositoryInterface');

    $nation_id = getNationId();

    // SA
    if($nation_id == 1){
        
        return $settingRepository->getWhere([['key', $key.'_sa_'.App::getLocale()], ['nation_id', $nation_id]])->first();
    }
    // UK
    else if($nation_id == 2){

        return $settingRepository->getWhere([['key', $key.'_uk_'.App::getLocale()], ['nation_id', $nation_id]])->first();
    }
    else{
        
        return $settingRepository->getWhere([['key', $key.'_sa_'.App::getLocale()], ['nation_id', 1]])->first();
    }
}

// UPDATE RATING  USER
function updateRatingUser($project_id , $rating_id = null , $type)
{
    
    $projectRepository = \App::make('App\Repositories\ProjectRepositoryInterface');
    
    $project = $projectRepository->findOne($project_id);

    if ($project->ratings->isNotEmpty()) {
        
        if ($type == 'grade') {
            
            $comment = $project->ratings()->where([['user_id' , auth()->user()->id] , ['id' , $rating_id]])->first();

            if($comment){

                return $comment->grade;
            }
            else{

                return null;
            }
            
        } else if($type == 'comment')
        {
            $comment = $project->ratings()->where('user_id' , auth()->user()->id)->orderBy('id', 'desc')->first();

            if($comment){

                return $comment->comment;
            }
            else{
                return null;
            }
            // return $project->ratings()->where('user_id' , auth()->user()->id)->latest()->first()['comment'];

        } else {

            return null;

        } // end of type

    } // end of if project ratings not empty

} // end of update rating user

// get cart items
function getCart($limit){

    $cartRepository = \App::make('App\Repositories\CartRepositoryInterface');

    // $limit == 'count' ? return count of items
    // $limit == 'limit' ? return part of cart items to be listed in header
    // #limit == 'all' ? retutn all cart items to be viewd in cart page

    if(auth()->check()){

        if($limit == 'count'){

            if(auth()->user()->hasRole('charity')){
                
                $carts = 0;
            }
            else{

                $carts = count(auth()->user()->carts);
            }

        }

        if($limit == 'limit'){

            $carts = $cartRepository->paginateWhereWith([['user_id', auth()->user()->id], ['status', 'cart']], ['project', 'project.charity', 'currency'], ['column' => 'id', 'dir' => 'DESC'], 3);

        }

        if($limit == 'all'){

            $carts = $cartRepository->paginateWhereWith([['user_id', auth()->user()->id], ['sttaus', 'cart']], ['project', 'project.charity', 'currency']);
        
        }
    }
    else if(session()->has('cart_no')){

        if($limit == 'count'){

            $carts = count($cartRepository->getWhere([['cart_no', session('cart_no')]]));
        }

        if($limit == 'limit'){
            
            $carts = $cartRepository->paginateWhereWith([['cart_no', session('cart_no')]], ['project', 'project.charity', 'currency'], ['column' => 'id', 'dir' => 'DESC'], 3);
        }

        if($limit == 'all'){

            $carts = $cartRepository->paginateWhereWith([['cart_no', session('cart_no')]], ['project', 'project.charity', 'currency']);
        }

        
    }
    else{

        if($limit == 'count'){

            $carts = 0;
        }
        else{

            $carts = [];
        }
    }

    return $carts;

}

function sendNotifiyAcceptBondExchange($financial)
{
    $nation_id = getNationId();

    // notification 
    $message = new \stdClass();
    $message->message_ar = getSettingMsg('msg_bond_accept' , $nation_id)->value;
    $message->message_en = getSettingMsg('msg_bond_accept' , $nation_id)->value;    

    Notification::create([ 

        'message_en'     => getSettingMsg('msg_bond_accept' , $nation_id)->value,
        'message_ar'     => getSettingMsg('msg_bond_accept' , $nation_id)->value,
        'send_user_id'   => $financial->requestable_id,
        'model_id'       => $financial->project_id,
        'title'          => 'acceptbondexchange',
        'type'           => 'charity'

    ]);


    $send_user_id = [];

    array_push($send_user_id, $financial->requestable_id);

    notifyMobile($financial->project_id, $send_user_id, 'acceptbondexchange', 'charity', 
                getSettingMsg('msg_bond_accept' , $nation_id)->value, getSettingMsg('msg_bond_accept' , $nation_id)->value, 
                asset('web/images/main/user_avatar.png'));

    Notify::NotifyWeb($message , 'acceptbondexchange' , 'charity' , $financial->requestable_id); 

    sendMailAcceptBondExchange($financial);


} // end of funcation send notifiy accept bond exchange 

function sendMailAcceptBondExchange($financial)
{

    $nation_id = getNationId();

    $data = [ 

        'message'   => getSettingMsg('msg_bond_accept' , $nation_id)->value,
        // 'link'      => \URL::to('/').'/charity/bond/exchange',
        'link'      => route('charity.bond.exchange'),
        'nation_id' => $nation_id

    ];

    Mail::to($financial->project->charity->email)->send(new BondMail($data));

} // end of funcation send mail accept bond exchange 

function sendMailSubscribe($message , $slug , $nation_id)
{

    $subscriberepository = \App::make('App\Repositories\SubscribeRepositoryInterface');
    
    $emails = $subscriberepository->getAll()->pluck('email-subscribe');

    $data = [ 

        'emails'    => $emails,
        'message'   => $message,
        // 'link'      => \URL::to('/').'/projects/' . $slug,
        'link'      => route('web.projects.show', $slug),
        'nation_id' => getNationId()

    ];

    $job = (new sendMailSubscribe($data))->delay(now()->addSeconds(2));

    dispatch($job);     

} // end of function send mail subscribe


function apiValidation($validator){

    // $errors = [];
    $errors_str = "";
    foreach ($validator->errors()->toArray() as $key => $error) {
        $data['message'] = $error[0];
        break;
        // array_push($errors, $error[0]);
        // $errors[$key] = $error[0];
        // $errors_str = $errors_str . $error[0] . "\n";
    }
    $data['status'] = false;
    // $data['message'] = $errors;
    return $data;
}


function notifyMobile($model_id = null, $send_user_id, $title, $user_type, $notify_ar, $notify_en, $image){

    $notificationRepository = \App::make('App\Repositories\NotificationRepositoryInterface');
    $userRepository = \App::make('App\Repositories\UserRepositoryInterface');

    foreach($send_user_id as $user_id){

        $user = $userRepository->findOne($user_id);
        if($user->is_notify == 1){

            $data['title'] = $title;
            $data['message_ar'] = $notify_ar;
            $data['message_en'] = $notify_en;
            $data['send_user_id'] = $user_id;
            $data['type'] = $user_type;
            $data['model_id'] = $model_id;
            $data['image'] = $image;

            $notificationRepository->create($data);
        }
    }
    
    Notify::NotifyMob($title, $notify_ar, $notify_en, $send_user_id, $user_type, $model_id, $image); 
    
    // Notify::NotifyMob($data['title'], $data['message_ar'], $data['message_en'], $data['send_user_id'], $data['type'], $$data['model_id'], $data['image']); 
}


// Recurring Payment Setup
/**
 * Inputs:
 * $support -> the support operation object (contains: user + project + cost)
*/
// function recurringPayment($support){

//     // initiat strip connection
//     $stripe = new \Stripe\StripeClient(
//         'sk_test_51JCQI8IoY6AGxvlCOndhF0syV5KFm3XvmxaFzj42VeCE3qFRXnbNauCRwmDeIrmqw1UoekBkUG6OaTigG85kerQo00YPszLvBc'
//       );

//     // create product
//     $product = $stripe->products->create([
//         'name' => $support->project->name,
//       ]);

//     // create plan
//     $plan = $stripe->plans->create([
//         'amount' => $support->cost * 100,
//         'currency' => currencySymbol($support->currency_id),
//         'interval' => 'month',
//         'product' => $product->id,
//       ]);

//     // create customer  
//     $customer = $stripe->customers->create([
//         'description' => 'Project Monthly Deduction',
//         'name' => $support->user->name,
//         'email' => $support->user->email
//       ]);

//     $source = $stripe->customers->createSource(
//         $customer->id,
//         ['source' => 'tok_visa']
//       );

//       // create payment method
//     // $payment_method = $stripe->paymentMethods->create([
//     //     'type' => 'card',
//     //     'card' => [
//     //       'number' => $card->number,
//     //       'exp_month' => $card->exp_month,
//     //       'exp_year' => $card->exp_year,
//     //       'cvc' => $card->cvc,
//     //     ],
//     //   ]);

//     // // attach payment method to customer  
//     // $stripe->paymentMethods->attach(
//     //     $payment_method->id,
//     //     ['customer' => $customer->id]
//     //   );

//     // create subscription  
//     $subscription = $stripe->subscriptions->create([
//         'customer' => $customer->id,
//         'items' => [
//           ['price' => $plan->id],
//         ],
//       ]);

//     // create session and performe payment  
//     $session = $stripe->checkout->sessions->create([
//         'success_url' => route('payment.success', $support->id),
//         'cancel_url' => route('payment.cancle', $support->id),
//         'payment_method_types' => ['card'],
//         'line_items' => [
//           [
//             'price' => $plan->id,
//             'quantity' => 1,
//           ],
//         ],
//         'mode' => 'subscription',
//       ]);

//     $monthlyDeductionRepository = \App::make('App\Repositories\MonthlyDeductionRepositoryInterface');

//     $data['product_id'] = $product->id;
//     $data['plan_id'] = $plan->id;
//     $data['customer_id'] = $customer->id;
//     $data['customer_source'] = $source->id;
//     // $data['payment_method_id'] = $payment_method->id;
//     $data['subscription_id'] = $subscription->id;
//     $data['session_id'] = $session->id;

//     $monthlyDeductionRepository->create($data);

//     return true;
// }