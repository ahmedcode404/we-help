//e3tmad form
var $form2 = $(".e3tmad_form");

var lang = $('#lang').val();

if(lang == 'ar'){

    var success_msg = 'جارى المراجعة من قبل الإدارة';
    var sig_required = 'التوقيع مطلوب';
}
else{

    var success_msg = 'Under review by Adminstration';
    var sig_required = 'Signature is required';
}

$form2.validate({

    errorPlacement: function errorPlacement(error, element) {
        element.before(error)
    },

    rules: {
        e3tmad_input: {
            required: true
        },
        file_input: {
            required: "#check_1:checked"
        },

    },
    messages: {

    },
    submitHandler: function (form) {
        if ($("#check_1").is(":checked")) {
            Swal.fire({
                icon: 'success',
                title: success_msg,
                showConfirmButton: false,
                timer: 1500
            });
            setTimeout(function () {
                form.submit();
            }, 2000);
        }


        if (($("#check_2").is(":checked")) && (signaturePad.isEmpty())) {
            $("#check-2").find("#sig-error").text(sig_required).show();
        } else {
            $('#sig-error').remove();
            var canvas = $("#signature-pad");
            var dataUrl = canvas[0].toDataURL("image/png");
            
            $(".canvas_convert").val(dataUrl);
            Swal.fire({
                icon: 'success',
                title: success_msg,
                showConfirmButton: false,
                timer: 1500
            });
            setTimeout(function () {
                form.submit();
            }, 2000);
        }
    }
});

//file label name
$(".custom-file-div input").change(function () {
    var filenames = [];
    for (var i = 0; i < this.files.length; i++) {
        // filenames +=  this.files[i].name + ',';
        filenames.push(this.files[i].name);
    }
    $(".file-name").text(filenames);
});

//slide check
$(".custom-checkbox input").click(function () {
    var check = $(this).next("label").attr('datacheck');
    $(".sign-as .hide-div").slideUp("fast");
    $('#' + check).find(".hide-div").slideDown("slow");
});


$("#print-pg").click(function(){
    print();
})