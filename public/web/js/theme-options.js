//colors
$(function () {
    $(".custom-colors").click(function () {
        $(".theme_config").toggleClass("active");
    })


        var $winc = $(window); // or $box parent container
        var $boxc = $(".theme_config,.custom-colors");
        $winc.on("click.Bst", function (event) {
            if (
                $boxc.has(event.target).length === 0 && //checks if descendants of $box was clicked
                !$boxc.is(event.target) //checks if the $box itself was clicked
            ) {
                $(".theme_config").removeClass("active")
            }
        });

        $(".close-theme-config").click(function () {
            $(".theme_config").removeClass("active");
        })
    


    //header
    $(".header-option .color-boxes span").click(function () {
        var theme_color = $(this).attr("data-theme-color");
        $(".inner-header").css({
            "background-color": theme_color,
            "transition": "background-color .5s ease"
        })
        if (theme_color == "#fff") {
            $(".inner-header").css({
                "box-shadow": "0 0 8px #333"
            });
        } else {
            $(".inner-header").css({
                "box-shadow": "none"
            })
        }
    });

    $(".header-option .position-boxes span").click(function () {
        var theme_position = $(this).attr("data-theme-position");
        $("header").css("position", theme_position)
    });


    //footer
    $(".footer-option .color-boxes span").click(function () {
        var theme_color = $(this).attr("data-theme-color");
        $("footer").css({
            "background-color": theme_color,
            "transition": "background-color .5s ease"
        })
    });


    //body
    $(".body-option .color-boxes span").click(function () {
        var theme_color = $(this).attr("data-theme-color");
        $("body").css({
            "background-color": theme_color,
            "transition": "background-color .5s ease"
        })
    });

    //rest
    $(".reset-color").click(function () {
        $("body,footer,header,.inner-header").removeAttr('style');
    });



});