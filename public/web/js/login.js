$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });


});



//donate-form validation 
var $form = $(".login-form");
$form.validate({
    errorPlacement: function errorPlacement(error, element) {
        if ((element.attr("name") == "code[]")) {
            error.insertAfter(".verfication-phone");
        } else if (element.hasClass("select-input")) {
            error.insertAfter(element.next());
        } else {
            element.after(error)
        }
    },

    rules: {
        password_confirm : {
            equalTo : "#password"
        },
    },

    messages: {},
    submitHandler: function () {
        $form[0].submit();
        // swal.fire({
        //     title: 'success',
        //     icon: 'success',
        //     showConfirmButton: false,
        //     timer: 1500
        // });
        // setTimeout(function () {
        //     $form[0].submit();
        // }, 3000)
    }
});

$(".numb-input").each(function () {
    $(this).rules('add', {
        required: true,
        digits: true
    });
});

//verify phone
$(function() {
    'use strict';
  
    var body = $('body');
  
    function goToNextInput(e) {
      var key = e.which,
        t = $(e.target),
        sib = t.next('.numb-input');
  
      if (key != 9 && (key < 48 || key > 57) && (key < 96 || key > 105)) {
        e.preventDefault();
        return false;
      }
  
      if (key === 9) {
        return true;
      }
  
      if (!sib || !sib.length) {
        sib = body.find('.numb-input').eq(0);
      }
      sib.select().focus();
    }
  
    function onKeyDown(e) {
      var key = e.which;
  
      if (key === 9 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
        return true;
      }
  
      e.preventDefault();
      return false;
    }
    
    function onFocus(e) {
      $(e.target).select();
    }
  
    body.on('keyup', '.numb-input', goToNextInput);
    body.on('keydown', '.numb-input', onKeyDown);
    body.on('click', '.numb-input', onFocus);
  
  })