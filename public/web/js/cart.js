//counter
$('.plus-num').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$('.minus-num').click(function () {
    if ($(this).next().val() > 100) {
        $(this).next().val(+$(this).next().val() - 1);
    }
});

$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });

});

$('.select-input2').select2({
    theme: 'bootstrap4',
    language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',
    dropdownCssClass: "select-width"
});



var lang = $('.language').attr('lang');

if(lang == 'ar'){

    var another_owner = 'إضافة مالك';
    var delete_gift = 'حذف';
    var name = 'الإسم';
    var mobile_no = 'رقم الجوال';
    var email = 'البريد الإلكترونى';
}
else{

    var another_owner = 'Another Owner';
    var delete_gift = 'Delete';
    var name = 'Name';
    var mobile_no = 'Mobile No.';
    var email = 'Email';
}


//donate-form 
$(".repeate-btn").click(function () {
    var input_val = $(".main-repeate input").val();
    if (input_val.length === 0) {
        $(".main-repeate input").valid()
    } else {
        var append_content = `
        <div class="repeated-div">
        <h3 class="first_color text-cap">`+another_owner+`<i class="fa fa-times-circle first_color remove-chart tooltip-link second_color_hover" data-tippy-placement="top" title="حذف "></i></h3>

        <div class="form-group">
            <input type="text" class="form-control" />
            <label class="moving-label">`+name+`</label>
        </div>
    
        <div class="form-group">
            <input type="tel" class="form-control" minlength="9" maxlength="14" />
            <label class="moving-label">`+mobile_no+`.</label>
        </div>
    
        <div class="form-group">
            <input type="email" class="form-control"/>
            <label class="moving-label">`+email+`</label>
        </div>
    </div>
        `
        $(".repeat-div").append(append_content);
    }
});


$(document).on("click",".remove-chart",function(){
    $(this).closest(".repeated-div").remove();
});


//copy
$(".share-url-div .custom-btn").click(function () {
    $(this).prev(".form-control").select();
    document.execCommand("copy");
});



$(".ambassador-modal .custom-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
    if (form_near.valid()) {
        $(".share-url-div").slideDown();
    } else {
        $(".share-url-div").slideUp();

    }
})



// $('.cart-btn').on('click', function (e) {
//     var val_option = $(this).closest("tr").find(".pop-select").val();
//     console.log(val_option);
//     $(".select-modal").modal("hide");
//     if (val_option == "ambassador") {
//         $(".ambassador-modal").modal("show");
//     }
//         if (val_option == "once") {
//             $(".once-modal").modal("show");
//         } else if (val_option == "gift") {
//             $(".gift-modal").modal("show");
//         } else if (val_option == "month") {
//             $(".month-modal").modal("show");
//         } else{

//         }

// })




// var $cart_form = $(".cart-form");
// $cart_form.validate({
//     errorPlacement: function errorPlacement(error, element) {
//         if (element.hasClass("number-input")) {
//         } else {
//             element.before(error)
//         }
//     },

//     rules: {
      
//     },

//     messages: {},
//     submitHandler: function () {
//         swal.fire({
//             title: 'success ... continue to checkout',
//             icon: 'success',
//             showConfirmButton: false,
//             timer: 1500
//         });
//         setTimeout(function () {
//             $cart_form[0].submit();
//         }, 3000)
//     }
// });


$select_form = $(".select-modal form");
$select_form.each(function () {
    $(this).validate({
    errorPlacement: function errorPlacement(error, element) {
        if (element.attr("type") == "checkbox") {
            element.before(error)
        }
        else if ((element.attr("name") == "currency") || (element.attr("name") == "donate_price")) {
            error.insertBefore(element.closest(".donate-quantity"));
        } else if (element.hasClass("select-input") || element.hasClass("select-input2")) {
            error.insertAfter(element.next());
        } else if ((element.attr("name") == "donate_time")) {
            error.insertAfter(element.closest(".multi-checkboxes"));
        }
         else {
            element.before(error)
        }
    },

    rules: {
    
    },

    messages: {},
    submitHandler: function () {
        $(".modal").modal("hide")
    }
});
});


