$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });


});


var tel_url = $('#tel_url').val();
var phone_number = window.intlTelInput(document.querySelector("#phone"), {
    preferredCountries: ["gb", "sa"],
    separateDialCode: true,
    hiddenInput: "phone",
    formatOnDisplay: false,
    utilsScript: tel_url
  });



//donate-form validation 
var $form = $(".login-form");
$form.validate({
    focusInvalid: false,
    invalidHandler: function(form, validator) {
        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 1000);
    },
    errorPlacement: function errorPlacement(error, element) {
        if (element.hasClass("select-input") || element.hasClass("select-input2")) {
            error.insertAfter(element.next());
        } else if ((element.attr("type") == "checkbox")) {
            element.before(error)
        }else {
            element.after(error)
        }
    },

    rules: {
        password_confirm : {
            equalTo : "#password"
        },
        phone_num:{
            digits: true,
        }
    },

    messages: {},
    submitHandler: function () {
        // $form[0].submit();
        // swal.fire({
        //     title: 'success',
        //     icon: 'success',
        //     showConfirmButton: false,
        //     timer: 1500
        // });
        // setTimeout(function () {
        //     $form[0].submit();
        // }, 3000)
    }
});



$(document).on('click', '.submit_button', function () {
    var form_submit = $('.submit_form');
    form_submit.valid();
    if(form_submit.valid()){

        $(document).find('.error').html("");
        $(document).find('.error').hide();
        
        var formData = new FormData(form_submit[0]),
        action = form_submit.attr('action'),
        method = form_submit.attr('method');
        
        var phone = phone_number.getNumber(intlTelInputUtils.numberFormat.E164);

        formData.append('phone', phone);

        // make the ajax request
        $.ajax({
            url: action,
            type: method,
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {

                if(response.status == 1){

                    form_submit.submit();
                    
                }
                else if(response.status == 2){

                    $(document).find('.error-phone').show();
                    $(document).find('.error-phone').html(response.message);

                }
                else if(response.status == 0){

                    Swal.fire({
                        title: "حدث خطأ ما",
                        type: 'error',
                        timer: 2000,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                }
    
                // check for redirect
                if(response.redirect){

                    setTimeout(function (){

                        window.location.href = response.redirect;
                        
                    });
                }
    
                // check for reload
                if(response.reload && response.reload == 1){

                    setTimeout(function (){

                        location.reload();

                    }, 2000);
                }

            },
            error: function (errors) {
                for (var k in errors.responseJSON.errors) {
                    $(document).find('.error-'+k).show();
                    $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                }
            }
        });
    }


});

