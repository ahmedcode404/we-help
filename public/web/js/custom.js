//AOS
AOS.init();

//loading
$(window).on("load", function () {
    $('.bg_spinner').hide();     
    $("body").removeClass("no-trans");
    $(".loading").addClass("active");
    setTimeout(() => {
        $(".loading").removeClass("active");
    }, 2000);

    setTimeout(() => {
        $(".loading").fadeOut("slow")
    }, 2500);
});

var lang = $('.language').attr('lang');

if (lang == 'ar') {

    var not_donate_charity = '! لا يمكنك التبرع كجمعيه';
    var no_charity_project = '! لا يمكنك التبرع لهذا المشروع لانه معلق حاليا';
    var diffrent_nation_id = 'غير مسموح بالتبرع لمشروع خارج الدوله ';
    var success_subscribe = 'تم الاشتراك بنجاح';
    var user_blocked = 'هذا الحساب محظور';
    var user_donation_full = 'لقد بلغت الحد الأقصى للتبرع هذا العام';
} else 
{

    var not_donate_charity = 'Can Not Donate of Charity !';
    var no_charity_project = 'Can Not Donate of Project Put of Holding Now !';
    var diffrent_nation_id = 'It is not allowed to donate to a project outside the country';
    var success_subscribe = 'Done Subscribe Successfully';
    var user_blocked = 'This account is blocked';
    var user_donation_full = 'You have reached your donation limit this year';

}



//header scroll
$(document).ready(function () {




//////////////////////////////////////////////////////////// BEGIN: share sohial media //////////////////

var pageTitle   = document.title; //HTML page title
var pageUrl     = $('.share-url').attr('data-href');

$('.share-url a').click(function(event){
    event.preventDefault();
    var shareName = $(this).attr('data-network'); //get the first class name of clicked element
    switch(shareName) //switch to different links based on different social name
    {
        case 'facebook':
            OpenShareUrl('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle));
            break;
        case 'twitter':
            OpenShareUrl('http://twitter.com/home?status=' + encodeURIComponent(pageTitle + ' ' + pageUrl));
            break;
        case 'email':
            OpenShareUrl('mailto:?subject=' + pageTitle + '&body=Found this useful link for you : ' + pageUrl);
            break;
        case 'whatsapp':
            OpenShareUrl('whatsapp://send?text=' + pageUrl);
            break;                
    }

});

function OpenShareUrl(openLink){

    //Parameters for the Popup window
    winWidth    = 650; 
    winHeight   = 450;
    winLeft     = ($(window).width()  - winWidth)  / 2,
    winTop      = ($(window).height() - winHeight) / 2,
    winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   + winLeft;
    window.open(openLink,'Share This Link',winOptions); //open Popup window to share website.
    return false;

}

//////////////////////////////////////////////////////////// END: share sohial media ////////////////////






   
    //progressbar
    var docHeight = $(document).height(),
        windowHeight = $(window).height(),
        scrollPercent;

    var updateProgressbar = function () {
        scrollPercent = $(window).scrollTop() / (docHeight - windowHeight) * 100;
        $('.scroll_progress div').width(scrollPercent + '%');
    }

    updateProgressbar();
    $(window).scroll(updateProgressbar);

    $(window).scroll(function () {
        if ($(this).scrollTop() >= 100) {
            $("header").addClass("active");
        } else {
            $("header").removeClass("active");
        }
    });

});

$(".nav-icon").click(function () {
    $(this).addClass('open');
    setTimeout(() => {
        $(".side-menu").fadeIn(10).addClass('active');
    }, 500);

});

$(".close-menu").click(function () {
    $(".side-menu").removeClass('active');
    setTimeout(() => {
        $(".nav-icon").removeClass('open');
        $(".side-menu").fadeOut(10);
    }, 500);
});

$(function () {
    var $winm = $(window); // or $box parent container
    var $boxm = $(".side-menu,.nav-icon,#modal-login");
    $winm.on("click.Bst", function (event) {
        if (
            $boxm.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxm.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".side-menu").removeClass('active');
            setTimeout(() => {
                $(".nav-icon").removeClass('open');
                $(".side-menu").fadeOut(10);
            }, 500);
        }
    });
});

//slide menu
$(document).on("click", ".slide-link", (function () {
    $(this).toggleClass("active");
    $(this).next(".slide-menu").slideToggle();
    $(this).closest(".header-icon").siblings().find(".slide-menu").slideUp("fast");
    $(this).closest(".header-icon").siblings().find(".slide-link").removeClass("active");

}));
// $(document).on("click", ".languages-choose .icon-img", (function () {
//     $(this).toggleClass("active");
//     $(".languages-choose").toggleClass("active");
//     $(".slide-menu").slideUp("fast");


// }));

$(function () {
    var $wins = $(window); // or $box parent container
    var $boxs = $(".header-icon");
    $wins.on("click.Bst", function (event) {
        if (
            $boxs.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxs.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".slide-menu").slideUp("fast")
            $(".slide-link,.languages-choose,.languages-choose .icon-img").removeClass("active")
        }
    });
});

$(".top-btn").click(function () {
    $("html, body").animate({
        scrollTop: 0
    }, 2000);
});



//search
$(document).on("click", ".responsive-search", (function () {
    $(".search-main-div").addClass("active");
}));

$(document).on("click", ".close-search", (function () {
    $(".search-main-div,.responsive-search").removeClass("active");
}));
$(function () {
    var $winss = $(window); // or $box parent container
    var $boxss = $(".header-search,.search-main-div");
    $winss.on("click.Bst", function (event) {
        if (
            $boxss.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxss.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".search-main-div").removeClass("active");
        }
    });
});
//tooltip
// tippy('.tooltip-link', {
//     arrow: true,
//     placement: 'bottom',

// });

//cart add
// $(function () {

//     var cart = $('.cart-count'),
//         addToCart = $('.add-to-cart');
//         cartCountCont = cart.find('.num'),
//         cartCount = parseInt(cartCountCont.text(), 10),
//         addToCart.on('click', function () {

//             // auth user or no
//             if(auth)
//             {            

//                 ///////////////////////////////// BEGIN: add item to cart ////////////////////////////////
//                 $.ajaxSetup({
//                     headers: {
//                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                     }
//                 });
    
//                 var el  = $(this),
//                 id  = el.attr('id'),
//                 url = el.attr('data-url');

//                 var form     = $('.donate-cost-error-'+id)
//                 project_id   = form.find('.project_id'),
//                 currency_id  = form.find('.currency_id'),
//                 number_input = form.find('.number-input');
//                 pop_select   = form.find('.pop-select');

//                 var formData = new FormData();

//                 formData.append('pop_select'  , pop_select.val());
//                 formData.append('project_id'  , project_id.val());
//                 formData.append('currency_id' , currency_id.val());
//                 formData.append('number_input', number_input.val());
            
//                 // send request ajax
//                 $.ajax({
    
//                     url: url,
//                     type: 'POST',
//                     data: formData,  
//                     dataType: 'json',  
//                     processData: false,
//                     contentType: false,                                 
//                     success: function(result){

//                         if(result.data == 1)
//                         {

//                             success(add_to_cart , 'success');

//                             $('.cart-count').each(function() {

//                                 var cartCountFind = parseInt($(this).find('.num').text());
//                                 var total_cart = cartCountFind + 1
//                                 var item = el.parents(".project-div").find(".project-img"),
//                                 img = item.find('img'),
//                                 cartTopOffset = cart.offset().top - item.offset().top,
//                                 cartLeftOffset = cart.offset().left - item.offset().left;                             
        
//                                 var flyingImg = $('<img class="b-flying-img">');
//                                 flyingImg.attr('src', img.acartCountContCountCont
//                                     left: cartLeftOffset,
//                                     width: 100,
//                                     height: 100,
//                                     // opacity: 0.1
//                                 }, 800, function () {
    
//                                     flyingImg.remove();
//                                     cartCountCont.text(total_cart);
//                                     cartCountCont.removeClass("animated bounce")
                    
//                                 });
                    
//                                 el.parents(".project-div").append(flyingImg);
//                                 setTimeout(() => {
//                                     cartCountCont.addClass("animated bounce")
//                                 }, 900);
    
//                             }) // end of each                            

//                         } else 
//                         {

//                             addProjectFromBefor(add_to_cart_from_befor , 'info');

//                         } // end of else result data == 1 or 0


//                     } // end of success
                    
//                 }); // end of ajax                 
//                 ///////////////////////////////// END: add item to cart ////////////////////////////////

//             } else  
//             {

//                 notAuth(not_auth , 'question');

//             } // end of else auth user on no

//         }); // end of click button

// }); // end of function

//search form validation 
var $form_search = $("#searchform");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form_search.validate({
    errorPlacement: function (e, i) {
        i.before(e)
    },
    rules: {},
    submitHandler: function () {
        $form_search[0].submit();
    }
});

//newsletter form validation 
var $form2 = $(".newsletter-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form2.validate({
    errorPlacement: function (e, i) {
        i.after(e)
    },
    rules: {},
    submitHandler: function () {

        success(success_subscribe , 'success');

        setTimeout(() => {

            $form2[0].submit(); 
            
        }, 1500);
    }
});


//select 
$('.select-input').select2({
    theme: 'bootstrap4',
    language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en'
});

function toEnglishNumber(strNum) {
    var ar = '٠١٢٣٤٥٦٧٨٩'.split('');
    var en = '0123456789'.split('');
    strNum = strNum.replace(/[٠١٢٣٤٥٦٧٨٩]/g, x => en[ar.indexOf(x)]);
    return strNum;
}

$("input").each(function(){
    var val = toEnglishNumber($(this).text())
    $(this).text(val)
})


$(document).on('keyup', 'input', function (e) {
    var val = toEnglishNumber($(this).val())
    $(this).val(val)
});
$('.change-langouage').change(function (e) {
    e.preventDefault();
    var url = $(this).attr('url');

    $.ajax({
        url: url,
        type: "GET",      
        data: {},
        dataType: 'json',
        success: function(){

            location.reload();

        }, // end of success
        
    }); // end of ajax      
})

$('.change-langouage').click(function (e) {
    e.preventDefault();
    var url = $(this).attr('url');

    $.ajax({
        url: url,
        type: "GET",      
        data: {},
        dataType: 'json',
        success: function(){

            location.reload();

        }, // end of success
        
    }); // end of ajax      
})

$('.auth-charity').click(function (e) {
   
    complateCost(not_donate_charity , 'info');

}) // end of auth cahrity

$('.no-charity-project').click(function () {

    complateCost(no_charity_project , 'info');
    
}) // end of no charity project

$('.user-bocked').click(function () {

    complateCost(user_blocked , 'info');
    
}) // end of no charity project


// user can not donate if hist total donations is 10000 GBP a year
$(document).on('click', '.user_donation_full', function(){

    complateCost(user_donation_full , 'info');

});

$('.diffrent-nation-id').click(function () {

    complateCost(diffrent_nation_id , 'info');
    
}) // end of no charity project

$('#filter-projects').change(function () {

    var url = $(this).attr('url');

    $.ajax({
        url: url,
        type: "GET",      
        data: {},
        dataType: 'json',
        success: function(){

            location.reload();

        }, // end of success
        
    }); // end of ajax  
    
}) // end of no charity project



