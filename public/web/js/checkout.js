
var $checkoutform = $(".checkout-form");
$checkoutform.validate({
    errorPlacement: function errorPlacement(error, element) {
            error.insertAfter(".circle-checkboxes-div");
    },

    rules: {
        checkout: {
            required: true
        }
    },

    messages: {},
    submitHandler: function () {
        
    }
});