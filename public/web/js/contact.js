
//phone
var tel_url = $('#tel_url').val();
var input_num = document.querySelector("#contact_number");
window.intlTelInput(input_num, {
    preferredCountries: ["gb", "sa"],
    separateDialCode: true,
    formatOnDisplay: false,
    hiddenInput: "phone",
    utilsScript: tel_url
});

$('.contact-form').on('submit' , function() {

    var country_code = $(this).find('.iti__selected-dial-code').text();

    $('input[name="phone_key"]').val(country_code);
  
});

//contact-form  validation 
var $form_contact = $(".contact-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form_contact.validate({
    errorPlacement: function (e, i) {
        i.before(e)
    },
    rules: {
        phone: {
            required: true,
            number: true,
            digits:true,
        },
        content: {
            required: true,
        },
    },
    submitHandler: function () {
    
        $form_contact[0].submit();

    }
});

