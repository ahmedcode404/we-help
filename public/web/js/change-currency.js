// change currency
$('.currency-default').click(function (e) {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

        // route get request
         // route get request
        var action = $(this).data('action');
        var id = $(this).data('id');

        // send request ajax
        $.ajax({

            url: action,
            type: 'GET',
            data: {'id' : id},              
            success: function(){

                location.reload();

            }, // end of success
            error: function (jqXHR) {
            } //  end of error
            
        }); // end of ajax    

}) // end of change currency
