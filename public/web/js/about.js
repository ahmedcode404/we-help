var mydir = $("html").attr("dir");
if (mydir == 'rtl') {rtlVal=true}else{rtlVal=false}

//owl-carousel
$("#about-owl").owlCarousel({
    loop: true,
    items: 1,
    nav: false,
    dots: true,
    autoplay: true,
    mouseDrag: true,
    touchDrag: true,
    rtl:rtlVal,
    autoplayTimeout: 10000,
});



$(window).on("load", function () {
    var textWrappers = document.querySelector('.marquee-div div');
    textWrappers.innerHTML = textWrappers.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
    anime.timeline({
        loop: true
      })
      .add({
        targets: '.marquee-div  .letter',
        opacity: [0, 1],
        easing: "easeInOutQuad",
        duration: 1000,
        delay: (el, i) => 20 * (i + 1)
      }).add({
        targets: '.marquee-div div',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
      });
  
  
  });
  