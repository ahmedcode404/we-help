$(document).ready(function(){

    var lang = $('#lang').val();
    if(lang == 'ar'){

        var success_add = ' تم الإشتراك بنجاح!';
        var failed = 'حدث خطأ ما';
        var text = 'الإشتراك فى النشرة البريدية';
    }
    else{
        var success_add = 'Subscribed successfully!';
        var failed = 'An error has occured';
        var text = 'subscribe to newletter';
    }

    // submit form
    $(document).on('click', '.submit_request_form_button', function(){

        var self = $(this).parent().prev('.general_error');
        var form_submit = $('.request_form');
        form_submit.valid();

        if(form_submit.valid()){

            $(document).find('.error').html("");
            $(document).find('.error').hide();
            var edit = $(document).find('#edit').val();

            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');

            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#email').val(null);
                    if(response.status == 1){

                        general_alert(success_add , '', 'success');
                    }
                    else{

                        general_alert(failed , '', 'error');
                    }

                },
                error: function (errors) {

                    $('#email').val(null);
                    general_alert(failed , '', 'error');
                }
            });
        }
        
    });

});