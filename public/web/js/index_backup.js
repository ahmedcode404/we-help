var mydir = $("html").attr("dir");
if (mydir == 'rtl') {
    rtlVal = true
} else {
    rtlVal = false
}

//owl-carousel
$("#main-owl").owlCarousel({
    loop: true,
    items: 1,
    nav: false,
    dots: true,
    autoplay: true,
    mouseDrag: true,
    touchDrag: true,
    rtl: rtlVal,
    autoplayTimeout: 8000,
    animateOut: 'rotateOut',
    animateIn: 'fadeIn'
});

$("#partener-owl").owlCarousel({
    loop: true,
    margin: 15,
    items: 5,
    nav: false,
    dots: false,
    autoplay: true,
    mouseDrag: true,
    touchDrag: true,
    rtl: rtlVal,
    responsive: {
        0: {
            items: 2,

        },
        550: {
            items: 2,

        },
        800: {
            items: 3,
        },
        1200: {
            items: 4,
        },
        1600: {
            items: 5,
        }
    }

});



$(".projects-owl").owlCarousel({
    loop: false,
    margin: 15,
    items: 5,
    nav: false,
    dots: false,
    autoplay: false,
    mouseDrag: true,
    touchDrag: true,
    rtl: rtlVal,
    responsive: {
        0: {
            items: 1,
        },
        515: {
            stagePadding: 40,
            items: 1,
        },
        768: {
            stagePadding: 120,
            items: 1,
        },

        992: {
            items: 2,
        },
        1200: {
            stagePadding: 70,
            items: 2,
        },
        1400: {
            items: 3,
        }
    }

});

//select 

$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });


});

$('.select-input2').select2({
    theme: 'bootstrap4',
    language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',
    dropdownCssClass: "select-width"
});

$('.pop-select').on('select2:select', function (e) {
    $(".select-modal").modal("hide");
    var val_option = $(this).val();
    if (val_option == "once") {
        $(".once-modal").modal("show")
    } else if (val_option == "gift") {
        $(".gift-modal").modal("show")
    } else if (val_option == "month") {
        $(".month-modal").modal("show")
    } else {
        $(".ambassador-modal").modal("show")
    }
});

var lang = $('.language').attr('lang');

if(lang == 'ar'){

    var another_owner = 'إضافة مالك';
    var delete_gift = 'حذف';
    var name = 'الإسم';
    var mobile_no = 'رقم الجوال';
    var email = 'البريد الإلكترونى';
}
else{

    var another_owner = 'Another Owner';
    var delete_gift = 'Delete';
    var name = 'Name';
    var mobile_no = 'Mobile No.';
    var email = 'Email';
}


//donate-form 
$(".repeate-btn").click(function () {
    var input_val = $(".main-repeate input").val();
    if (input_val.length === 0) {
        $(".main-repeate input").valid()
    } else {
        var append_content = `
        <div class="repeated-div">
        <h3 class="first_color text-cap">`+another_owner+`<i class="fa fa-times-circle first_color remove-chart tooltip-link second_color_hover" data-tippy-placement="top" title="حذف "></i></h3>

        <div class="form-group">
            <input type="text" class="form-control" />
            <label class="moving-label">`+name+`</label>
        </div>
    
        <div class="form-group">
            <input type="tel" class="form-control" minlength="9" maxlength="14" />
            <label class="moving-label">`+mobile_no+`.</label>
        </div>
    
        <div class="form-group">
            <input type="email" class="form-control"/>
            <label class="moving-label">`+email+`</label>
        </div>
    </div>
        `
        $(".repeat-div").append(append_content);
    }
});


$(document).on("click",".remove-chart",function(){
    $(this).closest(".repeated-div").remove();
});

//scroll sections

$(window).scroll(function () {
    if ($(this).scrollTop() > $('#sec_1').offset().top) {
        $(".fast-scroll").fadeIn();
    } else {
        $(".fast-scroll").fadeOut();
    }
});


$(".fast-scroll a").click(function (event) {
    event.preventDefault();
    $("html, body").animate({
        scrollTop: $($(this).attr("href")).offset().top - 100
    }, 1000);
});

$(window).scroll(function () {
    var scrollDistance = $(window).scrollTop();
    $('.slide_section').each(function (i) {
        headerheight = $("header").outerHeight();
        if (($(this).offset().top - headerheight) <= scrollDistance) {
            $('.fast-scroll  a.active').removeClass('active');
            $('.fast-scroll a').eq(i).addClass('active');
        }
    });
}).scroll();

$(window).on("load", function () {
    $(".progress-bar-ratio").each(function () {
    var data_progress = $(this).attr("data-progress") + "%";
    $(this).animate({
        'width': data_progress
    }, 500);
    });

    $(".nav-item").click(function () {
        var light_color = $(this).attr("data-light-color");
        var main_color = $(this).attr("data-main-color");
        $("body").get(0).style.setProperty("--light_color", light_color);
        $("body").get(0).style.setProperty("--main_color", main_color);
        $(".progress-bar-ratio").each(function () {
            var data_progress = $(this).attr("data-progress") + "%";
            setTimeout(() => {
                $(this).animate({
                    'width': data_progress
                }, 300);
            }, 300);

        });
    });

})

//resposive filter
$(function () {
    $(".filter-tabs-btn").click(function () {
        $(".filter-tabs").toggleClass("active");
    })


    var $winf = $(window); // or $box parent container
    var $boxf = $(".filter-tabs-btn,.filter-tabs");
    $winf.on("click.Bst", function (event) {
        if (
            $boxf.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxf.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".filter-tabs").removeClass("active")
        }
    });

    $(".close-tabs").click(function () {
        $(".filter-tabs").removeClass("active");
    })
});
//statisics
var a = 0;
$(window).scroll(function () {

    var oTop = $('#counter').offset().top - window.innerHeight;
    var stat_height = $(".statistics-sec").outerHeight() / 2;
    if (a == 0 && $(window).scrollTop() > (oTop + stat_height)) {
        $('.counter-value').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.find("span").text()
            }).animate({
                    countNum: countTo
                },

                {

                    duration: 4000,
                    easing: 'swing',
                    step: function () {
                        $this.find("span").text(Math.floor(this.countNum));
                    },
                    complete: function () {
                        $this.find("span").text(this.countNum);
                        //alert('finished');
                    }

                });
        });
        a = 1;
    }

});

//counter
$('.plus-num').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$('.minus-num').click(function () {
    if ($(this).next().val() > 100) {
        $(this).next().val(+$(this).next().val() - 1);
    }
});

//phone
$(function () {
    var input_num = document.querySelector("#contact_number");
    window.intlTelInput(input_num, {
        preferredCountries: ["gb", "sa"],
        separateDialCode: true,
        formatOnDisplay: false,
        utilsScript: "web/js/utils.js"
    });

});

//copy
$(".share-url-div .custom-btn").click(function () {
    $(this).prev(".form-control").select();
    document.execCommand("copy");
});

//donate-sm-form
$('.donate-sm-form').each(function () {
    $(this).validate({
        errorPlacement: function (error, element) {
            if (element.hasClass("number-input")) {
            } else {
                element.after(error)
            }
        },

        rules:{

        }
      
    });
});


$('.select-modal form').each(function () {
    $(this).validate({
        errorPlacement: function (error, element) {
            if (element.hasClass("select-input") || element.hasClass("select-input2")) {
                error.insertAfter(element.next());
            } else {
                element.before(error)
            }
        },
    });
});

$(".ambassador-modal .custom-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
    if (form_near.valid()) {
        $(".share-url-div").slideDown();
    } else {
        $(".share-url-div").slideUp();

    }
})



//contact-form  validation 
var $form_full = $(".contact-form");
$.validator.addMethod("letters", (function (e, i) {
    return this.optional(i) || e == e.match(/^[a-zA-Z\s]*$/)
})), $form_full.validate({
    errorPlacement: function (e, i) {
        i.before(e)
    },
    rules: {},
    submitHandler: function () {
        $form_full[0].submit()
    }
});