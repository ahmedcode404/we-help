$(window).on("load", function () {
    var textWrappers = document.querySelector('.pages-header-content h1');
    textWrappers.innerHTML = textWrappers.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
    anime.timeline({
        loop: true
    }).add({
        targets: '.line',
        opacity: [0.5, 1],
        scaleX: [0, 1],
        easing: "easeInOutQuad",
        duration: 800
    }).add({
        targets: '.pages-header-content .letter',
        opacity: [0, 1],
        easing: "easeInOutQuad",
        duration: 2250,
        delay: (el, i) => 150 * (i + 1)
    }).add({
        targets: '.pages-header-content h1',
        opacity: 0,
        duration: 1000,
        easing: "easeOutExpo",
        delay: 1000
    });
});


$(window).scroll(function () {
    var scrollDistance = $(window).scrollTop();
    headerheight = $(".pages-header").outerHeight()/2;
    if (headerheight <= scrollDistance) {
        $('body').addClass('active-title');
    } else {
        $('body').removeClass('active-title');
    }
});