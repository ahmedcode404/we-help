// // remove-alert
// $(document).on('click', '.remove-btn', function (e) {
// e.preventDefault();
//   Swal.fire({
//     title: 'Do you want to remove this item?',
//     icon: 'warning',
//     showCancelButton: true,
//     confirmButtonText: 'yes',
//     cancelButtonText: 'no',
//   }).then((result) => {
//     if (result.isConfirmed) {
//       Swal.fire({
//         icon: 'success',
//         title: 'Delete successfully',
//         showConfirmButton: false,
//         timer: 1000,
//       });
//       $(this).closest(".remove-item").remove();

//     }
//   })
// });



$(document).on('click', '.remove-all-btn', function (e) {
  e.preventDefault();
    Swal.fire({
      title: 'Do you want to delete all?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'yes',
      cancelButtonText: 'no',
    }).then((result) => {
      if (result.isConfirmed) {
        Swal.fire({
          icon: 'success',
          title: 'Delete successfully',
          showConfirmButton: false,
          timer: 1000,
        });
        $(".remove-all").remove();
      }
    })
  
  });

  function success(msg , icon) {

      Swal.fire({
        icon: icon,
        title: msg,
        showConfirmButton: false,
        timer: 2000,
      });
    
  }

  function error(msg , icon) {

    Swal.fire({
      icon: icon,
      title: msg,
      showConfirmButton: false,
      timer: 1500,
    });
  
}

  function notAuth(msg , icon) {

    Swal.fire({
      icon: icon,
      title: msg,
      showConfirmButton: true,
      timer: 2000,
    });
  
  }

  function addProjectFromBefor(msg , icon) {

    Swal.fire({
      icon: icon,
      title: msg,
      showConfirmButton: true,
      timer: 2000,
    });
  
  }  

  function complateCost(msg , icon) {

    Swal.fire({
      icon: icon,
      title: msg,
      showConfirmButton: true,
      timer: 2000,
    });
  
  }  



function general_alert(msg , html, icon) {

    Swal.fire({
      icon: icon,
      title: msg,
      html: html,
      showConfirmButton: false,
      timer: 2500,
    });
  
}