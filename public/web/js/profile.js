var lang = $('.language').attr('lang');

if (lang == 'ar') {

    var success = 'تم تحديث البيانات بنجاح';
    var success_file = 'تم تحديث صورة الحساب بنجاح';
    var error_donate = 'الملبع المتبرع به اكبر من المبلغ المطلوب';
    var success_donate_again = 'تم التبرع مره اخري بنحاج';
    var complate_cost = 'تم اكمال مبلغ التبرع للمشروع يمكنك التبرع لمشروع اخر ';
    var success_delete = 'تم حذف المشروع بنجاح';
    var do_delete = 'هل تريد حذف المشروع ؟';
    var no = 'لا';
    var yes = 'نعم';
    var do_donate = 'هل تريد التبرع مره اخري ؟';
    var success_donate   = 'تم التبرع بنجاح';
    var success_active = 'تم تفعيل الاشعارات بنجاح';
    var success_unactive = 'تم ايقاف الاشعارات بنجاح';
    var delete_account = 'تم حذف الحساب بنجاح';
    
} else 
{

    var success = 'Done Update Data Successfully';
    var success_file = 'Done Update Image Account Successfully';
    var error_donate = 'The amount donated is greater than the required amount';
    var success_donate_again = 'Donated once again Successfully';
    var complate_cost = 'The donation amount for the project has been completed. You can donate to another project';
    var success_delete = 'Done Delete Project Successfully';
    var do_delete = 'Do you want to delete project ?';
    var no = 'NO';
    var yes = 'Yes';  
    var do_donate = 'Do you want to donate again ?';
    var success_donate   = 'Done Donate Successfully';
    var success_active = 'Done Active Notification Successfully';
    var success_unactive = 'Done Stop Notification Successfully';
    var delete_account = 'Done Delete Account Successfully';

}


//profile-img
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
            $('#imagePreview').hide();
            $('#imagePreview').fadeIn(650);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$("#imageUpload").change(function () {
    readURL(this);

    var form_data = new FormData();                  
    var file_data = $(this)[0].files;

    form_data.append('image', file_data[0]);  
    var url = $(this).data('url');

    $.ajax({
        url: url,
        type: "POST",
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},        
        data: form_data,
        contentType: false,
        cache: false,
        processData:false,
        success: function(data){
            Swal.fire({
                icon: 'success',
                title: success_file,
                showConfirmButton: false,
                timer: 1000,
              });            
        }
    });

});

$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });

});

//slide profile
$(document).on("click", ".arrow-profile-text", (function () {
    $(this).toggleClass("active");
    $(this).next(".slide-profile").slideToggle();
}));

$(".ambassador-modal .amb-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
     if (form_near.valid()) {
        $(".check-donate-amb").show();
        $(".share-url-div").slideDown();
    } else {
        $(".share-url-div,.ambasador-donate").slideUp();

    }
})


//cart add
// $(function () {
//     var cart = $('.cart-count'),
//         addToCart = $('.add-to-cart2');
//     cartCountCont = cart.find('.num'),
//         cartCount = parseInt(cartCountCont.text(), 10),
//         addToCart.on('click', function () {
//             cartCount += 1;
//             cartCountCont.text(cartCount);
//             cartCountCont.removeClass("animated bounce")
//             setTimeout(() => {
//                 cartCountCont.addClass("animated bounce")
//             }, 900);

//         });
// });


//delete account 
$(document).on("click", ".delete-account", (function (e) {
    e.preventDefault();
    $("#remove-modal").modal("hide");

    var url = $(this).attr('url');
    var redirect = $(this).attr('redirect');

    $.ajax({
        url: url,
        type: "GET",      
        data: {},
        dataType: 'json',
        success: function(){

            Swal.fire({
                icon: 'success',
                title: delete_account,
                showConfirmButton: false,
                timer: 1000,
            }); 

            setTimeout(() => {

                window.location.replace(redirect);
                
            }, 1200);
    
        }, // end of success
        
    }); // end of ajax  

    // Swal.fire({
    //   title: 'Do you want to delete account?',
    //   icon: 'warning',
    //   showCancelButton: true,
    //   confirmButtonText: 'yes',
    //   cancelButtonText: 'no',
    // }).then((result) => {
    //   if (result.isConfirmed) {

    //     $.ajax({
    //         url: url,
    //         type: "GET",      
    //         data: {},
    //         dataType: 'json',
    //         success: function(){

    //             Swal.fire({
    //                 icon: 'success',
    //                 title: delete_account,
    //                 showConfirmButton: false,
    //                 timer: 1000,
    //             }); 

    //             setTimeout(() => {

    //                 window.location.replace(redirect);
                    
    //             }, 1200);
        
    //         }, // end of success
            
    //     }); // end of ajax  



    //   } // end of result is confirmed
    // });
}));

//counter
$('.plus-num').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$('.minus-num').click(function () {
    if ($(this).next().val() > 100) {
        $(this).next().val(+$(this).next().val() - 1);
    }
});
//donate-form validation 
var $prof_form = $(".login-form");
$prof_form.validate({
    errorPlacement: function errorPlacement(error, element) {
        if (element.hasClass("select-input")) {
            error.insertAfter(element.next());
        } else {
            element.after(error)
        }
    },

    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
            email: true,
        }, 
        country_id: {
            required: true,
        },                      
    },

    messages: {},
    submitHandler: function () {

        $prof_form[0].submit();
       

    }
});


var $donate_form = $(".donate-form");
$donate_form.validate({
    errorPlacement: function errorPlacement(error, element) {
         if ((element.attr("name") == "donate_price")) {
            error.insertBefore(element.closest(".donate-quantity"));
        }
         else {
            element.before(error)
        }
    },

    rules: {
        donate_time: {
            required: true
        }
    },

    messages: {},
    // submitHandler: function () {
    //     // swal.fire({
    //     //     title: 'Your donation has been edit',
    //     //     icon: 'success',
    //     //     showConfirmButton: false,
    //     //     timer: 1500
    //     // });
    //     // setTimeout(function () {
    //     //     $donate_form[0].submit();
    //     // }, 3000)
    // }
});

$('.update-donate-month').click(function(){


    $("#modal-month").modal("show");

    var project_id   = $('#project_id');
    var month_id     = $('#month_id');
    var number_input = $('#number-input');
    
    var is_this = $(this);

    project_id.val(is_this.data('project'));
    month_id.val(is_this.data('id'));

}) // end of update donate month
 
$(document).on( 'click' , '.update-month' , function(){
    var donate_form = $(".donate-form");
    donate_form.valid();

    if(donate_form.valid()){
        
        // show loading
        $('.bg_spinner').show(); 

        var project_id   = $('#project_id');
        var month_id     = $('#month_id');
        var switch_1     = $('#switch_1:checked');
        var number_input = $('#number-input');
        var currency_id  = $('#currency_id');
        
        var form_data = new FormData();                  
    
        
        if(switch_1.val() != undefined) 
        {
            
            form_data.append('switch_1', switch_1.val());  
            
        } // end of if switch_1 != undefined
        
        form_data.append('project_id', project_id.val());  
        form_data.append('month_id', month_id.val()); 
        form_data.append('number_input', number_input.val());  
        form_data.append('currency_id', currency_id.val());  
    
        var url = $(this).data('url');
    
        $.ajax({
            url: url,
            type: "POST",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},        
            data: form_data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(result){
                
                // hide loading
                $('.bg_spinner').hide(); 

                // Swal.fire({
                //     icon: 'success',
                //     title: success_file,
                //     showConfirmButton: false,
                //     timer: 1000,
                // }); 
                
                $("#modal-month").modal("hide");

                location.href = result.checkoute_route;

            }, // end of success
            error: function (jqXHR) {
    
                // show loading
                $('.bg_spinner').hide();                

                // error validation
                validatePrice(donate_form , number_input , jqXHR.responseJSON.errors.number_input);
    
            } //  end of error
        });   

    } // end of donate form valid
 
}) // end of update donate month

$(document).on( 'click' , '.donate-form .complate-cost' , function(){

    complateCost(complate_cost , 'info');

}); // end of complate

$('.donate-again').click(function(){

    var month_id     = $(this).data('id');

    var url = $(this).data('url');

    Swal.fire({
        title: do_donate,
        icon: 'question',
        showCancelButton: true,
        confirmButtonText: yes,
        cancelButtonText: no,
        }).then((result) => {

        if (result.isConfirmed) {

            // show loading
            $('.bg_spinner').show(); 

            $.ajax({
                url: url,
                type: "GET",      
                data: {'id' : month_id},
                dataType: 'json',
                success: function(result){

                    if (result.status == 0) {
                        
                        // hide loading
                        $('.bg_spinner').hide(); 
    
                        Swal.fire({
                            icon: 'info',
                            title: error_donate,
                            showConfirmButton: true,
                        }); 
    
                    } else 
                    {
    
                        // hide loading
                        $('.bg_spinner').hide(); 
    
                        // Swal.fire({
                        //     icon: 'success',
                        //     title: success_donate_again,
                        //     showConfirmButton: false,
                        //     timer: 1000,
                        // }); 
    
                        location.href = result.checkoute_route;
    
                    } // end of else result status == 0
    
                }, // end of success
                
            }); // end of ajax  

        } // end of result is confirmed

        }) // end of then
          
          

}) // end of update donate month

$('.donate-ambassador').click(function(){

    var peoject_id  = $(this).data('id');
    var url = $(this).data('url');

    if ($("#donate-ambsaador-form").valid()) 
    {


            // show loading
            $('.bg_spinner').show();  

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // get all id inputs
            var form                  = $('#form-donate-ambassador');
            var project_id_ambassador = form.find('#project_id_ambassador');

            var form_donate = $('#donate-ambsaador-form');
            var price_ambsaador    = form_donate.find('#price_ambsaador');
            var currency_ambsaador = form_donate.find('#currency_ambsaador');

            var type = $(this).attr('data-type');

            var url = $(this).attr('data-url');

            var formData = new FormData();

            formData.append('type'            , type);
            formData.append('project_id'      , project_id_ambassador.val());
            formData.append('price_ambsaador' , price_ambsaador.val());
            formData.append('currency_id'     , currency_ambsaador.val());

            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(result){

                    // hide loading
                    $('.bg_spinner').hide();  

                    // success(success_donate , 'success');

                    $(".ambassador-modal").modal("hide");                    

                    location.href = result.checkoute_route;
                    

                }, // end of success
                error: function (jqXHR) {

                    // hide loading
                    $('.bg_spinner').hide();  

                    var forrm_err = $('.ambasador-donate');
                    // error validation
                    validatePriceAmbsaador(forrm_err , price_ambsaador , jqXHR.responseJSON.errors.price_ambsaador);

                } //  end of error
                
            }); // end of ajax 


    } // end of if main-repeate input

}) // end of update donate month

$('.remove-project-month').click(function() {

    var id  = $(this).data('id');
    var url = $(this).data('url');


    Swal.fire({
        title: do_delete,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: yes,
        cancelButtonText: no,
        }).then((result) => {

        if (result.isConfirmed) {

            
            $.ajax({
                url: url,
                type: "GET",      
                data: {'id' : id},
                dataType: 'json',
                success: function(){

                    Swal.fire({
                        icon: 'success',
                        title: success_delete,
                        showConfirmButton: false,
                        timer: 1000,
                    }); 
                    
                    $('.remove-project-month').closest('.remove-item').remove();

                }, // end of success
                
            }); // end of ajax   

        } // end of result is confirmed

        });        

}); // end of remove button

// show modal and append data in modal
$('.donate-now').click(function() {
    $('#project').val($(this).data('id'));
});
// end of show modal and append data in modal

// start valid form donate month
var $donate_form = $(".donate-ambsaador-form");
$donate_form.validate({
    errorPlacement: function errorPlacement(error, element) {
         if ((element.attr("name") == "donate_price")) {
            error.insertBefore(element.closest(".donate-quantity"));
        }
         else {
            element.before(error)
        }
    },

    rules: {
        donate_price_ambassador: {
            required: true
        }
    },

    messages: {},

});
// end valid form donate month

$('.send-donate').click(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // get all id inputs
    var form = $('.donate-ambsaador-form');
    form.valid();

    if (form.valid()) 
    {
        
        // show loading
        $('.bg_spinner').show();  
    
        var project_id   = form.find('#project');
        var price        = form.find('#price');
        var currency     = form.find('#currency');

        // route post request
        var url = $(this).attr('data-url');

        var formData = new FormData();

        // uplaode other inputs
        formData.append('price_ambsaador' , price.val());
        formData.append('project_id'   , project_id.val());
        formData.append('currency_id'  , currency.val());
        formData.append('type'         , 'ambassador');

        // send request ajax
        $.ajax({

            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,                
            success: function(result){


                // hide loading
                $('.bg_spinner').hide();  
                
                // Swal.fire({
                //     icon: 'success',
                //     title: success_donate,
                //     showConfirmButton: false,
                //     timer: 1000,
                // }); 

                location.href = result.checkoute_route;

            }, // end of success
            error: function (jqXHR) {

                // hide loading
                $('.bg_spinner').hide();  

                var forrm_err = $('.ambasador-donate');
                // error validation
                validatePriceAmbsaador(forrm_err , price , jqXHR.responseJSON.errors.price);


            } //  end of error
            
        }); // end of ajax 

    } // end of valid

});

// active notification
$('#active-notification').click(function () {

    var url = $(this).attr('url');

    $.ajax({
        url: url,
        type: "GET",      
        data: {},
        dataType: 'json',
        success: function(result){
            if (result.data == 0) {
                
                Swal.fire({
                    icon: 'success',
                    title: success_unactive,
                    showConfirmButton: false,
                    timer: 1000,
                }); 

            } else 
            {

                Swal.fire({
                    icon: 'success',
                    title: success_active,
                    showConfirmButton: false,
                    timer: 1000,
                }); 

            } // end of else if result data == 0

        }, // end of success
        
    }); // end of ajax  

}) // end of active notification