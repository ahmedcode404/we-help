//select 
var lang = $('.language').attr('lang');
var auth = $('#auth').val();

if (lang == 'ar') {

    var success_donate   = 'تم التبرع بنجاح';
    var success_compaign = 'تم انشاء الحملة بنجاح';
    var not_auth = 'للتبرع يجب عليك التسجيل اولا ';
    var complate_cost = 'تم اكمال مبلغ التبرع للمشروع يمكنك التبرع لمشروع اخر ';
    var user_donation_full = 'لا يمكنك تجاوز الحد الأقصى للتبرع فى السنة و هو 10000 جنية إسترلينى';
    var user_donation_full_html = 'المبلغ المسموح لك بالتبرع به هو ';
    var cur = 'جنية إسترلينى';
    var invalid_cost = 'مبلغ التبرع غير صحيح'; 

} else 
{

    var success_donate   = 'Done Donate Successfully';
    var success_compaign = 'Done Create Campaign Successfully';
    var not_auth = 'Oops , Donate Need To Register First';
    var complate_cost = 'The donation amount for the project has been completed. You can donate to another project';
    var user_donation_full = 'You cannot exceed the maximum donation amount per year which is £10,000';
    var user_donation_full_html = 'The amount you are allowed to donate is';
    var cur = '£';
    var invalid_cost = 'Invalid donation cost';

}

$('.complate-cost').click(function(){

    complateCost(complate_cost , 'info');

}); // end of complate

$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });


});


$('.select-input2').select2({
    theme: 'bootstrap4',
    language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',
    dropdownCssClass: "select-width"
});

//counter
$('.plus-num').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$('.minus-num').click(function () {
    if ($(this).next().val() > 100) {
        $(this).next().val(+$(this).next().val() - 1);
    }
});


//copy
$(".share-url-div .custom-btn").click(function () {
    $(this).prev(".form-control").select();
    document.execCommand("copy");
});

//donate-sm-form
$('.donate-sm-form').each(function () {
    $(this).validate({
        errorPlacement: function (error, element) {
            if (element.hasClass("number-input")) {} else {
                element.after(error)
            }
        },

        rules: {

        }

    });
});


$('.select-modal form').each(function () {
    $(this).validate({
        errorPlacement: function (error, element) {
            if (element.hasClass("select-input") || element.hasClass("select-input2")) {
                error.insertAfter(element.next());
            } else {
                element.before(error)
            }
        },
    });
});


$(".ambassador-modal .amb-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
     if (form_near.valid()) {
        $(".check-donate-amb").show();
        $(".share-url-div").slideDown();
    } else {
        $(".share-url-div,.ambasador-donate").slideUp();

    }
})

$("#amb_pay").click(function () {
    if ($("#amb_pay").is(":checked")) {
        $(".ambasador-donate").slideDown();
        $(".donate-ambsaador-form").valid();
    } else {
        $(".ambasador-donate").slideUp();
    }
});

//colors
$(window).on("load", function () {
    $(".progress-bar-ratio").each(function () {
        var data_progress = $(this).attr("data-progress") + "%";
        $(this).animate({
            'width': data_progress
        }, 500);
    });
    $(".nav-item").click(function () {
        var light_color = $(this).attr("data-light-color");
        var main_color = $(this).attr("data-main-color");
        $("body").get(0).style.setProperty("--light_color", light_color);
        $("body").get(0).style.setProperty("--main_color", main_color);
        $(".progress-bar-ratio").each(function () {
            var data_progress = $(this).attr("data-progress") + "%";
            setTimeout(() => {
                $(this).animate({
                    'width': data_progress
                }, 300);
            }, 300);

        });
    });

});

//resposive filter
$(function () {
    $(".filter-tabs-btn").click(function () {
        $(".filter-tabs").toggleClass("active");
    })


    var $winf = $(window); // or $box parent container
    var $boxf = $(".filter-tabs-btn,.filter-tabs");
    $winf.on("click.Bst", function (event) {
        if (
            $boxf.has(event.target).length === 0 && //checks if descendants of $box was clicked
            !$boxf.is(event.target) //checks if the $box itself was clicked
        ) {
            $(".filter-tabs").removeClass("active")
        }
    });

    $(".close-tabs").click(function () {
        $(".filter-tabs").removeClass("active");
    })
});


/////////////////////////////////////////// BEGIN: donate //////////////////////////////////////////

// start show model of type donate 
$(document).on("click", ".donate-sm-form .donate-project", function (e) {

    e.preventDefault();

    // auth user or no
    if(auth != 0)
    {

        // show loading
        $('.bg_spinner').show();        

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    
        var id = $(this).attr('data-id');
    
        // get all id inputs
        var form_near = $(this).parents(".donate-cost-error-"+id);
    
        var number_input      = form_near.find('.number-input');
        var pop_select        = form_near.find('.pop-select');
        var currency_id       = form_near.find('.currency_id');
        var project_id        = form_near.find('.project_id');
        
        // route post request
        var url = $(this).attr('data-url');
    
        var formData = new FormData();
    
        // uplaode other inputs
        
        if (pop_select.val() == 'ambassador') {
            
            formData.append('pop_select'  , pop_select.val());
            formData.append('project_id'  , project_id.val());
    
        } else 
        {
            
            formData.append('project_id'  , project_id.val());
            formData.append('pop_select'  , pop_select.val());
            formData.append('currency_id' , currency_id.val());
            formData.append('number_input', number_input.val());
    
        }// end of else if
    
        // send request ajax
        $.ajax({
    
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,                
            success: function(result){

                if(result.project_id == -1){

                    // show loading
                    $('.bg_spinner').hide();
                    $(".select-modal").modal("hide");

                    general_alert(user_donation_full , user_donation_full_html+' '+result.allowed_cost+' '+cur, 'warning');

                    return;
                }
                else if(result.project_id == -2){

                    // show loading
                    $('.bg_spinner').hide();
                    $(".select-modal").modal("hide");

                    general_alert(invalid_cost , null, 'warning');

                    return;
                }

                
                // show loading
                $('.bg_spinner').hide();                

                var val_option = form_near.find(".pop-select").val();
    
                $(".select-modal").modal("hide");
            
                if (val_option == "ambassador") {
    
                    var project = $('#form-donate-ambassador').find('#project_id_ambassador');
    
                    project.val(result.project_id);
    
                    $(".ambassador-modal").modal("show");
            
                }
                if (val_option == "once") {
    
                    // append date to form donate once
                    var project          = $('#form-donate-once').find('#project_id');
                    var price            = $('#form-donate-once').find('#price');
                    var currency         = $('#form-donate-once').find('#currency');
                    var currency_in_text = $('#form-donate-once').find('#currency_in_text');
                    var price_in_text    = $('#form-donate-once').find('#price_in_text');
    
                    project.val(result.project_id);
                    price.val(result.number_input);
                    currency.val(result.currency_id);
                    currency_in_text.text(result.currency);
                    price_in_text.text(result.number_input);
    
                    $(".once-modal").modal("show");
    
    
                } else if (val_option == "gift") {
    
                    // append date to form donate gift
    
                    var project       = $('#form-donate-gift').find('#project_id_gift');
                    var price_gift    = $('#form-donate-gift').find('#price_gift');
                    var currency_gift = $('#form-donate-gift').find('#currency_gift');
    
                    project.val(result.project_id);
                    price_gift.val(result.number_input);
                    currency_gift.val(result.currency_id);
    
                    $(".gift-modal").modal("show");
    
                } else if (val_option == "month") {
    
    
                    // append date to form donate month
    
                    var project                = $('#form-donate-month').find('#project_id_month');
                    var price_month            = $('#form-donate-month').find('#price_month');
                    var currency_month         = $('#form-donate-month').find('#currency_month');
                    var currency_in_text_month = $('#form-donate-month').find('#currency_in_text_month');
                    var price_in_text_month     = $('#form-donate-month').find('#price_in_text_month');
    
                    project.val(result.project_id);
                    price_month.val(result.number_input);
                    currency_month.val(result.currency_id);
                    currency_in_text_month.text(result.currency);
                    price_in_text_month.text(result.number_input);
    
                    $(".month-modal").modal("show");
    
                } else{
        
                }
            
            }, // end of success
            error: function (jqXHR) {
    
                // show loading
                $('.bg_spinner').hide();                
                console.log(jqXHR.responseJSON.errors.number_input);
                // error validation
                validatePrice(form_near , number_input , jqXHR.responseJSON.errors.number_input);
    
            } //  end of error
             
        }); // end of ajax        

    } else
    {

        notAuth(not_auth , 'question');

    } // end of else auth user or no

}); // end of show model of type donate

// start repeate donate of gift
var lang = $('.language').attr('lang');

if(lang == 'ar'){

    var another_owner = 'إضافة مالك';
    var delete_gift = 'حذف';
    var name = 'الإسم';
    var mobile_no = 'رقم الجوال';
    var email = 'البريد الإلكترونى';
}
else{

    var another_owner = 'Another Owner';
    var delete_gift = 'Delete';
    var name = 'Name';
    var mobile_no = 'Mobile No.';
    var email = 'Email';
}


//donate-form 
$(".repeate-btn").click(function () {
    var input_val = $(".main-repeate input").val();
    if (input_val.length === 0) {
        $(".main-repeate input").valid()
    } else {
        var append_content = `
        <div class="repeated-div">
        <h3 class="first_color text-cap">`+another_owner+`<i class="fa fa-times-circle first_color remove-chart tooltip-link second_color_hover" data-tippy-placement="top" title="حذف "></i></h3>

        <div class="form-group">
            <input type="text" class="form-control" />
            <label class="moving-label">`+name+`</label>
        </div>
    
        <div class="form-group">
            <input type="tel" class="form-control" minlength="9" maxlength="14" />
            <label class="moving-label">`+mobile_no+`.</label>
        </div>
    
        <div class="form-group">
            <input type="email" class="form-control"/>
            <label class="moving-label">`+email+`</label>
        </div>
    </div>
        `
        $(".repeat-div").append(append_content);
    }
});


$(document).on("click",".remove-chart",function(){
    $(this).closest(".repeated-div").remove();
});

// add donate once  
$('.add-donate-once').click(function (e) {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // get all id inputs
    var form = $('#form-donate-once');

    if (form.valid()) 
    {
        
        // show loading
        $('.bg_spinner').show();  
    
        var project_id   = form.find('#project_id');
        var price        = form.find('#price');
        var currency     = form.find('#currency');
        var once_check_2 = form.find('#once_check_2');
        var once_check_1 = form.find('#once_check_1');

        // route post request
        var url = $(this).attr('data-url');
        var type = $(this).attr('data-type');

        var formData = new FormData();

        // uplaode other inputs
        formData.append('price' , price.val());

        if (once_check_2.is(':checked')) {

            formData.append('once_check_2' , once_check_2.val());

        }
        if (once_check_1.is(':checked')) 
        {

            formData.append('once_check_1' , once_check_1.val());

        }
        
        formData.append('project_id'   , project_id.val());
        formData.append('currency_id'  , currency.val());
        formData.append('type'         , type);

        // send request ajax
        $.ajax({

            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,                
            success: function(result){

                // hide loading
                $('.bg_spinner').hide();  
                
                $(".once-modal").modal("hide");

                // success(success_donate , 'success');

                location.href = result.checkoute_route;

            }, // end of success
            error: function (jqXHR) {

                // hide loading
                $('.bg_spinner').hide();  

                // error validation
                checkboxFieldError(form , once_check_2 , jqXHR.responseJSON.errors.once_check_2);

            } //  end of error
            
        }); // end of ajax    


    } // end of if form valid

}) // end of add donate once

// add donate monthly
$('.add-donate-month').click(function (e) {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // get all id inputs
    var form = $('#form-donate-month');

    if (form.valid()) 
    {

        // show loading
        $('.bg_spinner').show();       

        var project_id     = form.find('#project_id_month');
        var price          = form.find('#price_month');
        var month_check_2  = form.find('#month_check_2');
        var month_check_1  = form.find('#month_check_1');
        var currency_month = form.find('#currency_month');

        // route post request
        var url = $(this).attr('data-url');
        var type = $(this).attr('data-type');

        var formData = new FormData();

        // uplaode other inputs
        formData.append('price' , price.val());

        if (month_check_2.is(':checked')) 
        {

            formData.append('month_check_2' , month_check_2.val());

        }

        if (month_check_1.is(':checked')) 
        {

            formData.append('month_check_1' , month_check_1.val());

        }

        formData.append('currency_id' , currency_month.val());
        formData.append('project_id'     , project_id.val());
        formData.append('type'           , type);

        // send request ajax
        $.ajax({

            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,                
            success: function(result){

                // hide loading
                $('.bg_spinner').hide();  

                $(".month-modal").modal("hide");

                // success(success_donate , 'success');

                location.href = result.checkoute_route;                

            }, // end of success
            error: function (jqXHR) {

                // hide loading
                $('.bg_spinner').hide();  

                // error validation
                checkboxFieldError(form , month_check_2 , jqXHR.responseJSON.errors.month_check_2);

            } //  end of error
            
        }); // end of ajax    


    } // end of if form valid

}) // end of add donate monthly

// add donate gift
$('.add-donate-gift').click(function (e) {

    if ($(".main-repeate input").valid()) 
    {

        if ($("#form-donate-gift").find('.input-valid').valid()) {
            
            // show loading
            $('.bg_spinner').show();  

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // get all id inputs
            var form = $('#form-donate-gift');
        
            var name            = form.find('#name');
            var name2           = form.find(".name2");
            var tel2            = form.find(".tel2");
            var email2          = form.find(".email2");
            var project_id_gift = form.find("#project_id_gift");
            var currency_gift   = form.find("#currency_gift");
            var price_gift      = form.find("#price_gift");
            var gift_check_1    = form.find("#gift_check_1");
            var check_14        = form.find("#check_14");

            var url = $(this).attr('data-url');
            var type = $(this).attr('data-type');

            var formData = new FormData();

            formData.append('type'        , type);
            formData.append('name'        , name.val());
            formData.append('project_id'  , project_id_gift.val());
            formData.append('price'       , price_gift.val());
            formData.append('currency_id' , currency_gift.val());

            if (gift_check_1.is(':checked')) 
            {

                formData.append('gift_check_1' , gift_check_1.val());
            
            }

            if (check_14.is(':checked')) 
            {

                formData.append('check_14' , check_14.val());
            
            }    

            name2.each(function() { formData.append('name2[]'   , $(this).val()); });

            tel2.each(function()  { formData.append('tel2[]'   , $(this).val()); });  
            
            email2.each(function(){ formData.append('email2[]' , $(this).val()); });          
            
            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(result){

                    // hide loading
                    $('.bg_spinner').hide();  

                    $(".gift-modal").modal("hide");

                    // success(success_donate , 'success');

                    location.href = result.checkoute_route;                

                }, // end of success
                error: function (jqXHR) {


                    // hide loading
                    $('.bg_spinner').hide();  

                    // convert object to array
                    var obj = jqXHR.responseJSON.errors;

                    var result = Object.keys(obj).map(function(key) {
                        
                        // Using Number() to convert key to number type
                        // Using obj[key] to retrieve key value
                        if (key.indexOf('name2') > -1 || key.indexOf('tel2') > -1 || key.indexOf('email2') > -1 ) {

                            return [key, obj[key]];

                        }

                    });
                    //end
                    // console.log(result);
                    var arr = result.filter(Boolean);
        
                    if (arr.length > 1) {
                    
                        for (const j in arr) {

                            var key = arr[j][0];
                            var num_key = key.split('.')[1];
                            //prop service  
                            if (key.indexOf('name2') > -1) {

                                textError(form , $('#name2_' + num_key) , arr[j][1]);

                            }

                            if (key.indexOf('tel2') > -1) {
                            
                                textError(form , $('#tel2_' + num_key) , arr[j][1]);

                            }   
                            
                            if (key.indexOf('email2') > -1) {
                            
                                textError(form , $('#email2_' + num_key) , arr[j][1]);

                            }                       

                        } // END OF FOR

                    } // END OF IF        

                    textError(form , name , jqXHR.responseJSON.errors.name);
                    checkboxFieldError(form , gift_check_1 , jqXHR.responseJSON.errors.gift_check_1);


                } //  end of error
                
            }); // end of ajax 

        } // end of if input-valid

    } // end of if main-repeate input

}) // end of add donate gift

// add campaign goal
$('.add-campaign-goal').click(function (e) {

    if ($("#form-donate-ambassador").valid()) 
    {

            // show loading
            $('.bg_spinner').show();  

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // get all id inputs
            var form                  = $('#form-donate-ambassador');
            var project_id_ambassador = form.find('#project_id_ambassador');
            var ambassador_name       = form.find('#ambassador_name');
            var campaign_name         = form.find('#campaign_name');
            var campaign_goal_id         = form.find('#campaign_goal_id');
            var amb_check_1           = form.find('#amb_check_1');

            var url = $(this).attr('data-url');

            var formData = new FormData();

            formData.append('project_id'      , project_id_ambassador.val());
            formData.append('ambassador_name' , ambassador_name.val());
            formData.append('campaign_name'   , campaign_name.val());
            formData.append('campaign_goal_id'   , campaign_goal_id.val());

            if (amb_check_1.is(':checked')) 
            {

                formData.append('amb_check_1' , amb_check_1.val());
            
            }    

            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(result){

                    // hide loading
                    $('.bg_spinner').hide();  

                    // disabled button of user can not click
                    $('.add-campaign-goal').attr('disabled' , true);
                    form.find('#affiliate-url').val(result.url);
                    success(success_compaign , 'success');

                }, // end of success
                error: function (jqXHR) {

                    // hide loading
                    $('.bg_spinner').hide();  


                    // error validation
                    textError(form , ambassador_name , jqXHR.responseJSON.errors.ambassador_name);
                    textError(form , campaign_name , jqXHR.responseJSON.errors.campaign_name);
                    textError(form , campaign_goal , jqXHR.responseJSON.errors.campaign_goal);
                    checkboxFieldError(form , amb_check_1 , jqXHR.responseJSON.errors.amb_check_1);

                } //  end of error
                
            }); // end of ajax 


    } // end of if main-repeate input

}) // end of add campaign goal

// add donate ambassador
$('.add-donate-ambassador').click(function (e) {

    if ($("#donate-ambsaador-form").valid()) 
    {


            // show loading
            $('.bg_spinner').show();  

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // get all id inputs
            var form                  = $('#form-donate-ambassador');
            var project_id_ambassador = form.find('#project_id_ambassador');

            var form_donate = $('#donate-ambsaador-form');
            var price_ambsaador    = form_donate.find('#price_ambsaador');
            var currency_ambsaador = form_donate.find('#currency_ambsaador');

            var type = $(this).attr('data-type');

            var url = $(this).attr('data-url');

            var formData = new FormData();

            formData.append('type'            , type);
            formData.append('project_id'      , project_id_ambassador.val());
            formData.append('price_ambsaador' , price_ambsaador.val());
            formData.append('currency_id'     , currency_ambsaador.val());

            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(result){

                    // hide loading
                    $('.bg_spinner').hide();  

                    // success(success_donate , 'success');

                    $(".ambassador-modal").modal("hide"); 
                                       
                    location.href = result.checkoute_route;
                    

                }, // end of success
                error: function (jqXHR) {

                    // hide loading
                    $('.bg_spinner').hide();  

                    var forrm_err = $('.ambasador-donate');
                    // error validation
                    validatePriceAmbsaador(forrm_err , price_ambsaador , jqXHR.responseJSON.errors.price_ambsaador);

                } //  end of error
                
            }); // end of ajax 


    } // end of if main-repeate input

}) // end of add donate ambassador

/////////////////////////////////////////// END: donate //////////////////////////////////////////



// get projects of selected category
$('.project_slug').click(function(){
    var self = $(this).parent();
    $('#myTabContent').html(`
        <div class="projects-loading">
            <img src="../../web/images/main/ajax-loader.gif" alt="loading">
        </div>
    `);

    var action = $(this).data('action');
    var method = $(this).data('method');

     // Make ajax request
     $.ajax({
        url: action,
        type: method,
        data:{"_token": $('meta[name="csrf-token"]').attr('content')},
        dataType: 'html',
        success: function (data) {
            // $('#myTabContent').html(data);
            $('#myTabContent').html('');
            $('#myTabContent').append(data);
            var light_color = self.attr("data-light-color");
            var main_color = self.attr("data-main-color");
            $("body").get(0).style.setProperty("--light_color", light_color);
            $("body").get(0).style.setProperty("--main_color", main_color);
            $(".progress-bar-ratio").each(function () {
                var data_progress = $(this).attr("data-progress") + "%";
                setTimeout(() => {
                    $(this).animate({
                        'width': data_progress
                    }, 300);
                }, 300);
    
            });
            $(".active .project-grid").each(function(i){
                var div = $(this);
                setTimeout(function(){
                div.animate({opacity: 0}, {duration: 300, queue: false})
                        .delay(100).animate({opacity: 1}, {duration: 300});
                },i*300);
            });  

            $('.select-input2').select2({
                theme: 'bootstrap4',
                language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',
                dropdownCssClass: "select-width"
            });
            $('.select-input').select2({
                theme: 'bootstrap4',
                language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',
            });
                        
        }
    });
});

// $(window).on("load",function() {
//     $(".project-grid").each(function(i){
//       var div = $(this);
//       setTimeout(function(){
//       div.animate({opacity: 0}, {duration: 1500, queue: false})
//               .delay(100).animate({opacity: 1}, {duration: 1500});
//       },i*300);
//   });       
// });

    $(".active .project-grid").each(function(i){
              var div = $(this);
              setTimeout(function(){
              div.animate({opacity: 0}, {duration: 1500, queue: false})
                      .delay(100).animate({opacity: 1}, {duration: 1500});
              },i*300);
          });  


