//signture
var signature = {
    canvas: null,
    clearButton: null,
    init: function init() {
        this.canvas = document.querySelector(".signature-pad");
        this.clearButton = document.getElementById('clear');
        signaturePad = new SignaturePad(this.canvas);
        this.clearButton.addEventListener('click', function (event) {
            signaturePad.clear();
        });
    }
};

signature.init();

