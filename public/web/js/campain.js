var mydir = $("html").attr("dir");
if (mydir == 'rtl') {
    rtlVal = true
} else {
    rtlVal = false
}

//counter
$('.plus-num').click(function () {
    $(this).prev().val(+$(this).prev().val() + 1);
});
$('.minus-num').click(function () {
    if ($(this).next().val() > 100) {
        $(this).next().val(+$(this).next().val() - 1);
    }
});

var lang = $('.language').attr('lang');

if (lang == 'ar') {

    var show_more = 'عرض المزيد';
    var show_less = 'عرض اقل';

} else 
{

    var show_more = 'Show More';
    var show_less = 'Show less';

}


//owl-carousel
$("#campain-owl").owlCarousel({
    loop: true,
    items: 1,
    nav: false,
    dots: true,
    autoplay: true,
    mouseDrag: true,
    touchDrag: true,
    rtl: rtlVal,
    autoplayTimeout: 8000
});


$('#rate-modal').on('shown.bs.modal', function () {
    var $owl = $("#rate-owl");
    $owl.owlCarousel({
        loop: false,
        items: 1,
        nav: false,
        dots: true,
        autoplay: false,
        mouseDrag: false,
        touchDrag: false,
        rtl: rtlVal,
    });
});

$('.next-owl').click(function () {
    $(this).next(".done-rate").addClass("active");
    
    setTimeout(() => {
        $("#rate-owl").trigger('next.owl.carousel');
    }, 500);
})

//select 
$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });


});

$('.select-input2').select2({
    theme: 'bootstrap4',
    language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',
    dropdownCssClass: "select-width"
});




$(".ambassador-modal .amb-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
     if (form_near.valid()) {
        $(".check-donate-amb").show();
        $(".share-url-div").slideDown();
    } else {
        $(".share-url-div,.ambasador-donate").slideUp();

    }
})

$("#amb_pay").click(function () {
    if ($("#amb_pay").is(":checked")) {
        $(".ambasador-donate").slideDown();
        $(".donate-ambsaador-form form").valid();
    } else {
        $(".ambasador-donate").slideUp();
    }
});


// $(document).on("click", ".donate-form .donate-prog-btn", function () {
//     var form_near = $(this).parents(".donate-form");
//     form_near.valid();
//     var val_option = form_near.find(".multi-checkboxes input:checked").val();
//     $(".select-modal").modal("hide");
//     if (val_option == "ambassador") {
//         $(".ambassador-modal").modal("show");
//     }
//     if (form_near.valid()) {
//         if (val_option == "once") {
//             $(".once-modal").modal("show");
//         } else if (val_option == "gift") {
//             $(".gift-modal").modal("show");
//         } else if (val_option == "month") {
//             $(".month-modal").modal("show");
//         } else{

//         }
//     } else {
//         $(".select-modal").modal("hide");
//     }
// });

var lang = $('.language').attr('lang');

if(lang == 'ar'){

    var another_owner = 'إضافة مالك';
    var delete_gift = 'حذف';
    var name = 'الإسم';
    var mobile_no = 'رقم الجوال';
    var email = 'البريد الإلكترونى';
}
else{

    var another_owner = 'Another Owner';
    var delete_gift = 'Delete';
    var name = 'Name';
    var mobile_no = 'Mobile No.';
    var email = 'Email';
}

//donate-form 
$(".repeate-btn").click(function () {
    var input_val = $(".main-repeate input").val();
    if (input_val.length === 0) {
        $(".main-repeate input").valid()
    } else {
        var append_content = `
        <div class="repeated-div">
        <h3 class="first_color text-cap">`+another_owner+`<i class="fa fa-times-circle first_color remove-chart tooltip-link second_color_hover" data-tippy-placement="top" title="حذف "></i></h3>

        <div class="form-group">
            <input type="text" class="form-control" />
            <label class="moving-label">`+name+`</label>
        </div>
    
        <div class="form-group">
            <input type="tel" class="form-control" minlength="9" maxlength="14" />
            <label class="moving-label">`+mobile_no+`.</label>
        </div>
    
        <div class="form-group">
            <input type="email" class="form-control"/>
            <label class="moving-label">`+email+`</label>
        </div>
    </div>
        `
        $(".repeat-div").append(append_content);
    }
});


$(document).on("click",".remove-chart",function(){
    $(this).closest(".repeated-div").remove();
});

//progress
$(window).on("load", function () {
    $(".progress-bar-ratio").each(function () {
        var data_progress = $(this).attr("data-progress") + "%";
        $(this).animate({
            'width': data_progress
        }, 300);
    });

})
//comments
$(".inner-comments li").slice(0, 4).fadeIn();
$(".loadmore").click(function () {
    var divNum = $(".inner-comments").find("li").length;
    $('.inner-comments li').each(function (index) {
        if (divNum > 4 && index > 3) {
            $(this).fadeToggle();
        }
    });
    $(this).toggleClass("active");
    $(".loadmore").hasClass("active") ? $(this).text(show_less) : $(this).text(show_more)

})


//statisics
var a = 0;
$(window).scroll(function () {
    var oTop = $('.campain-icons').offset().top - window.innerHeight;
    if (a == 0 && $(window).scrollTop() > oTop) {
        $('.campain-icons p').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.find("span").text()
            }).animate({
                countNum: countTo
            }, {
                duration: 1000,
                step: function () {
                    $this.find("span").text(Math.floor(this.countNum));
                },
                complete: function () {
                    $this.find("span").text(this.countNum);
                }

            });
        });
        a = 1;
    }

});


// //cart add
// $(function () {
//     var cart = $('.cart-count'),
//         addToCart = $('.add-to-cart2');
//     cartCountCont = cart.find('.num'),
//         cartCount = parseInt(cartCountCont.text(), 10),
//         addToCart.on('click', function () {
//             cartCount += 1;
//             cartCountCont.text(cartCount);
//             cartCountCont.removeClass("animated bounce")
//             setTimeout(() => {
//                 cartCountCont.addClass("animated bounce")
//             }, 900);

//         });
// });




//copy
$(".share-url-div .custom-btn").click(function () {
    $(this).prev(".form-control").select();
    document.execCommand("copy");
});

$(".select-modal .custom-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
})

$(".ambassador-modal .custom-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
    if (form_near.valid()) {
        $(".share-url-div").slideDown();
    } else {
        $(".share-url-div").slideUp();

    }
})
$('.select-modal form').each(function () {
    $(this).validate({
        errorPlacement: function (error, element) {
            if (element.hasClass("select-input") || element.hasClass("select-input2")) {
                error.insertAfter(element.next());
            } else {
                element.before(error)
            }
        },
        submitHandler: function () {
            // $(".modal").modal("hide")
        }
    });
});


var $rate_form = $(".rate-form");
$rate_form.validate({
    errorPlacement: function errorPlacement(error, element) {
        if (element.attr("type") == "radio") {
            error.insertAfter(element.closest(".inner-rate"))
        } else {
            element.after(error)
        }

    },

    rules: {

    },

    messages: {},
    submitHandler: function () {

        $(".last-done").addClass("active");

        setTimeout(function () {
            $("#rate-modal").modal("hide");
        }, 1000)
      
        $rate_form[0].submit()

    }
});



var $donateform = $(".donate-form");
$donateform.validate({
    errorPlacement: function errorPlacement(error, element) {
        if ((element.attr("name") == "currency") || (element.attr("name") == "donate_price")) {
            error.insertBefore(element.closest(".donate-quantity"));
        }  else if ((element.attr("name") == "donate_time")) {
            error.insertAfter(element.closest(".multi-checkboxes"));
        }
         else {
            element.before(error)
        }
    },

    rules: {
        donate_time: {
            required: true
        }
    },

    messages: {},
    submitHandler: function () {
     
    }
});
