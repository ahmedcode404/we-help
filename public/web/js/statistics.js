//statisics
var a = 0;
$(window).scroll(function () {

    var oTop = $('#counter').offset().top - window.innerHeight;
    var stat_height = $(".statistics-sec").outerHeight() / 2;
    if (a == 0 && $(window).scrollTop() > (oTop + stat_height)) {
        $('.counter-value').each(function () {
            var $this = $(this),
                countTo = $this.attr('data-count');
            $({
                countNum: $this.find("span").text()
            }).animate({
                    countNum: countTo
                },

                {

                    duration: 4000,
                    easing: 'swing',
                    step: function () {
                        $this.find("span").text(Math.floor(this.countNum));
                    },
                    complete: function () {
                        $this.find("span").text(this.countNum);
                        //alert('finished');
                    }

                });
        });
        a = 1;
    }

});
