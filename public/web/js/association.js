$(window).on('load', function(){

    var action = $('#country_id').data('action');
    var method = $('#country_id').data('method');
    var id = $('#country_id').val();

    if(id != null){
        // Make ajax request
        $.ajax({
            url: action,
            type: method,
            data:{"_token": $('meta[name="csrf-token"]').attr('content'), id: id},
            dataType: 'html',
            success: function (data) {
                
                $('#city_id').html(data);

                $('#city_id').select2({ theme: 'bootstrap4', language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',width:'100%'});

            }
        });
    }
});

$(function () {
    $('.gray-form input,.gray-form textarea').keyup(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $(".gray-form select,.gray-form input").change(function () {
        if ($(this).val() != "") {
            $(this).parents(".form-group").addClass("has_val")
        } else {
            $(this).parents(".form-group").removeClass("has_val")
        }
    });

    $('.gray-form input,.gray-form textarea,.gray-form select').each(function () {
        if (($(this).val() == "") || ($(this).val() == null)) {
            $(this).parents(".form-group").removeClass("has_val")
        } else {
            $(this).parents(".form-group").addClass("has_val")
        }
    });


});

//datepicker
$(function () {
    $('.datepick').datepicker({
        changeYear: true,
        dateFormat: 'mm/dd/yy',
        onSelect: function () {
            $(this).parents(".form-group").addClass("has_val")
        }
    })
});

// phone
var tel_url = $('#tel_url').val();
var input = window.intlTelInput(document.querySelector("#phone"), {
    preferredCountries: ["gb", "sa"],
    hiddenInput: "phone",
    separateDialCode:true,
    formatOnDisplay:false,
    utilsScript: tel_url
});

// mobile
var input_mobile = window.intlTelInput(document.querySelector("#mobile"), {
    preferredCountries: ["gb", "sa"],
    hiddenInput: "mobile",
    separateDialCode:true,
    formatOnDisplay:false,
    utilsScript: tel_url
});


// bank_phone
var bank_phone = window.intlTelInput(document.querySelector("#bank_phone"), {
    preferredCountries: ["gb", "sa"],
    hiddenInput: "bank_phone",
    separateDialCode:true,
    formatOnDisplay:false,
    utilsScript: tel_url
});

if ($("#yes_check").is(':checked')) {
    $(".organization-div").fadeIn();
}

$(".custom-checkbox input").click(function () {
    if ($("#yes_check").is(':checked')) {
        $(".organization-div").fadeIn();
    } else {
        $(".organization-div").fadeOut();
    }
});


//clone div
// $(".copy-chart").click(function(){
//     var append_dt = `<div class="clone-div">
//     <div class="row align-items-end">
//     <div class="col-12" data-aos="fade-in">
//     <h3 class="first_color">add another Project :</h3>
//     </div>

//     <div class="col-12" data-aos="fade-in">
//         <div class="form-group">
//             <input type="text" class="form-control" name="project_Name" required>
//             <label class="moving-label">Project Name</label>
//         </div>
//     </div>

//     <div class="col-12" data-aos="fade-in">
//         <div class="form-group">
//             <input type="text" class="form-control" name="project_Type" required>
//             <label class="moving-label">Project Type</label>
//         </div>
//     </div>

//     <div class="col-12" data-aos="fade-in">
//         <div class="row align-items-end files-show">
//             <div class="col-md-3 col-6">
//                 <div class="form-group">
//                     <div class="custom-file-div single-file">
//                         <input type="file" class="hidden-input" name="file_1"
//                             id="filesapp_1" accept=" .jpg, .png,.jpeg" required />
//                         <label for="filesapp_1"><img src="images/main/file.png"
//                                 alt="add image"></label>
//                     </div>
//                 </div>
//             </div>

//             <div class="col-md-3 col-6">
//                 <div class="form-group">
//                     <div class="custom-file-div single-file">
//                         <input type="file" class="hidden-input" name="file_1"
//                             id="files_2" accept=" .jpg, .png,.jpeg" required />
//                         <label for="files_2"><img src="images/main/file.png"
//                                 alt="add image"></label>
//                     </div>
//                 </div>
//             </div>

//             <div class="col-md-3 col-6">
//                 <div class="form-group">
//                     <div class="custom-file-div single-file">
//                         <input type="file" class="hidden-input" name="file_1"
//                             id="files_3" accept=" .jpg, .png,.jpeg" required />
//                         <label for="files_3"><img src="images/main/file.png"
//                                 alt="add image"></label>
//                     </div>
//                 </div>
//             </div>

//             <div class="col-md-3 col-6">
//                 <div class="form-group">
//                     <div class="custom-file-div multi-file">
//                         <input type="file" class="hidden-input" name="file_1"
//                             multiple id="files_4" accept=" .jpg, .png,.jpeg" />
//                         <label for="files_4"
//                             class="row no-marg-row align-items-center hirozintal-scroll"><img
//                                 src="images/main/add.png" alt="add image">
//                             <div class="col-12 images-multi"></div>
//                         </label>
//                     </div>
//                 </div>
//             </div>
//         </div>

//     </div>
// </div>`
//     $(".main-clone-sec").append(append_dt);

// });

//upload file single
$(function () {
       
    $(document).on("click",".remove-input-img",function () {
        $(this).parent().find("input").val('');
        $(this).parent().find("img").attr('src','https://beta.wehelp.jadara.work/web/images/main/file.png');
        $(this).remove();
    });

    $(document).on("change",".files-show .single-file input",function () {
        var self = $(this).parent().find("img");
        self.fadeIn().css("display", "block");

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    self.attr('src', e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        readURL(this)
        $(this).parent().append('<i class="fa fa-times first_color remove-input-img second_color_hover"></i>');
    });
 

    

    $(".single-file input").change(function () {
        var filenames = [];
        for (var i = 0; i < this.files.length; i++) {
            // filenames +=  this.files[i].name + ',';
            filenames.push(this.files[i].name);
        }
        $(this).parent().find(".file-label").text(filenames);
    });

});

//upload file multiple
// $(function () {
//     // Multiple images preview in browser
//     var imagesPreview = function (input, placeToInsertImagePreview) {

//         if (input.files) {
            
//             var filesAmount = input.files.length;

//             for (i = 0; i < filesAmount; i++) {
//                 var reader = new FileReader();

//                 reader.onload = function (event) {
//                     $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
//                 }

//                 reader.readAsDataURL(input.files[i]);
//             }
//         }

//     };


//     $(document).on('change', '.multi-file input',function () {
//         $(this).parent().find("label > img").remove();
//         var content = $(this).parents(".multi-file").find(".images-multi");
//         imagesPreview(this, content);
//         $(this).parent().append("<span class='remove-file auto-icon'><i class='fa fa-times'></span>");
//     });

//     $(document).on('click', '.remove-file', function () {
//         $(this).parents(".multi-file").find("input").val('');
//         $(this).parents(".multi-file").find(".images-multi img").remove();
//         $(this).parents(".multi-file").find("label").append('<img src="'+window.location.origin+'/web/images/main/add.png" alt="add images">');
//         $(this).parents(".multi-file").find(".remove-file").remove();
//     });
// });

//donate-form validation 
var $association_form = $(".association-form");
$association_form.validate({
    focusInvalid: false,
    invalidHandler: function(form, validator) {
        if (!validator.numberOfInvalids())
            return;

        $('html, body').animate({
            scrollTop: $(validator.errorList[0].element).offset().top
        }, 1000);
    },
    errorPlacement: function errorPlacement(error, element) {
        if (element.hasClass("select-input") || element.hasClass("select-input2")) {
            error.insertAfter(element.next());
        } else if (element.attr("name") == "file_1") {
            error.insertAfter(".files-show");
        } else if (element.attr("type") == "file") {
            error.insertAfter(element.parent().find(".file-label"));
        } else if (element.attr("type") == "checkbox"){
            element.before(error)

        }else {
            element.after(error)
        }
    },

    rules: {
        phone: {
            digits: true,
        },
        password_confirmation : {
            equalTo : "#password_id"
        },
        country:{
            required:true,
        },
        // international_name_ar: {
        //     required: "#yes_check:checked"
        // },
        international_name_en: {
            required: "#yes_check:checked"
        },
        

    },

    messages: {},
    submitHandler: function () {
        // $association_form[0].submit();
    }
});

$(document).on("click",".remove-chart",function(){
    var images_counter = parseInt($('#images_counter').val());
    images_counter --; 
    $('#images_counter').val(images_counter);

    $(this).closest(".clone-div").remove();
});


// get cities of selected country
$('#country_id').change(function(){

    var action = $(this).data('action');
    var method = $(this).data('method');
    var id = $(this).val();

    // Make ajax request
    $.ajax({
        url: action,
        type: method,
        data:{"_token": $('meta[name="csrf-token"]').attr('content'), id: id},
        dataType: 'html',
        success: function (data) {
            
            $('#city_id').html(data);

            $('#city_id').select2({ theme: 'bootstrap4', language: $('html').attr('dir') == 'rtl' ? 'ar' : 'en',width:'100%'});

        }
    });
});

if(lang == 'ar'){

    var success_add = 'تمت الإضافة بنجاح';
    var success_edit = 'تم التعديل بنجاح';
    var failed = 'حدث خطأ ما';
    var join_us = 'إنضم';
    var general_error = 'يوجد خطأ ما، برجاء مراجعة المدخلات';
}
else{
    var success_add = 'Addess successfully';
    var success_edit = 'Updated successfully';
    var failed = 'An error has occured';
    var join_us = 'Join';
    var general_error = 'There is something wrong, please check the inputs';
}

$(document).on('click', '.submit_button', function () {

    var form_submit = $('.submit_form');
    form_submit.valid();
    
    if(form_submit.valid()){

        $('.submit_button').addClass('loading-btn');
        $('.submit_button').prop('disabled', true);
        $('#loading_image').html(join_us+`<img src="../../web/images/main/ajax-loader.gif" alt="loading">`);

        $(document).find('.error').html("");
        $(document).find('.error').hide();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });        

        var formData = new FormData(form_submit[0]),
        action = form_submit.attr('action'),
        method = form_submit.attr('method');

        formData.append('phone', input.getNumber());
        formData.append('mobile', input_mobile.getNumber());
        formData.append('bank_phone', bank_phone.getNumber());


        // make the ajax request
        $.ajax({
            url: action,
            type: method,
            data: formData,
            contentType: false,
            processData: false,
            success: function (response) {

                $('.submit_button').removeClass('loading-btn');
                $('.submit_button').prop('disabled', false);
                $('#loading_image').html(join_us);

                // response = JSON.parse(response);
                
                if(response.status == 1){

                    // form_submit.submit();
                    
                }
                else if(response.status == 0){

                    Swal.fire({
                        title: failed,
                        type: 'error',
                        timer: 2000,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                }
    
                // check for redirect
                if(response.redirect){

                    setTimeout(function (){

                        window.location.href = response.redirect;
                        
                    });
                }
    
                // check for reload
                if(response.reload && response.reload == 1){

                    setTimeout(function (){

                        location.reload();

                    }, 2000);
                }

            },
            error: function (errors) {

                $('#general_error').show();
                $('#general_error').text(general_error);
                $('.submit_button').removeClass('loading-btn');
                $('.submit_button').prop('disabled', false);
                $('#loading_image').html(join_us);

                for (var k in errors.responseJSON.errors) {
                    $(document).find('.error-'+k).show();
                    $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                }
            }
        });
    }


});
