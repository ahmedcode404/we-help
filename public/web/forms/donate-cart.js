var lang = $('.language').attr('lang');
var auth = $('#auth').val();

if (lang == 'ar') {

    var success_donate   = 'تم التبرع بنجاح';
    var success_compaign = 'تم انشاء الحملة بنجاح';
    var not_auth = 'للتبرع يجب عليك التسجيل اولا ';
    var complate_cost = 'تم اكمال مبلغ التبرع للمشروع يمكنك التبرع لمشروع اخر ';
    var user_donation_full_html = 'المبلغ المسموح لك بالتبرع به هو ';
    var cur = 'جنية إسترلينى';
    var invalid_cost = 'مبلغ التبرع غير صحيح'; 

} else 
{

    var success_donate   = 'Done Donate Successfully';
    var success_compaign = 'Done Create Campaign Successfully';
    var not_auth = 'Oops , Donate Need To Register First';
    var complate_cost = 'The donation amount for the project has been completed. You can donate to another project';
    var user_donation_full_html = 'The amount you are allowed to donate is';
    var cur = '£';
    var invalid_cost = 'Invalid donation cost';

}




$('.donate-form .complate-cost').click(function(){

    complateCost(complate_cost , 'info');

}); // end of complate


$(".ambassador-modal .amb-btn").click(function () {
    var form_near = $(this).closest("form");
    form_near.valid();
     if (form_near.valid()) {
        $(".check-donate-amb").show();
        $(".share-url-div").slideDown();
    } else {
        $(".share-url-div,.ambasador-donate").slideUp();

    }
})

$("#amb_pay").click(function () {
    if ($("#amb_pay").is(":checked")) {
        $(".ambasador-donate").slideDown();
        $(".donate-ambsaador-form   ").valid();
    } else {
        $(".ambasador-donate").slideUp();
    }
});

///////////////// BEGIN: validation form gift ////////////////////

$('[name^="name2"]').each(function() {
    $(this).rules('add', {
        required: true,
    })
});

$('[name^="tel2"]').each(function() {
    $(this).rules('add', {
        required: true,
        number: true,  
        rangelength: [9, 11],
        messages: {
            number: lang == 'ar' ? 'يجب ان يكون الحقل رقم'  : 'Your phone must be number'
        }
    })
});

$('[name^="email2"]').each(function() {
    $(this).rules('add', {
        required: true,  
        email : true,       
        messages: {
            email: lang == 'ar' ? 'البريد الالكتروني يجب ان يكون name@domain.com' : "Your email address must be in the format of name@domain.com"
        }
    })
});

///////////////// END: validation form gift //////////////////////


////////////////////////////////////////// BEGIN: show model once , month , gift ////////////////////////

$(document).on("click", ".donate-form .donate-prog-btn", function (e) {

    e.preventDefault();

    var form_near_valid = $(this).parents(".donate-form");

    // auth user or no
    if(auth !=0)
    {

        if (form_near_valid.valid()) {

            // show loading
            $('.bg_spinner').show();
            
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        
            var id = $(this).attr('data-id');
        
            // get all id inputs
            var form_near    = $(this).parents("#donate-cost-error-"+id);
            var number_input = form_near.find('.number-input');
            var pop_select   = form_near.find('.pop-select');
            var currency_id  = form_near.find('.currency_id');
            var project_id   = form_near.find('.project_id');

            // route post request
            var url = $(this).attr('data-url');
            var pu_id = $(this).attr('data-puid');

        
            var formData = new FormData();
        
            if (pop_select.val() == 'ambassador') {

                formData.append('pop_select'  , pop_select.val());
                formData.append('project_id'  , project_id.val());

            } 
            else 
            {

                formData.append('project_id'   , project_id.val());
                formData.append('pop_select'   , pop_select.val());
                formData.append('currency_id'  , currency_id.val());
                formData.append('number_input' , number_input.val());

            }
 

            // send request ajax
            $.ajax({
        
                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(result){

                    if(result.project_id == -1){

                        // show loading
                        $('.bg_spinner').hide();
                        $(".select-modal").modal("hide");
    
                        general_alert(user_donation_full , user_donation_full_html+' '+result.allowed_cost+' '+cur, 'warning');
    
                        return;
                    }
                    else if(result.project_id == -2){
    
                        // show loading
                        $('.bg_spinner').hide();
                        $(".select-modal").modal("hide");
    
                        general_alert(invalid_cost , null, 'warning');
    
                        return;
                    }

                    // append id project user
                    $('.add-donate-once').attr('data-projectuserid'       , pu_id);
                    $('.add-donate-month').attr('data-projectuserid'      , pu_id);
                    $('.add-donate-gift').attr('data-projectuserid'       , pu_id);
                    $('.add-donate-ambassador').attr('data-projectuserid' , pu_id);

                    // hide loading
                    $('.bg_spinner').hide();

                    var val_option  = form_near.find('.pop-select').val();

                    $(".select-modal").modal("hide");
                    if (val_option == "ambassador") {

                        var project = $('#form-donate-ambassador').find('#project_id_ambassador');
    
                        project.val(result.project_id);                        

                        $(".ambassador-modal").modal("show");

                    }

                    if (val_option == "once") {

                        // append date to form donate once
                        var project          = $('#form-donate-once').find('#project_id');
                        var price            = $('#form-donate-once').find('#price');
                        var currency         = $('#form-donate-once').find('#currency');
                        var currency_in_text = $('#form-donate-once').find('#currency_in_text');
                        var price_in_text    = $('#form-donate-once').find('#price_in_text');
        
                        project.val(result.project_id);
                        price.val(result.number_input);
                        currency.val(result.currency_id);
                        currency_in_text.text(result.currency);
                        price_in_text.text(result.number_input);
        
                        $(".once-modal").modal("show");

                    } else if (val_option == "gift") {
        
                        // append date to form donate gift
        
                        var project       = $('#form-donate-gift').find('#project_id_gift');
                        var price_gift    = $('#form-donate-gift').find('#price_gift');
                        var currency_gift = $('#form-donate-gift').find('#currency_gift');
        
                        project.val(result.project_id);
                        price_gift.val(result.number_input);
                        currency_gift.val(result.currency_id);
        
                        $(".gift-modal").modal("show");
        
                    } else if (val_option == "month") {
        
    
                        // append date to form donate month
        
                        var project                = $('#form-donate-month').find('#project_id_month');
                        var price_month            = $('#form-donate-month').find('#price_month');
                        var currency_month         = $('#form-donate-month').find('#currency_month');
                        var currency_in_text_month = $('#form-donate-month').find('#currency_in_text_month');
                        var price_in_text_month     = $('#form-donate-month').find('#price_in_text_month');
        
                        project.val(result.project_id);
                        price_month.val(result.number_input);
                        currency_month.val(result.currency_id);
                        currency_in_text_month.text(result.currency);
                        price_in_text_month.text(result.number_input);
        
                        $(".month-modal").modal("show");
        
                    } else{
            
                    }
                
                }, // end of success
                error: function (jqXHR) {
        
                    // hide loading
                    $('.bg_spinner').hide();

                    // error validation
                    validatePrice(form_near , number_input , jqXHR.responseJSON.errors.number_input);
        
                } //  end of error
                
            }); // end of ajax

        } // end of valid      

    } else
    {

        notAuth(not_auth , 'question');

    } // end of else auth user or no

}); // end of click custom btn

////////////////////////////////////////// END: show model once , month , gift //////////////////////////


//////////////////////////////////////////////////////////// END: donate ////////////////////////////////

// add donate once  
$('.add-donate-once').click(function (e) {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // get all id inputs
    var form = $('#form-donate-once');

    // auth user or no
    if(auth !=0)
    {    

        if (form.valid()) 
        {
            
            // show loadinng
            $('.bg_spinner').show();

            var project_id   = form.find('#project_id');
            var price        = form.find('#price');
            var currency     = form.find('#currency');
            var once_check_2 = form.find('#once_check_2');
            var once_check_1 = form.find('#once_check_1');

            // route post request
            var url = $(this).attr('data-url');
            var type = $(this).attr('data-type');
            var id = $(this).attr('data-projectuserid');

            var formData = new FormData();

            // uplaode other inputs
            formData.append('price' , price.val());

            if (once_check_2.is(':checked')) {

                formData.append('once_check_2' , once_check_2.val());

            }
            if (once_check_1.is(':checked')) 
            {

                formData.append('once_check_1' , once_check_1.val());

            }
            
            formData.append('project_id'   , project_id.val());
            formData.append('currency_id'  , currency.val());
            formData.append('type'         , type);
            formData.append('id'           , id);
            formData.append('cart'         , 'cart');

            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(result){

                    // hide loading
                    $('.bg_spinner').hide();                
                    
                    $(".once-modal").modal("hide");
                    
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: success_donate,
                    //     showConfirmButton: false,
                    //     timer: 1500,
                    // });
                    
                    location.href = result.checkoute_route;

                }, // end of success
                error: function (jqXHR) {

                    // hide loading
                    $('.bg_spinner').hide();                

                    // error validation
                    checkboxFieldError(form , once_check_2 , jqXHR.responseJSON.errors.once_check_2);

                } //  end of error
                
            }); // end of ajax    


        } // end of if form valid

    } else
    {

        notAuth(not_auth , 'question');

    } // end of else auth user or no

}) // end of add donate once

// add donate monthly
$('.add-donate-month').click(function (e) {
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // get all id inputs
    var form = $('#form-donate-month');

    if (form.valid()) 
    {
        
        // show loadinng     
        $('.bg_spinner').show();

        var project_id     = form.find('#project_id_month');
        var price          = form.find('#price_month');
        var month_check_2  = form.find('#month_check_2');
        var month_check_1  = form.find('#month_check_1');
        var currency_month = form.find('#currency_month');

        // route post request
        var url = $(this).attr('data-url');
        var type = $(this).attr('data-type');
        var id = $(this).attr('data-projectuserid');

        var formData = new FormData();

        // uplaode other inputs
        formData.append('price' , price.val());

        if (month_check_2.is(':checked')) 
        {

            formData.append('month_check_2' , month_check_2.val());

        }

        if (month_check_1.is(':checked')) 
        {

            formData.append('month_check_1' , month_check_1.val());

        }

        formData.append('currency_id' , currency_month.val());
        formData.append('project_id'  , project_id.val());
        formData.append('type'        , type);
        formData.append('id'          , id);
        formData.append('cart'        , 'cart');

        // send request ajax
        $.ajax({

            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,                
            success: function(result){

                // hide loading
                $('.bg_spinner').hide();                

                $(".month-modal").modal("hide");

                // Swal.fire({
                //     icon: 'success',
                //     title: success_donate,
                //     showConfirmButton: false,
                //     timer: 1500,
                // });

                location.href = result.checkoute_route;               

            }, // end of success
            error: function (jqXHR) {

                // hide loading
                $('.bg_spinner').hide();

                // error validation
                checkboxFieldError(form , month_check_2 , jqXHR.responseJSON.errors.month_check_2);

            } //  end of error
            
        }); // end of ajax    


    } // end of if form valid

}) // end of add donate monthly

// add donate gift
$('.add-donate-gift').click(function (e) {

    if ($(".main-repeate input").valid()) 
    {

        if ($("#form-donate-gift").find('.input-valid').valid()) {
            
            $('.bg_spinner').show();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // get all id inputs
            var form = $('#form-donate-gift');
        
            var name            = form.find('#name');
            var name2           = form.find(".name2");
            var tel2            = form.find(".tel2");
            var email2          = form.find(".email2");
            var project_id_gift = form.find("#project_id_gift");
            var currency_gift   = form.find("#currency_gift");
            var price_gift      = form.find("#price_gift");
            var gift_check_1    = form.find("#gift_check_1");
            var check_14        = form.find("#check_14");

            var url = $(this).attr('data-url');
            var type = $(this).attr('data-type');
            var id = $(this).attr('data-projectuserid');

            var formData = new FormData();

            formData.append('type'        , type);
            formData.append('name'        , name.val());
            formData.append('project_id'  , project_id_gift.val());
            formData.append('price'       , price_gift.val());
            formData.append('currency_id' , currency_gift.val());
            formData.append('id'          , id);
            formData.append('cart'        , 'cart');

            if (gift_check_1.is(':checked')) 
            {

                formData.append('gift_check_1' , gift_check_1.val());
            
            }

            if (check_14.is(':checked')) 
            {

                formData.append('check_14' , check_14.val());
            
            }    

            name2.each(function() { formData.append('name2[]'   , $(this).val()); });

            tel2.each(function()  { formData.append('tel2[]'   , $(this).val()); });  
            
            email2.each(function(){ formData.append('email2[]' , $(this).val()); });          
            
            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(){

                    // hide loading
                    $('.bg_spinner').hide();                    

                    $(".gift-modal").modal("hide");

                    // Swal.fire({
                    //     icon: 'success',
                    //     title: success_donate,
                    //     showConfirmButton: false,
                    //     timer: 1500,
                    // });
                    
                    location.href = result.checkoute_route;               

                }, // end of success
                error: function (jqXHR) {

                    // hide loading
                    $('.bg_spinner').hide();

                    // convert object to array
                    var obj = jqXHR.responseJSON.errors;

                    var result = Object.keys(obj).map(function(key) {
                        
                        // Using Number() to convert key to number type
                        // Using obj[key] to retrieve key value
                        if (key.indexOf('name2') > -1 || key.indexOf('tel2') > -1 || key.indexOf('email2') > -1 ) {

                            return [key, obj[key]];

                        }

                    });
                    //end
                    // console.log(result);
                    var arr = result.filter(Boolean);
        
                    if (arr.length > 1) {
                    
                        for (const j in arr) {

                            var key = arr[j][0];
                            var num_key = key.split('.')[1];
                            //prop service  
                            if (key.indexOf('name2') > -1) {

                                textError(form , $('#name2_' + num_key) , arr[j][1]);

                            }

                            if (key.indexOf('tel2') > -1) {
                            
                                textError(form , $('#tel2_' + num_key) , arr[j][1]);

                            }   
                            
                            if (key.indexOf('email2') > -1) {
                            
                                textError(form , $('#email2_' + num_key) , arr[j][1]);

                            }                       

                        } // END OF FOR

                    } // END OF IF        

                    textError(form , name , jqXHR.responseJSON.errors.name);
                    checkboxFieldError(form , gift_check_1 , jqXHR.responseJSON.errors.gift_check_1);


                } //  end of error
                
            }); // end of ajax 

        } // end of if input-valid

    } // end of if main-repeate input

}); // end of add donate gift

// add campaign goal
$('.add-campaign-goal').click(function (e) {

    if ($("#form-donate-ambassador").valid()) 
    {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // get all id inputs
            var form                  = $('#form-donate-ambassador');
            var project_id_ambassador = form.find('#project_id_ambassador');
            var ambassador_name       = form.find('#ambassador_name');
            var campaign_name         = form.find('#campaign_name');
            var campaign_goal_id         = form.find('#campaign_goal_id');
            var amb_check_1           = form.find('#amb_check_1');

            var url = $(this).attr('data-url');

            var formData = new FormData();

            formData.append('project_id'      , project_id_ambassador.val());
            formData.append('ambassador_name' , ambassador_name.val());
            formData.append('campaign_name'   , campaign_name.val());
            formData.append('campaign_goal_id', campaign_goal_id.val());


            if (amb_check_1.is(':checked')) 
            {

                formData.append('amb_check_1' , amb_check_1.val());
            
            }    

            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(result){

                    // disabled button of user can not click
                    $('.add-campaign-goal').attr('disabled' , true);
                    form.find('#affiliate-url').val(result.url);

                    Swal.fire({
                        icon: 'success',
                        title: success_compaign,
                        showConfirmButton: false,
                        timer: 1500,
                    });

                }, // end of success
                error: function (jqXHR) {

                    // error validation
                    textError(form , ambassador_name , jqXHR.responseJSON.errors.ambassador_name);
                    textError(form , campaign_name , jqXHR.responseJSON.errors.campaign_name);
                    textError(form , campaign_goal , jqXHR.responseJSON.errors.campaign_goal);
                    checkboxFieldError(form , amb_check_1 , jqXHR.responseJSON.errors.amb_check_1);

                } //  end of error
                
            }); // end of ajax 


    } // end of if main-repeate input

}) // end of add campaign goal

// add donate ambassador
$('.add-donate-ambassador').click(function (e) {

    if ($("#donate-ambsaador-form").valid()) 
    {

        // show loading
        $('.bg_spinner').show();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // get all id inputs
            var form                  = $('#form-donate-ambassador');
            var project_id_ambassador = form.find('#project_id_ambassador');

            var form_donate = $('#donate-ambsaador-form');
            var price_ambsaador    = form_donate.find('#price_ambsaador');
            var currency_ambsaador = form_donate.find('#currency_ambsaador');

            var type = $(this).attr('data-type');

            var url = $(this).attr('data-url');
            var id = $(this).attr('data-projectuserid');

            var formData = new FormData();

            formData.append('type'            , type);
            formData.append('project_id'      , project_id_ambassador.val());
            formData.append('price_ambsaador' , price_ambsaador.val());
            formData.append('currency_id'     , currency_ambsaador.val());
            formData.append('id'              , id);
            formData.append('cart'            , 'cart');

            // send request ajax
            $.ajax({

                url: url,
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,                
                success: function(){

                    // hide loading
                    $('.bg_spinner').hide();                    

                    $(".ambassador-modal").modal("hide");  
                                      
                    // Swal.fire({
                    //     icon: 'success',
                    //     title: success_donate,
                    //     showConfirmButton: false,
                    //     timer: 1500,
                    // });

                    location.href = result.checkoute_route;
                    

                }, // end of success
                error: function (jqXHR) {

                    // hide loading
                    $('.bg_spinner').hide();                    

                    var forrm_err = $('.ambasador-donate');
                    // error validation
                    validatePriceAmbsaador(forrm_err , price_ambsaador , jqXHR.responseJSON.errors.price_ambsaador);

                } //  end of error
                
            }); // end of ajax 


    } // end of if main-repeate input

}) // end of add donate ambassador


//////////////////////////////////////////////////////////// END: donate ////////////////////////////////



//////////////////////////////////////////////////////////// BEGIN: share sohial media //////////////////

$(document).ready(function(){
    var pageTitle   = document.title; //HTML page title
    var pageUrl     = $('.share-social').attr('data-href');
    
    $('.share-social button').click(function(event){
        var shareName = $(this).attr('data-network'); //get the first class name of clicked element
        switch(shareName) //switch to different links based on different social name
        {
            case 'facebook':
                OpenShareUrl('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(pageUrl) + '&amp;title=' + encodeURIComponent(pageTitle));
                break;
            case 'twitter':
                OpenShareUrl('http://twitter.com/home?status=' + encodeURIComponent(pageTitle + ' ' + pageUrl));
                break;
            case 'email':
                OpenShareUrl('mailto:?subject=' + pageTitle + '&body=Found this useful link for you : ' + pageUrl);
                break;
            case 'whatsapp':
                OpenShareUrl('whatsapp://send?text=' + pageUrl);
                break;                
        }
    
    });
    
    function OpenShareUrl(openLink){

        //Parameters for the Popup window
        winWidth    = 650; 
        winHeight   = 450;
        winLeft     = ($(window).width()  - winWidth)  / 2,
        winTop      = ($(window).height() - winHeight) / 2,
        winOptions   = 'width='  + winWidth  + ',height=' + winHeight + ',top='    + winTop    + ',left='   + winLeft;
        window.open(openLink,'Share This Link',winOptions); //open Popup window to share website.
        return false;

    }
    });

//////////////////////////////////////////////////////////// END: share sohial media ////////////////////
