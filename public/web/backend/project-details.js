$(document).ready(function(){

   // get lang
    var lang = $('.language').attr('lang');

    if (lang == 'ar') {

        var success_donate   = 'تم التبرع بنجاح';
        var success_compaign = 'تم انشاء الحملة بنجاح';
        var you_will_donate = 'سوف تقوم بالتبرع بمبلغ';
        var for_one_time = 'لمرة واحدة';
        var agree_support_error = 'من فضلك قم بالموافقة على عقد الدعم';
        var msg = 'لقد قمت بتقييم المشروع بالفعل';
    } else 
    {

        var success_donate   = 'Done Donate Successfully';
        var success_compaign = 'Done Create Campaign Successfully';
        var you_will_donate = 'You will donate';
        var for_one_time = 'form one time';
        var agree_support_error = 'Please Agree to Support Contarct';
        var msg = 'You have already rated this project';

    }

    $(document).on('click', '.rate_exist', function(){

        success(msg , 'success');
    });

    // // get donate price
    // var price = $('input[name="donate_price"]').val();
    // $('input[name="donate_price"]').change(function(){
        
    //     price = $(this).val();

    //     if(price < 100){

    //         // https://stackoverflow.com/questions/6302531/jquery-validation-for-more-than-min-value
    //         // $(this).rules('add', {
    //         //     required: true
    //         // });

    //         $(".donate_price-error").html('skvjdvbdbdsj');

    //         // $(".multi-checkboxes .custom-error").html('jsdfsbd');
    //     }
    //     else{
    //         alert('yes');
    //     }
    // });


    // get currency
    var currency = $('select[name="currency"]').children("option:selected").data('name');
    var currency_id = $('select[name="currency"]').children("option:selected").val();

    $('select[name="currency"]').change(function(){
        
        currency = $(this).children("option:selected").data('name');
        currency_id = $(this).children("option:selected").val();

    });

    // get donate_time

    var donate_time = $('input[name="donate_time"]').val();
    $('input[name="donate_time"]').change(function(){

        donate_time = $(this).val();

    });

    $('#donate_once_form').submit(function(e){

        e.preventDefault();

        var action = $(this).attr('action');
        var method = $(this).attr('method');


        var cost = price;
        var status = 'support';
        var type = 'once';
        var name_anonymouse = null;

        if($('#once_check_1').is(':checked')){

            name_anonymouse = 1;
        }
        else{
            name_anonymouse = 0;
        }

        // Make ajax request
        $.ajax({
            url: action,
            type: method,
            data:{
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    cost: cost,
                    status: status,
                    type: type,
                    name_anonymouse: name_anonymouse
                },
            dataType: 'JSON',
            success: function (data) {
                console.log(data);
                
            },
        });

        
    });

});