//select 
var lang = $('.language').attr('lang');
// var auth = $('#auth').val();
var min_cost = $('#cost_min').val();

if (lang == 'ar') {

    var invalid_cost = 'مبلغ التبرع يجب أن يكون '+min_cost+ ' على الأقل';
    var error = 'حدث خطأ ما، برجاء المحاولة فى وقت لاحق';
    var show_all = 'عرض الكل';
} 
else 
{
    var invalid_cost = 'Support Cost must be at least'+min_cost;
    var error = 'Problem occured, please try again later';
    var show_all = 'Show All';
}


//cart add
$(function () {
    var addToCart = $('.add-to-cart');
        addToCart.on('click', function () {
            // store cart to database
            var action = $(this).data('url');
            var method = $(this).data('method');

            var project_id = $(this).data('project_id');

            if($('#select-type-'+project_id).val() == 'select'){

                var type = $('#type-'+project_id).children("option:selected").val();
            }
            else if($('#select-type-'+project_id).val() == 'radio'){

                var type = $('input[name="donate_time"]:checked').val();
            }

            var currency_id = $('#currency-'+project_id).children("option:selected").val();
            var cost = $('#cost-'+project_id).val();


            // check if support cost is less than the minimum allowed cost
            if(parseInt(cost) < parseInt(min_cost)){

                error(invalid_cost , 'error');

                return ;

            }
        
            // Make ajax request to store cart
            $.ajax({
                url: action,
                type: method,
                data:{
                    "_token": $('meta[name="csrf-token"]').attr('content'), 
                    project_id: project_id,
                    type: type,
                    currency_id: currency_id,
                    cost: cost
                },
                dataType: 'HTML',
                success: function (data) {

                    data = JSON.parse(data);

                    if(data.status == 1){

                        $('.cart-count').each(function() {
                            var cartCountCont = $(this).find('.num');
                            var cartCountFind = parseInt(cartCountCont.text());
                            var total_cart = cartCountFind + 1;
                            cartCountCont.text(total_cart);            
                        }) // end of each    
                        
                        $('.more-slide-menu').text(show_all);
                        $('.more-slide-menu').attr('href', window.location.origin+'/cart');

                        general_alert(data.msg, data.html, data.icon);
                    }
                    else if(data.status == 2){ 

                        general_alert(data.msg, data.html, data.icon);
                    }

                },
                error: function(){

                    general_alert(error ,'', 'error');
                }
            });

            // Make ajax request to get latest cart
            $.ajax({
                url: window.location.origin+'/get-cart-items',
                type: 'GET',
                data:{
                    "_token": $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'HTML',
                success: function (data) {

                    $('#cart_holder').html(data);

                }
            });

    });
});


// change total cart price according to single item price increase or decreas-
$(document).on("click",".numb-control",function(){

    // update total_price
    updateCartPrice();
});

$(document).on("change", ".cart-table .no_apperance_number", function(){

    // update total_price
    updateCartPrice();

});

