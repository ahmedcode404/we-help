var tel_url = $('#tel_url').val();

var phone_number = window.intlTelInput(document.querySelector("#phone"), {
    preferredCountries: ["gb", "sa"],
    separateDialCode: true,
    hiddenInput: "phone",
    formatOnDisplay: false,
    utilsScript: tel_url
});