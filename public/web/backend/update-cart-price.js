function updateCartPrice(){

    var sum = 0;

    $('.cart-table .no_apperance_number').each(function(){
        
        if($(this).val() != ''){

            var cart_id = $(this).data('id')
            
            $('#cart_cost'+cart_id).text($(this).val());

            sum += parseFloat($(this).val());
        }

    });

    $('#cart_total').text(sum);
}