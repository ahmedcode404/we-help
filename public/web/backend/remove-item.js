$(document).ready(function(){

    var lang = $('.language').attr('lang');
    
    if (lang == 'ar') {
        
        var error = 'حدث خطأ ما، برجاء المحاولة فى وقت لاحق';
        var remove_item = 'هل تريد حذف هذا العنصر؟';
        var yes = 'نعم';
        var no = 'لا';
    
    } 
    else 
    {
        var error = 'Problem occured, please try again later';
        var remove_item = 'Do you want to remove this item?';
        var yes = 'Yes';
        var no = 'No';
    }


    // remove item
    $('.remove-btn').click(function (e) {

        var self = $(this);
        
        e.preventDefault();


        var action = $(this).data('action');
        var method = $(this).data('method');
        var model = $(this).data('model');

        Swal.fire({
            title: remove_item,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: yes,
            cancelButtonText: no,
        }).then((result) => {
            if (result.isConfirmed) {

            // Make ajax request
            $.ajax({
                url: action,
                type: method,
                data:{
                    "_token": $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'HTML',
                success: function (data) {

                    data = JSON.parse(data);

                    if(data.status == 1){

                        general_alert(data.msg, data.html, data.icon);

                        self.closest(".remove-item").remove();

                        var total_cart;

                        if(model == 'cart'){

                            // reduce cart count
                            $('.cart-count').each(function() {
                                var cartCountCont = $(this).find('.num');
                                var cartCountFind = parseInt(cartCountCont.text());
                                total_cart = cartCountFind - 1;
                                cartCountCont.text(total_cart);            
                            }) // end of each 

                            $('.cart_items').text(total_cart);

                            // update total_price
                            updateCartPrice();
                        }
                    }
                    else{
                        general_alert(data.msg, data.html, data.icon);
                    }

                },
                error: function(){

                    general_alert(error ,'', 'error');
                }
            });


            //   Swal.fire({
            //     icon: 'success',
            //     title: 'Delete successfully',
            //     showConfirmButton: false,
            //     timer: 1000,
            //   });
        
            }
        });
    });
});