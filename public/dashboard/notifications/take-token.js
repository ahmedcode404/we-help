$(document).ready(function () {
    
    var firebaseConfig = {
        apiKey: "AIzaSyCMN71jZ2IiJbCv9K-9eA-hnVOqI6k5okY",
        authDomain: "we-help-98457.firebaseapp.com",
        projectId: "we-help-98457",
        storageBucket: "we-help-98457.appspot.com",
        messagingSenderId: "418471964638",
        appId: "1:418471964638:web:f5768ae112e44b9c75f74f",
        measurementId: "G-RWK9VF5P3Y"   
    };
    firebase.initializeApp(firebaseConfig);
    const messaging=firebase.messaging();
    
    function IntitalizeFireBaseMessaging() {
        messaging
            .requestPermission()
            .then(function () {
                console.log("Notification Permission");
                return messaging.getToken();
            })
            .then(function (token) {
                console.log("Token : "+token);
    
                    console.log(token);
    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    }); // end of ajaxsetup
    
                    var url = $('#url-save-token').val();
    
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            token: token
                        },
                        dataType: 'JSON',
                        success: function (response) {
                            // alert('Token saved successfully.');
                        },
                        error: function (err) {
                            console.log('User Chat Token Error'+ err);
                        },
                    }); // end of ajax
    
            })
            .catch(function (reason) {
                console.log(reason);
            });
    }
    
    messaging.onMessage(function (payload) {
        console.log(payload);

        var a = parseInt($(".count-notify-charity").text());
        $('.count-notify-charity').empty();
        $('.count-notify-charity').append(a+1);

        const notificationOption={
            body:payload.notification.body,
            icon:payload.notification.icon
        };

    
        if(Notification.permission==="granted"){
            var notification=new Notification(payload.notification.title,notificationOption);
            
            notification.onclick=function (ev) {
                ev.preventDefault();
                window.open(payload.notification.click_action,'_blank');
                notification.close();
            }
        }
    
    });
    messaging.onTokenRefresh(function () {
        messaging.getToken()
            .then(function (newtoken) {

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                }); // end of ajaxsetup

                var url = $('#url-save-token').val();

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        token: newtoken
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        
                        // alert('Token saved successfully.');
                    },
                    error: function (err) {

                        console.log('User Chat Token Error'+ err);
                    },
                }); // end of ajax

            })
            .catch(function (reason) {
                console.log(reason);
            })
    })
    
    IntitalizeFireBaseMessaging();        

})