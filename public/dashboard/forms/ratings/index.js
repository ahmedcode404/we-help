$(document).ready(function(){

    if($('#lang').val() == 'ar'){
        var are_you_sure = 'هل أنت متأكد من الحذف؟';
        var yes = 'نعم';
        var no = 'لا';
        var success = 'تم الحذف بنجاح';
        var changed = 'تم التعديل بنجاح';
        var error = 'حدث خطأ أثناء التعديل، برجاء المحاولة مرة أخرى لاحقاً';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البريد التالى: info@jaadara.com';
        var permission = 'ليست لديك صلاحية';
    }
    else{
        var are_you_sure = 'Are You Sure?';
        var yes = 'Yes';
        var no = 'No';
        var success = 'Comment Deleted Successfully';
        var changed = 'Updated Successfully';
        var error = 'Something Went Wrong, Please Try Again Later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
        var permission = 'You Do not Have Permission';
    }

    // delete comment
    $('.delete-comment').click(function(e) {

        var id = $(this).data(id);

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        });

        swalWithBootstrapButtons.fire({
            title: are_you_sure,
            icon: 'question',
            showCancelButton: true,
            confirmButtonText: yes,
            cancelButtonText: no,
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: $(this).data('action'),
                    type: $(this).data('method'),
                    dataType: 'html',
                    data: {"_token": $('meta[name="csrf-token"]').attr('content')},
                    success: function (data) {
                
                        data = JSON.parse(data);
        
                        if(data.data == 1){
                            
                            Swal.fire({
                                title: success,
                                type: 'success',
                                timer: 1500,
                                showCancelButton: false,
                                showConfirmButton: false,
                            });

                            $('#delete_btn'+id.id).html('');
                        }
                        else if(data.data == 2){
        
                            Swal.fire({
                                title: permission,
                                type: 'error',
                                timer: 1500,
                                showConfirmButton: false,
                                showCancelButton: false,
                            });
                        }
                        else{
                            Swal.fire({
                                title: error,
                                html: html,
                                type: 'error',
                                timer: 2000,
                                showCancelButton: false,
                                showConfirmButton: false,
                            });
                        }
        
                    },
                    error: function(){
        
                        Swal.fire({
                            title: error,
                            html: html,
                            type: 'error',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                });

            }
            else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });

    });

    // show / hide ratings from website
    // delete comment
    $('.show_rate').change(function(e) {

        var action = $(this).data('action');
        var method = $(this).data('method');
        var value = $(this).val();

        $.ajax({
            url: action,
            type: method,
            dataType: 'html',
            data: {"_token": $('meta[name="csrf-token"]').attr('content'), value: value},
            success: function (data) {
        
                data = JSON.parse(data);

                if(data.data == 1){
                    
                    Swal.fire({
                        title: changed,
                        type: 'success',
                        timer: 1500,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });

                }
                else if(data.data == 2){

                    Swal.fire({
                        title: permission,
                        type: 'error',
                        timer: 1500,
                        showConfirmButton: false,
                        showCancelButton: false,
                    });
                }
                else{
                    Swal.fire({
                        title: error,
                        html: html,
                        type: 'error',
                        timer: 2000,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                }

            },
            error: function(){

                Swal.fire({
                    title: error,
                    html: html,
                    type: 'error',
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false,
                });
            }
        });

    });
});