$(document).ready(function(){

    if($('#lang').val() == 'ar'){
        var success = 'تم التعديل بنجاح';
        var error = 'حدث خطأ أثناء التعديل، برجاء المحاولة مرة أخرى لاحقاً';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البريد التالى: info@jaadara.com';
        var permission = 'ليست لديك صلاحية';
    }
    else{
        var success = 'Item Updated Successfully';
        var error = 'Something Went Wrong, Please Try Again Later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
        var permission = 'You Do not Have Permission';
    }
    
    $('.block').click(function(){

        var action = $(this).data('action');
        var method = $(this).data('method');

        // Make ajax request
        $.ajax({
            url: action,
            type: method,
            data:{"_token": $('meta[name="csrf-token"]').attr('content')},
            dataType: 'html',
            success: function (data) {
                
                data = JSON.parse(data);

                if(data.data == 'success'){
                    
                    Swal.fire({
                        title: success,
                        type: 'success',
                        timer: 1500,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });

                    setInterval(function(){
                        window.location.reload();
                    }, 1500);
                }
                else if(data.data == 2){

                    Swal.fire({
                        title: permission,
                        type: 'error',
                        timer: 1500,
                        showConfirmButton: false,
                        showCancelButton: false,
                    });
                }
                else{
                    Swal.fire({
                        title: error,
                        html: html,
                        type: 'error',
                        timer: 2000,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                }

            },
            error: function(){

                Swal.fire({
                    title: error,
                    html: html,
                    type: 'error',
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false,
                });
            }
        });
    });
});