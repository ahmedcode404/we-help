$(document).ready(function(){

    // Validate form
    if ($('#support').length) {
        $('#support').validate({
            rules: {
                to_project_id: "required",
                cost: "required",
            },
        });
    }

});