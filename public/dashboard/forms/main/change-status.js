$(document).ready(function(){

    if($('#lang').val() == 'ar'){
        var success = 'تم التعديل بنجاح';
        var error = 'حدث خطأ أثناء التعديل، برجاء المحاولة مرة أخرى لاحقاً';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البريد التالى: info@jaadara.com';
        var warning = 'تعذر إشعار الجمعية عبر البريد الإلكترونى';
    }
    else{
        var success = 'Item Updated Successfully';
        var error = 'Something Went Wrong, Please Try Again Later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
        var warning = 'Unable to Notify Charity via Email';
    }

    // change status
    $('.save_status').click(function(){
        
        var action = $(this).data('action');
        var method = $(this).data('method');
        var model  = $(this).data('model');
        var id     = $(this).data('id');

        if(model == 'report'){
            var status = $('#change_report_status-'+id).val();
            var notes  = $('#report_notes-'+id).val();    
        }
        else if(model == 'edit_request_project' || model == 'edit_request_charity'){
            var status = $('#change_edit_status-'+id).val();
            var notes  = $('#edit_notes-'+id).val();
        }
        else if(model == 'fin_request'){

            var status = $('#change_fin_status-'+id).val();
            var notes  = $('#fin_notes-'+id).val();
        }
        else if(model == 'charity'){
            var status = $('#change_charity_status-'+id).val();
            var notes  = $('#charity_notes-'+id).val();
        }
        else if(model == 'project'){
            var status = $('#change_project_status-'+id).val();
            var notes  = $('#project_notes-'+id).val();
        } 
        else if(model = 'financial_request'){
            var status = $('#change_request_status-'+id).val();
            var notes  = $('#request_notes-'+id).val();
        }
        
        
        $('.processing-div-'+ id).css("display","inline-block").show();

        // Make ajax request
        $.ajax({
            url: action,
            type: method,
            data:{"_token": $('meta[name="csrf-token"]').attr('content') , id: $(this).data('id'), status: status, notes: notes, model: model},
            dataType: 'html',
            success: function (data) {

                data = JSON.parse(data);

                if(data.data == 'success'){
                    
                    Swal.fire({
                        title: success,
                        type: 'success',
                        timer: 1500,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });

                    $('#current_status-'+id).text(getStatusText(status));
                    
                    if(status == 'waiting'){

                        $('#current_status-'+id).removeAttr('class');
                        $('#current_status-'+id).addClass('badge badge-light-warning');
                    }
                    else if(status == 'approved'){

                        $('#current_status-'+id).removeAttr('class');
                        $('#current_status-'+id).addClass('badge badge-light-success');
                    }
                    else if(status == 'rejected'){

                        $('#current_status-'+id).removeAttr('class');
                        $('#current_status-'+id).addClass('badge badge-light-danger');
                    }
                    else if(status == 'hold'){

                        $('#current_status-'+id).removeAttr('class');
                        $('#current_status-'+id).addClass('badge badge-light-info');
                    }
                }
                else{
                    Swal.fire({
                        title: error,
                        html: html,
                        type: 'error',
                        timer: 2000,
                        showCancelButton: false,
                        showConfirmButton: false,
                    });
                }

                $('.processing-div-'+ id).css("display","none").hide();

                if(model == 'edit_request_project' || model == 'edit_request_charity'){
                    
                    $('#req_status-'+id).modal('hide');
                }
                else{

                    $('#'+model+'-'+id).modal('hide');
                }

            },
            error: function(){

                Swal.fire({
                    title: warning,
                    html: html,
                    type: 'warning',
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false,
                });

                $('.processing-div-'+ id).css("display","none").hide();

                if(model == 'edit_request_project' || model == 'edit_request_charity'){
                    
                    $('#req_status-'+id).modal('hide');
                }
                else{

                    $('#'+model+'-'+id).modal('hide');
                }

                $('#current_status-'+id).text(getStatusText(status));
                    
                if(status == 'waiting'){

                    $('#current_status-'+id).removeAttr('class');
                    $('#current_status-'+id).addClass('badge badge-light-warning');
                }
                else if(status == 'approved'){

                    $('#current_status-'+id).removeAttr('class');
                    $('#current_status-'+id).addClass('badge badge-light-success');
                }
                else if(status == 'rejected'){

                    $('#current_status-'+id).removeAttr('class');
                    $('#current_status-'+id).addClass('badge badge-light-danger');
                }
                else if(status == 'hold'){

                    $('#current_status-'+id).removeAttr('class');
                    $('#current_status-'+id).addClass('badge badge-light-info');
                }
            }
        });

    });
});

function getStatusText(status){

    if($('#lang').val() == 'ar'){

        if(status == 'waiting'){
            return 'قيد الإنتظار';
        }

        if(status == 'approved'){
            return 'معتمد';
        }

        if(status == 'rejected'){
            return 'مرفوض';
        }

        if(status == 'hold'){
            return 'معلق';
        }
    }
    else{
        if(status == 'waiting'){
            return 'Waiting';
        }

        if(status == 'approved'){
            return 'Approved';
        }

        if(status == 'rejected'){
            return 'Rejected';
        }

        if(status == 'hold'){
            return 'Holding';
        }
    }
}


