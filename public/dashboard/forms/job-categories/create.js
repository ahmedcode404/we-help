$(document).ready(function(){

    // Validate form
    if ($('#job-categories').length) {
        $('#job-categories').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
                nation_id: "required"
            }
        });
    }

});