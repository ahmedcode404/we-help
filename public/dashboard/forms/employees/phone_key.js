

$(document).ready(function () {
    
    var tel_url = $('#tel_url').val();
    var input = window.intlTelInput(document.querySelector("#phone"), {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "phone",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: tel_url
    });

    // $("#form-employee").on("submit", function() {

    //     var country_code = $(this).find('.iti__selected-dial-code').text();
    
    //     $('input[name="phone_key"]').val(country_code);
    
    // });  
    
    var lang = $('#lang').val();
    if(lang == 'ar'){
        var required_msg = 'هذا الحقل إلزامى';
        var email_msg = 'من فضلك أدخل بردي إلكترونى صحيح';
        var emailtrue_msg = 'يجب ان يحتوي البريد الالكتروني علي نطاقات مثال : com , .net , .org , .info. , الي اخره....';
        var save = 'حفظ';
        var success_add = 'تمت الإضافة بنجاح';
        var success_edit = 'تم التعديل بنجاح';
        var failed = 'حدث خطأ ما';
        var general_error = 'يوجد خطأ ما، برجاء مراجعة المدخلات';
    }
    else{
        var required_msg = 'This feild is required';
        var email_msg = 'Please enter a valid email';
        var emailtrue_msg = 'The email must contain domains such as: com, .net, .org, .info. , Etc....';
        var save = 'Save';
        var success_add = 'Added successfully';
        var success_edit = 'Updated successfully';
        var failed = 'An error has occured';
        var general_error = 'There is something wrong, please check the inputs';
    }

    // validate email
    $.validator.addMethod("emailtrue", function (value, element) {
        return this.optional(element) || value == value.match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
    });
    
    
    // Validate form
    if ($('#form-employee').length) {
        $('#form-employee').validate({
            rules: {
                name_employee_ar: "required",
                name_employee_en: "required",
                jop_category_id: "required",
                nation_number: "required",
                country_id: "required",
                region: "required",
                street: "required",
                mail_box: "required",
                contract_start_date: "required",
                contract_end_date: "required",
                degree_id: "required",
                nationality_id: "required",
                nation_id: "required",
                permission: "required",
                job_id: "required",
                city_id: "required",
                postal_code: {
                    required: true,
                    minlength:5,
                    min:0,

                },
                unit_number: {
                    required: true,
                    number: true,
                    step:1,
                    min:0,
                },
                phone: {
                    required: true,
                    digits:true,
                },
                email: {
                    required: true,
                    email: true,
                    emailtrue : true,
                },
                
            },
            messages: {
                email:{
                    required: required_msg,
                    email: email_msg,
                    emailtrue : emailtrue_msg
                },
            }
        });
    }



    // Submit form
    $(document).on('click', '.employee_form_button', function(){

        var self = $(this).parent().prev('.general_error');
        var form_submit = $('.employee_form');
        form_submit.valid();

        if(form_submit.valid()){

            $('#loading').html(`
                <button type="button" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 employee_form_button" disabled><div class="spinner-border" style="display:inline-block;" role="status"></div>`+save+`</button>
            `);

            $(document).find('.error').html("");
            $(document).find('.error').hide();
            var edit = $(document).find('#edit').val();

            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');

            formData.append('phone', input.getNumber());

            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 employee_form_button">`+save+`</button>
                    `);

                    if(response.status == 1 && edit == "true"){

                        Swal.fire({
                            title: success_edit,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                        
                    }
                    else if(response.status == 1 && edit != "true"){

                        Swal.fire({
                            title: success_add,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                    else if(response.status == 0){

                        Swal.fire({
                            title: failed,
                            type: 'error',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
        
                    // check for redirect
                    if(response.redirect){

                        setTimeout(function (){

                            window.location.href = response.redirect;
                            
                        });
                    }
        
                    // check for reload
                    if(response.reload && response.reload == 1){

                        setTimeout(function (){

                            location.reload();

                        }, 2000);
                    }

                },
                error: function (errors) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary mb-1 mb-sm-0 mr-0 mr-sm-1 employee_form_button">`+save+`</button>
                    `);
                    
                    for (var k in errors.responseJSON.errors) {
                        $(document).find('.error-'+k).show();
                        $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                    }

                    self.show();
                    self.text(general_error);
                }
            });
        }
        
    });

})
