$(document).ready(function() {

    $('#jop_category_id').change(function() {

            var id = $(this).val();

            var url = $(this).data('url');
            
            $('.append-jop').html("");


            $.ajax({
                type: 'GET',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: url,
                data: { 'id': id },
                dataType: 'html',
                success: function(result) {

                        $('.append-jop').html(result);

                    } // end of success

            }); // end of ajax


        }) // end of other name

});