$(document).ready(function () {

    // Date picker
    // var datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }

    $('.check-all-permission').click(function () {

        var role_id = $(this).data('id');

        if (this.checked) {

            document.querySelector(".permission-" + role_id).checked = localStorage.checked == true
            $(".permission-" + role_id).prop("checked", true);
            $(".permission-" + role_id).attr("checked", "checked");

        } else {
            document.querySelector(".permission-" + role_id).checked = localStorage.checked == false
            $("#role-" + role_id).prop("checked", false);
            $(".permission-" + role_id).removeAttr("checked", "checked");            
            $(".permission-" + role_id).prop("checked", false);
            
        }

    });
   
       $(document).on("change",".one-permission",function () {
        var self2 = $(this).closest(".tab-pane"),
        self = $(this).closest(".tab-pane").find(".one-permission");
        if (self.not(":checked").length === 0){
            self2.find(".check-all-permission").attr("checked", "checked");
            self2.find(".check-all-permission").prop("checked", true);
        }else{
            self2.find(".check-all-permission").attr("checked", "");
            self2.find(".check-all-permission").prop("checked", false); 
        }
 });
            

    // validate agreement form
    if ($('#agreement_form').length) {
        $('#agreement_form').validate({
            rules: {
                name_ar: "agreement_approve"
            }
        });
    }
    
})
