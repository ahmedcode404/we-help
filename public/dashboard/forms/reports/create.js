$(document).ready(function(){
    
    // Validate form
    if ($('.create_report_form').length) {
        $('.create_report_form').validate({
            ignore: [],
            rules: {
                report_ar: {
                    required: function(){
                        return CKEDITOR.instances.report_ar.updateElement();
                    }
                },
                report_en: {
                    required: function(){
                        return CKEDITOR.instances.report_en.updateElement();
                    }
                },
                status: "required",
                vedio: "required",
                images: "reqiored"
            },

        });
    }


    var lang = $('#lang').val();
    if(lang == 'ar'){

        var success_add = 'تمت الإضافة بنجاح';
        var success_edit = 'تم التعديل بنجاح';
        var failed = 'حدث خطأ ما';
        var save = 'حفظ';
        var general_error = 'يوجد خطأ ما، برجاء مراجعة المدخلات';
    }
    else{
        var success_add = 'Added successfully';
        var success_edit = 'Updated successfully';
        var failed = 'An error has occured';
        var save = 'Save';
        var general_error = 'There is something wrong, please check the inputs';
    }


    $(document).on('click', '.submit_create_report_form_button', function(){

        var self = $(this).parent().prev('.general_error');
        var form_submit = $('.create_report_form');
        form_submit.valid();

        if(form_submit.valid()){

            $('#loading').html(`
                <button class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_create_report_form_button" disabled><div class="spinner-border" style="display:inline-block;" role="status"></div>`+save+`</button>
            `);

            $(document).find('.error').html("");
            $(document).find('.error').hide();
            var edit = $(document).find('#edit').val();

            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');

            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#loading').html(`
                        <button class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_create_report_form_button">`+save+`</button>
                    `);

                    if(response.status == 1 && edit == "true"){

                        Swal.fire({
                            title: success_edit,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                        
                    }
                    else if(response.status == 1 && edit != "true"){

                        Swal.fire({
                            title: success_add,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                    else if(response.status == 0){

                        Swal.fire({
                            title: failed,
                            type: 'error',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
        
                    // check for redirect
                    if(response.redirect){

                        setTimeout(function (){

                            window.location.href = response.redirect;
                            
                        });
                    }
        
                    // check for reload
                    if(response.reload && response.reload == 1){

                        setTimeout(function (){

                            location.reload();

                        }, 2000);
                    }

                },
                error: function (errors) {

                    $('#loading').html(`
                        <button class="btn btn-primary mb-sm-0 mr-0 mr-sm-1 submit_create_report_form_button">`+save+`</button>
                    `);
                    
                    for (var k in errors.responseJSON.errors) {
                        $(document).find('.error-'+k).show();
                        $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                    }

                    self.show();
                    self.text(general_error);
                }
            });
        }
        
    });



    function _(el) {
        return document.getElementById(el);
       }
       
    
       function uploadFile() {
        
            var file = _("vedio").files[0];
            var formdata = new FormData();
            formdata.append("vedio", file);
            var ajax = new XMLHttpRequest();
            ajax.upload.addEventListener("progress", progressHandler, false);
            ajax.addEventListener("load", completeHandler, false);
            ajax.addEventListener("error", errorHandler, false);
            ajax.addEventListener("abort", abortHandler, false);
            ajax.open("POST", "file_upload_parser.php");
            ajax.send(formdata);
       }
       
       function progressHandler(event) {
          $("#progressBar").show().css("display","block");;
        _("loaded_n_total").innerHTML = "تم تحميل " + event.loaded + " من " + event.total;
        var percent = (event.loaded / event.total) * 100;
        _("progressBar").value = Math.round(percent);
        _("status_up").innerHTML = "  جاري التحميل " + Math.round(percent) + "%";
       }
       
       function completeHandler(event) {
          $("#progressBar").hide().css("display","none");
          _("status_up").innerHTML = "  تم التحميل ";
        _("progressBar").value = 0; //wil clear progress bar after successful upload
       }
       
       function errorHandler(event) {
        _("status_up").innerHTML = "فشل التحميل";
       }
       
       function abortHandler(event) {
        _("status_up").innerHTML = "تم إلغاء التحميل";
       }
       
       $("#vedio").change(function(){
          uploadFile()
       });



});