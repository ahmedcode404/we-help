$(document).ready(function(){

    // Validate form
    if ($('#create_feature').length) {
        $('#create_feature').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
                parent_id: "required",
            }
        });
    }
    
}); 

