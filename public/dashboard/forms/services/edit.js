$(document).ready(function(){

    $('#click_edit_service').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form       = $('#edit_service');

        // show loadinng
        onFormSubmit(form);      

        var item      = form.find('#item');
        var name_ar   = form.find('#name_ar');
        var name_en   = form.find('#name_en');
        var type      = form.find('#type');

        // route post request
        var url = $(this).attr('url');
        var redirect = $(this).attr('redirect');

        var formData = new FormData();

        // append version saudia
        formData.append('item'      , item.val());
        formData.append('name_ar'   , name_ar.val());
        formData.append('name_en'   , name_en.val());
        formData.append('type'      , type.val());

        // request method
        formData.append('_method'    , 'PATCH');

        var stauts = 'edit-service';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,   
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            cache: false,                         
            success: function(result){

                onFormSuccess(form, stauts, true)

                setTimeout(() => {

                    window.location.replace(redirect);
                    
                }, 1500);
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);
                
                // error validation 
                selectFieldError(form , type      , jqXHR.responseJSON.errors.type);
                textFieldError(form   , name_ar   , jqXHR.responseJSON.errors.name_ar);
                textFieldError(form   , name_en   , jqXHR.responseJSON.errors.name_en);

            } //  end of error

             
        }); // end of ajax

    }); // end of click button

}); // end of document ready

