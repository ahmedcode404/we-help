$(document).ready(function(){

    // Validate form
    if ($('#create_service').length) {
        $('#create_service').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
                charity_category_id: "required",
            }
        });
    }
    
}); 

