$(document).ready(function(){

    // Validate form
    if ($('#forgot-password-form').length) {
        $('#forgot-password-form').validate({
            rules: {
                role: "required",
                email: "required",
            },
        });
    }
});