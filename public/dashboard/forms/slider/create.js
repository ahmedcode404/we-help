$(document).ready(function(){

    // Validate form
    if ($('#slider').length) {
        $('#slider').validate({
            rules: {
                path: "required",
                text: "required",
                appearance: "required",
            }
        });
    }

});