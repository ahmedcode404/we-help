$(document).ready(function(){

    // Validate form
    if ($('#create_sponser').length) {
        $('#create_sponser').validate({
            rules: {
                sponser_name: "required",
                sponser_work_field_id: "required",
            },

            // messages: {
            //     path: "من فضلك قم بتحميل صورة",
            //     text: "من فضلك أدخل نص توضيحى",
            //     appearance: "من فضلك إختر مكان العرض",
            // }
        });
    }

});