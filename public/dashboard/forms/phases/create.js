$(document).ready(function(){

    if($('#lang').val() == 'ar'){
        var error = 'حدث خطأ، برجاء المحاولة مرة أخرى لاحقاً';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البريد التالى: info@jaadara.com';
    }
    else{
        var error = 'Something Went Wrong, Please Try Again Later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
    }

    // Date picker
    // datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }

    $('#start_date').change(function(){

        if($('#end_date').val() != ''){

            const start_date = new Date($(this).val());
            const end_date = new Date($('#end_date').val());

            // if(start_date.getDate() > end_date.getDate()){
            //     alert('wrong dates');
            // }

            const diffTime = Math.abs(end_date - start_date);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

            $('#duration').val(diffDays);
            
            // var start_date = $(this).val();
            // var end_date = $('#end_date').val();

            // var action = $(this).data('action');
            // var method = $(this).data('method');

            // Make ajax request
            // $.ajax({
            //     url: action,
            //     type: method,
            //     data:{"_token": $('meta[name="csrf-token"]').attr('content') , start_date: start_date, end_date: end_date},
            //     dataType: 'html',
            //     success: function (data) {
                    
            //         // data = JSON.parse(data);

            //         console.log(data);

            //         if(data.data == 'success'){
                        
            //             $('#duration').val(data.duration);
            //         }
            //         else{
            //             $('#duration').val(0);
            //         }

            //     },
            //     error: function(){

            //         Swal.fire({
            //             title: error,
            //             html: html,
            //             type: 'warning',
            //             timer: 2000,
            //             showCancelButton: false,
            //             showConfirmButton: false,
            //         });
            //     }
            // });
        }
    });

    $('#end_date').change(function(){

        if($('#start_date').val() != ''){

            const start_date = new Date($('#start_date').val());
            const end_date = new Date($(this).val());

            // if(start_date.getDate() > end_date.getDate()){
            //    alert('wrong dates');
            // }
            const diffTime = Math.abs(end_date - start_date);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

            $('#duration').val(diffDays);
            
            // var start_date = $('#start_date').val();
            // var end_date = $(this).val();

            // var action = $(this).data('action');
            // var method = $(this).data('method');

            // Make ajax request
            // $.ajax({
            //     url: action,
            //     type: method,
            //     data:{"_token": $('meta[name="csrf-token"]').attr('content') , start_date: start_date, end_date: end_date},
            //     dataType: 'json',
            //     success: function (data) {
                    
            //         // data = JSON.parse(data);
            //         console.log(data);

            //         if(data.data == 'success'){
                        
            //             $('#duration').val(data.duration);
            //         }
            //         else{
            //             $('#duration').val(0);
            //         }

            //     },
            //     error: function(){

            //         Swal.fire({
            //             title: error,
            //             html: html,
            //             type: 'warning',
            //             timer: 2000,
            //             showCancelButton: false,
            //             showConfirmButton: false,
            //         });
            //     }
            // });
        }
    });

    // Validate form
    if ($('#create_phase').length) {
        $('#create_phase').validate({
            ignore: [],
            rules: {
                order: "required",
                name_ar: "required",
                name_en: "required",
                desc_ar: {
                    required: function(){
                        return CKEDITOR.instances.desc_ar.updateElement();
                    }
                },
                desc_en: {
                    required: function(){
                        return CKEDITOR.instances.desc_en.updateElement();
                    }
                },
                start_date: "required",
                end_date: "required",
                cost: {
                    required: true,
                    number: true,
                    min: 1,
                    step: 0.1
                },
                cost_details_ar: {
                    required: function(){
                        return CKEDITOR.instances.cost_details_ar.updateElement();
                    }
                },
                cost_details_en: {
                    required: function(){
                        return CKEDITOR.instances.cost_details_en.updateElement();
                    }
                },
                output_ar: {
                    required: function(){
                        return CKEDITOR.instances.output_ar.updateElement();
                    }
                },
                output_en: {
                    required: function(){
                        return CKEDITOR.instances.output_en.updateElement();
                    }
                },
            },

        });
    }
    
});