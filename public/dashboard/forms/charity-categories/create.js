$(document).ready(function(){

    // Validate form
    if ($('#charity-categories').length) {
        $('#charity-categories').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
                nation_id: "required",
                color: "required",
                superadmin_ratio: {
                    required: true,
                    number: true,
                    step:0.1,
                    min:0
                }
            },
        });
    }

});