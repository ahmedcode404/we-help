$(document).ready(function(){

    // Validate form
    if ($('#create_city').length) {
        $('#create_city').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
                category: "required",
                nation_id: "required",
                parent_id: {
                    required: "#city:checked"
                },
            },
        });
    }

    if($('input[name="category"]:checked').val() == 'country'){

        $('#parent_id').prop('disabled', true);
    }
    else{

        $('#parent_id').prop('disabled', false);
    }
    

    $('input[name="category"]').click(function(){

        if($(this).val() == 'country'){

            $('#parent_id').prop('disabled', true);
        }
        else{

            $('#parent_id').prop('disabled', false);
        }
       
    });

});