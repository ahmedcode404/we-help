$(document).ready(function(){

    // Validate form
    if ($('#currencies_form').length) {
        $('#currencies_form').validate({
            rules: {
                name_currency_ar: "required",
                name_currency_en: "required",
                symbol: "required",
                icon: "required",
            },
        });
    }

});