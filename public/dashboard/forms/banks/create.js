$(document).ready(function(){

    // Validate form
    if ($('#banks-form').length) {
        $('#banks-form').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
                nation_id: "required",
                city_id: "required",
                address: "required",
                phone: "required"
            },
        });
    }

});