// $(document).ready( function( ) {   

    // Date picker
    // var datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }

    // phone
    var tel_url = $('#tel_url').val();
    var input = window.intlTelInput(document.querySelector("#phone"), {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "phone",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: tel_url
    });

    // mobile
    var input_mobile = window.intlTelInput(document.querySelector("#mobile"), {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "mobile",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: tel_url
    });


    // bank_phone
    var bank_phone = window.intlTelInput(document.querySelector("#bank_phone"), {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "bank_phone",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: tel_url
    });

    // var phone = input.getNumber(intlTelInputUtils.numberFormat.E164);
    // var input_mobile2 = input_mobile.getNumber(intlTelInputUtils.numberFormat.E164);
    // var bank_phone2 = bank_phone.getNumber(intlTelInputUtils.numberFormat.E164);

    // console.log(phone);
    // console.log(input_mobile);
    // console.log(bank_phone);


    var select = $('.select2');

    select.each(function () {
        var $this = $(this);
        $this.wrap('<div class="position-relative"></div>');
        $this.select2({
          // the following code is used to disable x-scrollbar when click in select input and
          // take 100% width in responsive also
          dropdownAutoWidth: true,
          width: '100%',
          dropdownParent: $this.parent()
        });
    });

    //add multi images
    $(document).on("click", ".img-btn-success",function(){ 
            var app = $(this).parents(".image_increment").next(".image_clone").html();
            $(this).closest(".image_increment").not(".hidden").append(app);
    });

    $("body").on("click",".img-btn-danger",function(){ 
        $(this).closest(".control-group").remove();
    });

    $('#international_member').change(function(){
        
        if($(this).is(":checked")){

            $('#international_name_container').removeClass('hidden');
            CKEDITOR.replace( 'international_name_ar' );
            CKEDITOR.replace( 'international_name_en' );
        }
        else{
            
            $('#international_name_container').addClass('hidden');
            $('#international_name_ar').text('');
            $('#international_name_ar').html('');
            $('#international_name_en').text('');
            $('#international_name_en').html('');
        }
    });    
    

    var lang = $('#lang').val();

    if(lang == 'ar'){
        var required_msg = 'هذا الحقل إلزامى';
        var email_msg = 'من فضلك أدخل بردي إلكترونى صحيح';
        var emailtrue_msg = 'يجب ان يحتوي البريد الالكتروني علي نطاقات مثال : com , .net , .org , .info. , الي اخره....';
    }
    else{
        var required_msg = 'This feild is required';
        var email_msg = 'Please enter a valid email';
        var emailtrue_msg = 'The email must contain domains such as: com, .net, .org, .info. , Etc....';
    }
    
    // validate email
    $.validator.addMethod("emailtrue", function (value, element) {
        return this.optional(element) || value == value.match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
    });


    // validate form
    if ($('.submit_form').length) {

        $('.submit_form').validate({
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
        
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top
                }, 1000);
            },
            ignore: [],
            rules:{
                name:"required",
                email: {
                    required: true,
                    email: true,
                    emailtrue : true,
                },
                phone_num: {
                    required: true,
                    digits: true
                },
                mobile_num: {
                    required: true,
                    digits: true
                },
                address_ar: "required",
                address_en: "required",
                logo: "required",
                charity_category_id: "required",
                type: "required",
                status: "required",
                country: "required",
                city: "required",


                license_number: {
                    required: true,
                    min: 1,
                    number: true,
                },
                license_start_date: "required",
                license_end_date: "required",
                establish_date: "required",
                license_file: "required",
                branches_num: {
                    required: true,
                    min: 1,
                    number: true,
                },
                members_num: {
                    required: true,
                    min: 1,
                    number: true,
                },
                contract_date: "required",
                contract_after_sign: "required",

                attach: "required",
                year_report: "required",
                years: "required",
                internal_image: "required",



                representer_name_ar: "required",
                representer_name_en: "required",
                representer_title_ar: "required",
                representer_title_en: "required",
                representer_title_file: "required",
                representer_email: {
                    required: true,
                    email: true,
                    emailtrue : true,
                },
                representer_passport_image: "required",
                representer_nation_image: "required",
                international_name_ar: {     
                    required: function() {
                      return $("#international_member").is(':checked') && CKEDITOR.instances.international_name_ar.updateElement();
                    }
                },
                international_name_en: {     
                    required: function() {
                      return $("#international_member").is(':checked') && CKEDITOR.instances.international_name_en.updateElement();
                    }
                },


                bank_name: "required",
                bank_adress: "required",
                contact_number: {
                    required: true,
                    digits: true,
                },
                bank_city: "required",
                swift_code: "required",
                iban: {
                    required: true,
                    minlength: 16,
                    maxlength: 35, 
                },
                work_fields: "required",
                strategy_ar:{
                    required: function(){
                        return CKEDITOR.instances.strategy_ar.updateElement();
                    }
                },
                strategy_en:{
                    required: function(){
                        return CKEDITOR.instances.strategy_en.updateElement();
                    }
                },
            },
            messages: {
                email:{
                    required: required_msg,
                    email: email_msg,
                    emailtrue : emailtrue_msg
                },
            }
        });
    }




    if(lang == 'ar'){

        var success_add = 'تمت الإضافة بنجاح';
        var success_edit = 'تم التعديل بنجاح';
        var failed = 'حدث خطأ ما';
        var save = 'حفظ';
        var general_error = 'يوجد خطأ ما، برجاء مراجعة المدخلات';
    }
    else{
        var success_add = 'Addess successfully';
        var success_edit = 'Updated successfully';
        var failed = 'An error has occured';
        var save = 'Save';
        var general_error = 'There is something wrong, please check the inputs';
    }

    $(document).on('click', '.submit_button', function () {

        var self = $(this).parent().prev('.general_error');
        var form_submit = $('.submit_form');
        form_submit.valid();

        if(form_submit.valid()){

            $('#loading').html(`
                <button type="button" class="btn btn-primary submit_button" disabled><div class="spinner-border" style="display:inline-block;" role="status"></div>`+save+`</button>
            `);
            

            $(document).find('.error').html("");
            $(document).find('.error').hide();

            var edit = $(document).find('#edit').val();

            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');

            var phone = input.getNumber();
            var mobilephone = input_mobile.getNumber();
            var bankphone = bank_phone.getNumber();
     

            formData.append('strategy_ar', CKEDITOR.instances['strategy_ar'].getData());
            formData.append('strategy_en', CKEDITOR.instances['strategy_en'].getData());
            formData.append('international_name_ar', CKEDITOR.instances['international_name_ar'].getData());
            formData.append('international_name_en', CKEDITOR.instances['international_name_en'].getData());
            formData.append('phone', phone);
            formData.append('mobile', mobilephone);
            formData.append('bank_phone', bankphone);

            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_button">`+save+`</button>
                    `);

                    if(response.status == 1 && edit == "true"){


                        Swal.fire({
                            title: success_edit,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                        
                    }
                    else if(response.status == 1 && edit != "true"){


                        Swal.fire({
                            title: success_add,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                    else if(response.status == 0){

                        Swal.fire({
                            title: failed,
                            type: 'error',
                            html: response.message,
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
        
                    // check for redirect
                    if(response.redirect){

                        setTimeout(function (){

                            window.location.href = response.redirect;
                            
                        });
                    }
        
                    // check for reload
                    if(response.reload && response.reload == 1){

                        setTimeout(function (){

                            location.reload();

                        }, 2000);
                    }

                },
                error: function (errors) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_button">`+save+`</button>
                    `);

                    for (var k in errors.responseJSON.errors) {
                        $(document).find('.error-'+k).show();
                        $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                    }

                    self.show();
                    self.text(general_error);
                }
            });
        }else{
            $('html, body').animate({
                scrollTop: ($('.error').offset().top - 300)
           }, 1000);
        }


    });

// });