$(document).ready( function( ) {   

    // Date picker
    // var datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }


    var select = $('.select2');

    select.each(function () {
        var $this = $(this);
        $this.wrap('<div class="position-relative"></div>');
        $this.select2({
          // the following code is used to disable x-scrollbar when click in select input and
          // take 100% width in responsive also
          dropdownAutoWidth: true,
          width: '100%',
          dropdownParent: $this.parent()
        });
    });

    //add multi images
    $(document).on("click", ".img-btn-success",function(){ 
            var app = $(this).parents(".image_increment").next(".image_clone").html();
            $(this).closest(".image_increment").not(".hidden").append(app);
    });

    $("body").on("click",".img-btn-danger",function(){ 
        $(this).closest(".control-group").remove();
    });

    $('#international_member').change(function(){
        
        if($(this).is(":checked")){

            $('#international_name_container').removeClass('hidden');
            CKEDITOR.replace( 'international_name_ar' );
            CKEDITOR.replace( 'international_name_en' );
        }
        else{
            
            $('#international_name_container').addClass('hidden');
            $('#international_name_ar').text('');
            $('#international_name_ar').html('');
            $('#international_name_en').text('');
            $('#international_name_en').html('');
        }
    });    
    

    var lang = $('#lang').val();

    if(lang == 'ar'){
        var required_msg = 'هذا الحقل إلزامى';
        var email_msg = 'من فضلك أدخل بردي إلكترونى صحيح';
        var emailtrue_msg = 'يجب ان يحتوي البريد الالكتروني علي نطاقات مثال : com , .net , .org , .info. , الي اخره....';
    }
    else{
        var required_msg = 'This feild is required';
        var email_msg = 'Please enter a valid email';
        var emailtrue_msg = 'The email must contain domains such as: com, .net, .org, .info. , Etc....';
    }
    
    // validate email
    $.validator.addMethod("emailtrue", function (value, element) {
        return this.optional(element) || value == value.match(/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i);
    });


    // validate forms
    // Basic info
    if ($('#basic_info_form').length) {
        $('#basic_info_form').validate({
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
        
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top
                }, 1000);
            },
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true,
                    emailtrue : true,
                },
                phone_num: {
                    required: true,
                    digits: true
                    
                },
                mobile_num: {
                    required: true,
                    digits: true
                    
                },
                address_ar: "required",
                address_en: "required",
                charity_category_id: "required",
                type: "required",
                status: "required",
                country: "required",
                city: "required",
                work_fields: "required",
            },
            messages: {
                email:{
                    required: required_msg,
                    email: email_msg,
                    emailtrue : emailtrue_msg
                },
            }
        });
    }

    //official_info_data
    if ($('#official_info_data').length) {
        $('#official_info_data').validate({
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
        
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top
                }, 1000);
            },
            ignore: [],
            rules: {
                license_number: {
                    required: true,
                    min: 1,
                    number: true,
                },
                license_start_date: "required",
                license_end_date: "required",
                establish_date: "required",
                branches_num: {
                    required: true,
                    min: 1,
                    number: true,
                },
                members_num: {
                    required: true,
                    min: 1,
                    number: true,
                },
                contract_date: "required",
                years: "required",
                strategy_ar:{
                    required: function(){
                        return CKEDITOR.instances.strategy_ar.updateElement();
                    }
                },
                strategy_en:{
                    required: function(){
                        return CKEDITOR.instances.strategy_en.updateElement();
                    }
                },
            },
        });
    }

    //representer_form
    if ($('#representer_form').length) {
        $('#representer_form').validate({
            rules: {
                representer_name_ar: "required",
                representer_name_en: "required",
                representer_title_ar: "required",
                representer_title_en: "required",
                representer_email: {
                    required: true,
                    email: true,
                    emailtrue : true,
                },
                international_name_ar: {     
                    required: function() {
                      return $("#international_member").is(':checked') && CKEDITOR.instances.international_name_ar.updateElement();
                    }
                },
                international_name_en: {     
                    required: function() {
                      return $("#international_member").is(':checked') && CKEDITOR.instances.international_name_en.updateElement();
                    }
                },
            },
        });
    }

    // Bank data
    if ($('#bank_data_form').length) {
        $('#bank_data_form').validate({
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
        
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top
                }, 1000);
            },
            rules: {
                bank_name: "required",
                bank_adress: "required",
                bank_phone: {
                    required: true,
                    number: true,
                    min: 1
                },
                bank_city: "required",
                swift_code: "required",
                iban: {
                    required: true,
                    minlength: 16,
                    maxlength: 35, 
                },
            },
        });
    }

    //socials_form
    // if ($('#socials_form').length) {
    //     $('#socials_form').validate({
    //         rules: {
    //             website:{
    //                 required: true,
    //                 url: true,
    //             }
    //         },
    //     });
    // }
    
});