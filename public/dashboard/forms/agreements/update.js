$(document).ready(function(){

    $('#click_create_agreement').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form                = $('#create_agreement');

        // show loadinng
        onFormSubmit(form);      

        // verion saudia
        var supporter_payment_rules_sa_ar       = form.find('#supporter_payment_rules_sa_ar');
        var supporter_payment_rules_sa_en       = form.find('#supporter_payment_rules_sa_en');
        var charity_register_rules_sa_en        = form.find('#charity_register_rules_sa_en');
        var charity_register_rules_sa_ar        = form.find('#charity_register_rules_sa_ar');
        // inctanse ckeditor
        var inctanse_supporter_payment_rules_sa_ar  = CKEDITOR.instances.supporter_payment_rules_sa_ar
        var inctanse_supporter_payment_rules_sa_en  = CKEDITOR.instances.supporter_payment_rules_sa_en
        var inctanse_charity_register_rules_sa_en   = CKEDITOR.instances.charity_register_rules_sa_en
        var inctanse_charity_register_rules_sa_ar   = CKEDITOR.instances.charity_register_rules_sa_ar

        // version italtian
        var supporter_payment_rules_uk_ar       = form.find('#supporter_payment_rules_uk_ar');
        var supporter_payment_rules_uk_en       = form.find('#supporter_payment_rules_uk_en');
        var charity_register_rules_uk_ar        = form.find('#charity_register_rules_uk_ar');
        var charity_register_rules_uk_en        = form.find('#charity_register_rules_uk_en');

        // inctanse ckeditor
        var inctanse_supporter_payment_rules_uk_ar  = CKEDITOR.instances.supporter_payment_rules_uk_ar
        var inctanse_supporter_payment_rules_uk_en  = CKEDITOR.instances.supporter_payment_rules_uk_en
        var inctanse_charity_register_rules_uk_ar   = CKEDITOR.instances.charity_register_rules_uk_ar
        var inctanse_charity_register_rules_uk_en   = CKEDITOR.instances.charity_register_rules_uk_en

        // route post request
        var url = $(this).attr('url');

        var formData = new FormData();

        // append version saudia
        if(inctanse_supporter_payment_rules_sa_ar){ formData.append('supporter_payment_rules_sa_ar' , inctanse_supporter_payment_rules_sa_ar.getData()); }
        if(inctanse_supporter_payment_rules_sa_en){ formData.append('supporter_payment_rules_sa_en' , inctanse_supporter_payment_rules_sa_en.getData()); }
        if(inctanse_charity_register_rules_sa_en) { formData.append('charity_register_rules_sa_en'  , inctanse_charity_register_rules_sa_en.getData()); }
        if(inctanse_charity_register_rules_sa_ar){ formData.append('charity_register_rules_sa_ar'  , inctanse_charity_register_rules_sa_ar.getData()); }
        

        // append version italian
        if(inctanse_supporter_payment_rules_uk_ar) { formData.append('supporter_payment_rules_uk_ar' , inctanse_supporter_payment_rules_uk_ar.getData()); }
        if(inctanse_supporter_payment_rules_uk_en) { formData.append('supporter_payment_rules_uk_en' , inctanse_supporter_payment_rules_uk_en.getData()); }
        if(inctanse_charity_register_rules_uk_ar) { formData.append('charity_register_rules_uk_ar'  , inctanse_charity_register_rules_uk_ar.getData()); }
        if(inctanse_charity_register_rules_uk_en) { formData.append('charity_register_rules_uk_en'  , inctanse_charity_register_rules_uk_en.getData()); }

        var stauts = 'edit-agreement';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,   
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            cache: false,                         
            success: function(result){

                onFormSuccess(form, stauts, true)

                setTimeout(() => {

                    window.location.replace(redirect);
                    
                }, 1500);
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation version saudia
                ckEditorFieldError(form , supporter_payment_rules_sa_ar , inctanse_supporter_payment_rules_sa_ar , jqXHR.responseJSON.errors.supporter_payment_rules_sa_ar);      
                ckEditorFieldError(form , supporter_payment_rules_sa_en , inctanse_supporter_payment_rules_sa_en , jqXHR.responseJSON.errors.supporter_payment_rules_sa_en);      
                ckEditorFieldError(form , charity_register_rules_sa_en  , inctanse_charity_register_rules_sa_en  , jqXHR.responseJSON.errors.charity_register_rules_sa_en);      
                ckEditorFieldError(form , charity_register_rules_sa_ar  , inctanse_charity_register_rules_sa_ar  , jqXHR.responseJSON.errors.charity_register_rules_sa_ar);      
                
                // error validation version italian
                ckEditorFieldError(form , supporter_payment_rules_uk_ar , inctanse_supporter_payment_rules_uk_ar , jqXHR.responseJSON.errors.supporter_payment_rules_uk_ar);      
                ckEditorFieldError(form , supporter_payment_rules_uk_en , inctanse_supporter_payment_rules_uk_en , jqXHR.responseJSON.errors.supporter_payment_rules_uk_en);      
                ckEditorFieldError(form , charity_register_rules_uk_ar  , inctanse_charity_register_rules_uk_ar  , jqXHR.responseJSON.errors.charity_register_rules_uk_ar);      
                ckEditorFieldError(form , charity_register_rules_uk_en  , inctanse_charity_register_rules_uk_en  , jqXHR.responseJSON.errors.charity_register_rules_uk_en);      

            } //  end of error

             
        }); // end of ajax

    }); // end of click button

}); // end of document ready

