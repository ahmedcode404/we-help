$(document).ready(function(){

    if($('#lang').val() == 'ar'){
        
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البريد التالى: info@jaadara.com';
        var error = 'حدث خطأ أثناء التعديل، برجاء المحاولة مرة أخرى لاحقاً';
    }
    else{
       
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
        var error = 'Something Went Wrong, Please Try Again Later';
    }

    // Validate form
    if ($('#financial-request').length) {
        $('#financial-request').validate({
            rules: {
                requestable_id: "required",
                project_id: "required",
                out_to_charity: "required",
            },
        });
    }

    $('#requestable_id').change(function(){

        var action = $(this).data('action');
        var method = $(this).data('method');

        var charity_id = $(this).val();


        // Make ajax request
        $.ajax({
            url: action,
            type: method,
            data:{"_token": $('meta[name="csrf-token"]').attr('content') , charity_id: charity_id},
            dataType: 'html',
            success: function (data) {
                
                $('#projects_container').html(data);

            },
            error: function(){

                Swal.fire({
                    title: error,
                    html: html,
                    type: 'error',
                    timer: 2000,
                    showCancelButton: false,
                    showConfirmButton: false,
                });
            }
        });

    });

    var lang = $('#lang').val();
    if(lang == 'ar'){

        var success_add = 'تمت الإضافة بنجاح';
        var success_edit = 'تم التعديل بنجاح';
        var failed = 'حدث خطأ ما';
        var save = 'حفظ';
    }
    else{
        var success_add = 'Added successfully';
        var success_edit = 'Updated successfully';
        var failed = 'An error has occured';
        var save = 'Save';
    }

    // submit form
    $(document).on('click', '.submit_request_form_button', function(){

        var form_submit = $('.request_form');
        form_submit.valid();

        if(form_submit.valid()){

            $('#loading').html(`
                <button type="button" class="btn btn-primary submit_request_form_button" disabled><div class="spinner-border" style="display:inline-block;" role="status"></div>`+save+`</button>
            `);

            $(document).find('.error').html("");
            $(document).find('.error').hide();
            var edit = $(document).find('#edit').val();

            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');

            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_request_form_button">`+save+`</button>
                    `);

                    if(response.status == 1 && edit == "true"){

                        Swal.fire({
                            title: success_edit,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                        
                    }
                    else if(response.status == 1 && edit != "true"){

                        Swal.fire({
                            title: success_add,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                    else if(response.status == 0){

                        Swal.fire({
                            title: failed,
                            html: response.message,
                            type: 'error',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
        
                    // check for redirect
                    if(response.redirect){

                        setTimeout(function (){

                            window.location.href = response.redirect;
                            
                        });
                    }
        
                    // check for reload
                    if(response.reload && response.reload == 1){

                        setTimeout(function (){

                            location.reload();

                        }, 2000);
                    }

                },
                error: function (errors) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_request_form_button">`+save+`</button>
                    `);
                    
                    for (var k in errors.responseJSON.errors) {
                        $(document).find('.error-images').show();
                        $(document).find('.error-images').html(errors.responseJSON.errors[k]);
                    }
                }
            });
        }
        
    });

});