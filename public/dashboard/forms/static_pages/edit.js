$(document).ready(function(){

    // Validate form
    if ($('#static_pages').length) {
        $('#static_pages').validate({
            rules: {
                title_ar: "required",
                title_en: "required",
                content_ar: "required",
                content_en: "required",
            },
        });
    }

});