$(document).ready(function(){

    // Validate form
    if ($('#partener_form').length) {
        $('#partener_form').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
                nation_id: "required",
                // logo: "required",
                link: "required",
            },
        });
    }

});