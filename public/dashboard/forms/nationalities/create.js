$(document).ready(function(){

    // Validate form
    if ($('#nationalities-form').length) {
        $('#nationalities-form').validate({
            rules: {
                name_ar: "required",
                name_en: "required",
            },
        });
    }

});