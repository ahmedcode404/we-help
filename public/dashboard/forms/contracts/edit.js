$(document).ready(function(){

    $('#click_edit_contract').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form       = $('#edit_contract');

        // show loadinng
        onFormSubmit(form);      

        var item       = form.find('#item');
        var title_ar   = form.find('#title_ar');
        var title_en   = form.find('#title_en');
        var content_ar = form.find('#content_ar');
        var content_en = form.find('#content_en');

        // inctanse ckeditor
        var inctanse_content_ar  = CKEDITOR.instances.content_ar
        var inctanse_content_en  = CKEDITOR.instances.content_en

        // route post request
        var url = $(this).attr('url');
        var redirect = $(this).attr('redirect');

        var formData = new FormData();

        // append version saudia
        formData.append('item'       , item.val());
        formData.append('title_ar'   , title_ar.val());
        formData.append('title_en'   , title_en.val());
        formData.append('content_ar' , inctanse_content_ar.getData());
        formData.append('content_en' , inctanse_content_en.getData());
        // request method
        formData.append('_method'    , 'PATCH');

        var stauts = 'edit-contract';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,   
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            cache: false,                         
            success: function(result){

                onFormSuccess(form, stauts, true)

                setTimeout(() => {

                    window.location.replace(redirect);
                    
                }, 1500);
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);
                
                // error validation 
                textFieldError(form     , title_ar   , jqXHR.responseJSON.errors.title_ar);
                textFieldError(form     , title_en   , jqXHR.responseJSON.errors.title_en);
                ckEditorFieldError(form , content_ar , inctanse_content_ar , jqXHR.responseJSON.errors.content_ar);      
                ckEditorFieldError(form , content_en , inctanse_content_en , jqXHR.responseJSON.errors.content_en);      

            } //  end of error

             
        }); // end of ajax

    }); // end of click button

}); // end of document ready

