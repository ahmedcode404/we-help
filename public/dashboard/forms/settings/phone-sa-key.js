$(document).ready(function () {

    var input_phone_1 = document.querySelector("#phone_sa");
    window.intlTelInput(input_phone_1, {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "phone_sa_key",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: "{{ url('dashboard/js/utils.js') }}"
    });     
    
    
    $("#form-setting-sa").on("submit", function() {
        var country_code = $('.iti__selected-dial-code').text();

        $('input[name="phone_sa_key"]').val(country_code);

    });    


})