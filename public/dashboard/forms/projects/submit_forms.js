

$(document).ready(function(){

    var lang = $('#lang').val();
    if(lang == 'ar'){

        var success_add = 'تمت الإضافة بنجاح';
        var success_edit = 'تم التعديل بنجاح';
        var failed = 'حدث خطأ ما';
        var save = 'حفظ';
        var general_error = 'يوجد خطأ ما، برجاء مراجعة المدخلات';
    }
    else{
        var success_add = 'Added successfully';
        var success_edit = 'Updated successfully';
        var failed = 'An error has occured';
        var save = 'Save';
        var general_error = 'There is something wrong, please check the inputs';
    }

    // Basic info
    $(document).on('click', '.submit_basic_info_button', function(){

        var self = $(this).parent().prev('.general_error');
        var form_submit = $('.basic_info_form');
        form_submit.valid();

        if(form_submit.valid()){

            $('#loading').html(`
                <button type="button" class="btn btn-primary submit_basic_info_button" disabled><div class="spinner-border" style="display:inline-block;" role="status"></div>`+save+`</button>
            `);

            $(document).find('.error').html("");
            $(document).find('.error').hide();
            var edit = $(document).find('#edit').val();

            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');

            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_basic_info_button">`+save+`</button>
                    `);

                    if(response.status == 1 && edit == "true"){

                        Swal.fire({
                            title: success_edit,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                        
                    }
                    else if(response.status == 1 && edit != "true"){

                        Swal.fire({
                            title: success_add,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                    else if(response.status == 0){

                        Swal.fire({
                            title: failed,
                            type: 'error',
                            html: response.message,
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
        
                    // check for redirect
                    if(response.redirect){

                        setTimeout(function (){

                            window.location.href = response.redirect;
                            
                        });
                    }
        
                    // check for reload
                    if(response.reload && response.reload == 1){

                        setTimeout(function (){

                            location.reload();

                        }, 2000);
                    }

                },
                error: function (errors) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_basic_info_button">`+save+`</button>
                    `);
                    
                    for (var k in errors.responseJSON.errors) {
                        $(document).find('.error-'+k).show();
                        $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                    }

                    self.show();
                    self.text(general_error);
                }
            });
        }else{
            $('html, body').animate({
                scrollTop: ($('.error').offset().top - 300)
           }, 1000);
        }
        
    });

    // Images form
    $(document).on('click', '.submit_images_button', function(){

        var self = $(this).parent().prev('.general_error');
        var form_submit = $('.images_form');
        form_submit.valid();

        if(form_submit.valid()){

            $('#loading').html(`
                <button type="button" class="btn btn-primary submit_images_button" disabled><div class="spinner-border" style="display:inline-block;" role="status"></div>`+save+`</button>
            `);

            $(document).find('.error').html("");
            $(document).find('.error').hide();
            var edit = $(document).find('#edit').val();

            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');

            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_images_button">`+save+`</button>
                    `);

                    if(response.status == 1 && edit == "true"){

                        Swal.fire({
                            title: success_edit,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                        
                    }
                    else if(response.status == 1 && edit != "true"){

                        Swal.fire({
                            title: success_add,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                    else if(response.status == 0){

                        Swal.fire({
                            title: failed,
                            type: 'error',
                            html: response.message,
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
        
                    // check for redirect
                    if(response.redirect){

                        setTimeout(function (){

                            window.location.href = response.redirect;
                            
                        });
                    }
        
                    // check for reload
                    if(response.reload && response.reload == 1){

                        setTimeout(function (){

                            location.reload();

                        }, 2000);
                    }

                },
                error: function (errors) {

                    $('#loading').html(`
                        <button type="button" class="btn btn-primary submit_images_button">`+save+`</button>
                    `);
                    
                    for (var k in errors.responseJSON.errors) {
                        $(document).find('.error-images').show();
                        $(document).find('.error-images').html(errors.responseJSON.errors[k]);
                    }

                    self.show();
                    self.text(general_error);
                }
            });
        }else{
            $('html, body').animate({
                scrollTop: ($('.error').offset().top - 300)
           }, 1000);
        }
        
    });

    // Goals and desription form form
    $(document).on('click', '.goals_and_description_form_button', function () {

        var self = $(this).parent().prev('.general_error');
        var form_submit = $('.goals_and_description_form');
        form_submit.valid();
    
        if(form_submit.valid()){
    
            $('#loading').html(`
                <button type="button" class="btn btn-primary goals_and_description_form_button" disabled><div class="spinner-border" style="display:inline-block;" role="status"></div>`+save+`</button>
            `);
    
            $(document).find('.error').html("");
            $(document).find('.error').hide();
            var edit = $(document).find('#edit').val();
    
            var formData = new FormData(form_submit[0]),
            action = form_submit.attr('action'),
            method = form_submit.attr('method');
    
            formData.append('long_desc_ar', CKEDITOR.instances['long_desc_ar'].getData());
            formData.append('long_desc_en', CKEDITOR.instances['long_desc_en'].getData());
            formData.append('goals_ar', CKEDITOR.instances['goals_ar'].getData());
            formData.append('goals_en', CKEDITOR.instances['goals_en'].getData());
    
            // make the ajax request
            $.ajax({
                url: action,
                type: method,
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {
    
                    $('#loading').html(`
                        <button type="button" class="btn btn-primary goals_and_description_form_button">`+save+`</button>
                    `);
    
                    if(response.status == 1 && edit == "true"){

    
                        Swal.fire({
                            title: success_edit,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                        
                    }
                    else if(response.status == 1 && edit != "true"){

    
                        Swal.fire({
                            title: success_add,
                            type: 'success',
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
                    else if(response.status == 0){
    
                        Swal.fire({
                            title: failed,
                            type: 'error',
                            html: response.message,
                            timer: 2000,
                            showCancelButton: false,
                            showConfirmButton: false,
                        });
                    }
        
                    // check for redirect
                    if(response.redirect){
    
                        setTimeout(function (){
    
                            window.location.href = response.redirect;
                            
                        });
                    }
        
                    // check for reload
                    if(response.reload && response.reload == 1){
    
                        setTimeout(function (){
    
                            location.reload();
    
                        }, 2000);
                    }
    
                },
                error: function (errors) {
    
                    $('#loading').html(`
                        <button type="button" class="btn btn-primary goals_and_description_form_button">`+save+`</button>
                    `);
                    
                    for (var k in errors.responseJSON.errors) {
                        $(document).find('.error-'+k).show();
                        $(document).find('.error-'+k).html(errors.responseJSON.errors[k]);
                    }

                    self.show();
                    self.text(general_error);
                }
            });
        }else{
            $('html, body').animate({
                scrollTop: ($('.error').offset().top - 300)
           }, 1000);
        }
    
    
    });
});