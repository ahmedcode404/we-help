$(document).ready(function(){

    var lang = $('#lang').val();

    if(lang == 'ar'){

        var are_you_sure = 'هل تريد إسترجاع المشروع؟';
        var yes = 'نعم';
        var no = 'لا';
        var restore_success = 'تم إسترجاع المشروع بنجاح';
        var restore_error = 'حدث خطأ ما، برجاء المحاولة فى وقت لاحق';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البريد التالى: info@jaadara.com';

    }
    else{

        var are_you_sure = 'Are you sure you want to resotre project';
        var yes = 'Yes';
        var no = 'No';
        var restore_success = 'Project is resotred successfully';
        var restore_error = 'Something wont wrong, pleae try again later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
    }

    $(document).on('click', '.restore-table', function(e) {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: true
        });

        swalWithBootstrapButtons.fire({
            title: are_you_sure,
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: yes,
            cancelButtonText: no,
            showLoaderOnConfirm: true,

        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: $(this).data('url'),
                    type: "GET",
                    dataType: 'html',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        _method: "GET"
                    },
                    success: function(data) {

                        data = JSON.parse(data);

                        if (data.data == 1) {
                            swalWithBootstrapButtons.fire({
                                type: 'success',
                                title: restore_success,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        } 
                        else if (data.data == 0) {
                            swalWithBootstrapButtons.fire({
                                type: 'error',
                                title: restore_error,
                                title: html,
                                showConfirmButton: false,
                                timer: 1000
                            });
                        } 
                    },
                    error: function(){

                        swalWithBootstrapButtons.fire({
                            type: 'error',
                            title: restore_error,
                            title: html,
                            showConfirmButton: false,
                            timer: 1000
                        });
                    }
                });

                $(this).closest('tr').remove();

            } else if (
                /* Read more about handling dismissals below */
                result.dismiss === Swal.DismissReason.cancel
            ) {

            }
        });

    });
});