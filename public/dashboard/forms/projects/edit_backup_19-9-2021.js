$(document).ready(function(){

    if($('#lang').val() == 'ar'){
        var success = 'تم الحذف بنجاح';
        var error = 'حدث خطأ أثناء الحذف، برجاء المحاولة مرة أخرى لاحقاً';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البريد التالى: info@jaadara.com';
        var permission = 'ليست لديك صلاحية';
    }
    else{
        var success = 'Item Deleted Successfully';
        var error = 'Something Went Wrong, Please Try Again Later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
        var permission = 'You Don not have Permission';
    }

    // Date picker
    // datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }

    var type = $('#type').val();

    if(type == 'educational'){

        $('#eng_maps').val(null);

        $('#care_type').val(null);
        $('#care_duration').val(null);

        $('#emerg_service_id').val(null);
        $('#emerg_service_other').val(null);

        $('#stable_project').val(null);

    }
    else if(type == 'engineering'){

        $('#edu_service_id').val(null);
        $('#edu_service_other').val(null);
        $('#stage').val(null);

        $('#care_type').val(null);
        $('#care_duration').val(null);

        $('#emerg_service_id').val(null);
        $('#emerg_service_other').val(null);

        $('#stable_project').val(null);

    }
    else if(type == 'sponsorship'){

        $('#edu_service_id').val(null);
        $('#edu_service_other').val(null);
        $('#stage').val(null);

        $('#eng_maps').val(null);

        $('#emerg_service_id').val(null);
        $('#emerg_service_other').val(null);

        $('#stable_project').val(null);

    }
    else if(type == 'emergency'){

        $('#edu_service_id').val(null);
        $('#edu_service_other').val(null);
        $('#stage').val(null);

        $('#eng_maps').val(null);

        $('#care_type').val(null);
        $('#care_duration').val(null);

        $('#stable_project').val(null);
    }
    else if(type == 'sustainable'){

        $('#edu_service_id').val(null);
        $('#edu_service_other').val(null);
        $('#stage').val(null);

        $('#eng_maps').val(null);

        $('#care_type').val(null);
        $('#care_duration').val(null);

        $('#emerg_service_id').val(null);
        $('#emerg_service_other').val(null);

    }
    else{

        $('#edu_service_id').val(null);
        $('#edu_service_other').val(null);
        $('#stage').val(null);

        $('#eng_maps').val(null);

        $('#care_type').val(null);
        $('#care_duration').val(null);

        $('#emerg_service_id').val(null);
        $('#emerg_service_other').val(null);

        $('#stable_project').val(null);

    }

    $('#charity_category_id').change(function(){

        var type = $(this).find(':selected').data('type');

        $('#type').val(type);

        // 3 تعليمى
        if(type == 'educational'){
            $('#edu_service_id_container_1').removeClass('hidden');
            $('#edu_service_id_container_3').removeClass('hidden');

            // $('#stage').val(null);
            $('#eng_maps').val(null);

            $('#care_type').val(null);
            $('#care_duration').val(null);

            $('#emerg_service_id').val(null);
            $('#emerg_service_other').val(null);

            $('#stable_project').val(null);
        }
        else{
            $('#edu_service_id_container_1').addClass('hidden'); 
            $('#edu_service_id_container_2').addClass('hidden'); 
            $('#edu_service_id_container_3').addClass('hidden'); 

            $('#edu_service_id').val(null);
            $('#edu_service_other').val(null);
        }

        // 4 انشائى
        if(type == 'engineering'){
            $('#eng_maps_container').removeClass('hidden');

            $('#edu_service_id').val(null);
            $('#edu_service_other').val(null);

            $('#stage').val(null);

            $('#care_type').val(null);
            $('#care_duration').val(null);

            $('#emerg_service_id').val(null);
            $('#emerg_service_other').val(null);

            $('#stable_project').val(null);
        }
        else{
            $('#eng_maps_container').addClass('hidden');

            $('#eng_maps').val(null);
        }

        // 5 رعاية يتيم
        if(type == 'sponsorship'){
            $('#care_type_container').removeClass('hidden');
            $('#care_duration_container').removeClass('hidden');

            $('#edu_service_id').val(null);
            $('#edu_service_other').val(null);

            $('#stage').val(null);
            $('#eng_maps').val(null);

            $('#emerg_service_id').val(null);
            $('#emerg_service_other').val(null);

            $('#stable_project').val(null);
        }
        else{
            $('#care_type_container').addClass('hidden');
            $('#care_duration_container').addClass('hidden');

            $('#care_type').val(null);
            $('#care_duration').val(null);
        }

        // 6 طارئ 
        if(type == 'emergency'){

            $('#emerg_service_id_container_1').removeClass('hidden');  
            
            $('#edu_service_id').val(null);
            $('#edu_service_other').val(null);

            $('#stage').val(null);
            $('#eng_maps').val(null);

            $('#care_type').val(null);
            $('#care_duration').val(null);

            $('#stable_project').val(null);
        }
        else{
            $('#emerg_service_id_container_1').addClass('hidden'); 
            $('#emerg_service_id_container_2').addClass('hidden'); 

            $('#emerg_service_id').val(null);
            $('#emerg_service_other').val(null);
        }

        // 7 مستديم 
        if(type == 'sustainable'){
            $('#stable_project_container').removeClass('hidden');

            $('#edu_service_id').val(null);
            $('#edu_service_other').val(null);

            $('#stage').val(null);
            $('#eng_maps').val(null);

            $('#care_type').val(null);
            $('#care_duration').val(null);

            $('#emerg_service_id').val(null);
            $('#emerg_service_other').val(null);
        }
        else{
            $('#stable_project_container').addClass('hidden');

            $('#stable_project').val(null);
        }
    });

    $(document).on('change', '#edu_service_id', function(){
        
        if($(this).val() == 'other'){
            $('#edu_service_id_container_2').removeClass('hidden');
        }
        else{
            $('#edu_service_id_container_2').addClass('hidden'); 
        }
    });

    $(document).on('change', '#emerg_service_id', function(){
        
        if($(this).val() == 'other'){
            $('#emerg_service_id_container_2').removeClass('hidden');
        }
        else{
            $('#emerg_service_id_container_2').addClass('hidden'); 
        }
    });

    //add multi images
    $(".img-btn-success").click(function(){ 
        var html = $(".image_clone").html();
        $(".image_increment").after(html);
    });

    $("body").on("click",".img-btn-danger",function(){ 
        $(this).parents(".control-group").remove();
    });

    // Validate form
    if ($('#basic_info_form').length) {
        $('#basic_info_form').validate({
            rules: {
                project_num: "required",
                charity_id: "required",
                name: "required",
                name_other: {
                    required: function(e){
                        return $('#name').val() == 5;
                    }
                },
                start_date: "required",
                duration: {
                    required: true,
                    number: true,
                    min: 1,
                    step: 1,
                },
                cost: {
                    required: true,
                    number: true,
                    min: 1,
                    step: 0.1,
                },
                currency_id: "required",
                benef_num: {
                    required: true,
                    number: true,
                    min: 1,
                    step: 1,
                },
                charity_category_id: "required",
                edu_service_id: {
                    required: function(e){
                        return $('#charity_category_id').val() == 3;
                    }
                },
                edu_service_other: {
                    required: function(e){
                        return $('#edu_service_id').val() == 'other';
                    },
                },
                stage: {
                    required: function(e){
                        return $('#charity_category_id').val() == 3;
                    }
                },
                eng_maps: {
                    required: function(e){
                        return $('#charity_category_id').val() == 4;
                    }
                },
                care_type: {
                    required: function(e){
                        return $('#charity_category_id').val() == 5;
                    }
                },
                care_duration: {
                    required: function(e){
                        return $('#charity_category_id').val() == 5;
                    }
                },
                emerg_service_id: {
                    required: function(e){
                        return $('#charity_category_id').val() == 5;
                    }
                },
                emerg_service_other: {
                    required: function(e){
                        return $('#emerg_service_id').val() == 'other';
                    }
                },
                stable_project: {
                    required: function(e){
                        return $('#charity_category_id').val() == 5;
                    }
                },
                status: "required",
                country: "required",
                location: "required",
            },
        });
    }

    // Validate form
    if ($('#goals_and_description_form').length) {
        $('#goals_and_description_form').validate({
            ignore: [],
            rules: {
                desc_ar: "required",
                desc_en: "required",
                long_desc_ar: {
                    required: function(){
                        return CKEDITOR.instances.long_desc_ar.updateElement();
                    }
                },
                long_desc_en: {
                    required: function(){
                        return CKEDITOR.instances.long_desc_en.updateElement();
                    }
                },
                goals_ar: {
                    required: function(){
                        return CKEDITOR.instances.goals_ar.updateElement();
                    }
                },
                goals_en: {
                    required: function(){
                        return CKEDITOR.instances.goals_en.updateElement();
                    }
                },
                
            },
        });
    }



    
});