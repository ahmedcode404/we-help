$(document).ready(function() {

    // toase erro when data login not found

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    var lang = $('loaded').attr('lang');

    Toast.fire({
        icon: 'error',
        title: lang == 'ar' ? 'بيانات التسجيل غير موجوده برجاء التحقق من البيانات' : 'Data Login Not Foud Please check the data',
    })

});