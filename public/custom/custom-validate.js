
function onFormSuccess(form, status = null, resetform = false) {

    form.find('.spinner-border').remove();

    // form.find('.processing-div').hide();

    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    var locale = $('.loaded').attr('lang');

    if (locale == 'en') {

        var  success_project  = 'Add Project Suucessfully';
        var  update_project   = 'Update Project Suucessfully';
        var  success_phase    = 'Add Phase Suucessfully';
        var  update_phase     = 'Update Phase Suucessfully';
        var  success_report   = 'Add Report Successfully';
        var  update_report    = 'Update Report Successfully';        
        var  update_charity   = 'Update Data Charity Successfully';        
        var  update_agreement = 'Update Data Agreement Successfully';        
        var  contract         = 'Add Contract Successfully';        
        var  edit_contract    = 'Update Contract Successfully';        
        var  edit_setting_sa  = 'Update Setting Version Saudia Successfully';        
        var  edit_setting_uk  = 'Update Setting Version Italian Successfully';        
        var  service          = 'Add Service Successfully';        
        var  edit_service     = 'Update Service Successfully';        
        var  sponser          = 'Add Sponser Successfully';        
        var  edit_sponser     = 'Update Sponser Successfully';        

    } else {

        var  success_project  = 'تم اضافة المشروع بنجاح';
        var  update_project   = 'تم تعديل المشروع بنجاح';
        var  success_phase    = 'تم اضافة المرحلة بنجاح';
        var  update_phase     = 'تم تعديل المرحلة بنجاح';
        var  success_report   = 'تم اضافة التقرير بنجاح';
        var  update_report    = 'تم تعديل التقرير بنجاح';
        var  update_charity   = 'تم تحديث البيانات بنجاح';
        var  update_agreement = 'تم تحديث بيانات الاتفاقيات بنجاح';
        var  contract         = 'تم اضافة البند بنجاح';
        var  edit_contract    = 'تم تعديل البند بنجاح';
        var  edit_setting_sa  = 'تم تعديل الاعدادات في نسخه السعودية بنجاح';
        var  edit_setting_uk  = 'تم تعديل الاعدادات في نسخه الايطالية بنجاح';
        var  service          = 'تم اضافة الخدمة بنجاح';
        var  edit_service     = 'تم تعديل الخدمة بنجاح';
        var  sponser          = 'تم أضافة الجهه بنجاح';
        var  edit_sponser     = 'تم تعديل الجهه بنجاح';

    }

    Toast.fire({
        icon: 'success',
        title: 
            status == 'project'         ? success_project   : '' |
            status == 'edit-project'    ? update_project    : '' |
            status == 'phase'           ? success_phase     : '' |
            status == 'edit-phase'      ? update_phase      : '' |
            status == 'edit-report'     ? update_report     : '' |
            status == 'report'          ? success_report    : '' |
            status == 'edit-charity'    ? update_charity    : '' |
            status == 'edit-agreement'  ? update_agreement  : '' |
            status == 'contract'        ? contract          : '' |
            status == 'edit-contract'   ? edit_contract     : '' |
            status == 'edit-setting-sa' ? edit_setting_sa   : '' |
            status == 'edit-setting-uk' ? edit_setting_uk   : '' |
            status == 'service'         ? service           : '' |
            status == 'edit-service'    ? edit_service      : '' |
            status == 'sponser'         ? sponser           : '' |
            status == 'edit-sponser'    ? edit_sponser      : '' 
    });
    
    form.find('button[type="button"]').show();

    form.find('button[type="button"]').prop('disabled', false);

    if (resetform == true) {

        form[0].reset();
        if ($('input.custom-file-input')) {
            $('input.custom-file-input').next().html('');
            $('input.custom-file-input').parent().prev().attr('src', window.location.protocol + '//' + window.location.hostname + '/img/default-upload-img.png');
        }

    }
    setTimeout(function() {
        form.find('.success-div').fadeOut();
    }, 5000);
}

function successPrice(form) {

    form.find('button[type="button"]').show();

    form.find('button[type="button"]').prop('disabled', false);

}


function resetCKEdtior(instances) {
    console.log(instances.length);
    for (i = 0; i < instances.length; i++) {
        instances[i].setData('', function() {
            this.updateElement();
        });
    }
}

function onFormSubmit(form) {

    // form.find('.processing-div').show();
    form.find('button[type="button"]').prepend('<div class="spinner-border" style="display:inline-block;" role="status"></div>');
    form.find('button[type="button"]').prop('disabled', true);

}

function onFormErrors(form) {

    // form.find('.processing-div').hide();
    form.find('button[type="button"]').find('.spinner-border').remove();
    form.find('button[type="button"]').prop('disabled', true);

}

function showError(form, error) {
    form.find('.errors-div').find('span').html(error);
    form.find('.errors-div, .errors-div .alert-danger').show();
    setTimeout(function() {
        form.find('.errors-div').fadeOut();
    }, 5000);
}

function textFieldError(form, field_id, error) {

    if (error && error.length > 0) {
        $('button[type="button"]').prop('disabled', true);

        field_id.parent().find('.error-input').html(error[0]);
        field_id.parent().show();
        field_id.keydown(function() {
            hideTabPaneError(field_id);
            form.find('button[type="button"]').show();
            field_id.parent().find('.error-input').html('');
            $('button[type="button"]').prop('disabled', false)

        });

        showTabPaneError(field_id);

    }
}


function textFieldPhoneKeyError(form, field_id, error) {

    if (error && error.length > 0) {
        field_id.parent().next('.error-input').html(error[0]);
        field_id.parent().show();
        field_id.keydown(function() {
            hideTabPaneError(field_id);
            form.find('button[type="button"]').show();
            field_id.parent().next('.error-input').html('');
            $('button[type="button"]').prop('disabled', false)

        });
        showTabPaneError(field_id);

    }
}

function textDateFieldError(form, field_id, error) {

    if (error && error.length > 0) {
        $('button[type="button"]').prop('disabled', true);
        field_id.parent().find('.error-input').html(error[0]);
        field_id.parent().show();
        field_id.change(function() {
            
            hideTabPaneError(field_id);
            form.find('button[type="button"]').show();
            field_id.parent().find('.error-input').html('');
            $('button[type="button"]').prop('disabled', false)

        });

        showTabPaneError(field_id);

    }
}



function selectFieldError(form, field_id, error) {

    if (error && error.length > 0) {
        $('button[type="button"]').prop('disabled', true);

        field_id.parent().find('.error-input').html(error[0]);
        field_id.next().show();
        field_id.on('change', function() {
            form.find('button[type="button"]').show();
            $('button[type="button"]').prop('disabled', false)
            hideTabPaneError(field_id);
            field_id.next().hide();
            field_id.next().find('span').html('');

        });
        showTabPaneError(field_id);
    }
}



function fileFieldError(form, field_id, error) {
console.log('image ' + error);
    if (error && error.length > 0) {
        $('button[type="button"]').prop('disabled', true);

        field_id.next('.error-input').html(error[0]);
        field_id.next('.error-input').show();
        field_id.on('change', function() {
            form.find('button[type="button"]').show();
            $('button[type="button"]').prop('disabled', false)
            hideTabPaneError(field_id);
            field_id.next('.error-input').hide();
            field_id.next('.error-input').html('');
        });
        showTabPaneError(field_id);
    }
}

function ckEditorFieldError(form, field_id, instance, error) {
    if (error && error.length > 0) {
        $('button[type="button"]').prop('disabled', true);

        field_id.parent().find('.error-input').html(error[0]);
        field_id.parent('.error-input').show();
        instance.focus();
        // window.scrollTo(0, field_id.next().offset().top - 200);
        instance.on('change', function() {
            form.find('button[type="button"]').show();
            $('button[type="button"]').prop('disabled', false)
            field_id.parent().find('.error-input').hide();
            field_id.parent().find('.error-input').html('');
        });
        showTabPaneError(field_id);

    }
}

function showTabPaneError(field_id) {
    field_id.addClass('show-tab-error');
}

function showTabCustomError(field_id) {
    field_id.addClass('custom-error');
}



function hideTabPaneError(field_id) {
    field_id.removeClass('show-tab-error');
}

/////////////////////////////////////////// web ////////////////////////////////////////////////

 // custom validation 

 function validatePriceAmbsaador(form, field_id, error) {
    if (error && error.length > 0) {
        console.log(form.find('.error'));
        form.find('.error').html(error[0]);
        field_id.parent().show();
        field_id.keydown(function() {
            hideTabPaneError(field_id);
            form.find('.error').html('');


        });

        showTabPaneErrorPrice(field_id);

    }
}

 function validatePrice(form, field_id, error) {
    if (error && error.length > 0) {
        form.find('.error').html(error[0]);
        field_id.parent().show();
        field_id.keydown(function() {
            hideTabPaneError(field_id);
            form.find('.error').html('');


        });

        showTabPaneErrorPrice(field_id);

    }
}
 
function checkboxFieldError(form, field_id, error) {

    if (error && error.length > 0) {
        field_id.parent().find('.error').html(error[0]);
        field_id.parent().show();

        field_id.on('click', function() {
            hideTabPaneWebError(field_id);
            field_id.parent().find('.error').html('');

        });
        showTabPaneError(field_id);
    }
}


function textError(form, field_id, error) {

    if (error && error.length > 0) {
        field_id.parent().find('.error').html(error[0]);
        field_id.parent().show();
        field_id.keydown(function() {
            hideTabPaneWebError(field_id);
            field_id.parent().find('.error').html('');

        });

        showTabPaneError(field_id);

    }
}


// show and hide error

function showTabPaneError(field_id) {
    field_id.addClass('show-tab-error');
}

function hideTabPaneWebError(field_id) {
    field_id.removeClass('.error');
}

function showTabPaneErrorPrice(field_id) {
    field_id.addClass('error');
}

function hideTabPaneError(field_id) {
    field_id.removeClass('show-tab-error');
}