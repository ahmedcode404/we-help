$(document).ready(function () {

    $('.click-delete').click(function(e) {

        e.preventDefault();

        const Toast2 = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 4000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          
        var locale = $('.loaded').attr('lang');

        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: true,
            showCancelButton: true,
            confirmButtonColor: '#f30507',
            cancelButtonColor: '#888',
            confirmButtonText: locale == 'ar' ? 'حذف' : 'Delete',
            cancelButtonText: locale == 'ar' ? 'لا' : 'No',
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          
          Toast.fire({
            icon: 'question',
            title: locale == 'ar' ? 'هل تريد الحذف ؟' : 'Do You Want Delete' 
          }).then((result) => {
                if (result.isConfirmed) {
    
                    var id    = $(this).attr('id');
                    var url = $(this).attr('href');
                    var elem  = $(this).closest('tr');
    
                    $.ajax({
                        type: 'delete',
                        url: url,
                        data: {
                            _token  : $('meta[name="csrf-token"]').attr('content'),
                            id      : id,
                        },
                        dataType: 'json',
                        success: function(result) {
                            elem.fadeOut();
        
                            Toast2.fire({
                                title: locale == 'ar' ? 'تم الحذف بنجاح' : 'Delete Successfully',
                                showConfirmButton: false,
                                icon: 'success',
                                timer: 1000
                            });
                        } // end of success
    
                    }); // end of ajax 
    
                } else if (result.dismiss === Swal.DismissReason.cancel) 
                {
                    Toast2.fire({
                        title:  locale == 'ar' ?  'تم إلإلغاء' : 'Cnacel Done',
                        showConfirmButton: false,
                        icon: 'success',
                        timer: 1000
                    });
        
                }
    
            }) // end of then
    });
    
}) // end of document ready 