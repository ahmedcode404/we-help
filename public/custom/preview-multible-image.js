
    // create preview image when upload
    // if (window.File && window.FileList && window.FileReader) 
    // {
    //     $("#images").on("change", function(e) 
    //     {
    //         $('#db_images').html('');
    //         var files = e.target.files,filesLength = files.length;
    //         for (var i = 0; i < filesLength; i++) {

    //             var f = files[i]
    //             var fileReader = new FileReader();

    //             fileReader.onload = (function(e) {

    //                 var file = e.target;

    //                 $("<div class=\"preview\">" +
    //                 "<img style=\"width:110px;height: 100px;margin-top: 11px;\" class=\"imageThumb\" src=\"" + e.target.result + "\" title=\"" + file.name + "\"/>" +
    //                 "<br/><button class=\"btn btn-danger btn-sm remove\">x</button>" +
    //                 "</div>").insertAfter("#images");

    //                 $(".remove").click(function(){

    //                     $(this).parent(".preview").remove();

    //                 }); // end of click remove

    //             }); // end of fileReader

    //             fileReader.readAsDataURL(f);

    //         } // end of for var i

    //     }); // end 0f files

    // } 
    // else 
    // {

    //     alert("Your browser doesn't support to File API")

    // } // end of else if window file


    // BEGIN: delete image
    $(document).on( 'click', '.delete-image' , function(e){

        e.preventDefault();

        var id = $(this).data('id');
        var url = $(this).data('url');
        var item = $(this);

        $.ajax({
            type: 'get',
            // headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: url,
            data:  {id:id},
            dataType: 'json',
            success: function() {

                item.parent().remove();

                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showCancelButton: false,
                    showConfirmButton: false,
                    timer: 4000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                      toast.addEventListener('mouseenter', Swal.stopTimer)
                      toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                  })

                var locale = $('.loaded').attr('lang');

                if (locale == "en") 
                {
                    var success = 'Delete Data Succefully';
                } else 
                {
                    var success = 'تم حذف البيانات بنجاح';
                }
                  
                Toast.fire({
                icon: 'success',
                title: success
                })                

            } // end of success

        }); // end of ajax

   }); // end of click remove
    // END: delete image