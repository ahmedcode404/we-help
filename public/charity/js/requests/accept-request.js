$(document).ready(function(){

    var lang = $('#lang').val();

    if(lang == 'ar'){
        var success = 'تم التعديل بنجاح';
        var error = 'حدث خطأ أثناء التعديل، برجاء المحاولة مرة أخرى لاحقاً';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البردي التالى: info@jaadara.com';
    }
    else{
        var success = 'Item Updated Successfully';
        var error = 'Something Went Wrong, Please Try Again Later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
    }

    $('.recieve_status').change(function(){

        var action = $(this).data('action');
        var method = $(this).data('method');

        var recieve_status = $(this).val();
        var id = $(this).data('id');

        // Make ajax request
        $.ajax({
            url: action,
            type: method,
            data:{"_token": $('meta[name="csrf-token"]').attr('content') , id: id, recieve_status: recieve_status},
            dataType: 'JSON',
            success: function (data) {

                if (data.data == 1) {
                    swal.fire({
                        type: 'success',
                        title: success,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
                else{
                    swal.fire({
                        type: 'error',
                        title: error,
                        html: html,
                        showConfirmButton: false,
                        timer: 1500
                    });
                }
            
            }
        });

    })
});