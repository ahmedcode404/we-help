$(document).ready(function(){

    $('#edit-form-project').click(function(e){

        e.preventDefault();


        // get all id inputs
        var form                = $('#form-update-project');

        // show loadinng
        onFormSubmit(form);      

        var cost                = form.find('#cost');
        var currency_id         = form.find('#currency_id');
        var images              = form.find('#images');
        var attach              = form.find('#attach');
        var image               = form.find('#image');
        var long_desc_en        = form.find('#long_desc_en');
        var long_desc_ar        = form.find('#long_desc_ar');
        var desc_en             = form.find('#desc_en');
        var desc_ar             = form.find('#desc_ar');
        var goals_en            = form.find('#goals_en');
        var goals_ar            = form.find('#goals_ar');
        var benef_num           = form.find('#benef_num');
        var country_id          = form.find('#country_id');
        var city_id             = form.find('#city_id');
        var location            = form.find('#location');
        var lat                 = form.find('#lat');
        var lng                 = form.find('#lng');
        var start_date          = form.find('#start_date');
        // var duration            = form.find('#duration');
        var name                = form.find('#name');
        var care_duration       = form.find('#care_duration');
        var stable_project      = form.find('#stable_project');
        var care_type           = form.find('#care_type');
        var stage               = form.find('#stage');
        var charity_category_id = form.find('#charity_category_id');
        var type                = charity_category_id.find(':selected').data('type');
        var name_other          = form.find('#name_other');
        var eng_maps            = form.find('#eng_maps');
        var emerg_service_id    = form.find('#emerg_service_id');
        var edu_service_id      = form.find('#edu_service_id');
        var edu_service_other   = form.find('#edu_service_other');
        var emerg_service_other = form.find('#emerg_service_other');
        var item                = form.find('#item');
        
        // inctanse ckeditor
        inctanse_long_desc_ar   = CKEDITOR.instances.long_desc_ar
        inctanse_long_desc_en   = CKEDITOR.instances.long_desc_en
        // inctanse_desc_ar        = CKEDITOR.instances.desc_ar
        // inctanse_desc_en        = CKEDITOR.instances.desc_en
        inctanse_goals_ar       = CKEDITOR.instances.goals_ar
        inctanse_goals_en       = CKEDITOR.instances.goals_en

        // route post request
        var url = $(this).attr('url');
        var redirect = $(this).attr('redirect');

        var formData = new FormData();

        if(type == 'engineering')
        {

            // uploade file eng_maps
            var data_eng_maps = $('#eng_maps')[0].files;

            if(data_eng_maps[0] != undefined)     
            {

                formData.append('eng_maps', data_eng_maps[0]);  

            }  

        } // end of if
        
        // uploade file attach
        var data_attach = $('#attach')[0].files;
        if (data_attach[0] != undefined) {
            
            formData.append('attach', data_attach[0]);

        }   
        
        
        // uploade file image
        var data_image = $('#image')[0].files;
        if (data_image[0] != undefined) {
            
            formData.append('image', data_image[0]);

        }           
     
        // uploade mulitble files images
        var totalfiles = form.find('#images')[0].files.length;

        if (totalfiles > 0) {
            
            for (var index = 0; index < totalfiles; index++) {
    
                formData.append('images[]', form.find('#images')[0].files[index]);
            }
            
        }


        // uplaode other inputs
        formData.append('item', item.val());
        formData.append('currency_id', currency_id.val());
        formData.append('cost', cost.val());
        formData.append('long_desc_en', inctanse_long_desc_en.getData());
        formData.append('long_desc_ar', inctanse_long_desc_ar.getData());
        formData.append('desc_en', desc_en.val());
        formData.append('desc_ar', desc_ar.val());
        formData.append('goals_en', inctanse_goals_en.getData());
        formData.append('goals_ar', inctanse_goals_ar.getData());
        formData.append('benef_num', benef_num.val());
        formData.append('country_id', country_id.val());
        formData.append('city_id', city_id.val());
        formData.append('location', location.val());
        formData.append('lat', lat.val());
        formData.append('lng', lng.val());
        formData.append('start_date', start_date.val());
        formData.append('edu_service_id', edu_service_id.val());
        formData.append('edu_service_other', edu_service_other.val());
        // formData.append('duration', duration.val());
        formData.append('emerg_service_id', emerg_service_id.val());
        formData.append('emerg_service_other', emerg_service_other.val());
        formData.append('name', name.val());
        formData.append('start_date', start_date.val());
        formData.append('care_duration', care_duration.val());
        formData.append('stable_project', stable_project.val());
        formData.append('care_type', care_type.val());
        formData.append('stage', stage.val());
        formData.append('charity_category_id', charity_category_id.val());
        formData.append('type', type);        
        formData.append('name_other', name_other.val());
        formData.append('_method', 'PATCH');


        var stauts = 'edit-project';
        // send request ajax

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            cache: false,               
            success: function(result){

                onFormSuccess(form, stauts, true)

                setTimeout(() => {

                    // window.location.replace(redirect);
                    
                }, 1500);
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error cost
                textFieldError(form , cost , jqXHR.responseJSON.errors.cost);
                // error currency_id
                selectFieldError(form , currency_id , jqXHR.responseJSON.errors.currency_id);
                // error start_date
                textDateFieldError(form , start_date , jqXHR.responseJSON.errors.start_date);
                // error care_duration
                textFieldError(form , care_duration , jqXHR.responseJSON.errors.care_duration);    
                // error stable_project
                selectFieldError(form , stable_project , jqXHR.responseJSON.errors.stable_project);    
                // error edu_service_other
                textFieldError(form , edu_service_other , jqXHR.responseJSON.errors.edu_service_other);    
                // error eng_maps
                fileFieldError(form , eng_maps , jqXHR.responseJSON.errors.eng_maps);    
                // error stage
                textFieldError(form , stage , jqXHR.responseJSON.errors.stage);  
                // error care_type
                textFieldError(form , care_type , jqXHR.responseJSON.errors.care_type);  
                // error location
                textFieldError(form , location , jqXHR.responseJSON.errors.location);  
                // error benef_num
                textFieldError(form , benef_num , jqXHR.responseJSON.errors.benef_num);   
                // error images
                fileFieldError(form , images , jqXHR.responseJSON.errors.images);  
                // error attach
                fileFieldError(form , attach , jqXHR.responseJSON.errors.attach); 
                // error image
                fileFieldError(form , image , jqXHR.responseJSON.errors.image); 
                // error long_desc_en
                ckEditorFieldError(form , long_desc_en , inctanse_long_desc_en , jqXHR.responseJSON.errors.long_desc_en); 
                // error long_desc_ar
                ckEditorFieldError(form , long_desc_ar , inctanse_long_desc_ar , jqXHR.responseJSON.errors.long_desc_ar);  
                // error goals_ar
                ckEditorFieldError(form , goals_ar , inctanse_goals_ar , jqXHR.responseJSON.errors.goals_ar);      
                // error goals_en
                ckEditorFieldError(form , goals_en , inctanse_goals_en , jqXHR.responseJSON.errors.goals_en);   
                // error desc_ar
                fileFieldError(form , desc_ar , jqXHR.responseJSON.errors.desc_ar);       
                // error desc_en
                fileFieldError(form , desc_en , jqXHR.responseJSON.errors.desc_en);                                                                                                                                                                                      
                // error country_id
                selectFieldError(form , country_id , jqXHR.responseJSON.errors.country_id);     
                // error city_id
                selectFieldError(form , city_id , jqXHR.responseJSON.errors.city_id);                                                                                                                                                                                    
                // error name other
                textFieldError(form , name_other , jqXHR.responseJSON.errors.name_other);
                // error  duartion
                // textFieldError(form , duration , jqXHR.responseJSON.errors.duration);
                // error category chartiy
                selectFieldError(form , charity_category_id , jqXHR.responseJSON.errors.charity_category_id);
                // error name
                selectFieldError(form , name , jqXHR.responseJSON.errors.name);
                // error edu_service_id
                selectFieldError(form , edu_service_id , jqXHR.responseJSON.errors.edu_service_id);   
                // error emerg_service_id
                selectFieldError(form , emerg_service_id , jqXHR.responseJSON.errors.emerg_service_id);  
                textFieldError(form , emerg_service_other , jqXHR.responseJSON.errors.emerg_service_other);  
                // error edu_service_other
                textFieldError(form , edu_service_other , jqXHR.responseJSON.errors.edu_service_other);                                                             
                
            } //  end of error

             
        }); // end of ajax

    }); // end of click button

}); // end of document ready

