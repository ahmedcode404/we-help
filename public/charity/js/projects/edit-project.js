$(document).ready(function(){

    if($('#lang').val() == 'ar'){
        var success = 'تم الحذف بنجاح';
        var error = 'حدث خطأ أثناء الحذف، برجاء المحاولة مرة أخرى لاحقاً';
        var html = 'يمكنك التواصل مع الدعم الفنى من خلال البردي التالى: info@jaadara.com';
        var permission = 'ليست لديك صلاحية';
    }
    else{
        var success = 'Item Deleted Successfully';
        var error = 'Something Went Wrong, Please Try Again Later';
        var html = 'You can Contact Technical Support through Following Email: info@jaadara.com';
        var permission = 'You Don not have Permission';
    }

    // Date picker
    // datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }

    // get category services
    $('#charity_category_id').change(function(){

        $('#eng_maps_container').addClass('hidden');
        $('#eng_maps').val(null);

        $('#other_service_option_container').addClass('hidden');
        $('#other_service_option').val(null);

        $('#other_service_feature_container').addClass('hidden');
        $('#other_service_feature').val(null);

        $('#services').html('');
        $('#features').html('');

        var action = $(this).data('action');
        var method = $(this).data('method');

        if($(this).find(':selected').data('type') == 'engineering'){

            $('#eng_maps_container').removeClass('hidden');

            $('#service_option_id').val(null);
            $('#other_service_option').val(null);
            $('#service_feature_id').val(null);
            $('#other_service_feature').val(null);
        }
        else{

            $('#eng_maps_container').addClass('hidden');
            $('#eng_maps').val(null);

            $('#services').removeClass('hidden');
            $('#features').removeClass('hidden');

            // Make ajax request
            $.ajax({
                url: action,
                type: method,
                data:{"_token": $('meta[name="csrf-token"]').attr('content') , id: $(this).val()},
                dataType: 'html',
                success: function (data) {
                    console.log(data);
                    $('#services').html(data);
                
                }
            });
        }

    });

    // get services features
    $(document).on('change', '#service_option_id', function(){

        $('#other_service_feature_container').addClass('hidden');
        $('#other_service_feature').val(null);

        var action = $(this).data('action');
        var method = $(this).data('method');

        if($(this).val() == 'other'){

            $('#other_service_option_container').removeClass('hidden');
            
        }
        else{

            $('#other_service_option_container').addClass('hidden');
            $('#other_service_option').val(null);

            // Make ajax request
            $.ajax({
                url: action,
                type: method,
                data:{"_token": $('meta[name="csrf-token"]').attr('content') , id: $(this).val()},
                dataType: 'html',
                success: function (data) {

                    $('#features').html(data);
                
                }
            });
        }

    });

    // show otehr services features
    $(document).on('change', '#service_feature_id', function(){

        if($(this).val() == 'other'){

            $('#other_service_feature_container').removeClass('hidden');
        }
        else{

            $('#other_service_feature_container').addClass('hidden');
            $('#other_service_feature').val(null);
        }
    });

    //add multi images
    $(".img-btn-success").click(function(){ 
        var html = $(".image_clone").html();
        $(".image_increment").after(html);
    });

    $("body").on("click",".img-btn-danger",function(){ 
        $(this).parents(".control-group").remove();
    });

    // Validate form
    if ($('#basic_info_form').length) {
        $('#basic_info_form').validate({
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
        
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top
                }, 1000);
            },
            rules: {
                project_num: "required",
                charity_id: "required",
                name: "required",
                start_date: "required",
                duration: {
                    required: true,
                    number: true,
                    min: 1,
                    step: 1,
                },
                cost: {
                    required: true,
                    number: true,
                    min: 1,
                    step: 0.1,
                },
                currency_id: "required",
                benef_num: {
                    required: true,
                    number: true,
                    min: 1,
                    step: 1,
                },
                charity_category_id: "required",
                service_option_id: "required",
                other_service_option: {
                    required: function(){
                        return $('#service_option_id').val() == 'other';
                    }
                },
                service_feature_id: "required",
                other_service_feature: {
                    required: function(){
                        return $('#service_feature_id').val() == 'other';
                    }
                },
                status: "required",
                country: "required",
                location: "required",
            },
        });
    }

    // Validate form
    if ($('#goals_and_description_form').length) {
        $('#goals_and_description_form').validate({
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
        
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top
                }, 1000);
            },
            ignore: [],
            rules: {
                desc_ar: "required",
                desc_en: "required",
                long_desc_ar: {
                    required: function(){
                        return CKEDITOR.instances.long_desc_ar.updateElement();
                    }
                },
                long_desc_en: {
                    required: function(){
                        return CKEDITOR.instances.long_desc_en.updateElement();
                    }
                },
                goals_ar: {
                    required: function(){
                        return CKEDITOR.instances.goals_ar.updateElement();
                    }
                },
                goals_en: {
                    required: function(){
                        return CKEDITOR.instances.goals_en.updateElement();
                    }
                },
                
            },
        });
    }



    
});