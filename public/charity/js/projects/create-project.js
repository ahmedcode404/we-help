$(document).ready(function(){

    // Date picker
    var datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }

    // get category services
    $('#charity_category_id').change(function(){

        $('#eng_maps_container').addClass('hidden');
        $('#eng_maps').val(null);

        $('#other_service_option_container').addClass('hidden');
        $('#other_service_option').val(null);

        $('#other_service_feature_container').addClass('hidden');
        $('#other_service_feature').val(null);

        var action = $(this).data('action');
        var method = $(this).data('method');

        if($(this).find(':selected').data('type') == 'engineering'){

            $('#eng_maps_container').removeClass('hidden');

        }
        else{

            $('#eng_maps_container').addClass('hidden');
            $('#eng_maps').val(null);

            // Make ajax request
            $.ajax({
                url: action,
                type: method,
                data:{"_token": $('meta[name="csrf-token"]').attr('content') , id: $(this).val()},
                dataType: 'html',
                success: function (data) {

                    $('#services').html(data);
                
                }
            });
        }

    });

    // get services features
    $(document).on('change', '#service_option_id', function(){

        var action = $(this).data('action');
        var method = $(this).data('method');

        if($(this).val() == 'other'){

            $('#other_service_option_container').removeClass('hidden');
        }
        else{

            $('#other_service_option_container').addClass('hidden');
            $('#other_service_option').val(null);

            // Make ajax request
            $.ajax({
                url: action,
                type: method,
                data:{"_token": $('meta[name="csrf-token"]').attr('content') , id: $(this).val()},
                dataType: 'html',
                success: function (data) {

                    $('#features').html(data);
                
                }
            });
        }

    });

    // show otehr services features
    $(document).on('change', '#service_feature_id', function(){

        if($(this).val() == 'other'){

            $('#other_service_feature_container').removeClass('hidden');
        }
        else{

            $('#other_service_feature_container').addClass('hidden');
            $('#other_service_feature').val(null);
        }
    });

    // Validate form
    if ($('#form-create-project').length) {
        $('#form-create-project').validate({
            focusInvalid: false,
            invalidHandler: function(form, validator) {
                if (!validator.numberOfInvalids())
                    return;
        
                $('html, body').animate({
                    scrollTop: $(validator.errorList[0].element).offset().top
                }, 1000);
            },
            rules: {
                charity_category_id: "required",
                service_option_id: "required",
                other_service_option: {
                    required: function(){
                        return $('#service_option_id').val() == 'other';
                    }
                },
                service_feature_id: "required",
                other_service_feature: {
                    required: function(){
                        return $('#service_feature_id').val() == 'other';
                    }
                },
                start_date: "required",
                location: "required",
                country: "required",
                city: "required",
                benef_num: "required",
                goals_en: {
                    required: function(){
                        return CKEDITOR.instances.goals_en.updateElement();
                    }
                },
                desc_en: "required",
                long_desc_en: {
                    required: function(){
                        return CKEDITOR.instances.long_desc_en.updateElement();
                    }
                },
                images: "required",
                attach: "required",
                image: "required",
                currency_id: "required",
                cost: "required",
            }
        });
    }
        
});

