$(document).ready(function() {

    $('#country_id').change(function() {

            var id = $(this).val();

            var url = $(this).data('action');
            
            $('.append-city').html("");


            $.ajax({
                type: 'GET',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: url,
                data: { 'id': id },
                dataType: 'html',
                success: function(result) {

                        console.log(result);

                        $('.append-city').html(result);

                    } // end of success

            }); // end of ajax


        }) // end of other name

});