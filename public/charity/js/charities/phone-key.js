$(document).ready(function () {
    
    
    var tel_url = $('#tel_url').val();
    var input = document.querySelector("#phone");
    var mobile = document.querySelector("#mobile");
    window.intlTelInput(input, {
        preferredCountries: ["gb", "sa"],
        separateDialCode: true,
        hiddenInput: "phone",
        formatOnDisplay: false,
        utilsScript: tel_url
    });

    window.intlTelInput(mobile, {
        preferredCountries: ["gb", "sa"],
        separateDialCode: true,
        hiddenInput: "mobile",
        formatOnDisplay: false,
        utilsScript: tel_url
    });

    // var input_phone = document.querySelector("#phone");
    // window.intlTelInput(input_phone, {
    //     preferredCountries: ["gb", "sa"],
    //     hiddenInput: "phone_key",
    //     separateDialCode:true,
    //     formatOnDisplay:false,
    //     utilsScript: "{{ url('dashboard/js/utils.js') }}"
    // });

    var input_bank_phone = document.querySelector("#bank_phone");
    window.intlTelInput(input_bank_phone, {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "bank_phone_key",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: tel_url
    });  
    

})
