$(document).ready(function(){

    var tel_url = $('#tel_url').val();
    // phone
    var input = document.querySelector("#phone");
    window.intlTelInput(input, {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "phone",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: tel_url
    });

    // mobile
    var input_mobile = document.querySelector("#mobile");
    window.intlTelInput(input_mobile, {
        preferredCountries: ["gb", "sa"],
        hiddenInput: "mobile",
        separateDialCode:true,
        formatOnDisplay:false,
        utilsScript: tel_url
    });


    // validate forms
    // Basic info
    if ($('#basic_info_form').length) {
        $('#basic_info_form').validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                phone_num: {
                        required: true,
                        
                },
                mobile_num: {
                    required: true,     
                },
            },
        });
    }

});