$(document).ready(function(){
    /////////////////////////////////////////////////////// basic_info_form ////////////////////////////////////////////////

    $('#click_basic_info_form').click(function(e){

        e.preventDefault();
        
        // get all id inputs
        var form   = $('#basic_info_form');
        // show loadinng
        onFormSubmit(form); 

        var type_form  = form.find('.type_form');
        var name       = form.find('#name');
        var email      = form.find('#email');
        var type       = form.find('#type');
        var country    = form.find('#country');
        var city       = form.find('#city');
        var phone_num  = form.find('#phone');
        var phone  = form.find(phone_num).parent().find('.iti__selected-dial-code').text()+phone_num.val();
        var mobile_num = form.find('#mobile');
        var mobile  = form.find(mobile_num).parent().find('.iti__selected-dial-code').text()+mobile_num.val();
        // var phone      = form.find('input[name="phone"]');
        // var mobile      = form.find('input[name="mobile"]');
        var address_ar = form.find('#address_ar');
        var address_en = form.find('#address_en');
        var formData = new FormData();

        // uploade logo
        var logo = $('#logo')[0].files;
        if (logo[0] != undefined) {
            
            formData.append('logo', logo[0]);

        } // end of      

        formData.append('type_form', type_form.val());
        formData.append('name', name.val());
        formData.append('email', email.val());
        formData.append('type', type.val());
        formData.append('country', country.val());
        if (city.val() != undefined) {
            
            formData.append('city', city.val());

        }

        // if (phone.val().length <= 9) {} else {

        //     formData.append('phone', phone_key + phone.val());

        // }       

        formData.append('phone_num', phone_num.val());
        formData.append('phone', phone);
        formData.append('mobile_num', mobile_num.val());
        formData.append('mobile', mobile);
        formData.append('address_ar', address_ar.val());
        formData.append('address_en', address_en.val());
        formData.append('_method', 'PATCH');        

        // route post request
        var url = $(this).attr('url');
        var redirect = $(this).attr('redirect');        

        var stauts = 'edit-charity';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,  
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(result){

                onFormSuccess(form, stauts, false)
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textFieldError(form         , name       , jqXHR.responseJSON.errors.name);
                selectFieldError(form       , type       , jqXHR.responseJSON.errors.type);
                selectFieldError(form       , country , jqXHR.responseJSON.errors.country);
                selectFieldError(form       , city    , jqXHR.responseJSON.errors.city);
                textFieldError(form         , email      , jqXHR.responseJSON.errors.email);

                textFieldPhoneKeyError(form , phone_num      , jqXHR.responseJSON.errors.phone_num);
                textFieldPhoneKeyError(form , phone_num      , jqXHR.responseJSON.errors.phone);

                textFieldPhoneKeyError(form   , mobile_num     , jqXHR.responseJSON.errors.mobile_num);
                textFieldPhoneKeyError(form   , mobile_num     , jqXHR.responseJSON.errors.mobile);

                textFieldError(form         , address_ar , jqXHR.responseJSON.errors.address_ar);
                textFieldError(form         , address_en , jqXHR.responseJSON.errors.address_en);
                fileFieldError(form         , logo       , jqXHR.responseJSON.errors.logo);

            } //  end of error

             
        }); // end of ajax

    }); // end of click_basic_info_form

    /////////////////////////////////////////////////////// official_info_data ////////////////////////////////////////////////

    $('#click_official_info_data').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form               = $('#official_info_data'); 

        // show loadinng
        onFormSubmit(form); 

        var type_form          = form.find('.type_form');
        var license_number     = form.find('#license_number');
        var license_file       = form.find('#license_file');
        var license_start_date = form.find('#license_start_date');
        var license_end_date   = form.find('#license_end_date');
        var establish_date     = form.find('#establish_date');
        var branches_num       = form.find('#branches_num');
        var members_num        = form.find('#members_num');
        var contract_date      = form.find('#contract_date');
        var years              = form.find('#years');
        var strategy_ar        = form.find('#strategy_ar');
        var strategy_en        = form.find('#strategy_en');
        var work_fields        = form.find('#work_fields');

        // // inctanse ckeditor
        inctanse_strategy_ar = CKEDITOR.instances.strategy_ar
        inctanse_strategy_en = CKEDITOR.instances.strategy_en

        var formData = new FormData();

        // uploade license_file
        var license_file = $('#license_file')[0].files;
        if (license_file[0] != undefined) {
            
            formData.append('license_file', license_file[0]);

        } // end of           

        // uploade attach
        var attach = $('#attach')[0].files;
        if (attach[0] != undefined) {
            
            formData.append('attach', attach[0]);

        } // end of   
        
        // uploade year_report
        var year_report = $('#year_report')[0].files;
        if (year_report[0] != undefined) {
            
            formData.append('year_report', year_report[0]);

        } // end of 
        
        // uploade internal_image
        var internal_image = $('#internal_image')[0].files;
        if (internal_image[0] != undefined) {
            
            formData.append('internal_image', internal_image[0]);

        } // end of         

        formData.append('type_form', type_form.val());
        formData.append('license_number', license_number.val());
        formData.append('license_start_date', license_start_date.val());
        formData.append('license_end_date', license_end_date.val());
        formData.append('establish_date', establish_date.val());
        formData.append('branches_num', branches_num.val());
        formData.append('members_num', members_num.val());
        formData.append('contract_date', contract_date.val() );
        formData.append('strategy_ar', inctanse_strategy_ar.getData());
        formData.append('strategy_en', inctanse_strategy_en.getData());        
        formData.append('work_fields', work_fields.text());        
        formData.append('years', years.val());
        formData.append('_method', 'PATCH');        

        // route post request
        var url = $(this).attr('url');

        var stauts = 'edit-charity';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,  
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(result){

                onFormSuccess(form, stauts, false)
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textDateFieldError(form , license_start_date , jqXHR.responseJSON.errors.license_start_date);
                textDateFieldError(form , license_end_date   , jqXHR.responseJSON.errors.license_end_date);
                textDateFieldError(form , establish_date     , jqXHR.responseJSON.errors.establish_date);
                textDateFieldError(form , contract_date      , jqXHR.responseJSON.errors.contract_date);
                textFieldError(form     , branches_num       , jqXHR.responseJSON.errors.branches_num);
                textFieldError(form     , members_num        , jqXHR.responseJSON.errors.members_num);
                selectFieldError(form   , years              , jqXHR.responseJSON.errors.years);
                fileFieldError(form     , attach             , jqXHR.responseJSON.errors.attach);
                fileFieldError(form     , license_number     , jqXHR.responseJSON.errors.license_number);
                fileFieldError(form     , year_report        , jqXHR.responseJSON.errors.year_report);
                fileFieldError(form     , work_fields        , jqXHR.responseJSON.errors.work_fields);
                fileFieldError(form     , internal_image     , jqXHR.responseJSON.errors.internal_image);
                ckEditorFieldError(form , strategy_ar        , inctanse_strategy_ar , jqXHR.responseJSON.errors.strategy_ar);      
                ckEditorFieldError(form , strategy_en        , inctanse_strategy_en , jqXHR.responseJSON.errors.strategy_en);      

            } //  end of error

             
        }); // end of ajax

    }); // end of click_official_info_data

    /////////////////////////////////////////////////////// representer form ////////////////////////////////////////////////

    // click show ccontainer
    $('#international_member').click(function () {

        if ($(this).is(':checked')) {

            $('#international_name_container').show();            
            
        } else {

            $('#international_name_container').hide();            

        }
        
    }) // end of international_member

    $('#click_representer_form').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form               = $('#representer_form'); 

        // show loadinng
        onFormSubmit(form); 

        var type_form                  = form.find('.type_form');
        var representer_name_ar        = form.find('#representer_name_ar');
        var representer_name_en        = form.find('#representer_name_en');
        var representer_title_ar       = form.find('#representer_title_ar');
        var representer_title_en       = form.find('#representer_title_en');
        var representer_title_file     = form.find('#representer_title_file');
        var international_member       = form.find('#international_member:checked');
        var international_name_en      = form.find('#international_name_en');
        var international_name_ar      = form.find('#international_name_ar');
        var representer_passport_image = form.find('#representer_passport_image');
        var representer_nation_image   = form.find('#representer_nation_image');

        var formData = new FormData();

        // uploade representer_title
        var representer_title_file = $('#representer_title_file')[0].files;
        if (representer_title_file[0] != undefined) {
            
            formData.append('representer_title_file', representer_title_file[0]);

        } // end of           

        // uploade representer_passport_image
        var representer_passport_image = $('#representer_passport_image')[0].files;
        if (representer_passport_image[0] != undefined) {
            
            formData.append('representer_passport_image', representer_passport_image[0]);

        } // end of   
        
        // uploade representer_nation_image
        var representer_nation_image = $('#representer_nation_image')[0].files;
        if (representer_nation_image[0] != undefined) {
            
            formData.append('representer_nation_image', representer_nation_image[0]);

        } // end of 
              
        formData.append('type_form', type_form.val());
        formData.append('representer_name_ar', representer_name_ar.val());
        formData.append('representer_name_en', representer_name_en.val());
        formData.append('representer_title_ar', representer_title_ar.val());
        formData.append('representer_title_en', representer_title_en.val());

        if (international_member.val() != undefined) {
            
            formData.append('international_member', international_member.val());
            formData.append('international_name_en', international_name_en.val());
            formData.append('international_name_ar', international_name_ar.val());       

        }
        
        formData.append('_method', 'PATCH');        

        // route post request
        var url = $(this).attr('url');

        var stauts = 'edit-charity';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,  
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(result){

                onFormSuccess(form, stauts, false)
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textFieldError(form     , representer_name_ar        , jqXHR.responseJSON.errors.representer_name_ar);
                textFieldError(form     , representer_name_en        , jqXHR.responseJSON.errors.representer_name_en);
                fileFieldError(form     , representer_title_ar          , jqXHR.responseJSON.errors.representer_title_ar);
                fileFieldError(form     , representer_title_en          , jqXHR.responseJSON.errors.representer_title_en);
                fileFieldError(form     , representer_title_file          , jqXHR.responseJSON.errors.representer_title_file);
                fileFieldError(form     , representer_passport_image , jqXHR.responseJSON.errors.representer_passport_image);
                fileFieldError(form     , representer_nation_image   , jqXHR.responseJSON.errors.representer_nation_image);
                ckEditorFieldError(form , international_name_en      , international_name_en , jqXHR.responseJSON.errors.international_name_en);      
                ckEditorFieldError(form , international_name_ar      , international_name_ar , jqXHR.responseJSON.errors.international_name_ar);      

            } //  end of error

             
        }); // end of ajax

    }); // end of click_official_info_data 
    
    /////////////////////////////////////////////////////// bank data form ////////////////////////////////////////////////

    $('#click_bank_data_form').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form        = $('#bank_data_form'); 

        // show loadinng
        onFormSubmit(form); 

        var type_form   = form.find('.type_form');
        var iban        = form.find('#iban');
        var bank_name   = form.find('#bank_name');
        var bank_address = form.find('#bank_address');
        var bank_city   = form.find('#bank_city');
        var bank_phone  = form.find('#bank_phone');
        var phone_key   = form.find('.iti__selected-dial-code').text();        
        var swift_code  = form.find('#swift_code');

        var formData = new FormData();

        formData.append('type_form', type_form.val());
        formData.append('iban', iban.val());
        formData.append('bank_name', bank_name.val());
        formData.append('bank_address', bank_address.val());
        formData.append('bank_city', bank_city.val());

        // if (bank_phone.val().length <= 9) 
        // {

            formData.append('bank_phone', phone_key + bank_phone.val());

        // }
        
        formData.append('swift_code', swift_code.val() );
        formData.append('_method', 'PATCH');        

        // route post request
        var url = $(this).attr('url');

        var stauts = 'edit-charity';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,  
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(result){

                onFormSuccess(form, stauts, false)
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textFieldError(form , iban        , jqXHR.responseJSON.errors.iban);
                textFieldError(form , bank_name   , jqXHR.responseJSON.errors.bank_name);
                textFieldError(form , bank_address , jqXHR.responseJSON.errors.bank_address);
                textFieldError(form , bank_city   , jqXHR.responseJSON.errors.bank_city);
                textFieldPhoneKeyError(form , bank_phone  , jqXHR.responseJSON.errors.bank_phone);
                textFieldError(form , swift_code  , jqXHR.responseJSON.errors.swift_code);

            } //  end of error

             
        }); // end of ajax

    }); // end of click_official_info_data    

    /////////////////////////////////////////////////////// socials form ////////////////////////////////////////////////

    $('#click_socials_form').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form        = $('#socials_form'); 

        // show loadinng
        onFormSubmit(form); 

        var type_form   = form.find('.type_form');
        var website        = form.find('#website');
        var facebook_link   = form.find('#facebook_link');
        var twitter_link = form.find('#twitter_link');
 

        var formData = new FormData();

        formData.append('type_form', type_form.val());
        formData.append('website', website.val());
        formData.append('facebook_link', facebook_link.val());
        formData.append('twitter_link', twitter_link.val());
        formData.append('_method', 'PATCH');        

        // route post request
        var url = $(this).attr('url');

        var stauts = 'edit-charity';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,  
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            success: function(result){

                onFormSuccess(form, stauts, false)
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textFieldError(form , website   , jqXHR.responseJSON.errors.website);
                textFieldError(form , facebook_link , jqXHR.responseJSON.errors.facebook_link);
                textFieldError(form , twitter_link   , jqXHR.responseJSON.errors.twitter_link);


            } //  end of error

             
        }); // end of ajax

    }); // end of click_official_info_data        

}); // end of document ready

