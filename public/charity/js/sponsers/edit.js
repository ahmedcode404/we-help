$(document).ready(function(){

    $('#click-form-sponser').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form                = $('#form-sponser');

        // show loadinng
        onFormSubmit(form);      

        var item            = form.find('#item');
        var project_id      = form.find('#project_id');
        var work_field_id   = form.find('#work_field_id');
        var name_sponser_ar = form.find('#name_sponser_ar');
        var name_sponser_en = form.find('#name_sponser_en');

        // route post request
        var url = $(this).attr('url');
        var redirect = window.location.origin + "/charity/projects/" + project_id.val();

        var formData = new FormData();

        // uplaode other inputs
        formData.append('item', item.val());
        formData.append('project_id', project_id.val());
        formData.append('work_field_id', work_field_id.val());
        formData.append('name_sponser_ar', name_sponser_ar.val());
        formData.append('name_sponser_en', name_sponser_en.val());
        
        formData.append('_method', 'PATCH');

        var stauts = 'edit-sponser';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,   
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            cache: false,                         
            success: function(result){

                onFormSuccess(form, stauts, true)

                setTimeout(() => {

                    window.location.replace(redirect);
                    
                }, 1500);
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textFieldError(form   , name_sponser_ar , jqXHR.responseJSON.errors.name_sponser_ar);
                textFieldError(form   , name_sponser_en , jqXHR.responseJSON.errors.name_sponser_en);
                selectFieldError(form , project_id      , jqXHR.responseJSON.errors.project_id);
                selectFieldError(form , work_field_id   , jqXHR.responseJSON.errors.work_field_id);

            } //  end of error

             
        }); // end of ajax

    }); // end of click button

    $(document).on('change' , '.calculate' , function () {

        var start = new Date($('#start_date').val());
        var end   = new Date($('#end_date').val());

        const diffTime = Math.abs(end - start);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        $('#duration').val(diffDays);

    })    

}); // end of document ready

