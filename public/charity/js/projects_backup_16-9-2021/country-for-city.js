$(document).ready(function() {

    $('#country_id').change(function() {

            var id = $(this).val();

            var url = $(this).data('action');
            
            $('.append-cities').html("");


            $.ajax({
                type: 'GET',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                url: url,
                data: { 'id': id },
                dataType: 'html',
                success: function(result) {

                        // console.log(result);

                        $('.append-cities').html(result);

                    } // end of success

            }); // end of ajax


        }) // end of other name

});