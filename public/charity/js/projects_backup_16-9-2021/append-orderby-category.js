$(document).ready(function() {

    $('#charity_category_id').change(function() {

            // service emerg or edu
            $('.append-orderby-category-service').hide();
            // eng maps
            $('.append-orderby-category-1').html('');   
            // stages
            $('.append-orderby-category-2').hide();

            // care type
            $('.append-orderby-category-3').hide();
            // care duration
            $('.append-orderby-category-4').hide();
            // stable project
            $('.append-orderby-category-5').hide();
            
            
            $('.care_type_select').html("");

            var id = $(this).find(':selected').data('type');

            var  eng_maps = $('#eng_maps_lable').attr('eng_maps');
            
            // stages
            var stages_en = ['primary', 'secondary', 'intermediate'];
            var stages_ar = ['ابتدائي', 'متوسط', 'ثانوي'];
            var stage_option = $('.stage_select');

            // care type
            var care_type_en = ['clothing', 'warranty' ,'feast'];
            var care_type_ar = ['رعاية', 'عيدية' , 'كسوة'];            
            var care_type = $('.care_type_select');

            // stable project
            var stable_project_en = ['debt', 'loan' ,'donation'];
            var stable_project_ar = ['دين', 'قرض' , 'هبة'];            
            var stable_project  = $('#stable_project');            

            // get language
            var lang = $('.loaded').attr('lang');

            if (id == 'engineering') {

                var append = 
                `<div class="form-group">
                    <label>${eng_maps}</label>
                    <div class="custom-file">
                        <input type="file" class="form-control image" id="eng_maps" name="eng_maps" />
                        <span class="error-input"></span>
                    </div>
                    <div class="form-group prev" >
                        <img src="" style="width: 100px" class="img-thumbnail preview-eng_maps" alt="">
                    </div>                    
                </div>`;

                $('.append-orderby-category-1').show();
                $('.append-orderby-category-1').html(append);

            } else if(id == 'educational') {

                $('.append-orderby-category-2').show();

                stage_option.html("");

                if(lang == 'ar')
                {

                    // stages arabic
                    $.each(stages_ar , function(index , stage) {
                    
                        var option = '<option value="'+ stages_en[index] +'">'+ stage +'</option>';
    
                        stage_option.append(option);
    
                    });                    

                } else 
                {
                    // stages english
                    $.each(stages_en , function(index , stage) {
                    
                        var option = '<option value="' + stages_en[index] + '">' + stage + '</option>';
    
                        stage_option.append(option);
    
                    }); 

                } // end of else 


                // get services edu
                var url_edu = $(this).attr('url-edu');

                $.ajax({
                    type: 'GET',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: url_edu,
                    data: {},
                    dataType: 'html',
                    success: function(result) {
    
                        $('.append-orderby-category-1').show();
                        $('.append-orderby-category-1').html(result);
    
                        } // end of success
    
                }); // end of ajax                 
                
            } else if(id == 'sponsorship') 
            {

                // $('.append-orderby-category-6').hide();
                $('.append-orderby-category-3').show();
                $('.append-orderby-category-4').hide();
                var care_duration_en = ['annual', 'cut_off'];
                var care_duration_ar = ['سنوية', 'مقطوعة']; 
                var care_duration = $('#care_duration');

                care_duration.html("");

                care_type.html("");

                if(lang == 'ar')
                {

                    // stages arabic
                    $.each(care_type_ar , function(index , type) {
                    
                        var option = '<option value="'+ care_type_en[index] +'">'+ type +'</option>';
    
                        care_type.append(option);
    
                    });  
                    
                    $.each(care_duration_ar , function(index , duration) {
                        var option = '<option value="' + care_duration_en[index] + '">' + duration + '</option>';
                        care_duration.append(option);

                    });                       

                } else 
                {
                    // stages english
                    $.each(care_type_en , function(index , type) {
                    
                        var option = '<option value="' + care_type_en[index] + '">' + type + '</option>';
    
                        care_type.append(option);
    
                    }); 

                    $.each(care_duration_en , function(index , duration) {
                        var option = '<option value="' + care_duration_en[index] + '">' + duration + '</option>';
                        care_duration.append(option);
                    });                       

                } // end of else                 

            } else if(id == 'emergency')
            {

                var url_emerg = $(this).attr('url-emerg');

                $.ajax({
                    type: 'GET',
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    url: url_emerg,
                    data: {},
                    dataType: 'html',
                    success: function(result) {
    
                        $('.append-orderby-category-1').show();
                        $('.append-orderby-category-1').html(result);
    
                        } // end of success
    
                }); // end of ajax                

                
            } else if(id == 'sustainable')
            {

                $('.append-orderby-category-5').show();

                stable_project.html("");

                if(lang == 'ar')
                {

                    // stages arabic
                    $.each(stable_project_ar , function(index , stable_pro) {
                    
                        var option = '<option value="'+ stable_project_en[index] +'">'+ stable_pro +'</option>';
    
                        stable_project.append(option);
    
                    });                    

                } else 
                {

                    // stages english
                    $.each(stable_project_en , function(index , stable_pro) {
                    
                        var option = '<option value="' + stable_project_en[index] + '">' + stable_pro + '</option>';
    
                        stable_project.append(option);
    
                    }); 

                } // end of else                 

            } // end of else if

        }) // end of categories charity

        // append input for add Other Services
        $(document).on( 'change','.emerg_service_id' , function (e) {

            var name = $(this).val();

            var  emerg_service_other = $('#emerg_service_other').attr('emerg_service_other');

            // append other service
            $('.append-orderby-category-service').hide();

            if(name == 'other')
            {

                $('.append-orderby-category-service').show();

                var append = 
                `<div class="form-group">
                    <label class="form-label" for="basic-default-name">${emerg_service_other}</label>
                    <input type="text" class="form-control" id="emerg_service_other" name="emerg_service_other" placeholder="${emerg_service_other}" />
                    <span class="error-input"></span>
                </div>`;

                $('.append-orderby-category-service').html(append);

            }
            
        }) // end of append input for add Other Services        

        // append input for add Other Services
        $(document).on( 'change','.edu_service_id' , function (e) {

            var name = $(this).val();

            var  edu_service_other = $('#edu_service_other').attr('edu_service_other');


            // append other service
            
            if(name == 'other')
            {
                $('.append-orderby-category-service').html('');

                
                var append = 
                `<div class="form-group">
                <label class="form-label" for="basic-default-name">${edu_service_other}</label>
                <input type="text" class="form-control" id="edu_service_other" name="edu_service_other" placeholder="${edu_service_other}" />
                <span class="error-input"></span>
                </div>`;
                
                $('.append-orderby-category-service').show();
                $('.append-orderby-category-service').html(append);

            }
            
        }) // end of append input for add Other Services        

}); // end of document ready

