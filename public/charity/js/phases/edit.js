$(document).ready(function(){

    // Date picker
    var datePickr = $('.date-picker');

    // if (datePickr.length) {
    //     datePickr.flatpickr();
    // }

    $(document).on('change' , '#project_id' , function () {

        $('#input-created-at').html('');

        var id = $(this).val();

        var url = $(this).attr('url');

        $.ajax({
            type: 'GET',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            url: url,
            data: { 'id': id },
            dataType: 'html',           
            success: function(result){

                $('#input-created-at').html(result);

                // Date picker
                var datePickr = $('.date-picker');

                $('.date-picker').datepicker({
                    changeYear: true,
                    dateFormat: 'yy-mm-dd',
                })
                
            } // end of success

        }); // end of ajax        
        
    }) // end of change select


    $('#click-form-phases').click(function(e){

        e.preventDefault();

        // get all id inputs
        var form                = $('#form-phases');

        // show loadinng
        onFormSubmit(form);      

        var item            = form.find('#item');
        var project_id      = form.find('#project_id');
        var name_ar         = form.find('#name_ar');
        var name_en         = form.find('#name_en');
        var start_date      = form.find('#start_date');
        var end_date        = form.find('#end_date');
        var duration        = form.find('#duration');
        var cost            = form.find('#cost');
        var order           = form.find('#order');
        var output_ar       = form.find('#output_ar');
        var output_en       = form.find('#output_en');
        var desc_ar         = form.find('#desc_ar');
        var desc_en         = form.find('#desc_en');
        var cost_details_ar = form.find('#cost_details_ar');
        var cost_details_en = form.find('#cost_details_en');

        // inctanse ckeditor
        inctanse_output_ar       = CKEDITOR.instances.output_ar
        inctanse_output_en       = CKEDITOR.instances.output_en
        inctanse_desc_ar         = CKEDITOR.instances.desc_ar
        inctanse_desc_en         = CKEDITOR.instances.desc_en
        inctanse_cost_details_ar = CKEDITOR.instances.cost_details_ar
        inctanse_cost_details_en = CKEDITOR.instances.cost_details_en

        // route post request
        var url = $(this).attr('url');
        // var redirect = window.location.origin + "/charity/projects/" + project_id.val();

        var formData = new FormData();

        // uplaode other inputs
        formData.append('item', item.val());
        formData.append('cost', cost.val());
        formData.append('project_id', project_id.val());
        formData.append('output_ar', inctanse_output_ar.getData());
        formData.append('output_en', inctanse_output_en.getData());
        formData.append('desc_ar', inctanse_desc_ar.getData());
        formData.append('desc_en', inctanse_desc_en.getData());
        formData.append('cost_details_ar', inctanse_cost_details_ar.getData());
        formData.append('cost_details_en', inctanse_cost_details_en.getData());
        formData.append('name_ar', name_ar.val());
        formData.append('name_en', name_en.val());
        formData.append('end_date', end_date.val());
        formData.append('start_date', start_date.val());
        formData.append('order', order.val());
        formData.append('duration', duration.val());
        formData.append('_method', 'PATCH');


        var stauts = 'edit-phase';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,   
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            cache: false,                         
            success: function(result){

                if(result.status == 0){

                    Swal.fire({
                        icon: 'error',
                        title: result.message,
                        showConfirmButton: false,
                        timer: 1500,
                    });

                    form.find('.processing-div').hide();
                    
                    form.find('button[type="button"]').show();

                    form.find('button[type="button"]').prop('disabled', false);
                }
                else{
                    
                    onFormSuccess(form, stauts, true)

                    setTimeout(() => {

                        window.location.replace(result.redirect);
                        
                    }, 1500);
                }
                
            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textFieldError(form , cost , jqXHR.responseJSON.errors.cost);
                selectFieldError(form , project_id , jqXHR.responseJSON.errors.project_id);
                textFieldError(form , name_ar , jqXHR.responseJSON.errors.name_ar);
                textFieldError(form , name_en , jqXHR.responseJSON.errors.name_en);
                textDateFieldError(form , end_date , jqXHR.responseJSON.errors.end_date);
                textDateFieldError(form , start_date , jqXHR.responseJSON.errors.start_date);
                selectFieldError(form , order , jqXHR.responseJSON.errors.order);
                textFieldError(form , duration , jqXHR.responseJSON.errors.duration);
                ckEditorFieldError(form , output_ar , inctanse_output_ar , jqXHR.responseJSON.errors.output_ar);      
                ckEditorFieldError(form , output_en , inctanse_output_en , jqXHR.responseJSON.errors.output_en);      
                ckEditorFieldError(form , desc_ar , inctanse_desc_ar , jqXHR.responseJSON.errors.desc_ar);      
                ckEditorFieldError(form , desc_en , inctanse_desc_en , jqXHR.responseJSON.errors.desc_en);      
                ckEditorFieldError(form , cost_details_ar , inctanse_cost_details_ar , jqXHR.responseJSON.errors.cost_details_ar);      
                ckEditorFieldError(form , cost_details_en , inctanse_cost_details_en , jqXHR.responseJSON.errors.cost_details_en);      

            } //  end of error

             
        }); // end of ajax

    }); // end of click button

    $(document).on('change' , '.calculate' , function () {

        var start = new Date($('#start_date').val());
        var end   = new Date($('#end_date').val());

        const diffTime = Math.abs(end - start);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        $('#duration').val(diffDays);

    })    

}); // end of document ready

