$(document).ready(function () {

    $('#click-form-report').click(function (e) {

        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // get all id inputs
        var form = $('#form-report');

        // show loadinng
        onFormSubmit(form);


        var report_ar  = form.find('#report_ar');
        var report_en  = form.find('#report_en');
        var vedio      = form.find('#vedio');
        var images     = form.find('#images');
        var project_id = form.find('#project_id');
        var phase_id   = form.find('#phase_id');
        var type       = form.find('#type');

        // inctanse ckeditor
        inctanse_report_ar = CKEDITOR.instances.report_ar
        inctanse_report_en = CKEDITOR.instances.report_en

        // route post request
        var url = $(this).attr('url');
        var redirect = $(this).attr('redirect');

        var formData = new FormData();

        // uploade mulitble files images
        var totalfiles = form.find('#images')[0].files.length;
        for (var index = 0; index < totalfiles; index++) {
            formData.append('images[]', form.find('#images')[0].files[index]);
        }

        var data_vedio = $('#vedio')[0].files;  

        // uplaode other inputs
        formData.append('vedio', data_vedio[0]);  
        formData.append('report_ar', inctanse_report_ar.getData());
        formData.append('report_en', inctanse_report_en.getData());
        formData.append('project_id', project_id.val());
        formData.append('phase_id', phase_id.val());
        formData.append('type', type.val());

        var stauts = 'report';
        // send request ajax
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {

                onFormSuccess(form, stauts, true)

                setTimeout(() => {

                    // window.location.replace(redirect);

                }, 2000);

            }, // end of success
            error: function (jqXHR) {

                // hide loadinng
                onFormErrors(form);

                // error validation
                textFieldError(form, vedio, jqXHR.responseJSON.errors.vedio);
                fileFieldError(form, images, jqXHR.responseJSON.errors.images);
                ckEditorFieldError(form, report_ar, inctanse_report_ar, jqXHR.responseJSON.errors.report_ar);
                ckEditorFieldError(form, report_en, inctanse_report_en, jqXHR.responseJSON.errors.report_en);

            } //  end of error


        }); // end of ajax

    }); // end of click button  


       function _(el) {
            return document.getElementById(el);
       }
       
    
       function uploadFile() {
        var file = _("vedio").files[0];
        // alert(file.name+" | "+file.size+" | "+file.type);
        var formdata = new FormData();
        formdata.append("vedio", file);
        var ajax = new XMLHttpRequest();
        ajax.upload.addEventListener("progress", progressHandler, false);
        ajax.addEventListener("load", completeHandler, false);
        ajax.addEventListener("error", errorHandler, false);
        ajax.addEventListener("abort", abortHandler, false);
        ajax.open("POST", "file_upload_parser.php");
        ajax.send(formdata);
       }
       
       function progressHandler(event) {
          $("#progressBar").show().css("display","block");;
        _("loaded_n_total").innerHTML = "تم تحميل " + event.loaded + " من " + event.total;
        var percent = (event.loaded / event.total) * 100;
        _("progressBar").value = Math.round(percent);
        _("status").innerHTML = "  جاري التحميل " + Math.round(percent) + "%";
       }
       
       function completeHandler(event) {
          $("#progressBar").hide().css("display","none");
          _("status").innerHTML = "  تم التحميل ";
        _("progressBar").value = 0; //wil clear progress bar after successful upload
       }
       
       function errorHandler(event) {
        _("status").innerHTML = "فشل التحميل";
       }
       
       function abortHandler(event) {
        _("status").innerHTML = "تم إلغاء التحميل";
       }
       
       $("#vedio").change(function(){
          uploadFile()
       });




}); // end of document ready
