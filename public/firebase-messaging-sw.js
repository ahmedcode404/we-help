
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js');

var firebaseConfig = {
    apiKey: "AIzaSyCMN71jZ2IiJbCv9K-9eA-hnVOqI6k5okY",
    authDomain: "we-help-98457.firebaseapp.com",
    projectId: "we-help-98457",
    storageBucket: "we-help-98457.appspot.com",
    messagingSenderId: "418471964638",
    appId: "1:418471964638:web:f5768ae112e44b9c75f74f",
    measurementId: "G-RWK9VF5P3Y"  
};

firebase.initializeApp(firebaseConfig);
const messaging=firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    // console.log(payload);
    const title = "Hello world is awesome";
    const options = {
        body: "Your notificaiton message .",
        icon: "/firebase-logo.png",
    };

    return self.registration.showNotification(
        title,
        options,
    );
});